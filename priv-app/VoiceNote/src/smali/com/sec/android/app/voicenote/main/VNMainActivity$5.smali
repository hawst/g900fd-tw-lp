.class Lcom/sec/android/app/voicenote/main/VNMainActivity$5;
.super Ljava/lang/Object;
.source "VNMainActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 2064
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 11
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "iBinder"    # Landroid/os/IBinder;

    .prologue
    const-wide/16 v6, -0x1

    const/4 v10, 0x1

    const/16 v9, 0x3ec

    const/16 v8, 0x3eb

    .line 2067
    const-string v4, "VerificationLog"

    const-string v5, "Excuted"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->noI(Ljava/lang/String;Ljava/lang/String;)V

    .line 2068
    const-string v4, "VoiceNoteMainActivity"

    const-string v5, "onServiceConnected()"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2070
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-static {p2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v5

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$802(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    .line 2071
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$900(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v4

    if-nez v4, :cond_0

    .line 2072
    const-string v4, "category_text"

    invoke-static {v4, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getLongSettings(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v4, v6, v4

    if-eqz v4, :cond_0

    .line 2073
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    iget-boolean v4, v4, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v4, :cond_0

    .line 2074
    const-string v4, "category_text"

    invoke-static {v4, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;J)V

    .line 2078
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 2079
    .local v1, "ft":Landroid/app/FragmentTransaction;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawLibraryList(Landroid/app/FragmentTransaction;)V
    invoke-static {v4, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1000(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;)V

    .line 2081
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isResumed()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2082
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isLockScreen()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2083
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->hideNotification()Z

    .line 2085
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIServiceCallback:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1100(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->registerCallback(Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;)V

    .line 2086
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v5, 0x1

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mRegistedIService:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1202(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z

    .line 2089
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromFindo()Z
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1300(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2090
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v4

    if-eq v4, v9, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v4

    if-ne v4, v8, :cond_7

    .line 2092
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->saveRecording()Z

    .line 2096
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v5, 0x0

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoListwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V
    invoke-static {v4, v1, v5, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1500(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;ZZ)V

    .line 2097
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->displayToolbarHelp()V
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1600(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    .line 2098
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mPlayIdFromFindo:J
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->onPressListView(J)Z

    .line 2099
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "intent_extra_data_key"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2142
    :cond_4
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isResumed()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2143
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->updateUIThread()V
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    .line 2144
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v5, 0x0

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawMainview(Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V
    invoke-static {v4, v1, v5, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2600(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;ZLjava/lang/CharSequence;)V

    .line 2145
    const-string v4, "change_notification_service"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2146
    const-string v4, "change_notification_service"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 2150
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "android.intent.action.RUN"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 2152
    .local v3, "shouldStartRecording":Z
    if-eqz v3, :cond_6

    .line 2153
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "android.intent.action.RUN"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2154
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doRecord(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2160
    .end local v3    # "shouldStartRecording":Z
    :cond_6
    :goto_2
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 2161
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->invalidateOptionsMenu()V

    .line 2162
    :goto_3
    return-void

    .line 2094
    :cond_7
    :try_start_1
    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2156
    :catch_0
    move-exception v0

    .line 2157
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "VoiceNoteMainActivity"

    const-string v5, "onServiceConnected error"

    invoke-static {v4, v5, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2158
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 2100
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_8
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    iget-boolean v4, v4, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-eqz v4, :cond_e

    .line 2101
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->isResultRecord:Z
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2102
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->isResultRecord:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1802(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z

    .line 2103
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->doRecord(Z)V

    .line 2111
    :cond_9
    :goto_4
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachModeActivityCreate:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1902(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z

    goto :goto_1

    .line 2104
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v4

    if-eq v4, v9, :cond_b

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v4

    if-ne v4, v8, :cond_c

    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachModeActivityCreate:Z
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1900(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 2106
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v5, 0x1

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBackkeyPress:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2002(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z

    .line 2107
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->saveRecording()Z

    goto :goto_4

    .line 2108
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->isPlaying()Z

    move-result v4

    if-nez v4, :cond_d

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->isPaused()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2109
    :cond_d
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->stopPlay()V

    goto :goto_4

    .line 2112
    :cond_e
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->fromRecordedNumber()Z
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2100(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 2113
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v4

    if-eq v4, v9, :cond_f

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v4

    if-ne v4, v8, :cond_10

    .line 2115
    :cond_f
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v5, 0x1

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mBackkeyPress:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2002(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z

    .line 2116
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->saveRecording()Z

    goto/16 :goto_1

    .line 2117
    :cond_10
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->isPlaying()Z

    move-result v4

    if-nez v4, :cond_11

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->isPaused()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2118
    :cond_11
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->stopPlay()V

    goto/16 :goto_1

    .line 2120
    :cond_12
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    iget-boolean v4, v4, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v4, :cond_15

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->isPlaying()Z

    move-result v4

    if-nez v4, :cond_13

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->isPaused()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 2121
    :cond_13
    const-string v4, "category_text"

    const-wide/16 v6, -0x1

    invoke-static {v4, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;J)V

    .line 2122
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mActivityMode:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$900(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v4

    if-eq v4, v10, :cond_14

    .line 2123
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v5, 0x0

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoListwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V
    invoke-static {v4, v1, v5, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1500(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;ZZ)V

    .line 2124
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->addPlayerforBackgroundPlay()V

    .line 2126
    :cond_14
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_3

    .line 2128
    :cond_15
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_18

    .line 2129
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 2130
    .local v2, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->hasValidNFCfile(Landroid/content/Intent;)J
    invoke-static {v4, v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2200(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/content/Intent;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_16

    .line 2131
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->checkNFCRead(Landroid/content/Intent;)V
    invoke-static {v4, v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2300(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/content/Intent;)V

    .line 2132
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_3

    .line 2134
    :cond_16
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v4

    if-eq v4, v9, :cond_17

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v4

    if-ne v4, v8, :cond_4

    .line 2136
    :cond_17
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v5, 0x0

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoMainwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V
    invoke-static {v4, v1, v5, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2400(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;ZZ)V

    goto/16 :goto_1

    .line 2138
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_18
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v4

    if-eq v4, v9, :cond_19

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->getMediaRecorderState()I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$1400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)I

    move-result v4

    if-ne v4, v8, :cond_4

    .line 2140
    :cond_19
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v5, 0x0

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->gotoMainwithVIAnimation(Landroid/app/FragmentTransaction;ZZ)V
    invoke-static {v4, v1, v5, v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2400(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;ZZ)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 2166
    const-string v0, "VoiceNoteMainActivity"

    const-string v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2167
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$5;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$802(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    .line 2168
    return-void
.end method
