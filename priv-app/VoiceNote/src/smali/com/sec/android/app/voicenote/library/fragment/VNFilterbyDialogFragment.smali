.class public Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNFilterbyDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;


# static fields
.field private static final ADD_REQUEST:I = 0x0

.field private static final RESULT_OK:I = -0x1

.field private static final TAG:Ljava/lang/String; = "VNFilterbyDialogFragment"

.field private static mIsShown:Z


# instance fields
.field public clickListener:Landroid/view/View$OnClickListener;

.field private currentFileId:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mAllItem:Landroid/widget/LinearLayout;

.field private mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field private mCursor:Landroid/database/Cursor;

.field private mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

.field private mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mViewChild:Landroid/view/View;

.field private mtitle:Ljava/lang/String;

.field private posivebtn:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mIsShown:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mViewChild:Landroid/view/View;

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->currentFileId:Ljava/util/ArrayList;

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mtitle:Ljava/lang/String;

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->posivebtn:Landroid/widget/Button;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    .line 316
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment$3;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->clickListener:Landroid/view/View$OnClickListener;

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;)Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    return-object v0
.end method

.method private initView(Landroid/view/LayoutInflater;)V
    .locals 14
    .param p1, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    const-wide/16 v12, -0x1

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 167
    const-string v5, "VNFilterbyDialogFragment"

    const-string v6, "initView"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mViewChild:Landroid/view/View;

    const v6, 0x7f0e0033

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    .line 169
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 170
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mViewChild:Landroid/view/View;

    const v7, 0x7f0e0034

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 172
    const v5, 0x7f030025

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p1, v5, v6, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    .line 174
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 175
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    const v6, 0x7f0e005a

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 176
    .local v4, "title":Landroid/widget/TextView;
    const v5, 0x7f0b0013

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 177
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    const v6, 0x7f0e005b

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 178
    .local v0, "allcount":Landroid/widget/TextView;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "(%d)"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileCountInLibrary(Landroid/content/Context;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    const v6, 0x7f0e005c

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 182
    .local v1, "radio":Landroid/widget/RadioButton;
    const-string v5, "category_text"

    invoke-static {v5, v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getLongSettings(Ljava/lang/String;J)J

    move-result-wide v2

    .line 183
    .local v2, "id":J
    cmp-long v5, v12, v2

    if-nez v5, :cond_0

    .line 184
    invoke-virtual {v1, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 186
    :cond_0
    return-void
.end method

.method public static isShown()Z
    .locals 1

    .prologue
    .line 397
    sget-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mIsShown:Z

    return v0
.end method

.method private listBinding()Z
    .locals 15

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v13, 0x1

    .line 189
    const-string v0, "VNFilterbyDialogFragment"

    const-string v1, "listBinding E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 193
    iput-object v14, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 196
    :cond_0
    new-array v4, v3, [Ljava/lang/String;

    const-string v0, "COLOR"

    aput-object v0, v4, v12

    const-string v0, "TITLE"

    aput-object v0, v4, v13

    const-string v0, "_id"

    aput-object v0, v4, v2

    .line 201
    .local v4, "cols":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 203
    .local v5, "to":[I
    const/4 v0, 0x4

    new-array v5, v0, [I

    .line 204
    const v0, 0x7f0e0059

    aput v0, v5, v12

    .line 205
    const v0, 0x7f0e005a

    aput v0, v5, v13

    .line 206
    const v0, 0x7f0e005b

    aput v0, v5, v2

    .line 207
    const v0, 0x7f0e005c

    aput v0, v5, v3

    .line 210
    const/4 v9, 0x0

    .line 211
    .local v9, "selection":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getListQuery()Ljava/lang/StringBuilder;

    move-result-object v6

    .line 212
    .local v6, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 215
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getDB()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v9, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 230
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_2

    .line 231
    const-string v0, "VNFilterbyDialogFragment"

    const-string v1, "listBinding() : cursor null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v12

    .line 247
    :goto_1
    return v0

    .line 216
    :catch_0
    move-exception v7

    .line 217
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "VNFilterbyDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "listBinding - SQLiteException :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 220
    iput-object v14, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    goto :goto_0

    .line 222
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 223
    .local v8, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v0, "VNFilterbyDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "listBinding - UnsupportedOperationException :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 226
    iput-object v14, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    goto :goto_0

    .line 235
    .end local v8    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_2
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f030025

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, v13}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->selectMode(Z)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, v13}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->setClearAll(Z)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, v12}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->setCategorySelected(Z)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->setFilterbyCallback(Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;)V

    .line 240
    const-string v0, "category_text"

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getLongSettings(Ljava/lang/String;J)J

    move-result-wide v10

    .line 241
    .local v10, "pos":J
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    long-to-int v1, v10

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->selectRadionButton(I)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->getCount()I

    move-result v0

    if-gt v0, v13, :cond_3

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v14}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :goto_2
    move v0, v13

    .line 247
    goto/16 :goto_1

    .line 246
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_2
.end method

.method private selectRadioButton(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x1

    .line 384
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    const v2, 0x7f0e005c

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 385
    .local v0, "radio":Landroid/widget/RadioButton;
    if-nez p1, :cond_0

    .line 386
    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 390
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->setClearAll(Z)V

    .line 391
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->setCategorySelected(Z)V

    .line 392
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->selectRadionButton(I)V

    .line 393
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 394
    return-void

    .line 388
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0
.end method


# virtual methods
.method public RefreshList()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 367
    const-string v0, "VNFilterbyDialogFragment"

    const-string v1, "RefreshList"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    if-nez v0, :cond_0

    .line 369
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->listBinding()Z

    .line 381
    :goto_0
    return-void

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 373
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 375
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getDB()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getListQuery()Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    goto :goto_0
.end method

.method public SetParameter(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mtitle:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public SetParameter(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 1
    .param p2, "title"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->currentFileId:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 85
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mtitle:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 143
    const-string v1, "VNFilterbyDialogFragment"

    const-string v2, "onActivityCreated"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    if-eqz v1, :cond_0

    .line 146
    const-string v1, "listview"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 148
    .local v0, "state":Landroid/os/Parcelable;
    if-eqz v0, :cond_0

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 153
    .end local v0    # "state":Landroid/os/Parcelable;
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 154
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 342
    const-string v2, "VNFilterbyDialogFragment"

    const-string v3, "onActivityResult"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 344
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    if-nez v2, :cond_0

    .line 364
    :goto_0
    return-void

    .line 347
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 348
    packed-switch p1, :pswitch_data_0

    .line 363
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    goto :goto_0

    .line 350
    :pswitch_0
    const/4 v2, -0x1

    if-ne p2, v2, :cond_1

    .line 351
    const-string v2, "color"

    invoke-virtual {p3, v2, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 352
    .local v0, "color":I
    const-string v2, "title"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 353
    .local v1, "title":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->insertColumn(Ljava/lang/String;Ljava/lang/String;)J

    .line 354
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->RefreshList()V

    .line 355
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mViewChild:Landroid/view/View;

    if-nez v2, :cond_1

    .line 356
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->posivebtn:Landroid/widget/Button;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 348
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 90
    const-string v0, "VNFilterbyDialogFragment"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 95
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 96
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 100
    const-string v3, "VNFilterbyDialogFragment"

    const-string v4, "onCreateDialog"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    new-instance v3, Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 103
    if-eqz p1, :cond_0

    .line 104
    const-string v3, "title"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mtitle:Ljava/lang/String;

    .line 107
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 108
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030014

    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mViewChild:Landroid/view/View;

    .line 109
    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->initView(Landroid/view/LayoutInflater;)V

    .line 110
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 111
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0b008b

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 112
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mViewChild:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 113
    const v3, 0x7f0b0008

    invoke-virtual {v0, v3, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 114
    const v3, 0x7f0b0021

    invoke-virtual {v0, v3, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 116
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 117
    .local v1, "dialog":Landroid/app/AlertDialog;
    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 131
    new-instance v3, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->posivebtn:Landroid/widget/Button;

    .line 132
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->posivebtn:Landroid/widget/Button;

    const-string v4, "BUTTON_POSITIVE"

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 134
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mViewChild:Landroid/view/View;

    if-nez v3, :cond_1

    .line 135
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->posivebtn:Landroid/widget/Button;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 137
    :cond_1
    const/4 v3, 0x1

    sput-boolean v3, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mIsShown:Z

    .line 138
    return-object v1
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 252
    const-string v0, "VNFilterbyDialogFragment"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 257
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    if-eqz v0, :cond_1

    .line 260
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 264
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 266
    :cond_2
    sput-boolean v3, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mIsShown:Z

    .line 267
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 268
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "clickedView"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 288
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string v1, "VNFilterbyDialogFragment"

    const-string v2, "onItemClick"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->isResumed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 290
    const-string v1, "VNFilterbyDialogFragment"

    const-string v2, "already dialog is dismissed"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    :goto_0
    return-void

    .line 294
    :cond_0
    const-string v1, "category_text"

    invoke-static {v1, p4, p5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;J)V

    .line 295
    invoke-direct {p0, p3}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->selectRadioButton(I)V

    .line 296
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 297
    .local v0, "data":Landroid/content/Intent;
    const-string v1, "editedType"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    const v2, 0x186a1

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onActivityResult(IILandroid/content/Intent;)V

    .line 300
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;)V

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 272
    const-string v0, "VNFilterbyDialogFragment"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->refreshAllItemCount()V

    .line 276
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->listBinding()Z

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    .line 279
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 284
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 158
    const-string v1, "VNFilterbyDialogFragment"

    const-string v2, "onSaveInstanceState"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-string v1, "id"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->currentFileId:Ljava/util/ArrayList;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 160
    const-string v1, "title"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mtitle:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 162
    .local v0, "state":Landroid/os/Parcelable;
    const-string v1, "listview"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 163
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 164
    return-void
.end method

.method public refreshAllItemCount()V
    .locals 6

    .prologue
    .line 401
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 402
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mAllItem:Landroid/widget/LinearLayout;

    const v2, 0x7f0e005b

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 403
    .local v0, "allcount":Landroid/widget/TextView;
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "(%d)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileCountInLibrary(Landroid/content/Context;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    .end local v0    # "allcount":Landroid/widget/TextView;
    :cond_0
    return-void
.end method
