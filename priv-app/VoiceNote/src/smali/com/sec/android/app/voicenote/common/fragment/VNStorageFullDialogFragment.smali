.class public Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNStorageFullDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 35
    return-void
.end method

.method public static newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;
    .locals 3
    .param p0, "title"    # I
    .param p1, "message"    # I

    .prologue
    .line 16
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;-><init>()V

    .line 17
    .local v1, "dialogFragment":Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 18
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 19
    const-string v2, "message"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 20
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 21
    return-object v1
.end method

.method public static newInstance(ILjava/lang/String;)Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;
    .locals 3
    .param p0, "title"    # I
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 25
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;-><init>()V

    .line 26
    .local v1, "dialogFragment":Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 27
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 28
    const-string v2, "text"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 30
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    const-string v4, "TAG"

    const-string v5, "onCreateDialog Dialog"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "title"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 51
    .local v3, "title":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "message"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 52
    .local v1, "message":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "text"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 53
    .local v2, "text":Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 55
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 56
    if-eqz v2, :cond_0

    .line 57
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 61
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b008f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 72
    const v4, 0x104000a

    new-instance v5, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 80
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4

    .line 59
    :cond_0
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 39
    const-string v0, "TAG"

    const-string v1, "onSaveInstanceState Dialog"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v0, "title"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 41
    const-string v0, "message"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "message"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 42
    const-string v0, "count"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "count"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 43
    const-string v0, "text"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "text"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 45
    return-void
.end method
