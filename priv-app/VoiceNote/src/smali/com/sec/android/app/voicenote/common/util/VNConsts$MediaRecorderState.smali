.class public final Lcom/sec/android/app/voicenote/common/util/VNConsts$MediaRecorderState;
.super Ljava/lang/Object;
.source "VNConsts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/util/VNConsts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MediaRecorderState"
.end annotation


# static fields
.field public static final INITIAL:I = 0x3e8

.field public static final PAUSED:I = 0x3ec

.field public static final PREPARED:I = 0x3e9

.field public static final PRERECORDING:I = 0x3ea

.field public static final RECORDING:I = 0x3eb

.field public static final SAVED:I = 0x3ed


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getStateString(I)Ljava/lang/String;
    .locals 1
    .param p0, "recorderState"    # I

    .prologue
    .line 93
    packed-switch p0, :pswitch_data_0

    .line 107
    const-string v0, "UNDEFINED"

    :goto_0
    return-object v0

    .line 95
    :pswitch_0
    const-string v0, "INITIAL"

    goto :goto_0

    .line 97
    :pswitch_1
    const-string v0, "PRERECORDING"

    goto :goto_0

    .line 99
    :pswitch_2
    const-string v0, "RECORDING"

    goto :goto_0

    .line 101
    :pswitch_3
    const-string v0, "PREPARED"

    goto :goto_0

    .line 103
    :pswitch_4
    const-string v0, "PAUSED"

    goto :goto_0

    .line 105
    :pswitch_5
    const-string v0, "SAVED"

    goto :goto_0

    .line 93
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
