.class Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;
.super Landroid/widget/BaseAdapter;
.source "VNKNOXOperationUiFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContainerListAdapater"
.end annotation


# instance fields
.field mArrList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PersonaInfo;",
            ">;"
        }
    .end annotation
.end field

.field mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PersonaInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "objects":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    const/4 v0, 0x0

    .line 588
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 584
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;->mContext:Landroid/content/Context;

    .line 585
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;->mArrList:Ljava/util/List;

    .line 589
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;->mContext:Landroid/content/Context;

    .line 590
    iput-object p3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;->mArrList:Ljava/util/List;

    .line 591
    return-void
.end method

.method private getContainerIcon(ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "id"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/high16 v7, 0x1050000

    .line 640
    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mIcons:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    .line 641
    .local v0, "bm":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    if-eqz v0, :cond_0

    .line 642
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    .line 656
    :goto_0
    return-object v5

    .line 643
    :cond_0
    if-eqz p2, :cond_1

    .line 644
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 645
    .local v4, "width":I
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 646
    .local v1, "height":I
    invoke-static {p2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 647
    .local v3, "tmpBitmap":Landroid/graphics/Bitmap;
    if-eqz v3, :cond_1

    .line 648
    const/4 v5, 0x1

    invoke-static {v3, v4, v1, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 649
    .local v2, "scaledMainImage":Landroid/graphics/Bitmap;
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 650
    const/4 v3, 0x0

    .line 651
    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mIcons:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Ljava/lang/ref/SoftReference;

    invoke-direct {v7, v2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v5, v2

    .line 652
    goto :goto_0

    .line 656
    .end local v1    # "height":I
    .end local v2    # "scaledMainImage":Landroid/graphics/Bitmap;
    .end local v3    # "tmpBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "width":I
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;->mArrList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/content/pm/PersonaInfo;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 636
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;->mArrList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PersonaInfo;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 583
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;->getItem(I)Landroid/content/pm/PersonaInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 626
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;->mArrList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PersonaInfo;

    invoke-virtual {v0}, Landroid/content/pm/PersonaInfo;->getId()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 596
    if-nez p2, :cond_0

    .line 597
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 598
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f03005a

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 601
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;->mArrList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PersonaInfo;

    .line 602
    .local v2, "item":Landroid/content/pm/PersonaInfo;
    const v4, 0x7f0e00e9

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 603
    .local v0, "icon":Landroid/widget/ImageView;
    const v4, 0x7f0e00ea

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 605
    .local v3, "tv":Landroid/widget/TextView;
    if-eqz v0, :cond_1

    .line 617
    :cond_1
    if-eqz v3, :cond_2

    .line 621
    :cond_2
    return-object p2
.end method
