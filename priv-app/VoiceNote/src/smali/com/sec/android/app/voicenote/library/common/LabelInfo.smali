.class public Lcom/sec/android/app/voicenote/library/common/LabelInfo;
.super Ljava/lang/Object;
.source "LabelInfo.java"


# instance fields
.field private color:I

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/library/common/LabelInfo;->color:I

    .line 5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/LabelInfo;->title:Ljava/lang/String;

    .line 9
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "color"    # I
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/library/common/LabelInfo;->color:I

    .line 5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/LabelInfo;->title:Ljava/lang/String;

    .line 13
    iput p1, p0, Lcom/sec/android/app/voicenote/library/common/LabelInfo;->color:I

    .line 14
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/common/LabelInfo;->title:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public getColor()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/sec/android/app/voicenote/library/common/LabelInfo;->color:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/LabelInfo;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/sec/android/app/voicenote/library/common/LabelInfo;->color:I

    .line 23
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/LabelInfo;->title:Ljava/lang/String;

    .line 31
    return-void
.end method
