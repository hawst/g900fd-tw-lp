.class public Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
.super Ljava/lang/Object;
.source "VNTrimAudioUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;,
        Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;,
        Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;,
        Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;
    }
.end annotation


# static fields
.field public static final HARDWARE_DECODE_LIBRARY:Ljava/lang/String; = "/system/lib/liblifevibes_mediashare_hw_jni.so"

.field public static final INPUT_DIR:Ljava/lang/String;

.field public static final MAX_ENABLE_TRIM_RANGE:J = 0x44bL

.field static final MSG_COMPLETION:I = 0x64

.field public static final MSG_ERROR:I = 0x65

.field static final MSG_WARNING:I = 0x67

.field public static final OUTPUT_DIR:Ljava/lang/String;

.field public static final SDCARD_ROOT:Ljava/lang/String;

.field public static final SOFTWARE_DECODE_LIBRARY:Ljava/lang/String; = "/system/lib/liblifevibes_mediashare_sw_jni.so"

.field private static final TAG:Ljava/lang/String; = "VNTrimAudioUtil"

.field public static final TRIM_CANCELED:I = 0x49

.field public static final TRIM_COMPLETED:I = 0x46

.field public static final TRIM_FAILED:I = 0x48

.field public static final TRIM_PROGRESS:I = 0x3e

.field public static final TRIM_SAVE_AS_NEW_FILE:I = 0x3d

.field public static final TRIM_SAVE_TO_ORIGINAL_FILE:I = 0x3c

.field public static final TRIM_STARTED_RESULT:I = 0x47

.field private static mErrorStr:Ljava/lang/String;

.field private static mLastSavedFileUri:Landroid/net/Uri;

.field public static mLeftPercentage:F

.field private static mLibrary:Ljava/lang/String;

.field public static mRightPercentage:F

.field public static wasTrimming:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEventHandler:Landroid/os/Handler;

.field private final mInputProperties:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;

.field private mIsOverride:Z

.field private mLastTrimEndTime:I

.field private mLastTrimStartTime:I

.field private final mMediaShareApi:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

.field private mSourceFile:Ljava/lang/String;

.field private final mSyncKey:Ljava/lang/Object;

.field private mTrimmedFile:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 62
    sput-object v4, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLibrary:Ljava/lang/String;

    .line 63
    sput-object v4, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mErrorStr:Ljava/lang/String;

    .line 66
    sput-object v4, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mErrorStr:Ljava/lang/String;

    .line 68
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v2, "/system/lib/liblifevibes_mediashare_sw_jni.so"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 69
    .local v1, "library":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 70
    new-instance v1, Ljava/io/File;

    .end local v1    # "library":Ljava/io/File;
    const-string v2, "/system/lib/liblifevibes_mediashare_hw_jni.so"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 71
    .restart local v1    # "library":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-nez v2, :cond_0

    .line 88
    :goto_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->SDCARD_ROOT:Ljava/lang/String;

    .line 89
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->SDCARD_ROOT:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Sounds/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->INPUT_DIR:Ljava/lang/String;

    .line 90
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->SDCARD_ROOT:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Output/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->OUTPUT_DIR:Ljava/lang/String;

    .line 104
    const/4 v2, 0x0

    sput v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLeftPercentage:F

    .line 105
    const/high16 v2, 0x3f800000    # 1.0f

    sput v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mRightPercentage:F

    .line 106
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->wasTrimming:Z

    .line 113
    sput-object v4, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastSavedFileUri:Landroid/net/Uri;

    return-void

    .line 73
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLibrary:Ljava/lang/String;

    .line 74
    sget-object v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLibrary:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mErrorStr:Ljava/lang/String;

    goto :goto_0

    .line 77
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :cond_1
    :try_start_2
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLibrary:Ljava/lang/String;

    .line 78
    sget-object v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLibrary:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 82
    :catch_1
    move-exception v0

    .line 83
    .local v0, "ex":Ljava/lang/ExceptionInInitializerError;
    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mErrorStr:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v1, 0x0

    .line 376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;

    .line 103
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mIsOverride:Z

    .line 110
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSyncKey:Ljava/lang/Object;

    .line 112
    new-instance v0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mInputProperties:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;

    .line 377
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;

    .line 378
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;

    .line 380
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSourceFile:Ljava/lang/String;

    .line 381
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mTrimmedFile:Ljava/lang/String;

    .line 383
    new-instance v0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;-><init>(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mMediaShareApi:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    .line 384
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mMediaShareApi:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSyncKey:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mErrorStr:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1102(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 58
    sput-object p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mErrorStr:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLibrary:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mTrimmedFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mIsOverride:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSourceFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastTrimStartTime:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastTrimEndTime:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->getDestPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastSavedFileUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$902(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Landroid/net/Uri;

    .prologue
    .line 58
    sput-object p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastSavedFileUri:Landroid/net/Uri;

    return-object p0
.end method

.method private static findFileIndex(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)I
    .locals 18
    .param p0, "dir"    # Ljava/lang/String;
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 837
    move-object/from16 v11, p0

    .line 838
    .local v11, "filePath":Ljava/lang/String;
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_data"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "cast(replace(replace(substr(_data, ?, 1000), \'.amr\', \'\'), \'.m4a\', \'\') as INTEGER) idx"

    aput-object v3, v4, v2

    .line 842
    .local v4, "projection":[Ljava/lang/String;
    const-string v5, "_data like ?"

    .line 843
    .local v5, "whereString":Ljava/lang/String;
    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "%d"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v17, v17, 0x2

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v3, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v14, "_%"

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 846
    .local v6, "selectionArgs":[Ljava/lang/String;
    const-string v7, "idx desc limit 1"

    .line 847
    .local v7, "oderBy":Ljava/lang/String;
    const/4 v8, 0x0

    .line 850
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v8

    .line 861
    :goto_0
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 862
    :cond_0
    const/4 v2, 0x1

    .line 883
    :goto_1
    return v2

    .line 853
    :catch_0
    move-exception v9

    .line 854
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "VNTrimAudioUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "findFileIndex - SQLiteException :"

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    const/4 v8, 0x0

    .line 859
    goto :goto_0

    .line 856
    .end local v9    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v10

    .line 857
    .local v10, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v2, "VNTrimAudioUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "findFileIndex - UnsupportedOperationException :"

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    const/4 v8, 0x0

    goto :goto_0

    .line 865
    .end local v10    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_2

    .line 866
    const-string v2, "VNTrimAudioUtil"

    const-string v3, "findFileIndex - cursor count is zero"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 868
    const/4 v8, 0x0

    .line 869
    const/4 v2, 0x1

    goto :goto_1

    .line 872
    :cond_2
    const-wide/16 v12, 0x0

    .line 873
    .local v12, "lastIndex":J
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 875
    :try_start_1
    const-string v2, "idx"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-wide v12

    .line 880
    :goto_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 881
    const/4 v8, 0x0

    .line 883
    const-wide/16 v2, 0x1

    add-long/2addr v2, v12

    long-to-int v2, v2

    goto :goto_1

    .line 876
    :catch_2
    move-exception v9

    .line 877
    .local v9, "e":Ljava/lang/Exception;
    const-string v2, "VNTrimAudioUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "findFileIndex - Exception : "

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private getDestPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 21
    .param p1, "src"    # Ljava/lang/String;

    .prologue
    .line 793
    move-object/from16 v10, p1

    .line 794
    .local v10, "rst":Ljava/lang/String;
    const-string v11, ""

    .line 795
    .local v11, "strDir":Ljava/lang/String;
    const-string v13, ""

    .line 797
    .local v13, "strTitle":Ljava/lang/String;
    const/4 v3, 0x0

    .line 798
    .local v3, "hasDir":Z
    const/4 v9, 0x0

    .line 799
    .local v9, "isNormalFile":Z
    const/4 v8, 0x1

    .line 801
    .local v8, "isFirst":Z
    const-string v15, "/"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 802
    .local v4, "intDirIndex":I
    const-string v15, "_"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    .line 803
    .local v7, "intPrefixIndex":I
    const-string v15, "."

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    .line 805
    .local v5, "intExtIndex":I
    const/4 v15, -0x1

    if-eq v4, v15, :cond_0

    .line 806
    const/4 v3, 0x1

    .line 807
    const/4 v15, 0x0

    add-int/lit8 v16, v4, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 809
    :cond_0
    const/4 v15, -0x1

    if-eq v7, v15, :cond_1

    .line 810
    add-int/lit8 v15, v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 811
    .local v14, "strTmpIndex":Ljava/lang/String;
    const-string v15, "\\d{3,4}"

    invoke-virtual {v14, v15}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_4

    const/4 v8, 0x1

    .line 813
    .end local v14    # "strTmpIndex":Ljava/lang/String;
    :cond_1
    :goto_0
    const/4 v15, -0x1

    if-eq v5, v15, :cond_2

    .line 814
    const/4 v9, 0x1

    .line 817
    :cond_2
    if-eqz v3, :cond_3

    if-eqz v9, :cond_3

    .line 818
    if-eqz v8, :cond_5

    .line 819
    add-int/lit8 v15, v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 825
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;

    invoke-static {v11, v13, v15}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->findFileIndex(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)I

    move-result v6

    .line 827
    .local v6, "intFileIndex":I
    sget-object v15, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v16, "%s%s_%03d%s"

    const/16 v17, 0x4

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    add-int/lit8 v20, v4, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    aput-object v13, v17, v18

    const/16 v18, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v15 .. v17}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 830
    .local v12, "strNewFilePath":Ljava/lang/String;
    const-string v15, "VNTrimAudioUtil"

    const-string v16, "strNewFilePath : "

    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    move-object v10, v12

    .line 833
    return-object v10

    .line 811
    .end local v6    # "intFileIndex":I
    .end local v12    # "strNewFilePath":Ljava/lang/String;
    .restart local v14    # "strTmpIndex":Ljava/lang/String;
    :cond_4
    const/4 v8, 0x0

    goto :goto_0

    .line 821
    .end local v14    # "strTmpIndex":Ljava/lang/String;
    :cond_5
    add-int/lit8 v15, v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    goto :goto_1
.end method

.method public static getLastSavedFileUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 887
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastSavedFileUri:Landroid/net/Uri;

    return-object v0
.end method


# virtual methods
.method public CancelTrimming()V
    .locals 6

    .prologue
    const/16 v5, 0x49

    const/4 v4, 0x0

    .line 360
    const-string v2, "VNTrimAudioUtil"

    const-string v3, "CancelTrimming"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mMediaShareApi:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->releaseTrimming()Z

    .line 365
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mTrimmedFile:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 367
    .local v1, "trimmedFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 372
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 373
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;

    invoke-static {v2, v5, v4, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 374
    .end local v1    # "trimmedFile":Ljava/io/File;
    :goto_0
    return-void

    .line 368
    :catch_0
    move-exception v0

    .line 369
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public preCheckTrim(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "srcPath"    # Ljava/lang/String;

    .prologue
    .line 387
    const-string v1, "VNTrimAudioUtil"

    const-string v2, "preCheckTrim()"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    const-string v1, "/system/lib/liblifevibes_mediashare_sw_jni.so"

    sget-object v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLibrary:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 390
    const-string v1, "VNTrimAudioUtil"

    const-string v2, "trim SOFTWARE_DECODE_LIBRARY"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mMediaShareApi:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mInputProperties:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;

    invoke-virtual {v1, p2, v2}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->getProperties(Ljava/lang/String;Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;)I

    move-result v0

    .line 396
    .local v0, "result":I
    packed-switch v0, :pswitch_data_0

    .line 406
    const v1, 0x7f0b0021

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 410
    :pswitch_0
    return v0

    .line 391
    .end local v0    # "result":I
    :cond_1
    const-string v1, "/system/lib/liblifevibes_mediashare_hw_jni.so"

    sget-object v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLibrary:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 392
    const-string v1, "VNTrimAudioUtil"

    const-string v2, "trim HARDWARE_DECODE_LIBRARY"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 396
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public startTrimming(Ljava/lang/String;ZIILcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;)V
    .locals 10
    .param p1, "src"    # Ljava/lang/String;
    .param p2, "override"    # Z
    .param p3, "startTrim"    # I
    .param p4, "endTrim"    # I
    .param p5, "trimmingListener"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;

    .prologue
    const/16 v9, 0x47

    .line 329
    const-string v0, "VNTrimAudioUtil"

    const-string v1, "startTrimming"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSourceFile:Ljava/lang/String;

    .line 331
    iput-boolean p2, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mIsOverride:Z

    .line 332
    iput p3, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastTrimStartTime:I

    .line 333
    iput p4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLastTrimEndTime:I

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSourceFile:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 336
    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSourceFile:Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 337
    .local v6, "builder":Ljava/lang/StringBuilder;
    const-string v0, "."

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    const-string v1, "Trimmed"

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mTrimmedFile:Ljava/lang/String;

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mMediaShareApi:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSourceFile:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mInputProperties:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->getProperties(Ljava/lang/String;Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;)I

    move-result v7

    .line 343
    .local v7, "propertiesResult":I
    move v8, v7

    .line 344
    .local v8, "result":I
    if-nez v7, :cond_0

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mMediaShareApi:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSourceFile:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mTrimmedFile:Ljava/lang/String;

    new-instance v5, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;

    invoke-direct {v5, p0}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$TrimmingListener;-><init>(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)V

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->startTrimming(Ljava/lang/String;Ljava/lang/String;IILcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;)I

    move-result v8

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v9}, Landroid/os/Handler;->removeMessages(I)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mEventHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-static {v0, v9, v8, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;

    const-string v1, "TRIM"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 357
    .end local v6    # "builder":Ljava/lang/StringBuilder;
    .end local v7    # "propertiesResult":I
    .end local v8    # "result":I
    :cond_1
    :goto_0
    return-void

    .line 355
    :cond_2
    const-string v0, "VNTrimAudioUtil"

    const-string v1, "mSourceFile null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
