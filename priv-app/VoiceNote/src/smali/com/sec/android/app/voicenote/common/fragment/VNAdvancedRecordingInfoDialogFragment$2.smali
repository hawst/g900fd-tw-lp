.class Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$2;
.super Ljava/lang/Object;
.source "VNAdvancedRecordingInfoDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I

    .prologue
    const/4 v3, 0x1

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->mCheckbox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->mRecord_mode:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 150
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->mRecord_mode:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;)I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    .line 151
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 152
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->needDataCheckHelpDialog(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 153
    .local v1, "needDataCheck":Ljava/lang/String;
    sget-boolean v2, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_DATA_CHECK_POPUP:Z

    if-eqz v2, :cond_3

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 154
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->newInstance(Ljava/lang/String;)Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 160
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "needDataCheck":Ljava/lang/String;
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->dismissAllowingStateLoss()V

    .line 163
    :cond_2
    return-void

    .line 135
    :pswitch_0
    const-string v2, "show_interview_info"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    goto :goto_0

    .line 139
    :pswitch_1
    const-string v2, "show_conversation_info"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    goto :goto_0

    .line 143
    :pswitch_2
    const-string v2, "show_stt_recc_info"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    goto :goto_0

    .line 146
    :pswitch_3
    const-string v2, "show_stt_recc_info2"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    goto :goto_0

    .line 156
    .restart local v0    # "context":Landroid/content/Context;
    .restart local v1    # "needDataCheck":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    if-eqz v2, :cond_1

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->startBind()V

    goto :goto_1

    .line 133
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
