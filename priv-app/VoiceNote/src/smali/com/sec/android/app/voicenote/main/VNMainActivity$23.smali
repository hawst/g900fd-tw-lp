.class Lcom/sec/android/app/voicenote/main/VNMainActivity$23;
.super Ljava/lang/Object;
.source "VNMainActivity.java"

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;->displayToolbarHelp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 5139
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$23;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "hpw"    # Landroid/widget/HoverPopupWindow;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 5142
    const-string v8, "skip_interval"

    invoke-static {v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 5168
    :cond_0
    :goto_0
    return v6

    .line 5145
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$23;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 5146
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 5149
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEnableAirViewInforPreview(Landroid/content/ContentResolver;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 5150
    if-eqz p2, :cond_0

    .line 5151
    iget-object v8, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$23;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFileTitle(I)Ljava/lang/String;

    move-result-object v4

    .line 5152
    .local v4, "mPreSelectedTitle":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$23;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v8, v7}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFileTitle(I)Ljava/lang/String;

    move-result-object v3

    .line 5153
    .local v3, "mNextSelectedTitle":Ljava/lang/String;
    const/4 v5, 0x0

    .line 5154
    .local v5, "v":Landroid/widget/TextView;
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 5155
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v8, 0x7f03001f

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .end local v5    # "v":Landroid/widget/TextView;
    check-cast v5, Landroid/widget/TextView;

    .line 5157
    .restart local v5    # "v":Landroid/widget/TextView;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 5158
    .local v1, "id":I
    const v8, 0x7f0e0053

    if-ne v1, v8, :cond_3

    .line 5159
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5163
    :cond_2
    :goto_1
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 5164
    invoke-virtual {p2, v5}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    move v6, v7

    .line 5165
    goto :goto_0

    .line 5160
    :cond_3
    const v8, 0x7f0e0055

    if-ne v1, v8, :cond_2

    .line 5161
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
