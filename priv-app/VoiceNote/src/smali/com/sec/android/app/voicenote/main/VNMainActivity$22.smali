.class Lcom/sec/android/app/voicenote/main/VNMainActivity$22;
.super Ljava/lang/Object;
.source "VNMainActivity.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;->displayToolbarHelp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 5127
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$22;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 5130
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 5131
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 5132
    .local v0, "event":I
    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    .line 5133
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$22;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$22;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const-string v3, "audio"

    invoke-virtual {v1, v3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    invoke-static {v2, v1, p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hoverHapticAndFeedback(Landroid/content/Context;Landroid/media/AudioManager;Landroid/view/View;)V

    .line 5135
    :cond_0
    const/4 v1, 0x0

    return v1
.end method
