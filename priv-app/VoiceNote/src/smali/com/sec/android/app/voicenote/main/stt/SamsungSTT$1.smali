.class final Lcom/sec/android/app/voicenote/main/stt/SamsungSTT$1;
.super Ljava/lang/Object;
.source "SamsungSTT.java"

# interfaces
.implements Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->startRecording(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEndOfSpeech()V
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 266
    const-string v0, "SamsungSTT"

    const-string v3, "   --- onEndOfSpeech"

    invoke-static {v0, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizer:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$000()Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->getRecentTimeStamp()J

    move-result-wide v10

    .line 269
    .local v10, "lRecentTimeStamp":J
    cmp-long v0, v10, v4

    if-ltz v0, :cond_0

    .line 270
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizerSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$100()Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    move-result-object v0

    invoke-virtual {v0, v10, v11}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->adjustBuffer(J)J

    move-result-wide v12

    .line 272
    .local v12, "newtime":J
    cmp-long v0, v12, v4

    if-eqz v0, :cond_0

    .line 273
    move-wide v10, v12

    .line 277
    .end local v12    # "newtime":J
    :cond_0
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizer:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$000()Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->processResult()V

    .line 279
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$200()Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;->disconnectAudioSource()Lcom/nuance/dragon/toolkit/audio/AudioSource;

    .line 280
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$200()Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;->release()V

    .line 281
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_endPointer:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$300()Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;->disconnectAudioSource()Lcom/nuance/dragon/toolkit/audio/AudioSource;

    .line 284
    new-instance v0, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    const/16 v3, 0x32

    const/16 v4, 0xf

    const/4 v5, 0x3

    const/16 v7, 0x1e

    const/16 v9, 0x23

    move v6, v2

    move v8, v1

    invoke-direct/range {v0 .. v9}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;-><init>(IIIIIIIII)V

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$202(Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;)Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    .line 294
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$200()Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    move-result-object v0

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_epdSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$400()Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 296
    new-instance v0, Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_detectListener:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$500()Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;-><init>(Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;)V

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_endPointer:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$302(Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;)Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    .line 297
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_endPointer:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$300()Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    move-result-object v0

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_speexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$200()Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 299
    new-instance v3, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$600()Landroid/content/Context;

    move-result-object v4

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$700()Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    move-result-object v5

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizerSource:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$100()Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    move-result-object v8

    move-wide v6, v10

    move v9, v2

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;-><init>(Landroid/content/Context;Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;JLcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;Z)V

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizer:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$002(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    .line 300
    const-string v0, "SamsungSTT"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "=== SamsungRecognizer created ==="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rec"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$800()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecDuration()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    return-void
.end method

.method public onStartOfSpeech()V
    .locals 2

    .prologue
    .line 258
    const-string v0, "SamsungSTT"

    const-string v1, "onStartOfSpeech"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizer:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$000()Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 260
    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->_recognizer:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->access$000()Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->startRecognition()V

    .line 262
    :cond_0
    return-void
.end method
