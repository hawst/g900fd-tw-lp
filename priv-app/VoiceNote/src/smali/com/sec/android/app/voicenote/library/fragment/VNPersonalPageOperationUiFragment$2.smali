.class Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$2;
.super Ljava/lang/Object;
.source "VNPersonalPageOperationUiFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->createProgressDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPersonalPageOperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPersonalPageOperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 196
    const-string v0, "VNPersonalPageOperationUiFragment"

    const-string v1, "Negative button : cancel runned task"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPersonalPageOperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->cancel(Z)Z

    .line 200
    :cond_0
    return-void
.end method
