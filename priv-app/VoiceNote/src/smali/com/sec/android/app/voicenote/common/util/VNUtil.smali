.class public Lcom/sec/android/app/voicenote/common/util/VNUtil;
.super Ljava/lang/Object;
.source "VNUtil.java"


# static fields
.field private static final ENC_NUM:I = 0x3e

.field private static final ENC_START:C = 'A'

.field private static final EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

.field public static final FILE_ERROR:I = -0x1

.field public static final FILE_EXIST:I = 0x0

.field public static final FILE_RENAMED:I = 0x1

.field public static final FLAG_SUPPORT_PINYIN:Z = false

.field public static FUNC_RETURN_CORRECT:Z = false

.field public static FUNC_RETURN_INCORRECT:Z = false

.field private static final GBYTES:D = 1.073741824E9

.field private static final INVALID_POINTER_ID:I = -0x1

.field private static final JELLY_BEAN_MR2:I = 0x12

.field private static final KBYTES:D = 1024.0

.field private static final LOCKSCREEN_TAG:Ljava/lang/String; = "LockScreen_VNUtil"

.field public static final LOW_STORAGE_SAFTY_THRESHOLD:J = 0x1400000L

.field public static final LOW_STORAGE_THRESHOLD:J = 0x1457800L

.field private static final M4A:Ljava/lang/String; = ".m4a"

.field private static final MAXDELETEITEM:I = 0x64

.field private static final MBYTES:D = 1048576.0

.field private static final SPD:Ljava/lang/String; = "spd"

.field private static final TAG:Ljava/lang/String; = "VNUtil"

.field public static avaliableSize:J

.field private static fixedName:Ljava/lang/String;

.field private static isNameFixed:Z

.field private static mActivePointerId:I

.field private static mCurrentSaveId:J

.field private static mDoFinishWhenClickItem:Z

.field public static mExternalStorage:Ljava/lang/String;

.field public static mFileName:Ljava/lang/String;

.field public static mKNOXCallingUserId:I

.field private static mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

.field private static mRecordedNumber:Ljava/lang/String;

.field public static mSdSlotState:I

.field public static mVersionInfo:Ljava/lang/String;

.field private static mWakeLock:Landroid/os/PowerManager$WakeLock;

.field public static sCscFeature:Lcom/sec/android/app/CscFeature;

.field private static toast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 121
    sput-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    .line 125
    sput-wide v4, Lcom/sec/android/app/voicenote/common/util/VNUtil;->avaliableSize:J

    .line 126
    sput-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mDoFinishWhenClickItem:Z

    .line 127
    sput-wide v4, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mCurrentSaveId:J

    .line 129
    sput-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->FUNC_RETURN_CORRECT:Z

    .line 130
    sput-boolean v2, Lcom/sec/android/app/voicenote/common/util/VNUtil;->FUNC_RETURN_INCORRECT:Z

    .line 132
    sput-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mExternalStorage:Ljava/lang/String;

    .line 133
    sput v2, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mSdSlotState:I

    .line 134
    sput-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mFileName:Ljava/lang/String;

    .line 136
    sput v2, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mKNOXCallingUserId:I

    .line 138
    sput-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mVersionInfo:Ljava/lang/String;

    .line 150
    sput-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 151
    sput-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 153
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

    .line 156
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->sCscFeature:Lcom/sec/android/app/CscFeature;

    .line 161
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mActivePointerId:I

    .line 165
    sput-boolean v2, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isNameFixed:Z

    .line 168
    sput-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mRecordedNumber:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static DeleteTagsData(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 2496
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2497
    .local v1, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2498
    const-string v4, "_data"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2500
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2501
    .local v3, "where":Ljava/lang/String;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2502
    .local v2, "v":Landroid/content/ContentValues;
    const-string v4, "year_name"

    const-string v5, ""

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2504
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v2, v3, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 2517
    .end local v1    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v2    # "v":Landroid/content/ContentValues;
    .end local v3    # "where":Ljava/lang/String;
    :goto_0
    return v7

    .line 2505
    :catch_0
    move-exception v0

    .line 2506
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 2507
    const-string v4, "VNUtil"

    const-string v5, "error occurred while extractMetadata"

    invoke-static {v4, v5, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2509
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 2510
    .local v0, "e":Landroid/database/sqlite/SQLiteConstraintException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    goto :goto_0

    .line 2512
    .end local v0    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    :catch_2
    move-exception v0

    .line 2513
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2514
    const-string v4, "VNUtil"

    const-string v5, "error occurred while input data to MediaStore"

    invoke-static {v4, v5, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static SaveBitmapToFileCache(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 6
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 1036
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1037
    .local v1, "fileCacheItem":Ljava/io/File;
    const/4 v2, 0x0

    .line 1039
    .local v2, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 1040
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1041
    .end local v2    # "out":Ljava/io/OutputStream;
    .local v3, "out":Ljava/io/OutputStream;
    :try_start_1
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {p0, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1046
    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 1051
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    :goto_0
    return-void

    .line 1047
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 1048
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v2, v3

    .line 1050
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_0

    .line 1042
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1043
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1046
    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1047
    :catch_2
    move-exception v0

    .line 1048
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1045
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 1046
    :goto_2
    :try_start_5
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1049
    :goto_3
    throw v4

    .line 1047
    :catch_3
    move-exception v0

    .line 1048
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1045
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_2

    .line 1042
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_1
.end method

.method public static checkAlmostFull(ZIJI)J
    .locals 14
    .param p0, "internal"    # Z
    .param p1, "warningSize"    # I
    .param p2, "realFileSize"    # J
    .param p4, "preSize"    # I

    .prologue
    .line 485
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getAvailableStorage(Z)J

    move-result-wide v2

    .line 489
    .local v2, "available":J
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getBlockSize(Z)I

    move-result v4

    .line 491
    .local v4, "blockSize":I
    if-nez v4, :cond_0

    .line 492
    const-string v5, "VNUtil"

    const-string v10, "blockSize is zero"

    invoke-static {v5, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    const-wide/32 v10, -0x1457800

    .line 516
    :goto_0
    return-wide v10

    .line 496
    :cond_0
    move/from16 v0, p4

    int-to-long v10, v0

    cmp-long v5, v10, p2

    if-gez v5, :cond_2

    .line 497
    const-wide/16 v6, 0x0

    .line 502
    .local v6, "gap":J
    :goto_1
    int-to-long v10, v4

    rem-long v10, p2, v10

    const-wide/16 v12, 0x0

    cmp-long v5, v10, v12

    if-lez v5, :cond_3

    .line 503
    int-to-long v10, v4

    int-to-long v12, v4

    rem-long v12, p2, v12

    sub-long/2addr v10, v12

    add-long v8, v2, v10

    .line 508
    .local v8, "realFreeSize":J
    :goto_2
    const-wide/32 v10, 0x1400000

    add-long/2addr v10, v6

    sub-long/2addr v8, v10

    .line 510
    int-to-long v10, p1

    cmp-long v5, v8, v10

    if-gez v5, :cond_1

    .line 511
    const-string v5, "VNUtil"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkAlmostFull available:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " realFree:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " real:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-wide/from16 v0, p2

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " pre:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, p4

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v5, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    :cond_1
    int-to-long v10, p1

    sub-long v10, v8, v10

    const-wide/16 v12, 0x0

    cmp-long v5, v10, v12

    if-gez v5, :cond_4

    .line 514
    const-wide/16 v10, 0x0

    goto :goto_0

    .line 499
    .end local v6    # "gap":J
    .end local v8    # "realFreeSize":J
    :cond_2
    move/from16 v0, p4

    int-to-long v10, v0

    sub-long v6, v10, p2

    .restart local v6    # "gap":J
    goto :goto_1

    .line 505
    :cond_3
    move-wide v8, v2

    .restart local v8    # "realFreeSize":J
    goto :goto_2

    .line 516
    :cond_4
    int-to-long v10, p1

    sub-long v10, v8, v10

    goto :goto_0
.end method

.method public static checkAvailableStorage(Z)Z
    .locals 4
    .param p0, "internal"    # Z

    .prologue
    .line 421
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getAvailableStorage(Z)J

    move-result-wide v0

    const-wide/32 v2, 0x1457800

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 422
    const/4 v0, 0x0

    .line 424
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static checkMediaScannerRunning(Landroid/content/ContentResolver;)Z
    .locals 10
    .param p0, "cr"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 929
    const-string v0, "VNUtil"

    const-string v1, "checkMediaScannerRunning()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    const/4 v7, 0x0

    .line 932
    .local v7, "result":Z
    invoke-static {}, Landroid/provider/MediaStore;->getMediaScannerUri()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "volume"

    aput-object v0, v2, v8

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 936
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 937
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v9, :cond_1

    .line 938
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 939
    const-string v0, "external"

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "internal"

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v7, v9

    .line 943
    :cond_1
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 946
    :cond_2
    return v7

    :cond_3
    move v7, v8

    .line 939
    goto :goto_0
.end method

.method public static checkSTT(Landroid/content/Context;J)Ljava/lang/String;
    .locals 17
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    .line 2005
    const-string v13, "VNUtil"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "[HIYA]checkSTT : start id = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-wide/from16 v0, p1

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2007
    const/4 v6, 0x0

    .line 2008
    .local v6, "hasSttd":Z
    const/4 v10, 0x0

    .line 2010
    .local v10, "mSttData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    invoke-static/range {p0 .. p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v11

    .line 2011
    .local v11, "path":Ljava/lang/String;
    invoke-static/range {p0 .. p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileName(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 2013
    .local v5, "filename":Ljava/lang/String;
    if-eqz v11, :cond_0

    if-nez v5, :cond_1

    .line 2014
    :cond_0
    const/4 v9, 0x0

    .line 2050
    :goto_0
    return-object v9

    .line 2017
    :cond_1
    const/4 v13, 0x0

    const/16 v14, 0x2f

    invoke-virtual {v11, v14}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v14

    invoke-virtual {v11, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 2018
    .local v12, "pathOnly":Ljava/lang/String;
    const/4 v9, 0x0

    .line 2020
    .local v9, "mFileSavePath":Ljava/lang/String;
    const/4 v3, 0x0

    .line 2022
    .local v3, "file":Ljava/io/File;
    const-string v13, "VNUtil"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "[HIYA]path : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2023
    const-string v13, "VNUtil"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "[HIYA]filename : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2025
    if-eqz v11, :cond_4

    invoke-static {v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isM4A(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 2026
    sget-object v14, Lcom/sec/android/app/voicenote/common/util/M4aConsts;->FILE_LOCK:Ljava/lang/Object;

    monitor-enter v14

    .line 2027
    :try_start_0
    new-instance v7, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    invoke-direct {v7, v11}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 2028
    .local v7, "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v8

    .line 2029
    .local v8, "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    if-nez v8, :cond_2

    const/4 v13, 0x0

    monitor-exit v14

    move-object v9, v13

    goto :goto_0

    .line 2031
    :cond_2
    iget-boolean v6, v8, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z

    .line 2033
    const-string v13, "VNUtil"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "[HIYA]hasSttd : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v15}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2035
    if-eqz v6, :cond_3

    .line 2036
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/16 v15, 0x2f

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "_memo.txt"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2038
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2039
    .end local v3    # "file":Ljava/io/File;
    .local v4, "file":Ljava/io/File;
    :try_start_1
    const-string v13, "VNUtil"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "[HIYA]savePath1 : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v15}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2041
    new-instance v2, Lcom/sec/android/app/voicenote/common/util/SttdHelper;

    move-object/from16 v0, p0

    invoke-direct {v2, v8, v0}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;Landroid/content/Context;)V

    .line 2042
    .local v2, "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->readSttd()Ljava/io/Serializable;

    move-result-object v13

    move-object v0, v13

    check-cast v0, Ljava/util/ArrayList;

    move-object v10, v0

    .line 2043
    invoke-static {v4, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->makeSTTTextFile(Ljava/io/File;Ljava/util/ArrayList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v3, v4

    .line 2045
    .end local v2    # "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    .end local v4    # "file":Ljava/io/File;
    .restart local v3    # "file":Ljava/io/File;
    :cond_3
    :try_start_2
    monitor-exit v14
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2048
    .end local v7    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v8    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :cond_4
    const-string v13, "VNUtil"

    const-string v14, "[HIYA]checkSTT : end"

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2045
    :catchall_0
    move-exception v13

    :goto_1
    :try_start_3
    monitor-exit v14
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v13

    .end local v3    # "file":Ljava/io/File;
    .restart local v4    # "file":Ljava/io/File;
    .restart local v7    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .restart local v8    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :catchall_1
    move-exception v13

    move-object v3, v4

    .end local v4    # "file":Ljava/io/File;
    .restart local v3    # "file":Ljava/io/File;
    goto :goto_1
.end method

.method public static checkSecretBox(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1935
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1937
    .local v0, "secretBoxDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1938
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 1940
    :cond_0
    return-void
.end method

.method public static convertDPtoPX(Landroid/content/Context;FLandroid/util/DisplayMetrics;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dp"    # F
    .param p2, "displayMetrics"    # Landroid/util/DisplayMetrics;

    .prologue
    .line 2393
    if-nez p2, :cond_0

    .line 2394
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    .line 2396
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0, p1, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static copyFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 18
    .param p0, "fromPath"    # Ljava/lang/String;
    .param p1, "toPath"    # Ljava/lang/String;

    .prologue
    .line 950
    const/4 v2, 0x0

    .line 951
    .local v2, "copied":Z
    invoke-virtual/range {p0 .. p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 952
    const/4 v4, 0x1

    .line 1032
    :goto_0
    return v4

    .line 954
    :cond_0
    new-instance v11, Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 956
    .local v11, "fOrg":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 957
    const-string v4, "VNUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "file is not exists : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v2

    .line 958
    goto :goto_0

    .line 961
    :cond_1
    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 962
    const-string v4, "VNUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "path is directory : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v2

    .line 963
    goto :goto_0

    .line 965
    :cond_2
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 967
    .local v10, "fDest":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 968
    const-string v4, "VNUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "file is exists already : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v2

    .line 969
    goto :goto_0

    .line 972
    :cond_3
    const/4 v12, 0x0

    .line 973
    .local v12, "fis":Ljava/io/FileInputStream;
    const/4 v14, 0x0

    .line 975
    .local v14, "fos":Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    .line 976
    .local v3, "inChannel":Ljava/nio/channels/FileChannel;
    const/4 v8, 0x0

    .line 978
    .local v8, "outChannel":Ljava/nio/channels/FileChannel;
    :try_start_0
    new-instance v13, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/nio/channels/NonReadableChannelException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/nio/channels/NonWritableChannelException; {:try_start_0 .. :try_end_0} :catch_4

    .line 979
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .local v13, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v15, Ljava/io/FileOutputStream;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_11
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/nio/channels/NonReadableChannelException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/nio/channels/NonWritableChannelException; {:try_start_1 .. :try_end_1} :catch_9

    .line 981
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .local v15, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {v13}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    .line 982
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v8

    .line 984
    const-wide/16 v4, 0x0

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    invoke-virtual/range {v3 .. v8}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_12
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_10
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_e
    .catch Ljava/nio/channels/NonReadableChannelException; {:try_start_2 .. :try_end_2} :catch_c
    .catch Ljava/nio/channels/NonWritableChannelException; {:try_start_2 .. :try_end_2} :catch_a

    move-result-wide v16

    .line 985
    .local v16, "result":J
    const-wide/16 v4, 0x0

    cmp-long v4, v16, v4

    if-lez v4, :cond_8

    const/4 v2, 0x1

    :goto_1
    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    move-object v12, v13

    .line 998
    .end local v13    # "fis":Ljava/io/FileInputStream;
    .end local v16    # "result":J
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    :goto_2
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1000
    :try_start_3
    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    .line 1001
    const/4 v8, 0x0

    .line 1006
    :cond_4
    :goto_3
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1008
    :try_start_4
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    .line 1009
    const/4 v3, 0x0

    .line 1015
    :cond_5
    :goto_4
    if-eqz v14, :cond_6

    .line 1017
    :try_start_5
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7

    .line 1018
    const/4 v14, 0x0

    .line 1023
    :cond_6
    :goto_5
    if-eqz v12, :cond_7

    .line 1025
    :try_start_6
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8

    .line 1026
    const/4 v12, 0x0

    :cond_7
    :goto_6
    move v4, v2

    .line 1032
    goto/16 :goto_0

    .line 985
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v16    # "result":J
    :cond_8
    const/4 v2, 0x0

    goto :goto_1

    .line 986
    .end local v13    # "fis":Ljava/io/FileInputStream;
    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .end local v16    # "result":J
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v9

    .line 987
    .local v9, "e":Ljava/io/FileNotFoundException;
    :goto_7
    invoke-virtual {v9}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 988
    .end local v9    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v9

    .line 989
    .local v9, "e":Ljava/io/IOException;
    :goto_8
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 990
    .end local v9    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v9

    .line 991
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :goto_9
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 992
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v9

    .line 993
    .local v9, "e":Ljava/nio/channels/NonReadableChannelException;
    :goto_a
    invoke-virtual {v9}, Ljava/nio/channels/NonReadableChannelException;->printStackTrace()V

    goto :goto_2

    .line 994
    .end local v9    # "e":Ljava/nio/channels/NonReadableChannelException;
    :catch_4
    move-exception v9

    .line 995
    .local v9, "e":Ljava/nio/channels/NonWritableChannelException;
    :goto_b
    invoke-virtual {v9}, Ljava/nio/channels/NonWritableChannelException;->printStackTrace()V

    goto :goto_2

    .line 1002
    .end local v9    # "e":Ljava/nio/channels/NonWritableChannelException;
    :catch_5
    move-exception v9

    .line 1003
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1010
    .end local v9    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v9

    .line 1011
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1019
    .end local v9    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v9

    .line 1020
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 1027
    .end local v9    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v9

    .line 1028
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 994
    .end local v9    # "e":Ljava/io/IOException;
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    :catch_9
    move-exception v9

    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_b

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catch_a
    move-exception v9

    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_b

    .line 992
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    :catch_b
    move-exception v9

    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_a

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catch_c
    move-exception v9

    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_a

    .line 990
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    :catch_d
    move-exception v9

    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_9

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catch_e
    move-exception v9

    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_9

    .line 988
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    :catch_f
    move-exception v9

    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_8

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catch_10
    move-exception v9

    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_8

    .line 986
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    :catch_11
    move-exception v9

    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_7

    .end local v12    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catch_12
    move-exception v9

    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_7
.end method

.method public static createNewFileName(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 349
    sget-boolean v3, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isNameFixed:Z

    if-eqz v3, :cond_0

    const-string v3, "contextual_filename"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 350
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNUtil;->fixedName:Ljava/lang/String;

    .line 363
    :goto_0
    return-object v2

    .line 352
    :cond_0
    const/4 v2, 0x0

    .line 353
    .local v2, "prefix":Ljava/lang/String;
    const-string v3, "contextual_filename"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 354
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0162

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 355
    goto :goto_0

    .line 357
    :cond_1
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getDefaultFileName()Ljava/lang/String;

    move-result-object v2

    .line 360
    invoke-static {v2, p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->findFileIndex(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    .line 361
    .local v0, "indexInt":I
    invoke-static {v2, v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->findFileName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .local v1, "newFileName":Ljava/lang/String;
    move-object v2, v1

    .line 363
    goto :goto_0
.end method

.method public static deleteFile(Landroid/content/Context;JLjava/lang/String;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1089
    if-nez p3, :cond_1

    .line 1116
    :cond_0
    :goto_0
    return v5

    .line 1093
    :cond_1
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1094
    .local v3, "tempFile":Ljava/io/File;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExistFile(Ljava/io/File;)Z

    move-result v7

    sget-boolean v8, Lcom/sec/android/app/voicenote/common/util/VNUtil;->FUNC_RETURN_CORRECT:Z

    if-ne v7, v8, :cond_2

    .line 1095
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 1096
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getCacheLoader()Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    move-result-object v0

    .line 1097
    .local v0, "cacheLoader":Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;
    if-eqz v0, :cond_2

    .line 1098
    invoke-virtual {v0, p3}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->removePathFromCache(Ljava/lang/String;)V

    .line 1101
    .end local v0    # "cacheLoader":Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1102
    .local v2, "strBuilder":Ljava/lang/StringBuilder;
    const-string v7, "_id"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1103
    const/16 v7, 0x3d

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1104
    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1105
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1106
    .local v4, "where":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v4, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1107
    .local v1, "result":I
    if-lez v1, :cond_0

    .line 1109
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->isGateEnabled()Z

    move-result v5

    if-ne v5, v6, :cond_3

    .line 1110
    const-string v5, "GATE"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<GATE-M> AUDIO_DELETED: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " </GATE-M>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->noI(Ljava/lang/String;Ljava/lang/String;)V

    .line 1113
    :cond_3
    invoke-static {p0, p3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->sendFileDeleteBroadcast(Landroid/content/Context;Ljava/lang/String;)V

    move v5, v6

    .line 1114
    goto :goto_0
.end method

.method public static deletefilesSet(Landroid/content/Context;Ljava/util/ArrayList;Landroid/os/Handler;)Ljava/util/ArrayList;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/os/Handler;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1147
    .local p1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1148
    .local v2, "deletedIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 1151
    .local v8, "sizeIDs":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    const/16 v13, 0x64

    if-ge v4, v13, :cond_2

    if-ge v4, v8, :cond_2

    .line 1152
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Long;

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 1153
    .local v6, "id":J
    move-object/from16 v0, p0

    invoke-static {v0, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 1155
    .local v5, "path":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 1156
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1157
    .local v10, "tempFile":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 1158
    move-object v3, v5

    .line 1159
    .local v3, "fPath":Ljava/lang/String;
    new-instance v13, Lcom/sec/android/app/voicenote/common/util/VNUtil$3;

    invoke-direct {v13, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil$3;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1168
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1170
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->isGateEnabled()Z

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_0

    .line 1171
    const-string v13, "GATE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "<GATE-M> AUDIO_DELETED: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " </GATE-M>"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->noI(Ljava/lang/String;Ljava/lang/String;)V

    .line 1174
    :cond_0
    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->sendFileDeleteBroadcast(Landroid/content/Context;Ljava/lang/String;)V

    .line 1151
    .end local v3    # "fPath":Ljava/lang/String;
    .end local v10    # "tempFile":Ljava/io/File;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1178
    .end local v5    # "path":Ljava/lang/String;
    .end local v6    # "id":J
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lez v13, :cond_3

    .line 1179
    const-string v13, "VNUtil"

    const-string v14, "deletedIDs is"

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    invoke-virtual {v2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v13

    const-string v14, ", "

    const-string v15, " or _id="

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "["

    const-string v15, "_id="

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "]"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    .line 1184
    .local v12, "where_ids":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 1185
    .local v9, "strBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1186
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1188
    .local v11, "where":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    sget-object v14, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v11, v15}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1194
    .end local v9    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v11    # "where":Ljava/lang/String;
    .end local v12    # "where_ids":Ljava/lang/String;
    :goto_1
    return-object v2

    .line 1191
    :cond_3
    const-string v13, "VNUtil"

    const-string v14, "deletedIDs is nothing"

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z
    .locals 5
    .param p0, "fm"    # Landroid/app/FragmentManager;
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 2792
    if-nez p0, :cond_0

    .line 2793
    const-string v3, "VNUtil"

    const-string v4, "dismissDialogFragment() fragment manager is null"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 2807
    :goto_0
    return v2

    .line 2796
    :cond_0
    invoke-virtual {p0, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 2797
    .local v0, "dialogFragment":Landroid/app/Fragment;
    instance-of v3, v0, Landroid/app/DialogFragment;

    if-nez v3, :cond_1

    .line 2798
    const-string v3, "VNUtil"

    const-string v4, "dismissDialogFragment() it is not dialog fragment"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2802
    :cond_1
    :try_start_0
    check-cast v0, Landroid/app/DialogFragment;

    .end local v0    # "dialogFragment":Landroid/app/Fragment;
    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 2803
    const-string v2, "VNUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dismissDialogFragment() sucess dismiss "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2807
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 2804
    :catch_0
    move-exception v1

    .line 2805
    .local v1, "e":Ljava/lang/IllegalStateException;
    const-string v2, "VNUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dismissDialogFragment() failed dismiss + "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static enableProgressAirView(Landroid/content/ContentResolver;)Z
    .locals 4
    .param p0, "cr"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1878
    const-string v3, "finger_air_view_pregress_bar_preview"

    invoke-static {p0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1880
    .local v0, "result":I
    if-ne v0, v1, :cond_0

    .line 1883
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public static findFileIndex(Ljava/lang/String;Landroid/content/Context;)I
    .locals 15
    .param p0, "prefix"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 368
    if-nez p0, :cond_0

    .line 369
    const/4 v0, 0x1

    .line 413
    :goto_0
    return v0

    .line 371
    :cond_0
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "title"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "cast(substr(title, ?, 1000) as INTEGER) idx"

    aput-object v1, v2, v0

    .line 374
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "title like ? AND (_data LIKE \'%.amr\' or (_data LIKE \'%.m4a\' and recordingtype == \'1\'))"

    .line 375
    .local v3, "whereString":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "%d"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v14

    add-int/lit8 v14, v14, 0x2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v1, v9, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, " %"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 378
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string v5, "idx desc limit 1"

    .line 379
    .local v5, "oderBy":Ljava/lang/String;
    const/4 v6, 0x0

    .line 382
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 391
    :goto_1
    if-nez v6, :cond_1

    .line 392
    const/4 v0, 0x1

    goto :goto_0

    .line 383
    :catch_0
    move-exception v7

    .line 384
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "VNUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "findFileIndex - SQLiteException :"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    const/4 v6, 0x0

    .line 389
    goto :goto_1

    .line 386
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 387
    .local v8, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v0, "VNUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "findFileIndex - UnsupportedOperationException :"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    const/4 v6, 0x0

    goto :goto_1

    .line 395
    .end local v8    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 396
    const-string v0, "VNUtil"

    const-string v1, "findFileIndex - cursor count is zero"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 398
    const/4 v6, 0x0

    .line 399
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 402
    :cond_2
    const-wide/16 v10, 0x0

    .line 403
    .local v10, "lastIndex":J
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 405
    :try_start_1
    const-string v0, "idx"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-wide v10

    .line 410
    :goto_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 411
    const/4 v6, 0x0

    .line 413
    const-wide/16 v0, 0x1

    add-long/2addr v0, v10

    long-to-int v0, v0

    goto/16 :goto_0

    .line 406
    :catch_2
    move-exception v7

    .line 407
    .local v7, "e":Ljava/lang/Exception;
    const-string v0, "VNUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "findFileIndex - Exception : "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private static findFileName(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p0, "prefix"    # Ljava/lang/String;
    .param p1, "num"    # I

    .prologue
    .line 417
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%1$s %2$03d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static findLabelID(Landroid/content/Context;J)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 2275
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 2276
    .local v6, "builder":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2277
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2279
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 2280
    .local v8, "labelID":I
    if-eqz v7, :cond_1

    .line 2281
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2282
    const-string v0, "label_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 2284
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 2286
    :cond_1
    return v8
.end method

.method public static getApkVersion(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2668
    sget-object v3, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mVersionInfo:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 2669
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2671
    .local v2, "version":Ljava/lang/StringBuilder;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.android.app.voicenote"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 2673
    .local v1, "pinfo":Landroid/content/pm/PackageInfo;
    const-string v3, "(KVN "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2676
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mVersionInfo:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2681
    .end local v1    # "pinfo":Landroid/content/pm/PackageInfo;
    .end local v2    # "version":Ljava/lang/StringBuilder;
    :cond_0
    :goto_0
    sget-object v3, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mVersionInfo:Ljava/lang/String;

    return-object v3

    .line 2677
    .restart local v2    # "version":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v0

    .line 2678
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getAvailableSpace(Ljava/lang/String;)J
    .locals 10
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 449
    move-object v4, p0

    .line 451
    .local v4, "storageDirectory":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 454
    .local v2, "remainingSpace":J
    :try_start_0
    new-instance v1, Landroid/os/StatFs;

    invoke-direct {v1, v4}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 455
    .local v1, "stat":Landroid/os/StatFs;
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v6

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v8

    mul-long v2, v6, v8

    .line 456
    const-string v5, "VNUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getAvailableSpace size="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-wide v6, v2

    .line 460
    .end local v1    # "stat":Landroid/os/StatFs;
    :goto_0
    return-wide v6

    .line 458
    :catch_0
    move-exception v0

    .line 459
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v5, "VNUtil"

    const-string v6, "getAvailableStorage - exception. return 0"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const-wide/32 v6, 0x1457801

    goto :goto_0
.end method

.method public static getAvailableStorage(Z)J
    .locals 8
    .param p0, "internal"    # Z

    .prologue
    .line 428
    const/4 v2, 0x0

    .line 430
    .local v2, "storageDirectory":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 431
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getExternalStorageDirectorySd(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 437
    :goto_0
    :try_start_0
    new-instance v1, Landroid/os/StatFs;

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 438
    .local v1, "stat":Landroid/os/StatFs;
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v4

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v6

    mul-long/2addr v4, v6

    sput-wide v4, Lcom/sec/android/app/voicenote/common/util/VNUtil;->avaliableSize:J

    .line 439
    const-string v3, "VNUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAvailableStorage S size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-wide v6, Lcom/sec/android/app/voicenote/common/util/VNUtil;->avaliableSize:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    sget-wide v4, Lcom/sec/android/app/voicenote/common/util/VNUtil;->avaliableSize:J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 443
    .end local v1    # "stat":Landroid/os/StatFs;
    :goto_1
    return-wide v4

    .line 433
    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 441
    :catch_0
    move-exception v0

    .line 442
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v3, "VNUtil"

    const-string v4, "getAvailableStorage - exception. return 0"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const-wide/32 v4, 0x1457801

    goto :goto_1
.end method

.method public static getBlockSize(Z)I
    .locals 6
    .param p0, "internal"    # Z

    .prologue
    .line 465
    const/4 v3, 0x0

    .line 466
    .local v3, "storageDirectory":Ljava/lang/String;
    const/4 v0, 0x0

    .line 468
    .local v0, "blockSize":I
    if-nez p0, :cond_0

    .line 469
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getExternalStorageDirectorySd(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 475
    :goto_0
    :try_start_0
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 476
    .local v2, "stat":Landroid/os/StatFs;
    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v4, v0

    .line 480
    .end local v2    # "stat":Landroid/os/StatFs;
    :goto_1
    return v4

    .line 471
    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 478
    :catch_0
    move-exception v1

    .line 479
    .local v1, "ex":Ljava/lang/RuntimeException;
    const-string v4, "VNUtil"

    const-string v5, "cannot gain block size"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static getByte(JZ)J
    .locals 8
    .param p0, "bytes"    # J
    .param p2, "isAmr"    # Z

    .prologue
    .line 577
    const v0, 0x414828f6    # 12.51f

    .line 579
    .local v0, "bitrate":F
    if-nez p2, :cond_0

    .line 580
    const/high16 v0, 0x43000000    # 128.0f

    .line 583
    :cond_0
    move-wide v2, p0

    .line 584
    .local v2, "gbytes":J
    long-to-double v4, v2

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4020000000000000L    # 8.0

    mul-double/2addr v4, v6

    float-to-double v6, v0

    div-double/2addr v4, v6

    const-wide v6, 0x408f400000000000L    # 1000.0

    mul-double/2addr v4, v6

    double-to-long v2, v4

    .line 585
    return-wide v2
.end method

.method public static getContentMimeType(Landroid/content/Context;J)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    .line 1516
    const/4 v3, 0x0

    .line 1517
    .local v3, "selection":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1518
    .local v6, "builder":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1519
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1520
    const/4 v9, 0x0

    .line 1522
    .local v9, "mimeType":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1524
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1526
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1528
    if-eqz v7, :cond_0

    .line 1529
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1530
    const-string v1, "mime_type"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 1537
    :cond_0
    if-eqz v7, :cond_1

    .line 1538
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1539
    const/4 v7, 0x0

    .line 1543
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_1
    :goto_0
    return-object v9

    .line 1534
    :catch_0
    move-exception v8

    .line 1535
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "VNUtil"

    const-string v2, "mimetype is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1537
    if-eqz v7, :cond_1

    .line 1538
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1539
    const/4 v7, 0x0

    goto :goto_0

    .line 1537
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_2

    .line 1538
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1539
    const/4 v7, 0x0

    :cond_2
    throw v1
.end method

.method public static getContentURI(Landroid/content/Context;J)Landroid/net/Uri;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    const/4 v11, 0x0

    .line 1483
    const/4 v8, 0x0

    .line 1485
    .local v8, "contentUri":Landroid/net/Uri;
    const/4 v3, 0x0

    .line 1486
    .local v3, "selection":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1487
    .local v6, "builder":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1488
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1490
    const/4 v7, 0x0

    .line 1492
    .local v7, "c":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 1493
    .local v10, "sendId":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1496
    if-eqz v7, :cond_1

    .line 1497
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1498
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 1500
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1502
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://media/external/audio/media/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    move-object v0, v8

    .line 1512
    :goto_0
    return-object v0

    .line 1504
    :catch_0
    move-exception v9

    .line 1505
    .local v9, "e":Ljava/lang/Exception;
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1506
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1507
    const/4 v7, 0x0

    :cond_2
    move-object v0, v11

    .line 1509
    goto :goto_0
.end method

.method public static getContentUriFromFilePath(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1431
    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 1434
    .local v2, "cols":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 1435
    .local v8, "sendUri":Landroid/net/Uri;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 1436
    .local v9, "where":Ljava/lang/StringBuilder;
    const-string v0, "_data = ?"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1438
    const/4 v6, 0x0

    .line 1440
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1444
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1445
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://media"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 1451
    :cond_0
    if-eqz v6, :cond_1

    .line 1452
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1453
    const/4 v6, 0x0

    .line 1456
    :cond_1
    :goto_0
    return-object v8

    .line 1448
    :catch_0
    move-exception v7

    .line 1449
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    const-string v0, "VNUtil"

    const-string v1, "sendUri is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1451
    if-eqz v6, :cond_1

    .line 1452
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1453
    const/4 v6, 0x0

    goto :goto_0

    .line 1451
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1452
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1453
    const/4 v6, 0x0

    :cond_2
    throw v0
.end method

.method public static getCurrenId(Landroid/content/Context;Landroid/net/Uri;)J
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 889
    const-wide/16 v10, -0x1

    .line 891
    .local v10, "id":J
    if-nez p1, :cond_0

    move-wide v12, v10

    .line 925
    .end local v10    # "id":J
    .local v12, "id":J
    :goto_0
    return-wide v12

    .line 895
    .end local v12    # "id":J
    .restart local v10    # "id":J
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 896
    const/4 v7, 0x0

    .line 898
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x2

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 901
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_data=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 902
    .local v3, "where":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 904
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 905
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 906
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 907
    .local v6, "columnIndex":I
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    int-to-long v10, v0

    .line 912
    .end local v6    # "columnIndex":I
    :cond_1
    if-eqz v7, :cond_2

    .line 913
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 915
    :cond_2
    const/4 v7, 0x0

    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_3
    :goto_1
    move-wide v12, v10

    .line 925
    .end local v10    # "id":J
    .restart local v12    # "id":J
    goto :goto_0

    .line 909
    .end local v12    # "id":J
    .restart local v7    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "id":J
    :catch_0
    move-exception v8

    .line 910
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 912
    if-eqz v7, :cond_4

    .line 913
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 915
    :cond_4
    const/4 v7, 0x0

    .line 916
    goto :goto_1

    .line 912
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_5

    .line 913
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 915
    :cond_5
    const/4 v7, 0x0

    throw v0

    .line 918
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_6
    const/4 v9, 0x0

    .line 919
    .local v9, "temp":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v9

    .line 921
    if-eqz v9, :cond_3

    .line 922
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v10, v0

    goto :goto_1
.end method

.method public static getCurrentLabelInfo(Landroid/content/Context;J)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 1305
    const/4 v3, 0x0

    .line 1306
    .local v3, "selection":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1307
    .local v6, "builder":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1308
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1310
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1313
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 1314
    .local v8, "data":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 1315
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1316
    const-string v0, "_data"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 1317
    .local v9, "dataIndex":I
    const-string v0, "date_added"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 1319
    .local v10, "dataTime":I
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getIDCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 1320
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v7, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1321
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1323
    .end local v9    # "dataIndex":I
    .end local v10    # "dataTime":I
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1325
    :cond_1
    return-object v8
.end method

.method public static getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    const/4 v4, 0x0

    .line 857
    if-nez p0, :cond_0

    .line 870
    :goto_0
    return-object v4

    .line 858
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 859
    .local v6, "builder":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 860
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 861
    .local v2, "selection":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 863
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 864
    .local v8, "data":Ljava/lang/String;
    if-eqz v7, :cond_2

    .line 865
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 866
    const-string v0, "_data"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 868
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v4, v8

    .line 870
    goto :goto_0
.end method

.method public static getCurrentPathFromUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    .line 874
    if-nez p0, :cond_0

    .line 885
    :goto_0
    return-object v3

    .line 875
    :cond_0
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 876
    .local v2, "selection":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 878
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 879
    .local v7, "data":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 880
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 881
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 883
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v3, v7

    .line 885
    goto :goto_0
.end method

.method public static getCurrentSaveId()J
    .locals 2

    .prologue
    .line 1368
    sget-wide v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mCurrentSaveId:J

    return-wide v0
.end method

.method public static getCurrentUtcTime()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2577
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd HH:mm:ss.SSS"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2578
    .local v2, "mTimeFormatUtc":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 2579
    .local v0, "currentTime":Ljava/util/Date;
    const-string v3, "UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 2580
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 2582
    .local v1, "currentUtcTime":Ljava/lang/String;
    return-object v1
.end method

.method public static getDoFinishWhenClickItem()Z
    .locals 1

    .prologue
    .line 1376
    sget-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mDoFinishWhenClickItem:Z

    return v0
.end method

.method private static getDualStanbyCallState()I
    .locals 1

    .prologue
    .line 1804
    const/4 v0, 0x0

    .line 1816
    .local v0, "callState":I
    return v0
.end method

.method public static getExistingFiles(Ljava/io/File;)[Ljava/io/File;
    .locals 9
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 2812
    const/4 v1, 0x0

    .line 2814
    .local v1, "existingFiles":[Ljava/io/File;
    if-nez p0, :cond_0

    .line 2815
    const-string v7, "VNUtil"

    const-string v8, "isExistFile file is null"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 2843
    .end local v1    # "existingFiles":[Ljava/io/File;
    .local v2, "existingFiles":[Ljava/io/File;
    :goto_0
    return-object v2

    .line 2819
    .end local v2    # "existingFiles":[Ljava/io/File;
    .restart local v1    # "existingFiles":[Ljava/io/File;
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v6

    .line 2820
    .local v6, "parentDirectory":Ljava/io/File;
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 2821
    .local v3, "fileName":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    .line 2823
    .local v5, "intExtIndex":I
    if-lez v5, :cond_1

    .line 2824
    const/4 v7, 0x0

    invoke-virtual {v3, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 2828
    .local v4, "fileNameWithoutExt":Ljava/lang/String;
    :goto_1
    if-nez v6, :cond_2

    .line 2829
    const-string v7, "VNUtil"

    const-string v8, "isExistFile parentDirectory is null"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 2830
    .end local v1    # "existingFiles":[Ljava/io/File;
    .restart local v2    # "existingFiles":[Ljava/io/File;
    goto :goto_0

    .line 2826
    .end local v2    # "existingFiles":[Ljava/io/File;
    .end local v4    # "fileNameWithoutExt":Ljava/lang/String;
    .restart local v1    # "existingFiles":[Ljava/io/File;
    :cond_1
    move-object v4, v3

    .restart local v4    # "fileNameWithoutExt":Ljava/lang/String;
    goto :goto_1

    .line 2834
    :cond_2
    :try_start_0
    new-instance v7, Lcom/sec/android/app/voicenote/common/util/VNUtil$5;

    invoke-direct {v7, v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil$5;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_2
    move-object v2, v1

    .line 2843
    .end local v1    # "existingFiles":[Ljava/io/File;
    .restart local v2    # "existingFiles":[Ljava/io/File;
    goto :goto_0

    .line 2840
    .end local v2    # "existingFiles":[Ljava/io/File;
    .restart local v1    # "existingFiles":[Ljava/io/File;
    :catch_0
    move-exception v0

    .line 2841
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_2
.end method

.method public static getExternalStorageDirectorySd(Landroid/content/Context;)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 627
    if-eqz p0, :cond_1

    sget v7, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mSdSlotState:I

    if-nez v7, :cond_1

    .line 628
    const/4 v7, 0x2

    sput v7, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mSdSlotState:I

    .line 629
    const/4 v7, 0x0

    sput-object v7, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mExternalStorage:Ljava/lang/String;

    .line 630
    const-string v7, "storage"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/storage/StorageManager;

    .line 632
    .local v2, "mStorageManager":Landroid/os/storage/StorageManager;
    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v5

    .line 633
    .local v5, "storageVolumes":[Landroid/os/storage/StorageVolume;
    array-length v1, v5

    .line 634
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 635
    aget-object v4, v5, v0

    .line 636
    .local v4, "storageVolume":Landroid/os/storage/StorageVolume;
    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v6

    .line 637
    .local v6, "subSystem":Ljava/lang/String;
    if-eqz v6, :cond_0

    const-string v7, "sd"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 638
    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 639
    .local v3, "storagePath":Ljava/lang/String;
    sput-object v3, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mExternalStorage:Ljava/lang/String;

    .line 640
    const/4 v7, 0x1

    sput v7, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mSdSlotState:I

    .line 641
    move v0, v1

    .line 634
    .end local v3    # "storagePath":Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 645
    .end local v0    # "i":I
    .end local v1    # "len":I
    .end local v2    # "mStorageManager":Landroid/os/storage/StorageManager;
    .end local v4    # "storageVolume":Landroid/os/storage/StorageVolume;
    .end local v5    # "storageVolumes":[Landroid/os/storage/StorageVolume;
    .end local v6    # "subSystem":Ljava/lang/String;
    :cond_1
    sget-object v7, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mExternalStorage:Ljava/lang/String;

    return-object v7
.end method

.method public static getExternalStorageStateSd(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 536
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getExternalStorageDirectorySd(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 537
    .local v0, "externalStorage":Ljava/lang/String;
    const/4 v2, 0x0

    .line 539
    .local v2, "state":Ljava/lang/String;
    sget v3, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mSdSlotState:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 540
    const-string v3, "storage"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    .line 541
    .local v1, "mStorageManager":Landroid/os/storage/StorageManager;
    invoke-virtual {v1, v0}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 546
    .end local v1    # "mStorageManager":Landroid/os/storage/StorageManager;
    :goto_0
    return-object v2

    .line 543
    :cond_0
    const-string v2, "unknown"

    goto :goto_0
.end method

.method public static getFileCountByLabel(Landroid/content/Context;I)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "labelid"    # I

    .prologue
    .line 1583
    const/4 v3, 0x0

    .line 1584
    .local v3, "selection":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1585
    .local v6, "builder":Ljava/lang/StringBuilder;
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getListQuery(Landroid/content/Context;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1586
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    .line 1587
    const-string v1, " and ("

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1588
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "label_id=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1589
    const-string v1, "\')"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1591
    :cond_0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1592
    const/4 v9, 0x0

    .line 1594
    .local v9, "retVal":I
    const/4 v7, 0x0

    .line 1596
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1598
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1600
    if-eqz v7, :cond_1

    .line 1601
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 1607
    :cond_1
    if-eqz v7, :cond_2

    .line 1608
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1609
    const/4 v7, 0x0

    .line 1613
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_2
    :goto_0
    return v9

    .line 1604
    :catch_0
    move-exception v8

    .line 1605
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "VNUtil"

    const-string v2, "mimetype is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1607
    if-eqz v7, :cond_2

    .line 1608
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1609
    const/4 v7, 0x0

    goto :goto_0

    .line 1607
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_3

    .line 1608
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1609
    const/4 v7, 0x0

    :cond_3
    throw v1
.end method

.method public static getFileCountGroupByLabel(Landroid/content/Context;)Ljava/util/HashMap;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1645
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 1646
    .local v11, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "label_id"

    aput-object v4, v2, v1

    const/4 v1, 0x1

    const-string v4, "count(*) as cnt"

    aput-object v4, v2, v1

    .line 1647
    .local v2, "projection":[Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 1648
    .local v7, "builder":Ljava/lang/StringBuilder;
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getListQuery(Landroid/content/Context;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1649
    const-string v12, ") group by (label_id"

    .line 1650
    .local v12, "sortBy":Ljava/lang/String;
    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1651
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1653
    .local v3, "selection":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1655
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1657
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1659
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1660
    const/4 v6, 0x0

    .line 1661
    .local v6, "allCnt":I
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1662
    const-string v1, "label_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 1663
    .local v10, "key":I
    const-string v1, "cnt"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 1664
    .local v13, "value":I
    add-int/2addr v6, v13

    .line 1665
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v11, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1666
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1669
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v6    # "allCnt":I
    .end local v10    # "key":I
    .end local v13    # "value":I
    :catch_0
    move-exception v9

    .line 1670
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "VNUtil"

    const-string v4, "mimetype is null"

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1672
    if-eqz v8, :cond_0

    .line 1673
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1674
    const/4 v8, 0x0

    .line 1678
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_1
    return-object v11

    .line 1668
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v6    # "allCnt":I
    :cond_1
    const/4 v1, -0x1

    :try_start_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v11, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1672
    if-eqz v8, :cond_0

    .line 1673
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1674
    const/4 v8, 0x0

    goto :goto_1

    .line 1672
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v6    # "allCnt":I
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_2

    .line 1673
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1674
    const/4 v8, 0x0

    :cond_2
    throw v1
.end method

.method public static getFileCountInLibrary(Landroid/content/Context;)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1617
    const/4 v3, 0x0

    .line 1618
    .local v3, "selection":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getListQuery(Landroid/content/Context;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1619
    .local v6, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1620
    const/4 v9, 0x0

    .line 1622
    .local v9, "retVal":I
    const/4 v7, 0x0

    .line 1624
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1626
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1628
    if-eqz v7, :cond_0

    .line 1629
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 1635
    :cond_0
    if-eqz v7, :cond_1

    .line 1636
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1637
    const/4 v7, 0x0

    .line 1641
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_1
    :goto_0
    return v9

    .line 1632
    :catch_0
    move-exception v8

    .line 1633
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "VNUtil"

    const-string v2, "mimetype is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1635
    if-eqz v7, :cond_1

    .line 1636
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1637
    const/4 v7, 0x0

    goto :goto_0

    .line 1635
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_2

    .line 1636
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1637
    const/4 v7, 0x0

    :cond_2
    throw v1
.end method

.method public static getFileName(Landroid/content/Context;J)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 1054
    const/4 v3, 0x0

    .line 1055
    .local v3, "selection":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1056
    .local v6, "builder":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1057
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1059
    const/4 v9, 0x0

    .line 1060
    .local v9, "title":Ljava/lang/String;
    if-eqz p0, :cond_1

    .line 1061
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1063
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 1064
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1065
    const-string v0, "title"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 1066
    .local v8, "dataIndex":I
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1068
    .end local v8    # "dataIndex":I
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1072
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_1
    return-object v9
.end method

.method public static getHoverPopupWindowByToolType(Landroid/view/View;Z)Landroid/widget/HoverPopupWindow;
    .locals 3
    .param p0, "v"    # Landroid/view/View;
    .param p1, "isEnableAnim"    # Z

    .prologue
    const/4 v2, 0x1

    .line 842
    const/4 v0, 0x0

    .line 843
    .local v0, "hover":Landroid/widget/HoverPopupWindow;
    sget-boolean v1, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_FINGER_AIR_VIEW:Z

    if-eqz v1, :cond_1

    .line 844
    invoke-virtual {p0, v2}, Landroid/view/View;->getHoverPopupWindow(I)Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 845
    if-eqz v0, :cond_0

    .line 846
    invoke-virtual {v0, v2}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 847
    invoke-virtual {v0, p1}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 853
    :cond_0
    :goto_0
    return-object v0

    .line 850
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    goto :goto_0
.end method

.method public static getIDCode(Landroid/content/Context;)Ljava/lang/String;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1329
    const-string v7, "phone"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 1330
    .local v1, "mTelephonyMgr":Landroid/telephony/TelephonyManager;
    const-wide/16 v2, -0x1

    .line 1331
    .local v2, "input":J
    const-string v6, ""

    .line 1334
    .local v6, "result":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1340
    :cond_0
    :goto_0
    const-wide/16 v10, 0x3e

    div-long v8, v2, v10

    .line 1341
    .local v8, "value":J
    const-wide/16 v10, 0x3e

    mul-long/2addr v10, v8

    sub-long v4, v2, v10

    .line 1342
    .local v4, "rest":J
    move-wide v2, v8

    .line 1344
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-wide/16 v10, 0x41

    add-long/2addr v10, v4

    long-to-int v10, v10

    int-to-char v10, v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1345
    const-wide/16 v10, 0x3e

    cmp-long v7, v2, v10

    if-gtz v7, :cond_0

    .line 1346
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-wide/16 v10, 0x41

    add-long/2addr v10, v2

    long-to-int v10, v10

    int-to-char v10, v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1350
    return-object v6

    .line 1335
    .end local v4    # "rest":J
    .end local v8    # "value":J
    :catch_0
    move-exception v0

    .line 1336
    .local v0, "e":Ljava/lang/Exception;
    const-string v7, "VNUtil"

    const-string v10, "Invalid IMEI exception"

    invoke-static {v7, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getIDFromFilePath(Landroid/content/Context;Ljava/lang/String;)J
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v10, 0x0

    .line 1460
    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v10

    .line 1463
    .local v2, "cols":[Ljava/lang/String;
    const-wide/16 v8, 0x0

    .line 1464
    .local v8, "id":J
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 1465
    .local v7, "where":Ljava/lang/StringBuilder;
    const-string v0, "_data = ?"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1467
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v10

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1471
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1472
    invoke-interface {v6, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 1475
    :cond_0
    if-eqz v6, :cond_1

    .line 1476
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1477
    const/4 v6, 0x0

    .line 1479
    :cond_1
    return-wide v8
.end method

.method public static getListQuery(Landroid/content/Context;)Ljava/lang/StringBuilder;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 273
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "VNUtil"

    const-string v2, "support previous version"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const-string v1, "((_data LIKE \'%.3ga\' and is_music == \'0\') or "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    const-string v1, "_data LIKE \'%.amr\' or (_data LIKE \'%.m4a\' and recordingtype == \'1\'))"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and (_data NOT LIKE \'%/.voice.3ga\' and _data NOT LIKE \'%/.voice.amr\' and _data NOT LIKE \'%/.voice.m4a\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and (mime_type LIKE \'audio/3gpp\' or mime_type LIKE \'audio/amr\' or mime_type LIKE \'audio/mp4\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and (_size != \'0\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    sget-boolean v1, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_RECORDED_CALLS:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mRecordedNumber:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 281
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mRecordedNumber:Ljava/lang/String;

    const-string v2, "01000000000"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 282
    const-string v1, " and (recorded_number != \'null\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    :cond_0
    :goto_0
    return-object v0

    .line 284
    :cond_1
    const-string v1, " and (recorded_number = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mRecordedNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static getMaxLength(Landroid/content/Intent;)J
    .locals 6
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2385
    const-string v2, "android.provider.MediaStore.extra.MAX_BYTES"

    const-wide/32 v4, 0x9c5e00

    invoke-virtual {p0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2386
    .local v0, "result":J
    const-wide/16 v2, 0x2800

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 2387
    const-wide/16 v0, 0x0

    .line 2389
    :cond_0
    return-wide v0
.end method

.method public static getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1923
    const/4 v1, 0x0

    .line 1926
    .local v1, "secretDir":Ljava/lang/String;
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1931
    :goto_0
    return-object v1

    .line 1927
    :catch_0
    move-exception v0

    .line 1928
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getRecordedNumber(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v9, 0x0

    .line 2769
    const/4 v7, 0x0

    .line 2770
    .local v7, "number":Ljava/lang/String;
    sget-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_RECORDED_CALLS:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mRecordedNumber:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2771
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mRecordedNumber:Ljava/lang/String;

    const-string v1, "01000000000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2772
    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "recorded_number"

    aput-object v0, v2, v9

    .line 2773
    .local v2, "cols":[Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 2774
    .local v8, "where":Ljava/lang/StringBuilder;
    const-string v0, "_data = ?"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2775
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v9

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2777
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2778
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2780
    :cond_0
    if-eqz v6, :cond_1

    .line 2781
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2788
    .end local v2    # "cols":[Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v8    # "where":Ljava/lang/StringBuilder;
    :cond_1
    :goto_0
    return-object v7

    .line 2785
    :cond_2
    sget-object v7, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mRecordedNumber:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getRemainingMmsSize(JI)J
    .locals 10
    .param p0, "realFreeSize"    # J
    .param p2, "warningSize"    # I

    .prologue
    const-wide/16 v2, 0x0

    .line 521
    cmp-long v5, p0, v2

    if-gtz v5, :cond_1

    .line 531
    :cond_0
    :goto_0
    return-wide v2

    .line 524
    :cond_1
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getMmsMaxSize()J

    move-result-wide v0

    .line 525
    .local v0, "getMmsMaxSize":J
    const-wide/16 v2, 0x0

    .line 526
    .local v2, "mAvailableTimeSize":J
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getRootPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".voice"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 527
    .local v4, "saveFile":Ljava/io/File;
    int-to-long v6, p2

    add-long/2addr v6, p0

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v8

    add-long v2, v6, v8

    .line 528
    cmp-long v5, v2, v0

    if-lez v5, :cond_0

    .line 529
    move-wide v2, v0

    goto :goto_0
.end method

.method public static getRootPath()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 652
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getInternalStorageSelected()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 653
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 663
    .local v1, "dirpath":Ljava/lang/String;
    :goto_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 664
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_0

    .line 665
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 668
    :cond_0
    return-object v1

    .line 655
    .end local v0    # "dir":Ljava/io/File;
    .end local v1    # "dirpath":Ljava/lang/String;
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getExternalStorageDirectorySd(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 656
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 657
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "dirpath":Ljava/lang/String;
    goto :goto_0

    .line 659
    .end local v1    # "dirpath":Ljava/lang/String;
    :cond_2
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getExternalStorageDirectorySd(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "dirpath":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getSdslotState(Landroid/content/Context;)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 550
    sget v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mSdSlotState:I

    if-nez v0, :cond_0

    .line 551
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getExternalStorageStateSd(Landroid/content/Context;)Ljava/lang/String;

    .line 553
    :cond_0
    sget v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mSdSlotState:I

    return v0
.end method

.method public static getSecretFileCountInLibrary(Landroid/content/Context;Z)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bSecretFile"    # Z

    .prologue
    .line 1753
    const/4 v3, 0x0

    .line 1754
    .local v3, "selection":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getListQuery(Landroid/content/Context;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1755
    .local v6, "builder":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_2

    .line 1756
    const-string v1, " and (is_secretbox != \'1\')"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1760
    :goto_0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1761
    const/4 v9, 0x0

    .line 1763
    .local v9, "retVal":I
    const/4 v7, 0x0

    .line 1765
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1767
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1769
    if-eqz v7, :cond_0

    .line 1770
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 1776
    :cond_0
    if-eqz v7, :cond_1

    .line 1777
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1778
    const/4 v7, 0x0

    .line 1782
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_1
    :goto_1
    return v9

    .line 1758
    .end local v7    # "c":Landroid/database/Cursor;
    .end local v9    # "retVal":I
    :cond_2
    const-string v1, " and (is_secretbox != \'0\')"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1773
    .restart local v7    # "c":Landroid/database/Cursor;
    .restart local v9    # "retVal":I
    :catch_0
    move-exception v8

    .line 1774
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "VNUtil"

    const-string v2, "mimetype is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1776
    if-eqz v7, :cond_1

    .line 1777
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1778
    const/4 v7, 0x0

    goto :goto_1

    .line 1776
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_3

    .line 1777
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1778
    const/4 v7, 0x0

    :cond_3
    throw v1
.end method

.method public static getShareIntents(Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;
    .locals 8
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "pm"    # Landroid/content/pm/PackageManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/content/pm/PackageManager;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2204
    new-instance v0, Landroid/content/Intent;

    const/4 v7, 0x0

    invoke-direct {v0, p0, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2205
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2206
    const/4 v7, 0x0

    invoke-virtual {p2, v0, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 2208
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2209
    .local v6, "targetedShareIntents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 2210
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 2211
    .local v4, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v7, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 2212
    .local v3, "packageName":Ljava/lang/String;
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2213
    .local v5, "targetedShareIntent":Landroid/content/Intent;
    invoke-virtual {v5, p1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2214
    invoke-virtual {v5, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2215
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2219
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v5    # "targetedShareIntent":Landroid/content/Intent;
    :cond_0
    return-object v6
.end method

.method public static getSortQuery()Ljava/lang/String;
    .locals 4

    .prologue
    .line 295
    const-string v2, "sort_mode"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v1

    .line 297
    .local v1, "sortmode":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 299
    .local v0, "sb":Ljava/lang/StringBuilder;
    packed-switch v1, :pswitch_data_0

    .line 328
    const-string v2, "date_modified"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DESC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 301
    :pswitch_0
    const-string v2, "date_added"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DESC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    const-string v2, "_id"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DESC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 306
    :pswitch_1
    const-string v2, "date_added"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ASC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 310
    :pswitch_2
    const-string v2, "_display_name"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " COLLATE LOCALIZED ASC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 317
    :pswitch_3
    const-string v2, "label_id"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ASC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 320
    :pswitch_4
    const-string v2, "year_name"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DESC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 323
    :pswitch_5
    const-string v2, "is_secretbox"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DESC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    const-string v2, "date_modified"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DESC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 299
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getSoundsPath()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 672
    const-string v3, "/Sounds"

    .line 676
    .local v3, "sounds":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getInternalStorageSelected()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 677
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 687
    .local v1, "dirpath":Ljava/lang/String;
    :goto_0
    const-string v4, "VNUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Environment.getExternalStorageDirectory().getPath()="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secV(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 690
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_0

    .line 691
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 694
    :cond_0
    return-object v1

    .line 679
    .end local v0    # "dir":Ljava/io/File;
    .end local v1    # "dirpath":Ljava/lang/String;
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getExternalStorageDirectorySd(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 680
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 681
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "dirpath":Ljava/lang/String;
    goto :goto_0

    .line 684
    .end local v1    # "dirpath":Ljava/lang/String;
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getExternalStorageDirectorySd(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "dirpath":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getTagsData(Landroid/content/Context;J)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 2480
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 2481
    .local v6, "builder":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2482
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2483
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2484
    .local v7, "cursor":Landroid/database/Cursor;
    const-string v8, ""

    .line 2485
    .local v8, "tagsData":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 2486
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2487
    const-string v0, "year_name"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2489
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 2491
    :cond_1
    return-object v8
.end method

.method public static getTempSPDFilepath(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1971
    const/4 v3, 0x0

    .line 1973
    .local v3, "spdfilepath":Ljava/lang/String;
    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/files/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1974
    .local v0, "directory":Ljava/io/File;
    const-string v4, "vnote"

    const-string v5, "spd"

    invoke-static {v4, v5, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    .line 1976
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 1977
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1978
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1984
    .end local v0    # "directory":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    return-object v3

    .line 1980
    :catch_0
    move-exception v1

    .line 1981
    .local v1, "e":Ljava/io/IOException;
    const-string v4, "VNUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getTempSPDFilepath : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 1982
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getTypeOfSelectedItem(Landroid/content/Context;J)I
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    .line 2685
    const/4 v4, -0x1

    .line 2686
    .local v4, "typeOfSelectItem":I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMode()Z

    move-result v1

    .line 2687
    .local v1, "isPersonalPageMode":Z
    const/4 v2, 0x0

    .line 2688
    .local v2, "isPersonalPageMounted":Z
    const/4 v3, 0x0

    .line 2690
    .local v3, "personalPageRoot":Ljava/lang/String;
    if-eqz p0, :cond_0

    if-eqz v1, :cond_0

    .line 2691
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMounted(Landroid/content/Context;)Z

    move-result v2

    .line 2692
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 2694
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 2695
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 2696
    .local v0, "filePath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 2697
    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2698
    const/4 v4, 0x1

    .line 2705
    .end local v0    # "filePath":Ljava/lang/String;
    :cond_0
    :goto_0
    return v4

    .line 2700
    .restart local v0    # "filePath":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static getVTShareIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2223
    const-string v0, "android.intent.action.SEND_MULTIPLE"

    .line 2224
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 2225
    .local v4, "pm":Landroid/content/pm/PackageManager;
    const-string v6, "application/txt"

    invoke-static {v0, v6, v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getShareIntents(Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;

    move-result-object v5

    .line 2226
    .local v5, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    const-string v6, "application/txt"

    invoke-static {v0, v6, v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getShareIntents(Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2227
    .local v2, "list1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    const-string v6, "audio/mp4"

    invoke-static {v0, v6, v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getShareIntents(Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;

    move-result-object v3

    .line 2228
    .local v3, "list2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 2229
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 2231
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2232
    .local v1, "chooserIntent":Landroid/content/Intent;
    const-string v6, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v1, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2233
    return-object v1
.end method

.method public static getVersionOfContextProviders(Landroid/content/Context;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2565
    const/4 v2, -0x1

    .line 2567
    .local v2, "version":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.samsung.android.providers.context"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 2569
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2573
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 2570
    :catch_0
    move-exception v0

    .line 2571
    .local v0, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "VNUtil"

    const-string v4, "[SW] Could not find ContextProvider"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static hasAvailableApp(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x0

    .line 1380
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1383
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static hasLED()Z
    .locals 6

    .prologue
    .line 2400
    const-string v0, "/sys/class/sec/led/led_pattern"

    .line 2401
    .local v0, "LED_FILE":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2402
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    .line 2404
    .local v2, "result":Z
    const-string v3, "VNUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "hasLED() result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2406
    return v2
.end method

.method public static hasPermanentMenuKey(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 193
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    return v0
.end method

.method public static hasTagData(Landroid/content/Context;Ljava/util/ArrayList;)Z
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v12, 0x0

    .line 1198
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 1199
    invoke-virtual {p1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ", "

    const-string v2, " or _id="

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "["

    const-string v2, "(_id="

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "]"

    const-string v2, ")"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    .line 1202
    .local v11, "where_ids":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 1203
    .local v9, "strBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1204
    const-string v0, " and "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "year_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIKE \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NFC%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1206
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1208
    .local v3, "where":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1210
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 1225
    :cond_0
    :goto_0
    const/4 v10, 0x0

    .line 1226
    .local v10, "tagsData":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 1227
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1228
    const-string v0, "year_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1230
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1232
    :cond_2
    const-string v0, "VNUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hasTagData "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secI(Ljava/lang/String;Ljava/lang/String;)V

    .line 1233
    if-eqz v10, :cond_3

    const-string v0, "NFC"

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 1238
    .end local v3    # "where":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v9    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v10    # "tagsData":Ljava/lang/String;
    .end local v11    # "where_ids":Ljava/lang/String;
    :goto_1
    return v0

    .line 1212
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v9    # "strBuilder":Ljava/lang/StringBuilder;
    .restart local v11    # "where_ids":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 1213
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "VNUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hasTagData - SQLiteException  :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1215
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1216
    const/4 v6, 0x0

    goto :goto_0

    .line 1218
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 1219
    .local v8, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v0, "VNUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hasTagData - UnsupportedOperationException :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1221
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1222
    const/4 v6, 0x0

    goto/16 :goto_0

    .end local v8    # "ex":Ljava/lang/UnsupportedOperationException;
    .restart local v10    # "tagsData":Ljava/lang/String;
    :cond_3
    move v0, v12

    .line 1233
    goto :goto_1

    .line 1235
    .end local v3    # "where":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v9    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v10    # "tagsData":Ljava/lang/String;
    .end local v11    # "where_ids":Ljava/lang/String;
    :cond_4
    const-string v0, "VNUtil"

    const-string v1, "tagDataIDs is nothing"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v12

    .line 1238
    goto :goto_1
.end method

.method public static hoverHapticAndFeedback(Landroid/content/Context;Landroid/media/AudioManager;Landroid/view/View;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "audioManager"    # Landroid/media/AudioManager;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 2743
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 2751
    :cond_0
    :goto_0
    return-void

    .line 2746
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "finger_air_view_sound_and_haptic_feedback"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2748
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 2749
    const/16 v0, 0x9

    invoke-virtual {p2, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    goto :goto_0
.end method

.method public static initLabelDB(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    .line 2237
    new-instance v4, Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;-><init>(Landroid/content/Context;)V

    .line 2238
    .local v4, "mDbOpenHelper":Lcom/sec/android/app/voicenote/common/util/LabelHelper;
    const/4 v3, 0x0

    .line 2239
    .local v3, "mCursor":Landroid/database/Cursor;
    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 2240
    const/4 v5, 0x0

    .line 2241
    .local v5, "selection":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getListQuery()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2242
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2244
    :try_start_0
    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getDB()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 2259
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2272
    :cond_1
    :goto_1
    return-void

    .line 2245
    :catch_0
    move-exception v1

    .line 2246
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    const-string v6, "VNUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "listBinding - SQLiteException :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 2247
    if-eqz v3, :cond_0

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-nez v6, :cond_0

    .line 2248
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 2249
    const/4 v3, 0x0

    goto :goto_0

    .line 2251
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v2

    .line 2252
    .local v2, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v6, "VNUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "listBinding - UnsupportedOperationException :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 2253
    if-eqz v3, :cond_0

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-nez v6, :cond_0

    .line 2254
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 2255
    const/4 v3, 0x0

    goto :goto_0

    .line 2262
    .end local v2    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_2
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v6

    const/4 v7, 0x1

    if-ge v6, v7, :cond_3

    .line 2263
    const/16 v6, 0x43

    const/16 v7, 0x5b

    const/16 v8, 0x79

    invoke-static {v6, v7, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0b0029

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->insertColumn(Ljava/lang/String;Ljava/lang/String;)J

    .line 2264
    const/16 v6, 0x8f

    const/16 v7, 0xb9

    const/16 v8, 0x3b

    invoke-static {v6, v7, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0b0027

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->insertColumn(Ljava/lang/String;Ljava/lang/String;)J

    .line 2265
    const/16 v6, 0xff

    const/16 v7, 0x97

    invoke-static {v6, v7, v9}, Landroid/graphics/Color;->rgb(III)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0b002a

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->insertColumn(Ljava/lang/String;Ljava/lang/String;)J

    .line 2266
    const/16 v6, 0x3f

    const/16 v7, 0xa9

    const/16 v8, 0xf6

    invoke-static {v6, v7, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0b0025

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->insertColumn(Ljava/lang/String;Ljava/lang/String;)J

    .line 2268
    const-string v6, "category_label_id"

    invoke-static {v6, v9}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 2270
    :cond_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 2271
    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    goto/16 :goto_1
.end method

.method public static insertLog(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "feature"    # Ljava/lang/String;

    .prologue
    .line 2622
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 2623
    :cond_0
    const-string v4, "VNUtil"

    const-string v5, "ContextProvider cannot insert log datas are invalidated"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 2624
    const-string v4, "VNUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "insertLog feature : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 2647
    :cond_1
    :goto_0
    return-void

    .line 2628
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isLogingContextProviders(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2632
    const-string v4, "content://com.samsung.android.providers.context.log.use_app_feature_survey"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2637
    .local v3, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2638
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2639
    .local v2, "row":Landroid/content/ContentValues;
    const-string v4, "app_id"

    const-string v5, "com.sec.android.app.voicenote"

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2640
    const-string v4, "feature"

    invoke-virtual {v2, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2641
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 2642
    const-string v4, "VNUtil"

    const-string v5, "ContextProvider insertion operation is performed."

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2643
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 2644
    .local v1, "ex":Ljava/lang/Exception;
    const-string v4, "VNUtil"

    const-string v5, "Error while using the ContextProvider"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 2645
    const-string v4, "VNUtil"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contentUri"    # Ljava/lang/String;
    .param p2, "startTime"    # Ljava/lang/String;
    .param p3, "stopTime"    # Ljava/lang/String;
    .param p4, "duration"    # I

    .prologue
    .line 2587
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-gez p4, :cond_2

    .line 2589
    :cond_0
    const-string v4, "VNUtil"

    const-string v5, "ContextProvider cannot insert log datas are invalidated"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 2590
    const-string v4, "VNUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "insertLog contentUri : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 2591
    const-string v4, "VNUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "insertLog startTime : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 2592
    const-string v4, "VNUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "insertLog stopTime : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 2593
    const-string v4, "VNUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "insertLog duration : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 2619
    :cond_1
    :goto_0
    return-void

    .line 2598
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isLogingContextProviders(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2602
    const-string v4, "content://com.samsung.android.providers.context.log.record_audio"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2606
    .local v3, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2607
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2608
    .local v2, "row":Landroid/content/ContentValues;
    const-string v4, "app_id"

    const-string v5, "com.sec.android.app.voicenote"

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2609
    const-string v4, "uri"

    invoke-virtual {v2, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2610
    const-string v4, "start_time"

    invoke-virtual {v2, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2611
    const-string v4, "stop_time"

    invoke-virtual {v2, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2612
    const-string v4, "duration"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2613
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 2614
    const-string v4, "VNUtil"

    const-string v5, "ContextProvider insertion operation is performed."

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2615
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 2616
    .local v1, "ex":Ljava/lang/Exception;
    const-string v4, "VNUtil"

    const-string v5, "Error while using the ContextProvider"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 2617
    const-string v4, "VNUtil"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isAvailableForMemoryCard(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 557
    sget v3, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mKNOXCallingUserId:I

    if-lez v3, :cond_1

    .line 558
    const-string v2, "VNUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isAvailableForMemoryCard() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mKNOXCallingUserId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    :cond_0
    :goto_0
    return v1

    .line 562
    :cond_1
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_PRODUCT_FEATURE_SETTINGS_SUPPORT_SDCARD"

    invoke-virtual {v3, v4, v2}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 566
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getExternalStorageStateSd(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 567
    .local v0, "state":Ljava/lang/String;
    sget v3, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mSdSlotState:I

    if-ne v3, v2, :cond_0

    if-eqz v0, :cond_0

    const-string v3, "bad_removal"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "unmounted"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "removed"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 573
    goto :goto_0
.end method

.method public static isBackgrounRunning(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2363
    const-string v3, "activity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 2364
    .local v0, "activityMgr":Landroid/app/ActivityManager;
    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    .line 2365
    .local v1, "runningTask":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 2367
    .local v2, "topActivity":Ljava/lang/String;
    const-class v3, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v4

    .line 2370
    :goto_0
    return v3

    :cond_0
    move v3, v5

    goto :goto_0
.end method

.method public static isCallActive(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1820
    if-nez p0, :cond_1

    .line 1823
    :cond_0
    :goto_0
    return v1

    .line 1822
    :cond_1
    const-string v3, "phone"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1823
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v3

    if-ne v3, v2, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public static isCallIdle(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1827
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isVoipIdle()Z

    move-result v1

    .line 1830
    .local v1, "isVoipIdle":Z
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCommunicationIdle(Landroid/content/Context;)Z

    move-result v0

    .line 1832
    .local v0, "isCommIdle":Z
    const-string v2, "VNUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isCallIdle() isVoipIdle:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isCommIdle(Such like google talk..):"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 1834
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isCallIdleInternal(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1853
    const/4 v0, 0x0

    .line 1854
    .local v0, "callState":I
    const-string v3, "phone"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 1855
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    .line 1857
    if-nez v0, :cond_0

    .line 1859
    const-string v3, "phone2"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 1860
    .local v2, "tm2":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_0

    .line 1861
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    .line 1865
    .end local v2    # "tm2":Landroid/telephony/TelephonyManager;
    :cond_0
    if-nez v0, :cond_1

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static isCommunicationIdle(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1869
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1870
    .local v0, "am":Landroid/media/AudioManager;
    const/4 v1, 0x0

    .line 1871
    .local v1, "mode":I
    if-eqz v0, :cond_0

    .line 1872
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v1

    .line 1874
    :cond_0
    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isEasyMode(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 2650
    if-nez p0, :cond_1

    .line 2651
    const-string v3, "VNUtil"

    const-string v4, "Context == NULL in isEasyMode(); returning false"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 2659
    :cond_0
    :goto_0
    return v2

    .line 2654
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "easy_mode_switch"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 2655
    .local v0, "easyModeSwitchState":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "easy_mode_voicerecorder"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 2656
    .local v1, "easyModeVoiceRecorderState":I
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    move v2, v3

    .line 2657
    goto :goto_0
.end method

.method public static isEnableAirViewInforPreview(Landroid/content/ContentResolver;)Z
    .locals 3
    .param p0, "cr"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1956
    const-string v2, "finger_air_view_information_preview"

    invoke-static {p0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 1959
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isEnablePenHoverInforPreview(Landroid/content/ContentResolver;)Z
    .locals 3
    .param p0, "cr"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1963
    const-string v2, "pen_hovering"

    invoke-static {p0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    const-string v2, "air_view_master_onoff"

    invoke-static {p0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 1967
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isExistFile(Ljava/io/File;)Z
    .locals 6
    .param p0, "file"    # Ljava/io/File;

    .prologue
    const/4 v3, 0x0

    .line 706
    if-nez p0, :cond_1

    .line 707
    const-string v4, "VNUtil"

    const-string v5, "isExistFile file is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    :cond_0
    :goto_0
    return v3

    .line 711
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    .line 712
    .local v2, "parentDirectory":Ljava/io/File;
    if-nez v2, :cond_2

    .line 713
    const-string v4, "VNUtil"

    const-string v5, "isExistFile parentDirectory is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 718
    :cond_2
    :try_start_0
    new-instance v4, Lcom/sec/android/app/voicenote/common/util/VNUtil$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil$1;-><init>(Ljava/io/File;)V

    invoke-virtual {v2, v4}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 724
    .local v1, "files":[Ljava/io/File;
    if-eqz v1, :cond_0

    array-length v4, v1
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-lez v4, :cond_0

    .line 725
    const/4 v3, 0x1

    goto :goto_0

    .line 727
    .end local v1    # "files":[Ljava/io/File;
    :catch_0
    move-exception v0

    .line 728
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isExistFile(Ljava/lang/String;)Z
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 698
    if-nez p0, :cond_0

    .line 699
    const-string v0, "VNUtil"

    const-string v1, "isExistFile path is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    const/4 v0, 0x0

    .line 702
    :goto_0
    return v0

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExistFile(Ljava/io/File;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isExistFileName(Ljava/io/File;)Z
    .locals 9
    .param p0, "file"    # Ljava/io/File;

    .prologue
    const/4 v6, 0x0

    .line 735
    if-nez p0, :cond_1

    .line 736
    const-string v7, "VNUtil"

    const-string v8, "isExistFile file is null"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    :cond_0
    :goto_0
    return v6

    .line 740
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    .line 741
    .local v5, "parentDirectory":Ljava/io/File;
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 742
    .local v1, "fileName":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 744
    .local v4, "intExtIndex":I
    if-lez v4, :cond_2

    .line 745
    invoke-virtual {v1, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 749
    .local v2, "fileNameWithoutExt":Ljava/lang/String;
    :goto_1
    if-nez v5, :cond_3

    .line 750
    const-string v7, "VNUtil"

    const-string v8, "isExistFile parentDirectory is null"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 747
    .end local v2    # "fileNameWithoutExt":Ljava/lang/String;
    :cond_2
    move-object v2, v1

    .restart local v2    # "fileNameWithoutExt":Ljava/lang/String;
    goto :goto_1

    .line 755
    :cond_3
    :try_start_0
    new-instance v7, Lcom/sec/android/app/voicenote/common/util/VNUtil$2;

    invoke-direct {v7, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil$2;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v3

    .line 764
    .local v3, "files":[Ljava/io/File;
    if-eqz v3, :cond_0

    array-length v7, v3
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-lez v7, :cond_0

    .line 765
    const/4 v6, 0x1

    goto :goto_0

    .line 767
    .end local v3    # "files":[Ljava/io/File;
    :catch_0
    move-exception v0

    .line 768
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isExploreByTouchEnabled(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2730
    const-string v3, "accessibility"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 2732
    .local v0, "am":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    .line 2733
    .local v1, "isAccessibilityEnabled":Z
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v2

    .line 2735
    .local v2, "isExploreByTouchEnabled":Z
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 2736
    const/4 v3, 0x1

    .line 2738
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isHoveringUI(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1948
    if-nez p0, :cond_0

    .line 1949
    const/4 v0, 0x0

    .line 1951
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isLogingContextProviders(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2557
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getVersionOfContextProviders(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 2558
    const/4 v0, 0x0

    .line 2560
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isLowBattery(Landroid/content/Intent;)Z
    .locals 10
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    .line 242
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 243
    .local v0, "action":Ljava/lang/String;
    const/4 v5, 0x1

    .line 247
    .local v5, "mLowBatteryWarningLevel":I
    const-string v7, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 248
    const-string v7, "status"

    invoke-virtual {p0, v7, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 250
    .local v4, "battStatus":I
    const-string v7, "scale"

    const/16 v8, 0x64

    invoke-virtual {p0, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 251
    .local v3, "battScale":I
    const-string v7, "level"

    invoke-virtual {p0, v7, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 253
    .local v1, "battLevel":I
    if-nez v3, :cond_0

    .line 267
    .end local v1    # "battLevel":I
    .end local v3    # "battScale":I
    .end local v4    # "battStatus":I
    :goto_0
    return v6

    .line 257
    .restart local v1    # "battLevel":I
    .restart local v3    # "battScale":I
    .restart local v4    # "battStatus":I
    :cond_0
    int-to-float v7, v1

    const/high16 v8, 0x42c80000    # 100.0f

    mul-float/2addr v7, v8

    int-to-float v8, v3

    div-float v2, v7, v8

    .line 259
    .local v2, "battLevelPercent":F
    int-to-float v7, v5

    cmpg-float v7, v2, v7

    if-gtz v7, :cond_1

    const/4 v7, 0x2

    if-eq v4, v7, :cond_1

    .line 260
    const-string v7, "VNUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Level = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const-string v7, "VNUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Status = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 267
    .end local v1    # "battLevel":I
    .end local v2    # "battLevelPercent":F
    .end local v3    # "battScale":I
    .end local v4    # "battStatus":I
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static isM4A(Ljava/lang/String;)Z
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1076
    if-nez p0, :cond_1

    .line 1084
    :cond_0
    :goto_0
    return v0

    .line 1080
    :cond_1
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    .line 1081
    const-string v1, ".m4a"

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1082
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isMobileDataConnected(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 2335
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 2336
    .local v0, "manager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 2338
    .local v1, "mobile":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2339
    const/4 v2, 0x1

    .line 2342
    :goto_0
    return v2

    .line 2341
    :cond_0
    const-string v3, "VNUtil"

    const-string v4, "mobile data is not Connected"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isMultiTouch(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p0, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 2521
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 2523
    .local v0, "action":I
    and-int/lit16 v5, v0, 0xff

    packed-switch v5, :pswitch_data_0

    :goto_0
    :pswitch_0
    move v1, v4

    .line 2553
    :cond_0
    :goto_1
    :pswitch_1
    return v1

    .line 2526
    :pswitch_2
    invoke-virtual {p0, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    sput v5, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mActivePointerId:I

    goto :goto_0

    .line 2531
    :pswitch_3
    const/4 v5, -0x1

    sput v5, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mActivePointerId:I

    goto :goto_0

    .line 2539
    :pswitch_4
    const v5, 0xff00

    and-int/2addr v5, v0

    shr-int/lit8 v3, v5, 0x8

    .line 2541
    .local v3, "pointerIndex":I
    invoke-virtual {p0, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    .line 2543
    .local v2, "pointerId":I
    sget v5, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mActivePointerId:I

    if-ne v2, v5, :cond_0

    .line 2546
    if-nez v3, :cond_1

    .line 2547
    .local v1, "newPointerIndex":I
    :goto_2
    invoke-virtual {p0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    sput v5, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mActivePointerId:I

    move v1, v4

    .line 2548
    goto :goto_1

    .end local v1    # "newPointerIndex":I
    :cond_1
    move v1, v4

    .line 2546
    goto :goto_2

    .line 2523
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public static isNFCEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2471
    const-string v2, "nfc"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/nfc/NfcManager;

    .line 2472
    .local v1, "manager":Landroid/nfc/NfcManager;
    invoke-virtual {v1}, Landroid/nfc/NfcManager;->getDefaultAdapter()Landroid/nfc/NfcAdapter;

    move-result-object v0

    .line 2473
    .local v0, "adapter":Landroid/nfc/NfcAdapter;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2474
    const/4 v2, 0x1

    .line 2476
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isNetworkConnected(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2309
    const/4 v0, 0x0

    .line 2310
    .local v0, "bIsConnected":Z
    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 2311
    .local v1, "manager":Landroid/net/ConnectivityManager;
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 2312
    .local v2, "mobile":Landroid/net/NetworkInfo;
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 2314
    .local v3, "wifi":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2315
    :cond_1
    const/4 v0, 0x1

    .line 2319
    :goto_0
    return v0

    .line 2317
    :cond_2
    const-string v4, "VNUtil"

    const-string v5, "isNetworkConnected : network error"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static declared-synchronized isPersonalPageMode()Z
    .locals 4

    .prologue
    .line 1888
    const-class v2, Lcom/sec/android/app/voicenote/common/util/VNUtil;

    monitor-enter v2

    const/4 v1, 0x0

    .line 1891
    .local v1, "isMounted":Z
    :try_start_0
    invoke-static {}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateMode()Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 1895
    :goto_0
    monitor-exit v2

    return v1

    .line 1892
    :catch_0
    move-exception v0

    .line 1893
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const/4 v1, 0x0

    goto :goto_0

    .line 1888
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catchall_0
    move-exception v3

    monitor-exit v2

    throw v3
.end method

.method public static declared-synchronized isPersonalPageMounted(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1912
    const-class v2, Lcom/sec/android/app/voicenote/common/util/VNUtil;

    monitor-enter v2

    const/4 v1, 0x0

    .line 1915
    .local v1, "isMounted":Z
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 1919
    :goto_0
    monitor-exit v2

    return v1

    .line 1916
    :catch_0
    move-exception v0

    .line 1917
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const/4 v1, 0x0

    goto :goto_0

    .line 1912
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catchall_0
    move-exception v3

    monitor-exit v2

    throw v3
.end method

.method public static declared-synchronized isPersonalPageNormal(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1899
    const-class v2, Lcom/sec/android/app/voicenote/common/util/VNUtil;

    monitor-enter v2

    const/4 v1, 0x0

    .line 1902
    .local v1, "isReady":Z
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->isReady(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 1908
    :goto_0
    monitor-exit v2

    return v1

    .line 1903
    :catch_0
    move-exception v0

    .line 1904
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const/4 v1, 0x0

    .line 1907
    goto :goto_0

    .line 1905
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catch_1
    move-exception v0

    .line 1906
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    const/4 v1, 0x0

    goto :goto_0

    .line 1899
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catchall_0
    move-exception v3

    monitor-exit v2

    throw v3
.end method

.method public static isRecordedCalls()Z
    .locals 1

    .prologue
    .line 2762
    sget-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_RECORDED_CALLS:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mRecordedNumber:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2763
    const/4 v0, 0x1

    .line 2765
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSIMInsertedOnlyInSlot2()Z
    .locals 3

    .prologue
    .line 1420
    const-string v2, "ril.MSIMM"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1421
    .local v1, "simm":Ljava/lang/String;
    const/4 v0, 0x1

    .line 1422
    .local v0, "onlyInSlot2":Z
    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1423
    const/4 v0, 0x1

    .line 1427
    :goto_0
    return v0

    .line 1425
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSTTFileExist(Landroid/content/Context;)Z
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1682
    const/4 v3, 0x0

    .line 1683
    .local v3, "selection":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getListQuery(Landroid/content/Context;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1684
    .local v6, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1685
    const/4 v12, 0x0

    .line 1687
    .local v12, "retVal":Z
    const/4 v7, 0x0

    .line 1689
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1690
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1691
    if-eqz v7, :cond_1

    .line 1692
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1694
    :cond_0
    const-string v1, "_data"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1696
    .local v11, "path":Ljava/lang/String;
    if-eqz v11, :cond_3

    invoke-static {v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isM4A(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1697
    new-instance v9, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    invoke-direct {v9, v11}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 1698
    .local v9, "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v10

    .line 1699
    .local v10, "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    iget-boolean v1, v10, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_3

    .line 1700
    const/4 v12, 0x1

    .line 1712
    .end local v9    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v10    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .end local v11    # "path":Ljava/lang/String;
    :cond_1
    :goto_0
    if-eqz v7, :cond_2

    .line 1713
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1714
    const/4 v7, 0x0

    .line 1717
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_2
    :goto_1
    return v12

    .line 1704
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v11    # "path":Ljava/lang/String;
    :cond_3
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 1709
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v11    # "path":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 1710
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v1, "VNUtil"

    const-string v2, "mimetype is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1712
    if-eqz v7, :cond_2

    .line 1713
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1714
    const/4 v7, 0x0

    goto :goto_1

    .line 1712
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_4

    .line 1713
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1714
    const/4 v7, 0x0

    :cond_4
    throw v1
.end method

.method public static isSameFileInLibrary(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1722
    const/4 v3, 0x0

    .line 1723
    .local v3, "selection":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getListQuery(Landroid/content/Context;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1724
    .local v6, "builder":Ljava/lang/StringBuilder;
    const-string v1, " and (title == \'"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' COLLATE NOCASE)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1725
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1726
    const/4 v9, 0x0

    .line 1728
    .local v9, "retVal":Z
    const/4 v7, 0x0

    .line 1730
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1732
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1734
    if-eqz v7, :cond_0

    .line 1735
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_0

    .line 1736
    const/4 v9, 0x1

    .line 1742
    :cond_0
    if-eqz v7, :cond_1

    .line 1743
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1744
    const/4 v7, 0x0

    .line 1748
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_1
    :goto_0
    return v9

    .line 1739
    :catch_0
    move-exception v8

    .line 1740
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "VNUtil"

    const-string v2, "mimetype is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1742
    if-eqz v7, :cond_1

    .line 1743
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1744
    const/4 v7, 0x0

    goto :goto_0

    .line 1742
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_2

    .line 1743
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1744
    const/4 v7, 0x0

    :cond_2
    throw v1
.end method

.method public static isSecretBoxFile(Landroid/content/Context;J)I
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    const/4 v10, 0x1

    .line 1547
    const/4 v3, 0x0

    .line 1548
    .local v3, "selection":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1549
    .local v6, "builder":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1550
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1551
    const/4 v9, -0x1

    .line 1553
    .local v9, "secretbox":I
    const/4 v7, 0x0

    .line 1555
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1557
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1559
    if-eqz v7, :cond_0

    .line 1560
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1561
    const-string v1, "is_secretbox"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 1568
    :cond_0
    if-eqz v7, :cond_1

    .line 1569
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1570
    const/4 v7, 0x0

    .line 1574
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_1
    :goto_0
    if-nez v9, :cond_3

    move v1, v10

    .line 1579
    :goto_1
    return v1

    .line 1565
    :catch_0
    move-exception v8

    .line 1566
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "VNUtil"

    const-string v2, "mimetype is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1568
    if-eqz v7, :cond_1

    .line 1569
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1570
    const/4 v7, 0x0

    goto :goto_0

    .line 1568
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_2

    .line 1569
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1570
    const/4 v7, 0x0

    :cond_2
    throw v1

    .line 1576
    :cond_3
    if-ne v9, v10, :cond_4

    .line 1577
    const/4 v1, 0x0

    goto :goto_1

    .line 1579
    :cond_4
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public static isServiceRunning(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "serviceClassName"    # Ljava/lang/String;

    .prologue
    .line 2347
    const-string v4, "activity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 2349
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const v4, 0x7fffffff

    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v3

    .line 2352
    .local v3, "services":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v3, :cond_1

    .line 2353
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 2354
    .local v2, "runningServiceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v4, v2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2355
    const/4 v4, 0x1

    .line 2359
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "runningServiceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static isUsaEnglish(Landroid/content/res/Resources;)Z
    .locals 4
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 2374
    const-string v2, "USA"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 2376
    .local v1, "result":Z
    if-eqz v1, :cond_0

    .line 2377
    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2378
    .local v0, "locale":Ljava/lang/String;
    const-string v2, "en_US"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 2381
    .end local v0    # "locale":Ljava/lang/String;
    :cond_0
    return v1
.end method

.method public static isValidTags(Landroid/content/Context;Ljava/lang/String;)I
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "labelInfo"    # Ljava/lang/String;

    .prologue
    .line 1242
    const/16 v2, 0x3a

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v16

    .line 1243
    .local v16, "idIndex":I
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v10

    .line 1244
    .local v10, "dateIndex":I
    const/4 v11, 0x0

    .line 1245
    .local v11, "fileID":Ljava/lang/String;
    const/4 v12, 0x0

    .line 1246
    .local v12, "filedate":Ljava/lang/String;
    const/4 v13, 0x0

    .line 1248
    .local v13, "filepath":Ljava/lang/String;
    if-lez v16, :cond_0

    .line 1249
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 1251
    :cond_0
    if-lez v10, :cond_1

    .line 1252
    add-int/lit8 v2, v16, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 1253
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 1256
    :cond_1
    if-nez v13, :cond_2

    .line 1257
    const-string v2, "VNUtil"

    const-string v3, "File does not exist."

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 1258
    const/4 v2, -0x1

    .line 1301
    :goto_0
    return v2

    .line 1260
    :cond_2
    if-eqz v11, :cond_3

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getIDCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1261
    const-string v2, "VNUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DIFFERENT DEVICE : fileID is different, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 1262
    const/4 v2, -0x2

    goto :goto_0

    .line 1265
    :cond_3
    const/4 v5, 0x0

    .line 1266
    .local v5, "selection":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1267
    .local v8, "builder":Ljava/lang/StringBuilder;
    const-string v2, "_data"

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1268
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1270
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1271
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_9

    .line 1272
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1273
    const-string v2, "year_name"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1274
    .local v17, "tagsData":Ljava/lang/String;
    const-string v2, "date_added"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 1276
    .local v18, "tagsTime":Ljava/lang/String;
    if-eqz v11, :cond_4

    if-nez v12, :cond_5

    .line 1277
    :cond_4
    const-string v2, "VNUtil"

    const-string v3, "OLD VERSION : fileID and filedate is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 1278
    const-string v2, "NFC"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1279
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1282
    :cond_5
    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1283
    const-string v2, "VNUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DIFFERENT TIME : filedate("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "), tagsTime("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 1285
    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 1286
    .local v20, "tagslong":J
    invoke-static {v12}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 1288
    .local v14, "filelong":J
    sub-long v2, v20, v14

    const-wide/16 v6, 0x1

    cmp-long v2, v2, v6

    if-nez v2, :cond_7

    .line 1289
    const-string v2, "VNUtil"

    const-string v3, "DIFFERENT TIME : same"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 1294
    .end local v14    # "filelong":J
    .end local v20    # "tagslong":J
    :cond_6
    const-string v2, "NFC"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1295
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1291
    .restart local v14    # "filelong":J
    .restart local v20    # "tagslong":J
    :cond_7
    const/4 v2, -0x1

    goto/16 :goto_0

    .line 1299
    .end local v14    # "filelong":J
    .end local v17    # "tagsData":Ljava/lang/String;
    .end local v18    # "tagsTime":Ljava/lang/String;
    .end local v20    # "tagslong":J
    :cond_8
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1301
    :cond_9
    const/4 v2, -0x1

    goto/16 :goto_0
.end method

.method private static isVoipIdle()Z
    .locals 5

    .prologue
    .line 1838
    const/4 v1, 0x1

    .line 1841
    .local v1, "isVoipIdle":Z
    :try_start_0
    const-string v3, "voip"

    invoke-static {v3}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/os/IVoIPInterface$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IVoIPInterface;

    move-result-object v2

    .line 1843
    .local v2, "voipCall":Landroid/os/IVoIPInterface;
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/os/IVoIPInterface;->isVoIPIdle()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_0

    .line 1844
    const/4 v1, 0x0

    .line 1849
    .end local v2    # "voipCall":Landroid/os/IVoIPInterface;
    :cond_0
    :goto_0
    return v1

    .line 1846
    :catch_0
    move-exception v0

    .line 1847
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "VNUtil"

    const-string v4, "remote exception happened, something was wrong in voip module."

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isWiFiConnected(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 2323
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 2324
    .local v0, "manager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 2326
    .local v1, "wifi":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2330
    :goto_0
    return v2

    .line 2329
    :cond_0
    const-string v2, "VNUtil"

    const-string v3, "Wi-Fi is not Connected"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static makeSTTTextFile(Ljava/io/File;Ljava/util/ArrayList;)V
    .locals 7
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2055
    .local p1, "mSttData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    const-string v5, "VNUtil"

    const-string v6, "[HIYA]makeSTTTextFile : start"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2057
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2058
    :cond_0
    const-string v5, "VNUtil"

    const-string v6, "[HIYA]makeSTTTextFile : data is null or empty"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2092
    :goto_0
    return-void

    .line 2062
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2063
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 2065
    .local v3, "os":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2066
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 2067
    invoke-virtual {p0}, Ljava/io/File;->createNewFile()Z

    .line 2070
    :cond_2
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2072
    .end local v3    # "os":Ljava/io/FileOutputStream;
    .local v4, "os":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    :try_start_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_4

    .line 2073
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget v5, v5, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    if-nez v5, :cond_3

    .line 2074
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v5, v5, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2072
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2077
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2086
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v4

    .line 2091
    .end local v2    # "i":I
    .end local v4    # "os":Ljava/io/FileOutputStream;
    .restart local v3    # "os":Ljava/io/FileOutputStream;
    :goto_2
    const-string v5, "VNUtil"

    const-string v6, "[HIYA]makeSTTTextFile : end"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2087
    .end local v3    # "os":Ljava/io/FileOutputStream;
    .restart local v2    # "i":I
    .restart local v4    # "os":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v5

    move-object v3, v4

    .line 2089
    .end local v4    # "os":Ljava/io/FileOutputStream;
    .restart local v3    # "os":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 2078
    .end local v2    # "i":I
    :catch_1
    move-exception v1

    .line 2080
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2086
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 2087
    :catch_2
    move-exception v5

    goto :goto_2

    .line 2081
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v1

    .line 2083
    .local v1, "e":Ljava/io/IOException;
    :goto_4
    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2086
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_2

    .line 2087
    :catch_4
    move-exception v5

    goto :goto_2

    .line 2085
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 2086
    :goto_5
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 2088
    :goto_6
    throw v5

    .line 2087
    :catch_5
    move-exception v6

    goto :goto_6

    .line 2085
    .end local v3    # "os":Ljava/io/FileOutputStream;
    .restart local v2    # "i":I
    .restart local v4    # "os":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "os":Ljava/io/FileOutputStream;
    .restart local v3    # "os":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 2081
    .end local v3    # "os":Ljava/io/FileOutputStream;
    .restart local v4    # "os":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v1

    move-object v3, v4

    .end local v4    # "os":Ljava/io/FileOutputStream;
    .restart local v3    # "os":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 2078
    .end local v3    # "os":Ljava/io/FileOutputStream;
    .restart local v4    # "os":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v1

    move-object v3, v4

    .end local v4    # "os":Ljava/io/FileOutputStream;
    .restart local v3    # "os":Ljava/io/FileOutputStream;
    goto :goto_3
.end method

.method public static needDataCheckHelpDialog(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2847
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isWiFiConnected(Landroid/content/Context;)Z

    move-result v1

    .line 2848
    .local v1, "isWifi":Z
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isMobileDataConnected(Landroid/content/Context;)Z

    move-result v0

    .line 2850
    .local v0, "isMobileData":Z
    if-eqz v1, :cond_0

    const-string v2, "connect_via_wifi_help"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    .line 2851
    const-string v2, "need_wifi_check"

    .line 2855
    :goto_0
    return-object v2

    .line 2852
    :cond_0
    if-eqz v0, :cond_1

    const-string v2, "connect_via_mobile_help"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 2853
    const-string v2, "need_mobile_check"

    goto :goto_0

    .line 2855
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static readFile(Ljava/io/File;)Ljava/lang/String;
    .locals 10
    .param p0, "file"    # Ljava/io/File;

    .prologue
    const/4 v6, 0x0

    .line 2143
    const/4 v5, 0x0

    .line 2144
    .local v5, "readcount":I
    const/4 v0, 0x0

    .line 2145
    .local v0, "buffer":[B
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2146
    const/4 v3, 0x0

    .line 2148
    .local v3, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2149
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v8

    long-to-int v5, v8

    .line 2150
    new-array v0, v5, [B

    .line 2151
    invoke-virtual {v4, v0}, Ljava/io/FileInputStream;->read([B)I

    .line 2152
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 2165
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V

    .line 2166
    .end local v4    # "fis":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    return-object v6

    .line 2153
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v1

    .line 2154
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 2156
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 2157
    :catch_1
    move-exception v2

    .line 2158
    .local v2, "ex":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2153
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "ex":Ljava/lang/Exception;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_2
    move-exception v1

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public static renameFile(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)I
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .param p3, "oldPath"    # Ljava/lang/String;
    .param p4, "newTitle"    # Ljava/lang/String;

    .prologue
    .line 776
    const/4 v9, -0x1

    .line 777
    .local v9, "dbResult":I
    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 778
    .local v14, "oldFile":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    .line 779
    .local v16, "oldFileName":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x4

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 782
    .local v15, "oldFileExtension":Ljava/lang/String;
    invoke-virtual {v14}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v13

    .line 783
    .local v13, "newPath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 784
    .local v12, "newName":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 796
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 799
    .local v8, "c":Landroid/database/Cursor;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 800
    .local v18, "strBuilder":Ljava/lang/StringBuilder;
    if-eqz v8, :cond_1

    .line 801
    invoke-interface {v8}, Landroid/database/Cursor;->moveToLast()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 802
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v11

    .local v11, "i":I
    :goto_0
    if-lez v11, :cond_0

    .line 803
    const-string v2, "_id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v2, v2

    cmp-long v2, p1, v2

    if-nez v2, :cond_2

    .line 804
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 805
    const-string v2, "_id="

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 806
    const-string v2, "_id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 808
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 809
    .local v20, "where":Ljava/lang/String;
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 810
    .local v19, "v":Landroid/content/ContentValues;
    const-string v2, "title"

    move-object/from16 v0, v19

    move-object/from16 v1, p4

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    const-string v2, "_display_name"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    const-string v2, "_data"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    .line 825
    .end local v11    # "i":I
    .end local v19    # "v":Landroid/content/ContentValues;
    .end local v20    # "where":Ljava/lang/String;
    :cond_0
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 828
    :cond_1
    invoke-static {v14}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExistFile(Ljava/io/File;)Z

    move-result v2

    sget-boolean v3, Lcom/sec/android/app/voicenote/common/util/VNUtil;->FUNC_RETURN_CORRECT:Z

    if-ne v2, v3, :cond_4

    const/4 v2, 0x1

    if-ne v9, v2, :cond_4

    .line 829
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v17

    .line 831
    .local v17, "result":Z
    if-nez v17, :cond_3

    .line 832
    const-string v2, "VNUtil"

    const-string v3, "renameFile() failed rename"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    const/4 v2, -0x1

    .line 838
    .end local v17    # "result":Z
    :goto_2
    return v2

    .line 817
    .restart local v11    # "i":I
    .restart local v19    # "v":Landroid/content/ContentValues;
    .restart local v20    # "where":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 818
    .local v10, "e":Landroid/database/sqlite/SQLiteConstraintException;
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    goto :goto_1

    .line 822
    .end local v10    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    .end local v19    # "v":Landroid/content/ContentValues;
    .end local v20    # "where":Ljava/lang/String;
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 802
    add-int/lit8 v11, v11, -0x1

    goto/16 :goto_0

    .line 836
    .end local v11    # "i":I
    .restart local v17    # "result":Z
    :cond_3
    const/4 v2, 0x1

    goto :goto_2

    .line 838
    .end local v17    # "result":Z
    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public static declared-synchronized resetToast()V
    .locals 2

    .prologue
    .line 221
    const-class v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 222
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 223
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    :cond_0
    monitor-exit v1

    return-void

    .line 221
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static sendFileDeleteBroadcast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1354
    const/4 v0, 0x0

    .line 1355
    .local v0, "i":Landroid/content/Intent;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-gt v1, v2, :cond_0

    .line 1356
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "i":Landroid/content/Intent;
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/voicenote/common/util/VNUtil;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1357
    .restart local v0    # "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1361
    :goto_0
    return-void

    .line 1359
    :cond_0
    invoke-static {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendScan(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1943
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.MEDIA_SCAN"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1944
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1945
    return-void
.end method

.method public static setCurrentSaveId(J)V
    .locals 0
    .param p0, "currentSaveId"    # J

    .prologue
    .line 1364
    sput-wide p0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mCurrentSaveId:J

    .line 1365
    return-void
.end method

.method public static setDoFinishWhenClickItem(Z)V
    .locals 0
    .param p0, "value"    # Z

    .prologue
    .line 1372
    sput-boolean p0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mDoFinishWhenClickItem:Z

    .line 1373
    return-void
.end method

.method public static setExternalStorageEnable(I)V
    .locals 0
    .param p0, "knoxUserId"    # I

    .prologue
    .line 2664
    sput p0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mKNOXCallingUserId:I

    .line 2665
    return-void
.end method

.method public static setFixedNewFileName(ZLjava/lang/String;)V
    .locals 0
    .param p0, "enable"    # Z
    .param p1, "newName"    # Ljava/lang/String;

    .prologue
    .line 344
    sput-boolean p0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isNameFixed:Z

    .line 345
    sput-object p1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->fixedName:Ljava/lang/String;

    .line 346
    return-void
.end method

.method public static setRecordedNumber(Ljava/lang/String;)V
    .locals 1
    .param p0, "recordedNumber"    # Ljava/lang/String;

    .prologue
    .line 2754
    sget-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_RECORDED_CALLS:Z

    if-eqz v0, :cond_0

    .line 2755
    sput-object p0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mRecordedNumber:Ljava/lang/String;

    .line 2759
    :goto_0
    return-void

    .line 2757
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mRecordedNumber:Ljava/lang/String;

    goto :goto_0
.end method

.method public static declared-synchronized setWakeLock(Landroid/content/Context;I)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mode"    # I

    .prologue
    .line 2410
    const-class v2, Lcom/sec/android/app/voicenote/common/util/VNUtil;

    monitor-enter v2

    packed-switch p1, :pswitch_data_0

    .line 2459
    :pswitch_0
    :try_start_0
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 2460
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2461
    const-string v1, "VNUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setWakeLock() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2462
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2463
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2468
    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    .line 2412
    :pswitch_1
    :try_start_1
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 2413
    const-string v1, "VNUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setWakeLock() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2414
    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 2415
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x20000006

    const-string v3, "VNUtil"

    invoke-virtual {v0, v1, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 2416
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 2417
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2410
    .end local v0    # "pm":Landroid/os/PowerManager;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 2422
    :pswitch_2
    :try_start_2
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 2423
    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 2424
    .restart local v0    # "pm":Landroid/os/PowerManager;
    const v1, 0x1000001a

    const-string v3, "LockScreen_VNUtil"

    invoke-virtual {v0, v1, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 2426
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 2427
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 2428
    const-string v1, "VNUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mWakeLock.acquire(); : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2433
    .end local v0    # "pm":Landroid/os/PowerManager;
    :pswitch_3
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 2434
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2435
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2436
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 2437
    const-string v1, "VNUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mWakeLock.release(); : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 2443
    :pswitch_4
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 2444
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2445
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2446
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 2447
    const-string v1, "VNUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mWakeLock.release(); : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2450
    :cond_1
    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 2451
    .restart local v0    # "pm":Landroid/os/PowerManager;
    const v1, 0x20000006

    const-string v3, "LockScreen_VNUtil"

    invoke-virtual {v0, v1, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 2452
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 2453
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mLockScreenWakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 2454
    const-string v1, "VNUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mWakeLock.acquire(); : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 2410
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static setWindowStatusBarFlag(Landroid/app/Activity;)V
    .locals 2
    .param p0, "a"    # Landroid/app/Activity;

    .prologue
    .line 1995
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1996
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 1997
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 1998
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1999
    return-void
.end method

.method public static showChooseSimCardDialog(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ringUri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x1

    .line 1388
    sput-boolean v5, Lcom/sec/android/app/voicenote/common/util/VNUtil;->mDoFinishWhenClickItem:Z

    .line 1389
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "select_name_1"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1391
    .local v1, "strSim1":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "select_name_2"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1393
    .local v2, "strSim2":Ljava/lang/String;
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    aput-object v1, v0, v3

    aput-object v2, v0, v5

    .line 1396
    .local v0, "items":[Ljava/lang/CharSequence;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0b0119

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/voicenote/common/util/VNUtil$4;

    invoke-direct {v4, p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil$4;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-virtual {v3, v0, v4}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 1417
    return-void
.end method

.method public static showEditDialoginPlay(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Landroid/content/Context;Landroid/app/FragmentManager;)J
    .locals 10
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fragmentManager"    # Landroid/app/FragmentManager;

    .prologue
    const-wide/16 v6, -0x1

    .line 2708
    if-nez p0, :cond_1

    .line 2726
    :cond_0
    :goto_0
    return-wide v6

    .line 2712
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v8

    invoke-static {p1, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 2713
    .local v0, "currntPath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 2716
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2717
    .local v3, "prevFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 2718
    .local v4, "selelectedFileName":Ljava/lang/String;
    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 2720
    .local v2, "dotIndex":I
    const/4 v5, -0x1

    if-eq v2, v5, :cond_2

    .line 2721
    const/4 v5, 0x0

    invoke-virtual {v4, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 2723
    :cond_2
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v6

    invoke-static {p1, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->findLabelID(Landroid/content/Context;J)I

    move-result v6

    invoke-static {v5, v4, v6}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->setArguments(ILjava/lang/String;I)Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    move-result-object v1

    .line 2724
    .local v1, "dialog":Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;
    const-string v5, "FRAGMENT_DIALOG"

    invoke-virtual {v1, p2, v5}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 2726
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v6

    goto :goto_0
.end method

.method public static declared-synchronized showManagedToast(Landroid/content/Context;II)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stringId"    # I
    .param p2, "toastLength"    # I

    .prologue
    .line 197
    const-class v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 198
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    .line 205
    :goto_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    monitor-exit v1

    return-void

    .line 200
    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 201
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    .line 202
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    invoke-virtual {v0, p2}, Landroid/widget/Toast;->setDuration(I)V

    .line 203
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized showManagedToast(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "toastLength"    # I

    .prologue
    .line 209
    const-class v1, Lcom/sec/android/app/voicenote/common/util/VNUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 210
    invoke-static {p0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    .line 217
    :goto_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    monitor-exit v1

    return-void

    .line 212
    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 213
    invoke-static {p0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    .line 214
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    invoke-virtual {v0, p2}, Landroid/widget/Toast;->setDuration(I)V

    .line 215
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNUtil;->toast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static showToast(Landroid/content/Context;II)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stringId"    # I
    .param p2, "toastLength"    # I

    .prologue
    .line 185
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showManagedToast(Landroid/content/Context;II)V

    .line 186
    return-void
.end method

.method public static showToast(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "toastLength"    # I

    .prologue
    .line 189
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showManagedToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 190
    return-void
.end method

.method public static showToastSaved(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 228
    const v1, 0x7f0b0089

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "msg":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "iw"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 231
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u200f"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 233
    :cond_0
    invoke-static {p0, v0, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 234
    return-void
.end method

.method public static singleSend(Landroid/content/Context;J)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    .line 1787
    const/4 v2, 0x0

    .line 1788
    .local v2, "uri":Landroid/net/Uri;
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getContentURI(Landroid/content/Context;J)Landroid/net/Uri;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1789
    new-instance v3, Ljava/io/File;

    invoke-static {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileName(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1791
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1792
    .local v1, "shareIntent":Landroid/content/Intent;
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getContentMimeType(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1793
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1796
    const v3, 0x7f0b011f

    :try_start_0
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1801
    :goto_0
    return-void

    .line 1798
    :catch_0
    move-exception v0

    .line 1799
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "SendAppListDialog"

    const-string v4, "singleSend() - activity not found!"

    invoke-static {v3, v4, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static singleSendText_File(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 2096
    const-string v3, "VNUtil"

    const-string v4, "[HIYA]singleSendText_File : start"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2098
    const/4 v2, 0x0

    .line 2100
    .local v2, "uri":Landroid/net/Uri;
    const-string v3, "VNUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[HIYA]filename : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2102
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 2103
    const-string v3, "VNUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[HIYA]uri : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2105
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2106
    .local v1, "shareIntent":Landroid/content/Intent;
    const-string v3, "application/txt"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2107
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2110
    const v3, 0x7f0b011f

    :try_start_0
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2116
    :goto_0
    const-string v3, "VNUtil"

    const-string v4, "[HIYA]singleSendText_File : end"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2117
    return-void

    .line 2112
    :catch_0
    move-exception v0

    .line 2113
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "SendAppListDialog"

    const-string v4, "singleSendText_File() - activity not found!"

    invoke-static {v3, v4, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static singleSendText_String(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 2121
    const-string v5, "VNUtil"

    const-string v6, "singleSendText_String : start"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2123
    const-string v2, "text/plain"

    .line 2125
    .local v2, "mimeType":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2126
    .local v1, "file":Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->readFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v4

    .line 2128
    .local v4, "text":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.SEND"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2129
    .local v3, "shareIntent":Landroid/content/Intent;
    const-string v5, "text/plain"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2130
    const-string v5, "android.intent.extra.TEXT"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2133
    const v5, 0x7f0b011f

    :try_start_0
    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2139
    :goto_0
    const-string v5, "VNUtil"

    const-string v6, "[HIYA]singleSendText_String : end"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2140
    return-void

    .line 2135
    :catch_0
    move-exception v0

    .line 2136
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v5, "SendAppListDialog"

    const-string v6, "singleSendText_String() - activity not found!"

    invoke-static {v5, v6, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static singleSendVT(Landroid/content/Context;JLjava/lang/String;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .param p3, "filename"    # Ljava/lang/String;

    .prologue
    .line 2171
    const-string v5, "VNUtil"

    const-string v6, "[HIYA]singleSendVT : start"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2173
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2175
    .local v0, "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    const/4 v3, 0x0

    .line 2176
    .local v3, "uri1":Landroid/net/Uri;
    const/4 v4, 0x0

    .line 2178
    .local v4, "uri2":Landroid/net/Uri;
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getContentURI(Landroid/content/Context;J)Landroid/net/Uri;

    move-result-object v3

    if-nez v3, :cond_0

    .line 2179
    new-instance v5, Ljava/io/File;

    invoke-static {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileName(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 2182
    :cond_0
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    .line 2184
    const-string v5, "VNUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[HIYA]uri1 : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2185
    const-string v5, "VNUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[HIYA]uri2 : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2187
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2188
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2190
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2191
    .local v2, "shareIntent":Landroid/content/Intent;
    const-string v5, "android.intent.extra.STREAM"

    invoke-virtual {v2, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2192
    const-string v5, "application/txt,audio/mp4"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2195
    const v5, 0x7f0b011f

    :try_start_0
    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2200
    :goto_0
    const-string v5, "VNUtil"

    const-string v6, "[HIYA]singleSendVT : end"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2201
    return-void

    .line 2196
    :catch_0
    move-exception v1

    .line 2197
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    const-string v5, "SendAppListDialog"

    const-string v6, "singleSendText() - activity not found!"

    invoke-static {v5, v6, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static stringForReadDate(Landroid/content/Context;J)Ljava/lang/String;
    .locals 5
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "date"    # J

    .prologue
    .line 589
    const-string v0, ""

    .line 590
    .local v0, "dateString":Ljava/lang/String;
    const-string v1, "yyyy/MM/dd"

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-static {v1, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 592
    return-object v0
.end method

.method public static stringForReadTime(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 596
    const-string v9, ":"

    invoke-virtual {p1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 597
    .local v6, "out":[Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 598
    .local v8, "result":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 600
    .local v7, "res":Landroid/content/res/Resources;
    const/4 v4, 0x0

    .line 603
    .local v4, "index":I
    :try_start_0
    array-length v9, v6
    :try_end_0
    .catch Ljava/util/UnknownFormatConversionException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v10, 0x2

    if-le v9, v10, :cond_4

    .line 604
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "index":I
    .local v5, "index":I
    :try_start_1
    aget-object v9, v6, v4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 605
    .local v1, "iHour":I
    if-lez v1, :cond_0

    .line 606
    const v9, 0x7f0a0001

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v7, v9, v1, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/util/UnknownFormatConversionException; {:try_start_1 .. :try_end_1} :catch_1

    .line 611
    .end local v1    # "iHour":I
    :cond_0
    :goto_0
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "index":I
    .restart local v4    # "index":I
    :try_start_2
    aget-object v9, v6, v5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 612
    .local v2, "iMin":I
    aget-object v9, v6, v4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 614
    .local v3, "iSec":I
    if-lez v2, :cond_1

    .line 615
    const v9, 0x7f0a0002

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v7, v9, v2, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 617
    :cond_1
    if-gtz v3, :cond_2

    if-nez v3, :cond_3

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    if-nez v9, :cond_3

    .line 618
    :cond_2
    const v9, 0x7f0a0003

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v7, v9, v3, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/util/UnknownFormatConversionException; {:try_start_2 .. :try_end_2} :catch_0

    .line 623
    .end local v2    # "iMin":I
    .end local v3    # "iSec":I
    :cond_3
    :goto_1
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9

    .line 620
    :catch_0
    move-exception v0

    .line 621
    .local v0, "e":Ljava/util/UnknownFormatConversionException;
    :goto_2
    const-string v9, "VNUtil"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "stringForReadTime "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 620
    .end local v0    # "e":Ljava/util/UnknownFormatConversionException;
    .end local v4    # "index":I
    .restart local v5    # "index":I
    :catch_1
    move-exception v0

    move v4, v5

    .end local v5    # "index":I
    .restart local v4    # "index":I
    goto :goto_2

    :cond_4
    move v5, v4

    .end local v4    # "index":I
    .restart local v5    # "index":I
    goto :goto_0
.end method

.method public static updateDBinKNOX(Landroid/content/Context;JLjava/lang/String;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1121
    if-nez p3, :cond_1

    .line 1142
    :cond_0
    :goto_0
    return v4

    .line 1125
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1126
    .local v2, "tempFile":Ljava/io/File;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExistFile(Ljava/io/File;)Z

    move-result v6

    sget-boolean v7, Lcom/sec/android/app/voicenote/common/util/VNUtil;->FUNC_RETURN_CORRECT:Z

    if-eq v6, v7, :cond_0

    .line 1129
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1130
    .local v1, "strBuilder":Ljava/lang/StringBuilder;
    const-string v6, "_id"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131
    const/16 v6, 0x3d

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1132
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1133
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1134
    .local v3, "where":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v3, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1135
    .local v0, "result":I
    if-lez v0, :cond_0

    .line 1136
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->isGateEnabled()Z

    move-result v4

    if-ne v4, v5, :cond_2

    .line 1137
    const-string v4, "GATE"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "<GATE-M> AUDIO_DELETED: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " </GATE-M>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 1139
    :cond_2
    invoke-static {p0, p3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->sendFileDeleteBroadcast(Landroid/content/Context;Ljava/lang/String;)V

    move v4, v5

    .line 1140
    goto :goto_0
.end method

.method public static updateLabelInMediaDB(Landroid/content/Context;I)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    .line 2292
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2293
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v4, "label_id"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2294
    const-string v3, "label_id == ? AND recordingtype == \'1\'"

    .line 2295
    .local v3, "whereString":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    .line 2299
    .local v2, "selectionArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v0, v3, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2306
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v2    # "selectionArgs":[Ljava/lang/String;
    .end local v3    # "whereString":Ljava/lang/String;
    :goto_0
    return-void

    .line 2301
    :catch_0
    move-exception v1

    .line 2302
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 2303
    const-string v4, "VNUtil"

    const-string v5, "Fail to update label"

    invoke-static {v4, v5, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
