.class Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$2;
.super Ljava/lang/Object;
.source "VNSettingLogoTextFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->maxLineOverCheck()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$2;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 296
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$2;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getLineCount()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$2;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 298
    .local v0, "text":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 299
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$2;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 300
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$2;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0

    .line 303
    .end local v0    # "text":Ljava/lang/String;
    :cond_0
    return-void
.end method
