.class Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment$2;
.super Ljava/lang/Object;
.source "VNLocationTagInfoDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I

    .prologue
    const v3, 0x7f0e001e

    const/4 v2, 0x1

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->mCheckbox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    const-string v1, "show_location_tag_info"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->dismissAllowingStateLoss()V

    .line 71
    :cond_1
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setLocationVoicerecorder(I)V

    .line 73
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isAllowedLocation()Z

    move-result v1

    if-nez v1, :cond_3

    .line 74
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 75
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 81
    .end local v0    # "i":Landroid/content/Intent;
    :cond_2
    :goto_0
    return-void

    .line 77
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;

    if-eqz v1, :cond_2

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->initializePreference()V

    goto :goto_0
.end method
