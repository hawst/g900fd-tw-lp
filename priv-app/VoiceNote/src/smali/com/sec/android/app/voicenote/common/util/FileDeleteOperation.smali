.class public Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;
.super Ljava/lang/Object;
.source "FileDeleteOperation.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/common/util/IVNOperation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;
    }
.end annotation


# static fields
.field private static final ABORT_PROGRESS:I = 0x2

.field private static final FINISH_PROGRESS:I = 0x1

.field private static final SHOW_DB_PROGRESS:I = 0x3

.field private static final SHOW_PROGRESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "FileDeleteOperation"

.field private static mInstance:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;",
            ">;"
        }
    .end annotation
.end field

.field private static mOpThread:Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;


# instance fields
.field private mAlertDialog:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

.field private mFragment:Landroid/app/Fragment;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mProgressHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mInstance:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private constructor <init>(Landroid/app/Fragment;)V
    .locals 1
    .param p1, "context"    # Landroid/app/Fragment;

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mFragment:Landroid/app/Fragment;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mAlertDialog:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 207
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$1;-><init>(Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mProgressHandler:Landroid/os/Handler;

    .line 44
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mFragment:Landroid/app/Fragment;

    .line 45
    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 22
    invoke-static {p0, p1}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->deleteFromDB(Landroid/content/Context;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;)Landroid/app/Fragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mFragment:Landroid/app/Fragment;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$300()Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mOpThread:Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mProgressHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->closeProgressDialog()V

    return-void
.end method

.method private declared-synchronized closeProgressDialog()V
    .locals 2

    .prologue
    .line 108
    monitor-enter p0

    :try_start_0
    const-string v0, "FileDeleteOperation"

    const-string v1, "DeleteFiles : closeProgressDialog"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mOpThread:Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mOpThread:Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mOpThread:Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->interrupt()V

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    :cond_1
    monitor-exit p0

    return-void

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static deleteFromDB(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "deletedIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v7, 0x0

    .line 174
    const-string v4, "FileDeleteOperation"

    const-string v5, "delete all selected files and start delete from DB"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    if-nez p1, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 182
    .local v1, "remainIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 183
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_3

    .line 184
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/16 v5, 0x64

    if-lt v4, v5, :cond_2

    .line 186
    invoke-virtual {v1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ", "

    const-string v6, " or _id="

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "["

    const-string v6, "_id="

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "]"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 188
    .local v3, "where_ids":Ljava/lang/String;
    const-string v4, "FileDeleteOperation"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "remainIds.size() = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", where : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 192
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 183
    .end local v3    # "where_ids":Ljava/lang/String;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 196
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 197
    invoke-virtual {v1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ", "

    const-string v6, " or _id="

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "["

    const-string v6, "_id="

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "]"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 199
    .restart local v3    # "where_ids":Ljava/lang/String;
    const-string v4, "FileDeleteOperation"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "where : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 203
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0
.end method

.method private doDelete(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V
    .locals 3
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p3, "callback"    # Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v1, 0x0

    .line 96
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mProgressHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mProgressHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 100
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mProgressHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p2, v1, v2, p3}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;-><init>(Ljava/util/ArrayList;Landroid/os/Handler;Landroid/content/Context;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mOpThread:Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;

    .line 103
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mOpThread:Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->start()V

    .line 105
    :cond_0
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/app/Fragment;)Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;
    .locals 4
    .param p0, "activity"    # Landroid/app/Fragment;

    .prologue
    .line 48
    const-class v2, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;

    monitor-enter v2

    if-nez p0, :cond_0

    .line 49
    const/4 v1, 0x0

    .line 60
    :goto_0
    monitor-exit v2

    return-object v1

    .line 51
    :cond_0
    :try_start_0
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mInstance:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mInstance:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    .line 52
    :cond_1
    new-instance v1, Ljava/lang/ref/WeakReference;

    new-instance v3, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;-><init>(Landroid/app/Fragment;)V

    invoke-direct {v1, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mInstance:Ljava/lang/ref/WeakReference;

    .line 55
    :cond_2
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mInstance:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;

    .line 56
    .local v0, "fdo":Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;
    iget-object v1, v0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v1, p0}, Landroid/app/Fragment;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 57
    iput-object p0, v0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mFragment:Landroid/app/Fragment;

    .line 60
    :cond_3
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mInstance:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 48
    .end local v0    # "fdo":Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method


# virtual methods
.method public askDelete(Landroid/app/Fragment;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "fragment"    # Landroid/app/Fragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 72
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mAlertDialog:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    .line 73
    invoke-virtual {p1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasTagData(Landroid/content/Context;Ljava/util/ArrayList;)Z

    move-result v0

    .line 74
    .local v0, "hasData":Z
    if-eqz v0, :cond_0

    .line 75
    const v1, 0x7f0b005d

    const v2, 0x7f0b0068

    invoke-static {v1, v2, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(III)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mAlertDialog:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    .line 85
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mAlertDialog:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    invoke-virtual {p1}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_FILE"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 86
    return-void

    .line 77
    :cond_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v5, :cond_1

    .line 78
    const v1, 0x7f0b0060

    const v2, 0x7f0b005f

    invoke-static {v1, v2, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(III)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mAlertDialog:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    goto :goto_0

    .line 81
    :cond_1
    const v1, 0x7f0b0063

    const v2, 0x7f0b0062

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v1, v2, v3, v5, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(IIIZI)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mAlertDialog:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    goto :goto_0
.end method

.method public askOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)Ljava/lang/ref/WeakReference;
    .locals 2
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p3, "callback"    # Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/DialogFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->askDelete(Landroid/app/Fragment;Ljava/util/ArrayList;)V

    .line 68
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->mAlertDialog:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public startOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V
    .locals 0
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p3, "callback"    # Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 91
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->doDelete(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V

    .line 92
    return-void
.end method
