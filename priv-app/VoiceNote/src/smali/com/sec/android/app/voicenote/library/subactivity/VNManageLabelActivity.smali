.class public Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;
.super Landroid/app/ListActivity;
.source "VNManageLabelActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;
.implements Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/common/util/IVNOperation;
.implements Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$ManageLabelCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;
    }
.end annotation


# static fields
.field private static final ACTIONMODE_STATE:Ljava/lang/String; = "is_actionmode"

.field private static final ADD_REQUEST:I = 0x0

.field public static final EDITED_ID:Ljava/lang/String; = "editedId"

.field public static final EDITED_TYPE:Ljava/lang/String; = "editedType"

.field public static final EDITED_TYPE_CHANGE:I = 0x2

.field public static final EDITED_TYPE_DELETE:I = 0x1

.field public static final EDITED_TYPE_MODIFY:I = 0x0

.field private static final EDIT_REQUEST:I = 0x1

.field private static final TAG:Ljava/lang/String; = "VNManageLabelActivity"


# instance fields
.field private isActionMode:Z

.field private isShownPopupMenu:Z

.field private mActionMode:Landroid/view/ActionMode;

.field private mActionModeListener:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

.field private mActionTitle:Landroid/widget/TextView;

.field private mCursor:Landroid/database/Cursor;

.field private mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

.field private mLastPressedMenuKey:J

.field private mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mNothingCheckedItem:Z

.field private mPopupMenu:Landroid/widget/PopupMenu;

.field private mPopupMenuDismissListener:Landroid/widget/PopupMenu$OnDismissListener;

.field private mRenameMenuEnable:Z

.field mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

.field private mcurrentID:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 70
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 75
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 76
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    .line 77
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    .line 78
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    .line 81
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    .line 82
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    .line 90
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;

    .line 91
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionModeListener:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    .line 93
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isActionMode:Z

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mNothingCheckedItem:Z

    .line 95
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mRenameMenuEnable:Z

    .line 96
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionTitle:Landroid/widget/TextView;

    .line 744
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mLastPressedMenuKey:J

    .line 774
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 775
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isShownPopupMenu:Z

    .line 815
    new-instance v0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$2;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mPopupMenuDismissListener:Landroid/widget/PopupMenu$OnDismissListener;

    return-void
.end method

.method private RefreshList()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 393
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 394
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 395
    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    .line 397
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 398
    const/4 v0, 0x0

    .line 399
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getListQueryfromSecondRecord()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 400
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getDB()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    .line 401
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 402
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 403
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    .line 405
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->invalidateOptionsMenu()V

    .line 406
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionModeListener:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Landroid/view/ActionMode;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;
    .param p1, "x1"    # Landroid/view/ActionMode;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isShownPopupMenu:Z

    return p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isActionMode:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    .prologue
    .line 70
    iget v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;
    .param p1, "x1"    # I

    .prologue
    .line 70
    iput p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/common/util/LabelHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->listBinding()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mNothingCheckedItem:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mNothingCheckedItem:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mRenameMenuEnable:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mRenameMenuEnable:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->startActivityForResult(ILjava/lang/String;)V

    return-void
.end method

.method private initViews()V
    .locals 2

    .prologue
    .line 311
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    const v1, 0x1020004

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 316
    return-void
.end method

.method private listBinding()Z
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v0, 0x0

    .line 320
    const-string v1, "VNManageLabelActivity"

    const-string v2, "listBinding E"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 323
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 324
    iput-object v12, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    .line 327
    :cond_0
    new-array v4, v11, [Ljava/lang/String;

    const-string v1, "COLOR"

    aput-object v1, v4, v0

    const-string v1, "TITLE"

    aput-object v1, v4, v10

    const/4 v1, 0x2

    const-string v2, "_id"

    aput-object v2, v4, v1

    .line 332
    .local v4, "cols":[Ljava/lang/String;
    new-array v5, v11, [I

    fill-array-data v5, :array_0

    .line 337
    .local v5, "to":[I
    const/4 v9, 0x0

    .line 338
    .local v9, "selection":Ljava/lang/String;
    const/4 v6, 0x0

    .line 339
    .local v6, "builder":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getListQueryfromSecondRecord()Ljava/lang/StringBuilder;

    move-result-object v6

    .line 340
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 342
    const/4 v3, 0x0

    .line 345
    .local v3, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getDB()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v9, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 360
    :cond_1
    :goto_0
    if-nez v3, :cond_2

    .line 361
    const-string v1, "VNManageLabelActivity"

    const-string v2, "listBinding() : cursor null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    :goto_1
    return v0

    .line 346
    :catch_0
    move-exception v7

    .line 347
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "VNManageLabelActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "listBinding - SQLiteException :"

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    if-eqz v3, :cond_1

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 349
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 350
    const/4 v3, 0x0

    goto :goto_0

    .line 352
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 353
    .local v8, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v1, "VNManageLabelActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "listBinding - UnsupportedOperationException :"

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    if-eqz v3, :cond_1

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 355
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 356
    const/4 v3, 0x0

    goto :goto_0

    .line 365
    .end local v8    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    if-nez v0, :cond_3

    .line 366
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    const v2, 0x7f030026

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 372
    :goto_2
    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    move v0, v10

    .line 374
    goto :goto_1

    .line 370
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto :goto_2

    .line 332
    :array_0
    .array-data 4
        0x7f0e0059
        0x7f0e005a
        0x7f0e005b
    .end array-data
.end method

.method private preparePopupMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 807
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method private showLibraryMenuPopup()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 778
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isActionMode:Z

    if-eqz v4, :cond_1

    .line 802
    :cond_0
    :goto_0
    return v2

    .line 781
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v4, :cond_2

    .line 782
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v4}, Landroid/widget/PopupMenu;->dismiss()V

    .line 784
    :cond_2
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isShownPopupMenu:Z

    .line 785
    const v4, 0x7f0e0104

    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 786
    .local v1, "v":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 789
    new-instance v4, Landroid/widget/PopupMenu;

    const v5, 0x800005

    invoke-direct {v4, p0, v1, v5}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 790
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    const v5, 0x7f0d0005

    invoke-virtual {v4, v5}, Landroid/widget/PopupMenu;->inflate(I)V

    .line 791
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v4}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    .line 792
    .local v0, "menu":Landroid/view/Menu;
    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->preparePopupMenu(Landroid/view/Menu;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 793
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v2, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 794
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->show()V

    .line 795
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mPopupMenuDismissListener:Landroid/widget/PopupMenu$OnDismissListener;

    invoke-virtual {v2, v4}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    .line 796
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isShownPopupMenu:Z

    move v2, v3

    .line 797
    goto :goto_0

    .line 799
    :cond_3
    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 800
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v3, v6}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 801
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v3, v6}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    goto :goto_0
.end method

.method private startActivityForResult(ILjava/lang/String;)V
    .locals 3
    .param p1, "color"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 736
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 737
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 738
    .local v0, "extra":Landroid/os/Bundle;
    const-string v2, "color"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 739
    const-string v2, "title"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 741
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 742
    return-void
.end method


# virtual methods
.method public askOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)Ljava/lang/ref/WeakReference;
    .locals 5
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p3, "callback"    # Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/DialogFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const v4, 0x7f0b005d

    const/4 v3, 0x2

    .line 719
    const/4 v0, 0x0

    .line 720
    .local v0, "dialog":Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 721
    const v1, 0x7f0b0064

    invoke-static {v4, v1, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(III)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    move-result-object v0

    .line 725
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 727
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    return-object v1

    .line 723
    :cond_0
    const v1, 0x7f0b0065

    invoke-static {v4, v1, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(III)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    move-result-object v0

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const-wide/16 v10, 0x0

    const/16 v7, 0x52

    .line 747
    const-string v3, "VNManageLabelActivity"

    const-string v6, "dispatchKeyEvent()"

    invoke-static {v3, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    .line 749
    .local v2, "keycode":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 750
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v1

    .line 751
    .local v1, "flags":I
    and-int/lit8 v3, v1, 0x20

    const/16 v6, 0x20

    if-ne v3, v6, :cond_0

    .line 752
    const-string v3, "VNManageLabelActivity"

    const-string v6, "menu canceled"

    invoke-static {v3, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    iput-wide v10, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mLastPressedMenuKey:J

    .line 754
    invoke-super {p0, p1}, Landroid/app/ListActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v3

    .line 771
    :goto_0
    return v3

    .line 756
    :cond_0
    if-ne v2, v7, :cond_2

    if-nez v0, :cond_2

    .line 757
    const-string v3, "VNManageLabelActivity"

    const-string v6, "menu down"

    invoke-static {v3, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mLastPressedMenuKey:J

    .line 771
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/app/ListActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v3

    goto :goto_0

    .line 759
    :cond_2
    if-ne v2, v7, :cond_1

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    .line 760
    const-string v3, "VNManageLabelActivity"

    const-string v6, "menu up"

    invoke-static {v3, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 762
    .local v4, "uptime":J
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v3

    int-to-long v6, v3

    iget-wide v8, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mLastPressedMenuKey:J

    sub-long v8, v4, v8

    cmp-long v3, v6, v8

    if-ltz v3, :cond_3

    .line 763
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isShownPopupMenu:Z

    if-eqz v3, :cond_4

    .line 764
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isShownPopupMenu:Z

    .line 769
    :cond_3
    :goto_2
    iput-wide v10, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mLastPressedMenuKey:J

    goto :goto_1

    .line 766
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->showLibraryMenuPopup()Z

    goto :goto_2
.end method

.method public isItemChecked(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x0

    .line 491
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;

    if-nez v1, :cond_1

    .line 497
    :cond_0
    :goto_0
    return v0

    .line 494
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getChoiceMode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 10
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 256
    invoke-super {p0, p1, p2, p3}, Landroid/app/ListActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 258
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    if-nez v2, :cond_1

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 264
    :pswitch_0
    if-ne p2, v9, :cond_0

    .line 265
    const-string v2, "color"

    invoke-virtual {p3, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 266
    .local v0, "color":I
    const-string v2, "title"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 268
    .local v1, "title":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 269
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%d"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->insertColumn(Ljava/lang/String;Ljava/lang/String;)J

    .line 270
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->LoadLabeltoMap()V

    .line 271
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    .line 273
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->RefreshList()V

    goto :goto_0

    .line 278
    .end local v0    # "color":I
    .end local v1    # "title":Ljava/lang/String;
    :pswitch_1
    if-ne p2, v9, :cond_0

    iget v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    if-ltz v2, :cond_0

    .line 279
    const-string v2, "color"

    invoke-virtual {p3, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 280
    .restart local v0    # "color":I
    const-string v2, "title"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 281
    .restart local v1    # "title":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    iget v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelTitle(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    iget v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelTitle(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "category_label_title"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 282
    const-string v2, "category_label_title"

    invoke-static {v2, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const-string v2, "category_label_color"

    invoke-static {v2, v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 285
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 286
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    iget v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%d"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->updateColumn(ILjava/lang/String;Ljava/lang/String;)Z

    .line 287
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    .line 289
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->RefreshList()V

    .line 290
    const-string v2, "editedId"

    iget v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 291
    const-string v2, "editedType"

    invoke-virtual {p3, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 292
    invoke-virtual {p0, v9, p3}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->setResult(ILandroid/content/Intent;)V

    .line 293
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->LoadLabeltoMap()V

    .line 295
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$1;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 262
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onAlertDialogPositiveBtn(Z)V
    .locals 12
    .param p1, "positive"    # Z

    .prologue
    .line 413
    const/4 v10, 0x1

    if-ne p1, v10, :cond_4

    .line 414
    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v10}, Landroid/widget/ListView;->getCheckedItemIds()[J

    move-result-object v0

    .line 415
    .local v0, "arr":[J
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 417
    .local v1, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    if-eqz v0, :cond_0

    array-length v10, v0

    if-lez v10, :cond_0

    .line 418
    array-length v9, v0

    .line 420
    .local v9, "size":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v9, :cond_1

    .line 421
    aget-wide v6, v0, v4

    .line 422
    .local v6, "id":J
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 425
    .end local v4    # "i":I
    .end local v6    # "id":J
    .end local v9    # "size":I
    :cond_0
    iget v10, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    int-to-long v10, v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 428
    :cond_1
    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;

    if-eqz v10, :cond_5

    .line 429
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 430
    .restart local v9    # "size":I
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_1
    if-ge v4, v9, :cond_3

    .line 431
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->intValue()I

    move-result v10

    invoke-virtual {v11, v10}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelTitle(I)Ljava/lang/String;

    move-result-object v3

    .line 432
    .local v3, "getLabel":Ljava/lang/String;
    const-string v10, "category_label_title"

    invoke-static {v10}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 434
    .local v8, "setLabel":Ljava/lang/String;
    if-eqz v3, :cond_2

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 435
    const-string v10, "category_label_title"

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    const-string v10, "category_label_color"

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const-string v10, "category_label_position"

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 439
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 440
    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->intValue()I

    move-result v10

    invoke-virtual {v11, v10}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->deleteColumn(I)Z

    .line 441
    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    .line 442
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->intValue()I

    move-result v10

    invoke-static {p0, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->updateLabelInMediaDB(Landroid/content/Context;I)V

    .line 430
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 444
    .end local v3    # "getLabel":Ljava/lang/String;
    .end local v8    # "setLabel":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->invalidateOptionsMenu()V

    .line 445
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->RefreshList()V

    .line 447
    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v10}, Landroid/view/ActionMode;->finish()V

    .line 467
    .end local v4    # "i":I
    .end local v9    # "size":I
    :goto_2
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 468
    .local v2, "data":Landroid/content/Intent;
    const-string v10, "editedId"

    iget v11, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    invoke-virtual {v2, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 469
    const-string v10, "editedType"

    const/4 v11, 0x1

    invoke-virtual {v2, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 470
    const/4 v10, -0x1

    invoke-virtual {p0, v10, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->setResult(ILandroid/content/Intent;)V

    .line 472
    .end local v0    # "arr":[J
    .end local v1    # "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v2    # "data":Landroid/content/Intent;
    :cond_4
    return-void

    .line 451
    .restart local v0    # "arr":[J
    .restart local v1    # "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_5
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    iget v11, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    invoke-virtual {v10, v11}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelTitle(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "category_label_title"

    invoke-static {v11}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 453
    const-string v10, "category_label_title"

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    const-string v10, "category_label_color"

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    const-string v10, "category_label_position"

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 460
    :cond_6
    :goto_3
    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 461
    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    iget v11, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    invoke-virtual {v10, v11}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->deleteColumn(I)Z

    .line 462
    iget v10, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    invoke-static {p0, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->updateLabelInMediaDB(Landroid/content/Context;I)V

    .line 463
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->RefreshList()V

    .line 464
    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    goto :goto_2

    .line 457
    :catch_0
    move-exception v5

    .line 458
    .local v5, "npe":Ljava/lang/NullPointerException;
    const-string v10, "VNManageLabelActivity"

    const-string v11, "NullPointerException occured : Compare two String value"

    invoke-static {v10, v11, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 100
    const-string v0, "VNManageLabelActivity"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 102
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->finish()V

    .line 105
    :cond_0
    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->setContentView(I)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-nez v0, :cond_1

    .line 107
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    .line 109
    :cond_1
    if-eqz p1, :cond_2

    .line 110
    const-string v0, "mcurrentID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    .line 112
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show(Ljava/lang/String;)V

    .line 115
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->LoadLabeltoMap()V

    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->initViews()V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 120
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->listBinding()Z

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    .line 122
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 135
    const-string v0, "VNManageLabelActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 138
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mCursor:Landroid/database/Cursor;

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    if-eqz v0, :cond_1

    .line 141
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 143
    :cond_1
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 144
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 476
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string v0, "VNManageLabelActivity"

    const-string v1, "onItemClick()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    .line 478
    const-string v0, "VNManageLabelActivity"

    const-string v1, "onItemClick()2"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    :goto_0
    return-void

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    invoke-virtual {v0, p3, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 485
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 486
    const v0, 0x8000

    invoke-virtual {p2, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v0, 0x1

    .line 379
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;

    if-eqz v1, :cond_0

    .line 380
    const/4 v0, 0x0

    .line 388
    :goto_0
    return v0

    .line 383
    :cond_0
    iput p3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    .line 384
    new-instance v1, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    invoke-direct {v1, p0, p3}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;I)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionModeListener:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    .line 385
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionModeListener:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;

    .line 386
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isActionMode:Z

    .line 387
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v1, v0, v0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->setSelectionMode(ZZ)V

    goto :goto_0
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 812
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, -0x1

    const/4 v0, 0x1

    .line 221
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 249
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 251
    :goto_0
    return v0

    .line 223
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->finish()V

    goto :goto_0

    .line 227
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 231
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->showLibraryMenuPopup()Z

    goto :goto_0

    .line 235
    :sswitch_3
    new-instance v1, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    invoke-direct {v1, p0, v3, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;II)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionModeListener:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionModeListener:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;

    .line 237
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isActionMode:Z

    .line 238
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v1, v0, v0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->setSelectionMode(ZZ)V

    goto :goto_0

    .line 242
    :sswitch_4
    new-instance v1, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    const/4 v2, 0x3

    invoke-direct {v1, p0, v3, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;II)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionModeListener:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    .line 243
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionModeListener:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;

    .line 244
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isActionMode:Z

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v1, v0, v0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->setSelectionMode(ZZ)V

    goto :goto_0

    .line 221
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0e0103 -> :sswitch_1
        0x7f0e0104 -> :sswitch_2
        0x7f0e0105 -> :sswitch_3
        0x7f0e0106 -> :sswitch_4
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 208
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0d0004

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 212
    const v0, 0x7f0e0104

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 215
    :cond_0
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 167
    if-eqz p1, :cond_1

    .line 169
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    if-eqz v1, :cond_0

    .line 170
    const-string v1, "listview"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 171
    .local v0, "state":Landroid/os/Parcelable;
    if-eqz v0, :cond_0

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 176
    .end local v0    # "state":Landroid/os/Parcelable;
    :cond_0
    const-string v1, "is_actionmode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isActionMode:Z

    .line 177
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isActionMode:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 178
    new-instance v1, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    const/4 v2, -0x1

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;I)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionModeListener:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionModeListener:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    const-string v2, "mmenumode"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->setMenuMode(I)V

    .line 180
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionModeListener:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->setSelectionMode(ZZ)V

    .line 192
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 193
    return-void

    .line 183
    :cond_2
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isActionMode:Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 126
    const-string v0, "VNManageLabelActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 131
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 197
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 198
    .local v0, "state":Landroid/os/Parcelable;
    const-string v1, "listview"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 199
    const-string v1, "is_actionmode"

    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isActionMode:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 200
    const-string v1, "mcurrentID"

    iget v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionModeListener:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    if-eqz v1, :cond_0

    .line 202
    const-string v1, "mmenumode"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionModeListener:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->getMenuMode()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 203
    :cond_0
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 204
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 148
    const-string v0, "VNManageLabelActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTrimMemory() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    sparse-switch p1, :sswitch_data_0

    .line 162
    :sswitch_0
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onTrimMemory(I)V

    .line 163
    return-void

    .line 149
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method public startOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V
    .locals 0
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p3, "callback"    # Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 733
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    return-void
.end method
