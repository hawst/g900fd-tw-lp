.class public Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNNFCEnableDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$Callbacks;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VNNFCEnableDialogFragment"


# instance fields
.field private mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$Callbacks;

.field private mCheckbox:Landroid/widget/CheckBox;

.field private mNFCTaggingLabelInfo:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$Callbacks;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->mNFCTaggingLabelInfo:Ljava/lang/String;

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->startNFCWritingActivity()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;
    .locals 3
    .param p0, "labelinfo"    # Ljava/lang/String;

    .prologue
    .line 52
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;-><init>()V

    .line 53
    .local v1, "dialogFragment":Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 54
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "labelinfo"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 57
    return-object v1
.end method

.method private startNFCWritingActivity()V
    .locals 3

    .prologue
    .line 116
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 117
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 118
    const-string v1, "labelinfo"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->mNFCTaggingLabelInfo:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->mNFCTaggingLabelInfo:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setNFCfilename(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->startActivity(Landroid/content/Intent;)V

    .line 121
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 66
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$Callbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$Callbacks;

    .line 67
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 68
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 72
    const-string v3, "VNNFCEnableDialogFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCreate "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "labelinfo"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->mNFCTaggingLabelInfo:Ljava/lang/String;

    .line 75
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 76
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f03005f

    invoke-virtual {v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 77
    .local v1, "layout":Landroid/view/View;
    const v3, 0x7f0e00c3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 78
    .local v2, "warningText":Landroid/widget/TextView;
    const v3, 0x7f0e00ef

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 80
    const v3, 0x7f0b0016

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 81
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 82
    const v3, 0x7f0b00ce

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 84
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    const v3, 0x104000a

    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 104
    const/high16 v3, 0x1040000

    invoke-virtual {v0, v3, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 105
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 110
    const-string v0, "VNNFCEnableDialogFragment"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    const v1, 0x7f0b006f

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 112
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 113
    return-void
.end method
