.class public Lcom/sec/android/app/voicenote/common/util/VNLocationManager;
.super Ljava/lang/Object;
.source "VNLocationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/util/VNLocationManager$GeocoderThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VNLocationManager"

.field private static locationListener:Landroid/location/LocationListener;

.field private static mAddrTag:Ljava/lang/String;

.field public static final mConstraintLetter:[Ljava/lang/String;

.field private static mContext:Landroid/content/Context;

.field private static mLatitude:D

.field private static mLocationManager:Landroid/location/LocationManager;

.field private static mLocationTag:Ljava/lang/String;

.field private static mLongitude:D


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 39
    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mContext:Landroid/content/Context;

    .line 40
    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationTag:Ljava/lang/String;

    .line 41
    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mAddrTag:Ljava/lang/String;

    .line 42
    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationManager:Landroid/location/LocationManager;

    .line 43
    sput-wide v2, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLatitude:D

    sput-wide v2, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLongitude:D

    .line 44
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, " "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "|"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "\\"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "<"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mConstraintLetter:[Ljava/lang/String;

    .line 199
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager$1;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->locationListener:Landroid/location/LocationListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    return-void
.end method

.method static synthetic access$000()Landroid/content/Context;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$102(D)D
    .locals 0
    .param p0, "x0"    # D

    .prologue
    .line 36
    sput-wide p0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLatitude:D

    return-wide p0
.end method

.method static synthetic access$202(D)D
    .locals 0
    .param p0, "x0"    # D

    .prologue
    .line 36
    sput-wide p0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLongitude:D

    return-wide p0
.end method

.method static synthetic access$302(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 36
    sput-object p0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mAddrTag:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$402(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 36
    sput-object p0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationTag:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500()V
    .locals 0

    .prologue
    .line 36
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->removeUpdate()V

    return-void
.end method

.method public static getAddrTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 128
    const-string v0, "location_tag"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;Z)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 129
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mAddrTag:Ljava/lang/String;

    .line 131
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getLatitude()D
    .locals 2

    .prologue
    .line 112
    const-string v0, "location_tag"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;Z)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 113
    sget-wide v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLatitude:D

    .line 115
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static getLocationTag()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 98
    const-string v4, "contextual_filename"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "location_tag"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_1

    .line 100
    :cond_0
    const/4 v4, 0x0

    .line 108
    .local v0, "arr$":[Ljava/lang/String;
    .local v2, "i$":I
    .local v3, "len$":I
    :goto_0
    return-object v4

    .line 103
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_1
    sget-object v4, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationTag:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 104
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mConstraintLetter:[Ljava/lang/String;

    .restart local v0    # "arr$":[Ljava/lang/String;
    array-length v3, v0

    .restart local v3    # "len$":I
    const/4 v2, 0x0

    .restart local v2    # "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 105
    .local v1, "cl":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationTag:Ljava/lang/String;

    const-string v5, "_"

    invoke-virtual {v4, v1, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationTag:Ljava/lang/String;

    .line 104
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 108
    .end local v1    # "cl":Ljava/lang/String;
    :cond_2
    sget-object v4, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationTag:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getLongitude()D
    .locals 2

    .prologue
    .line 120
    const-string v0, "location_tag"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;Z)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 121
    sget-wide v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLongitude:D

    .line 123
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static getTimeTag()Ljava/lang/String;
    .locals 4

    .prologue
    .line 85
    const-string v1, "contextual_filename"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mContext:Landroid/content/Context;

    if-nez v1, :cond_2

    .line 86
    :cond_0
    const/4 v0, 0x0

    .line 94
    .local v0, "timeTag":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v0

    .line 89
    .end local v0    # "timeTag":Ljava/lang/String;
    :cond_2
    const-string v1, "yyyyMMdd_HHmmss"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 91
    .restart local v0    # "timeTag":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 92
    const-string v1, "/"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "."

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " "

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "-"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static releaseLocationTag()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 136
    sput-object v2, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationTag:Ljava/lang/String;

    .line 137
    sput-object v2, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mAddrTag:Ljava/lang/String;

    .line 138
    sput-wide v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLatitude:D

    .line 139
    sput-wide v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLongitude:D

    .line 140
    return-void
.end method

.method private static removeUpdate()V
    .locals 2

    .prologue
    .line 77
    const-string v0, "VNLocationManager"

    const-string v1, "removeUpdate"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 80
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationManager:Landroid/location/LocationManager;

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->locationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 82
    :cond_0
    return-void
.end method

.method private static requestLocationUpdates()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x3e8

    const/4 v4, 0x0

    .line 61
    const-string v0, "VNLocationManager"

    const-string v1, "requestLocationUpdates()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mContext:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationManager:Landroid/location/LocationManager;

    .line 65
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    sget-object v5, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->locationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 67
    const-string v0, "VNLocationManager"

    const-string v1, "privider:NEWTORK_PROVIDER"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :goto_0
    return-void

    .line 68
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    sget-object v5, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->locationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 70
    const-string v0, "VNLocationManager"

    const-string v1, "provider:GPS_PROVIDER"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 72
    :cond_1
    const-string v0, "VNLocationManager"

    const-string v1, "provider:No enabled provider"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static start(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 49
    sput-object p0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mContext:Landroid/content/Context;

    .line 50
    const-string v0, "location_tag"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "recording_quality"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 52
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->requestLocationUpdates()V

    .line 54
    :cond_0
    return-void
.end method

.method public static stop()V
    .locals 0

    .prologue
    .line 57
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->removeUpdate()V

    .line 58
    return-void
.end method
