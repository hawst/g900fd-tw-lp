.class public Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNProgressDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment$Callbacks;
    }
.end annotation


# instance fields
.field private mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment$Callbacks;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment$Callbacks;

    .line 43
    return-void
.end method

.method public static setArguments(II)Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;
    .locals 3
    .param p0, "title"    # I
    .param p1, "message"    # I

    .prologue
    .line 33
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;-><init>()V

    .line 34
    .local v1, "dialogFragment":Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 35
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 36
    const-string v2, "message"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 37
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 38
    return-object v1
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 63
    instance-of v0, p1, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment$Callbacks;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 64
    check-cast v0, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment$Callbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment$Callbacks;

    .line 66
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 67
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "title"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 52
    .local v2, "title":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "message"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 53
    .local v0, "message":I
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 54
    .local v1, "progressDialog":Landroid/app/ProgressDialog;
    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 55
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNProgressDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 56
    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 57
    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 58
    return-object v1
.end method
