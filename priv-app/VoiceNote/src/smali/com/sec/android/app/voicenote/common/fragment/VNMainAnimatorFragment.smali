.class public Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;
.super Landroid/app/Fragment;
.source "VNMainAnimatorFragment.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VNMainAnimatorFragment"


# instance fields
.field private mFilename:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->mFilename:Landroid/widget/TextView;

    .line 50
    return-void
.end method

.method public static newInstance(Z)Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;
    .locals 3
    .param p0, "fromMMS"    # Z

    .prologue
    .line 53
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;-><init>()V

    .line 54
    .local v1, "f":Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 55
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "fromMMS"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 56
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->setArguments(Landroid/os/Bundle;)V

    .line 57
    return-object v1
.end method

.method private updateFilename(Ljava/lang/String;I)V
    .locals 6
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "color"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020019

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 174
    .local v0, "caetgory":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->mFilename:Landroid/widget/TextView;

    if-nez v1, :cond_2

    .line 175
    :cond_0
    const-string v1, "VNMainAnimatorFragment"

    const-string v2, "updateFilename : return null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_1
    :goto_0
    return-void

    .line 178
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09002c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v5, v5, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 179
    if-nez p2, :cond_3

    .line 180
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08001b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 184
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->mFilename:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->mFilename:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 182
    :cond_3
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p2, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_1
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 24
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    const-string v20, "VNMainAnimatorFragment"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "onCreate "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const/4 v6, 0x0

    .line 65
    .local v6, "fromMMS":Z
    const-string v20, "record_mode"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v14

    .line 66
    .local v14, "record_mode":I
    const v15, 0x7f030038

    .line 67
    .local v15, "resource":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    .line 68
    .local v4, "args":Landroid/os/Bundle;
    if-eqz v4, :cond_0

    .line 69
    const-string v20, "fromMMS"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 72
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 73
    const v15, 0x7f030036

    .line 96
    :goto_0
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v15, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v19

    .line 97
    .local v19, "view":Landroid/view/View;
    const v20, 0x7f0e00d8

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 98
    .local v17, "time":Landroid/widget/TextView;
    const v20, 0x7f0e00c8

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->mFilename:Landroid/widget/TextView;

    .line 99
    const v20, 0x7f0e00d9

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 100
    .local v10, "mRecIconOn":Landroid/view/View;
    const v20, 0x7f0e0012

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 101
    .local v9, "logoView":Landroid/view/View;
    const v20, 0x7f0e0041

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 102
    .local v8, "logoText":Landroid/widget/TextView;
    const v20, 0x7f0e0040

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 104
    .local v7, "logoBG":Landroid/widget/ImageView;
    if-eqz v17, :cond_1

    .line 105
    const-string v20, "%02d:%02d:%02d"

    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->mFilename:Landroid/widget/TextView;

    move-object/from16 v20, v0

    if-eqz v20, :cond_2

    .line 108
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->createNewFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v20

    const-string v21, "category_label_color"

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->updateFilename(Ljava/lang/String;I)V

    .line 110
    :cond_2
    if-eqz v10, :cond_3

    .line 111
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/view/View;->setVisibility(I)V

    .line 113
    :cond_3
    const-string v20, "logo"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_8

    const/16 v20, 0x3

    move/from16 v0, v20

    if-eq v14, v0, :cond_8

    .line 117
    const-string v20, "logo_type"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v20

    packed-switch v20, :pswitch_data_0

    .line 165
    :cond_4
    :goto_1
    return-object v19

    .line 74
    .end local v7    # "logoBG":Landroid/widget/ImageView;
    .end local v8    # "logoText":Landroid/widget/TextView;
    .end local v9    # "logoView":Landroid/view/View;
    .end local v10    # "mRecIconOn":Landroid/view/View;
    .end local v17    # "time":Landroid/widget/TextView;
    .end local v19    # "view":Landroid/view/View;
    :cond_5
    if-nez v6, :cond_6

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v20

    if-eqz v20, :cond_7

    .line 75
    :cond_6
    const v15, 0x7f030038

    goto/16 :goto_0

    .line 77
    :cond_7
    packed-switch v14, :pswitch_data_1

    goto/16 :goto_0

    .line 79
    :pswitch_0
    const v15, 0x7f030038

    .line 80
    goto/16 :goto_0

    .line 83
    :pswitch_1
    const v15, 0x7f030037

    .line 84
    goto/16 :goto_0

    .line 87
    :pswitch_2
    const v15, 0x7f030035

    .line 88
    goto/16 :goto_0

    .line 91
    :pswitch_3
    const v15, 0x7f030039

    goto/16 :goto_0

    .line 119
    .restart local v7    # "logoBG":Landroid/widget/ImageView;
    .restart local v8    # "logoText":Landroid/widget/TextView;
    .restart local v9    # "logoView":Landroid/view/View;
    .restart local v10    # "mRecIconOn":Landroid/view/View;
    .restart local v17    # "time":Landroid/widget/TextView;
    .restart local v19    # "view":Landroid/view/View;
    :pswitch_4
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 123
    :pswitch_5
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    const-string v20, "logo_filepath"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    if-eqz v20, :cond_4

    .line 125
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5}, Landroid/graphics/Canvas;-><init>()V

    .line 126
    .local v5, "canvas":Landroid/graphics/Canvas;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/app/Activity;->getFilesDir()Ljava/io/File;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "logo_image.jpg"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 127
    .local v11, "mainImage":Landroid/graphics/Bitmap;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/fragment/VNMainAnimatorFragment;->getActivity()Landroid/app/Activity;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0201a6

    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 128
    .local v18, "tmpMask":Landroid/graphics/Bitmap;
    if-eqz v11, :cond_4

    if-eqz v18, :cond_4

    .line 131
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    const/16 v22, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 132
    .local v12, "mask":Landroid/graphics/Bitmap;
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->recycle()V

    .line 133
    const/16 v18, 0x0

    .line 135
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    sget-object v22, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v20 .. v22}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 136
    .local v16, "result":Landroid/graphics/Bitmap;
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 137
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    .line 138
    .local v13, "paint":Landroid/graphics/Paint;
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 140
    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v11, v0, v1, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 141
    new-instance v20, Landroid/graphics/PorterDuffXfermode;

    sget-object v21, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct/range {v20 .. v21}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 142
    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v12, v0, v1, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 143
    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 145
    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    .line 150
    .end local v5    # "canvas":Landroid/graphics/Canvas;
    .end local v11    # "mainImage":Landroid/graphics/Bitmap;
    .end local v12    # "mask":Landroid/graphics/Bitmap;
    .end local v13    # "paint":Landroid/graphics/Paint;
    .end local v16    # "result":Landroid/graphics/Bitmap;
    .end local v18    # "tmpMask":Landroid/graphics/Bitmap;
    :pswitch_6
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 151
    const-string v20, "logo_text"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    const-string v20, "logo_text_font_typeface"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getTextFont(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 153
    const-string v20, "logo_text_color"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 154
    const-string v20, "logo_bg_color"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 155
    const-string v20, "logo_bg_color"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v20

    const v21, 0x106000d

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_4

    .line 156
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 160
    :cond_8
    if-eqz v9, :cond_4

    .line 161
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 117
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 77
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public updateMic(Z)V
    .locals 3
    .param p1, "isRecord"    # Z

    .prologue
    .line 169
    const-string v0, "VNMainAnimatorFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateMic "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    return-void
.end method
