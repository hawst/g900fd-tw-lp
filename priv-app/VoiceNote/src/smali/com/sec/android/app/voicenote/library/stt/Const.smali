.class public interface abstract Lcom/sec/android/app/voicenote/library/stt/Const;
.super Ljava/lang/Object;
.source "Const.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;
    }
.end annotation


# static fields
.field public static final COMPLETED_ACTION:Ljava/lang/String; = "android.intent.action.VOICE_CONVERTING_COMPLETED"

.field public static final FILE_SCHEME:Ljava/lang/String; = "file://"

.field public static final FINISHED_ACTION:Ljava/lang/String; = "android.intent.action.VOICE_CONVERTING_FINISHED"

.field public static final HOST_BR:Ljava/lang/String; = "pt-br"

.field public static final HOST_CH:Ljava/lang/String; = "cn-ma"

.field public static final HOST_DE:Ljava/lang/String; = "de-de"

.field public static final HOST_ESP:Ljava/lang/String; = "es-es"

.field public static final HOST_FR:Ljava/lang/String; = "fr-fr"

.field public static final HOST_GB:Ljava/lang/String; = "en-gb"

.field public static final HOST_IT:Ljava/lang/String; = "it-it"

.field public static final HOST_JP:Ljava/lang/String; = "jp-jp"

.field public static final HOST_KO:Ljava/lang/String; = "ko-kr"

.field public static final HOST_PREFIX:Ljava/lang/String; = "eo.nvc."

.field public static final HOST_RU:Ljava/lang/String; = "ru-ru"

.field public static final HOST_SUFFIX:Ljava/lang/String; = ".nuancemobility.net"

.field public static final HOST_US:Ljava/lang/String; = "en-us"

.field public static final HOST_XLA:Ljava/lang/String; = "es-us"

.field public static final KEY_DATA_STATE:Ljava/lang/String; = "dataState"

.field public static final KEY_LANGUAGE:Ljava/lang/String; = "language"

.field public static final LANGUAGE_BR:Ljava/lang/String; = "por-BRA"

.field public static final LANGUAGE_CH:Ljava/lang/String; = "cmn-CHN"

.field public static final LANGUAGE_DE:Ljava/lang/String; = "deu-DEU"

.field public static final LANGUAGE_ESP:Ljava/lang/String; = "spa-ESP"

.field public static final LANGUAGE_FR:Ljava/lang/String; = "fra-FRA"

.field public static final LANGUAGE_GB:Ljava/lang/String; = "eng-GBR"

.field public static final LANGUAGE_IT:Ljava/lang/String; = "ita-ITA"

.field public static final LANGUAGE_JP:Ljava/lang/String; = "jpn-JPN"

.field public static final LANGUAGE_KO:Ljava/lang/String; = "kor-KOR"

.field public static final LANGUAGE_RU:Ljava/lang/String; = "rus-RUS"

.field public static final LANGUAGE_US:Ljava/lang/String; = "eng-USA"

.field public static final LANGUAGE_XLA:Ljava/lang/String; = "spa-XLA"

.field public static final M4A_EXTENSION:Ljava/lang/String; = ".m4a"

.field public static final OUTPUT_PATH:Ljava/lang/String; = "/.output/"

.field public static final PCM_EXTENSION:Ljava/lang/String; = ".pcm"

.field public static final TEXT_EXTENSION:Ljava/lang/String; = ".txt"
