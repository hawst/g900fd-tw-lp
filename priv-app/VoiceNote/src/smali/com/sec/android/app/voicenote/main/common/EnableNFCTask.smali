.class public Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;
.super Landroid/os/AsyncTask;
.source "EnableNFCTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "EnableNFCTask"


# instance fields
.field private final STATE_CARD_MODE_ON:I

.field private fromContextMenu:Z

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contextmenu"    # Z

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->fromContextMenu:Z

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->mContext:Landroid/content/Context;

    .line 38
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->STATE_CARD_MODE_ON:I

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->mContext:Landroid/content/Context;

    .line 42
    iput-boolean p2, p0, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->fromContextMenu:Z

    .line 43
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 12
    .param p1, "params"    # [Ljava/lang/Boolean;

    .prologue
    const/4 v10, 0x0

    .line 47
    aget-object v9, p1, v10

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 48
    .local v1, "enableNFC":Z
    const/4 v8, 0x0

    .line 49
    .local v8, "success":Z
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->mContext:Landroid/content/Context;

    invoke-static {v9}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v2

    .line 51
    .local v2, "mNfcAdapter":Landroid/nfc/NfcAdapter;
    if-nez v2, :cond_0

    .line 52
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 95
    :goto_0
    return-object v9

    .line 55
    :cond_0
    const-string v9, "EnableNFCTask"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Setting NFC enabled state to: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    if-eqz v1, :cond_1

    .line 58
    :try_start_0
    sget-boolean v9, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_NFC_CARDMODE_ADDED:Z

    if-eqz v9, :cond_3

    .line 59
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 60
    .local v0, "NfcManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v9, "getAdapterState"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Class;

    invoke-virtual {v0, v9, v10}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 61
    .local v6, "setNfcGetAdapterState":Ljava/lang/reflect/Method;
    const-string v9, "readerEnable"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Class;

    invoke-virtual {v0, v9, v10}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v7

    .line 62
    .local v7, "setNfcReaderEnable":Ljava/lang/reflect/Method;
    const-string v9, "enableNdefPush"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Class;

    invoke-virtual {v0, v9, v10}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 63
    .local v4, "setNfcEnableNdefPush":Ljava/lang/reflect/Method;
    const-string v9, "enable"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Class;

    invoke-virtual {v0, v9, v10}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 64
    .local v5, "setNfcEnabled":Ljava/lang/reflect/Method;
    const/4 v9, 0x1

    invoke-virtual {v6, v9}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 65
    const/4 v9, 0x1

    invoke-virtual {v7, v9}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 66
    const/4 v9, 0x1

    invoke-virtual {v4, v9}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 67
    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 69
    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v6, v2, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 71
    .local v3, "nfcState":I
    const/4 v9, 0x5

    if-ne v3, v9, :cond_2

    .line 72
    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v7, v2, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 73
    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v4, v2, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    .end local v0    # "NfcManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "nfcState":I
    .end local v4    # "setNfcEnableNdefPush":Ljava/lang/reflect/Method;
    .end local v5    # "setNfcEnabled":Ljava/lang/reflect/Method;
    .end local v6    # "setNfcGetAdapterState":Ljava/lang/reflect/Method;
    .end local v7    # "setNfcReaderEnable":Ljava/lang/reflect/Method;
    :cond_1
    :goto_1
    if-eqz v8, :cond_4

    .line 91
    const-string v9, "EnableNFCTask"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Successfully changed NFC enabled state to "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :goto_2
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    goto/16 :goto_0

    .line 75
    .restart local v0    # "NfcManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v3    # "nfcState":I
    .restart local v4    # "setNfcEnableNdefPush":Ljava/lang/reflect/Method;
    .restart local v5    # "setNfcEnabled":Ljava/lang/reflect/Method;
    .restart local v6    # "setNfcGetAdapterState":Ljava/lang/reflect/Method;
    .restart local v7    # "setNfcReaderEnable":Ljava/lang/reflect/Method;
    :cond_2
    const/4 v9, 0x0

    :try_start_1
    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v5, v2, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    goto :goto_1

    .line 78
    .end local v0    # "NfcManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "nfcState":I
    .end local v4    # "setNfcEnableNdefPush":Ljava/lang/reflect/Method;
    .end local v5    # "setNfcEnabled":Ljava/lang/reflect/Method;
    .end local v6    # "setNfcGetAdapterState":Ljava/lang/reflect/Method;
    .end local v7    # "setNfcReaderEnable":Ljava/lang/reflect/Method;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 79
    .restart local v0    # "NfcManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v9, "enable"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Class;

    invoke-virtual {v0, v9, v10}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 80
    .restart local v5    # "setNfcEnabled":Ljava/lang/reflect/Method;
    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 81
    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v5, v2, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v8

    goto :goto_1

    .line 93
    .end local v0    # "NfcManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v5    # "setNfcEnabled":Ljava/lang/reflect/Method;
    :cond_4
    const-string v9, "EnableNFCTask"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error setting NFC enabled state to "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 87
    :catch_0
    move-exception v9

    goto :goto_1

    .line 86
    :catch_1
    move-exception v9

    goto :goto_1

    .line 85
    :catch_2
    move-exception v9

    goto :goto_1

    .line 84
    :catch_3
    move-exception v9

    goto :goto_1

    .line 83
    :catch_4
    move-exception v9

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, [Ljava/lang/Boolean;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->doInBackground([Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 100
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->fromContextMenu:Z

    if-eqz v0, :cond_0

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0005

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
