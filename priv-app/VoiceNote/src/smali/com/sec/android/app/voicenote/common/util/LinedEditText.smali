.class public Lcom/sec/android/app/voicenote/common/util/LinedEditText;
.super Landroid/widget/TextView;
.source "LinedEditText.java"


# instance fields
.field private mPaint:Landroid/graphics/Paint;

.field private mRect:Landroid/graphics/Rect;

.field private margine:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/LinedEditText;->margine:I

    .line 38
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LinedEditText;->mRect:Landroid/graphics/Rect;

    .line 39
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LinedEditText;->mPaint:Landroid/graphics/Paint;

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LinedEditText;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LinedEditText;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40800000    # 4.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LinedEditText;->mPaint:Landroid/graphics/Paint;

    const v1, -0x7f404041

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/LinedEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/LinedEditText;->margine:I

    .line 45
    const v0, -0x7d7d7e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/util/LinedEditText;->setTextColor(I)V

    .line 46
    const v0, 0x7f0b012f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/util/LinedEditText;->setText(I)V

    .line 47
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 63
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 64
    return-void
.end method
