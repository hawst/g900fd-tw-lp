.class Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment$1;
.super Ljava/lang/Object;
.source "VNFilterbyDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v3, -0x1

    const/4 v4, -0x2

    .line 120
    move-object v2, p1

    check-cast v2, Landroid/app/AlertDialog;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 121
    .local v1, "positiveButton":Landroid/widget/Button;
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;)Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;)Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 124
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 126
    :cond_0
    check-cast p1, Landroid/app/AlertDialog;

    .end local p1    # "arg0":Landroid/content/DialogInterface;
    invoke-virtual {p1, v4}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 127
    .local v0, "negativeButton":Landroid/widget/Button;
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/library/fragment/VNFilterbyDialogFragment;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    return-void
.end method
