.class Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;
.super Landroid/widget/ArrayAdapter;
.source "VNBookmarkListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BookmarkAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
        ">;"
    }
.end annotation


# instance fields
.field private mIsCheckBoxAnimating:Z

.field private mIsSelectionMode:Z

.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;Landroid/content/Context;IILjava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resource"    # I
    .param p4, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p5, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    const/4 v0, 0x0

    .line 647
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    .line 648
    invoke-direct {p0, p2, p3, p4, p5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 643
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->mIsSelectionMode:Z

    .line 644
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->mIsCheckBoxAnimating:Z

    .line 649
    return-void
.end method

.method static synthetic access$702(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 641
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->mIsCheckBoxAnimating:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    .prologue
    .line 641
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->mIsSelectionMode:Z

    return v0
.end method

.method private addCheckBox(Landroid/widget/CheckBox;)V
    .locals 2
    .param p1, "cb"    # Landroid/widget/CheckBox;

    .prologue
    .line 662
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCheckBoxes:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$600()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 663
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCheckBoxes:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$600()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0xe

    if-le v0, v1, :cond_0

    .line 664
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCheckBoxes:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$600()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 666
    :cond_0
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCheckBoxes:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$600()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 668
    :cond_1
    return-void
.end method

.method private declared-synchronized controlCheckbox(Landroid/widget/CheckBox;I)V
    .locals 7
    .param p1, "cb"    # Landroid/widget/CheckBox;
    .param p2, "position"    # I

    .prologue
    .line 671
    monitor-enter p0

    if-nez p1, :cond_0

    .line 703
    :goto_0
    monitor-exit p0

    return-void

    .line 675
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->addCheckBox(Landroid/widget/CheckBox;)V

    .line 677
    const/4 v3, 0x0

    .line 678
    .local v3, "marginRight":I
    const/4 v2, 0x0

    .line 679
    .local v2, "marginLeft":I
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->mIsCheckBoxAnimating:Z

    if-nez v4, :cond_2

    .line 681
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->mIsSelectionMode:Z

    if-eqz v4, :cond_3

    .line 682
    const/4 v3, 0x0

    .line 683
    const/4 v2, 0x0

    .line 689
    :goto_1
    invoke-virtual {p1}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 690
    .local v1, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-nez v3, :cond_1

    .line 691
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const/high16 v5, 0x41800000    # 16.0f

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->convertDPtoPX(Landroid/content/Context;FLandroid/util/DisplayMetrics;)I

    move-result v2

    .line 693
    :cond_1
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 694
    invoke-virtual {p1, v1}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 697
    .end local v1    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 698
    .local v0, "listview":Landroid/widget/ListView;
    if-eqz v0, :cond_4

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 699
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 671
    .end local v0    # "listview":Landroid/widget/ListView;
    .end local v2    # "marginLeft":I
    .end local v3    # "marginRight":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 685
    .restart local v2    # "marginLeft":I
    .restart local v3    # "marginRight":I
    :cond_3
    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_1
    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {p1, v4, v5}, Landroid/widget/CheckBox;->measure(II)V

    .line 687
    invoke-virtual {p1}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v4

    neg-int v3, v4

    goto :goto_1

    .line 701
    .restart local v0    # "listview":Landroid/widget/ListView;
    :cond_4
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private startCheckBoxAnimation()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 706
    const/4 v0, 0x0

    .line 707
    .local v0, "endPosition":I
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCheckBoxes:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$600()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 708
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCheckBoxes:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$600()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->getWidth()I

    move-result v0

    .line 713
    const/4 v2, 0x1

    new-array v2, v2, [I

    aput v0, v2, v3

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 714
    .local v1, "varl":Landroid/animation/ValueAnimator;
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 715
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 736
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 777
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 778
    .end local v1    # "varl":Landroid/animation/ValueAnimator;
    :cond_0
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 653
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 655
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f0e0020

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 656
    .local v0, "checkbox":Landroid/widget/CheckBox;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->controlCheckbox(Landroid/widget/CheckBox;I)V

    .line 658
    return-object v1
.end method

.method public isSelectionMode()Z
    .locals 1

    .prologue
    .line 793
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->mIsSelectionMode:Z

    return v0
.end method

.method public setSelectionMode(ZZ)V
    .locals 2
    .param p1, "isSelectionMode"    # Z
    .param p2, "needAnim"    # Z

    .prologue
    .line 781
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCheckBoxes:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->access$600()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 790
    :cond_0
    :goto_0
    return-void

    .line 785
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->mIsSelectionMode:Z

    .line 787
    if-eqz p2, :cond_0

    .line 788
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->startCheckBoxAnimation()V

    goto :goto_0
.end method
