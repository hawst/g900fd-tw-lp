.class public Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;
.super Landroid/preference/PreferenceFragment;
.source "VNSettingFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment$EventHandler;
    }
.end annotation


# static fields
.field public static final LOGO_FRAGMENT_TAG:Ljava/lang/String; = "logo_fragment"

.field private static final OPENSOURCE_PREFERENCE_CATEGORY:Ljava/lang/String; = "opensource_license_category"

.field private static final PREFERENCE_SCREEN:Ljava/lang/String; = "settings_preference_screen"

.field private static final STT_PREFERENCE_CATEGORY:Ljava/lang/String; = "voice_memo_category"

.field private static final TAG:Ljava/lang/String; = "VNSettingFragment"


# instance fields
.field private final DEFAULTNAME_RETURNCODE:I

.field private final STTWARNING_RETURNCODE:I

.field private mActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

.field private mContextualTag:Landroid/preference/CheckBoxPreference;

.field private mDefaultName:Landroid/preference/Preference;

.field private mEventHandler:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment$EventHandler;

.field private mGeneralCategory:Landroid/preference/PreferenceCategory;

.field private mLocationTag:Landroid/preference/CheckBoxPreference;

.field private mLogo:Landroid/preference/Preference;

.field private mNoiseReduction:Landroid/preference/CheckBoxPreference;

.field private mOpensourceLicense:Landroid/preference/Preference;

.field private mRecordingQuality:Landroid/preference/ListPreference;

.field private mRecordingVolume:Landroid/preference/ListPreference;

.field private mSTTLanguage:Landroid/preference/ListPreference;

.field private mSkipInterval:Landroid/preference/Preference;

.field private mStorage:Landroid/preference/ListPreference;

.field private mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->DEFAULTNAME_RETURNCODE:I

    .line 67
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->STTWARNING_RETURNCODE:I

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .line 71
    new-instance v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment$EventHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment$EventHandler;-><init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mEventHandler:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment$EventHandler;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mStorage:Landroid/preference/ListPreference;

    .line 74
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingQuality:Landroid/preference/ListPreference;

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mDefaultName:Landroid/preference/Preference;

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLocationTag:Landroid/preference/CheckBoxPreference;

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mNoiseReduction:Landroid/preference/CheckBoxPreference;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mContextualTag:Landroid/preference/CheckBoxPreference;

    .line 79
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingVolume:Landroid/preference/ListPreference;

    .line 80
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mSkipInterval:Landroid/preference/Preference;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLogo:Landroid/preference/Preference;

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mSTTLanguage:Landroid/preference/ListPreference;

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mOpensourceLicense:Landroid/preference/Preference;

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->handleMessage(Landroid/os/Message;)V

    return-void
.end method

.method private handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 418
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 454
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 421
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mStorage:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 422
    .local v0, "storagedialog":Landroid/app/Dialog;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 423
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 425
    :cond_1
    const-string v1, "storage"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 426
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mStorage:Landroid/preference/ListPreference;

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 427
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->initializePreference()V

    goto :goto_0

    .line 431
    .end local v0    # "storagedialog":Landroid/app/Dialog;
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->initializePreference()V

    goto :goto_0

    .line 436
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 443
    :sswitch_4
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isLocationVoicerecorderOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 444
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isAllowedLocation()Z

    move-result v1

    if-nez v1, :cond_0

    .line 445
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setLocationVoicerecorder(I)V

    .line 446
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLocationTag:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 418
    nop

    :sswitch_data_0
    .sparse-switch
        0xfb4 -> :sswitch_0
        0xfd2 -> :sswitch_2
        0xfd3 -> :sswitch_1
        0xfd7 -> :sswitch_1
        0xfe6 -> :sswitch_3
        0xff0 -> :sswitch_3
        0x10d6 -> :sswitch_4
    .end sparse-switch
.end method

.method private resetFileNameDialogPositiveButton()V
    .locals 4

    .prologue
    .line 394
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 395
    .local v0, "fragment":Landroid/app/Fragment;
    instance-of v2, v0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    if-eqz v2, :cond_0

    .line 396
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getDefaultFileName()Ljava/lang/String;

    move-result-object v1

    .line 397
    .local v1, "origin_text":Ljava/lang/String;
    check-cast v0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    .end local v0    # "fragment":Landroid/app/Fragment;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->resetOriginalName(Ljava/lang/String;)V

    .line 399
    .end local v1    # "origin_text":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private setDialogsWindowParam()V
    .locals 4

    .prologue
    .line 458
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mStorage:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x100

    const/16 v3, 0x100

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setFlags(II)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4

    .line 463
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingQuality:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x100

    const/16 v3, 0x100

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setFlags(II)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3

    .line 468
    :goto_1
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingVolume:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x100

    const/16 v3, 0x100

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setFlags(II)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    .line 473
    :goto_2
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mSTTLanguage:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x100

    const/16 v3, 0x100

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setFlags(II)V
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1

    .line 477
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 478
    .local v0, "frag":Landroid/app/Fragment;
    instance-of v1, v0, Landroid/app/DialogFragment;

    if-eqz v1, :cond_0

    instance-of v1, v0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    if-nez v1, :cond_0

    .line 480
    :try_start_4
    check-cast v0, Landroid/app/DialogFragment;

    .end local v0    # "frag":Landroid/app/Fragment;
    invoke-virtual {v0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x100

    const/16 v3, 0x100

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setFlags(II)V
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0

    .line 485
    :cond_0
    :goto_4
    return-void

    .line 482
    :catch_0
    move-exception v1

    goto :goto_4

    .line 475
    :catch_1
    move-exception v1

    goto :goto_3

    .line 470
    :catch_2
    move-exception v1

    goto :goto_2

    .line 465
    :catch_3
    move-exception v1

    goto :goto_1

    .line 460
    :catch_4
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public initializePreference()V
    .locals 11

    .prologue
    const v10, 0x7f0b00df

    const v9, 0x7f0b0090

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 285
    const-string v3, "VNSettingFragment"

    const-string v4, "initializePreference()"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 288
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getSdslotState(Landroid/content/Context;)I

    move-result v3

    if-ne v3, v8, :cond_4

    .line 289
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isAvailableForMemoryCard(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 290
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mStorage:Landroid/preference/ListPreference;

    invoke-virtual {v3, v8}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 303
    :goto_0
    const-string v3, "contextual_filename_value"

    invoke-static {v3, v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 304
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mContextualTag:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v8}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 305
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mDefaultName:Landroid/preference/Preference;

    invoke-virtual {v3, v7}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 311
    :goto_1
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 312
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mSTTLanguage:Landroid/preference/ListPreference;

    invoke-virtual {v3, v7}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 316
    :goto_2
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isLocationVoicerecorderOn()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 317
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isAllowedLocation()Z

    move-result v3

    if-nez v3, :cond_8

    .line 318
    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setLocationVoicerecorder(I)V

    .line 319
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLocationTag:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 326
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mStorage:Landroid/preference/ListPreference;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mStorage:Landroid/preference/ListPreference;

    invoke-virtual {v4}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 327
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingQuality:Landroid/preference/ListPreference;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingQuality:Landroid/preference/ListPreference;

    invoke-virtual {v4}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 328
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mDefaultName:Landroid/preference/Preference;

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getDefaultFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 329
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingVolume:Landroid/preference/ListPreference;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingVolume:Landroid/preference/ListPreference;

    invoke-virtual {v4}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 330
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mSTTLanguage:Landroid/preference/ListPreference;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mSTTLanguage:Landroid/preference/ListPreference;

    invoke-virtual {v4}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 331
    const-string v3, "skip_interval_value"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v2

    .line 332
    .local v2, "skipinterval_val":I
    const-string v3, "skip_interval"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 333
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mSkipInterval:Landroid/preference/Preference;

    const v4, 0x7f0b006c

    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    sget-object v6, Lcom/sec/android/app/voicenote/common/util/VNSettings;->SKIP_INTERVAL_ARRAY:[I

    aget v6, v6, v2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 337
    :goto_4
    const-string v3, "logo_type"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 338
    const-string v3, "logo"

    invoke-static {v3, v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 340
    :cond_0
    const-string v3, "logo"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 341
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLogo:Landroid/preference/Preference;

    const v4, 0x7f0b00e1

    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 346
    :goto_5
    const-string v3, "record_mode"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 376
    :goto_6
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITH_SVOICE(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITHOUT_SVOICE()Z

    move-result v3

    if-nez v3, :cond_1

    .line 377
    const-string v3, "voice_memo_category"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 378
    .local v0, "mCategory":Landroid/preference/PreferenceCategory;
    const-string v3, "settings_preference_screen"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceScreen;

    .line 379
    .local v1, "preferenceScreen":Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 380
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 384
    .end local v0    # "mCategory":Landroid/preference/PreferenceCategory;
    .end local v1    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    :cond_1
    sget-boolean v3, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_OPENSOURCE_LICENSE:Z

    if-nez v3, :cond_2

    .line 385
    const-string v3, "opensource_license_category"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 386
    .restart local v0    # "mCategory":Landroid/preference/PreferenceCategory;
    const-string v3, "settings_preference_screen"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceScreen;

    .line 387
    .restart local v1    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 388
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 391
    .end local v0    # "mCategory":Landroid/preference/PreferenceCategory;
    .end local v1    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    :cond_2
    return-void

    .line 292
    .end local v2    # "skipinterval_val":I
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mStorage:Landroid/preference/ListPreference;

    invoke-virtual {v3, v7}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 293
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mStorage:Landroid/preference/ListPreference;

    invoke-virtual {v3, v7}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto/16 :goto_0

    .line 296
    :cond_4
    const-string v3, "VNSettingFragment"

    const-string v4, "This device hasn\'t got a slot for SD card"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mStorage:Landroid/preference/ListPreference;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    .line 300
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mStorage:Landroid/preference/ListPreference;

    invoke-virtual {v3, v7}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto/16 :goto_0

    .line 307
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mContextualTag:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 308
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mDefaultName:Landroid/preference/Preference;

    invoke-virtual {v3, v8}, Landroid/preference/Preference;->setEnabled(Z)V

    goto/16 :goto_1

    .line 314
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mSTTLanguage:Landroid/preference/ListPreference;

    invoke-virtual {v3, v8}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto/16 :goto_2

    .line 321
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLocationTag:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v8}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_3

    .line 323
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLocationTag:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_3

    .line 335
    .restart local v2    # "skipinterval_val":I
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mSkipInterval:Landroid/preference/Preference;

    invoke-virtual {p0, v10}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 343
    :cond_b
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLogo:Landroid/preference/Preference;

    invoke-virtual {p0, v10}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 348
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingVolume:Landroid/preference/ListPreference;

    invoke-virtual {v3, v8}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 349
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingQuality:Landroid/preference/ListPreference;

    invoke-virtual {v3, v8}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 350
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mNoiseReduction:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v8}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_6

    .line 354
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingVolume:Landroid/preference/ListPreference;

    invoke-virtual {v3, v7}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 355
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingVolume:Landroid/preference/ListPreference;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 356
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingQuality:Landroid/preference/ListPreference;

    invoke-virtual {v3, v8}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 357
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mNoiseReduction:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v7}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_6

    .line 361
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingVolume:Landroid/preference/ListPreference;

    invoke-virtual {v3, v7}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 362
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingVolume:Landroid/preference/ListPreference;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 363
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingQuality:Landroid/preference/ListPreference;

    invoke-virtual {v3, v8}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 364
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mNoiseReduction:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v7}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_6

    .line 368
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingVolume:Landroid/preference/ListPreference;

    invoke-virtual {v3, v7}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 369
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingVolume:Landroid/preference/ListPreference;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 370
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingQuality:Landroid/preference/ListPreference;

    invoke-virtual {v3, v7}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 371
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingQuality:Landroid/preference/ListPreference;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 372
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mNoiseReduction:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v7}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_6

    .line 346
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 259
    packed-switch p1, :pswitch_data_0

    .line 271
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 272
    return-void

    .line 261
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->initializePreference()V

    goto :goto_0

    .line 265
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->initializePreference()V

    goto :goto_0

    .line 259
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 92
    const-string v0, "VNSettingFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 95
    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->setHasOptionsMenu(Z)V

    .line 96
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    const v1, 0x7f0b011e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show(Ljava/lang/String;)V

    .line 99
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/VNIntent;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mEventHandler:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment$EventHandler;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNIntent;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiversForSettingActivity(Z)V

    .line 102
    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->addPreferencesFromResource(I)V

    .line 104
    const-string v0, "general"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mGeneralCategory:Landroid/preference/PreferenceCategory;

    .line 105
    const-string v0, "storage"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mStorage:Landroid/preference/ListPreference;

    .line 106
    const-string v0, "recording_quality"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingQuality:Landroid/preference/ListPreference;

    .line 107
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Message_DisableMMS"

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingQuality:Landroid/preference/ListPreference;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const v2, 0x7f0b0090

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const v2, 0x7f0b00db

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 112
    :cond_0
    const-string v0, "default_name"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mDefaultName:Landroid/preference/Preference;

    .line 113
    const-string v0, "location_tag"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLocationTag:Landroid/preference/CheckBoxPreference;

    .line 114
    const-string v0, "noise_reduction"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mNoiseReduction:Landroid/preference/CheckBoxPreference;

    .line 115
    const-string v0, "contextual_filename"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mContextualTag:Landroid/preference/CheckBoxPreference;

    .line 116
    const-string v0, "recording_volume"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingVolume:Landroid/preference/ListPreference;

    .line 117
    const-string v0, "stt_language"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mSTTLanguage:Landroid/preference/ListPreference;

    .line 118
    const-string v0, "skip_interval"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mSkipInterval:Landroid/preference/Preference;

    .line 119
    const-string v0, "logo"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLogo:Landroid/preference/Preference;

    .line 120
    const-string v0, "opensource_license"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mOpensourceLicense:Landroid/preference/Preference;

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mStorage:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingQuality:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingVolume:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mSTTLanguage:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->initializePreference()V

    .line 128
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 159
    const-string v0, "VNSettingFragment"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiversForSettingActivity(Z)V

    .line 162
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    .line 163
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 276
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 281
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 278
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 276
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 150
    const-string v0, "VNSettingFragment"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiversForSettingActivity(Z)V

    .line 153
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 154
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->setDialogsWindowParam()V

    .line 155
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 167
    move-object v0, p1

    check-cast v0, Landroid/preference/ListPreference;

    .line 168
    .local v0, "temp":Landroid/preference/ListPreference;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mStorage:Landroid/preference/ListPreference;

    if-ne p1, v1, :cond_0

    const-string v1, "storage"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const v4, 0x7f0b00ae

    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v1, p2

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "1"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getDefaultFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0162

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 171
    check-cast p2, Ljava/lang/String;

    .end local p2    # "object":Ljava/lang/Object;
    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 172
    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 173
    const-string v1, "default_name"

    invoke-static {v1, v6}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    sput-object v6, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mFilename:Ljava/lang/String;

    .line 175
    const-string v1, ""

    invoke-static {v3, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setFixedNewFileName(ZLjava/lang/String;)V

    .line 176
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->initializePreference()V

    .line 177
    const v1, 0x7f0b0110

    const v3, 0x7f0b010f

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    move v1, v2

    .line 190
    :goto_0
    return v1

    .line 182
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mRecordingQuality:Landroid/preference/ListPreference;

    if-ne p1, v1, :cond_1

    move-object v1, p2

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "2"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 183
    const-string v1, "record_mode"

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 186
    :cond_1
    check-cast p2, Ljava/lang/String;

    .end local p2    # "object":Ljava/lang/Object;
    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 187
    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->initializePreference()V

    .line 189
    const-string v1, "change_notification_resume"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    move v1, v3

    .line 190
    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 10
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f0e001e

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 195
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mDefaultName:Landroid/preference/Preference;

    if-ne p2, v6, :cond_2

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "FRAGMENT_DIALOG"

    invoke-virtual {v6, v7}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v6

    if-nez v6, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "FRAGMENT_DIALOG"

    invoke-virtual {v6, v7}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/app/DialogFragment;

    .line 198
    .local v1, "dialog":Landroid/app/DialogFragment;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getDefaultFileName()Ljava/lang/String;

    move-result-object v3

    .line 199
    .local v3, "ori_text":Ljava/lang/String;
    const/4 v6, 0x3

    const-wide/16 v8, 0x0

    const v7, 0x7f0b005c

    invoke-static {v6, v3, v8, v9, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->setArguments(ILjava/lang/String;JI)Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    move-result-object v1

    .line 200
    invoke-virtual {v1, p0, v5}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 201
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 202
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v5

    if-nez v5, :cond_0

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v5, v6}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 254
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v1    # "dialog":Landroid/app/DialogFragment;
    .end local v3    # "ori_text":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v5

    :cond_1
    :goto_1
    return v5

    .line 206
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mSkipInterval:Landroid/preference/Preference;

    if-ne p2, v6, :cond_3

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 208
    .local v4, "transaction":Landroid/app/FragmentTransaction;
    new-instance v5, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;

    invoke-direct {v5}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;-><init>()V

    invoke-virtual {v4, v8, v5}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 209
    invoke-virtual {v4, v9}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 210
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 211
    .end local v4    # "transaction":Landroid/app/FragmentTransaction;
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLogo:Landroid/preference/Preference;

    if-ne p2, v6, :cond_4

    .line 212
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 213
    .restart local v4    # "transaction":Landroid/app/FragmentTransaction;
    new-instance v5, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;

    invoke-direct {v5}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;-><init>()V

    const-string v6, "logo_fragment"

    invoke-virtual {v4, v8, v5, v6}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 214
    invoke-virtual {v4, v9}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 215
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 216
    .end local v4    # "transaction":Landroid/app/FragmentTransaction;
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLocationTag:Landroid/preference/CheckBoxPreference;

    if-ne p2, v6, :cond_9

    .line 217
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isLocationVoicerecorderOn()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 218
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setLocationVoicerecorder(I)V

    .line 219
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLocationTag:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 221
    :cond_5
    sget-boolean v6, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_DATA_CHECK_POPUP:Z

    if-eqz v6, :cond_6

    const-string v6, "key_data_check_location"

    invoke-static {v6, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v6

    if-nez v6, :cond_6

    .line 223
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLocationTag:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 224
    const-string v6, "location_tag"

    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->newInstance(Ljava/lang/String;)Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG"

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 226
    :cond_6
    const-string v6, "show_location_tag_info"

    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_7

    .line 227
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLocationTag:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "FRAGMENT_DIALOG"

    invoke-virtual {v6, v7}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v6

    if-nez v6, :cond_1

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "FRAGMENT_DIALOG"

    invoke-virtual {v6, v7}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/app/DialogFragment;

    .line 230
    .restart local v1    # "dialog":Landroid/app/DialogFragment;
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;

    .end local v1    # "dialog":Landroid/app/DialogFragment;
    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;-><init>()V

    .line 231
    .restart local v1    # "dialog":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v6, v7}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 234
    .end local v1    # "dialog":Landroid/app/DialogFragment;
    :cond_7
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isAllowedLocation()Z

    move-result v6

    if-nez v6, :cond_8

    .line 235
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mLocationTag:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 236
    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 237
    .local v2, "i":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 239
    .end local v2    # "i":Landroid/content/Intent;
    :cond_8
    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setLocationVoicerecorder(I)V

    goto/16 :goto_1

    .line 243
    :cond_9
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mContextualTag:Landroid/preference/CheckBoxPreference;

    if-ne p2, v6, :cond_b

    .line 244
    const-string v6, "contextual_filename"

    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 245
    const-string v6, "contextual_filename_value"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 246
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mDefaultName:Landroid/preference/Preference;

    invoke-virtual {v6, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    goto/16 :goto_0

    .line 248
    :cond_a
    const-string v6, "contextual_filename_value"

    invoke-static {v6, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 249
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mDefaultName:Landroid/preference/Preference;

    invoke-virtual {v5, v7}, Landroid/preference/Preference;->setEnabled(Z)V

    goto/16 :goto_0

    .line 251
    :cond_b
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mOpensourceLicense:Landroid/preference/Preference;

    if-ne p2, v5, :cond_0

    .line 252
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    new-instance v6, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const-class v8, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v6}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 132
    const-string v1, "VNSettingFragment"

    const-string v2, "onResume"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiversForSettingActivity(Z)V

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->mActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    const v2, 0x7f0b011e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->initializePreference()V

    .line 138
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->resetFileNameDialogPositiveButton()V

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 141
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 142
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 145
    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 146
    return-void
.end method
