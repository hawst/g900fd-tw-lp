.class public Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;
.super Landroid/app/Fragment;
.source "VNRecordingModeFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$Callbacks;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VNRecordingModeFragment"

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# instance fields
.field private defaultValue:I

.field private mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$Callbacks;

.field private mLastStartedAnim:Landroid/animation/ValueAnimator;

.field private mMicBody:Landroid/widget/ImageView;

.field private mMicBodyOff:Landroid/widget/ImageView;

.field private mMicBodyOn:Landroid/widget/ImageView;

.field private mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

.field private mMicIconDownOff:Landroid/widget/ImageView;

.field private mMicIconDownOn:Landroid/widget/ImageView;

.field private mMicIconUpOff:Landroid/widget/ImageView;

.field private mMicIconUpOn:Landroid/widget/ImageView;

.field private mViewChild:Landroid/view/View;

.field private resource:I

.field updateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 42
    const v0, 0x7f03004b

    iput v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->resource:I

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mViewChild:Landroid/view/View;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBody:Landroid/widget/ImageView;

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn:Landroid/widget/ImageView;

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$Callbacks;

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOff:Landroid/widget/ImageView;

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOn:Landroid/widget/ImageView;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOff:Landroid/widget/ImageView;

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOn:Landroid/widget/ImageView;

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOff:Landroid/widget/ImageView;

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->defaultValue:I

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mLastStartedAnim:Landroid/animation/ValueAnimator;

    .line 202
    new-instance v0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$1;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->updateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;)Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    return-object v0
.end method

.method private doEcholighting(F)V
    .locals 4
    .param p1, "eq"    # F

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const v1, 0x3e19999a    # 0.15f

    .line 173
    cmpl-float v0, p1, v2

    if-nez v0, :cond_1

    .line 174
    invoke-virtual {p0, v2, v2, v3}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->getOnAnimation(FFI)V

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    cmpl-float v0, p1, v1

    if-lez v0, :cond_2

    .line 176
    const/16 v0, 0x2bc

    invoke-virtual {p0, p1, v1, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->getOnAnimation(FFI)V

    goto :goto_0

    .line 177
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    :cond_3
    invoke-virtual {p0, v1, v1, v3}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->getOnAnimation(FFI)V

    goto :goto_0
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 232
    sput-object p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 233
    return-void
.end method

.method public static newInstance(II)Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;
    .locals 3
    .param p0, "resource"    # I
    .param p1, "recordState"    # I

    .prologue
    .line 64
    new-instance v1, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;-><init>()V

    .line 65
    .local v1, "f":Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 66
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "resource"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 67
    const-string v2, "recordstate"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 68
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->setArguments(Landroid/os/Bundle;)V

    .line 69
    return-object v1
.end method

.method public static releaseService()V
    .locals 1

    .prologue
    .line 236
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 237
    return-void
.end method


# virtual methods
.method public getOnAnimation(FFI)V
    .locals 4
    .param p1, "fromAlpah"    # F
    .param p2, "toAlpah"    # F
    .param p3, "time"    # I

    .prologue
    .line 183
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOff:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->defaultValue:I

    .line 185
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mLastStartedAnim:Landroid/animation/ValueAnimator;

    if-eqz v2, :cond_0

    .line 187
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mLastStartedAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->pause()V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mLastStartedAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->cancel()V

    .line 194
    :cond_0
    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput p1, v2, v3

    const/4 v3, 0x1

    aput p2, v2, v3

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 195
    .local v1, "va":Landroid/animation/ValueAnimator;
    int-to-long v2, p3

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 196
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->updateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 198
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mLastStartedAnim:Landroid/animation/ValueAnimator;

    .line 199
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mLastStartedAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 200
    return-void

    .line 188
    .end local v1    # "va":Landroid/animation/ValueAnimator;
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v2, "VNRecordingModeFragment"

    const-string v3, "NoSuchMethodError occured"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getResource()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->resource:I

    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 227
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$Callbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$Callbacks;

    .line 228
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 229
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$Callbacks;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment$Callbacks;->onControlButtonSelected(I)Z

    .line 223
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    const v5, 0x3ecccccd    # 0.4f

    const/16 v4, 0x8

    .line 78
    const-string v1, "VNRecordingModeFragment"

    const-string v3, "onCreateView()"

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 80
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 81
    const-string v1, "resource"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->resource:I

    .line 84
    :cond_0
    iget v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->resource:I

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mViewChild:Landroid/view/View;

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mViewChild:Landroid/view/View;

    const v3, 0x7f0e00d0

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBody:Landroid/widget/ImageView;

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mViewChild:Landroid/view/View;

    const v3, 0x7f0e00d1

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn:Landroid/widget/ImageView;

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mViewChild:Landroid/view/View;

    const v3, 0x7f0e00d2

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mViewChild:Landroid/view/View;

    const v3, 0x7f0e00cf

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOff:Landroid/widget/ImageView;

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mViewChild:Landroid/view/View;

    const v3, 0x7f0e00d3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOn:Landroid/widget/ImageView;

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mViewChild:Landroid/view/View;

    const v3, 0x7f0e00d4

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOff:Landroid/widget/ImageView;

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mViewChild:Landroid/view/View;

    const v3, 0x7f0e00d5

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOn:Landroid/widget/ImageView;

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mViewChild:Landroid/view/View;

    const v3, 0x7f0e00d6

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOff:Landroid/widget/ImageView;

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOff:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->defaultValue:I

    .line 94
    invoke-virtual {p0, v5, v5, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->getOnAnimation(FFI)V

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->removeEQ()V

    .line 98
    const-string v1, "logo"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOff:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBody:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->setVisibility(I)V

    .line 104
    :cond_1
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getRecorderState()I

    move-result v1

    const/16 v3, 0x3e8

    if-eq v1, v3, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->updateMic(Z)V

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mViewChild:Landroid/view/View;

    return-object v1

    :cond_2
    move v1, v2

    .line 104
    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 118
    const-string v0, "VNRecordingModeFragment"

    const-string v1, "onDestroyView()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOff:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOff:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 126
    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 127
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mLastStartedAnim:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mLastStartedAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 113
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 114
    return-void
.end method

.method public removeEQ()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOff:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOff:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOff:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOff:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOn:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 166
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOn:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 167
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->setVisibility(I)V

    .line 168
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 169
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mLastStartedAnim:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mLastStartedAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 170
    :cond_6
    return-void
.end method

.method public updateEQ(IF)V
    .locals 4
    .param p1, "amplitude"    # I
    .param p2, "maxAmp"    # F

    .prologue
    .line 156
    mul-int/lit8 v2, p1, 0x9

    int-to-float v2, v2

    div-float/2addr v2, p2

    const/high16 v3, 0x3f800000    # 1.0f

    add-float v0, v2, v3

    .line 157
    .local v0, "eq":F
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->log10(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 159
    .local v1, "log":F
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->doEcholighting(F)V

    .line 160
    return-void
.end method

.method public updateMic(Z)V
    .locals 5
    .param p1, "record"    # Z

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 134
    const-string v0, "VNRecordingModeFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateMic() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    if-eqz p1, :cond_6

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOn:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOff:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOff:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 139
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOff:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOff:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 140
    :cond_3
    const-string v0, "logo"

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->setVisibility(I)V

    .line 142
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 153
    :cond_5
    :goto_0
    return-void

    .line 145
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOff:Landroid/widget/ImageView;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOff:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 146
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOff:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOff:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 147
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOn:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconUpOn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 148
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOn:Landroid/widget/ImageView;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicIconDownOn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 149
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn_echo:Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->setVisibility(I)V

    .line 150
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mLastStartedAnim:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mLastStartedAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 151
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->mMicBodyOn:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
