.class Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;
.super Ljava/lang/Thread;
.source "FileDeleteOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FileOperationThread"
.end annotation


# instance fields
.field private mCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

.field private mContext:Landroid/content/Context;

.field private mDeleteArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;Landroid/os/Handler;Landroid/content/Context;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V
    .locals 1
    .param p2, "progressHandler"    # Landroid/os/Handler;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "callback"    # Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/os/Handler;",
            "Landroid/content/Context;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 128
    .local p1, "checkedIdArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 129
    invoke-virtual {p1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mDeleteArrayList:Ljava/util/ArrayList;

    .line 130
    iput-object p2, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mHandler:Landroid/os/Handler;

    .line 131
    iput-object p3, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mContext:Landroid/content/Context;

    .line 132
    iput-object p4, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    .line 133
    return-void
.end method

.method private DeleteFiles(Landroid/os/Handler;)V
    .locals 5
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v4, 0x2

    .line 145
    const-string v2, "FileDeleteOperation"

    const-string v3, "DeleteFiles : Start"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const/4 v0, 0x0

    .line 150
    .local v0, "deletedIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :goto_0
    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mDeleteArrayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 151
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->isInterrupted()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 152
    new-instance v2, Ljava/lang/InterruptedException;

    invoke-direct {v2}, Ljava/lang/InterruptedException;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :catch_0
    move-exception v1

    .line 160
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "FileDeleteOperation"

    const-string v3, "DeleteFiles : is failed. "

    invoke-static {v2, v3, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mContext:Landroid/content/Context;

    # invokes: Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->deleteFromDB(Landroid/content/Context;Ljava/util/ArrayList;)V
    invoke-static {v2, v0}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->access$000(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 162
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 163
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 170
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 155
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mDeleteArrayList:Ljava/util/ArrayList;

    invoke-static {v2, v3, p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->deletefilesSet(Landroid/content/Context;Ljava/util/ArrayList;Landroid/os/Handler;)Ljava/util/ArrayList;

    move-result-object v0

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mDeleteArrayList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 167
    :cond_2
    const-string v2, "FileDeleteOperation"

    const-string v3, "DeleteFiles : is success "

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mContext:Landroid/content/Context;

    # invokes: Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->deleteFromDB(Landroid/content/Context;Ljava/util/ArrayList;)V
    invoke-static {v2, v0}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->access$000(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 169
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mHandler:Landroid/os/Handler;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->DeleteFiles(Landroid/os/Handler;)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mDeleteArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation$FileOperationThread;->mCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    invoke-static {v0, v2, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 141
    const-string v0, "FileDeleteOperation"

    const-string v1, "DeleteFiles : Finish"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    return-void
.end method
