.class public interface abstract Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
.super Ljava/lang/Object;
.source "VNLibraryCallbacks.java"


# virtual methods
.method public abstract TransitAnimationState(Z)V
.end method

.method public abstract finishBookmarkActionMode()V
.end method

.method public abstract finishCustomActionMode()V
.end method

.method public abstract getActionBarIsSearched()Z
.end method

.method public abstract getActionMode()Landroid/view/ActionMode;
.end method

.method public abstract getBookmark()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFileTitle(I)Ljava/lang/String;
.end method

.method public abstract onActivityResult(IILandroid/content/Intent;)V
.end method

.method public abstract onBookmarkAdded(Lcom/sec/android/app/voicenote/common/util/Bookmark;)V
.end method

.method public abstract onBookmarkChildPressed(JI)V
.end method

.method public abstract onBookmarkItemClick(I)V
.end method

.method public abstract onBookmarksChanged(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onBookmarksRenamed(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onControlButtonSelected(IJ)Z
.end method

.method public abstract onDataChanged()V
.end method

.method public abstract onDeleteBookmark(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onDeleteFile()V
.end method

.method public abstract onDoResume()V
.end method

.method public abstract onDoSearch(Ljava/lang/String;)V
.end method

.method public abstract onDoStop()V
.end method

.method public abstract onMoveToBookmark(I)V
.end method

.method public abstract onSTTEditPopup(Z)V
.end method

.method public abstract onSTTStringChanged(ILjava/lang/String;Z)V
.end method

.method public abstract onTrimCompleted(I)V
.end method

.method public abstract onUpdateState(Z)V
.end method

.method public abstract onUpdateStateNoModeChange(Z)V
.end method

.method public abstract onWarningDialogResult(I)V
.end method

.method public abstract refreashAnim(Z)V
.end method

.method public abstract setActionMode(Landroid/view/ActionMode;)V
.end method

.method public abstract showLibraryActionbar()V
.end method

.method public abstract stop_HidePlayer()V
.end method
