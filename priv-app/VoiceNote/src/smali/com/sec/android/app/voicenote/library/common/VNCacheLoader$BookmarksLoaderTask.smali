.class Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;
.super Landroid/os/AsyncTask;
.source "VNCacheLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BookmarksLoaderTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field mDataChanged:Z

.field mInfo:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

.field mNotify:Z

.field mPath:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;Ljava/lang/String;Z)V
    .locals 2
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "notify"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 247
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->this$0:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 242
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mPath:Ljava/lang/String;

    .line 243
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mDataChanged:Z

    .line 244
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mNotify:Z

    .line 245
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mInfo:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    .line 248
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mPath:Ljava/lang/String;

    .line 249
    iput-boolean p3, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mNotify:Z

    .line 250
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;Ljava/lang/String;ZLcom/sec/android/app/voicenote/common/util/M4aInfo;)V
    .locals 0
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "notify"    # Z
    .param p4, "info"    # Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    .prologue
    .line 253
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;-><init>(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;Ljava/lang/String;Z)V

    .line 254
    iput-object p4, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mInfo:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    .line 255
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "arg0"    # [Ljava/lang/Object;

    .prologue
    .line 259
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mPath:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mPath:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isM4A(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 260
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mInfo:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    if-nez v3, :cond_0

    .line 261
    new-instance v2, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mPath:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 262
    .local v2, "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mInfo:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    .line 264
    .end local v2    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mInfo:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mInfo:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-boolean v3, v3, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasBookmarks:Z

    if-eqz v3, :cond_1

    .line 265
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mInfo:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    invoke-direct {v0, v3}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V

    .line 267
    .local v0, "bookmarksHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->this$0:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mBookmarksCache:Landroid/util/LruCache;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->access$000(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;)Landroid/util/LruCache;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mPath:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->getAllBookmarks()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    invoke-virtual {v4, v5, v3}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    :goto_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mDataChanged:Z

    .line 275
    .end local v0    # "bookmarksHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    :cond_1
    const/4 v3, 0x0

    return-object v3

    .line 269
    .restart local v0    # "bookmarksHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    :catch_0
    move-exception v1

    .line 270
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->this$0:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mAdapter:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mDataChanged:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->mNotify:Z

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->this$0:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mScrollingState:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->access$100(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;)I

    move-result v0

    if-nez v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->this$0:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mAdapter:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;

    invoke-interface {v0}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;->notifyOnCacheLoaded()V

    .line 286
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 287
    return-void
.end method
