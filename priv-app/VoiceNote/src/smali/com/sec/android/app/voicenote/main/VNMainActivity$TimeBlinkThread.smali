.class Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;
.super Ljava/lang/Thread;
.source "VNMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TimeBlinkThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 2729
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/main/VNMainActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/main/VNMainActivity$1;

    .prologue
    .line 2729
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 2734
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2747
    :cond_0
    :goto_1
    return-void

    .line 2738
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x83d

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 2739
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$TimeBlinkThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x83d

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 2740
    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 2742
    :catch_0
    move-exception v1

    .line 2743
    .local v1, "ne":Ljava/lang/NullPointerException;
    const-string v2, "VoiceNoteMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ignore pause time blink message after destroyed : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2744
    .end local v1    # "ne":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 2745
    .local v0, "ie":Ljava/lang/InterruptedException;
    const-string v2, "VoiceNoteMainActivity"

    const-string v3, "TimeBlinkThread InterruptedException"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
