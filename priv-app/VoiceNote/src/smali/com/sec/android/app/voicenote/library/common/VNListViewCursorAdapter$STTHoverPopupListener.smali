.class Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$STTHoverPopupListener;
.super Ljava/lang/Object;
.source "VNListViewCursorAdapter.java"

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "STTHoverPopupListener"
.end annotation


# instance fields
.field mInfo:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V
    .locals 1
    .param p2, "info"    # Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    .prologue
    .line 510
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$STTHoverPopupListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 509
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$STTHoverPopupListener;->mInfo:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    .line 511
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$STTHoverPopupListener;->mInfo:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    .line 512
    return-void
.end method


# virtual methods
.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "popup"    # Landroid/widget/HoverPopupWindow;

    .prologue
    const/4 v9, 0x0

    .line 515
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$STTHoverPopupListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mInflater:Landroid/view/LayoutInflater;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->access$100(Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f03001f

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 516
    .local v1, "fullText2":Landroid/widget/TextView;
    new-instance v4, Lcom/sec/android/app/voicenote/common/util/SttdHelper;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$STTHoverPopupListener;->mInfo:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$STTHoverPopupListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContextChild:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->access$200(Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;)Landroid/content/Context;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;Landroid/content/Context;)V

    .line 517
    .local v4, "sttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->readSttd()Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 518
    .local v5, "textData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 519
    .local v3, "ssb":Ljava/lang/StringBuilder;
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 520
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 521
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget v6, v6, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    if-nez v6, :cond_0

    .line 522
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v6, v6, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    aget-object v6, v6, v9

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 525
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 526
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 527
    invoke-virtual {p2, v1}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 528
    const/4 v6, 0x1

    return v6
.end method
