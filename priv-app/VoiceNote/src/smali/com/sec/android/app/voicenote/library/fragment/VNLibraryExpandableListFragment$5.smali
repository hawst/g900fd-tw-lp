.class Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$5;
.super Landroid/animation/AnimatorListenerAdapter;
.source "VNLibraryExpandableListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->onCreateAnimator(IZI)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)V
    .locals 0

    .prologue
    .line 1868
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 1879
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 1880
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1881
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$300()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->TransitAnimationState(Z)V

    .line 1883
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->enableBuildCache()V

    .line 1884
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 1871
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1872
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$300()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->TransitAnimationState(Z)V

    .line 1874
    :cond_0
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationStart(Landroid/animation/Animator;)V

    .line 1875
    return-void
.end method
