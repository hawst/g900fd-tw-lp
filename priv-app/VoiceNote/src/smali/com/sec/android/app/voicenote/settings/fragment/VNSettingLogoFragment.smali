.class public Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;
.super Landroid/app/Fragment;
.source "VNSettingLogoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field public static final CROP_FROM_CAMERA:I = 0xd

.field public static final REQUEST_CAPTURE_IMAGE:I = 0xc

.field public static final REQUEST_IMAGE:I = 0xb

.field public static final RESULT_CANCLE:I = 0x0

.field public static final RESULT_OK:I = -0x1

.field public static final SELECT_LOGO_DIALOG:I = 0xa

.field private static final TAG:Ljava/lang/String; = "VNSettingLogoFragment"

.field private static final TEMP_CROP_FILE:Ljava/lang/String; = "temporary_crop.jpg"

.field private static final TEMP_PHOTO_FILE:Ljava/lang/String; = "temporary_holder.jpg"


# instance fields
.field private mActionBar:Landroid/app/ActionBar;

.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mAddLogo:Landroid/widget/ImageView;

.field private mAddLogoText:Landroid/widget/TextView;

.field private mCancelButton:Landroid/widget/Button;

.field private mChangeButton:Landroid/widget/Button;

.field private mDialog:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;

.field private mDoneButton:Landroid/widget/Button;

.field private mLogoBG:Landroid/widget/ImageView;

.field private mLogoHeight:I

.field private mLogoText:Landroid/widget/TextView;

.field private mLogoWidth:I

.field private mNotUsedLogoLayout:Landroid/widget/FrameLayout;

.field private mTempLogoType:I

.field private mbLogoEnable:Z

.field private mbOKEnable:Z

.field private mcurrentUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x30c

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 109
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 87
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBarSwitch:Landroid/widget/Switch;

    .line 88
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBar:Landroid/app/ActionBar;

    .line 90
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    .line 91
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    .line 92
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogoText:Landroid/widget/TextView;

    .line 93
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoText:Landroid/widget/TextView;

    .line 94
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    .line 96
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    .line 97
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mCancelButton:Landroid/widget/Button;

    .line 98
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    .line 99
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mcurrentUri:Landroid/net/Uri;

    .line 100
    iput v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I

    .line 101
    iput v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoHeight:I

    .line 102
    iput v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoWidth:I

    .line 103
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbLogoEnable:Z

    .line 105
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mNotUsedLogoLayout:Landroid/widget/FrameLayout;

    .line 106
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDialog:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;

    .line 110
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogoText:Landroid/widget/TextView;

    return-object v0
.end method

.method private convert3rdpartyUri(Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 7
    .param p1, "from3rdparty"    # Landroid/net/Uri;
    .param p2, "renameTo"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x0

    .line 638
    const/4 v1, 0x0

    .line 639
    .local v1, "file":Ljava/io/File;
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    .line 640
    .local v3, "type":Ljava/lang/String;
    const-string v5, "content"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 641
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPathFromUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 642
    .local v2, "path":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 643
    const-string v5, "VNSettingLogoFragment"

    const-string v6, "URI is invalid"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    .end local v2    # "path":Ljava/lang/String;
    :goto_0
    return v4

    .line 646
    .restart local v2    # "path":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/io/File;

    .end local v1    # "file":Ljava/io/File;
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 647
    .restart local v1    # "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 648
    new-instance v0, Ljava/io/File;

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 649
    .local v0, "dest":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 650
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 652
    :cond_1
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->copyFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 671
    .end local v2    # "path":Ljava/lang/String;
    :goto_1
    const/4 v4, 0x1

    goto :goto_0

    .line 654
    .end local v0    # "dest":Ljava/io/File;
    .restart local v2    # "path":Ljava/lang/String;
    :cond_2
    const-string v5, "VNSettingLogoFragment"

    const-string v6, "Fail to pick picture : file is not exists"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 658
    .end local v2    # "path":Ljava/lang/String;
    :cond_3
    new-instance v1, Ljava/io/File;

    .end local v1    # "file":Ljava/io/File;
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 659
    .restart local v1    # "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 660
    new-instance v0, Ljava/io/File;

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 661
    .restart local v0    # "dest":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 662
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 664
    :cond_4
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->copyFile(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1

    .line 666
    .end local v0    # "dest":Ljava/io/File;
    :cond_5
    const-string v5, "VNSettingLogoFragment"

    const-string v6, "Fail to pick picture : file is not exists"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getTempCropFile()Ljava/io/File;
    .locals 3

    .prologue
    .line 692
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mounted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 693
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "temporary_crop.jpg"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 696
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTempCropUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 688
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempCropFile()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getTempFile()Ljava/io/File;
    .locals 3

    .prologue
    .line 679
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mounted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 680
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "temporary_holder.jpg"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 683
    .local v0, "file":Ljava/io/File;
    :goto_0
    return-object v0

    .end local v0    # "file":Ljava/io/File;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTempUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 675
    invoke-static {}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempFile()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private move(Ljava/io/File;Ljava/io/File;)V
    .locals 9
    .param p1, "source"    # Ljava/io/File;
    .param p2, "target"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 701
    const/4 v2, 0x0

    .line 702
    .local v2, "in":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 705
    .local v5, "out":Ljava/io/OutputStream;
    const/16 v7, 0x400

    new-array v0, v7, [B

    .line 709
    .local v0, "buf":[B
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 710
    .end local v2    # "in":Ljava/io/InputStream;
    .local v3, "in":Ljava/io/InputStream;
    :try_start_1
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    .end local v5    # "out":Ljava/io/OutputStream;
    .local v6, "out":Ljava/io/OutputStream;
    move-object v5, v6

    .end local v6    # "out":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/OutputStream;
    move-object v2, v3

    .line 717
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    :goto_0
    :try_start_2
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v4

    .local v4, "len":I
    if-lez v4, :cond_0

    .line 718
    const/4 v7, 0x0

    invoke-virtual {v5, v0, v7, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 720
    .end local v4    # "len":I
    :catch_0
    move-exception v1

    .line 722
    .local v1, "e":Ljava/io/IOException;
    :try_start_3
    const-string v7, "VNSettingLogoFragment"

    const-string v8, "file doesn\'t exist"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 724
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 725
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 726
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 728
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    return-void

    .line 711
    :catch_1
    move-exception v1

    .line 713
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_2
    const-string v7, "VNSettingLogoFragment"

    const-string v8, "file doesn\'t exist"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 724
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "len":I
    :cond_0
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 725
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 726
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    goto :goto_1

    .line 724
    .end local v4    # "len":I
    :catchall_0
    move-exception v7

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 725
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 726
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    throw v7

    .line 711
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_2
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_2
.end method

.method private recycleImage()V
    .locals 4

    .prologue
    .line 731
    const-string v2, "VNSettingLogoFragment"

    const-string v3, "recycleImage"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    const-string v2, "logo_type"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 734
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 736
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    instance-of v2, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_0

    .line 737
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 738
    .local v1, "temp":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    .line 739
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 742
    .end local v1    # "temp":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 744
    :cond_1
    return-void
.end method

.method private setEnableButton(Landroid/widget/Button;Z)V
    .locals 4
    .param p1, "v"    # Landroid/widget/Button;
    .param p2, "enable"    # Z

    .prologue
    .line 800
    if-eqz p2, :cond_0

    .line 801
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080041

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 802
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 810
    :goto_0
    return-void

    .line 804
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080043

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 805
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 807
    :catch_0
    move-exception v0

    .line 808
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "VNSettingLogoFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setEnableButton() isResumed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->isResumed()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setLogoClickable(Z)V
    .locals 2
    .param p1, "clickable"    # Z

    .prologue
    .line 813
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 814
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 815
    if-eqz p1, :cond_1

    .line 816
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 820
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment$2;-><init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 834
    :cond_0
    return-void

    .line 818
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private setLogoImage(Landroid/net/Uri;Z)Z
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "isFrom3rdparty"    # Z

    .prologue
    .line 747
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 748
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 749
    .local v2, "mainImage":Landroid/graphics/Bitmap;
    const/4 v4, 0x0

    .line 750
    .local v4, "oldBitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_0

    .line 751
    const-string v10, "VNSettingLogoFragment"

    const-string v11, "setLogoImage - mainImage is null"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    const/4 v10, 0x0

    .line 795
    :goto_0
    return v10

    .line 755
    :cond_0
    if-eqz p2, :cond_3

    .line 756
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    .line 757
    .local v9, "width":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 758
    .local v1, "height":I
    iget v10, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoWidth:I

    if-ne v9, v10, :cond_1

    iget v10, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoHeight:I

    if-eq v1, v10, :cond_3

    .line 759
    :cond_1
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    if-le v10, v11, :cond_5

    .line 760
    iget v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoWidth:I

    .line 761
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-float v10, v10

    iget v11, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoWidth:I

    int-to-float v11, v11

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    int-to-float v12, v12

    div-float/2addr v11, v12

    mul-float/2addr v10, v11

    float-to-int v1, v10

    .line 766
    :cond_2
    :goto_1
    const/4 v10, 0x1

    invoke-static {v2, v9, v1, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 767
    .local v7, "scaledMainImage":Landroid/graphics/Bitmap;
    move-object v4, v2

    .line 768
    move-object v2, v7

    .line 769
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->SaveBitmapToFileCache(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 773
    .end local v1    # "height":I
    .end local v7    # "scaledMainImage":Landroid/graphics/Bitmap;
    .end local v9    # "width":I
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0201a6

    invoke-static {v10, v11}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 774
    .local v8, "tmpMask":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    const/4 v12, 0x1

    invoke-static {v8, v10, v11, v12}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 776
    .local v3, "mask":Landroid/graphics/Bitmap;
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 777
    const/4 v8, 0x0

    .line 778
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    sget-object v12, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v10, v11, v12}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 780
    .local v6, "result":Landroid/graphics/Bitmap;
    invoke-virtual {v0, v6}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 781
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 782
    .local v5, "paint":Landroid/graphics/Paint;
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 784
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v0, v2, v10, v11, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 785
    new-instance v10, Landroid/graphics/PorterDuffXfermode;

    sget-object v11, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v10, v11}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 786
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v0, v3, v10, v11, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 787
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 788
    iget-object v10, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    invoke-virtual {v10, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 789
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v10

    if-nez v10, :cond_4

    .line 790
    const-string v10, "VNSettingLogoFragment"

    const-string v11, "setLogoImage() recycle old bitmap"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 792
    const/4 v4, 0x0

    .line 795
    :cond_4
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 762
    .end local v3    # "mask":Landroid/graphics/Bitmap;
    .end local v5    # "paint":Landroid/graphics/Paint;
    .end local v6    # "result":Landroid/graphics/Bitmap;
    .end local v8    # "tmpMask":Landroid/graphics/Bitmap;
    .restart local v1    # "height":I
    .restart local v9    # "width":I
    :cond_5
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    if-ge v10, v11, :cond_2

    .line 763
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    int-to-float v10, v10

    iget v11, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoHeight:I

    int-to-float v11, v11

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    int-to-float v12, v12

    div-float/2addr v11, v12

    mul-float/2addr v10, v11

    float-to-int v9, v10

    .line 764
    iget v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoHeight:I

    goto/16 :goto_1
.end method


# virtual methods
.method public CreateActionbar()V
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/4 v4, 0x0

    const/4 v6, -0x2

    const/4 v5, 0x1

    .line 132
    new-instance v1, Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBarSwitch:Landroid/widget/Switch;

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f090000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v4, v4, v2, v4}, Landroid/widget/Switch;->setPadding(IIII)V

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBar:Landroid/app/ActionBar;

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBar:Landroid/app/ActionBar;

    if-eqz v1, :cond_1

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00a5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v7, v7}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 141
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "fa"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ur"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBar:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    const/16 v4, 0x13

    invoke-direct {v3, v6, v6, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 153
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v5}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v5}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 158
    const-string v1, "logo"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 159
    .local v0, "setval":Ljava/lang/Boolean;
    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setHasOptionsMenu(Z)V

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 162
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 164
    .end local v0    # "setval":Ljava/lang/Boolean;
    :cond_1
    return-void

    .line 148
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBar:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    const/16 v4, 0x15

    invoke-direct {v3, v6, v6, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    goto :goto_0
.end method

.method public changedFouceble(Z)V
    .locals 2
    .param p1, "bool"    # Z

    .prologue
    .line 888
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 889
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    .line 891
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 892
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    .line 894
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mCancelButton:Landroid/widget/Button;

    if-eqz v0, :cond_2

    .line 895
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    .line 897
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 898
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 900
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 901
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 903
    :cond_4
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 12
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 513
    const-string v9, "VNSettingLogoFragment"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onActivityResult "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    const/16 v9, 0xa

    if-ne p1, v9, :cond_2

    .line 516
    packed-switch p2, :pswitch_data_0

    .line 634
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 635
    :cond_1
    return-void

    .line 520
    :pswitch_0
    new-instance v5, Landroid/content/Intent;

    const-string v9, "android.intent.action.GET_CONTENT"

    invoke-direct {v5, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 521
    .local v5, "galleryIntent":Landroid/content/Intent;
    const-string v9, "com.sec.android.gallery3d"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 522
    const-string v9, "image/*"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 523
    const-string v9, "crop"

    const-string v10, "true"

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 524
    const-string v9, "outputX"

    iget v10, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoWidth:I

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 525
    const-string v9, "outputY"

    iget v10, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoHeight:I

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 526
    const-string v9, "aspectX"

    const/4 v10, 0x1

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 527
    const-string v9, "aspectY"

    const/4 v10, 0x1

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 528
    const-string v9, "scale"

    const/4 v10, 0x1

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 529
    const-string v9, "set-as-image"

    const/4 v10, 0x1

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 530
    const-string v9, "output"

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempUri()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 531
    const-string v9, "outputFormat"

    sget-object v10, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v10}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 533
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const/16 v10, 0xb

    invoke-virtual {v9, v5, v10}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 534
    :catch_0
    move-exception v2

    .line 535
    .local v2, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 540
    .end local v2    # "e":Landroid/content/ActivityNotFoundException;
    .end local v5    # "galleryIntent":Landroid/content/Intent;
    :pswitch_1
    new-instance v1, Landroid/content/Intent;

    const-string v9, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v1, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 541
    .local v1, "cameraIntent":Landroid/content/Intent;
    const-string v9, "output"

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempCropUri()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 542
    const-string v9, "outputFormat"

    sget-object v10, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v10}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 544
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const/16 v10, 0xc

    invoke-virtual {v9, v1, v10}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 545
    :catch_1
    move-exception v2

    .line 546
    .restart local v2    # "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 551
    .end local v1    # "cameraIntent":Landroid/content/Intent;
    .end local v2    # "e":Landroid/content/ActivityNotFoundException;
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v8

    .line 552
    .local v8, "transaction":Landroid/app/FragmentTransaction;
    const v9, 0x7f0e001e

    new-instance v10, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-direct {v10}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;-><init>()V

    invoke-virtual {v8, v9, v10}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 553
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 554
    invoke-virtual {v8}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_0

    .line 561
    .end local v8    # "transaction":Landroid/app/FragmentTransaction;
    :cond_2
    const/16 v9, 0xb

    if-ne p1, v9, :cond_6

    .line 562
    const/4 v9, -0x1

    if-ne p2, v9, :cond_0

    if-eqz p3, :cond_0

    .line 563
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 564
    .local v4, "from3rdparty":Landroid/net/Uri;
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempUri()Landroid/net/Uri;

    move-result-object v0

    .line 565
    .local v0, "_uri":Landroid/net/Uri;
    const/4 v7, 0x0

    .line 567
    .local v7, "isFrom3rdparty":Z
    if-eqz v4, :cond_3

    .line 568
    const/4 v7, 0x1

    .line 569
    invoke-direct {p0, v4, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->convert3rdpartyUri(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 574
    :cond_3
    if-eqz v0, :cond_4

    .line 575
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->recycleImage()V

    .line 576
    invoke-direct {p0, v0, v7}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setLogoImage(Landroid/net/Uri;Z)Z

    .line 577
    const/4 v9, 0x0

    invoke-direct {p0, v9}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setLogoClickable(Z)V

    .line 578
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 579
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 580
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogoText:Landroid/widget/TextView;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 581
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoText:Landroid/widget/TextView;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 582
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 584
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mcurrentUri:Landroid/net/Uri;

    .line 586
    :cond_4
    const/4 v9, 0x1

    iput v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I

    .line 587
    const-string v9, "logo_type_temp"

    iget v10, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 588
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    .line 589
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    if-eqz v9, :cond_5

    .line 591
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    invoke-direct {p0, v9, v10}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setEnableButton(Landroid/widget/Button;Z)V

    .line 593
    :cond_5
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 594
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    invoke-virtual {v9, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 596
    .end local v0    # "_uri":Landroid/net/Uri;
    .end local v4    # "from3rdparty":Landroid/net/Uri;
    .end local v7    # "isFrom3rdparty":Z
    :cond_6
    const/16 v9, 0xc

    if-ne p1, v9, :cond_7

    .line 597
    if-eqz p2, :cond_0

    .line 598
    new-instance v6, Landroid/content/Intent;

    const-string v9, "com.android.camera.action.CROP"

    invoke-direct {v6, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 599
    .local v6, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempCropUri()Landroid/net/Uri;

    move-result-object v9

    const-string v10, "image/*"

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 600
    const-string v9, "outputX"

    iget v10, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoWidth:I

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 601
    const-string v9, "outputY"

    iget v10, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoHeight:I

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 602
    const-string v9, "aspectX"

    const/4 v10, 0x1

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 603
    const-string v9, "aspectY"

    const/4 v10, 0x1

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 604
    const-string v9, "scale"

    const/4 v10, 0x1

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 605
    const-string v9, "output"

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempUri()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 606
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const/16 v10, 0xd

    invoke-virtual {v9, v6, v10}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 608
    .end local v6    # "intent":Landroid/content/Intent;
    :cond_7
    const/16 v9, 0xd

    if-ne p1, v9, :cond_0

    .line 609
    if-eqz p3, :cond_0

    if-eqz p2, :cond_0

    .line 610
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempUri()Landroid/net/Uri;

    move-result-object v0

    .line 611
    .restart local v0    # "_uri":Landroid/net/Uri;
    if-eqz v0, :cond_8

    .line 612
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->recycleImage()V

    .line 613
    const/4 v9, 0x0

    invoke-direct {p0, v0, v9}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setLogoImage(Landroid/net/Uri;Z)Z

    .line 614
    const/4 v9, 0x0

    invoke-direct {p0, v9}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setLogoClickable(Z)V

    .line 615
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 616
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 617
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogoText:Landroid/widget/TextView;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 618
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mcurrentUri:Landroid/net/Uri;

    .line 620
    :cond_8
    const/4 v9, 0x1

    iput v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I

    .line 621
    const-string v9, "logo_type_temp"

    iget v10, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 622
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    .line 623
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    if-eqz v9, :cond_9

    .line 624
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    invoke-direct {p0, v9, v10}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setEnableButton(Landroid/widget/Button;Z)V

    .line 626
    :cond_9
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 627
    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    invoke-virtual {v9, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 628
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempCropFile()Ljava/io/File;

    move-result-object v3

    .line 629
    .local v3, "file":Ljava/io/File;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 630
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    .line 516
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 363
    invoke-static {}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempFile()Ljava/io/File;

    move-result-object v0

    .line 364
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 365
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 367
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->onInitTempPreference()V

    .line 368
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "bool"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 853
    iput-boolean p2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbLogoEnable:Z

    .line 854
    const-string v1, "logo"

    invoke-static {v1, p2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 857
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 858
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDialog:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 864
    :cond_0
    :goto_0
    if-eqz p2, :cond_2

    .line 865
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mNotUsedLogoLayout:Landroid/widget/FrameLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 866
    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->changedFouceble(Z)V

    .line 876
    :cond_1
    :goto_1
    const-string v1, "change_notification_resume"

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 877
    return-void

    .line 860
    :catch_0
    move-exception v0

    .line 861
    .local v0, "e":Ljava/lang/ClassCastException;
    const-string v1, "VNSettingLogoFragment"

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 868
    .end local v0    # "e":Ljava/lang/ClassCastException;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mNotUsedLogoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 869
    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->changedFouceble(Z)V

    .line 871
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDialog:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;

    if-eqz v1, :cond_1

    .line 872
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDialog:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->dismissAllowingStateLoss()V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x1

    .line 372
    const-string v7, "VNSettingLogoFragment"

    const-string v8, "onClick"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->isResumed()Z

    move-result v7

    if-nez v7, :cond_1

    .line 374
    const-string v7, "VNSettingLogoFragment"

    const-string v8, "onClick - !isResumed()"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 378
    :cond_1
    invoke-static {}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempFile()Ljava/io/File;

    move-result-object v3

    .line 380
    .local v3, "file":Ljava/io/File;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    goto :goto_0

    .line 416
    :sswitch_0
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 417
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 419
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->onInitTempPreference()V

    .line 420
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/FragmentManager;->popBackStack()V

    goto :goto_0

    .line 383
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG"

    invoke-virtual {v7, v8}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    if-nez v7, :cond_0

    .line 384
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG"

    invoke-virtual {v7, v8}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;

    iput-object v7, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDialog:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;

    .line 386
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 388
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    new-instance v4, Landroid/content/Intent;

    const-string v7, "android.intent.action.PICK"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 389
    .local v4, "galleryIntent":Landroid/content/Intent;
    const-string v7, "image/*"

    invoke-virtual {v4, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 390
    const-string v7, "crop"

    const-string v8, "true"

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 391
    const-string v7, "outputX"

    iget v8, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoWidth:I

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 392
    const-string v7, "outputY"

    iget v8, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoHeight:I

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 393
    const-string v7, "aspectX"

    invoke-virtual {v4, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 394
    const-string v7, "aspectY"

    invoke-virtual {v4, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 395
    const-string v7, "scale"

    invoke-virtual {v4, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 396
    const-string v7, "set-as-image"

    invoke-virtual {v4, v7, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 397
    const-string v7, "output"

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempUri()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 398
    const-string v7, "outputFormat"

    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v8}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 399
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 402
    new-instance v1, Landroid/content/Intent;

    const-string v7, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v1, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 403
    .local v1, "cameraIntent":Landroid/content/Intent;
    const-string v7, "output"

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempCropUri()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 404
    const-string v7, "outputFormat"

    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v8}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407
    new-instance v7, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;

    invoke-direct {v7}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDialog:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;

    .line 408
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 409
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {v5}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    invoke-virtual {v0, v8, v7}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 410
    iget-object v7, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDialog:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;

    invoke-virtual {v7, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 411
    iget-object v7, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDialog:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    const-string v9, "FRAGMENT_DIALOG"

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 424
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "cameraIntent":Landroid/content/Intent;
    .end local v4    # "galleryIntent":Landroid/content/Intent;
    .end local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    :sswitch_2
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 425
    new-instance v6, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getFilesDir()Ljava/io/File;

    move-result-object v7

    const-string v8, "logo_image.jpg"

    invoke-direct {v6, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 427
    .local v6, "prevLogoFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 428
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 431
    :cond_3
    :try_start_0
    new-instance v7, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getFilesDir()Ljava/io/File;

    move-result-object v8

    const-string v9, "logo_image.jpg"

    invoke-direct {v7, v8, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v3, v7}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->move(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 436
    :goto_1
    const-string v7, "logo_filepath"

    const-string v8, "logo_image.jpg"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    .end local v6    # "prevLogoFile":Ljava/io/File;
    :cond_4
    const-string v7, "logo_type_temp"

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_5

    .line 439
    const-string v7, "logo_text"

    const-string v8, "logo_text_temp"

    invoke-static {v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const-string v7, "logo_text_font"

    const-string v8, "logo_text_font_temp"

    invoke-static {v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 441
    const-string v7, "logo_text_color"

    const-string v8, "logo_text_color_temp"

    invoke-static {v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 442
    const-string v7, "logo_bg_color"

    const-string v8, "logo_bg_color_temp"

    invoke-static {v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 443
    const-string v7, "logo_text_font_typeface"

    const-string v8, "logo_text_font_typeface_temp"

    invoke-static {v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->onInitTempPreference()V

    .line 447
    const-string v7, "logo_type"

    iget v8, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 448
    const-string v7, "logo"

    invoke-static {v7, v10}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 449
    iput-boolean v10, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbLogoEnable:Z

    .line 450
    const-string v7, "change_notification_resume"

    invoke-static {v7, v10}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 451
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/FragmentManager;->popBackStack()V

    .line 453
    iget-boolean v7, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    if-eqz v7, :cond_0

    .line 454
    const-string v7, "logo_text_changed"

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 432
    .restart local v6    # "prevLogoFile":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 434
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 380
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0e0001 -> :sswitch_0
        0x7f0e0002 -> :sswitch_2
        0x7f0e0042 -> :sswitch_1
        0x7f0e0045 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 114
    const-string v1, "VNSettingLogoFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    if-eqz p1, :cond_1

    .line 117
    const-string v1, "logo"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbLogoEnable:Z

    .line 118
    const-string v1, "logo"

    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbLogoEnable:Z

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 119
    const-string v1, "ok"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    .line 120
    const-string v1, "current"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 121
    .local v0, "uriString":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 122
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mcurrentUri:Landroid/net/Uri;

    .line 124
    :cond_0
    const-string v1, "logo"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 128
    .end local v0    # "uriString":Ljava/lang/String;
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 129
    return-void

    .line 126
    :cond_1
    const-string v1, "logo"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbLogoEnable:Z

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 17
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 168
    const-string v13, "VNSettingLogoFragment"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "onCreateView "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->CreateActionbar()V

    .line 172
    const v13, 0x7f03001a

    const/4 v14, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v13, v1, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    .line 173
    .local v12, "v":Landroid/view/View;
    const v13, 0x7f0e003c

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mNotUsedLogoLayout:Landroid/widget/FrameLayout;

    .line 176
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    const-string v14, "FRAGMENT_DIALOG"

    invoke-virtual {v13, v14}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 177
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    const-string v14, "FRAGMENT_DIALOG"

    invoke-virtual {v13, v14}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDialog:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    :cond_0
    :goto_0
    const v13, 0x7f0e0001

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mCancelButton:Landroid/widget/Button;

    .line 184
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mCancelButton:Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mCancelButton:Landroid/widget/Button;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 186
    const v13, 0x7f0e0002

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    .line 187
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 189
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setEnableButton(Landroid/widget/Button;Z)V

    .line 191
    const v13, 0x7f0e0042

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    .line 192
    const v13, 0x7f0e0043

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogoText:Landroid/widget/TextView;

    .line 193
    const v13, 0x7f0e0041

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoText:Landroid/widget/TextView;

    .line 194
    const v13, 0x7f0e0040

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    .line 195
    const v13, 0x7f0e0045

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    .line 197
    const-string v13, "logo"

    invoke-static {v13}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 198
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mNotUsedLogoLayout:Landroid/widget/FrameLayout;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 199
    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->changedFouceble(Z)V

    .line 208
    :cond_1
    :goto_1
    const-string v13, "logo_type_temp"

    invoke-static {v13}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I

    .line 209
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v13

    iget-object v5, v13, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 210
    .local v5, "locale":Ljava/util/Locale;
    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 220
    .local v3, "displayLanguage":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogoText:Landroid/widget/TextView;

    new-instance v14, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment$1;-><init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;)V

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 229
    const/4 v10, 0x0

    .line 231
    .local v10, "showlogo":I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I

    if-lez v13, :cond_6

    .line 232
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I

    .line 234
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    if-nez v13, :cond_2

    .line 235
    const-string v13, "logo_text_changed"

    invoke-static {v13}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    .line 238
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    if-eqz v13, :cond_3

    .line 239
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setEnableButton(Landroid/widget/Button;Z)V

    .line 249
    :cond_3
    :goto_2
    packed-switch v10, :pswitch_data_0

    .line 349
    :cond_4
    :goto_3
    return-object v12

    .line 179
    .end local v3    # "displayLanguage":Ljava/lang/String;
    .end local v5    # "locale":Ljava/util/Locale;
    .end local v10    # "showlogo":I
    :catch_0
    move-exception v4

    .line 180
    .local v4, "e":Ljava/lang/ClassCastException;
    const-string v13, "VNSettingLogoFragment"

    invoke-virtual {v4}, Ljava/lang/ClassCastException;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 201
    .end local v4    # "e":Ljava/lang/ClassCastException;
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mNotUsedLogoLayout:Landroid/widget/FrameLayout;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 202
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->changedFouceble(Z)V

    .line 203
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDialog:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;

    if-eqz v13, :cond_1

    .line 204
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDialog:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;

    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->dismissAllowingStateLoss()V

    goto :goto_1

    .line 242
    .restart local v3    # "displayLanguage":Ljava/lang/String;
    .restart local v5    # "locale":Ljava/util/Locale;
    .restart local v10    # "showlogo":I
    :cond_6
    const-string v13, "logo_type"

    invoke-static {v13}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v10

    .line 243
    const/4 v13, 0x2

    if-ne v10, v13, :cond_3

    .line 244
    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I

    .line 245
    const-string v13, "logo_type_temp"

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    goto :goto_2

    .line 251
    :pswitch_0
    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setLogoClickable(Z)V

    .line 252
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 253
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogoText:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 254
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 255
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoText:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 256
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    const/16 v14, 0x16

    const/16 v15, 0x61

    const/16 v16, 0x6e

    invoke-static/range {v14 .. v16}, Landroid/graphics/Color;->rgb(III)I

    move-result v14

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 257
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_3

    .line 261
    :pswitch_1
    const-string v13, "logo_filepath"

    invoke-static {v13}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    if-nez v13, :cond_7

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I

    if-lez v13, :cond_4

    invoke-static {}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempFile()Ljava/io/File;

    move-result-object v13

    if-eqz v13, :cond_4

    .line 263
    :cond_7
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setLogoClickable(Z)V

    .line 264
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 265
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 266
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogoText:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 267
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoText:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 268
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 269
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/Button;->setVisibility(I)V

    .line 270
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 272
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2}, Landroid/graphics/Canvas;-><init>()V

    .line 273
    .local v2, "canvas":Landroid/graphics/Canvas;
    const/4 v6, 0x0

    .line 275
    .local v6, "mainImage":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I

    if-lez v13, :cond_9

    .line 276
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempUri()Landroid/net/Uri;

    move-result-object v13

    invoke-virtual {v13}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 283
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0201a6

    invoke-static {v13, v14}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 285
    .local v11, "tmpMask":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_8

    if-nez v11, :cond_b

    .line 286
    :cond_8
    const-string v13, "VNSettingLogoFragment"

    const-string v14, "onCreateView() : reset logo to default"

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const-string v13, "logo_type_temp"

    const/4 v14, 0x0

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 288
    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setLogoClickable(Z)V

    .line 289
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 290
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogoText:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 291
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 292
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoText:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 293
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    const/16 v14, 0x16

    const/16 v15, 0x61

    const/16 v16, 0x6e

    invoke-static/range {v14 .. v16}, Landroid/graphics/Color;->rgb(III)I

    move-result v14

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 294
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/Button;->setVisibility(I)V

    .line 295
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    .line 296
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setEnableButton(Landroid/widget/Button;Z)V

    goto/16 :goto_3

    .line 277
    .end local v11    # "tmpMask":Landroid/graphics/Bitmap;
    :cond_9
    const-string v13, "logo_filepath"

    invoke-static {v13}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_a

    .line 278
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    invoke-virtual {v14}, Landroid/app/Activity;->getFilesDir()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "logo_image.jpg"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    goto/16 :goto_4

    .line 280
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempUri()Landroid/net/Uri;

    move-result-object v13

    invoke-virtual {v13}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    goto/16 :goto_4

    .line 300
    .restart local v11    # "tmpMask":Landroid/graphics/Bitmap;
    :cond_b
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    const/4 v15, 0x1

    invoke-static {v11, v13, v14, v15}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 301
    .local v7, "mask":Landroid/graphics/Bitmap;
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    .line 302
    const/4 v11, 0x0

    .line 304
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    sget-object v15, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v13, v14, v15}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 306
    .local v9, "result":Landroid/graphics/Bitmap;
    invoke-virtual {v2, v9}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 307
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    .line 308
    .local v8, "paint":Landroid/graphics/Paint;
    const/4 v13, 0x0

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 310
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v2, v6, v13, v14, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 311
    new-instance v13, Landroid/graphics/PorterDuffXfermode;

    sget-object v14, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v13, v14}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 312
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v2, v7, v13, v14, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 313
    const/4 v13, 0x0

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 314
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    invoke-virtual {v13, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_3

    .line 319
    .end local v2    # "canvas":Landroid/graphics/Canvas;
    .end local v6    # "mainImage":Landroid/graphics/Bitmap;
    .end local v7    # "mask":Landroid/graphics/Bitmap;
    .end local v8    # "paint":Landroid/graphics/Paint;
    .end local v9    # "result":Landroid/graphics/Bitmap;
    .end local v11    # "tmpMask":Landroid/graphics/Bitmap;
    :pswitch_2
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setLogoClickable(Z)V

    .line 320
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 321
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogoText:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 322
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoText:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 323
    const-string v13, "logo_text_temp"

    invoke-static {v13}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_d

    .line 324
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoText:Landroid/widget/TextView;

    const-string v14, "logo_text_temp"

    invoke-static {v14}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoText:Landroid/widget/TextView;

    const-string v14, "logo_text_font_typeface_temp"

    invoke-static {v14}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getTextFont(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 327
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoText:Landroid/widget/TextView;

    const-string v14, "logo_text_color_temp"

    invoke-static {v14}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 329
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    const-string v14, "logo_bg_color_temp"

    invoke-static {v14}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 331
    const-string v13, "logo_bg_color_temp"

    invoke-static {v13}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v13

    const v14, 0x106000d

    if-ne v13, v14, :cond_c

    .line 332
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 345
    :cond_c
    :goto_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/Button;->setVisibility(I)V

    .line 346
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    .line 335
    :cond_d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoText:Landroid/widget/TextView;

    const-string v14, "logo_text"

    invoke-static {v14}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoText:Landroid/widget/TextView;

    const-string v14, "logo_text_font_typeface"

    invoke-static {v14}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getTextFont(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 338
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoText:Landroid/widget/TextView;

    const-string v14, "logo_text_color"

    invoke-static {v14}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 339
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    const-string v14, "logo_bg_color"

    invoke-static {v14}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 340
    const-string v13, "logo_bg_color"

    invoke-static {v13}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v13

    const v14, 0x106000d

    if-ne v13, v14, :cond_c

    .line 341
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    .line 249
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 498
    const-string v1, "VNSettingLogoFragment"

    const-string v2, "onDestroyView"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 500
    .local v0, "actionbar":Landroid/app/ActionBar;
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 502
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBar:Landroid/app/ActionBar;

    if-eqz v1, :cond_0

    .line 503
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 504
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mActionBar:Landroid/app/ActionBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 507
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->recycleImage()V

    .line 508
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 509
    return-void
.end method

.method public onInitTempPreference()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 879
    const-string v0, "logo_type_temp"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 880
    const-string v0, "logo_text_temp"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    const-string v0, "logo_text_font_temp"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 882
    const-string v0, "logo_text_color_temp"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 883
    const-string v0, "logo_bg_color_temp"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 884
    const-string v0, "logo_text_font_typeface_temp"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 885
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 838
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 848
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1

    .line 840
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->onBackPressed()V

    .line 842
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->popBackStack()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 843
    :catch_0
    move-exception v0

    .line 844
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "VNSettingLogoFragment"

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 838
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 464
    const-string v2, "VNSettingLogoFragment"

    const-string v3, "onResume"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    const-string v2, "logo_type"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "logo_type_temp"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v4, :cond_0

    .line 468
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->getTempUri()Landroid/net/Uri;

    move-result-object v0

    .line 469
    .local v0, "_uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 470
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->recycleImage()V

    .line 471
    invoke-direct {p0, v0, v6}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setLogoImage(Landroid/net/Uri;Z)Z

    move-result v1

    .line 472
    .local v1, "isSetLogo":Z
    if-ne v1, v4, :cond_1

    .line 473
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogoText:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 474
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 475
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 476
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mcurrentUri:Landroid/net/Uri;

    .line 493
    .end local v0    # "_uri":Landroid/net/Uri;
    .end local v1    # "isSetLogo":Z
    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 494
    return-void

    .line 478
    .restart local v0    # "_uri":Landroid/net/Uri;
    .restart local v1    # "isSetLogo":Z
    :cond_1
    const-string v2, "VNSettingLogoFragment"

    const-string v3, "onResume() : reset logo to default"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    const-string v2, "logo_type_temp"

    invoke-static {v2, v6}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 480
    invoke-direct {p0, v4}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setLogoClickable(Z)V

    .line 481
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 482
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;

    const v3, 0x7f02007a

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 483
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogoText:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 484
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 485
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoText:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 486
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mLogoBG:Landroid/widget/ImageView;

    const/16 v3, 0x15

    const/16 v4, 0x19

    const/16 v5, 0x1c

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 487
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mChangeButton:Landroid/widget/Button;

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 488
    iput-boolean v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    .line 489
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mDoneButton:Landroid/widget/Button;

    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setEnableButton(Landroid/widget/Button;Z)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 354
    const-string v0, "logo"

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbLogoEnable:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 355
    const-string v0, "ok"

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mbOKEnable:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mcurrentUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 357
    const-string v0, "current"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mcurrentUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 360
    return-void
.end method
