.class final enum Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;
.super Ljava/lang/Enum;
.source "VNSTTFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ViewState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

.field public static final enum Converted:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

.field public static final enum Converting:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

.field public static final enum Empty:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 80
    new-instance v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    const-string v1, "Empty"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Empty:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    new-instance v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    const-string v1, "Converting"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Converting:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    new-instance v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    const-string v1, "Converted"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Converted:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    .line 79
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    sget-object v1, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Empty:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Converting:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Converted:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->$VALUES:[Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 79
    const-class v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->$VALUES:[Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    invoke-virtual {v0}, [Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    return-object v0
.end method
