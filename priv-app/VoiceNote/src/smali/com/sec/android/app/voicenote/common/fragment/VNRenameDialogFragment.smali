.class public Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNRenameDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;
    }
.end annotation


# static fields
.field private static final CURRENT_FILE_ID:Ljava/lang/String; = "currentFileId"

.field private static final INPUT_TEXT:Ljava/lang/String; = "InputText"

.field private static final INVALID_CHARS:[Ljava/lang/String;

.field private static final IS_INPUT_TEXT:Ljava/lang/String; = "isInputText"

.field private static final IS_VISIBLE_WINDOW:Ljava/lang/String; = "AxT9IME.isVisibleWindow"

.field public static MAX_LENGTH:I = 0x0

.field public static final MAX_PATH_LENGTH:I = 0xf0

.field public static MOUNT_MEMORYCARD_MAX_LENGTH:I = 0x0

.field public static final RENAME_BOOKMARK:I = 0x2

.field public static final RENAME_DEFAULTNAME:I = 0x3

.field public static final RENAME_LIST:I = 0x1

.field public static RENAME_MAX_LENGTH:I = 0x0

.field public static final RENAME_PRIVATE:I = 0x4

.field private static final RESPONSE_AXT9INFO:Ljava/lang/String; = "ResponseAxT9Info"

.field private static final TAG:Ljava/lang/String; = "VNRenameDialogFragment"


# instance fields
.field private bShowKeyboard:Z

.field private changeFileName:Ljava/lang/String;

.field private currentFileId:J

.field private mActivity:Landroid/app/Activity;

.field private mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

.field private mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;

.field private mIsSaveInstance:Ljava/lang/Boolean;

.field private mReceiveTime:J

.field private mRenameMode:I

.field private mchangeText:Ljava/lang/Boolean;

.field private medit:Landroid/widget/EditText;

.field private prevFile:Ljava/io/File;

.field private prevFileName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 69
    const/16 v0, 0x32

    sput v0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->MAX_LENGTH:I

    .line 70
    sget v0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->MAX_LENGTH:I

    sput v0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->MOUNT_MEMORYCARD_MAX_LENGTH:I

    .line 71
    sput v2, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->RENAME_MAX_LENGTH:I

    .line 73
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "\\"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "/"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\n"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->INVALID_CHARS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 100
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 77
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    .line 78
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    .line 80
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->bShowKeyboard:Z

    .line 83
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFileName:Ljava/lang/String;

    .line 84
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->changeFileName:Ljava/lang/String;

    .line 85
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFile:Ljava/io/File;

    .line 86
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->currentFileId:J

    .line 88
    iput v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mRenameMode:I

    .line 89
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mReceiveTime:J

    .line 90
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;

    .line 91
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mchangeText:Ljava/lang/Boolean;

    .line 92
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mIsSaveInstance:Ljava/lang/Boolean;

    .line 101
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mRenameMode:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->refreshPositiveButton()V

    return-void
.end method

.method static synthetic access$402(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->changeFileName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->INVALID_CHARS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->bShowKeyboard:Z

    return p1
.end method

.method static synthetic access$802(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;
    .param p1, "x1"    # J

    .prologue
    .line 57
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mReceiveTime:J

    return-wide p1
.end method

.method public static getNameFilter(Landroid/app/Activity;ZI)[Landroid/text/InputFilter;
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "isIncludeCount"    # Z
    .param p2, "fixedMaxLength"    # I

    .prologue
    .line 301
    const/4 v2, 0x2

    new-array v0, v2, [Landroid/text/InputFilter;

    .line 302
    .local v0, "filterArray":[Landroid/text/InputFilter;
    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$6;-><init>(Landroid/app/Activity;)V

    aput-object v3, v0, v2

    .line 373
    const/4 v1, 0x0

    .line 374
    .local v1, "max_length":I
    if-eqz p1, :cond_0

    .line 375
    sget v2, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->RENAME_MAX_LENGTH:I

    if-ge v2, p2, :cond_1

    .line 376
    move v1, p2

    .line 381
    :cond_0
    :goto_0
    const/4 v2, 0x1

    new-instance v3, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$7;

    invoke-direct {v3, v1, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$7;-><init>(ILandroid/app/Activity;)V

    aput-object v3, v0, v2

    .line 392
    return-object v0

    .line 378
    :cond_1
    sget v1, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->RENAME_MAX_LENGTH:I

    goto :goto_0
.end method

.method private refreshPositiveButton()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 529
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    if-nez v2, :cond_1

    .line 549
    :cond_0
    :goto_0
    return-void

    .line 533
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    check-cast v2, Landroid/app/AlertDialog;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 534
    .local v0, "btn":Landroid/widget/Button;
    if-eqz v0, :cond_0

    .line 535
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 536
    .local v1, "trimmedStr":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mchangeText:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mIsSaveInstance:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 537
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 538
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mIsSaveInstance:Ljava/lang/Boolean;

    goto :goto_0

    .line 540
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 541
    :cond_3
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 542
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mchangeText:Ljava/lang/Boolean;

    goto :goto_0

    .line 544
    :cond_4
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 545
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mchangeText:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public static setArguments(ILjava/lang/String;JI)Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;
    .locals 4
    .param p0, "renameMode"    # I
    .param p1, "origin_text"    # Ljava/lang/String;
    .param p2, "id"    # J
    .param p4, "titleResId"    # I

    .prologue
    .line 104
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;-><init>()V

    .line 105
    .local v1, "dialogFragment":Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 106
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "id"

    invoke-virtual {v0, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 107
    const-string v2, "title"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 108
    const-string v2, "origintext"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v2, "mode"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 110
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 111
    return-object v1
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 568
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 569
    if-eqz p1, :cond_0

    .line 570
    const-string v1, "isInputText"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mchangeText:Ljava/lang/Boolean;

    .line 571
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNBookmarkListFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    .line 572
    .local v0, "frag":Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    if-eqz v0, :cond_0

    .line 573
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getCallback()Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;

    .line 576
    .end local v0    # "frag":Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 116
    instance-of v0, p1, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 117
    check-cast v0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;

    .line 119
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 120
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 486
    const-string v0, "VNRenameDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 489
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 128
    const-string v9, "VNRenameDialogFragment"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onCreateDialog "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const/16 v9, 0x32

    sput v9, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->MAX_LENGTH:I

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "title"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {p0, v9}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 131
    .local v8, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "mode"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mRenameMode:I

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    .line 133
    const v5, 0x7f03000e

    .line 134
    .local v5, "layoutId":I
    const/4 v4, 0x0

    .line 135
    .local v4, "layout":Landroid/widget/LinearLayout;
    if-eqz p1, :cond_4

    .line 136
    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mIsSaveInstance:Ljava/lang/Boolean;

    .line 137
    const-string v9, "InputText"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFileName:Ljava/lang/String;

    .line 138
    iget v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mRenameMode:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_2

    .line 139
    const-string v9, "currentFileId"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    iput-wide v10, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->currentFileId:J

    .line 140
    iget-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    iget-wide v10, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->currentFileId:J

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 141
    .local v6, "prevFilePath":Ljava/lang/String;
    if-nez v6, :cond_0

    .line 142
    const-string v9, "VNRenameDialogFragment"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "createRenameDialog id has valid but the path is null : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->currentFileId:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_0
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFile:Ljava/io/File;

    .line 147
    const v5, 0x7f03000f

    .line 187
    .end local v6    # "prevFilePath":Ljava/lang/String;
    :cond_1
    :goto_0
    sget v9, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->MAX_LENGTH:I

    sput v9, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->RENAME_MAX_LENGTH:I

    .line 188
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 189
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 191
    iget-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v5, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .end local v4    # "layout":Landroid/widget/LinearLayout;
    check-cast v4, Landroid/widget/LinearLayout;

    .line 192
    .restart local v4    # "layout":Landroid/widget/LinearLayout;
    const v9, 0x7f0e0025

    invoke-virtual {v4, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 194
    .local v3, "editText":Landroid/widget/EditText;
    iput-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    .line 195
    iget-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    const-string v10, "disableVoiceInput=false;inputType=filename;disableEmoticonInput=true"

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 197
    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setSelected(Z)V

    .line 198
    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 199
    const v9, 0x84001

    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setInputType(I)V

    .line 201
    iget-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFileName:Ljava/lang/String;

    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 202
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 203
    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 204
    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    .line 205
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 208
    const v9, 0x104000a

    new-instance v10, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;

    invoke-direct {v10, p0, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v0, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 241
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 242
    const/high16 v9, 0x1040000

    new-instance v10, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$2;

    invoke-direct {v10, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)V

    invoke-virtual {v0, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 252
    iget-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFileName:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getNameFilter(Landroid/app/Activity;ZI)[Landroid/text/InputFilter;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 254
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 256
    .local v1, "dialog":Landroid/app/AlertDialog;
    new-instance v9, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$3;

    invoke-direct {v9, p0, v1, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$3;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;Landroid/app/AlertDialog;Landroid/widget/EditText;)V

    invoke-virtual {v3, v9}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 274
    new-instance v9, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$4;

    invoke-direct {v9, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$4;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)V

    invoke-virtual {v1, v9}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 280
    new-instance v9, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$5;

    invoke-direct {v9, p0, v1}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$5;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;Landroid/app/AlertDialog;)V

    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 293
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->bShowKeyboard:Z

    .line 294
    iget-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFileName:Ljava/lang/String;

    iput-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->changeFileName:Ljava/lang/String;

    .line 295
    return-object v1

    .line 148
    .end local v0    # "alertDialog":Landroid/app/AlertDialog$Builder;
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    .end local v3    # "editText":Landroid/widget/EditText;
    :cond_2
    iget v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mRenameMode:I

    const/4 v10, 0x2

    if-eq v9, v10, :cond_3

    iget v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mRenameMode:I

    const/4 v10, 0x3

    if-ne v9, v10, :cond_1

    .line 149
    :cond_3
    const v5, 0x7f03000e

    goto/16 :goto_0

    .line 152
    :cond_4
    iget v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mRenameMode:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_7

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "id"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    iput-wide v10, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->currentFileId:J

    .line 154
    iget-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    iget-wide v10, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->currentFileId:J

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 155
    .restart local v6    # "prevFilePath":Ljava/lang/String;
    if-nez v6, :cond_5

    .line 156
    const-string v9, "VNRenameDialogFragment"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "createRenameDialog id has valid but the path is null : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->currentFileId:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_5
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFile:Ljava/io/File;

    .line 161
    iget-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFile:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    .line 162
    .local v7, "selelectedFileName":Ljava/lang/String;
    const/16 v9, 0x2e

    invoke-virtual {v7, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 164
    .local v2, "dotIndex":I
    const/4 v9, -0x1

    if-eq v2, v9, :cond_6

    .line 165
    const/4 v9, 0x0

    invoke-virtual {v7, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 167
    :cond_6
    iput-object v7, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFileName:Ljava/lang/String;

    .line 168
    const v5, 0x7f03000f

    .line 169
    goto/16 :goto_0

    .end local v2    # "dotIndex":I
    .end local v6    # "prevFilePath":Ljava/lang/String;
    .end local v7    # "selelectedFileName":Ljava/lang/String;
    :cond_7
    iget v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mRenameMode:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_8

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "origintext"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFileName:Ljava/lang/String;

    .line 171
    const v5, 0x7f03000e

    goto/16 :goto_0

    .line 172
    :cond_8
    iget v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mRenameMode:I

    const/4 v10, 0x3

    if-ne v9, v10, :cond_9

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "origintext"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFileName:Ljava/lang/String;

    .line 174
    const v5, 0x7f03000e

    .line 175
    const/16 v9, 0x2e

    sput v9, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->MAX_LENGTH:I

    goto/16 :goto_0

    .line 176
    :cond_9
    iget v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mRenameMode:I

    const/4 v10, 0x4

    if-ne v9, v10, :cond_1

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "origintext"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFileName:Ljava/lang/String;

    .line 178
    iget-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFileName:Ljava/lang/String;

    const/16 v10, 0x2e

    invoke-virtual {v9, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 180
    .restart local v2    # "dotIndex":I
    const/4 v9, -0x1

    if-eq v2, v9, :cond_1

    .line 181
    iget-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFileName:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-virtual {v9, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFileName:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 493
    const-string v0, "VNRenameDialogFragment"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 496
    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 478
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 479
    .local v0, "mInputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 480
    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->registerBroadcastReceiverSip(Z)V

    .line 481
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 482
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 444
    const-string v0, "VNRenameDialogFragment"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->registerBroadcastReceiverSip(Z)V

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->bShowKeyboard:Z

    if-nez v0, :cond_1

    .line 448
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 473
    :goto_0
    return-void

    .line 451
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isCursorVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 458
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$8;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 472
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 560
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 561
    const-string v0, "isInputText"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mchangeText:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 562
    const-string v0, "InputText"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->changeFileName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    const-string v0, "currentFileId"

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->currentFileId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 564
    return-void
.end method

.method public registerBroadcastReceiverSip(Z)V
    .locals 6
    .param p1, "register"    # Z

    .prologue
    .line 499
    if-eqz p1, :cond_2

    .line 500
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 526
    :cond_0
    :goto_0
    return-void

    .line 504
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$9;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    .line 513
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 514
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "ResponseAxT9Info"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 516
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 518
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mReceiveTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x190

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    .line 519
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->bShowKeyboard:Z

    .line 521
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 522
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 523
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mBroadcastReceiverSip:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method renameFile()V
    .locals 13

    .prologue
    const/4 v12, -0x1

    const v11, 0x7f0b0083

    const/4 v10, 0x0

    .line 396
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFileName:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFile:Ljava/io/File;

    if-eqz v5, :cond_0

    iget-wide v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->currentFileId:J

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    .line 440
    :cond_0
    :goto_0
    return-void

    .line 400
    :cond_1
    new-instance v2, Ljava/lang/StringBuffer;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 401
    .local v2, "destFileName":Ljava/lang/StringBuffer;
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFileName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_2

    .line 402
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v5, v11, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto :goto_0

    .line 405
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-gtz v5, :cond_3

    .line 406
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    const v6, 0x7f0b0078

    invoke-static {v5, v6, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto :goto_0

    .line 409
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 410
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 411
    .local v3, "index":I
    if-eq v3, v12, :cond_4

    .line 412
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 417
    .end local v3    # "index":I
    :cond_4
    new-instance v1, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->prevFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 421
    .local v1, "destFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0xf0

    if-le v5, v6, :cond_5

    .line 422
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    const v6, 0x7f0b00ac

    invoke-static {v5, v6, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 424
    :cond_5
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExistFile(Ljava/io/File;)Z

    move-result v5

    sget-boolean v6, Lcom/sec/android/app/voicenote/common/util/VNUtil;->FUNC_RETURN_CORRECT:Z

    if-ne v5, v6, :cond_6

    .line 425
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v5, v11, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 428
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    iget-wide v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->currentFileId:J

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 429
    .local v0, "Curpath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 430
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    iget-wide v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->currentFileId:J

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v6, v7, v0, v8}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->renameFile(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 431
    .local v4, "result":I
    if-ne v4, v12, :cond_7

    .line 432
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    const v6, 0x7f0b0086

    invoke-static {v5, v6, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 434
    :cond_7
    if-nez v4, :cond_0

    .line 435
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v5, v11, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_0
.end method

.method public resetOriginalName(Ljava/lang/String;)V
    .locals 2
    .param p1, "origin_text"    # Ljava/lang/String;

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 553
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "origintext"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->medit:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 556
    :cond_0
    return-void
.end method

.method public setCallback(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;

    .line 124
    return-void
.end method
