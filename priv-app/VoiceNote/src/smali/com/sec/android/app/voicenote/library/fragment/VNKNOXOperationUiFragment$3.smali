.class Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$3;
.super Ljava/lang/Object;
.source "VNKNOXOperationUiFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->createProgressDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 330
    const-string v0, "2.0"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXVersion:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$600(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOX2OperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOX2OperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 333
    const-string v0, "VNKNOXOperationUiFragment"

    const-string v1, "NegativeButton() : cancel runned task"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOX2OperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->cancel(Z)Z

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->access$800(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)Lcom/sec/knox/containeragent/ContainerInstallerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    iget-object v1, v1, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->KNOXFileRelayCancel(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0
.end method
