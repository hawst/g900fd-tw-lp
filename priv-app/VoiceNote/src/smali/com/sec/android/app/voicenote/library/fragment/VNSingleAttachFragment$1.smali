.class Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "VNSingleAttachFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->onCreateAnimator(IZI)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mLibraryCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mLibraryCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->TransitAnimationState(Z)V

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 205
    :cond_1
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 206
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mLibraryCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mLibraryCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->TransitAnimationState(Z)V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 194
    :cond_1
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationStart(Landroid/animation/Animator;)V

    .line 195
    return-void
.end method
