.class Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$2;
.super Ljava/lang/Object;
.source "VNDataCheckHelpDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 69
    const-string v0, "record_mode"

    const-string v1, "record_mode_prev"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$Callbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$Callbacks;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$Callbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$Callbacks;

    move-result-object v0

    const-string v1, "record_mode_prev"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$Callbacks;->onModeChanged(I)V

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->dismiss()V

    .line 74
    return-void
.end method
