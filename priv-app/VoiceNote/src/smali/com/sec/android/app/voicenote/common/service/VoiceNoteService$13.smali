.class Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$13;
.super Landroid/content/BroadcastReceiver;
.source "VoiceNoteService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->registerEasyModeReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0

    .prologue
    .line 2045
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$13;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2048
    const-string v1, "VoiceNoteService"

    const-string v2, "registerEasyModeReceiver com.android.launcher.action.EASY_MODE_CHANGE_VOICENOTE"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2049
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$13;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    .line 2050
    .local v0, "currentEasymode":Z
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$13;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsEasymode:Z
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2100(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Z

    move-result v1

    if-eq v1, v0, :cond_1

    .line 2051
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$13;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsEasymode:Z
    invoke-static {v1, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2102(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z

    .line 2057
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$13;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v1

    const/16 v2, 0x3eb

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$13;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v1

    const/16 v2, 0x3ec

    if-ne v1, v2, :cond_1

    .line 2059
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$13;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->isNotificationShow()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2060
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$13;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecordingOnBackground()Ljava/lang/String;

    .line 2061
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$13;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    .line 2067
    :cond_1
    :goto_0
    return-void

    .line 2063
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$13;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecording()Z

    goto :goto_0
.end method
