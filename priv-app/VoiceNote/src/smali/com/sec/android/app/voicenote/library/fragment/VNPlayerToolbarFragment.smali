.class public Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
.super Landroid/app/Fragment;
.source "VNPlayerToolbarFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;,
        Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;
    }
.end annotation


# static fields
.field private static final BOOKMARK_ANIMATION_DURATION:J = 0xc8L

.field private static final INTERNAL_EVENT_PROGRESS:I = 0x8281

.field private static final INTERNAL_EVENT_REPEAT_RESTORE:I = 0x1228

.field private static final INTERNAL_EVENT_WAIT_RESOURCE_LOADING:I = 0x1982

.field private static final TAG:Ljava/lang/String; = "VNPlyayerToolbarFragment"

.field private static mBookmarks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field private static mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

.field private static mSaveMode:I

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# instance fields
.field private INDICATOR_WIDTH:I

.field private final TRIMSAVE_REQUESTCODE:I

.field private mBookmarkButton:Landroid/widget/ImageButton;

.field private mBookmarkIndicators:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mBookmarkLayout:Landroid/widget/RelativeLayout;

.field private mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field private mDuration:I

.field private mDurationTextView:Landroid/widget/TextView;

.field private mElapseTimeTextView:Landroid/widget/TextView;

.field public mEventHandler:Landroid/os/Handler;

.field private mFileName:Landroid/widget/TextView;

.field private mIsCreatedEasyMode:Z

.field private mIsProgressRun:Z

.field private mIsResumePlay:Z

.field private mOldParams:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;",
            ">;"
        }
    .end annotation
.end field

.field private mOnkeyListener:Landroid/view/View$OnKeyListener;

.field private mParams:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;",
            ">;"
        }
    .end annotation
.end field

.field private mPlaySpeedButton:Landroid/widget/ImageButton;

.field private mPlayerToolbar:Landroid/widget/LinearLayout;

.field mProgressDialog:Landroid/app/ProgressDialog;

.field private mProgressLayout:Landroid/widget/FrameLayout;

.field private mRepeatAIndicator:Landroid/widget/ImageView;

.field private mRepeatAPosition:I

.field private mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

.field private mRepeatBIndicator:Landroid/widget/ImageView;

.field private mRepeatBPosition:I

.field private mRepeatButton:Landroid/widget/ImageButton;

.field private mRepeatLayout:Landroid/widget/RelativeLayout;

.field private mRepeatSeekbar:Landroid/widget/SeekBar;

.field private mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

.field private mSeekbar:Landroid/widget/SeekBar;

.field private mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

.field private mSkipSilenceButton:Landroid/widget/ImageButton;

.field private mTempButton:Landroid/widget/ImageButton;

.field private mToggleValue:I

.field private mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

.field private mTrimButton:Landroid/widget/ImageButton;

.field onTouchListener:Landroid/view/View$OnTouchListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 83
    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 97
    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    .line 114
    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    .line 120
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveMode:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 129
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 82
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->TRIMSAVE_REQUESTCODE:I

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlayerToolbar:Landroid/widget/LinearLayout;

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressLayout:Landroid/widget/FrameLayout;

    .line 86
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsCreatedEasyMode:Z

    .line 87
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    .line 88
    iput v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    .line 89
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mElapseTimeTextView:Landroid/widget/TextView;

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDurationTextView:Landroid/widget/TextView;

    .line 91
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mFileName:Landroid/widget/TextView;

    .line 92
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkLayout:Landroid/widget/RelativeLayout;

    .line 93
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimButton:Landroid/widget/ImageButton;

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    .line 98
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    .line 99
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mParams:Ljava/util/ArrayList;

    .line 100
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mOldParams:Ljava/util/ArrayList;

    .line 101
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    .line 102
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    .line 103
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatButton:Landroid/widget/ImageButton;

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatLayout:Landroid/widget/RelativeLayout;

    .line 106
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    .line 107
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    .line 108
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    .line 109
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    .line 111
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsResumePlay:Z

    .line 112
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTempButton:Landroid/widget/ImageButton;

    .line 119
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSkipSilenceButton:Landroid/widget/ImageButton;

    .line 122
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkButton:Landroid/widget/ImageButton;

    .line 123
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    .line 124
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mOnkeyListener:Landroid/view/View$OnKeyListener;

    .line 125
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsProgressRun:Z

    .line 126
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mToggleValue:I

    .line 404
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->onTouchListener:Landroid/view/View$OnTouchListener;

    .line 1089
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    .line 131
    return-void
.end method

.method static synthetic access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mParams:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->prepareParams()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setIndicators()V

    return-void
.end method

.method static synthetic access$1402(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;)Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    .param p1, "x1"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTempButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;Landroid/widget/ImageButton;)Landroid/widget/ImageButton;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    .param p1, "x1"    # Landroid/widget/ImageButton;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTempButton:Landroid/widget/ImageButton;

    return-object p1
.end method

.method static synthetic access$1600()I
    .locals 1

    .prologue
    .line 79
    sget v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveMode:I

    return v0
.end method

.method static synthetic access$1602(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 79
    sput p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveMode:I

    return p0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setProgressDialog()V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->restoreRepeat()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->showBookmarkIndicators()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->executeProgress()V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    .param p1, "x1"    # I

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->convertTimeFormat(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsResumePlay:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsResumePlay:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    return-object v0
.end method

.method static synthetic access$900()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$902(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Ljava/util/ArrayList;

    .prologue
    .line 79
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    return-object p0
.end method

.method private convertTimeFormat(I)Ljava/lang/String;
    .locals 7
    .param p1, "millisec"    # I

    .prologue
    .line 981
    div-int/lit16 v2, p1, 0x3e8

    .line 982
    .local v2, "sec":I
    div-int/lit8 v3, v2, 0x3c

    rem-int/lit8 v1, v3, 0x3c

    .line 983
    .local v1, "min":I
    div-int/lit16 v0, v2, 0xe10

    .line 984
    .local v0, "hour":I
    rem-int/lit8 v2, v2, 0x3c

    .line 985
    const-string v3, "%02d:%02d:%02d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private executeProgress()V
    .locals 5

    .prologue
    const v4, 0x8281

    .line 945
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->isRemoving()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->isDetached()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 978
    :cond_0
    :goto_0
    return-void

    .line 949
    :cond_1
    const/4 v0, 0x0

    .line 951
    .local v0, "currentPostion":I
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsProgressRun:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPausedForaWhile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 952
    :cond_2
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPositionForRepeat()I

    move-result v0

    .line 953
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    if-lt v1, v0, :cond_3

    if-gez v0, :cond_5

    .line 954
    :cond_3
    const-string v1, "VNPlyayerToolbarFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startProgress : invalid progress position : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " duration : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 974
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 975
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1e

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 956
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mElapseTimeTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_6

    .line 957
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mElapseTimeTextView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->convertTimeFormat(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 960
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getVisibility()I

    move-result v1

    if-nez v1, :cond_8

    .line 961
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    if-le v1, v3, :cond_7

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    :goto_2
    sub-int v1, v0, v1

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_1

    :cond_7
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    goto :goto_2

    .line 962
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_a

    .line 963
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    add-int/lit8 v1, v1, -0x32

    if-le v0, v1, :cond_9

    .line 964
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    .line 966
    :cond_9
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mToggleValue:I

    if-eq v0, v1, :cond_4

    .line 967
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_1

    .line 970
    :cond_a
    const-string v1, "VNPlyayerToolbarFragment"

    const-string v2, "startProgress : seekbar is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private executeProgressCallback(Landroid/widget/SeekBar;I)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I

    .prologue
    .line 989
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    if-eqz v0, :cond_0

    .line 990
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 991
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    invoke-interface {v0, p2}, Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;->onProgressChanged(I)V

    .line 1000
    :cond_0
    :goto_0
    return-void

    .line 993
    :cond_1
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    if-ge v0, v1, :cond_2

    .line 994
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    add-int/2addr v1, p2

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;->onProgressChanged(I)V

    goto :goto_0

    .line 996
    :cond_2
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    add-int/2addr v1, p2

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;->onProgressChanged(I)V

    goto :goto_0
.end method

.method private hideBookmarkIndicators()V
    .locals 4

    .prologue
    .line 778
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_0

    .line 779
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 780
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 781
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 780
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 784
    .end local v0    # "i":I
    .end local v1    # "size":I
    :cond_0
    return-void
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 923
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 924
    return-void
.end method

.method private isAnimationNeeded(I)Z
    .locals 3
    .param p1, "i"    # I

    .prologue
    const/4 v0, 0x1

    .line 1578
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mOldParams:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mParams:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 1586
    :cond_0
    :goto_0
    return v0

    .line 1582
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mOldParams:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mParams:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1583
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized prepareParams()Z
    .locals 14

    .prologue
    const/4 v10, 0x0

    .line 841
    monitor-enter p0

    :try_start_0
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    if-eqz v9, :cond_0

    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v9, :cond_1

    :cond_0
    move v9, v10

    .line 885
    :goto_0
    monitor-exit p0

    return v9

    .line 844
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v11, 0x7f0201a8

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 846
    .local v4, "indicatorImg":Landroid/graphics/drawable/Drawable;
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v9}, Landroid/widget/SeekBar;->getWidth()I

    move-result v9

    if-gtz v9, :cond_2

    .line 847
    const-string v9, "VNPlyayerToolbarFragment"

    const-string v11, "prepareParams : wait for resource loading"

    invoke-static {v9, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    move v9, v10

    .line 848
    goto :goto_0

    .line 850
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v9}, Landroid/widget/SeekBar;->getWidth()I

    move-result v9

    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v11}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v9, v11

    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v11}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v11

    sub-int/2addr v9, v11

    int-to-float v9, v9

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v11

    int-to-float v11, v11

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    sub-float v5, v9, v11

    .line 851
    .local v5, "length":F
    const/4 v7, 0x0

    .line 852
    .local v7, "padding":I
    const/4 v1, 0x0

    .line 853
    .local v1, "first":F
    const/4 v8, 0x0

    .line 854
    .local v8, "sec":F
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mParams:Ljava/util/ArrayList;

    .line 855
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v9}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v9

    iget v2, v9, Landroid/graphics/Rect;->top:I

    .line 856
    .local v2, "height":I
    const/4 v6, 0x0

    .line 857
    .local v6, "mBookmarkssize":I
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v9, :cond_3

    .line 858
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 860
    :cond_3
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v6, :cond_5

    .line 861
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPlayerDuration()I

    move-result v9

    if-nez v9, :cond_4

    .line 862
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v9, v10

    .line 863
    goto :goto_0

    .line 866
    :cond_4
    :try_start_2
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v9

    int-to-float v9, v9

    mul-float v1, v5, v9

    .line 872
    :try_start_3
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPlayerDuration()I
    :try_end_3
    .catch Ljava/lang/ArithmeticException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v9

    int-to-float v9, v9

    div-float v8, v1, v9

    .line 878
    float-to-int v7, v8

    .line 879
    :try_start_4
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mParams:Ljava/util/ArrayList;

    new-instance v11, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;

    const/4 v12, -0x2

    const/4 v13, -0x2

    invoke-direct {v11, p0, v12, v13}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;II)V

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 882
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mParams:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;

    const/16 v11, 0x8

    const v12, 0x7f0e00a6

    invoke-virtual {v9, v11, v12}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;->addRule(II)V

    .line 883
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mParams:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v9, v7, v2, v11, v12}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;->setMargins(IIII)V

    .line 860
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 867
    :catch_0
    move-exception v0

    .line 868
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v9, "VNPlyayerToolbarFragment"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "prepareParams : mBookmarks.get(i).getElapsed() "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v9, v10

    .line 869
    goto/16 :goto_0

    .line 873
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catch_1
    move-exception v0

    .local v0, "e":Ljava/lang/ArithmeticException;
    move v9, v10

    .line 874
    goto/16 :goto_0

    .line 875
    .end local v0    # "e":Ljava/lang/ArithmeticException;
    :catch_2
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move v9, v10

    .line 876
    goto/16 :goto_0

    .line 885
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 841
    .end local v1    # "first":F
    .end local v2    # "height":I
    .end local v3    # "i":I
    .end local v4    # "indicatorImg":Landroid/graphics/drawable/Drawable;
    .end local v5    # "length":F
    .end local v6    # "mBookmarkssize":I
    .end local v7    # "padding":I
    .end local v8    # "sec":F
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9
.end method

.method public static releaseService()V
    .locals 1

    .prologue
    .line 927
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 928
    return-void
.end method

.method private restoreInstance(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 144
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_1

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRepeatTime()[I

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    .line 146
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRepeatTime()[I

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    .line 147
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    if-ltz v0, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->restoreRepeat()V

    goto :goto_0
.end method

.method private restoreRepeat()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 153
    const-string v7, "VNPlyayerToolbarFragment"

    const-string v8, "restoreRepeat"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    if-nez v7, :cond_1

    .line 155
    const-string v7, "VNPlyayerToolbarFragment"

    const-string v8, "restoreRepeat : seekbar is null"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getWidth()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v8

    sub-int v5, v7, v8

    .line 161
    .local v5, "seekBarWidth":I
    const/4 v0, 0x0

    .line 163
    .local v0, "isPlaying":Z
    if-gez v5, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    if-eqz v7, :cond_2

    .line 164
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 165
    .local v1, "msg":Landroid/os/Message;
    const/16 v7, 0x1228

    iput v7, v1, Landroid/os/Message;->what:I

    .line 166
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    const-wide/16 v8, 0x64

    invoke-virtual {v7, v1, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 170
    .end local v1    # "msg":Landroid/os/Message;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 171
    .local v2, "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    int-to-float v7, v7

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    int-to-float v8, v5

    mul-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    sub-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 172
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 173
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 175
    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    if-ltz v7, :cond_0

    .line 176
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_3

    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 177
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->stopProgress()V

    .line 179
    const/4 v0, 0x1

    .line 182
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 183
    .local v4, "repeatBparam":Landroid/widget/RelativeLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    int-to-float v7, v7

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    int-to-float v8, v5

    mul-float/2addr v7, v8

    float-to-int v7, v7

    sub-int v7, v5, v7

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 184
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 185
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 187
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 188
    .local v3, "repeatAtoBparam":Landroid/widget/FrameLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    sub-int/2addr v7, v8

    int-to-float v6, v7

    .line 190
    .local v6, "temp":F
    const/4 v7, 0x0

    cmpl-float v7, v6, v7

    if-ltz v7, :cond_4

    .line 191
    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    int-to-float v7, v7

    div-float v7, v6, v7

    int-to-float v8, v5

    mul-float/2addr v7, v8

    float-to-int v7, v7

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 192
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    iget v7, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 193
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    iget v7, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 194
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    float-to-int v8, v6

    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setMax(I)V

    .line 195
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 206
    :goto_1
    const/4 v7, -0x1

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 208
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 209
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 211
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v9}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 213
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 214
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 216
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v10}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 217
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v9}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 218
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v10}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 219
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v9}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 221
    if-eqz v0, :cond_0

    .line 222
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumePlay()V

    .line 223
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->startProgress()V

    goto/16 :goto_0

    .line 197
    :cond_4
    const/high16 v7, -0x40800000    # -1.0f

    mul-float/2addr v6, v7

    .line 198
    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    int-to-float v7, v7

    div-float v7, v6, v7

    int-to-float v8, v5

    mul-float/2addr v7, v8

    float-to-int v7, v7

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 199
    iget v7, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    sub-int v7, v5, v7

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 200
    iget v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 201
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    float-to-int v8, v6

    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setMax(I)V

    .line 202
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_1
.end method

.method private setIndicators()V
    .locals 10

    .prologue
    .line 893
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->hideBookmarkIndicators()V

    .line 894
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    .line 895
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 896
    .local v0, "anim":Landroid/view/animation/Animation;
    const-wide/16 v4, 0xc8

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 897
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkLayout:Landroid/widget/RelativeLayout;

    if-eqz v4, :cond_2

    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mParams:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mParams:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v4, v5, :cond_2

    .line 898
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 900
    .local v3, "size":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 901
    :try_start_0
    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    .line 902
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    new-instance v6, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v4

    int-to-long v8, v4

    invoke-direct {v6, v7, v8, v9}, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;-><init>(Landroid/content/Context;J)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 903
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;

    const v5, 0x7f0201a8

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;->setImageResource(I)V

    .line 904
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;->setClickable(Z)V

    .line 905
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;->setFocusable(Z)V

    .line 906
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;->bringToFront()V

    .line 907
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;

    const v5, 0x7f0b001a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 908
    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->isAnimationNeeded(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 909
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 911
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mParams:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v6, v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 900
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 913
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mParams:Ljava/util/ArrayList;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mOldParams:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 920
    .end local v2    # "i":I
    .end local v3    # "size":I
    :cond_2
    :goto_1
    return-void

    .line 914
    .restart local v2    # "i":I
    .restart local v3    # "size":I
    :catch_0
    move-exception v1

    .line 915
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method private setPlaySpeedImage(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 690
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_1

    .line 691
    const-string v0, "VNPlyayerToolbarFragment"

    const-string v1, "try to set play speed image while service is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    :cond_0
    :goto_0
    return-void

    .line 694
    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v0

    const/16 v1, 0x15

    if-ne v0, v1, :cond_2

    .line 695
    const-string v0, "VNPlyayerToolbarFragment"

    const-string v1, "try to set play speed image while play sates is idle"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 699
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 700
    packed-switch p1, :pswitch_data_0

    .line 714
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    const v1, 0x7f020030

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 703
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    const v1, 0x7f020034

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 707
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    const v1, 0x7f020038

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 711
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    const v1, 0x7f02002c

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 700
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static setProgressCallback(Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;)V
    .locals 0
    .param p0, "callback"    # Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    .prologue
    .line 1014
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    .line 1015
    return-void
.end method

.method private setProgressDialog()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1559
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1560
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 1561
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f0b0157

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 1562
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 1563
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 1564
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1565
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, -0x2

    const v2, 0x7f0b0021

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$6;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1574
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1575
    return-void
.end method

.method private setProgressHoverWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1330
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.feature.hovering_ui"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 1332
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setHoverPopupType(I)V

    .line 1333
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 1334
    .local v0, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_0

    .line 1335
    invoke-virtual {v0, v3}, Landroid/widget/HoverPopupWindow;->setInstanceOfProgressBar(Z)V

    .line 1337
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarHoverListener(Landroid/widget/SeekBar$OnSeekBarHoverListener;)V

    .line 1434
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_1
    return-void
.end method

.method private setSTTButton()V
    .locals 0

    .prologue
    .line 1487
    return-void
.end method

.method private showBookmarkIndicators()V
    .locals 6

    .prologue
    const/16 v3, 0x1982

    .line 767
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->prepareParams()Z

    move-result v0

    .line 768
    .local v0, "bDone":Z
    if-eqz v0, :cond_1

    .line 769
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setIndicators()V

    .line 775
    :cond_0
    :goto_0
    return-void

    .line 770
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 771
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 772
    .local v1, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 773
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method private updateBookmarkButton()V
    .locals 4

    .prologue
    .line 1509
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkButton:Landroid/widget/ImageButton;

    if-nez v2, :cond_1

    .line 1521
    :cond_0
    :goto_0
    return-void

    .line 1511
    :cond_1
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v0

    .line 1512
    .local v0, "id":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 1513
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isM4A(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1514
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkButton:Landroid/widget/ImageButton;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1515
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkButton:Landroid/widget/ImageButton;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setAlpha(F)V

    goto :goto_0

    .line 1517
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkButton:Landroid/widget/ImageButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1518
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkButton:Landroid/widget/ImageButton;

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setAlpha(F)V

    goto :goto_0
.end method

.method private updateSeekBarView(Z)V
    .locals 5
    .param p1, "isEasy"    # Z

    .prologue
    const/4 v4, 0x0

    .line 502
    const-string v1, "VNPlyayerToolbarFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateSeekBarView() easymode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 505
    if-eqz p1, :cond_1

    .line 506
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlayerToolbar:Landroid/widget/LinearLayout;

    const v2, 0x7f0e00a7

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    .line 509
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsCreatedEasyMode:Z

    .line 510
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->resetRepeatMode()V

    .line 518
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 519
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 520
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mOnkeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 522
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v1, :cond_0

    .line 523
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v1

    const/16 v2, 0x17

    if-ne v1, v2, :cond_2

    .line 524
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->startProgress()V

    .line 534
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 535
    return-void

    .line 515
    :cond_1
    iput-boolean v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsCreatedEasyMode:Z

    goto :goto_0

    .line 527
    :cond_2
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v0

    .line 528
    .local v0, "pos":I
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mElapseTimeTextView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->convertTimeFormat(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 529
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 530
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_1
.end method


# virtual methods
.method public changeSkipSilenceButton()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1490
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_1

    .line 1506
    :cond_0
    :goto_0
    return-void

    .line 1492
    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isSkipSilenceMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1493
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSkipSilenceButton:Landroid/widget/ImageButton;

    const v1, 0x7f020029

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1494
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1495
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 1496
    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->enableRepeatButton(Z)V

    goto :goto_0

    .line 1499
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1500
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSkipSilenceButton:Landroid/widget/ImageButton;

    const v1, 0x7f020026

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1501
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1502
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 1503
    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->enableRepeatButton(Z)V

    goto :goto_0
.end method

.method public doRepeat()V
    .locals 15

    .prologue
    const/4 v14, -0x1

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 1196
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    .line 1197
    .local v2, "progress":I
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getWidth()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v8

    sub-int v5, v7, v8

    .line 1198
    .local v5, "seekBarWidth":I
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1200
    .local v1, "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x0

    .line 1202
    .local v0, "isPlaying":Z
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_1

    .line 1203
    invoke-virtual {p0, v13}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->enableSkipSilenceButton(Z)V

    .line 1204
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    .line 1205
    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    int-to-float v7, v7

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    int-to-float v8, v5

    mul-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    sub-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1206
    int-to-float v7, v2

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    int-to-float v8, v5

    mul-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    sub-int v7, v5, v7

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1207
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1208
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1209
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_0

    .line 1210
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    invoke-virtual {v7, v8, v14}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setRepeatTime(II)V

    .line 1288
    :cond_0
    :goto_0
    return-void

    .line 1212
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getVisibility()I

    move-result v7

    if-nez v7, :cond_2

    .line 1213
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->resetRepeatMode()V

    .line 1214
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-direct {p0, v7, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->executeProgressCallback(Landroid/widget/SeekBar;I)V

    goto :goto_0

    .line 1216
    :cond_2
    invoke-virtual {p0, v12}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->enableSkipSilenceButton(Z)V

    .line 1217
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    .line 1218
    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    sub-int/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    int-to-long v8, v7

    const-wide/16 v10, 0x44b

    cmp-long v7, v8, v10

    if-gez v7, :cond_3

    .line 1219
    invoke-virtual {p0, v13}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->enableSkipSilenceButton(Z)V

    goto :goto_0

    .line 1222
    :cond_3
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_4

    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1223
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 1224
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->stopProgress()V

    .line 1225
    const/4 v0, 0x1

    .line 1228
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1229
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1231
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1232
    .local v4, "repeatBparam":Landroid/widget/RelativeLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    int-to-float v7, v7

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    int-to-float v8, v5

    mul-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    sub-float/2addr v7, v8

    float-to-int v7, v7

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1233
    int-to-float v7, v2

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    int-to-float v8, v5

    mul-float/2addr v7, v8

    float-to-int v7, v7

    sub-int v7, v5, v7

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1234
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1235
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1237
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 1238
    .local v3, "repeatAtoBparam":Landroid/widget/FrameLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    sub-int/2addr v7, v8

    int-to-float v6, v7

    .line 1240
    .local v6, "temp":F
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_5

    .line 1241
    const/4 v7, 0x0

    cmpl-float v7, v6, v7

    if-ltz v7, :cond_7

    .line 1242
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    iget v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setRepeatTime(II)V

    .line 1243
    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    int-to-float v7, v7

    div-float v7, v6, v7

    int-to-float v8, v5

    mul-float/2addr v7, v8

    float-to-int v7, v7

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1244
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    iget v7, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1245
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    iget v7, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 1246
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    float-to-int v8, v6

    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1247
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1248
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    invoke-virtual {v7, v8}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 1260
    :cond_5
    :goto_1
    iput v14, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1262
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1263
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1265
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v12}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1266
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v12}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 1268
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v13}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 1269
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v12}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 1270
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v13}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 1271
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v12}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 1275
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1283
    :cond_6
    if-eqz v0, :cond_0

    .line 1284
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumePlay()V

    .line 1285
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->startProgress()V

    goto/16 :goto_0

    .line 1250
    :cond_7
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    iget v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setRepeatTime(II)V

    .line 1251
    const/high16 v7, -0x40800000    # -1.0f

    mul-float/2addr v6, v7

    .line 1252
    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    int-to-float v7, v7

    div-float v7, v6, v7

    int-to-float v8, v5

    mul-float/2addr v7, v8

    float-to-int v7, v7

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1253
    iget v7, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    sub-int v7, v5, v7

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1254
    iget v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 1255
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    float-to-int v8, v6

    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1256
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1257
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    invoke-virtual {v7, v8}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    goto/16 :goto_1
.end method

.method public enableRepeatButton(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 1446
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isSkipSilenceMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1447
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1448
    if-eqz p1, :cond_2

    .line 1449
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatButton:Landroid/widget/ImageButton;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 1454
    :cond_1
    :goto_0
    return-void

    .line 1451
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatButton:Landroid/widget/ImageButton;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(F)V

    goto :goto_0
.end method

.method public enableSkipSilenceButton(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 1457
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSkipSilenceButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 1458
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTempButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTempButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getId()I

    move-result v0

    const v1, 0x7f0e00a3

    if-eq v0, v1, :cond_1

    .line 1459
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSkipSilenceButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1460
    if-eqz p1, :cond_2

    .line 1461
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSkipSilenceButton:Landroid/widget/ImageButton;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 1467
    :cond_1
    :goto_0
    return-void

    .line 1463
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSkipSilenceButton:Landroid/widget/ImageButton;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(F)V

    goto :goto_0
.end method

.method protected isAmrFile()Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1174
    const/4 v6, 0x0

    .line 1175
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 1176
    .local v7, "mimetype":Ljava/lang/String;
    new-array v4, v9, [Ljava/lang/String;

    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 1177
    .local v4, "args":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v8

    .line 1191
    :goto_0
    return v0

    .line 1180
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "_id == ?"

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1181
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1182
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1183
    const-string v0, "mime_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1185
    :cond_1
    if-eqz v6, :cond_2

    .line 1186
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1188
    :cond_2
    if-eqz v7, :cond_3

    const-string v0, "audio/amr"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v9

    .line 1189
    goto :goto_0

    :cond_3
    move v0, v8

    .line 1191
    goto :goto_0
.end method

.method public isRepeatMode()Z
    .locals 1

    .prologue
    .line 1318
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveRepeatButtons(Landroid/view/View;I)V
    .locals 16
    .param p1, "view"    # Landroid/view/View;
    .param p2, "x"    # I

    .prologue
    .line 540
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v11}, Landroid/widget/SeekBar;->getLeft()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v12}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v12

    add-int v9, v11, v12

    .line 541
    .local v9, "startPoint":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v11}, Landroid/widget/SeekBar;->getRight()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v12}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v12

    sub-int v3, v11, v12

    .line 542
    .local v3, "endPoint":I
    move/from16 v0, p2

    if-ge v0, v9, :cond_2

    .line 543
    move/from16 p2, v9

    .line 546
    :cond_0
    :goto_0
    move/from16 v0, p2

    int-to-float v11, v0

    int-to-float v12, v9

    sub-float/2addr v11, v12

    int-to-float v12, v3

    int-to-float v13, v9

    sub-float/2addr v12, v13

    div-float/2addr v11, v12

    const/high16 v12, 0x42c80000    # 100.0f

    mul-float v6, v11, v12

    .line 547
    .local v6, "percentage":F
    sget-object v11, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v11, :cond_3

    .line 620
    :cond_1
    :goto_1
    return-void

    .line 544
    .end local v6    # "percentage":F
    :cond_2
    move/from16 v0, p2

    if-le v0, v3, :cond_0

    .line 545
    move/from16 p2, v3

    goto :goto_0

    .line 550
    .restart local v6    # "percentage":F
    :cond_3
    sget-object v11, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v11}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPlayerDuration()I

    move-result v7

    .line 551
    .local v7, "playerDuration":I
    int-to-float v11, v7

    const/high16 v12, 0x42c80000    # 100.0f

    div-float v12, v6, v12

    mul-float/2addr v11, v12

    float-to-int v2, v11

    .line 553
    .local v2, "currentPoint":I
    add-int/lit8 v11, v7, -0x64

    if-le v2, v11, :cond_4

    .line 554
    add-int/lit8 v2, v7, -0x64

    .line 557
    :cond_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/FrameLayout$LayoutParams;

    .line 558
    .local v8, "repeatAtoBparam":Landroid/widget/FrameLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 559
    .local v4, "paramA":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 562
    .local v5, "paramB":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    if-ge v11, v12, :cond_6

    .line 563
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v11

    const v12, 0x7f0e00ae

    if-ne v11, v12, :cond_5

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    sub-int/2addr v11, v2

    int-to-long v12, v11

    const-wide/16 v14, 0x44b

    cmp-long v11, v12, v14

    if-ltz v11, :cond_5

    .line 564
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    .line 565
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    sub-int v10, v11, v12

    .line 566
    .local v10, "temp":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int v11, p2, v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v12}, Landroid/widget/RelativeLayout;->getLeft()I

    move-result v12

    sub-int/2addr v11, v12

    iput v11, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 567
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getRight()I

    move-result v11

    sub-int v11, v11, p2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v12}, Landroid/widget/ImageView;->getWidth()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    sub-int/2addr v11, v12

    iput v11, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 568
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v11, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 569
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v11}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v11

    sub-int v11, p2, v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v12, v12, 0x2

    sub-int/2addr v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v12}, Landroid/widget/ImageView;->getWidth()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v13}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v13

    sub-int/2addr v12, v13

    div-int/lit8 v12, v12, 0x2

    sub-int/2addr v11, v12

    iput v11, v8, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 570
    iget v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v12, v12, 0x2

    add-int/2addr v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v12}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v12

    sub-int/2addr v11, v12

    iput v11, v8, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 571
    const/4 v11, -0x1

    iput v11, v8, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 572
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v11, v8}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 573
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 574
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v11, v10}, Landroid/widget/SeekBar;->setMax(I)V

    .line 575
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    invoke-virtual {v11, v12}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 576
    sget-object v11, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    invoke-virtual {v11, v12}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 577
    sget-object v11, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    invoke-virtual {v11, v12, v13}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setRepeatTime(II)V

    goto/16 :goto_1

    .line 578
    .end local v10    # "temp":I
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v11

    const v12, 0x7f0e00af

    if-ne v11, v12, :cond_1

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    sub-int v11, v2, v11

    int-to-long v12, v11

    const-wide/16 v14, 0x44b

    cmp-long v11, v12, v14

    if-ltz v11, :cond_1

    .line 579
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    .line 580
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int v11, p2, v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v12}, Landroid/widget/RelativeLayout;->getLeft()I

    move-result v12

    sub-int/2addr v11, v12

    iput v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 581
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getRight()I

    move-result v11

    sub-int v11, v11, p2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v12}, Landroid/widget/ImageView;->getWidth()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    sub-int/2addr v11, v12

    iput v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 582
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v11, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 583
    const/4 v11, -0x1

    iput v11, v8, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 584
    iget v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v12, v12, 0x2

    add-int/2addr v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v12}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v12

    sub-int/2addr v11, v12

    iput v11, v8, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 585
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v11, v8}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 586
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    sub-int v10, v11, v12

    .line 587
    .restart local v10    # "temp":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v11, v10}, Landroid/widget/SeekBar;->setMax(I)V

    .line 588
    sget-object v11, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    invoke-virtual {v11, v12, v13}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setRepeatTime(II)V

    goto/16 :goto_1

    .line 592
    .end local v10    # "temp":I
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v11

    const v12, 0x7f0e00af

    if-ne v11, v12, :cond_7

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    sub-int/2addr v11, v2

    int-to-long v12, v11

    const-wide/16 v14, 0x44b

    cmp-long v11, v12, v14

    if-ltz v11, :cond_7

    .line 593
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    .line 594
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    sub-int v10, v11, v12

    .line 595
    .restart local v10    # "temp":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int v11, p2, v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v12}, Landroid/widget/RelativeLayout;->getLeft()I

    move-result v12

    sub-int/2addr v11, v12

    iput v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 596
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getRight()I

    move-result v11

    sub-int v11, v11, p2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v12}, Landroid/widget/ImageView;->getWidth()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    sub-int/2addr v11, v12

    iput v11, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 597
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v11, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 598
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v11}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v11

    sub-int v11, p2, v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v12, v12, 0x2

    sub-int/2addr v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v12}, Landroid/widget/ImageView;->getWidth()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v13}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v13

    sub-int/2addr v12, v13

    div-int/lit8 v12, v12, 0x2

    sub-int/2addr v11, v12

    iput v11, v8, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 599
    iget v11, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v12, v12, 0x2

    add-int/2addr v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v12}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v12

    sub-int/2addr v11, v12

    iput v11, v8, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 600
    const/4 v11, -0x1

    iput v11, v8, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 601
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v11, v8}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 602
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 603
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v11, v10}, Landroid/widget/SeekBar;->setMax(I)V

    .line 604
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    invoke-virtual {v11, v12}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 605
    sget-object v11, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    invoke-virtual {v11, v12}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 606
    sget-object v11, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    invoke-virtual {v11, v12, v13}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setRepeatTime(II)V

    goto/16 :goto_1

    .line 607
    .end local v10    # "temp":I
    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v11

    const v12, 0x7f0e00ae

    if-ne v11, v12, :cond_1

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    sub-int v11, v2, v11

    int-to-long v12, v11

    const-wide/16 v14, 0x44b

    cmp-long v11, v12, v14

    if-ltz v11, :cond_1

    .line 608
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    .line 609
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int v11, p2, v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v12}, Landroid/widget/RelativeLayout;->getLeft()I

    move-result v12

    sub-int/2addr v11, v12

    iput v11, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 610
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getRight()I

    move-result v11

    sub-int v11, v11, p2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v12}, Landroid/widget/ImageView;->getWidth()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    sub-int/2addr v11, v12

    iput v11, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 611
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v11, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 612
    const/4 v11, -0x1

    iput v11, v8, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 613
    iget v11, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    div-int/lit8 v12, v12, 0x2

    add-int/2addr v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v12}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v12

    sub-int/2addr v11, v12

    iput v11, v8, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 614
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v11, v8}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 615
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    sub-int v10, v11, v12

    .line 616
    .restart local v10    # "temp":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v11, v10}, Landroid/widget/SeekBar;->setMax(I)V

    .line 617
    sget-object v11, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    invoke-virtual {v11, v12, v13}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setRepeatTime(II)V

    goto/16 :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, -0x1

    .line 1309
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    if-ne p2, v2, :cond_0

    .line 1310
    const-string v1, "what"

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1311
    .local v0, "what":I
    if-lez v0, :cond_0

    .line 1312
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1314
    .end local v0    # "what":I
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1315
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1297
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 1298
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 1299
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v5, 0x7f0e00a2

    const/4 v8, 0x0

    .line 1050
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 1052
    .local v0, "id":I
    if-eq v0, v5, :cond_1

    .line 1053
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTempButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_0

    .line 1054
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTempButton:Landroid/widget/ImageButton;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1056
    :cond_0
    check-cast p1, Landroid/widget/ImageButton;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTempButton:Landroid/widget/ImageButton;

    .line 1057
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTempButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v8}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1058
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTempButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$3;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    const-wide/16 v6, 0x3e8

    invoke-virtual {v3, v4, v6, v7}, Landroid/widget/ImageButton;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1069
    :cond_1
    const v3, 0x7f0e00a1

    if-ne v0, v3, :cond_3

    .line 1070
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->doRepeat()V

    .line 1087
    :cond_2
    :goto_0
    return-void

    .line 1071
    :cond_3
    if-ne v0, v5, :cond_4

    .line 1072
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->changePlaySpeed()V

    .line 1073
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getPlaySpeedIndex()I

    move-result v1

    .line 1074
    .local v1, "speed":I
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v3, :cond_2

    .line 1075
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setPlaySpeed(I)V

    .line 1076
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setPlaySpeedImage(I)V

    goto :goto_0

    .line 1078
    .end local v1    # "speed":I
    :cond_4
    const v3, 0x7f0e00a4

    if-ne v0, v3, :cond_5

    .line 1079
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v3, :cond_2

    .line 1080
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v2

    .line 1082
    .local v2, "time":I
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    new-instance v4, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    const v5, 0x7f0b001a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v4, v2, v5, v6, v8}, Lcom/sec/android/app/voicenote/common/util/Bookmark;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onBookmarkAdded(Lcom/sec/android/app/voicenote/common/util/Bookmark;)V

    goto :goto_0

    .line 1085
    .end local v2    # "time":I
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    const-wide/16 v4, 0x0

    invoke-interface {v3, v0, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onControlButtonSelected(IJ)Z

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/16 v5, 0x3031

    .line 230
    const-string v2, "VNPlyayerToolbarFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreateView : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 234
    const v2, 0x7f03003f

    invoke-virtual {p1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 238
    .local v1, "view":Landroid/view/View;
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090038

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->INDICATOR_WIDTH:I

    .line 240
    const v2, 0x7f0e009e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlayerToolbar:Landroid/widget/LinearLayout;

    .line 241
    const v2, 0x7f0e00a5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressLayout:Landroid/widget/FrameLayout;

    .line 242
    const v2, 0x7f0e00a6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    .line 243
    const v2, 0x7f0e00a8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mElapseTimeTextView:Landroid/widget/TextView;

    .line 244
    const v2, 0x7f0e00a9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDurationTextView:Landroid/widget/TextView;

    .line 246
    const v2, 0x7f0e00a0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimButton:Landroid/widget/ImageButton;

    .line 247
    const v2, 0x7f0e00a2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    .line 248
    const v2, 0x7f0e00ac

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkLayout:Landroid/widget/RelativeLayout;

    .line 249
    const v2, 0x7f0e00ae

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    .line 250
    const v2, 0x7f0e00af

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    .line 251
    const v2, 0x7f0e00a1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatButton:Landroid/widget/ImageButton;

    .line 252
    const v2, 0x7f0e00aa

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    .line 253
    const v2, 0x7f0e00ad

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatLayout:Landroid/widget/RelativeLayout;

    .line 254
    const v2, 0x7f0e00ab

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    .line 256
    const v2, 0x7f0e00a3

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSkipSilenceButton:Landroid/widget/ImageButton;

    .line 257
    const v2, 0x7f0e00a4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkButton:Landroid/widget/ImageButton;

    .line 259
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setSTTButton()V

    .line 262
    const v2, 0x7f0e009f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mFileName:Landroid/widget/TextView;

    .line 264
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v2, :cond_a

    .line 265
    iput v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    .line 269
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDurationTextView:Landroid/widget/TextView;

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->convertTimeFormat(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 272
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 273
    const v2, 0x7f0e00a7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    .line 274
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v6}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 275
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsCreatedEasyMode:Z

    .line 277
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 278
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 280
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 283
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 285
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSkipSilenceButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 286
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->onTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 287
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->onTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 288
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 290
    new-instance v2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    .line 291
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setProgressHoverWindow()V

    .line 293
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v2, :cond_b

    .line 294
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-interface {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onBookmarksChanged(Ljava/util/ArrayList;)V

    .line 295
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->showBookmarkIndicators()V

    .line 301
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isHoveringUI(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 302
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimButton:Landroid/widget/ImageButton;

    if-eqz v2, :cond_1

    .line 303
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 304
    .local v0, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_1

    .line 305
    invoke-virtual {v0, v5}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 309
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    if-eqz v2, :cond_2

    .line 310
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 311
    .restart local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_2

    .line 312
    invoke-virtual {v0, v5}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 316
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatButton:Landroid/widget/ImageButton;

    if-eqz v2, :cond_3

    .line 317
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 318
    .restart local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_3

    .line 319
    invoke-virtual {v0, v5}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 325
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSkipSilenceButton:Landroid/widget/ImageButton;

    if-eqz v2, :cond_4

    .line 326
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSkipSilenceButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 327
    .restart local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_4

    .line 328
    invoke-virtual {v0, v5}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 332
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkButton:Landroid/widget/ImageButton;

    if-eqz v2, :cond_5

    .line 333
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 334
    .restart local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_5

    .line 335
    invoke-virtual {v0, v5}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 340
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 341
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getPlaySpeedIndex()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setPlaySpeedImage(I)V

    .line 345
    :cond_6
    invoke-direct {p0, p3}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->restoreInstance(Landroid/os/Bundle;)V

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 349
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->changeSkipSilenceButton()V

    .line 350
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->updateBookmarkButton()V

    .line 352
    :cond_7
    sput v6, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveMode:I

    .line 354
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mOnkeyListener:Landroid/view/View$OnKeyListener;

    .line 397
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mOnkeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 398
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 399
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mOnkeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 401
    :cond_8
    return-object v1

    .line 236
    .end local v1    # "view":Landroid/view/View;
    :cond_9
    const v2, 0x7f030040

    invoke-virtual {p1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .restart local v1    # "view":Landroid/view/View;
    goto/16 :goto_0

    .line 267
    :cond_a
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPlayerDuration()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    goto/16 :goto_1

    .line 297
    :cond_b
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    invoke-direct {v2, p0, v7}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;)V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    .line 298
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    sget-object v3, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v4, v6, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_2
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 625
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->cancel(Z)Z

    .line 627
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    .line 629
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    .line 630
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mElapseTimeTextView:Landroid/widget/TextView;

    .line 631
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDurationTextView:Landroid/widget/TextView;

    .line 632
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimButton:Landroid/widget/ImageButton;

    .line 633
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    .line 634
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkLayout:Landroid/widget/RelativeLayout;

    .line 635
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    .line 636
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    .line 637
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatButton:Landroid/widget/ImageButton;

    .line 638
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    .line 639
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    .line 640
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    .line 642
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkButton:Landroid/widget/ImageButton;

    .line 644
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 645
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 646
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    .line 649
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_2

    .line 650
    sput-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    .line 653
    :cond_2
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mParams:Ljava/util/ArrayList;

    .line 654
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    .line 655
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTempButton:Landroid/widget/ImageButton;

    .line 657
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 658
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->dismiss()V

    .line 659
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    .line 661
    :cond_3
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 662
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 1303
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 1304
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 1305
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 1004
    if-eqz p3, :cond_1

    .line 1005
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1006
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    if-ge v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    :goto_0
    add-int/2addr p2, v0

    .line 1008
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mElapseTimeTextView:Landroid/widget/TextView;

    invoke-direct {p0, p2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->convertTimeFormat(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1010
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->executeProgressCallback(Landroid/widget/SeekBar;I)V

    .line 1011
    return-void

    .line 1006
    :cond_2
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    goto :goto_0
.end method

.method public onResume()V
    .locals 11

    .prologue
    const/high16 v10, 0x41700000    # 15.0f

    const/4 v9, -0x1

    const/4 v8, 0x1

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 442
    const-string v4, "VNPlyayerToolbarFragment"

    const-string v5, "onResume()"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 445
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 446
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 447
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 448
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 449
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 450
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 451
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 452
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 453
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 454
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v7}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 455
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSkipSilenceButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 456
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 457
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mFileName:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 458
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mFileName:Landroid/widget/TextView;

    if-eqz v4, :cond_0

    .line 459
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mFileName:Landroid/widget/TextView;

    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSelectedFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 461
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090030

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 462
    .local v1, "player_toolbar_height":I
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlayerToolbar:Landroid/widget/LinearLayout;

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v5, v9, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 463
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09002e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 464
    .local v2, "progress_layout_height":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09002f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 465
    .local v3, "progress_layout_margin_top":I
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v9, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 466
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v6, v3, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 467
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 468
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mElapseTimeTextView:Landroid/widget/TextView;

    const/high16 v5, 0x41900000    # 18.0f

    invoke-virtual {v4, v8, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 469
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDurationTextView:Landroid/widget/TextView;

    const/high16 v5, 0x41900000    # 18.0f

    invoke-virtual {v4, v8, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 471
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsCreatedEasyMode:Z

    if-nez v4, :cond_1

    .line 472
    invoke-direct {p0, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->updateSeekBarView(Z)V

    .line 499
    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "progress_layout_margin_top":I
    :cond_1
    :goto_0
    return-void

    .line 475
    .end local v1    # "player_toolbar_height":I
    .end local v2    # "progress_layout_height":I
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 476
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlaySpeedButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 477
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 480
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 481
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAtoBSeekbar:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 482
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 484
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSkipSilenceButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 485
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarkButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 486
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mFileName:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 488
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090032

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 489
    .restart local v1    # "player_toolbar_height":I
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mPlayerToolbar:Landroid/widget/LinearLayout;

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v5, v9, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 490
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090031

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 491
    .restart local v2    # "progress_layout_height":I
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressLayout:Landroid/widget/FrameLayout;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v9, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 492
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mElapseTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v8, v10}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 493
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDurationTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v8, v10}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 495
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsCreatedEasyMode:Z

    if-ne v4, v8, :cond_1

    .line 496
    invoke-direct {p0, v6}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->updateSeekBarView(Z)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 136
    const-string v0, "VNPlyayerToolbarFragment"

    const-string v1, "onSaveInstanceState:ChangingConfigurations"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 138
    const-string v0, "VNPlyayerToolbarFragment"

    const-string v1, "onSaveInstanceState:ChangingConfigurations - save repeat state"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 141
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 667
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_2

    .line 668
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v0

    const/16 v1, 0x17

    if-ne v0, v1, :cond_1

    .line 669
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mFileName:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 670
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mFileName:Landroid/widget/TextView;

    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSelectedFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 671
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->startProgress()V

    .line 674
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mElapseTimeTextView:Landroid/widget/TextView;

    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->convertTimeFormat(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 677
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 680
    :cond_2
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 681
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1019
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1020
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->stopProgress()V

    .line 1021
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 1022
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsResumePlay:Z

    .line 1024
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 685
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->stopProgress()V

    .line 686
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 687
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1028
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_1

    .line 1046
    :cond_0
    :goto_0
    return-void

    .line 1030
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1031
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 1032
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mToggleValue:I

    .line 1041
    :goto_1
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsResumePlay:Z

    if-eqz v0, :cond_0

    .line 1042
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumePlay()V

    .line 1043
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->startProgress()V

    .line 1044
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsResumePlay:Z

    goto :goto_0

    .line 1034
    :cond_2
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    if-gt v0, v1, :cond_3

    .line 1035
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    goto :goto_1

    .line 1037
    :cond_3
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    goto :goto_1
.end method

.method public refreshToolbar(Z)V
    .locals 6
    .param p1, "updateBookmark"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 721
    const-string v2, "VNPlyayerToolbarFragment"

    const-string v3, "refreshToolbar"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    if-nez v2, :cond_1

    .line 764
    :cond_0
    :goto_0
    return-void

    .line 725
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->stopProgress()V

    .line 726
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPlayerDuration()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    .line 727
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDurationTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    .line 728
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDurationTextView:Landroid/widget/TextView;

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->convertTimeFormat(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 730
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 731
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mFileName:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    .line 732
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mFileName:Landroid/widget/TextView;

    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSelectedFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 734
    :cond_3
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRepeatTime()[I

    move-result-object v1

    .line 735
    .local v1, "repeatMode":[I
    aget v2, v1, v4

    if-gez v2, :cond_4

    .line 736
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->resetRepeatMode()V

    .line 739
    :cond_4
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v2

    const/16 v3, 0x17

    if-ne v2, v3, :cond_8

    .line 740
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->startProgress()V

    .line 750
    :cond_5
    :goto_1
    if-eqz p1, :cond_7

    .line 751
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    sget-object v3, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v2, v3, :cond_6

    .line 752
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->cancel(Z)Z

    .line 754
    :cond_6
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    invoke-direct {v2, p0, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;)V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    .line 755
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    sget-object v3, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 756
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v2, v5}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onBookmarksChanged(Ljava/util/ArrayList;)V

    .line 759
    :cond_7
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getPlaySpeedIndex()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setPlaySpeedImage(I)V

    .line 760
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setSTTButton()V

    .line 761
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->changeSkipSilenceButton()V

    .line 763
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->updateBookmarkButton()V

    goto/16 :goto_0

    .line 743
    :cond_8
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v0

    .line 744
    .local v0, "pos":I
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mElapseTimeTextView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->convertTimeFormat(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 745
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 746
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_1
.end method

.method public resetRepeatMode()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 1523
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v1, :cond_1

    .line 1556
    :cond_0
    :goto_0
    return-void

    .line 1526
    :cond_1
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1, v2, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setRepeatTime(II)V

    .line 1527
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1528
    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->enableSkipSilenceButton(Z)V

    .line 1529
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 1530
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1531
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1532
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 1533
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    .line 1534
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    .line 1535
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v5}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 1536
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1537
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v5}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 1540
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_0

    .line 1541
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1542
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 1543
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 1545
    const/4 v0, 0x0

    .line 1547
    .local v0, "seekbarDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method public setProgress(I)V
    .locals 2
    .param p1, "progress"    # I

    .prologue
    .line 1437
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mElapseTimeTextView:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->convertTimeFormat(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1438
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1439
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1443
    :goto_0
    return-void

    .line 1441
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method public startProgress()V
    .locals 5

    .prologue
    const v4, 0x8281

    .line 936
    const-string v0, "VNPlyayerToolbarFragment"

    const-string v1, "startProgress"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsProgressRun:Z

    .line 938
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 939
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 940
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mEventHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 942
    :cond_0
    return-void
.end method

.method public startRepeatModeTrim()V
    .locals 5

    .prologue
    .line 1322
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    .line 1323
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getTag()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->setTargetFragmentTag(Ljava/lang/String;I)V

    .line 1324
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->setExpectedFileSize(Ljava/lang/String;II)V

    .line 1326
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1327
    return-void
.end method

.method public stopProgress()V
    .locals 2

    .prologue
    .line 931
    const-string v0, "VNPlyayerToolbarFragment"

    const-string v1, "stopProgress"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsProgressRun:Z

    .line 933
    return-void
.end method

.method public updateBookmarks(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1291
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    sput-object p1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;

    .line 1292
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->showBookmarkIndicators()V

    .line 1293
    return-void
.end method
