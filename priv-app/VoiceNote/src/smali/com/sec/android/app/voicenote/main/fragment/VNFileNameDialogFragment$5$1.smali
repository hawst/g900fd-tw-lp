.class Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;
.super Ljava/lang/Object;
.source "VNFileNameDialogFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->onShow(Landroid/content/DialogInterface;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->editText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$100(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 253
    .local v1, "retText":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isSameFileInLibrary(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->val$prevFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0b0087

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 270
    :goto_0
    return-void

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$300(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mRenameMode:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$400(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_2

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$300(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mId:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$500(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v3, v3, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mTitle:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$600(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v4, v4, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mColor:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$700(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v5, v5, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mPosition:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$800(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)I

    move-result v5

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;->onFileNameDialogPositiveBtn(Ljava/lang/String;ILjava/lang/String;II)V

    .line 269
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->val$dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_0

    .line 264
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mRenameMode:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$400(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$300(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->val$prevFileName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v3, v3, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mId:I
    invoke-static {v3}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$500(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5$1;->this$1:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;

    iget-object v4, v4, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->mColor:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$700(Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;)I

    move-result v4

    invoke-interface {v0, v2, v1, v3, v4}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$Callbacks;->onListitemFileNameDialogPositiveBtn(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_1
.end method
