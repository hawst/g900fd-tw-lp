.class Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment$1;
.super Landroid/widget/ArrayAdapter;
.source "VNSettingSkipIntervalFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;->listBinding()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;Landroid/content/Context;I[Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I
    .param p4, "x2"    # [Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment$1;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 172
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 173
    .local v0, "checkedTextView":Landroid/widget/CheckedTextView;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingSkipIntervalFragment$1;->isCheckEnabled(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 174
    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setTextColor(I)V

    .line 175
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    .line 180
    :goto_0
    return-object v0

    .line 177
    :cond_0
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setTextColor(I)V

    .line 178
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method public isCheckEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 163
    const-string v0, "skip_interval"

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x1

    .line 166
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
