.class Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;
.super Landroid/widget/RelativeLayout$LayoutParams;
.source "VNPlayerToolbarFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BookmarkParams"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;II)V
    .locals 0
    .param p2, "wrapContent"    # I
    .param p3, "wrapContent2"    # I

    .prologue
    .line 1597
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .line 1598
    invoke-direct {p0, p2, p3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1599
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2, "arg0"    # Landroid/content/Context;
    .param p3, "arg1"    # Landroid/util/AttributeSet;

    .prologue
    .line 1593
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .line 1594
    invoke-direct {p0, p2, p3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1595
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1603
    instance-of v1, p1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;

    if-nez v1, :cond_0

    .line 1604
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 1610
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 1606
    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;

    .line 1607
    .local v0, "other":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;->leftMargin:I

    iget v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$BookmarkParams;->leftMargin:I

    if-ne v1, v2, :cond_1

    .line 1608
    const/4 v1, 0x1

    goto :goto_0

    .line 1610
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
