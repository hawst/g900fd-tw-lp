.class public Lcom/sec/android/app/voicenote/common/util/TypefaceParser;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "TypefaceParser.java"


# static fields
.field private static final ATTR_NAME:Ljava/lang/String; = "displayname"

.field private static final NODE_DROIDNAME:Ljava/lang/String; = "droidname"

.field private static final NODE_FILE:Ljava/lang/String; = "file"

.field private static final NODE_FILENAME:Ljava/lang/String; = "filename"

.field private static final NODE_FONT:Ljava/lang/String; = "font"

.field private static final NODE_MONOSPACE:Ljava/lang/String; = "monospace"

.field private static final NODE_SANS:Ljava/lang/String; = "sans"

.field private static final NODE_SERIF:Ljava/lang/String; = "serif"


# instance fields
.field private in_droidname:Z

.field private in_file:Z

.field private in_filename:Z

.field private in_font:Z

.field private in_monospace:Z

.field private in_sans:Z

.field private in_serif:Z

.field private mFont:Lcom/sec/android/app/voicenote/common/util/Typeface;

.field private mFontFile:Lcom/sec/android/app/voicenote/common/util/TypefaceFile;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 42
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_font:Z

    .line 44
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_sans:Z

    .line 46
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_serif:Z

    .line 48
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_monospace:Z

    .line 50
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_file:Z

    .line 52
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_filename:Z

    .line 54
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_droidname:Z

    .line 56
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFont:Lcom/sec/android/app/voicenote/common/util/Typeface;

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFontFile:Lcom/sec/android/app/voicenote/common/util/TypefaceFile;

    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 2
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_filename:Z

    if-eqz v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFontFile:Lcom/sec/android/app/voicenote/common/util/TypefaceFile;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/TypefaceFile;->setFileName(Ljava/lang/String;)V

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_droidname:Z

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFontFile:Lcom/sec/android/app/voicenote/common/util/TypefaceFile;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/TypefaceFile;->setDroidName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 71
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "namespaceURI"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 100
    const-string v0, "font"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_font:Z

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    const-string v0, "sans"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_sans:Z

    goto :goto_0

    .line 104
    :cond_2
    const-string v0, "serif"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 105
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_serif:Z

    goto :goto_0

    .line 106
    :cond_3
    const-string v0, "monospace"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 107
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_monospace:Z

    goto :goto_0

    .line 108
    :cond_4
    const-string v0, "file"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 109
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_file:Z

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFontFile:Lcom/sec/android/app/voicenote/common/util/TypefaceFile;

    if-eqz v0, :cond_0

    .line 111
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_sans:Z

    if-eqz v0, :cond_5

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFont:Lcom/sec/android/app/voicenote/common/util/Typeface;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/Typeface;->mSansFonts:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFontFile:Lcom/sec/android/app/voicenote/common/util/TypefaceFile;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 113
    :cond_5
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_serif:Z

    if-eqz v0, :cond_6

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFont:Lcom/sec/android/app/voicenote/common/util/Typeface;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/Typeface;->mSerifFonts:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFontFile:Lcom/sec/android/app/voicenote/common/util/TypefaceFile;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 115
    :cond_6
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_monospace:Z

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFont:Lcom/sec/android/app/voicenote/common/util/Typeface;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/Typeface;->mMonospaceFonts:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFontFile:Lcom/sec/android/app/voicenote/common/util/TypefaceFile;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 119
    :cond_7
    const-string v0, "filename"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 120
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_filename:Z

    goto :goto_0

    .line 121
    :cond_8
    const-string v0, "droidname"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_droidname:Z

    goto :goto_0
.end method

.method public getParsedData()Lcom/sec/android/app/voicenote/common/util/Typeface;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFont:Lcom/sec/android/app/voicenote/common/util/Typeface;

    return-object v0
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 66
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/Typeface;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/common/util/Typeface;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFont:Lcom/sec/android/app/voicenote/common/util/Typeface;

    .line 67
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "namespaceURI"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "atts"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 77
    const-string v1, "font"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 78
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_font:Z

    .line 79
    const-string v1, "displayname"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "attrValue":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFont:Lcom/sec/android/app/voicenote/common/util/Typeface;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/util/Typeface;->setName(Ljava/lang/String;)V

    .line 95
    .end local v0    # "attrValue":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    const-string v1, "sans"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 82
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_sans:Z

    goto :goto_0

    .line 83
    :cond_2
    const-string v1, "serif"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 84
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_serif:Z

    goto :goto_0

    .line 85
    :cond_3
    const-string v1, "monospace"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 86
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_monospace:Z

    goto :goto_0

    .line 87
    :cond_4
    const-string v1, "file"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 88
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_file:Z

    .line 89
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/TypefaceFile;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/util/TypefaceFile;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->mFontFile:Lcom/sec/android/app/voicenote/common/util/TypefaceFile;

    goto :goto_0

    .line 90
    :cond_5
    const-string v1, "filename"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 91
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_filename:Z

    goto :goto_0

    .line 92
    :cond_6
    const-string v1, "droidname"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->in_droidname:Z

    goto :goto_0
.end method
