.class public Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;
.super Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;
.source "SamsungBufferingPipe.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<AudioChunkType:",
        "Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;",
        ">",
        "Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe",
        "<TAudioChunkType;TAudioChunkType;>;"
    }
.end annotation


# static fields
.field private static final BUFFER_SIZE:I = 0x8

.field private static final SEND_BUFFER_SIZE:I = 0x3

.field private static final TAG:Ljava/lang/String; = "SamsungBufferingPipe"


# instance fields
.field private _audioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

.field private final _buffer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;",
            ">;"
        }
    .end annotation
.end field

.field private _recentChunk:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;

.field private final _tempBuffer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TAudioChunkType;>;"
        }
    .end annotation
.end field

.field private _timeStamp:J


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1, "aStartTimeStamp"    # J

    .prologue
    .line 43
    .local p0, "this":Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;, "Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe<TAudioChunkType;>;"
    invoke-direct {p0}, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_recentChunk:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;

    .line 44
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_buffer:Ljava/util/List;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_tempBuffer:Ljava/util/ArrayList;

    .line 46
    sget-object v0, Lcom/nuance/dragon/toolkit/audio/AudioType;->UNKNOWN:Lcom/nuance/dragon/toolkit/audio/AudioType;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_audioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    .line 47
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_timeStamp:J

    .line 48
    return-void
.end method


# virtual methods
.method protected chunksAvailable(Lcom/nuance/dragon/toolkit/audio/AudioSource;Lcom/nuance/dragon/toolkit/audio/AudioSink;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<TAudioChunkType;>;",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TAudioChunkType;>;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p0, "this":Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;, "Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe<TAudioChunkType;>;"
    .local p1, "fromSource":Lcom/nuance/dragon/toolkit/audio/AudioSource;, "Lcom/nuance/dragon/toolkit/audio/AudioSource<TAudioChunkType;>;"
    .local p2, "internalSink":Lcom/nuance/dragon/toolkit/audio/AudioSink;, "Lcom/nuance/dragon/toolkit/audio/AudioSink<TAudioChunkType;>;"
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_tempBuffer:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Lcom/nuance/dragon/toolkit/audio/AudioSource;->getChunksAvailableForSink(Lcom/nuance/dragon/toolkit/audio/AudioSink;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 70
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_tempBuffer:Ljava/util/ArrayList;

    invoke-virtual {p1, p2, v3}, Lcom/nuance/dragon/toolkit/audio/AudioSource;->getAllAudioChunksForSink(Lcom/nuance/dragon/toolkit/audio/AudioSink;Ljava/util/List;)V

    .line 72
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_tempBuffer:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;

    .line 73
    .local v0, "chunk":Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;, "TAudioChunkType;"
    new-instance v2, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_audioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    iget-object v4, v0, Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;->audioBytes:[B

    iget v5, v0, Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;->audioDuration:I

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;-><init>(Lcom/nuance/dragon/toolkit/audio/AudioType;[BI)V

    .line 74
    .local v2, "newChunk":Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;
    iget-wide v4, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_timeStamp:J

    iget v3, v0, Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;->audioDuration:I

    invoke-virtual {v2, v4, v5, v3}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;->setTimeStamp(JI)V

    .line 76
    iget-wide v4, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_timeStamp:J

    iget v3, v0, Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;->audioDuration:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_timeStamp:J

    .line 77
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_buffer:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_buffer:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/16 v4, 0x8

    if-le v3, v4, :cond_0

    .line 79
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_buffer:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 82
    .end local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;, "TAudioChunkType;"
    .end local v2    # "newChunk":Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_tempBuffer:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->notifyChunksAvailable()V

    .line 85
    return-void
.end method

.method public connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<TAudioChunkType;>;)V"
        }
    .end annotation

    .prologue
    .line 108
    .local p0, "this":Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;, "Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe<TAudioChunkType;>;"
    .local p1, "source":Lcom/nuance/dragon/toolkit/audio/AudioSource;, "Lcom/nuance/dragon/toolkit/audio/AudioSource<TAudioChunkType;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_buffer:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 113
    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/audio/AudioSource;->getAudioType()Lcom/nuance/dragon/toolkit/audio/AudioType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_audioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    .line 114
    invoke-super {p0, p1}, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 115
    return-void
.end method

.method protected framesDropped(Lcom/nuance/dragon/toolkit/audio/AudioSource;Lcom/nuance/dragon/toolkit/audio/AudioSink;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<TAudioChunkType;>;",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TAudioChunkType;>;)V"
        }
    .end annotation

    .prologue
    .line 119
    .local p0, "this":Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;, "Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe<TAudioChunkType;>;"
    .local p1, "fromSource":Lcom/nuance/dragon/toolkit/audio/AudioSource;, "Lcom/nuance/dragon/toolkit/audio/AudioSource<TAudioChunkType;>;"
    .local p2, "internalSink":Lcom/nuance/dragon/toolkit/audio/AudioSink;, "Lcom/nuance/dragon/toolkit/audio/AudioSink<TAudioChunkType;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->notifyFramesDropped()V

    .line 120
    return-void
.end method

.method protected getAudioChunk()Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TAudioChunkType;"
        }
    .end annotation

    .prologue
    .line 54
    .local p0, "this":Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;, "Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe<TAudioChunkType;>;"
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_buffer:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_buffer:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;

    .local v0, "chunk":Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;, "TAudioChunkType;"
    move-object v1, v0

    .line 59
    check-cast v1, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_recentChunk:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;

    .line 63
    .end local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;, "TAudioChunkType;"
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAudioType()Lcom/nuance/dragon/toolkit/audio/AudioType;
    .locals 1

    .prologue
    .line 129
    .local p0, "this":Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;, "Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe<TAudioChunkType;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_audioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    return-object v0
.end method

.method public getChunksAvailable()I
    .locals 2

    .prologue
    .line 139
    .local p0, "this":Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;, "Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe<TAudioChunkType;>;"
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_buffer:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x2

    .line 140
    .local v0, "nCount":I
    if-gez v0, :cond_0

    .line 141
    const/4 v0, 0x0

    .line 144
    :cond_0
    return v0
.end method

.method public getChunksDuration()J
    .locals 6

    .prologue
    .line 88
    .local p0, "this":Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;, "Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe<TAudioChunkType;>;"
    const-wide/16 v2, 0x0

    .line 90
    .local v2, "lDuration":J
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_buffer:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;

    .line 91
    .local v0, "chunk":Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;
    iget v4, v0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;->audioDuration:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 92
    goto :goto_0

    .line 94
    .end local v0    # "chunk":Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;
    :cond_0
    return-wide v2
.end method

.method public getFirstChunkTimeStamp()J
    .locals 2

    .prologue
    .line 98
    .local p0, "this":Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;, "Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe<TAudioChunkType;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_buffer:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 99
    const-string v0, "SamsungBufferingPipe"

    const-string v1, "= buffer size is 0 in getFirstChunkTimeStamp, use _timeStamp"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_timeStamp:J

    .line 103
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_buffer:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;->timeStamp:J

    goto :goto_0
.end method

.method public getRecentTimeStamp()J
    .locals 2

    .prologue
    .line 148
    .local p0, "this":Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;, "Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe<TAudioChunkType;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_recentChunk:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;

    if-nez v0, :cond_0

    .line 149
    const-wide/16 v0, -0x1

    .line 151
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->_recentChunk:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;->timeStamp:J

    goto :goto_0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 134
    .local p0, "this":Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;, "Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe<TAudioChunkType;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->isSourceActive()Z

    move-result v0

    return v0
.end method

.method protected sourceClosed(Lcom/nuance/dragon/toolkit/audio/AudioSource;Lcom/nuance/dragon/toolkit/audio/AudioSink;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<TAudioChunkType;>;",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TAudioChunkType;>;)V"
        }
    .end annotation

    .prologue
    .line 124
    .local p0, "this":Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;, "Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe<TAudioChunkType;>;"
    .local p1, "fromSource":Lcom/nuance/dragon/toolkit/audio/AudioSource;, "Lcom/nuance/dragon/toolkit/audio/AudioSource<TAudioChunkType;>;"
    .local p2, "internalSink":Lcom/nuance/dragon/toolkit/audio/AudioSink;, "Lcom/nuance/dragon/toolkit/audio/AudioSink<TAudioChunkType;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungBufferingPipe;->notifySourceClosed()V

    .line 125
    return-void
.end method
