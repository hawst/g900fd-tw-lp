.class public Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;
.super Landroid/widget/BaseAdapter;
.source "VNSettingLogoFontListAdapter.java"


# static fields
.field public static final APPLE_MINT:Ljava/lang/String; = "Apple mint"

.field public static final CHOCO:Ljava/lang/String; = "Choco"

.field public static final CHOCO_COOKY:Ljava/lang/String; = "Choco cooky"

.field public static final COOL:Ljava/lang/String; = "Cool"

.field public static final COOL_JAZZ:Ljava/lang/String; = "Cool jazz"

.field public static final DEFAULT:Ljava/lang/String; = "default"

.field public static final DROIDSERIF_ITALIC:Ljava/lang/String; = "Droidserifitalic.xml"

.field static final FONT_DIRECTORY:Ljava/lang/String; = "fonts/"

.field static final FONT_DROID_SANS:Ljava/lang/String; = "DroidSans.ttf"

.field static final FONT_EXTENSION:Ljava/lang/String; = ".ttf"

.field static final FONT_PACKAGE:Ljava/lang/String; = "com.monotype.android.font."

.field public static final KAITI:Ljava/lang/String; = "Kaiti"

.field public static final MARUBERI:Ljava/lang/String; = "Maruberi"

.field public static final MIAO:Ljava/lang/String; = "Miao"

.field public static final MINCHO:Ljava/lang/String; = "Mincho"

.field public static final POP:Ljava/lang/String; = "Pop"

.field public static final ROSE:Ljava/lang/String; = "Rose"

.field public static final ROSE_MARY:Ljava/lang/String; = "Rosemary"

.field public static final SAMSUNG_SANS:Ljava/lang/String; = "Samsung sans"

.field public static final SHAONV:Ljava/lang/String; = "Shao nv"

.field private static final TAG:Ljava/lang/String; = "VNSettingLogoFontListAdapter"

.field public static final TINKER_BELL:Ljava/lang/String; = "Tinker bell"

.field public static final UDMINCHO:Ljava/lang/String; = "UDMincho"

.field public static final UDRGOTHIC:Ljava/lang/String; = "UDRGothic"

.field private static mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;


# instance fields
.field context:Landroid/content/Context;

.field private droidSansFont:Landroid/graphics/Typeface;

.field public mFontAssetManager:Landroid/content/res/AssetManager;

.field public mFontNames:Ljava/util/Vector;

.field public mFontPackageNames:Ljava/util/Vector;

.field mInflater:Landroid/view/LayoutInflater;

.field public mInstalledApplications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mPackageManager:Landroid/content/pm/PackageManager;

.field public mTypefaceFiles:Ljava/util/Vector;

.field public mTypefaceFinder:Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;

.field private final mTypefaces:Ljava/util/Vector;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    .line 54
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 56
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontAssetManager:Landroid/content/res/AssetManager;

    .line 58
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaceFinder:Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;

    .line 59
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontPackageNames:Ljava/util/Vector;

    .line 60
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontNames:Ljava/util/Vector;

    .line 61
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaceFiles:Ljava/util/Vector;

    .line 62
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaces:Ljava/util/Vector;

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->droidSansFont:Landroid/graphics/Typeface;

    .line 120
    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 123
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 53
    iput-object v7, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    .line 54
    iput-object v7, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 56
    iput-object v7, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 57
    iput-object v7, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontAssetManager:Landroid/content/res/AssetManager;

    .line 58
    new-instance v6, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;

    invoke-direct {v6}, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaceFinder:Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;

    .line 59
    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontPackageNames:Ljava/util/Vector;

    .line 60
    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontNames:Ljava/util/Vector;

    .line 61
    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaceFiles:Ljava/util/Vector;

    .line 62
    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaces:Ljava/util/Vector;

    .line 68
    iput-object v7, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->droidSansFont:Landroid/graphics/Typeface;

    .line 124
    iput-object p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    .line 125
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaceFinder:Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;

    iput-object p1, v6, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->context:Landroid/content/Context;

    .line 126
    const-string v6, "layout_inflater"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    iput-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 128
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 131
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v7, 0x80

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mInstalledApplications:Ljava/util/List;

    .line 134
    const/4 v3, 0x0

    .line 135
    .local v3, "fontPackageName":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mInstalledApplications:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_1

    .line 136
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mInstalledApplications:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ApplicationInfo;

    iget-object v3, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 137
    const-string v6, "com.monotype.android.font."

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 138
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v7, 0x80

    invoke-virtual {v6, v3, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 140
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v6, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iput-object v6, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 141
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v5

    .line 143
    .local v5, "res":Landroid/content/res/Resources;
    invoke-virtual {v5}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontAssetManager:Landroid/content/res/AssetManager;

    .line 144
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaceFinder:Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontAssetManager:Landroid/content/res/AssetManager;

    invoke-virtual {v6, v7, v3}, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->findTypefaces(Landroid/content/res/AssetManager;Ljava/lang/String;)Z

    .line 135
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v5    # "res":Landroid/content/res/Resources;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 148
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaceFinder:Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontNames:Ljava/util/Vector;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaceFiles:Ljava/util/Vector;

    iget-object v9, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontPackageNames:Ljava/util/Vector;

    invoke-virtual {v6, v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->getSansEntries(Ljava/util/Vector;Ljava/util/Vector;Ljava/util/Vector;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    .end local v3    # "fontPackageName":Ljava/lang/String;
    .end local v4    # "i":I
    :goto_1
    new-instance v2, Ljava/io/File;

    const-string v6, "/system/fonts/UIFont.ttf"

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 155
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 156
    const-string v6, "/system/fonts/UIFont.ttf"

    invoke-static {v6}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->droidSansFont:Landroid/graphics/Typeface;

    .line 160
    :goto_2
    return-void

    .line 150
    .end local v2    # "f":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 151
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "VNSettingLogoFontListAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "font package not found, just use default font, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 158
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "f":Ljava/io/File;
    :cond_2
    const-string v6, "/system/fonts/DroidSans.ttf"

    invoke-static {v6}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->droidSansFont:Landroid/graphics/Typeface;

    goto :goto_2
.end method

.method public static destroyInstance()V
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    if-eqz v0, :cond_0

    .line 115
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    .line 117
    :cond_0
    return-void
.end method

.method private getFont(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 8
    .param p1, "typeface"    # Ljava/lang/String;
    .param p2, "fontPackageName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 346
    const-string v6, ".xml"

    const-string v7, ".ttf"

    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 348
    .local v2, "fontName":Ljava/lang/String;
    const/4 v6, 0x0

    :try_start_0
    iput-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontAssetManager:Landroid/content/res/AssetManager;

    .line 349
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v7, 0x80

    invoke-virtual {v6, p2, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 351
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v6, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iput-object v6, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 352
    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v4

    .line 354
    .local v4, "res":Landroid/content/res/Resources;
    invoke-virtual {v4}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontAssetManager:Landroid/content/res/AssetManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontAssetManager:Landroid/content/res/AssetManager;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fonts/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 360
    .local v3, "is":Ljava/io/InputStream;
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 365
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fonts/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontAssetManager:Landroid/content/res/AssetManager;

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/settings/fragment/FontCache;->get(Ljava/lang/String;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v5

    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v4    # "res":Landroid/content/res/Resources;
    :goto_0
    return-object v5

    .line 355
    :catch_0
    move-exception v1

    .line 356
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0

    .line 361
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v4    # "res":Landroid/content/res/Resources;
    :catch_1
    move-exception v1

    .line 362
    .local v1, "e":Ljava/io/IOException;
    invoke-direct {p0, v2, p2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->getFontfromCR(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v5

    goto :goto_0
.end method

.method private getFontfromCR(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 15
    .param p1, "sFont"    # Ljava/lang/String;
    .param p2, "sPackageName"    # Ljava/lang/String;

    .prologue
    .line 288
    const/4 v2, 0x0

    .line 290
    .local v2, "cr":Landroid/content/ContentResolver;
    :try_start_0
    iget-object v13, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 294
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "content://"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/fonts/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    .line 295
    .local v12, "uriFont":Landroid/net/Uri;
    const/4 v7, 0x0

    .line 297
    .local v7, "isFont":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v2, v12}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v7

    .line 301
    const/4 v6, 0x0

    .line 303
    .local v6, "fTemp":Ljava/io/File;
    :try_start_2
    const-string v13, "font"

    const/4 v14, 0x0

    invoke-static {v13, v14}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v6

    .line 311
    const/4 v8, 0x0

    .line 313
    .local v8, "os":Ljava/io/BufferedOutputStream;
    :try_start_3
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 314
    .local v5, "fOut":Ljava/io/FileOutputStream;
    new-instance v9, Ljava/io/BufferedOutputStream;

    invoke-direct {v9, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 316
    .end local v8    # "os":Ljava/io/BufferedOutputStream;
    .local v9, "os":Ljava/io/BufferedOutputStream;
    const/16 v13, 0x400

    :try_start_4
    new-array v1, v13, [B

    .line 318
    .local v1, "b":[B
    const/4 v10, 0x0

    .line 319
    .local v10, "read":I
    :goto_0
    invoke-virtual {v7, v1}, Ljava/io/InputStream;->read([B)I

    move-result v10

    if-lez v10, :cond_0

    .line 320
    const/4 v13, 0x0

    invoke-virtual {v9, v1, v13, v10}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7

    goto :goto_0

    .line 328
    .end local v1    # "b":[B
    .end local v10    # "read":I
    :catch_0
    move-exception v3

    move-object v8, v9

    .line 329
    .end local v5    # "fOut":Ljava/io/FileOutputStream;
    .end local v9    # "os":Ljava/io/BufferedOutputStream;
    .local v3, "e":Ljava/io/FileNotFoundException;
    .restart local v8    # "os":Ljava/io/BufferedOutputStream;
    :goto_1
    const/4 v11, 0x0

    .line 342
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    .end local v6    # "fTemp":Ljava/io/File;
    .end local v7    # "isFont":Ljava/io/InputStream;
    .end local v8    # "os":Ljava/io/BufferedOutputStream;
    .end local v12    # "uriFont":Landroid/net/Uri;
    :goto_2
    return-object v11

    .line 291
    :catch_1
    move-exception v3

    .line 292
    .local v3, "e":Ljava/lang/Exception;
    const/4 v11, 0x0

    goto :goto_2

    .line 298
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v7    # "isFont":Ljava/io/InputStream;
    .restart local v12    # "uriFont":Landroid/net/Uri;
    :catch_2
    move-exception v3

    .line 299
    .restart local v3    # "e":Ljava/lang/Exception;
    const/4 v11, 0x0

    goto :goto_2

    .line 304
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v6    # "fTemp":Ljava/io/File;
    :catch_3
    move-exception v3

    .line 306
    .local v3, "e":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6

    .line 309
    :goto_3
    const/4 v11, 0x0

    goto :goto_2

    .line 322
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v1    # "b":[B
    .restart local v5    # "fOut":Ljava/io/FileOutputStream;
    .restart local v9    # "os":Ljava/io/BufferedOutputStream;
    .restart local v10    # "read":I
    :cond_0
    :try_start_6
    invoke-virtual {v9}, Ljava/io/BufferedOutputStream;->flush()V

    .line 323
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V

    .line 324
    invoke-virtual {v9}, Ljava/io/BufferedOutputStream;->close()V

    .line 325
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 326
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7

    .line 339
    invoke-static {v6}, Landroid/graphics/Typeface;->createFromFile(Ljava/io/File;)Landroid/graphics/Typeface;

    move-result-object v11

    .line 340
    .local v11, "tf":Landroid/graphics/Typeface;
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto :goto_2

    .line 330
    .end local v1    # "b":[B
    .end local v5    # "fOut":Ljava/io/FileOutputStream;
    .end local v9    # "os":Ljava/io/BufferedOutputStream;
    .end local v10    # "read":I
    .end local v11    # "tf":Landroid/graphics/Typeface;
    .restart local v8    # "os":Ljava/io/BufferedOutputStream;
    :catch_4
    move-exception v3

    .line 332
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_7
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 333
    invoke-virtual {v8}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 337
    :goto_5
    const/4 v11, 0x0

    goto :goto_2

    .line 334
    :catch_5
    move-exception v4

    .line 335
    .local v4, "ex":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 307
    .end local v4    # "ex":Ljava/lang/Exception;
    .end local v8    # "os":Ljava/io/BufferedOutputStream;
    :catch_6
    move-exception v13

    goto :goto_3

    .line 330
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v5    # "fOut":Ljava/io/FileOutputStream;
    .restart local v9    # "os":Ljava/io/BufferedOutputStream;
    :catch_7
    move-exception v3

    move-object v8, v9

    .end local v9    # "os":Ljava/io/BufferedOutputStream;
    .restart local v8    # "os":Ljava/io/BufferedOutputStream;
    goto :goto_4

    .line 328
    .end local v5    # "fOut":Ljava/io/FileOutputStream;
    :catch_8
    move-exception v3

    goto :goto_1
.end method

.method public static getInstanceFontListAdapter(Landroid/content/Context;)Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    sget-object v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    .line 110
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    return-object v0
.end method

.method private setFont(ILandroid/widget/TextView;)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 248
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaces:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    .line 249
    .local v0, "fontTypeface":Landroid/graphics/Typeface;
    if-eqz v0, :cond_0

    .line 250
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 252
    :cond_0
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 221
    if-nez p2, :cond_0

    .line 222
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 223
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030027

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 226
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v2, 0x7f0e0060

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 227
    .local v1, "tv":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->getFontName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->setFont(ILandroid/widget/TextView;)V

    .line 229
    const/high16 v2, 0x41900000    # 18.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 230
    return-object p2
.end method

.method public getFont(I)Landroid/graphics/Typeface;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 369
    if-nez p1, :cond_0

    .line 370
    const-string v1, "/system/fonts/Roboto-Regular.ttf"

    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 373
    :goto_0
    return-object v0

    .line 372
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaces:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    .line 373
    .local v0, "fontTypeface":Landroid/graphics/Typeface;
    goto :goto_0
.end method

.method public getFontName(I)Ljava/lang/String;
    .locals 5
    .param p1, "position"    # I

    .prologue
    const v4, 0x7f0b00bf

    const v3, 0x7f0b00b8

    const v2, 0x7f0b00b7

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 176
    .local v0, "tmpString":Ljava/lang/String;
    const-string v1, "Cool"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 216
    .restart local v0    # "tmpString":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 178
    :cond_1
    const-string v1, "Rose"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "tmpString":Ljava/lang/String;
    goto :goto_0

    .line 180
    :cond_2
    const-string v1, "Choco"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "tmpString":Ljava/lang/String;
    goto :goto_0

    .line 183
    :cond_3
    const-string v1, "Rosemary"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 184
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "tmpString":Ljava/lang/String;
    goto :goto_0

    .line 185
    :cond_4
    const-string v1, "Choco cooky"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "tmpString":Ljava/lang/String;
    goto :goto_0

    .line 188
    :cond_5
    const-string v1, "Cool jazz"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "tmpString":Ljava/lang/String;
    goto :goto_0

    .line 190
    :cond_6
    const-string v1, "Samsung sans"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 191
    const-string v0, "Samsung Sans"

    goto :goto_0

    .line 192
    :cond_7
    const-string v1, "Apple mint"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 193
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "tmpString":Ljava/lang/String;
    goto/16 :goto_0

    .line 195
    :cond_8
    const-string v1, "Tinker bell"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 196
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00c0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "tmpString":Ljava/lang/String;
    goto/16 :goto_0

    .line 198
    :cond_9
    const-string v1, "Shao nv"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 199
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "tmpString":Ljava/lang/String;
    goto/16 :goto_0

    .line 200
    :cond_a
    const-string v1, "Kaiti"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "tmpString":Ljava/lang/String;
    goto/16 :goto_0

    .line 203
    :cond_b
    const-string v1, "Miao"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "tmpString":Ljava/lang/String;
    goto/16 :goto_0

    .line 205
    :cond_c
    const-string v1, "Maruberi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, "UDRGothic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 206
    :cond_d
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "tmpString":Ljava/lang/String;
    goto/16 :goto_0

    .line 208
    :cond_e
    const-string v1, "Mincho"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, "UDMincho"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 209
    :cond_f
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00bd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "tmpString":Ljava/lang/String;
    goto/16 :goto_0

    .line 211
    :cond_10
    const-string v1, "Pop"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 212
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00be

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "tmpString":Ljava/lang/String;
    goto/16 :goto_0

    .line 213
    :cond_11
    const-string v1, "default"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .end local v0    # "tmpString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "tmpString":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method public getFontandPackageName(I)Ljava/lang/String;
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 377
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 378
    .local v1, "fontfile":Ljava/lang/String;
    const-string v3, ".xml"

    const-string v4, ".ttf"

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 379
    if-nez p1, :cond_0

    .line 380
    const-string v1, "Roboto-Regular.ttf"

    .line 382
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontPackageNames:Ljava/util/Vector;

    invoke-virtual {v3, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 383
    .local v0, "fontPackageName":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 385
    .local v2, "name":Ljava/lang/String;
    return-object v2
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 171
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 235
    if-nez p2, :cond_0

    .line 236
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 237
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030028

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 239
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v2, 0x7f0e0061

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 240
    .local v1, "tv":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->getFontName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->setFont(ILandroid/widget/TextView;)V

    .line 242
    return-object p2
.end method

.method public loadTypefaces()V
    .locals 6

    .prologue
    .line 256
    const/4 v1, 0x0

    .line 257
    .local v1, "fontfile":Ljava/lang/String;
    const/4 v0, 0x0

    .line 258
    .local v0, "fontPackageName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 260
    .local v3, "newTypeface":Landroid/graphics/Typeface;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaces:Ljava/util/Vector;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->droidSansFont:Landroid/graphics/Typeface;

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 262
    const/4 v2, 0x1

    .line 263
    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 264
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 265
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontPackageNames:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 266
    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->getFont(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 268
    const-string v4, "Droidserifitalic.xml"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 269
    const-string v4, "VNSettingLogoFontListAdapter"

    const-string v5, "Remove Doridserif-Italic font "

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const/4 v3, 0x0

    .line 273
    :cond_0
    if-nez v3, :cond_1

    .line 274
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontPackageNames:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 275
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 276
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 281
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->mTypefaces:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 282
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 285
    :cond_2
    return-void
.end method
