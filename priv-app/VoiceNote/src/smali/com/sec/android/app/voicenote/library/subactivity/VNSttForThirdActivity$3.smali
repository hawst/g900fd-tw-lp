.class Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;
.super Landroid/os/Handler;
.source "VNSttForThirdActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V
    .locals 0

    .prologue
    .line 541
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 544
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 585
    :cond_0
    :goto_0
    return-void

    .line 547
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 548
    .local v0, "ft":Landroid/app/FragmentTransaction;
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 583
    :goto_1
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 584
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 550
    :sswitch_0
    const-wide/16 v2, 0x0

    .line 552
    .local v2, "time":J
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$000(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v1

    if-eqz v1, :cond_2

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v1

    if-nez v1, :cond_3

    .line 553
    :cond_2
    const-wide/32 v2, 0x494a8

    .line 557
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$500(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    move-result-object v1

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->updateTime(J)V

    goto :goto_1

    .line 555
    :cond_3
    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecordingTime()J

    move-result-wide v2

    goto :goto_2

    .line 560
    .end local v2    # "time":J
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mPanelTextFragment:Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$500(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->startTimeBlinkAnimation()V

    goto :goto_1

    .line 564
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->invalidateOptionsMenu()V

    .line 565
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # invokes: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->updateUIThread()V
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$600(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V

    goto :goto_1

    .line 568
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # invokes: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->updateUIThread()V
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$600(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V

    .line 569
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # invokes: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V
    invoke-static {v1, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$700(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;Landroid/app/FragmentTransaction;)V

    .line 570
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # invokes: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->updateTime()V
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$800(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V

    goto :goto_1

    .line 573
    :sswitch_4
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # invokes: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->updateUIThread()V
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$600(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V

    .line 574
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # invokes: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V
    invoke-static {v1, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$700(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;Landroid/app/FragmentTransaction;)V

    .line 575
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # invokes: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->updateTime()V
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$800(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V

    goto :goto_1

    .line 578
    :sswitch_5
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$3;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    const/4 v4, -0x1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->setResultWithFinish(I)V

    goto :goto_1

    .line 548
    nop

    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_2
        0x3eb -> :sswitch_4
        0x3ec -> :sswitch_3
        0x7d1 -> :sswitch_5
        0x7d5 -> :sswitch_2
        0x835 -> :sswitch_0
        0x83d -> :sswitch_1
    .end sparse-switch
.end method
