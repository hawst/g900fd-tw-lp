.class Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;
.super Landroid/os/Handler;
.source "VNTrimFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->initEventHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 385
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$700()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$800(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    move-result-object v0

    if-nez v0, :cond_1

    .line 475
    :cond_0
    :goto_0
    return-void

    .line 388
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v3

    .line 389
    .local v3, "startTime":I
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getEndTime()I

    move-result v4

    .line 390
    .local v4, "endTime":I
    sub-int v8, v4, v3

    .line 392
    .local v8, "trimRange":I
    int-to-long v0, v8

    const-wide/16 v10, 0x44b

    cmp-long v0, v0, v10

    if-gez v0, :cond_2

    .line 393
    rsub-int v6, v8, 0x44b

    .line 394
    .local v6, "diff":I
    const-string v0, "VNTrimFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Trim diff : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " trimRange : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    if-le v3, v6, :cond_4

    .line 396
    sub-int/2addr v3, v6

    .line 401
    .end local v6    # "diff":I
    :cond_2
    :goto_1
    const-string v0, "VNTrimFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Trim startTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " endTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 474
    :cond_3
    :goto_2
    :sswitch_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 398
    .restart local v6    # "diff":I
    :cond_4
    add-int/2addr v4, v6

    goto :goto_1

    .line 404
    .end local v6    # "diff":I
    :sswitch_1
    iget v0, p1, Landroid/os/Message;->what:I

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveMode:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$902(I)I

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)I

    move-result v0

    const v1, 0x36ee80

    if-lt v0, v1, :cond_5

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->setProgressDialog()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1100(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V

    .line 412
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$800(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$700()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v10

    invoke-static {v1, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->startTrimming(Ljava/lang/String;ZIILcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;)V

    goto :goto_2

    .line 407
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)I

    move-result v0

    const v1, 0xea60

    if-lt v0, v1, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->isAmrFile()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->setProgressDialog()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1100(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V

    goto :goto_3

    .line 410
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1202(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    goto :goto_3

    .line 415
    :sswitch_2
    iget v0, p1, Landroid/os/Message;->what:I

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveMode:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$902(I)I

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)I

    move-result v0

    const v1, 0x36ee80

    if-lt v0, v1, :cond_7

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->setProgressDialog()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1100(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V

    .line 423
    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$800(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$700()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v10

    invoke-static {v1, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->startTrimming(Ljava/lang/String;ZIILcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;)V

    goto/16 :goto_2

    .line 418
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)I

    move-result v0

    const v1, 0xea60

    if-lt v0, v1, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->isAmrFile()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->setProgressDialog()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1100(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V

    goto :goto_4

    .line 421
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1202(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    goto :goto_4

    .line 428
    :sswitch_3
    iget v7, p1, Landroid/os/Message;->arg1:I

    .line 429
    .local v7, "progress":I
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/app/ProgressDialog;->setProgress(I)V

    goto/16 :goto_2

    .line 435
    .end local v7    # "progress":I
    :sswitch_4
    const/16 v0, 0x46

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimResult:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1302(I)I

    .line 436
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1400()Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 437
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1400()Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;->onProgressChanged(I)V

    .line 438
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1400()Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;->onTrimChanged(II)V

    .line 440
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 443
    :cond_a
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_b

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1500(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1500(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    const v1, 0x7f0e00bf

    const-wide/16 v10, 0x46

    invoke-interface {v0, v1, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onControlButtonSelected(IJ)Z

    .line 449
    :goto_5
    const-string v0, "VNTrimFragment"

    const-string v1, "Trim Completed"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$802(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    goto/16 :goto_2

    .line 447
    :cond_c
    const-string v0, "VNTrimFragment"

    const-string v1, "Trim Completed : mCallbacks is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 455
    :sswitch_5
    const/16 v0, 0x49

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimResult:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1302(I)I

    .line 456
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_d

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1500(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1500(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    const v1, 0x7f0e00bf

    const-wide/16 v10, 0x49

    invoke-interface {v0, v1, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onControlButtonSelected(IJ)Z

    .line 462
    :goto_6
    const-string v0, "VNTrimFragment"

    const-string v1, "Trim Canceled"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$802(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    goto/16 :goto_2

    .line 460
    :cond_e
    const-string v0, "VNTrimFragment"

    const-string v1, "Trim Canceled : mCallbacks is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 468
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/SeekBar;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/SeekBar;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_2

    .line 402
    nop

    :sswitch_data_0
    .sparse-switch
        0x3c -> :sswitch_1
        0x3d -> :sswitch_2
        0x3e -> :sswitch_3
        0x46 -> :sswitch_4
        0x47 -> :sswitch_0
        0x49 -> :sswitch_5
        0x73 -> :sswitch_6
    .end sparse-switch
.end method
