.class final Lcom/sec/android/app/voicenote/common/util/VNUtil$4;
.super Ljava/lang/Object;
.source "VNUtil.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/util/VNUtil;->showChooseSimCardDialog(Landroid/content/Context;Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$ringUri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1397
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNUtil$4;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/app/voicenote/common/util/VNUtil$4;->val$ringUri:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "optionPopupDialogId"    # Landroid/content/DialogInterface;
    .param p2, "position"    # I

    .prologue
    .line 1400
    if-nez p2, :cond_1

    .line 1401
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNUtil$4;->val$context:Landroid/content/Context;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNUtil$4;->val$ringUri:Landroid/net/Uri;

    invoke-static {v0, v1, v2}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 1407
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNUtil$4;->val$ringUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 1408
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNUtil$4;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "DEBUG_RINGTONE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VoiceRecorder : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNUtil$4;->val$ringUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1411
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNUtil$4;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNUtil$4;->val$context:Landroid/content/Context;

    const v2, 0x7f0b010a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1414
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1415
    return-void

    .line 1404
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNUtil$4;->val$context:Landroid/content/Context;

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNUtil$4;->val$ringUri:Landroid/net/Uri;

    invoke-static {v0, v1, v2}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    goto :goto_0
.end method
