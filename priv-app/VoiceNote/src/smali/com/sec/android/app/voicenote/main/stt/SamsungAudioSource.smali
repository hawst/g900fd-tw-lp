.class public Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;
.super Lcom/nuance/dragon/toolkit/audio/sources/SingleSinkSource;
.source "SamsungAudioSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/nuance/dragon/toolkit/audio/sources/SingleSinkSource",
        "<",
        "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
        ">;"
    }
.end annotation


# static fields
.field static final RECENT_BUFFER_SIZE:I = 0xa

.field private static final TAG:Ljava/lang/String; = "SamsungAudioSource"


# instance fields
.field private final _audioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

.field private final _bEnableDRC:Z

.field private _bEnableNS:Z

.field private final _buffer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;"
        }
    .end annotation
.end field

.field private final _bufferHistory:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;"
        }
    .end annotation
.end field

.field private final _bufferToRead:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;"
        }
    .end annotation
.end field

.field private _chunkTimeStamp:J

.field private _closed:Z

.field private _done:Z

.field private _listener:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;

.field private final _mainThreadHandler:Landroid/os/Handler;

.field private _started:Z

.field private _stopped:Z

.field private _voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

.field private _workerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

.field private _workerThreadHandler:Landroid/os/Handler;

.field private isCurrentUTTSaturated:Z

.field private isDRCon:Z

.field private isFirstFrame:Z

.field private isPreviousUTTSaturated:Z


# direct methods
.method public constructor <init>(Lcom/nuance/dragon/toolkit/audio/AudioType;JZ)V
    .locals 2
    .param p1, "audioType"    # Lcom/nuance/dragon/toolkit/audio/AudioType;
    .param p2, "recordingStartTime"    # J
    .param p4, "bEnableDRC"    # Z

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-direct {p0}, Lcom/nuance/dragon/toolkit/audio/sources/SingleSinkSource;-><init>()V

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    .line 78
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isCurrentUTTSaturated:Z

    .line 79
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isPreviousUTTSaturated:Z

    .line 80
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isFirstFrame:Z

    .line 81
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isDRCon:Z

    .line 84
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_buffer:Ljava/util/List;

    .line 85
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bufferToRead:Ljava/util/List;

    .line 86
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_audioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    .line 87
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_closed:Z

    .line 88
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_mainThreadHandler:Landroid/os/Handler;

    .line 89
    iput-wide p2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_chunkTimeStamp:J

    .line 90
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bufferHistory:Ljava/util/List;

    .line 91
    iput-boolean p4, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bEnableDRC:Z

    .line 92
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bEnableNS:Z

    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->initializeVoiceEngine()Z

    .line 95
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_listener:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;)Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;
    .param p1, "x1"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_listener:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_buffer:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->notifyChunksAvailable()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->stopRecordingInternal()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_started:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_started:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_closed:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_closed:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->notifySourceClosed()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)Lcom/nuance/dragon/toolkit/util/WorkerThread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;Lcom/nuance/dragon/toolkit/util/WorkerThread;)Lcom/nuance/dragon/toolkit/util/WorkerThread;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;
    .param p1, "x1"    # Lcom/nuance/dragon/toolkit/util/WorkerThread;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    return-object p1
.end method

.method private cleanup()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 296
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_done:Z

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/util/WorkerThread;->stop()V

    .line 300
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThreadHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThreadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 305
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThreadHandler:Landroid/os/Handler;

    .line 307
    :cond_1
    return-void
.end method

.method private initializeVoiceEngine()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 101
    invoke-static {}, Lcom/samsung/voiceshell/VoiceEngineWrapper;->getInstance()Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    .line 103
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bEnableNS:Z

    if-eqz v3, :cond_0

    .line 105
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v3}, Lcom/samsung/voiceshell/VoiceEngine;->initializeNS()I

    move-result v0

    .line 106
    .local v0, "ret":I
    if-eqz v0, :cond_0

    .line 108
    const-string v2, "SamsungAudioSource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SamsungAudioSource voiceEngine.initializeNS returned "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    .end local v0    # "ret":I
    :goto_0
    return v1

    .line 113
    :cond_0
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bEnableDRC:Z

    if-eqz v3, :cond_1

    .line 115
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v3}, Lcom/samsung/voiceshell/VoiceEngine;->initializeDRC()I

    move-result v0

    .line 116
    .restart local v0    # "ret":I
    if-eqz v0, :cond_1

    .line 118
    const-string v2, "SamsungAudioSource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SamsungAudioSource voiceEngine.initializeDRC returned "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 122
    .end local v0    # "ret":I
    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isCurrentUTTSaturated:Z

    .line 123
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isFirstFrame:Z

    move v1, v2

    .line 125
    goto :goto_0
.end method

.method private processDRC([SI)V
    .locals 4
    .param p1, "pcmBuffer"    # [S
    .param p2, "bufferLen"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 140
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isFirstFrame:Z

    if-ne v1, v3, :cond_0

    .line 141
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isPreviousUTTSaturated:Z

    if-ne v1, v3, :cond_3

    .line 142
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isDRCon:Z

    .line 146
    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isPreviousUTTSaturated:Z

    .line 147
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isFirstFrame:Z

    .line 150
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->checkSaturation([SI)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isCurrentUTTSaturated:Z

    .line 151
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isCurrentUTTSaturated:Z

    if-eqz v1, :cond_1

    .line 152
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isDRCon:Z

    .line 153
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isPreviousUTTSaturated:Z

    .line 154
    const-string v1, "SamsungAudioSource"

    const-string v2, "Saturation happens"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isDRCon:Z

    if-eqz v1, :cond_2

    .line 158
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/voiceshell/VoiceEngine;->processDRC([SI)I

    move-result v0

    .line 159
    .local v0, "ret":I
    if-eqz v0, :cond_2

    .line 160
    const-string v1, "SamsungAudioSource"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processDRC returned "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    .end local v0    # "ret":I
    :cond_2
    return-void

    .line 144
    :cond_3
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->isDRCon:Z

    goto :goto_0
.end method

.method private processNSFrame([SI)V
    .locals 2
    .param p1, "pcmBuffer"    # [S
    .param p2, "bufferLen"    # I

    .prologue
    .line 131
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_voiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/voiceshell/VoiceEngine;->processNSFrame([SI)I

    move-result v0

    .line 134
    .local v0, "ret":I
    return-void
.end method

.method private readBuffer()Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    .locals 2

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bufferToRead:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bufferToRead:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    .line 377
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private stopRecordingInternal()V
    .locals 2

    .prologue
    .line 268
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_done:Z

    if-eqz v1, :cond_0

    .line 278
    :goto_0
    return-void

    .line 271
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->readBuffer()Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    move-result-object v0

    .line 272
    .local v0, "buf":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    if-eqz v0, :cond_1

    .line 273
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->handleNewAudio(Lcom/nuance/dragon/toolkit/audio/AudioChunk;)V

    .line 276
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->cleanup()V

    .line 277
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->handleSourceClosed()V

    goto :goto_0
.end method


# virtual methods
.method public addAudioBuffer(Lcom/nuance/dragon/toolkit/audio/AudioChunk;)V
    .locals 1
    .param p1, "chunk"    # Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    .prologue
    .line 345
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_started:Z

    if-nez v0, :cond_0

    .line 346
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_started:Z

    .line 347
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->handleStarted()V

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_buffer:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->notifyChunksAvailable()V

    .line 357
    return-void
.end method

.method public addAudioBuffer([S)V
    .locals 6
    .param p1, "buf"    # [S

    .prologue
    .line 360
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bEnableNS:Z

    if-eqz v1, :cond_0

    .line 361
    array-length v1, p1

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->processNSFrame([SI)V

    .line 363
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bEnableDRC:Z

    if-eqz v1, :cond_1

    .line 364
    array-length v1, p1

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->processDRC([SI)V

    .line 366
    :cond_1
    new-instance v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    sget-object v1, Lcom/nuance/dragon/toolkit/audio/AudioType;->PCM_16k:Lcom/nuance/dragon/toolkit/audio/AudioType;

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_chunkTimeStamp:J

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/nuance/dragon/toolkit/audio/AudioChunk;-><init>(Lcom/nuance/dragon/toolkit/audio/AudioType;[SJ)V

    .line 367
    .local v0, "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->addAudioBuffer(Lcom/nuance/dragon/toolkit/audio/AudioChunk;)V

    .line 368
    iget-wide v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_chunkTimeStamp:J

    iget v1, v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;->audioDuration:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_chunkTimeStamp:J

    .line 369
    return-void
.end method

.method public adjustBuffer(J)J
    .locals 7
    .param p1, "lStartTimeStamp"    # J

    .prologue
    .line 165
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bufferHistory:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 166
    .local v3, "nSize":I
    const/4 v2, -0x1

    .line 167
    .local v2, "nIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 168
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bufferHistory:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    .line 172
    .local v0, "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    iget-wide v4, v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;->audioTimestamp:J

    cmp-long v4, p1, v4

    if-gez v4, :cond_0

    .line 173
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bufferHistory:Ljava/util/List;

    add-int/lit8 v5, v3, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    iget-wide v4, v4, Lcom/nuance/dragon/toolkit/audio/AudioChunk;->audioTimestamp:J

    .line 188
    .end local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    :goto_1
    return-wide v4

    .line 175
    .restart local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    :cond_0
    iget-wide v4, v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;->audioTimestamp:J

    cmp-long v4, p1, v4

    if-nez v4, :cond_2

    .line 176
    add-int/lit8 v2, v1, -0x1

    .line 181
    .end local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    :cond_1
    const-string v4, "SamsungAudioSource"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nIndex in adjustBuffer: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " size:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    if-ltz v2, :cond_3

    .line 183
    add-int/lit8 v1, v3, -0x1

    :goto_2
    if-lt v1, v2, :cond_3

    .line 184
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bufferHistory:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    .line 185
    .restart local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_buffer:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 183
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 167
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 188
    .end local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    :cond_3
    const-wide/16 v4, 0x0

    goto :goto_1
.end method

.method public checkSaturation([SI)Z
    .locals 6
    .param p1, "audioData"    # [S
    .param p2, "sizeInShorts"    # I

    .prologue
    .line 382
    const/4 v1, 0x0

    .line 383
    .local v1, "isSaturated":Z
    const/4 v2, 0x0

    .line 384
    .local v2, "saturationCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_2

    .line 385
    aget-short v3, p1, v0

    const/16 v4, 0x2000

    if-gt v3, v4, :cond_0

    aget-short v3, p1, v0

    const/16 v4, -0x2000

    if-ge v3, v4, :cond_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 384
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 387
    :cond_2
    const/16 v3, 0x64

    if-le v2, v3, :cond_3

    .line 388
    const/4 v1, 0x1

    .line 390
    :cond_3
    const/4 v3, 0x1

    if-le v2, v3, :cond_4

    .line 392
    const-string v3, "SamsungAudioSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DRC : saturationCount :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    :cond_4
    return v1
.end method

.method protected bridge synthetic getAudioChunk()Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->getAudioChunk()Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    move-result-object v0

    return-object v0
.end method

.method protected getAudioChunk()Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_buffer:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 195
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_buffer:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    .line 197
    .local v0, "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bufferHistory:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bufferHistory:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0xa

    if-le v1, v2, :cond_0

    .line 199
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_bufferHistory:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 204
    .end local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAudioType()Lcom/nuance/dragon/toolkit/audio/AudioType;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_audioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    return-object v0
.end method

.method public getChunksAvailable()I
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_buffer:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method protected handleNewAudio(Lcom/nuance/dragon/toolkit/audio/AudioChunk;)V
    .locals 2
    .param p1, "audio"    # Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_mainThreadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$2;-><init>(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;Lcom/nuance/dragon/toolkit/audio/AudioChunk;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 246
    return-void
.end method

.method public handleSourceClosed()V
    .locals 1

    .prologue
    .line 310
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->handleSourceClosed(Z)V

    .line 311
    return-void
.end method

.method protected handleSourceClosed(Z)V
    .locals 2
    .param p1, "success"    # Z

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_mainThreadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$4;-><init>(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 341
    return-void
.end method

.method protected handleStarted()V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_mainThreadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$1;-><init>(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 232
    return-void
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_closed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startRecording()V
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->startRecording(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;)V

    .line 250
    return-void
.end method

.method public startRecording(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;

    .prologue
    const/4 v2, 0x0

    .line 253
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_done:Z

    .line 254
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_listener:Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$Listener;

    .line 255
    new-instance v0, Lcom/nuance/dragon/toolkit/util/WorkerThread;

    const-string v1, "com.nuance.dragon.toolkit.sample.SamsungAudioSource"

    invoke-direct {v0, v1}, Lcom/nuance/dragon/toolkit/util/WorkerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/util/WorkerThread;->start()V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/util/WorkerThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThreadHandler:Landroid/os/Handler;

    .line 259
    const-string v0, "Starting recording"

    invoke-static {p0, v0}, Lcom/nuance/dragon/toolkit/util/Logger;->debug(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_started:Z

    .line 263
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_stopped:Z

    .line 264
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_closed:Z

    .line 265
    return-void
.end method

.method public stopRecording()J
    .locals 2

    .prologue
    .line 281
    const-string v0, "Stopping recording"

    invoke-static {p0, v0}, Lcom/nuance/dragon/toolkit/util/Logger;->debug(Ljava/lang/Object;Ljava/lang/String;)V

    .line 282
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_started:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_stopped:Z

    if-nez v0, :cond_0

    .line 283
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_stopped:Z

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_workerThreadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource$3;-><init>(Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 292
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioSource;->_chunkTimeStamp:J

    return-wide v0
.end method
