.class Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$3;
.super Ljava/lang/Object;
.source "VNExpandableCursorAdapter.java"

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setLableHoverPopup(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)V
    .locals 0

    .prologue
    .line 645
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$3;->this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "hpw"    # Landroid/widget/HoverPopupWindow;

    .prologue
    const/4 v1, 0x0

    .line 649
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$3;->this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->access$100(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEnableAirViewInforPreview(Landroid/content/ContentResolver;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 650
    if-eqz p2, :cond_0

    .line 651
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$3;->this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->access$100(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03001f

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 653
    .local v0, "titleText":Landroid/widget/TextView;
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 654
    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 655
    invoke-virtual {p2, v0}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 656
    invoke-virtual {p2}, Landroid/widget/HoverPopupWindow;->show()V

    .line 657
    const/4 v1, 0x1

    .line 660
    .end local v0    # "titleText":Landroid/widget/TextView;
    :cond_0
    return v1
.end method
