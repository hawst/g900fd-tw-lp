.class Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$4;
.super Ljava/lang/Object;
.source "VoiceNoteService.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->showStopDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0

    .prologue
    .line 1551
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$4;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/4 v3, 0x1

    .line 1554
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$4;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->hideNotification()Z

    .line 1556
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$4;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const-class v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1557
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "isFromQuick"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1558
    const-string v1, "activitymode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1559
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1560
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1561
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1562
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$4;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->startActivity(Landroid/content/Intent;)V

    .line 1563
    return-void
.end method
