.class Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;
.super Landroid/os/Handler;
.source "VNPlayerToolbarFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V
    .locals 0

    .prologue
    .line 1089
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v3, 0x36ee80

    const v1, 0xea60

    const/4 v2, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1093
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 1169
    :cond_0
    :goto_0
    :sswitch_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 1170
    return-void

    .line 1095
    :sswitch_1
    iget v0, p1, Landroid/os/Message;->what:I

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveMode:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1602(I)I

    .line 1096
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v0

    if-lt v0, v3, :cond_1

    .line 1097
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setProgressDialog()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1700(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    .line 1103
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v1

    if-le v0, v1, :cond_3

    .line 1104
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1800(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v8

    invoke-static {v1, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->startTrimming(Ljava/lang/String;ZIILcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;)V

    goto :goto_0

    .line 1098
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v0

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->isAmrFile()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1099
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setProgressDialog()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1700(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    goto :goto_1

    .line 1101
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    iput-object v5, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    goto :goto_1

    .line 1109
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1800(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v8

    invoke-static {v1, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->startTrimming(Ljava/lang/String;ZIILcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;)V

    goto/16 :goto_0

    .line 1116
    :sswitch_2
    iget v0, p1, Landroid/os/Message;->what:I

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveMode:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1602(I)I

    .line 1117
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v0

    if-lt v0, v3, :cond_4

    .line 1118
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setProgressDialog()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1700(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    .line 1124
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v1

    if-le v0, v1, :cond_6

    .line 1125
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1800(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v3

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v4

    move v2, v7

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->startTrimming(Ljava/lang/String;ZIILcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;)V

    goto/16 :goto_0

    .line 1119
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v0

    if-lt v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->isAmrFile()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1120
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setProgressDialog()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1700(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    goto :goto_2

    .line 1122
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    iput-object v5, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    goto :goto_2

    .line 1130
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1800(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v3

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v4

    move v2, v7

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->startTrimming(Ljava/lang/String;ZIILcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;)V

    goto/16 :goto_0

    .line 1137
    :sswitch_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_0

    .line 1138
    const-string v0, "VNPlyayerToolbarFragment"

    const-string v1, "Start Trim"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1142
    :sswitch_4
    iget v6, p1, Landroid/os/Message;->arg1:I

    .line 1143
    .local v6, "progress":I
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 1144
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setProgress(I)V

    goto/16 :goto_0

    .line 1148
    .end local v6    # "progress":I
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_7

    .line 1149
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1151
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$800(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onUpdateStateNoModeChange(Z)V

    .line 1152
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$800(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSaveMode:I
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1600()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onTrimCompleted(I)V

    .line 1153
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->refreshToolbar(Z)V

    goto/16 :goto_0

    .line 1158
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->restoreRepeat()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1900(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    goto/16 :goto_0

    .line 1161
    :sswitch_7
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->showBookmarkIndicators()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$2000(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    goto/16 :goto_0

    .line 1164
    :sswitch_8
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->executeProgress()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$2100(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    goto/16 :goto_0

    .line 1093
    :sswitch_data_0
    .sparse-switch
        0x3c -> :sswitch_1
        0x3d -> :sswitch_2
        0x3e -> :sswitch_4
        0x46 -> :sswitch_5
        0x47 -> :sswitch_3
        0x49 -> :sswitch_0
        0x1228 -> :sswitch_6
        0x1982 -> :sswitch_7
        0x8281 -> :sswitch_8
    .end sparse-switch
.end method
