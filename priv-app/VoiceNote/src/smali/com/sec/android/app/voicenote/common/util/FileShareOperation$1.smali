.class Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;
.super Landroid/os/Handler;
.source "FileShareOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/util/FileShareOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 288
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mFragment:Landroid/app/Fragment;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$100(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 290
    .local v1, "res":Landroid/content/res/Resources;
    iget v2, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v2, :pswitch_data_0

    .line 345
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 292
    :pswitch_0
    :try_start_1
    const-string v2, "FileShareOperation"

    const-string v3, "ShareFiles : handleMessage - SHOW_PROGRESS"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    new-instance v3, Landroid/app/ProgressDialog;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mFragment:Landroid/app/Fragment;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$100(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/Fragment;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$202(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 295
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$200(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/ProgressDialog;

    move-result-object v2

    const v3, 0x7f0b00f8

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 296
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$200(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/ProgressDialog;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 297
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$200(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/ProgressDialog;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 298
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$200(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 301
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$200(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/ProgressDialog;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1$1;-><init>(Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;)V

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 310
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$200(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 288
    .end local v1    # "res":Landroid/content/res/Resources;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 314
    .restart local v1    # "res":Landroid/content/res/Resources;
    :pswitch_1
    :try_start_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 315
    .local v0, "callback":Ljava/lang/Object;
    const-string v2, "FileShareOperation"

    const-string v3, "ShareFiles : handleMessage - FINISH_PROGRESS"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # invokes: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->closeProgressDialog()V
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$500(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)V

    .line 317
    instance-of v2, v0, Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    if-eqz v2, :cond_0

    .line 318
    check-cast v0, Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    .end local v0    # "callback":Ljava/lang/Object;
    invoke-interface {v0}, Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;->finishOperation()V

    goto/16 :goto_0

    .line 323
    :pswitch_2
    const-string v2, "FileShareOperation"

    const-string v3, "ShareFiles : handleMessage - ABORT_PROGRESS"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # invokes: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->closeProgressDialog()V
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$500(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)V

    .line 325
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mFragment:Landroid/app/Fragment;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$100(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0b006b

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 331
    :pswitch_3
    const-string v2, "FileShareOperation"

    const-string v3, "ShareFiles : handleMessage - SHOW_DB_PROGRESS"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    new-instance v3, Landroid/app/ProgressDialog;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mFragment:Landroid/app/Fragment;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$100(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/Fragment;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$202(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$200(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/ProgressDialog;

    move-result-object v2

    const v3, 0x7f0b00f8

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 335
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$200(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/ProgressDialog;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 336
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$200(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/ProgressDialog;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 337
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$200(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 339
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;->this$0:Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    # getter for: Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->access$200(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 290
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
