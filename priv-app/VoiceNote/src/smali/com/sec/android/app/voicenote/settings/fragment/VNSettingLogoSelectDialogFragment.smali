.class public Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNSettingLogoSelectDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private mArrayInteger:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mFragmentManager:Landroid/app/FragmentManager;

.field private mListView:Landroid/widget/ListView;

.field private mViewChild:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mListView:Landroid/widget/ListView;

    .line 48
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mViewChild:Landroid/view/View;

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mFragmentManager:Landroid/app/FragmentManager;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mArrayInteger:Ljava/util/ArrayList;

    .line 54
    return-void
.end method

.method private initView()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mViewChild:Landroid/view/View;

    const v1, 0x7f0e0035

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mListView:Landroid/widget/ListView;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 80
    return-void
.end method

.method private listBinding()Z
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 84
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 86
    .local v6, "resources":Landroid/content/res/Resources;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v3, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "android.intent.extra.STREAM"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 89
    .local v2, "arrayIntent":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    iget-object v8, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mArrayInteger:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 90
    if-eqz v2, :cond_1

    .line 91
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 93
    .local v7, "size":I
    if-lez v7, :cond_0

    .line 94
    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Intent;

    invoke-static {v0, v8}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasAvailableApp(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 95
    const v8, 0x7f0b0092

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v8, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mArrayInteger:Ljava/util/ArrayList;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_0
    if-le v7, v10, :cond_1

    .line 100
    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Intent;

    invoke-static {v0, v8}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasAvailableApp(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 101
    const v8, 0x7f0b013e

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    iget-object v8, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mArrayInteger:Ljava/util/ArrayList;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    .end local v7    # "size":I
    :cond_1
    const v8, 0x7f0b0141

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    iget-object v8, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mArrayInteger:Ljava/util/ArrayList;

    const/4 v9, 0x2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 110
    .restart local v7    # "size":I
    new-array v5, v7, [Ljava/lang/String;

    .line 111
    .local v5, "items":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v7, :cond_2

    .line 112
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    aput-object v8, v5, v4

    .line 111
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 115
    :cond_2
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f030023

    invoke-direct {v1, v8, v9, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 117
    .local v1, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v8, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 118
    return v10
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->listBinding()Z

    .line 75
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mFragmentManager:Landroid/app/FragmentManager;

    .line 60
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 61
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0b0115

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030015

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mViewChild:Landroid/view/View;

    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->initView()V

    .line 64
    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mViewChild:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 66
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 67
    .local v1, "dialog":Landroid/app/AlertDialog;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 68
    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 124
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v0, 0x0

    .line 125
    .local v0, "frag":Landroid/app/Fragment;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mFragmentManager:Landroid/app/FragmentManager;

    if-eqz v3, :cond_0

    .line 126
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mFragmentManager:Landroid/app/FragmentManager;

    const v4, 0x7f0e001e

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 128
    :cond_0
    long-to-int v2, p4

    .line 129
    .local v2, "position":I
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mArrayInteger:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mArrayInteger:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 130
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->mArrayInteger:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 132
    .local v1, "integer":Ljava/lang/Integer;
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 133
    const/16 v3, 0xa

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 136
    .end local v1    # "integer":Ljava/lang/Integer;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoSelectDialogFragment;->dismissAllowingStateLoss()V

    .line 137
    return-void
.end method
