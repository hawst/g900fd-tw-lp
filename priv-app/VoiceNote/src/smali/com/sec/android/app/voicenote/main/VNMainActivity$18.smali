.class Lcom/sec/android/app/voicenote/main/VNMainActivity$18;
.super Ljava/lang/Object;
.source "VNMainActivity.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 4745
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 7
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x848

    const/4 v6, 0x0

    .line 4769
    const-string v2, "VoiceNoteMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onQueryTextChange - string : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 4770
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionBarIsSearched()Z

    move-result v2

    if-nez v2, :cond_0

    .line 4786
    :goto_0
    return v6

    .line 4774
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITH_SVOICE(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITHOUT_SVOICE()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4776
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 4777
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "search"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4778
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 4779
    .local v1, "msg":Landroid/os/Message;
    iput v5, v1, Landroid/os/Message;->what:I

    .line 4780
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 4781
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$4600(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 4782
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$4600(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;

    move-result-object v2

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 4784
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onDoSearch(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 4
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 4749
    const-string v1, "VoiceNoteMainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onQueryTextSubmit - string : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 4751
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 4752
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$300(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/widget/SearchView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 4755
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$600(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setEmptyList()V

    .line 4756
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITH_SVOICE(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITHOUT_SVOICE()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4758
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doStop()V

    .line 4759
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$4600(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x848

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 4760
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->startSearchTask(Ljava/lang/String;)V
    invoke-static {v1, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$3800(Lcom/sec/android/app/voicenote/main/VNMainActivity;Ljava/lang/String;)V

    .line 4764
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 4762
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$18;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onDoSearch(Ljava/lang/String;)V

    goto :goto_0
.end method
