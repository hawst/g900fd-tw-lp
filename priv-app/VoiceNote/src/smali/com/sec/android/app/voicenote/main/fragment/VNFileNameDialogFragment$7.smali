.class final Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$7;
.super Ljava/lang/Object;
.source "VNFileNameDialogFragment.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->getNameFilter(Landroid/app/Activity;ZI)[Landroid/text/InputFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$7;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 16
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    .line 299
    sub-int v13, p3, p2

    sget v14, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->RENAME_MAX_LENGTH:I

    if-le v13, v14, :cond_0

    .line 300
    sget v13, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->RENAME_MAX_LENGTH:I

    add-int p3, p2, v13

    .line 301
    :cond_0
    invoke-interface/range {p1 .. p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v13

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    .line 302
    .local v10, "origTxt":Ljava/lang/String;
    move-object v11, v10

    .line 303
    .local v11, "validTxt":Ljava/lang/String;
    const/4 v6, 0x0

    .line 305
    .local v6, "invalidFlag":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->INVALID_CHARS:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$900()[Ljava/lang/String;

    move-result-object v13

    array-length v13, v13

    if-ge v3, v13, :cond_4

    .line 306
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v12

    .line 307
    .local v12, "validTxtLength":I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_1
    if-ge v7, v12, :cond_3

    .line 308
    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->INVALID_CHARS:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$900()[Ljava/lang/String;

    move-result-object v13

    aget-object v13, v13, v3

    invoke-virtual {v11, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 309
    .local v5, "index":I
    if-ltz v5, :cond_2

    .line 310
    const/4 v6, 0x1

    .line 312
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v13

    if-ge v5, v13, :cond_1

    .line 313
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v14, 0x0

    invoke-virtual {v11, v14, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    add-int/lit8 v14, v5, 0x1

    invoke-virtual {v11, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 307
    :cond_1
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 316
    :cond_2
    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->INVALID_CHARS:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->access$900()[Ljava/lang/String;

    move-result-object v13

    aget-object v13, v13, v3

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 317
    .local v1, "c":C
    const/16 v13, 0x21

    if-lt v1, v13, :cond_1

    const/16 v13, 0x7e

    if-ge v1, v13, :cond_1

    const/16 v13, 0x3f

    if-eq v1, v13, :cond_1

    .line 318
    const v13, 0xfee0

    add-int/2addr v13, v1

    int-to-char v1, v13

    .line 319
    invoke-virtual {v11, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 320
    .local v4, "iDBC":I
    if-ltz v4, :cond_1

    .line 321
    const/4 v6, 0x1

    .line 323
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v13

    if-ge v4, v13, :cond_1

    .line 324
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v14, 0x0

    invoke-virtual {v11, v14, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    add-int/lit8 v14, v4, 0x1

    invoke-virtual {v11, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_2

    .line 305
    .end local v1    # "c":C
    .end local v4    # "iDBC":I
    .end local v5    # "index":I
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 335
    .end local v7    # "j":I
    .end local v12    # "validTxtLength":I
    :cond_4
    const/4 v8, 0x0

    .local v8, "k":I
    :goto_3
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    if-ge v8, v13, :cond_7

    .line 336
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 337
    .local v2, "formerCharCode":C
    add-int/lit8 v13, v8, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    .line 338
    .local v9, "latterCharCode":C
    const v13, 0xd800

    if-lt v2, v13, :cond_5

    const v13, 0xdbff

    if-gt v2, v13, :cond_5

    const v13, 0xdc00

    if-lt v9, v13, :cond_5

    const v13, 0xdfff

    if-gt v9, v13, :cond_5

    .line 340
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v13

    invoke-virtual {v11, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 341
    .restart local v5    # "index":I
    if-ltz v5, :cond_5

    .line 342
    const/4 v6, 0x1

    .line 343
    add-int/lit8 v13, v5, 0x2

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v14

    if-lt v13, v14, :cond_6

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v13

    if-ge v5, v13, :cond_6

    .line 344
    const/4 v13, 0x0

    invoke-virtual {v11, v13, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 335
    .end local v5    # "index":I
    :cond_5
    :goto_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 346
    .restart local v5    # "index":I
    :cond_6
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v13

    if-ge v5, v13, :cond_5

    .line 347
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v14, 0x0

    invoke-virtual {v11, v14, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    add-int/lit8 v14, v5, 0x2

    invoke-virtual {v11, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_4

    .line 355
    .end local v2    # "formerCharCode":C
    .end local v5    # "index":I
    .end local v9    # "latterCharCode":C
    :cond_7
    invoke-static {v11}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->hasEmojiString(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 356
    const/4 v6, 0x1

    .line 357
    const-string v11, ""

    .line 360
    :cond_8
    if-eqz v6, :cond_9

    .line 361
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment$7;->val$activity:Landroid/app/Activity;

    const v14, 0x7f0b0099

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showManagedToast(Landroid/content/Context;II)V

    .line 366
    .end local v11    # "validTxt":Ljava/lang/String;
    :goto_5
    return-object v11

    .restart local v11    # "validTxt":Ljava/lang/String;
    :cond_9
    const/4 v11, 0x0

    goto :goto_5
.end method
