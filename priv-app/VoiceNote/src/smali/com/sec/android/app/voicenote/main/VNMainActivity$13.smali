.class Lcom/sec/android/app/voicenote/main/VNMainActivity$13;
.super Landroid/content/BroadcastReceiver;
.source "VNMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;->registerBroadcastReceiverSip(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 4188
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$13;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 4192
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$13;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    const-string v1, "AxT9IME.isVisibleWindow"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->bShowKeyboard:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$4402(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z

    .line 4193
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$13;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionBarIsSearched()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$13;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$300(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/widget/SearchView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$13;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$300(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/widget/SearchView;

    move-result-object v0

    iget-object v0, v0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    if-eqz v0, :cond_0

    .line 4194
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$13;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$300(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/widget/SearchView;

    move-result-object v0

    iget-object v0, v0, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$13;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->bShowKeyboard:Z
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$4400(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SearchView$SearchAutoComplete;->setCursorVisible(Z)V

    .line 4196
    :cond_0
    return-void
.end method
