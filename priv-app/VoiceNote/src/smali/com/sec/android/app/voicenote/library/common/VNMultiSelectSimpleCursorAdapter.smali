.class public Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "VNMultiSelectSimpleCursorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final KEY_TAG_ID:I = 0x0

.field private static final TAG:Ljava/lang/String; = "VNMultiSelectSimpleCursorAdapter"


# instance fields
.field private bShowSTTFileOnly:Z

.field private mCallback:Lcom/sec/android/app/voicenote/library/common/VNAdapterCallback;

.field private mCheckedIdArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mContextChild:Landroid/content/Context;

.field private mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

.field private mDisabledItemCount:I

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLayout:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "from"    # [Ljava/lang/String;
    .param p5, "to"    # [I
    .param p6, "flagRegisterContentObserver"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 64
    invoke-direct/range {p0 .. p6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 52
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mContextChild:Landroid/content/Context;

    .line 53
    iput v1, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mLayout:I

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mCheckedIdArrayList:Ljava/util/ArrayList;

    .line 56
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->bShowSTTFileOnly:Z

    .line 57
    iput v1, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mDisabledItemCount:I

    .line 59
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 60
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mCallback:Lcom/sec/android/app/voicenote/library/common/VNAdapterCallback;

    .line 65
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mContextChild:Landroid/content/Context;

    .line 66
    iput p2, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mLayout:I

    .line 67
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->LoadLabeltoMap()V

    .line 69
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;)Lcom/sec/android/app/voicenote/library/common/VNAdapterCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mCallback:Lcom/sec/android/app/voicenote/library/common/VNAdapterCallback;

    return-object v0
.end method

.method private getDateFormatByFormatSetting(J)Ljava/lang/String;
    .locals 5
    .param p1, "date"    # J

    .prologue
    .line 262
    const-string v0, ""

    .line 263
    .local v0, "dateString":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mContextChild:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 264
    .local v1, "shortDateFormat":Ljava/text/DateFormat;
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 266
    return-object v0
.end method

.method private stringForTime(J)Ljava/lang/String;
    .locals 13
    .param p1, "timeMs"    # J

    .prologue
    .line 249
    const-wide/16 v8, 0x3e8

    div-long v6, p1, v8

    .line 250
    .local v6, "totalSeconds":J
    const-wide/16 v8, 0x3c

    rem-long v4, v6, v8

    .line 251
    .local v4, "seconds":J
    const-wide/16 v8, 0x3c

    div-long v8, v6, v8

    const-wide/16 v10, 0x3c

    rem-long v2, v8, v10

    .line 252
    .local v2, "minutes":J
    const-wide/16 v8, 0xe10

    div-long v0, v6, v8

    .line 254
    .local v0, "hours":J
    const-wide/16 v8, 0x0

    cmp-long v8, v0, v8

    if-lez v8, :cond_0

    .line 255
    const-string v8, "%d:%02d:%02d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 257
    :goto_0
    return-object v8

    :cond_0
    const-string v8, "%02d:%02d"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 239
    return-void
.end method

.method public getEnabledItemCount()I
    .locals 2

    .prologue
    .line 356
    invoke-super {p0}, Landroid/widget/SimpleCursorAdapter;->getCount()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mDisabledItemCount:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 25
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 100
    if-nez p2, :cond_0

    .line 102
    new-instance v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;

    invoke-direct {v10}, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;-><init>()V

    .line 104
    .local v10, "holder":Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mLayout:I

    move/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 106
    const v21, 0x7f0e0067

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    .line 107
    const v21, 0x7f0e0068

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->dateText:Landroid/widget/TextView;

    .line 108
    const v21, 0x7f0e0069

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->mmsText:Landroid/widget/TextView;

    .line 109
    const v21, 0x7f0e006a

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    .line 111
    const v21, 0x7f0e006e

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    .line 112
    const v21, 0x7f0e0020

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/CheckBox;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    .line 113
    const v21, 0x7f0e006b

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    move-object/from16 v0, v21

    iput-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->secreticon:Landroid/widget/ImageView;

    .line 114
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 122
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 123
    .local v2, "cursor":Landroid/database/Cursor;
    if-nez v2, :cond_1

    .line 124
    const-string v21, "VNMultiSelectSimpleCursorAdapter"

    const-string v22, "bindview : cursor is null"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    .end local p2    # "convertView":Landroid/view/View;
    :goto_1
    return-object p2

    .line 118
    .end local v2    # "cursor":Landroid/database/Cursor;
    .end local v10    # "holder":Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;

    .restart local v10    # "holder":Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;
    goto :goto_0

    .line 128
    .restart local v2    # "cursor":Landroid/database/Cursor;
    :cond_1
    move/from16 v0, p1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 130
    const-wide/16 v6, 0x0

    .line 133
    .local v6, "duration":J
    const-wide/16 v4, 0x0

    .line 134
    .local v4, "date":J
    const-wide/16 v12, -0x1

    .line 135
    .local v12, "id":J
    const/16 v16, -0x1

    .line 136
    .local v16, "labelindex":I
    const/4 v14, 0x0

    .line 137
    .local v14, "is_secretbox":I
    const/16 v18, 0x0

    .line 138
    .local v18, "mimetype":Ljava/lang/String;
    const/16 v19, 0x0

    .line 141
    .local v19, "path":Ljava/lang/String;
    :try_start_0
    const-string v21, "duration"

    move-object/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 142
    const-string v21, "title"

    move-object/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 143
    .local v20, "title":Ljava/lang/String;
    const-string v21, "date_modified"

    move-object/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 144
    const-string v21, "_id"

    move-object/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 145
    const-string v21, "label_id"

    move-object/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 146
    const-string v21, "is_secretbox"

    move-object/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 147
    const-string v21, "mime_type"

    move-object/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 148
    const-string v21, "_data"

    move-object/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v19

    .line 158
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "  "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    const-string v21, "%s  |  %s"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->getDateFormatByFormatSetting(J)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->stringForTime(J)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 161
    .local v3, "date_duration":Ljava/lang/String;
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->dateText:Landroid/widget/TextView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelColor(I)Ljava/lang/Integer;

    move-result-object v15

    .line 164
    .local v15, "labelcolor":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelTitle(I)Ljava/lang/String;

    move-result-object v17

    .line 166
    .local v17, "labeltitle":Ljava/lang/String;
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    if-eqz v21, :cond_5

    if-eqz v15, :cond_5

    .line 167
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 168
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 169
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 180
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->isItemChecked(J)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 181
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 185
    :goto_3
    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v14, v0, :cond_7

    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->secreticon:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    if-eqz v21, :cond_7

    .line 186
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->secreticon:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 190
    :cond_3
    :goto_4
    if-eqz v18, :cond_8

    const-string v21, "audio/amr"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 191
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->mmsText:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setVisibility(I)V

    .line 196
    :goto_5
    if-eqz v19, :cond_a

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isM4A(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 197
    new-instance v9, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    move-object/from16 v0, v19

    invoke-direct {v9, v0}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 198
    .local v9, "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v11

    .line 199
    .local v11, "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    if-eqz v11, :cond_9

    iget-boolean v0, v11, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z

    move/from16 v21, v0

    if-eqz v21, :cond_9

    .line 200
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 209
    .end local v9    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v11    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->bShowSTTFileOnly:Z

    move/from16 v21, v0

    if-eqz v21, :cond_4

    .line 210
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/ImageView;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_b

    .line 211
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 212
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    const v22, 0x7f0e0020

    const-string v23, "disabled"

    invoke-virtual/range {v21 .. v23}, Landroid/widget/CheckBox;->setTag(ILjava/lang/Object;)V

    .line 213
    const/16 v21, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 214
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 215
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mDisabledItemCount:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mDisabledItemCount:I

    .line 224
    :cond_4
    :goto_7
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Landroid/widget/CheckBox;->setTag(ILjava/lang/Object;)V

    .line 225
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    new-instance v22, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$1;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$1;-><init>(Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;)V

    invoke-virtual/range {v21 .. v22}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    invoke-super/range {p0 .. p3}, Landroid/widget/SimpleCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_1

    .line 149
    .end local v3    # "date_duration":Ljava/lang/String;
    .end local v15    # "labelcolor":Ljava/lang/Integer;
    .end local v17    # "labeltitle":Ljava/lang/String;
    .end local v20    # "title":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 150
    .local v8, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v8}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    .line 151
    const-string v21, "VNMultiSelectSimpleCursorAdapter"

    const-string v22, "bindview : getcolumn error"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 153
    .end local v8    # "e":Landroid/database/CursorIndexOutOfBoundsException;
    :catch_1
    move-exception v8

    .line 154
    .local v8, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v8}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_1

    .line 171
    .end local v8    # "e":Ljava/lang/IllegalStateException;
    .restart local v3    # "date_duration":Ljava/lang/String;
    .restart local v15    # "labelcolor":Ljava/lang/Integer;
    .restart local v17    # "labeltitle":Ljava/lang/String;
    .restart local v20    # "title":Ljava/lang/String;
    :cond_5
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    if-eqz v21, :cond_2

    if-nez v15, :cond_2

    .line 172
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mContextChild:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f08001b

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 173
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 174
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const-string v22, ""

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    goto/16 :goto_2

    .line 183
    :cond_6
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_3

    .line 187
    :cond_7
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->secreticon:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    if-eqz v21, :cond_3

    .line 188
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->secreticon:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    .line 193
    :cond_8
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->mmsText:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 202
    .restart local v9    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .restart local v11    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :cond_9
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    .line 206
    .end local v9    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v11    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :cond_a
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    .line 217
    :cond_b
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 218
    const/16 v21, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 219
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    const v22, 0x7f0e0020

    const-string v23, "enabled"

    invoke-virtual/range {v21 .. v23}, Landroid/widget/CheckBox;->setTag(ILjava/lang/Object;)V

    .line 220
    iget-object v0, v10, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_7
.end method

.method isItemChecked(J)Z
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 348
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mCheckedIdArrayList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 349
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mCheckedIdArrayList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    .line 350
    const/4 v1, 0x1

    .line 352
    :goto_1
    return v1

    .line 348
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 352
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 243
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 245
    .local v0, "view":Landroid/view/View;
    return-object v0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 361
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mDisabledItemCount:I

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->LoadLabeltoMap()V

    .line 364
    :cond_0
    invoke-super {p0}, Landroid/widget/SimpleCursorAdapter;->notifyDataSetChanged()V

    .line 365
    return-void
.end method

.method public setAdapterCallback(Lcom/sec/android/app/voicenote/library/common/VNAdapterCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/app/voicenote/library/common/VNAdapterCallback;

    .prologue
    .line 368
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mCallback:Lcom/sec/android/app/voicenote/library/common/VNAdapterCallback;

    .line 369
    return-void
.end method

.method public setCheckedIDArrayList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->mCheckedIdArrayList:Ljava/util/ArrayList;

    .line 74
    return-void
.end method

.method public setShowSTTFileOnly(Z)V
    .locals 0
    .param p1, "bShow"    # Z

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/common/VNMultiSelectSimpleCursorAdapter;->bShowSTTFileOnly:Z

    .line 78
    return-void
.end method
