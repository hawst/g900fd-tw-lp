.class public Lcom/sec/android/app/voicenote/common/util/FileShareOperation;
.super Ljava/lang/Object;
.source "FileShareOperation.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/common/util/IVNOperation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;
    }
.end annotation


# static fields
.field private static final ABORT_PROGRESS:I = 0x2

.field private static final FINISH_PROGRESS:I = 0x1

.field private static final MAX_SHARE_FILES:I = 0x64

.field private static final SHOW_DB_PROGRESS:I = 0x3

.field private static final SHOW_PROGRESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "FileShareOperation"

.field private static mInstance:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/FileShareOperation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDialog:Landroid/app/DialogFragment;

.field private mFragment:Landroid/app/Fragment;

.field private mOpThread:Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mProgressHandler:Landroid/os/Handler;

.field private mShareMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mInstance:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private constructor <init>(Landroid/app/Fragment;I)V
    .locals 2
    .param p1, "context"    # Landroid/app/Fragment;
    .param p2, "shareMode"    # I

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mFragment:Landroid/app/Fragment;

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mShareMode:I

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 48
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mDialog:Landroid/app/DialogFragment;

    .line 283
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$1;-><init>(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressHandler:Landroid/os/Handler;

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mFragment:Landroid/app/Fragment;

    .line 52
    iput p2, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mShareMode:I

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mShareMode:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/Fragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mFragment:Landroid/app/Fragment;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/FileShareOperation;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mOpThread:Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->closeProgressDialog()V

    return-void
.end method

.method private declared-synchronized closeProgressDialog()V
    .locals 2

    .prologue
    .line 138
    monitor-enter p0

    :try_start_0
    const-string v0, "FileShareOperation"

    const-string v1, "DeleteFiles : closeProgressDialog"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mOpThread:Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mOpThread:Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mOpThread:Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->interrupt()V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 145
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressDialog:Landroid/app/ProgressDialog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    :cond_1
    monitor-exit p0

    return-void

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private doShare(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V
    .locals 6
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p3, "callback"    # Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v1, 0x0

    .line 115
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 118
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mProgressHandler:Landroid/os/Handler;

    move-object v1, p0

    move-object v2, p2

    move-object v4, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;-><init>(Lcom/sec/android/app/voicenote/common/util/FileShareOperation;Ljava/util/ArrayList;Landroid/os/Handler;Landroid/app/Fragment;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mOpThread:Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mOpThread:Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation$FileSharedThread;->start()V

    .line 121
    :cond_0
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/app/Fragment;I)Lcom/sec/android/app/voicenote/common/util/FileShareOperation;
    .locals 4
    .param p0, "fragment"    # Landroid/app/Fragment;
    .param p1, "shareMode"    # I

    .prologue
    .line 56
    const-class v2, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    monitor-enter v2

    if-nez p0, :cond_0

    .line 57
    const/4 v1, 0x0

    .line 71
    :goto_0
    monitor-exit v2

    return-object v1

    .line 59
    :cond_0
    :try_start_0
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mInstance:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mInstance:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    .line 60
    :cond_1
    new-instance v1, Ljava/lang/ref/WeakReference;

    new-instance v3, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;-><init>(Landroid/app/Fragment;I)V

    invoke-direct {v1, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mInstance:Ljava/lang/ref/WeakReference;

    .line 64
    :cond_2
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mInstance:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    .line 65
    .local v0, "fso":Lcom/sec/android/app/voicenote/common/util/FileShareOperation;
    iget-object v1, v0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v1, p0}, Landroid/app/Fragment;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 66
    iput-object p0, v0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mFragment:Landroid/app/Fragment;

    .line 69
    :cond_3
    iput p1, v0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mShareMode:I

    .line 71
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mInstance:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 56
    .end local v0    # "fso":Lcom/sec/android/app/voicenote/common/util/FileShareOperation;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private hasSTTFiles(Landroid/app/Activity;Ljava/util/ArrayList;)Z
    .locals 8
    .param p1, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 124
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 125
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 126
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p1, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkSTT(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 127
    .local v2, "sttfilename":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 128
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 129
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 130
    const/4 v3, 0x1

    .line 134
    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "sttfilename":Ljava/lang/String;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public askOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)Ljava/lang/ref/WeakReference;
    .locals 2
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p3, "callback"    # Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/DialogFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->askShare(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V

    .line 79
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mDialog:Landroid/app/DialogFragment;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public askShare(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V
    .locals 9
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p3, "callback"    # Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/16 v6, 0x64

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 84
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v6, :cond_0

    .line 85
    invoke-virtual {p1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v4, 0x7f0b00de

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p1, v4, v5}, Landroid/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v8}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 105
    :goto_0
    return-void

    .line 90
    :cond_0
    invoke-virtual {p1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->hasSTTFiles(Landroid/app/Activity;Ljava/util/ArrayList;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 91
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v8, :cond_1

    .line 92
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mDialog:Landroid/app/DialogFragment;

    .line 93
    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 94
    .local v2, "id":J
    invoke-virtual {p1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkSTT(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "filepath":Ljava/lang/String;
    const-string v1, "FileShareOperation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "filename : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mDialog:Landroid/app/DialogFragment;

    check-cast v1, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->SetParameter(JLjava/lang/String;)V

    .line 100
    .end local v0    # "filepath":Ljava/lang/String;
    .end local v2    # "id":J
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mDialog:Landroid/app/DialogFragment;

    invoke-virtual {v1, p1, v7}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mDialog:Landroid/app/DialogFragment;

    invoke-virtual {p1}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    const-string v5, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v4, v5}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    goto :goto_0

    .line 98
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNMultiShareViaSTTSelectDialogFragment;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mDialog:Landroid/app/DialogFragment;

    goto :goto_1

    .line 103
    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->startOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V

    goto :goto_0
.end method

.method public multipleSend([J)V
    .locals 12
    .param p1, "ids"    # [J

    .prologue
    .line 150
    if-nez p1, :cond_0

    .line 151
    const-string v8, "FileShareOperation"

    const-string v9, "multipleSend : ids is null!!"

    invoke-static {v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :goto_0
    return-void

    .line 155
    :cond_0
    new-instance v4, Landroid/content/Intent;

    const-string v8, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 156
    .local v4, "shareIntent":Landroid/content/Intent;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 157
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    const/4 v7, 0x0

    .line 158
    .local v7, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 159
    .local v6, "sttfilename":Ljava/lang/String;
    const-string v8, "audio/*"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    array-length v5, p1

    .line 162
    .local v5, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v5, :cond_4

    .line 163
    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v8}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    aget-wide v10, p1, v1

    invoke-static {v8, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getContentURI(Landroid/content/Context;J)Landroid/net/Uri;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 164
    iget v8, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mShareMode:I

    packed-switch v8, :pswitch_data_0

    .line 180
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    const-string v8, "audio/*"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 166
    :pswitch_0
    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v8}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    aget-wide v10, p1, v1

    invoke-static {v8, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkSTT(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 167
    if-eqz v6, :cond_2

    .line 168
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_2
    const-string v8, "application/txt"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 172
    :pswitch_1
    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v8}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    aget-wide v10, p1, v1

    invoke-static {v8, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkSTT(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 173
    if-eqz v6, :cond_3

    .line 174
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    :cond_3
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    const-string v8, "application/txt,audio/*"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 186
    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 187
    const-string v8, "FileShareOperation"

    const-string v9, "multipleSend : selected list is empty!!"

    invoke-static {v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 191
    :cond_5
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {v4, v8, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 193
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mFragment:Landroid/app/Fragment;

    const v9, 0x7f0b011f

    invoke-virtual {v8, v9}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 195
    .local v2, "intent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->mFragment:Landroid/app/Fragment;

    const/16 v9, 0x64

    invoke-virtual {v8, v2, v9}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 197
    .end local v2    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v8, "FileShareOperation"

    const-string v9, "multipleSend() - activity not found!"

    invoke-static {v8, v9, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 164
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public startOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V
    .locals 0
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p3, "callback"    # Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 110
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->doShare(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V

    .line 111
    return-void
.end method
