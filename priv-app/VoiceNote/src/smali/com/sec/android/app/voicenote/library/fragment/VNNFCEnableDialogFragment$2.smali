.class Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$2;
.super Ljava/lang/Object;
.source "VNNFCEnableDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->mCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    const-string v0, "show_nfc_enable"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->dismissAllowingStateLoss()V

    .line 99
    :cond_1
    new-instance v0, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;-><init>(Landroid/content/Context;Z)V

    new-array v1, v2, [Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/main/common/EnableNFCTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->startNFCWritingActivity()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNNFCEnableDialogFragment;)V

    .line 102
    return-void
.end method
