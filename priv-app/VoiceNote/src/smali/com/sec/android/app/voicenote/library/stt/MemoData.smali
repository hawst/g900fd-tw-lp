.class public Lcom/sec/android/app/voicenote/library/stt/MemoData;
.super Ljava/lang/Object;
.source "MemoData.java"


# static fields
.field private static mTextDataArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/MemoData;->mTextDataArrayList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/MemoData;->mContext:Landroid/content/Context;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/MemoData;->mTextDataArrayList:Ljava/util/ArrayList;

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/MemoData;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method static synthetic access$000()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/MemoData;->mTextDataArrayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/stt/MemoData;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/MemoData;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/MemoData;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static addSingleTextData(Lcom/sec/android/app/voicenote/common/util/TextData;)V
    .locals 1
    .param p0, "data"    # Lcom/sec/android/app/voicenote/common/util/TextData;

    .prologue
    .line 78
    if-nez p0, :cond_0

    .line 83
    :goto_0
    return-void

    .line 82
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/MemoData;->mTextDataArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getSttData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/MemoData;->mTextDataArrayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static setSttData(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "sttData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    sput-object p0, Lcom/sec/android/app/voicenote/library/stt/MemoData;->mTextDataArrayList:Ljava/util/ArrayList;

    .line 71
    return-void
.end method

.method public static sortCollections()V
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/MemoData;->mTextDataArrayList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 87
    return-void
.end method


# virtual methods
.method public hasSTTdata()Z
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/MemoData;->mTextDataArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initMemoData()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/MemoData;->mTextDataArrayList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 37
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/MemoData;->mTextDataArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 39
    :cond_0
    return-void
.end method

.method public saveMemoData(Ljava/lang/String;)V
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 46
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/voicenote/library/stt/MemoData$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/voicenote/library/stt/MemoData$1;-><init>(Lcom/sec/android/app/voicenote/library/stt/MemoData;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 67
    return-void
.end method
