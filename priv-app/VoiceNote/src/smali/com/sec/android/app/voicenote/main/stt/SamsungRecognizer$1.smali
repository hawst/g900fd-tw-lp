.class Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;
.super Ljava/lang/Object;
.source "SamsungRecognizer.java"

# interfaces
.implements Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->startRecognition()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;)V
    .locals 5
    .param p1, "error"    # Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  Ttype : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;->getTransactionError()Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  code : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;->getTransactionError()Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  text : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;->getTransactionError()Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getErrorText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  prompt : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;->getTransactionError()Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getPrompt()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  parameter : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;->getTransactionError()Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getParameterWithError()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  session : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;->getTransactionError()Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getSessionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    const-string v1, "SamsungRecognizer"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secI(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;->getTransactionError()Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getType()I

    move-result v1

    if-eq v1, v3, :cond_0

    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;->getTransactionError()Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getType()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;->getTransactionError()Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getType()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 162
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->getErrorWord(Ljava/lang/String;)Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->access$300(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;Ljava/lang/String;)Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->addSttData(Lcom/sec/android/app/voicenote/main/common/VNSTTData;Z)V

    .line 163
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->access$100(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 164
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->access$100(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;->onError(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;)V

    .line 173
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->access$200(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;->release()V

    .line 174
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;
    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->access$202(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;)Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    .line 175
    return-void

    .line 167
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    # invokes: Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->getEmptyWord()Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->access$400(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->addSttData(Lcom/sec/android/app/voicenote/main/common/VNSTTData;Z)V

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->access$100(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 169
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->access$100(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    move-result-object v1

    invoke-interface {v1, v4}, Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;->onResultWord(Lcom/sec/android/app/voicenote/main/common/VNSTTData;)V

    goto :goto_0
.end method

.method public onResult(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;)V
    .locals 3
    .param p1, "result"    # Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;

    .prologue
    const/4 v2, 0x0

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    # invokes: Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->getWord(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;)Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    invoke-static {v0, p1}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->access$000(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;)Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->addSttData(Lcom/sec/android/app/voicenote/main/common/VNSTTData;Z)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->access$100(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->access$100(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;->onResultWord(Lcom/sec/android/app/voicenote/main/common/VNSTTData;)V

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    # getter for: Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->access$200(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;)Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;->release()V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;

    # setter for: Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->_cloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;
    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;->access$202(Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;)Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    .line 146
    return-void
.end method
