.class public Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;
.super Ljava/lang/Object;
.source "AudioRecognizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$AppInfo;
    }
.end annotation


# static fields
.field static final DEFAULT_VAD_END_THRESHOLD:I = 0x1

.field public static final EMPTY_STRING:Ljava/lang/String; = "...."

.field private static final TAG:Ljava/lang/String;

.field public static mLastTimeStamp:J


# instance fields
.field private mAudioConverter:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

.field private mCloudRecognizer:Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

.field private mCloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

.field private mContext:Landroid/content/Context;

.field private mRecordingTime:J

.field private mSentenceMap:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSpeexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

.field private mTempPipe:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;

    .line 49
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mLastTimeStamp:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JLcom/sec/android/app/voicenote/library/stt/AudioSource;Ljava/util/TreeMap;Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "recordingStartTime"    # J
    .param p4, "audioSource"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource;
    .param p6, "audioConverter"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J",
            "Lcom/sec/android/app/voicenote/library/stt/AudioSource;",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/sec/android/app/voicenote/library/stt/AudioConverter;",
            ")V"
        }
    .end annotation

    .prologue
    .local p5, "orderedSentenceMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Long;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudRecognizer:Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mTempPipe:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mSpeexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    .line 46
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mRecordingTime:J

    .line 52
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mContext:Landroid/content/Context;

    .line 53
    iput-object p6, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mAudioConverter:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    .line 54
    iput-wide p2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mRecordingTime:J

    .line 56
    new-instance v0, Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;

    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$AppInfo;->getHostName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x1bb

    const-string v3, "Samsung_Android_VoiceNoteV2_20130911"

    sget-object v4, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$AppInfo;->AppKey:[B

    sget-object v5, Lcom/nuance/dragon/toolkit/audio/AudioType;->SPEEX_WB:Lcom/nuance/dragon/toolkit/audio/AudioType;

    sget-object v6, Lcom/nuance/dragon/toolkit/audio/AudioType;->SPEEX_WB:Lcom/nuance/dragon/toolkit/audio/AudioType;

    invoke-direct/range {v0 .. v6}, Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;-><init>(Ljava/lang/String;ILjava/lang/String;[BLcom/nuance/dragon/toolkit/audio/AudioType;Lcom/nuance/dragon/toolkit/audio/AudioType;)V

    .line 57
    .local v0, "config":Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;->createCloudServices(Landroid/content/Context;Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;)Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    .line 58
    new-instance v1, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    invoke-direct {v1, v2}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;-><init>(Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudRecognizer:Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

    .line 60
    new-instance v1, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    invoke-direct {v1}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mSpeexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mSpeexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    invoke-virtual {v1, p4}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 63
    new-instance v1, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;

    invoke-direct {v1, p2, p3}, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;-><init>(J)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mTempPipe:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;

    .line 64
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mTempPipe:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mSpeexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 66
    iput-object p5, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mSentenceMap:Ljava/util/TreeMap;

    .line 68
    sget-object v1, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "created recordingTime:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;
    .param p1, "x1"    # Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->addToMemoData(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;)Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;)Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;
    .param p1, "x1"    # Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;)Lcom/sec/android/app/voicenote/library/stt/AudioConverter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mAudioConverter:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->addEmptyMemoData()V

    return-void
.end method

.method private addEmptyMemoData()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 280
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/TextData;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/common/util/TextData;-><init>()V

    .line 281
    .local v0, "textData":Lcom/sec/android/app/voicenote/common/util/TextData;
    sget-wide v2, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mLastTimeStamp:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->timeStamp:J

    .line 282
    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mRecordingTime:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    .line 283
    iget-object v1, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "...."

    aput-object v3, v1, v2

    .line 284
    iget-wide v2, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->timeStamp:J

    sput-wide v2, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mLastTimeStamp:J

    .line 285
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/MemoData;->addSingleTextData(Lcom/sec/android/app/voicenote/common/util/TextData;)V

    .line 286
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/MemoData;->sortCollections()V

    .line 288
    sget-wide v2, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mLastTimeStamp:J

    const-string v1, "...."

    invoke-direct {p0, v2, v3, v1}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->saveToTextFile(JLjava/lang/String;)V

    .line 289
    return-void
.end method

.method private addToMemoData(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;)V
    .locals 18
    .param p1, "cloudResult"    # Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;

    .prologue
    .line 227
    const/4 v5, 0x0

    .line 228
    .local v5, "dicResult":Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;
    invoke-virtual/range {p1 .. p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;->getDictionary()Lcom/nuance/dragon/toolkit/data/Data$Dictionary;

    move-result-object v6

    .line 229
    .local v6, "dictaionResult":Lcom/nuance/dragon/toolkit/data/Data$Dictionary;
    const-string v14, "transcription"

    invoke-virtual {v6, v14}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->getBytes(Ljava/lang/String;)Lcom/nuance/dragon/toolkit/data/Data$Bytes;

    move-result-object v4

    .line 231
    .local v4, "buffer":Lcom/nuance/dragon/toolkit/data/Data$Bytes;
    if-nez v4, :cond_1

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 235
    :cond_1
    iget-object v14, v4, Lcom/nuance/dragon/toolkit/data/Data$Bytes;->value:[B

    invoke-static {v14}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResultManager;->createDictationResult([B)Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;

    move-result-object v5

    .line 237
    if-eqz v5, :cond_0

    .line 241
    const/4 v8, 0x0

    .local v8, "index":I
    :goto_1
    const/4 v14, 0x1

    if-ge v8, v14, :cond_4

    .line 242
    invoke-virtual {v5, v8}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;->sentenceAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;

    move-result-object v10

    .line 243
    .local v10, "sentence":Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;
    sget-object v14, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;

    invoke-interface {v10}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    invoke-interface {v10}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->size()I

    move-result v11

    .line 247
    .local v11, "sentenceSize":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    if-ge v7, v11, :cond_3

    .line 248
    invoke-interface {v10, v7}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->tokenAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Token;

    move-result-object v13

    .line 249
    .local v13, "token":Lcom/nuance/dragon/toolkit/recognition/dictation/Token;
    invoke-interface {v13}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getStartTime()J

    move-result-wide v14

    invoke-interface {v13}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getEndTime()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0, v1}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;->getAlternatives(JJ)Lcom/nuance/dragon/toolkit/recognition/dictation/Alternatives;

    move-result-object v3

    .line 251
    .local v3, "alternatives":Lcom/nuance/dragon/toolkit/recognition/dictation/Alternatives;
    new-instance v12, Lcom/sec/android/app/voicenote/common/util/TextData;

    invoke-direct {v12}, Lcom/sec/android/app/voicenote/common/util/TextData;-><init>()V

    .line 252
    .local v12, "textData":Lcom/sec/android/app/voicenote/common/util/TextData;
    invoke-interface {v13}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getConfidenceScore()D

    move-result-wide v14

    iput-wide v14, v12, Lcom/sec/android/app/voicenote/common/util/TextData;->ConfidenceScore:D

    .line 253
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mRecordingTime:J

    invoke-interface {v13}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getStartTime()J

    move-result-wide v16

    add-long v14, v14, v16

    iput-wide v14, v12, Lcom/sec/android/app/voicenote/common/util/TextData;->timeStamp:J

    .line 254
    iget-wide v14, v12, Lcom/sec/android/app/voicenote/common/util/TextData;->timeStamp:J

    iput-wide v14, v12, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    .line 255
    iget-wide v14, v12, Lcom/sec/android/app/voicenote/common/util/TextData;->timeStamp:J

    sput-wide v14, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mLastTimeStamp:J

    .line 257
    sget-object v14, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "token at "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " confidence:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v13}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getConfidenceScore()D

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " startTime:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v13}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getStartTime()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " endTime:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v13}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getEndTime()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " hasNoSpaceBeforeDirective:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v13}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->hasNoSpaceBeforeDirective()Z

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " hasNoSpaceAfterDirectivie:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v13}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->hasNoSpaceAfterDirective()Z

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    sget-object v14, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "mRecordingTime:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mRecordingTime:J

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " mLastTimeStamp:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-wide v16, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mLastTimeStamp:J

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    if-eqz v3, :cond_2

    .line 263
    invoke-interface {v3}, Lcom/nuance/dragon/toolkit/recognition/dictation/Alternatives;->size()I

    move-result v2

    .line 264
    .local v2, "alterSize":I
    const/4 v9, 0x0

    .local v9, "k":I
    :goto_3
    if-ge v9, v2, :cond_2

    .line 265
    iget-object v14, v12, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v3, v9}, Lcom/nuance/dragon/toolkit/recognition/dictation/Alternatives;->getAlternativeAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Alternative;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lcom/nuance/dragon/toolkit/recognition/dictation/Alternative;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v14, v9

    .line 266
    sget-object v14, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "textData mText["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "] :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v12, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v16, v0

    aget-object v16, v16, v9

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 270
    .end local v2    # "alterSize":I
    .end local v9    # "k":I
    :cond_2
    invoke-static {v12}, Lcom/sec/android/app/voicenote/library/stt/MemoData;->addSingleTextData(Lcom/sec/android/app/voicenote/common/util/TextData;)V

    .line 247
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    .line 272
    .end local v3    # "alternatives":Lcom/nuance/dragon/toolkit/recognition/dictation/Alternatives;
    .end local v12    # "textData":Lcom/sec/android/app/voicenote/common/util/TextData;
    .end local v13    # "token":Lcom/nuance/dragon/toolkit/recognition/dictation/Token;
    :cond_3
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/MemoData;->sortCollections()V

    .line 241
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 275
    .end local v7    # "i":I
    .end local v10    # "sentence":Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;
    .end local v11    # "sentenceSize":I
    :cond_4
    const/4 v14, 0x0

    invoke-virtual {v5, v14}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;->sentenceAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;

    move-result-object v10

    .line 276
    .restart local v10    # "sentence":Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;
    sget-wide v14, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mLastTimeStamp:J

    invoke-interface {v10}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v14, v15, v1}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->saveToTextFile(JLjava/lang/String;)V

    goto/16 :goto_0
.end method

.method private createRecogSpec()Lcom/nuance/dragon/toolkit/cloudservices/recognizer/RecogSpec;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 192
    new-instance v2, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;

    invoke-direct {v2}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;-><init>()V

    .line 193
    .local v2, "settings":Lcom/nuance/dragon/toolkit/data/Data$Dictionary;
    const-string v3, "dictation_type"

    const-string v4, "dictation"

    invoke-virtual {v2, v3, v4}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string v3, "dictation_language"

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/stt/Utils;->readLanguageSetting(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    new-instance v1, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/RecogSpec;

    const-string v3, "NVC_ASR_CMD"

    const-string v4, "AUDIO_INFO"

    invoke-direct {v1, v3, v2, v4}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/RecogSpec;-><init>(Ljava/lang/String;Lcom/nuance/dragon/toolkit/data/Data$Dictionary;Ljava/lang/String;)V

    .line 198
    .local v1, "retRecogSpec":Lcom/nuance/dragon/toolkit/cloudservices/recognizer/RecogSpec;
    new-instance v0, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;

    invoke-direct {v0}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;-><init>()V

    .line 199
    .local v0, "requestInfo":Lcom/nuance/dragon/toolkit/data/Data$Dictionary;
    const-string v3, "start"

    invoke-virtual {v0, v3, v5}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->put(Ljava/lang/String;I)V

    .line 200
    const-string v3, "end"

    invoke-virtual {v0, v3, v5}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->put(Ljava/lang/String;I)V

    .line 201
    const-string v3, "text"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const-string v3, "binary_results"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->put(Ljava/lang/String;I)V

    .line 204
    new-instance v3, Lcom/nuance/dragon/toolkit/cloudservices/DictionaryParam;

    const-string v4, "REQUEST_INFO"

    invoke-direct {v3, v4, v0}, Lcom/nuance/dragon/toolkit/cloudservices/DictionaryParam;-><init>(Ljava/lang/String;Lcom/nuance/dragon/toolkit/data/Data$Dictionary;)V

    invoke-virtual {v1, v3}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/RecogSpec;->addParam(Lcom/nuance/dragon/toolkit/cloudservices/DataParam;)V

    .line 206
    return-object v1
.end method

.method private saveToTextFile(JLjava/lang/String;)V
    .locals 3
    .param p1, "timeStamp"    # J
    .param p3, "sentence"    # Ljava/lang/String;

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mSentenceMap:Ljava/util/TreeMap;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mSentenceMap:Ljava/util/TreeMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    :cond_0
    return-void
.end method


# virtual methods
.method public displayPipeBuffer()V
    .locals 2

    .prologue
    .line 298
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mTempPipe:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    return-void
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;->release()V

    .line 217
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mSpeexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mSpeexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;->release()V

    .line 222
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mSpeexEncoder:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    .line 224
    :cond_1
    return-void
.end method

.method public getRecentTimeStamp()J
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mTempPipe:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->getRecentTimeStamp()J

    move-result-wide v0

    return-wide v0
.end method

.method public processResult()V
    .locals 2

    .prologue
    .line 76
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;

    const-string v1, "End of Recognition, processResult"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mTempPipe:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->disconnectAudioSource()Lcom/nuance/dragon/toolkit/audio/AudioSource;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudRecognizer:Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;->processResult()V

    .line 83
    :cond_0
    return-void
.end method

.method public startRecognition()V
    .locals 6

    .prologue
    .line 86
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mTempPipe:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;->getFirstChunkTimeStamp()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mRecordingTime:J

    .line 88
    .local v0, "firstTimeStamp":J
    sget-object v2, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "firstTimeStamp: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudRecognizer:Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->createRecogSpec()Lcom/nuance/dragon/toolkit/cloudservices/recognizer/RecogSpec;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mTempPipe:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;

    new-instance v5, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$1;-><init>(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;)V

    invoke-virtual {v2, v3, v4, v5}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;->startRecognition(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/RecogSpec;Lcom/nuance/dragon/toolkit/audio/AudioSource;Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer$Listener;)V

    .line 188
    return-void
.end method
