.class Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$1;
.super Landroid/os/Handler;
.source "VNSttForThirdActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$1;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$1;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->is_Resumed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 258
    :goto_0
    return-void

    .line 237
    :cond_0
    const/4 v0, 0x0

    .line 239
    .local v0, "frag":Landroid/app/DialogFragment;
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 257
    :goto_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 241
    :sswitch_0
    const-string v1, "VNSttForThirdActivity"

    const-string v2, "BroadcastMessage.LOW_BATTERY"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$1;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->setResultWithFinish(I)V

    goto :goto_1

    .line 245
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$1;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .end local v0    # "frag":Landroid/app/DialogFragment;
    check-cast v0, Landroid/app/DialogFragment;

    .line 247
    .restart local v0    # "frag":Landroid/app/DialogFragment;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/DialogFragment;

    if-eqz v1, :cond_1

    .line 248
    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 250
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$1;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->setResultWithFinish(I)V

    goto :goto_1

    .line 253
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$1;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->doRecord(Z)V

    goto :goto_1

    .line 239
    nop

    :sswitch_data_0
    .sparse-switch
        0xfd3 -> :sswitch_1
        0xff0 -> :sswitch_0
        0x30d4b -> :sswitch_2
    .end sparse-switch
.end method
