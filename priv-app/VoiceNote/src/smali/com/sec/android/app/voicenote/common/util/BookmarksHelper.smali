.class public Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
.super Ljava/lang/Object;
.source "BookmarksHelper.java"


# instance fields
.field private final STRING_CODING:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private final TEMP_NAME:Ljava/lang/String;

.field private bookLength:I

.field private bookmarksCount:I

.field private inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

.field private invalidInit:Z

.field final newBKMK:[B

.field final newBNUM:[B

.field final shortBook:[B


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V
    .locals 2
    .param p1, "inf"    # Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    .prologue
    const/16 v1, 0x8

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->invalidInit:Z

    .line 44
    const-string v0, "BookmarksHelper"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->TAG:Ljava/lang/String;

    .line 46
    const-string v0, "temp6546368.m4a"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->TEMP_NAME:Ljava/lang/String;

    .line 48
    const-string v0, "UTF-16BE"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->STRING_CODING:Ljava/lang/String;

    .line 55
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->newBKMK:[B

    .line 61
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->shortBook:[B

    .line 66
    new-array v0, v1, [B

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->newBNUM:[B

    .line 73
    if-eqz p1, :cond_0

    .line 74
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    .line 75
    iget-boolean v0, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->usedToWrite:Z

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->invalidInit:Z

    .line 76
    iget v0, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileBnumLength:I

    add-int/lit8 v0, v0, -0x8

    div-int/lit16 v0, v0, 0x264

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->bookmarksCount:I

    .line 77
    const/16 v0, 0x7798

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->bookLength:I

    .line 81
    :goto_0
    return-void

    .line 79
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->invalidInit:Z

    goto :goto_0

    .line 55
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x2t
        0x64t
        0x62t
        0x6bt
        0x6dt
        0x6bt
    .end array-data

    .line 61
    :array_1
    .array-data 1
        0x0t
        0x0t
        0x77t
        -0x68t
        0x62t
        0x6ft
        0x6ft
        0x6bt
    .end array-data

    .line 66
    :array_2
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x8t
        0x62t
        0x6et
        0x75t
        0x6dt
    .end array-data
.end method

.method private exportToFile(Ljava/lang/String;Ljava/util/List;)V
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 398
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    if-nez p1, :cond_1

    .line 435
    :cond_0
    :goto_0
    return-void

    .line 400
    :cond_1
    const/4 v7, 0x0

    const/16 v8, 0x2e

    invoke-virtual {p1, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 401
    .local v2, "export_filepath":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_bookmark.txt"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 403
    const/4 v5, 0x0

    .line 404
    .local v5, "os":Ljava/io/FileOutputStream;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 405
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->deleteOnExit()V

    .line 407
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 414
    .local v0, "builder":Ljava/lang/StringBuilder;
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 415
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 416
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .local v6, "os":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    :try_start_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    if-ge v4, v7, :cond_2

    .line 417
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 416
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 420
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 429
    :try_start_2
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-object v5, v6

    .line 431
    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 430
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v7

    move-object v5, v6

    .line 432
    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 421
    .end local v4    # "i":I
    :catch_1
    move-exception v1

    .line 423
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 429
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 430
    :catch_2
    move-exception v7

    goto/16 :goto_0

    .line 424
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v1

    .line 426
    .local v1, "e":Ljava/io/IOException;
    :goto_3
    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 429
    :try_start_6
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_0

    .line 430
    :catch_4
    move-exception v7

    goto/16 :goto_0

    .line 428
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 429
    :goto_4
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 431
    :goto_5
    throw v7

    .line 430
    :catch_5
    move-exception v8

    goto :goto_5

    .line 428
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v4    # "i":I
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 424
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v1

    move-object v5, v6

    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 421
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v1

    move-object v5, v6

    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method private strToByte(Ljava/lang/String;ILjava/lang/String;)[B
    .locals 6
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "length"    # I
    .param p3, "coding"    # Ljava/lang/String;

    .prologue
    .line 375
    new-array v2, p2, [B

    .line 378
    .local v2, "toRet":[B
    :try_start_0
    invoke-virtual {p1, p3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    .line 379
    .local v1, "output":[B
    array-length v3, v1

    if-le v3, p2, :cond_0

    .line 380
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v3, v2, v4, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 394
    .end local v1    # "output":[B
    :goto_0
    return-object v2

    .line 386
    .restart local v1    # "output":[B
    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    array-length v5, v1

    invoke-static {v1, v3, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 391
    .end local v1    # "output":[B
    :catch_0
    move-exception v0

    .line 392
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method private updateBnum(Ljava/nio/channels/FileChannel;)V
    .locals 4
    .param p1, "dst"    # Ljava/nio/channels/FileChannel;

    .prologue
    .line 364
    const/4 v2, 0x4

    :try_start_0
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 365
    .local v1, "length":Ljava/nio/ByteBuffer;
    iget v2, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->bookmarksCount:I

    mul-int/lit16 v2, v2, 0x264

    add-int/lit8 v2, v2, 0x8

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 366
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 367
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v2, v2, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->bnumPos:J

    invoke-virtual {p1, v2, v3}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 368
    invoke-virtual {p1, v1}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 372
    .end local v1    # "length":Ljava/nio/ByteBuffer;
    :goto_0
    return-void

    .line 369
    :catch_0
    move-exception v0

    .line 370
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getAllBookmarks()Ljava/util/List;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252
    const/4 v4, 0x0

    .line 253
    .local v4, "bookmarks":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    const/4 v14, 0x0

    .line 254
    .local v14, "readCount":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->invalidInit:Z

    move/from16 v20, v0

    if-eqz v20, :cond_0

    move-object/from16 v20, v4

    .line 307
    :goto_0
    return-object v20

    .line 257
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "bookmarks":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 259
    .restart local v4    # "bookmarks":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->path:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 260
    .local v12, "f":Ljava/io/File;
    const/4 v15, 0x0

    .line 262
    .local v15, "strIn":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v16, Ljava/io/FileInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v12}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    .end local v15    # "strIn":Ljava/io/FileInputStream;
    .local v16, "strIn":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v6

    .line 264
    .local v6, "channel":Ljava/nio/channels/FileChannel;
    const/16 v20, 0x4

    invoke-static/range {v20 .. v20}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 265
    .local v5, "buff":Ljava/nio/ByteBuffer;
    const/16 v20, 0x64

    invoke-static/range {v20 .. v20}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v19

    .line 266
    .local v19, "titleBuff":Ljava/nio/ByteBuffer;
    const/16 v20, 0x1f4

    invoke-static/range {v20 .. v20}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 267
    .local v9, "descBuff":Ljava/nio/ByteBuffer;
    const/16 v20, 0x1f4

    move/from16 v0, v20

    new-array v8, v0, [B

    .line 268
    .local v8, "descArr":[B
    const/16 v20, 0x64

    move/from16 v0, v20

    new-array v0, v0, [B

    move-object/from16 v18, v0

    .line 269
    .local v18, "titleArr":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->bnumPos:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x10

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    invoke-virtual {v6, v0, v1}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 271
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->bookmarksCount:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v13, v0, :cond_4

    .line 272
    invoke-virtual {v6, v5}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v14

    .line 273
    if-gez v14, :cond_1

    const/16 v20, 0x0

    .line 302
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 303
    :catch_0
    move-exception v21

    goto :goto_0

    .line 275
    :cond_1
    :try_start_3
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 276
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    .line 277
    .local v7, "data":I
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 278
    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v14

    .line 279
    if-gez v14, :cond_2

    const/16 v20, 0x0

    .line 302
    :try_start_4
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 303
    :catch_1
    move-exception v21

    goto/16 :goto_0

    .line 281
    :cond_2
    :try_start_5
    invoke-virtual/range {v19 .. v19}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 282
    invoke-virtual/range {v19 .. v19}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v18

    .line 284
    invoke-virtual {v6, v9}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_a
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v14

    .line 285
    if-gez v14, :cond_3

    const/16 v20, 0x0

    .line 302
    :try_start_6
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_0

    .line 303
    :catch_2
    move-exception v21

    goto/16 :goto_0

    .line 287
    :cond_3
    :try_start_7
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 288
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v8

    .line 289
    new-instance v17, Ljava/lang/String;

    const-string v20, "UTF-16BE"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 290
    .local v17, "title":Ljava/lang/String;
    new-instance v10, Ljava/lang/String;

    const-string v20, "UTF-16BE"

    move-object/from16 v0, v20

    invoke-direct {v10, v8, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 291
    .local v10, "description":Ljava/lang/String;
    new-instance v20, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v21

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x1

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v0, v7, v1, v2, v3}, Lcom/sec/android/app/voicenote/common/util/Bookmark;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, v20

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v20

    const-wide/16 v22, 0x8

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    invoke-virtual {v6, v0, v1}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 271
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    .line 295
    .end local v7    # "data":I
    .end local v10    # "description":Ljava/lang/String;
    .end local v17    # "title":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_a
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 302
    :try_start_8
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    move-object/from16 v15, v16

    .line 306
    .end local v5    # "buff":Ljava/nio/ByteBuffer;
    .end local v6    # "channel":Ljava/nio/channels/FileChannel;
    .end local v8    # "descArr":[B
    .end local v9    # "descBuff":Ljava/nio/ByteBuffer;
    .end local v13    # "i":I
    .end local v16    # "strIn":Ljava/io/FileInputStream;
    .end local v18    # "titleArr":[B
    .end local v19    # "titleBuff":Ljava/nio/ByteBuffer;
    .restart local v15    # "strIn":Ljava/io/FileInputStream;
    :goto_2
    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    move-object/from16 v20, v4

    .line 307
    goto/16 :goto_0

    .line 303
    .end local v15    # "strIn":Ljava/io/FileInputStream;
    .restart local v5    # "buff":Ljava/nio/ByteBuffer;
    .restart local v6    # "channel":Ljava/nio/channels/FileChannel;
    .restart local v8    # "descArr":[B
    .restart local v9    # "descBuff":Ljava/nio/ByteBuffer;
    .restart local v13    # "i":I
    .restart local v16    # "strIn":Ljava/io/FileInputStream;
    .restart local v18    # "titleArr":[B
    .restart local v19    # "titleBuff":Ljava/nio/ByteBuffer;
    :catch_3
    move-exception v20

    move-object/from16 v15, v16

    .line 304
    .end local v16    # "strIn":Ljava/io/FileInputStream;
    .restart local v15    # "strIn":Ljava/io/FileInputStream;
    goto :goto_2

    .line 296
    .end local v5    # "buff":Ljava/nio/ByteBuffer;
    .end local v6    # "channel":Ljava/nio/channels/FileChannel;
    .end local v8    # "descArr":[B
    .end local v9    # "descBuff":Ljava/nio/ByteBuffer;
    .end local v13    # "i":I
    .end local v18    # "titleArr":[B
    .end local v19    # "titleBuff":Ljava/nio/ByteBuffer;
    :catch_4
    move-exception v11

    .line 297
    .local v11, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_9
    invoke-virtual {v11}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 302
    :try_start_a
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    goto :goto_2

    .line 303
    :catch_5
    move-exception v20

    goto :goto_2

    .line 298
    .end local v11    # "e":Ljava/io/FileNotFoundException;
    :catch_6
    move-exception v11

    .line 299
    .local v11, "e":Ljava/io/IOException;
    :goto_4
    :try_start_b
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 302
    :try_start_c
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_7

    goto :goto_2

    .line 303
    :catch_7
    move-exception v20

    goto :goto_2

    .line 301
    .end local v11    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v20

    .line 302
    :goto_5
    :try_start_d
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_8

    .line 303
    :goto_6
    throw v20

    :catch_8
    move-exception v21

    goto :goto_6

    .line 301
    .end local v15    # "strIn":Ljava/io/FileInputStream;
    .restart local v16    # "strIn":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v20

    move-object/from16 v15, v16

    .end local v16    # "strIn":Ljava/io/FileInputStream;
    .restart local v15    # "strIn":Ljava/io/FileInputStream;
    goto :goto_5

    .line 298
    .end local v15    # "strIn":Ljava/io/FileInputStream;
    .restart local v16    # "strIn":Ljava/io/FileInputStream;
    :catch_9
    move-exception v11

    move-object/from16 v15, v16

    .end local v16    # "strIn":Ljava/io/FileInputStream;
    .restart local v15    # "strIn":Ljava/io/FileInputStream;
    goto :goto_4

    .line 296
    .end local v15    # "strIn":Ljava/io/FileInputStream;
    .restart local v16    # "strIn":Ljava/io/FileInputStream;
    :catch_a
    move-exception v11

    move-object/from16 v15, v16

    .end local v16    # "strIn":Ljava/io/FileInputStream;
    .restart local v15    # "strIn":Ljava/io/FileInputStream;
    goto :goto_3
.end method

.method public getBookmarksCount()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->bookmarksCount:I

    return v0
.end method

.method public overwriteBookmarks(Ljava/util/List;)V
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->invalidInit:Z

    if-eqz v4, :cond_1

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->usedToWrite:Z

    .line 92
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->newBKMK:[B

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 93
    .local v9, "bkmk":Ljava/nio/ByteBuffer;
    const/16 v4, 0x264

    invoke-virtual {v9, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 94
    const/4 v4, 0x4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v24

    .line 96
    .local v24, "time":Ljava/nio/ByteBuffer;
    const/16 v18, 0x0

    .line 97
    .local v18, "raf":Ljava/io/RandomAccessFile;
    const/16 v22, 0x0

    .line 98
    .local v22, "temp":Ljava/io/RandomAccessFile;
    const/4 v3, 0x0

    .line 99
    .local v3, "srcWrite":Ljava/nio/channels/FileChannel;
    const/4 v2, 0x0

    .line 105
    .local v2, "dst":Ljava/nio/channels/FileChannel;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-boolean v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasBookmarks:Z

    if-eqz v4, :cond_a

    .line 107
    new-instance v19, Ljava/io/RandomAccessFile;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-object v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->path:Ljava/lang/String;

    const-string v5, "rw"

    move-object/from16 v0, v19

    invoke-direct {v0, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 108
    .end local v18    # "raf":Ljava/io/RandomAccessFile;
    .local v19, "raf":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    .line 109
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->bnumPos:J

    const-wide/16 v6, 0x8

    add-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 112
    monitor-enter p1
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 113
    :try_start_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v20

    .line 114
    .local v20, "size":I
    const/16 v4, 0x32

    move/from16 v0, v20

    if-lt v0, v4, :cond_2

    .line 115
    const/16 v20, 0x32

    .line 116
    :cond_2
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    move/from16 v0, v20

    if-ge v14, v0, :cond_3

    .line 117
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 118
    invoke-virtual/range {v24 .. v24}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 119
    invoke-virtual {v3, v9}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 120
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 121
    invoke-virtual/range {v24 .. v24}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 122
    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 123
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getTitle()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-string v6, "UTF-16BE"

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->strToByte(Ljava/lang/String;ILjava/lang/String;)[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 124
    .local v12, "desc":Ljava/nio/ByteBuffer;
    invoke-virtual {v3, v12}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 125
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getDescription()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x1f4

    const-string v6, "UTF-16BE"

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->strToByte(Ljava/lang/String;ILjava/lang/String;)[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 126
    invoke-virtual {v3, v12}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 116
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 129
    .end local v12    # "desc":Ljava/nio/ByteBuffer;
    :cond_3
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->bookmarksCount:I

    .line 130
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 132
    const/16 v4, 0x32

    move/from16 v0, v20

    if-ge v0, v4, :cond_7

    .line 133
    rsub-int/lit8 v26, v20, 0x32

    .line 135
    .local v26, "writeEmpty":I
    const/4 v14, 0x0

    :goto_2
    move/from16 v0, v26

    if-ge v14, v0, :cond_7

    .line 136
    :try_start_3
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 137
    invoke-virtual/range {v24 .. v24}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 138
    invoke-virtual {v3, v9}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 139
    const/4 v4, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 140
    invoke-virtual/range {v24 .. v24}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 141
    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 142
    const-string v4, "empty title"

    const/16 v5, 0x64

    const-string v6, "UTF-16BE"

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->strToByte(Ljava/lang/String;ILjava/lang/String;)[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 143
    .restart local v12    # "desc":Ljava/nio/ByteBuffer;
    invoke-virtual {v3, v12}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 144
    const-string v4, "empty description"

    const/16 v5, 0x1f4

    const-string v6, "UTF-16BE"

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->strToByte(Ljava/lang/String;ILjava/lang/String;)[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 145
    invoke-virtual {v3, v12}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 135
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 130
    .end local v12    # "desc":Ljava/nio/ByteBuffer;
    .end local v14    # "i":I
    .end local v20    # "size":I
    .end local v26    # "writeEmpty":I
    :catchall_0
    move-exception v4

    :try_start_4
    monitor-exit p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v4
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 233
    :catch_0
    move-exception v13

    move-object/from16 v18, v19

    .line 234
    .end local v19    # "raf":Ljava/io/RandomAccessFile;
    .local v13, "e":Ljava/io/FileNotFoundException;
    .restart local v18    # "raf":Ljava/io/RandomAccessFile;
    :goto_3
    :try_start_6
    invoke-virtual {v13}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 239
    :try_start_7
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 240
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 241
    :cond_4
    if-eqz v18, :cond_5

    invoke-virtual/range {v18 .. v18}, Ljava/io/RandomAccessFile;->close()V

    .line 242
    :cond_5
    if-eqz v22, :cond_6

    invoke-virtual/range {v22 .. v22}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_9

    .line 246
    .end local v13    # "e":Ljava/io/FileNotFoundException;
    :cond_6
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-object v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->path:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->exportToFile(Ljava/lang/String;Ljava/util/List;)V

    .line 248
    const-string v4, "BookmarksHelper"

    const-string v5, "overwriteBookmarks has ended"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 148
    .end local v18    # "raf":Ljava/io/RandomAccessFile;
    .restart local v14    # "i":I
    .restart local v19    # "raf":Ljava/io/RandomAccessFile;
    .restart local v20    # "size":I
    :cond_7
    :try_start_8
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->updateBnum(Ljava/nio/channels/FileChannel;)V

    .line 149
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 150
    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->close()V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    move-object/from16 v18, v19

    .line 239
    .end local v19    # "raf":Ljava/io/RandomAccessFile;
    .end local v20    # "size":I
    .restart local v18    # "raf":Ljava/io/RandomAccessFile;
    :goto_5
    :try_start_9
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 240
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 241
    :cond_8
    if-eqz v18, :cond_9

    invoke-virtual/range {v18 .. v18}, Ljava/io/RandomAccessFile;->close()V

    .line 242
    :cond_9
    if-eqz v22, :cond_6

    invoke-virtual/range {v22 .. v22}, Ljava/io/RandomAccessFile;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    goto :goto_4

    .line 243
    :catch_1
    move-exception v4

    goto :goto_4

    .line 155
    .end local v14    # "i":I
    :cond_a
    :try_start_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasBookmarks:Z

    .line 156
    new-instance v19, Ljava/io/RandomAccessFile;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-object v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->path:Ljava/lang/String;

    const-string v5, "rw"

    move-object/from16 v0, v19

    invoke-direct {v0, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 157
    .end local v18    # "raf":Ljava/io/RandomAccessFile;
    .restart local v19    # "raf":Ljava/io/RandomAccessFile;
    :try_start_b
    new-instance v23, Ljava/io/RandomAccessFile;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "temp6546368.m4a"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "rw"

    move-object/from16 v0, v23

    invoke-direct {v0, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 159
    .end local v22    # "temp":Ljava/io/RandomAccessFile;
    .local v23, "temp":Ljava/io/RandomAccessFile;
    :try_start_c
    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    .line 160
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 162
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->udtaPos:J

    const-wide/16 v6, 0x8

    add-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 163
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v20

    .line 164
    .local v20, "size":J
    const-wide/16 v4, 0x0

    cmp-long v4, v20, v4

    if-nez v4, :cond_d

    .line 165
    const-string v4, "BookmarksHelper"

    const-string v5, "srcWrite.size() == 0 ! Aborting current operation"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 239
    :try_start_d
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 240
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 241
    :cond_b
    if-eqz v19, :cond_c

    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->close()V

    .line 242
    :cond_c
    if-eqz v23, :cond_0

    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2

    goto/16 :goto_0

    .line 243
    :catch_2
    move-exception v4

    goto/16 :goto_0

    .line 168
    :cond_d
    const-wide/16 v4, 0x0

    :try_start_e
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 169
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->udtaPos:J

    const-wide/16 v6, 0x8

    add-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 170
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->shortBook:[B

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 171
    .local v11, "book":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v6

    iput-wide v6, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->bookPos:J

    .line 172
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->bookLength:I

    invoke-virtual {v11, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 173
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 175
    invoke-virtual {v3, v11}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 176
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->newBNUM:[B

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v10

    .line 177
    .local v10, "bnum":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v6

    iput-wide v6, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->bnumPos:J

    .line 178
    invoke-virtual {v3, v10}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 181
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v16

    .line 183
    .local v16, "position":J
    const/4 v14, 0x0

    .restart local v14    # "i":I
    :goto_6
    const/16 v4, 0x32

    if-ge v14, v4, :cond_e

    .line 184
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 185
    invoke-virtual/range {v24 .. v24}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 186
    invoke-virtual {v3, v9}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 187
    const/4 v4, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 188
    invoke-virtual/range {v24 .. v24}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 189
    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 190
    const-string v4, "empty title"

    const/16 v5, 0x64

    const-string v6, "UTF-16BE"

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->strToByte(Ljava/lang/String;ILjava/lang/String;)[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 191
    .restart local v12    # "desc":Ljava/nio/ByteBuffer;
    invoke-virtual {v3, v12}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 192
    const-string v4, "empty description"

    const/16 v5, 0x1f4

    const-string v6, "UTF-16BE"

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->strToByte(Ljava/lang/String;ILjava/lang/String;)[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 193
    invoke-virtual {v3, v12}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 183
    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    .line 195
    .end local v12    # "desc":Ljava/nio/ByteBuffer;
    :cond_e
    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 201
    monitor-enter p1
    :try_end_e
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_e} :catch_3
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_8
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    .line 202
    :try_start_f
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v15

    .line 203
    .local v15, "listsize":I
    const/4 v14, 0x0

    :goto_7
    if-ge v14, v15, :cond_f

    .line 204
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 205
    invoke-virtual/range {v24 .. v24}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 206
    invoke-virtual {v3, v9}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 207
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 208
    invoke-virtual/range {v24 .. v24}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 209
    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 210
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getTitle()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-string v6, "UTF-16BE"

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->strToByte(Ljava/lang/String;ILjava/lang/String;)[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 211
    .restart local v12    # "desc":Ljava/nio/ByteBuffer;
    invoke-virtual {v3, v12}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 212
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getDescription()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x1f4

    const-string v6, "UTF-16BE"

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->strToByte(Ljava/lang/String;ILjava/lang/String;)[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 213
    invoke-virtual {v3, v12}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 203
    add-int/lit8 v14, v14, 0x1

    goto :goto_7

    .line 215
    .end local v12    # "desc":Ljava/nio/ByteBuffer;
    :cond_f
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->bookmarksCount:I

    .line 216
    monitor-exit p1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 218
    :try_start_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->bookPos:J

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->bookLength:I

    int-to-long v6, v6

    add-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 219
    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 220
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v5

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v7

    move-object v4, v2

    invoke-virtual/range {v3 .. v8}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 221
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->updateOuterAtomsLengths(Ljava/nio/channels/FileChannel;)V

    .line 222
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->updateBnum(Ljava/nio/channels/FileChannel;)V

    .line 223
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 225
    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->close()V

    .line 226
    invoke-virtual/range {v23 .. v23}, Ljava/io/RandomAccessFile;->close()V

    .line 228
    new-instance v25, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "temp6546368.m4a"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 230
    .local v25, "toDelete":Ljava/io/File;
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->delete()Z
    :try_end_10
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_10} :catch_3
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_8
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    move-object/from16 v22, v23

    .end local v23    # "temp":Ljava/io/RandomAccessFile;
    .restart local v22    # "temp":Ljava/io/RandomAccessFile;
    move-object/from16 v18, v19

    .end local v19    # "raf":Ljava/io/RandomAccessFile;
    .restart local v18    # "raf":Ljava/io/RandomAccessFile;
    goto/16 :goto_5

    .line 216
    .end local v15    # "listsize":I
    .end local v18    # "raf":Ljava/io/RandomAccessFile;
    .end local v22    # "temp":Ljava/io/RandomAccessFile;
    .end local v25    # "toDelete":Ljava/io/File;
    .restart local v19    # "raf":Ljava/io/RandomAccessFile;
    .restart local v23    # "temp":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v4

    :try_start_11
    monitor-exit p1
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    :try_start_12
    throw v4
    :try_end_12
    .catch Ljava/io/FileNotFoundException; {:try_start_12 .. :try_end_12} :catch_3
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_8
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    .line 233
    .end local v10    # "bnum":Ljava/nio/ByteBuffer;
    .end local v11    # "book":Ljava/nio/ByteBuffer;
    .end local v14    # "i":I
    .end local v16    # "position":J
    .end local v20    # "size":J
    :catch_3
    move-exception v13

    move-object/from16 v22, v23

    .end local v23    # "temp":Ljava/io/RandomAccessFile;
    .restart local v22    # "temp":Ljava/io/RandomAccessFile;
    move-object/from16 v18, v19

    .end local v19    # "raf":Ljava/io/RandomAccessFile;
    .restart local v18    # "raf":Ljava/io/RandomAccessFile;
    goto/16 :goto_3

    .line 235
    :catch_4
    move-exception v13

    .line 236
    .local v13, "e":Ljava/io/IOException;
    :goto_8
    :try_start_13
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    .line 239
    :try_start_14
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 240
    if-eqz v2, :cond_10

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 241
    :cond_10
    if-eqz v18, :cond_11

    invoke-virtual/range {v18 .. v18}, Ljava/io/RandomAccessFile;->close()V

    .line 242
    :cond_11
    if-eqz v22, :cond_6

    invoke-virtual/range {v22 .. v22}, Ljava/io/RandomAccessFile;->close()V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_5

    goto/16 :goto_4

    .line 243
    :catch_5
    move-exception v4

    goto/16 :goto_4

    .line 238
    .end local v13    # "e":Ljava/io/IOException;
    :catchall_2
    move-exception v4

    .line 239
    :goto_9
    :try_start_15
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 240
    if-eqz v2, :cond_12

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 241
    :cond_12
    if-eqz v18, :cond_13

    invoke-virtual/range {v18 .. v18}, Ljava/io/RandomAccessFile;->close()V

    .line 242
    :cond_13
    if-eqz v22, :cond_14

    invoke-virtual/range {v22 .. v22}, Ljava/io/RandomAccessFile;->close()V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_6

    .line 243
    :cond_14
    :goto_a
    throw v4

    :catch_6
    move-exception v5

    goto :goto_a

    .line 238
    .end local v18    # "raf":Ljava/io/RandomAccessFile;
    .restart local v19    # "raf":Ljava/io/RandomAccessFile;
    :catchall_3
    move-exception v4

    move-object/from16 v18, v19

    .end local v19    # "raf":Ljava/io/RandomAccessFile;
    .restart local v18    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_9

    .end local v18    # "raf":Ljava/io/RandomAccessFile;
    .end local v22    # "temp":Ljava/io/RandomAccessFile;
    .restart local v19    # "raf":Ljava/io/RandomAccessFile;
    .restart local v23    # "temp":Ljava/io/RandomAccessFile;
    :catchall_4
    move-exception v4

    move-object/from16 v22, v23

    .end local v23    # "temp":Ljava/io/RandomAccessFile;
    .restart local v22    # "temp":Ljava/io/RandomAccessFile;
    move-object/from16 v18, v19

    .end local v19    # "raf":Ljava/io/RandomAccessFile;
    .restart local v18    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_9

    .line 235
    .end local v18    # "raf":Ljava/io/RandomAccessFile;
    .restart local v19    # "raf":Ljava/io/RandomAccessFile;
    :catch_7
    move-exception v13

    move-object/from16 v18, v19

    .end local v19    # "raf":Ljava/io/RandomAccessFile;
    .restart local v18    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_8

    .end local v18    # "raf":Ljava/io/RandomAccessFile;
    .end local v22    # "temp":Ljava/io/RandomAccessFile;
    .restart local v19    # "raf":Ljava/io/RandomAccessFile;
    .restart local v23    # "temp":Ljava/io/RandomAccessFile;
    :catch_8
    move-exception v13

    move-object/from16 v22, v23

    .end local v23    # "temp":Ljava/io/RandomAccessFile;
    .restart local v22    # "temp":Ljava/io/RandomAccessFile;
    move-object/from16 v18, v19

    .end local v19    # "raf":Ljava/io/RandomAccessFile;
    .restart local v18    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_8

    .line 243
    .local v13, "e":Ljava/io/FileNotFoundException;
    :catch_9
    move-exception v4

    goto/16 :goto_4

    .line 233
    .end local v13    # "e":Ljava/io/FileNotFoundException;
    :catch_a
    move-exception v13

    goto/16 :goto_3
.end method

.method public removeBookmarks()V
    .locals 1

    .prologue
    .line 333
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 334
    .local v0, "emptyBookmarksList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->overwriteBookmarks(Ljava/util/List;)V

    .line 335
    return-void
.end method

.method public trimBookmarks(II)V
    .locals 6
    .param p1, "startTime"    # I
    .param p2, "endTime"    # I

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->getAllBookmarks()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 313
    .local v0, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    if-nez v0, :cond_0

    .line 330
    :goto_0
    return-void

    .line 317
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 318
    .local v4, "newBookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 321
    .local v1, "bookmarksIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 322
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 323
    .local v2, "checkedBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v3

    .line 324
    .local v3, "checkedBookmarkTime":I
    if-le v3, p1, :cond_1

    if-ge v3, p2, :cond_1

    .line 325
    sub-int v5, v3, p1

    invoke-virtual {v2, v5}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->setElapsed(I)V

    .line 326
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 329
    .end local v2    # "checkedBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    .end local v3    # "checkedBookmarkTime":I
    :cond_2
    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->overwriteBookmarks(Ljava/util/List;)V

    goto :goto_0
.end method

.method protected updateOuterAtomsLengths(Ljava/nio/channels/FileChannel;)V
    .locals 6
    .param p1, "dst"    # Ljava/nio/channels/FileChannel;

    .prologue
    .line 340
    const/4 v3, 0x4

    :try_start_0
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 341
    .local v0, "buff":Ljava/nio/ByteBuffer;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget v3, v3, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileMoovLength:I

    iget v4, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->bookLength:I

    add-int/2addr v3, v4

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 342
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 343
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v4, v3, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->moovPos:J

    invoke-virtual {p1, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 344
    invoke-virtual {p1, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 346
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 347
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget v3, v3, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileUdtaLength:I

    iget v4, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->bookLength:I

    add-int v2, v3, v4

    .line 348
    .local v2, "newUdtaLength":I
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 349
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 350
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v4, v3, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->udtaPos:J

    invoke-virtual {p1, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 351
    invoke-virtual {p1, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 352
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    .end local v0    # "buff":Ljava/nio/ByteBuffer;
    .end local v2    # "newUdtaLength":I
    :goto_0
    return-void

    .line 354
    :catch_0
    move-exception v1

    .line 355
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "BookmarksHelper"

    const-string v4, "updateOuterAtomsLengths - Some other exception"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
