.class public final Lcom/sec/android/app/voicenote/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final action_dropdown_disable_text_color:I = 0x7f080000

.field public static final action_dropdown_enable_text_color:I = 0x7f080001

.field public static final actionbar_dropdown_text_color_selector:I = 0x7f080062

.field public static final actionbar_progress_bg:I = 0x7f080002

.field public static final actionbar_stt_bottom_line:I = 0x7f080003

.field public static final bg_color:I = 0x7f080004

.field public static final black:I = 0x7f080005

.field public static final black_shadow:I = 0x7f080006

.field public static final button_normal:I = 0x7f080007

.field public static final button_press:I = 0x7f080008

.field public static final category_textcolor_selector:I = 0x7f080063

.field public static final checkbox_select_all_checked:I = 0x7f080009

.field public static final checkbox_select_all_unchecked:I = 0x7f08000a

.field public static final checked_bitmap_color_list:I = 0x7f080064

.field public static final cust_ab_transparent_light_holo:I = 0x7f08000b

.field public static final custom_button_color:I = 0x7f080065

.field public static final default_activity_background:I = 0x7f08000c

.field public static final dialog_checked:I = 0x7f08000d

.field public static final dialog_disabled:I = 0x7f08000e

.field public static final dialog_focus:I = 0x7f08000f

.field public static final dialog_normal:I = 0x7f080010

.field public static final dialog_press:I = 0x7f080011

.field public static final dialog_select:I = 0x7f080012

.field public static final dimblack:I = 0x7f080013

.field public static final dimtext:I = 0x7f080014

.field public static final disabled:I = 0x7f080015

.field public static final edittext_bg:I = 0x7f080016

.field public static final gray:I = 0x7f080017

.field public static final gridview_title_bigitem_normal:I = 0x7f080018

.field public static final gridview_title_smallitem_normal:I = 0x7f080019

.field public static final hold_the_back_of_your_device_over_the_tag_to_write:I = 0x7f08001a

.field public static final label_default_bg:I = 0x7f08001b

.field public static final list_item_normal_text:I = 0x7f08001c

.field public static final list_item_seconday_text:I = 0x7f08001d

.field public static final listview_date_disabled:I = 0x7f08001e

.field public static final listview_date_normal:I = 0x7f08001f

.field public static final listview_date_pressed:I = 0x7f080020

.field public static final listview_divider:I = 0x7f080021

.field public static final listview_duration_disabled:I = 0x7f080022

.field public static final listview_duration_normal:I = 0x7f080023

.field public static final listview_duration_pressed:I = 0x7f080024

.field public static final listview_item_date_color:I = 0x7f080066

.field public static final listview_item_title_color:I = 0x7f080067

.field public static final listview_select_all_normal:I = 0x7f080025

.field public static final listview_state_normal:I = 0x7f080026

.field public static final listview_state_pressed:I = 0x7f080027

.field public static final listview_title_disabled:I = 0x7f080028

.field public static final listview_title_highlight:I = 0x7f080029

.field public static final listview_title_normal:I = 0x7f08002a

.field public static final listview_title_pressed:I = 0x7f08002b

.field public static final listview_title_selected:I = 0x7f08002c

.field public static final logo_bg_color:I = 0x7f08002d

.field public static final logo_text_color:I = 0x7f08002e

.field public static final marinblue:I = 0x7f08002f

.field public static final material_shadow:I = 0x7f080030

.field public static final multishare_activity_name:I = 0x7f080031

.field public static final navigation_bar_color:I = 0x7f080032

.field public static final no_item:I = 0x7f080033

.field public static final no_recorded_file:I = 0x7f080034

.field public static final normal:I = 0x7f080035

.field public static final notification_divider:I = 0x7f080036

.field public static final panel_icon:I = 0x7f080037

.field public static final panel_text_color:I = 0x7f080038

.field public static final panel_text_stt_color:I = 0x7f080039

.field public static final popup_secondary:I = 0x7f08003a

.field public static final rec_control_btn_color_easy:I = 0x7f08003b

.field public static final recording_memo_tool_bar_line:I = 0x7f08003c

.field public static final recording_select_mode:I = 0x7f08003d

.field public static final recording_select_mode_devider:I = 0x7f08003e

.field public static final red:I = 0x7f08003f

.field public static final secondary:I = 0x7f080040

.field public static final select_mode_button:I = 0x7f080041

.field public static final select_mode_button_40:I = 0x7f080042

.field public static final select_mode_button_60:I = 0x7f080043

.field public static final select_mode_button_shadow:I = 0x7f080044

.field public static final select_mode_button_shadow_40:I = 0x7f080045

.field public static final skyblue:I = 0x7f080046

.field public static final split_line:I = 0x7f080047

.field public static final titletext:I = 0x7f080048

.field public static final toolbar_help_text:I = 0x7f080049

.field public static final toolbar_help_text_shadow:I = 0x7f08004a

.field public static final top_menu_item_text_color:I = 0x7f08004b

.field public static final twcolor1:I = 0x7f08004c

.field public static final twcolor140:I = 0x7f08004d

.field public static final twcolor141:I = 0x7f08004e

.field public static final twcolor151:I = 0x7f08004f

.field public static final twcolor2:I = 0x7f080050

.field public static final twcolor28:I = 0x7f080051

.field public static final twcolor29:I = 0x7f080052

.field public static final twcolor3:I = 0x7f080053

.field public static final twcolor30:I = 0x7f080054

.field public static final twcolor31:I = 0x7f080055

.field public static final twcolor5:I = 0x7f080056

.field public static final twcolor6:I = 0x7f080057

.field public static final unchecked_bitmap_color_list:I = 0x7f080068

.field public static final voice_note_in_bound_ripple_color:I = 0x7f080058

.field public static final voice_note_library_list_view_bg:I = 0x7f080059

.field public static final voice_note_progress_bg:I = 0x7f08005a

.field public static final voice_note_progress_thumb_point:I = 0x7f08005b

.field public static final voice_note_quick_panel_bg:I = 0x7f08005c

.field public static final voice_note_recording_memo_bg:I = 0x7f08005d

.field public static final voice_note_repeat_progress_thumb:I = 0x7f08005e

.field public static final voice_note_riffle_test:I = 0x7f08005f

.field public static final white:I = 0x7f080060

.field public static final yellowgreen:I = 0x7f080061


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
