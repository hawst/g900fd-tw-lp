.class Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$2;
.super Landroid/database/ContentObserver;
.source "VNLibraryExpandableListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->listBinding()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 587
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 5
    .param p1, "selfChange"    # Z

    .prologue
    const/16 v4, 0x15

    .line 590
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 591
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->requery()Z

    .line 593
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->refreshList()V

    .line 594
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v1

    if-eqz v1, :cond_1

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 603
    :cond_1
    :goto_0
    return-void

    .line 596
    :cond_2
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v1

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 597
    .local v0, "path":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExistFile(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 598
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCurrentPlayState()I

    move-result v1

    if-eq v1, v4, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 600
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$300()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onDoStop()V

    goto :goto_0
.end method
