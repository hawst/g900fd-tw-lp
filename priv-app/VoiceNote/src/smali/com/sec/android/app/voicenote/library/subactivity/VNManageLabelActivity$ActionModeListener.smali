.class Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;
.super Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;
.source "VNManageLabelActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionModeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;I)V
    .locals 0
    .param p2, "viewPosition"    # I

    .prologue
    .line 502
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    .line 503
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;-><init>(Landroid/content/Context;I)V

    .line 504
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;II)V
    .locals 0
    .param p2, "viewPosition"    # I
    .param p3, "mode"    # I

    .prologue
    .line 506
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    .line 507
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;-><init>(Landroid/content/Context;I)V

    .line 509
    iput p3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->mMenuMode:I

    .line 510
    return-void
.end method


# virtual methods
.method protected finishActionMode()V
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$100(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Landroid/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 673
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$100(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 675
    :cond_0
    return-void
.end method

.method protected getItemCount()I
    .locals 1

    .prologue
    .line 689
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->isCursorValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 690
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$600(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->getCount()I

    move-result v0

    .line 693
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 698
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->isCursorValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$600(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->getItemId(I)J

    move-result-wide v0

    .line 702
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getList()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$1000(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Landroid/widget/ListView;

    move-result-object v0

    return-object v0
.end method

.method protected getParentFragment()Landroid/app/Fragment;
    .locals 1

    .prologue
    .line 679
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isCursorValid()Z
    .locals 2

    .prologue
    .line 707
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$600(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 708
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 709
    :cond_0
    const/4 v1, 0x0

    .line 712
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 18
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 605
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v12

    .line 606
    .local v12, "listView":Landroid/widget/ListView;
    if-nez v12, :cond_0

    .line 607
    const-string v15, "VNLibraryActionModeListener"

    const-string v16, "onActionItemClicked() : list view is null"

    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    const/4 v15, 0x0

    .line 654
    :goto_0
    return v15

    .line 611
    :cond_0
    invoke-virtual {v12}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v2

    .line 612
    .local v2, "arr":Landroid/util/SparseBooleanArray;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 614
    .local v3, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v11

    .line 616
    .local v11, "len":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v11, :cond_2

    .line 617
    invoke-virtual {v2, v7}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v15

    invoke-virtual {v2, v15}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 618
    invoke-virtual {v2, v7}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->getItemId(I)J

    move-result-wide v8

    .line 619
    .local v8, "id":J
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 616
    .end local v8    # "id":J
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 622
    :cond_2
    const-wide/16 v4, -0x1

    .line 623
    .local v4, "checkedItemId":J
    invoke-virtual {v12}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_3

    .line 624
    const/4 v15, 0x0

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 625
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    long-to-int v0, v4

    move/from16 v16, v0

    # setter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I
    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$302(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;I)I

    .line 628
    :cond_3
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v10

    .line 629
    .local v10, "itemId":I
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/view/ActionMode;->setTag(Ljava/lang/Object;)V

    .line 631
    packed-switch v10, :pswitch_data_0

    .line 652
    const/4 v15, 0x0

    goto :goto_0

    .line 633
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v15, v0, v3, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->askOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)Ljava/lang/ref/WeakReference;

    .line 654
    :goto_2
    const/4 v15, 0x0

    goto :goto_0

    .line 637
    :pswitch_1
    const/4 v6, 0x0

    .line 638
    .local v6, "color":I
    const/4 v14, 0x0

    .line 640
    .local v14, "text":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;
    invoke-static {v15}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$400(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 641
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;
    invoke-static {v15}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$400(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$300(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelColor(I)Ljava/lang/Integer;

    move-result-object v13

    .line 642
    .local v13, "ret":Ljava/lang/Integer;
    if-eqz v13, :cond_4

    .line 643
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 645
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;
    invoke-static {v15}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$400(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$300(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelTitle(I)Ljava/lang/String;

    move-result-object v14

    .line 646
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;
    invoke-static {v15}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$400(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    .line 648
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # invokes: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->startActivityForResult(ILjava/lang/String;)V
    invoke-static {v15, v6, v14}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$900(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;ILjava/lang/String;)V

    goto :goto_2

    .line 631
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e00f6
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 4
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v1, 0x0

    .line 537
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->mMode:Landroid/view/ActionMode;

    .line 540
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 542
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v0

    .line 543
    .local v0, "listView":Landroid/widget/ListView;
    if-nez v0, :cond_0

    .line 544
    const-string v2, "VNLibraryActionModeListener"

    const-string v3, "onCreateActionMode() : list view is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    :goto_0
    return v1

    .line 548
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$400(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 549
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # invokes: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->listBinding()Z
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$500(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Z

    .line 550
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$400(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    .line 562
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->mMode:Landroid/view/ActionMode;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->setAllViews(Landroid/view/ActionMode;Landroid/app/Activity;)V

    .line 564
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 4
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 520
    invoke-super {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$102(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # setter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->isActionMode:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$202(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;Z)Z

    .line 523
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    const/4 v1, -0x1

    # setter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mcurrentID:I
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$302(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;I)I

    .line 524
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 526
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->isCursorValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$400(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 528
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # invokes: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->listBinding()Z
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$500(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Z

    .line 529
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$400(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    .line 530
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$600(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->setSelectionMode(ZZ)V

    .line 532
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->invalidateOptionsMenu()V

    .line 533
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 8
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const v7, 0x7f0e00f7

    const v6, 0x7f0e00f6

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 569
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v0

    .line 570
    .local v0, "checkedItemCount":I
    if-nez v0, :cond_3

    .line 571
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # setter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mNothingCheckedItem:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$702(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;Z)Z

    .line 575
    :goto_0
    if-ne v0, v4, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->getMenuMode()I

    move-result v3

    if-ne v3, v4, :cond_4

    .line 576
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # setter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mRenameMenuEnable:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$802(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;Z)Z

    .line 580
    :goto_1
    invoke-interface {p2}, Landroid/view/Menu;->clear()V

    .line 581
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 582
    .local v2, "inflater":Landroid/view/MenuInflater;
    const v3, 0x7f0d0002

    invoke-virtual {v2, v3, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 583
    invoke-interface {p2, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 584
    .local v1, "del":Landroid/view/MenuItem;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->getMenuMode()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 585
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 586
    const v3, 0x7f0b0070

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 588
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mNothingCheckedItem:Z
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$700(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 589
    invoke-interface {p2, v6}, Landroid/view/Menu;->removeItem(I)V

    .line 590
    invoke-interface {p2, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 592
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mRenameMenuEnable:Z
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$800(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 593
    invoke-interface {p2, v6}, Landroid/view/Menu;->removeItem(I)V

    .line 600
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v3

    return v3

    .line 573
    .end local v1    # "del":Landroid/view/MenuItem;
    .end local v2    # "inflater":Landroid/view/MenuInflater;
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # setter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mNothingCheckedItem:Z
    invoke-static {v3, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$702(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;Z)Z

    goto :goto_0

    .line 578
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # setter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mRenameMenuEnable:Z
    invoke-static {v3, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$802(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;Z)Z

    goto :goto_1
.end method

.method protected refreashActionMode()V
    .locals 3

    .prologue
    .line 659
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$100(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Landroid/view/ActionMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 660
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v0

    .line 661
    .local v0, "checkItemCount":I
    if-eqz v0, :cond_1

    .line 662
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mNothingCheckedItem:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$702(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;Z)Z

    .line 666
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$100(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ActionMode;->invalidate()V

    .line 668
    .end local v0    # "checkItemCount":I
    :cond_0
    return-void

    .line 664
    .restart local v0    # "checkItemCount":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->mNothingCheckedItem:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;->access$702(Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity;Z)Z

    goto :goto_0
.end method

.method setMenuMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 514
    iput p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNManageLabelActivity$ActionModeListener;->mMenuMode:I

    .line 515
    return-void
.end method
