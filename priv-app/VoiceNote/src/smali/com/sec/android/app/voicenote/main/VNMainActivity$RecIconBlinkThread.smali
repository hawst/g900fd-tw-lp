.class Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;
.super Ljava/lang/Thread;
.source "VNMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecIconBlinkThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 2750
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;Lcom/sec/android/app/voicenote/main/VNMainActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/main/VNMainActivity;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/main/VNMainActivity$1;

    .prologue
    .line 2750
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;-><init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/16 v4, 0x837

    .line 2755
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$800(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2770
    :cond_0
    :goto_1
    return-void

    .line 2759
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x836

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 2760
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x836

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 2761
    const-wide/16 v2, 0x708

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 2763
    :catch_0
    move-exception v1

    .line 2764
    .local v1, "ne":Ljava/lang/NullPointerException;
    const-string v2, "VoiceNoteMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ignore Rec icon blink message after destroyed : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2765
    .end local v1    # "ne":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 2766
    .local v0, "ie":Ljava/lang/InterruptedException;
    const-string v2, "VoiceNoteMainActivity"

    const-string v3, "RecIconBlinkThread InterruptedException"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2767
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2768
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$RecIconBlinkThread;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-static {v2, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1
.end method
