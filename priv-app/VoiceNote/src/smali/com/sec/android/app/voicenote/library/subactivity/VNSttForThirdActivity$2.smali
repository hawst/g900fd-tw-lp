.class Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$2;
.super Ljava/lang/Object;
.source "VNSttForThirdActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "iBinder"    # Landroid/os/IBinder;

    .prologue
    .line 264
    const-string v1, "VerificationLog"

    const-string v2, "Excuted"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->noI(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string v1, "VNSttForThirdActivity"

    const-string v2, "onServiceConnected()"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-static {p2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v2

    # setter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$002(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    .line 270
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->is_Resumed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 271
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$000(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIServiceCallback:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$100(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->registerCallback(Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;)V

    .line 272
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mRegistedIService:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$202(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;Z)Z

    .line 275
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # invokes: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getMediaRecorderState()I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$300(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)I

    move-result v1

    const/16 v2, 0x3ec

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # invokes: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->getMediaRecorderState()I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$300(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)I

    move-result v1

    const/16 v2, 0x3eb

    if-ne v1, v2, :cond_2

    .line 277
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$000(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;->saveRecording()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->invalidateOptionsMenu()V

    .line 287
    return-void

    .line 279
    :cond_2
    :try_start_1
    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 282
    :catch_0
    move-exception v0

    .line 283
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VNSttForThirdActivity"

    const-string v2, "onServiceConnected error"

    invoke-static {v1, v2, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 284
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 291
    const-string v0, "VNSttForThirdActivity"

    const-string v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$002(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    .line 293
    return-void
.end method
