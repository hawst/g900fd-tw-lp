.class public Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNDetailDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;,
        Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsListAdapter;,
        Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$Callbacks;
    }
.end annotation


# static fields
.field private static final DEFAULT_LOCALE:Ljava/util/Locale;

.field private static final TAG:Ljava/lang/String; = "VNDetailDialogFragment"


# instance fields
.field private mCursor:Landroid/database/Cursor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 53
    new-instance v0, Ljava/util/Locale;

    const-string v1, "en"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->DEFAULT_LOCALE:Ljava/util/Locale;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 59
    return-void
.end method

.method private getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "date"    # J

    .prologue
    .line 187
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 188
    .local v1, "shortDateFormat":Ljava/text/DateFormat;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 190
    .local v0, "dateString":Ljava/lang/StringBuffer;
    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 192
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getFileLocation(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x2f

    const/4 v5, 0x0

    .line 252
    const/4 v3, 0x1

    invoke-virtual {p1, v4, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 253
    .local v2, "startIndex":I
    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 254
    .local v0, "endIndex":I
    const-string v1, ""

    .line 256
    .local v1, "fileLocation":Ljava/lang/String;
    if-ltz v2, :cond_0

    if-ltz v0, :cond_0

    .line 257
    invoke-virtual {p1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/mnt"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 258
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 263
    :cond_0
    :goto_0
    return-object v1

    .line 260
    :cond_1
    invoke-virtual {p1, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getShortSize(J)Ljava/lang/String;
    .locals 9
    .param p1, "number"    # J

    .prologue
    const/high16 v8, 0x44800000    # 1024.0f

    .line 223
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v3, "0.#"

    invoke-direct {v0, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 224
    .local v0, "df":Ljava/text/DecimalFormat;
    long-to-float v1, p1

    .line 226
    .local v1, "size":F
    float-to-double v4, v1

    const-wide v6, 0x412999999999999aL    # 838860.8

    cmpg-double v3, v4, v6

    if-gez v3, :cond_0

    .line 227
    div-float/2addr v1, v8

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b009a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 236
    .local v2, "unit":Ljava/lang/String;
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    float-to-double v4, v1

    invoke-virtual {v0, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 229
    .end local v2    # "unit":Ljava/lang/String;
    :cond_0
    float-to-double v4, v1

    const-wide v6, 0x41c999999999999aL    # 8.589934592E8

    cmpg-double v3, v4, v6

    if-gez v3, :cond_1

    .line 230
    div-float v3, v1, v8

    div-float v1, v3, v8

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00ad

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "unit":Ljava/lang/String;
    goto :goto_0

    .line 233
    .end local v2    # "unit":Ljava/lang/String;
    :cond_1
    div-float v3, v1, v8

    div-float/2addr v3, v8

    div-float v1, v3, v8

    .line 234
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b008d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "unit":Ljava/lang/String;
    goto :goto_0
.end method

.method private getSizeFormat(J)Ljava/lang/String;
    .locals 7
    .param p1, "size"    # J

    .prologue
    .line 240
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->DEFAULT_LOCALE:Ljava/util/Locale;

    invoke-static {v3}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    .line 241
    .local v1, "nf":Ljava/text/NumberFormat;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getShortSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 242
    .local v2, "strBuf":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0020

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 243
    .local v0, "bytes":Ljava/lang/String;
    const-wide/16 v4, 0x1

    cmp-long v3, p1, v4

    if-lez v3, :cond_0

    .line 245
    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v1, p1, p2}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const/16 v4, 0x29

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 248
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static setArguments(JI)Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;
    .locals 4
    .param p0, "id"    # J
    .param p2, "title"    # I

    .prologue
    .line 62
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;-><init>()V

    .line 63
    .local v1, "dialogFragment":Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 64
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "id"

    invoke-virtual {v0, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 65
    const-string v2, "title"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 67
    return-object v1
.end method

.method public static stringForLocationTag(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "tag"    # Ljava/lang/String;

    .prologue
    .line 196
    move-object v4, p0

    .line 197
    .local v4, "result":Ljava/lang/String;
    const-string v5, "TAG"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Location = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    if-eqz v4, :cond_2

    .line 199
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mConstraintLetter:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 200
    .local v1, "cl":Ljava/lang/String;
    const-string v5, " "

    if-ne v1, v5, :cond_0

    .line 201
    const-string v5, ""

    invoke-virtual {v4, v1, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 199
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 203
    :cond_0
    const-string v5, " "

    invoke-virtual {v4, v1, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 206
    .end local v1    # "cl":Ljava/lang/String;
    :cond_1
    const-string v5, "  "

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 207
    const-string v5, "  "

    const-string v6, " "

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 210
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_2
    return-object v4
.end method

.method public static stringForReadDate(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # J

    .prologue
    const-wide/16 v4, 0x3e8

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 215
    .local v0, "result":Ljava/lang/StringBuilder;
    const-string v1, "yyyy/MM/dd "

    mul-long v2, p1, v4

    invoke-static {v1, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    mul-long/2addr v4, p1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private stringForTime(J)Ljava/lang/String;
    .locals 13
    .param p1, "timeMs"    # J

    .prologue
    const-wide/16 v10, 0x3c

    .line 267
    const-wide/16 v8, 0x3e8

    div-long v6, p1, v8

    .line 268
    .local v6, "totalSeconds":J
    rem-long v4, v6, v10

    .line 269
    .local v4, "seconds":J
    div-long v8, v6, v10

    rem-long v2, v8, v10

    .line 270
    .local v2, "minutes":J
    const-wide/16 v8, 0xe10

    div-long v0, v6, v8

    .line 272
    .local v0, "hours":J
    const-string v8, "%02d:%02d:%02d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 29
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v16

    .line 77
    .local v16, "currentFileId":J
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "title"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 79
    .local v28, "title":Ljava/lang/String;
    new-instance v14, Landroid/app/AlertDialog$Builder;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v14, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 80
    .local v14, "alertDialog":Landroid/app/AlertDialog$Builder;
    move-object/from16 v0, v28

    invoke-virtual {v14, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 82
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_0

    .line 83
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 84
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 87
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 88
    .local v7, "selection":Ljava/lang/String;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .local v15, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;>;"
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 92
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v4, :cond_6

    .line 93
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 94
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    const-string v6, "_data"

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 95
    .local v26, "path":Ljava/lang/String;
    new-instance v22, Ljava/io/File;

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 96
    .local v22, "file":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    const-string v6, "duration"

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 97
    .local v18, "duration":J
    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;

    const v5, 0x7f0b00c8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    const-string v6, "date_modified"

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 99
    .local v24, "lastModified":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    const-string v6, "addr"

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 100
    .local v10, "Address":Ljava/lang/String;
    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;

    const v5, 0x7f0b009d

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-wide/16 v8, 0x3e8

    mul-long v8, v8, v24

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v8, v9}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    move-wide/from16 v0, v24

    invoke-static {v8, v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->stringForReadDate(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;

    const v5, 0x7f0b00e6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getFileLocation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->stringForTime(J)Ljava/lang/String;

    move-result-object v27

    .line 105
    .local v27, "time":Ljava/lang/String;
    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;

    const v5, 0x7f0b0072

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-static {v6, v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->stringForReadTime(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-direct {v4, v5, v0, v6}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;

    const v5, 0x7f0b0120

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->length()J

    move-result-wide v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getSizeFormat(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    invoke-static {v10}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->stringForLocationTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 108
    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;

    const v5, 0x7f0b00a2

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v10}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->stringForLocationTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    .end local v10    # "Address":Ljava/lang/String;
    .end local v18    # "duration":J
    .end local v22    # "file":Ljava/io/File;
    .end local v24    # "lastModified":J
    .end local v26    # "path":Ljava/lang/String;
    .end local v27    # "time":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 130
    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    if-nez v4, :cond_3

    .line 131
    const-string v4, "VNDetailDialogFragment"

    const-string v5, "listBinding() : cursor null"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v12

    .line 136
    .local v12, "activity":Landroid/app/Activity;
    if-eqz v12, :cond_4

    .line 137
    new-instance v11, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsListAdapter;

    invoke-direct {v11, v12, v15}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 138
    .local v11, "DetailsListAdapter":Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsListAdapter;
    const/4 v4, 0x0

    invoke-virtual {v14, v11, v4}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    new-instance v6, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$1;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 148
    .end local v11    # "DetailsListAdapter":Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsListAdapter;
    :cond_4
    invoke-virtual {v14}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    .line 149
    .local v13, "ad":Landroid/app/AlertDialog;
    invoke-virtual {v13}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v23

    .line 150
    .local v23, "listview":Landroid/view/View;
    if-eqz v23, :cond_5

    .line 151
    const/4 v4, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 154
    :cond_5
    return-object v13

    .line 114
    .end local v12    # "activity":Landroid/app/Activity;
    .end local v13    # "ad":Landroid/app/AlertDialog;
    .end local v23    # "listview":Landroid/view/View;
    :cond_6
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->finish()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 116
    :catch_0
    move-exception v20

    .line 117
    .local v20, "e":Landroid/database/sqlite/SQLiteException;
    const-string v4, "VNDetailDialogFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SQLiteException :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_2

    .line 119
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 120
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    goto :goto_0

    .line 122
    .end local v20    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v21

    .line 123
    .local v21, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v4, "VNDetailDialogFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UnsupportedOperationException :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_2

    .line 125
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 126
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->mCursor:Landroid/database/Cursor;

    goto/16 :goto_0
.end method
