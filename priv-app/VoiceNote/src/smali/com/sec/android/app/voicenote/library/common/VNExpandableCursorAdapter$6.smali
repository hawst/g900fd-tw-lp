.class Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$6;
.super Ljava/lang/Object;
.source "VNExpandableCursorAdapter.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->startCheckBoxAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)V
    .locals 0

    .prologue
    .line 1165
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$6;->this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 11
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 1170
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 1172
    .local v6, "value":Ljava/lang/Integer;
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$6;->this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    monitor-enter v8

    .line 1173
    :try_start_0
    # getter for: Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->access$400()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1174
    .local v5, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v5, :cond_3

    .line 1175
    # getter for: Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->access$400()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1176
    .local v0, "checkbox":Landroid/widget/CheckBox;
    if-eqz v0, :cond_1

    .line 1177
    const/4 v3, 0x0

    .line 1178
    .local v3, "marginLeft":I
    const/4 v4, 0x0

    .line 1180
    .local v4, "marginRight":I
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$6;->this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mIsSelectionMode:Z
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->access$500(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1181
    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-static {v7, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v0, v7, v9}, Landroid/widget/CheckBox;->measure(II)V

    .line 1184
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v9

    sub-int v4, v7, v9

    .line 1190
    :goto_1
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 1191
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-nez v4, :cond_0

    .line 1192
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$6;->this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->access$100(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)Landroid/content/Context;

    move-result-object v7

    const v9, 0x418b3333    # 17.4f

    const/4 v10, 0x0

    invoke-static {v7, v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->convertDPtoPX(Landroid/content/Context;FLandroid/util/DisplayMetrics;)I

    move-result v3

    .line 1194
    :cond_0
    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual {v2, v3, v7, v4, v9}, Landroid/widget/LinearLayout$LayoutParams;->setMarginsRelative(IIII)V

    .line 1195
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1174
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "marginLeft":I
    .end local v4    # "marginRight":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1186
    .restart local v3    # "marginLeft":I
    .restart local v4    # "marginRight":I
    :cond_2
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v7

    mul-int/lit8 v4, v7, -0x1

    .line 1187
    const-string v7, "VNExpandableCursorAdapter"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onAnimationUpdate() : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1198
    .end local v0    # "checkbox":Landroid/widget/CheckBox;
    .end local v1    # "i":I
    .end local v3    # "marginLeft":I
    .end local v4    # "marginRight":I
    .end local v5    # "size":I
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .restart local v1    # "i":I
    .restart local v5    # "size":I
    :cond_3
    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1199
    return-void
.end method
