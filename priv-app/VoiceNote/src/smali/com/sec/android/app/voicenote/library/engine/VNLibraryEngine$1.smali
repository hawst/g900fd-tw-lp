.class Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;
.super Landroid/os/Handler;
.source "VNLibraryEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;)V
    .locals 0

    .prologue
    .line 879
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    .line 883
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 907
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 908
    return-void

    .line 885
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->switchSkipSilenceMode(ZLandroid/os/Bundle;)V

    goto :goto_0

    .line 888
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    # getter for: Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->access$100(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->isNextItem()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getSttData()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isSTTMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 889
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->switchSTT()V

    goto :goto_0

    .line 893
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    # getter for: Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->access$100(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->isPreviousItem()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->getSttData()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->isSTTMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 894
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->switchSTT()V

    goto :goto_0

    .line 898
    :pswitch_4
    const-string v1, "initial_player_access"

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;Z)Z

    move-result v0

    .line 899
    .local v0, "isInitialPlayerAccess":Z
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    # getter for: Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->access$200(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    # getter for: Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->access$200(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExploreByTouchEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    # getter for: Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->access$200(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEnablePenHoverInforPreview(Landroid/content/ContentResolver;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 901
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$1;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->doPause()V

    goto/16 :goto_0

    .line 883
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
