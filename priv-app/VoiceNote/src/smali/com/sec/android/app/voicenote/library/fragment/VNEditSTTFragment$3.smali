.class Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$3;
.super Ljava/lang/Object;
.source "VNEditSTTFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 142
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 152
    :goto_0
    return v0

    .line 145
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsMoving:Z
    invoke-static {v2, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$502(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Z)Z

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$600(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_1
    :goto_1
    move v0, v1

    .line 152
    goto :goto_0

    .line 148
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$600(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$600(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1
.end method
