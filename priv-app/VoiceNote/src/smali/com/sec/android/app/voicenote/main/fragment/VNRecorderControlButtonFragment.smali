.class public Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;
.super Landroid/app/Fragment;
.source "VNRecorderControlButtonFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment$Callbacks;
    }
.end annotation


# instance fields
.field private close_btn:Landroid/widget/ImageView;

.field private mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment$Callbacks;

.field private mEnable:Z

.field private mIsMMSMode:Z

.field private mList_btn:Landroid/view/View;

.field private mRecord_btn:Landroid/view/View;

.field private mResource:I

.field private mSetting_mode_btn:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 39
    const v0, 0x7f030044

    iput v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mResource:I

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment$Callbacks;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mIsMMSMode:Z

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mEnable:Z

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mRecord_btn:Landroid/view/View;

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->close_btn:Landroid/widget/ImageView;

    .line 49
    return-void
.end method

.method public static newInstance(IZZ)Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;
    .locals 3
    .param p0, "resource"    # I
    .param p1, "mmsmode"    # Z
    .param p2, "enable"    # Z

    .prologue
    .line 52
    new-instance v1, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;-><init>()V

    .line 53
    .local v1, "f":Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 54
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "resource"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 55
    const-string v2, "mmsmode"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 56
    const-string v2, "enable"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 57
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->setArguments(Landroid/os/Bundle;)V

    .line 58
    return-object v1
.end method


# virtual methods
.method public drawControlButtonforVoiceMemo()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mRecord_btn:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mRecord_btn:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mRecord_btn:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 274
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 277
    :cond_2
    return-void
.end method

.method public getResource()I
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mResource:I

    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 248
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment$Callbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment$Callbacks;

    .line 249
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 250
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment$Callbacks;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment$Callbacks;->onControlButtonSelected(I)Z

    move-result v0

    .line 237
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    if-ne p1, v1, :cond_0

    if-eqz v0, :cond_0

    .line 238
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mRecord_btn:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 244
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 68
    .local v2, "args":Landroid/os/Bundle;
    if-eqz v2, :cond_0

    .line 69
    const-string v9, "resource"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mResource:I

    .line 70
    const-string v9, "mmsmode"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mIsMMSMode:Z

    .line 71
    const-string v9, "enable"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mEnable:Z

    .line 74
    :cond_0
    iget v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mResource:I

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 76
    .local v8, "view":Landroid/view/View;
    const v9, 0x7f0e00c5

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    .line 77
    const v9, 0x7f0e00c7

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mRecord_btn:Landroid/view/View;

    .line 78
    const v9, 0x7f0e00c9

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    .line 80
    const v9, 0x7f0e00ca

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->close_btn:Landroid/widget/ImageView;

    .line 81
    const v9, 0x7f0e00ce

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 82
    .local v5, "pause_bth":Landroid/widget/ImageView;
    const v9, 0x7f0e00cc

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 83
    .local v6, "resume_bth":Landroid/widget/ImageView;
    const v9, 0x7f0e00cb

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 84
    .local v7, "stop_btn":Landroid/widget/ImageView;
    const v9, 0x7f0e00cd

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 86
    .local v3, "bookmark_btn":Landroid/widget/ImageView;
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    if-eqz v9, :cond_1

    .line 87
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    invoke-virtual {v9, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mEnable:Z

    invoke-virtual {v9, v10}, Landroid/view/View;->setEnabled(Z)V

    .line 89
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mEnable:Z

    invoke-virtual {v9, v10}, Landroid/view/View;->setFocusable(Z)V

    .line 91
    :cond_1
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mRecord_btn:Landroid/view/View;

    if-eqz v9, :cond_2

    .line 92
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mRecord_btn:Landroid/view/View;

    invoke-virtual {v9, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mRecord_btn:Landroid/view/View;

    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mEnable:Z

    invoke-virtual {v9, v10}, Landroid/view/View;->setEnabled(Z)V

    .line 94
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mRecord_btn:Landroid/view/View;

    iget-boolean v10, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mEnable:Z

    invoke-virtual {v9, v10}, Landroid/view/View;->setFocusable(Z)V

    .line 96
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->close_btn:Landroid/widget/ImageView;

    if-eqz v9, :cond_3

    .line 97
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->close_btn:Landroid/widget/ImageView;

    invoke-virtual {v9, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    :cond_3
    if-eqz v5, :cond_4

    .line 100
    invoke-virtual {v5, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    invoke-virtual {v5}, Landroid/widget/ImageView;->requestFocus()Z

    .line 103
    :cond_4
    if-eqz v6, :cond_5

    .line 104
    invoke-virtual {v6, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    invoke-virtual {v6}, Landroid/widget/ImageView;->requestFocus()Z

    .line 107
    :cond_5
    if-eqz v7, :cond_6

    .line 108
    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    :cond_6
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    if-eqz v9, :cond_a

    .line 112
    sget-boolean v9, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_BEAMFORMING:Z

    if-nez v9, :cond_7

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITH_SVOICE(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_7

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITHOUT_SVOICE()Z

    move-result v9

    if-eqz v9, :cond_8

    .line 115
    :cond_7
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 118
    :cond_8
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v9

    if-nez v9, :cond_9

    iget-boolean v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mIsMMSMode:Z

    if-eqz v9, :cond_18

    .line 119
    :cond_9
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 120
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    const v10, 0x3ecccccd    # 0.4f

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 125
    :goto_0
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 126
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    invoke-virtual {v9, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 130
    .local v1, "activity":Landroid/app/Activity;
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_b

    const-string v9, "android.provider.MediaStore.RECORD_SOUND"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 133
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    if-eqz v9, :cond_b

    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 146
    :cond_b
    if-eqz v3, :cond_d

    .line 147
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v9

    if-nez v9, :cond_c

    iget-boolean v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mIsMMSMode:Z

    if-eqz v9, :cond_19

    .line 148
    :cond_c
    const/4 v9, 0x4

    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 154
    :cond_d
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isHoveringUI(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_15

    .line 155
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mRecord_btn:Landroid/view/View;

    if-eqz v9, :cond_e

    .line 156
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mRecord_btn:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    .line 157
    .local v4, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v4, :cond_e

    .line 158
    const/16 v9, 0x3035

    invoke-virtual {v4, v9}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 163
    .end local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_e
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    if-eqz v9, :cond_f

    .line 164
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    .line 165
    .restart local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v4, :cond_f

    .line 166
    const/16 v9, 0x3035

    invoke-virtual {v4, v9}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 171
    .end local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_f
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    if-eqz v9, :cond_10

    .line 172
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    .line 173
    .restart local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v4, :cond_10

    .line 174
    const/16 v9, 0x3033

    invoke-virtual {v4, v9}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 184
    .end local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_10
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->close_btn:Landroid/widget/ImageView;

    if-eqz v9, :cond_11

    .line 185
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->close_btn:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    .line 186
    .restart local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v4, :cond_11

    .line 187
    const/16 v9, 0x3031

    invoke-virtual {v4, v9}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 191
    .end local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_11
    if-eqz v5, :cond_12

    .line 192
    invoke-virtual {v5}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    .line 193
    .restart local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v4, :cond_12

    .line 194
    const/16 v9, 0x3031

    invoke-virtual {v4, v9}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 198
    .end local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_12
    if-eqz v6, :cond_13

    .line 199
    invoke-virtual {v6}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    .line 200
    .restart local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v4, :cond_13

    .line 201
    const/16 v9, 0x3031

    invoke-virtual {v4, v9}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 205
    .end local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_13
    if-eqz v7, :cond_14

    .line 206
    invoke-virtual {v7}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    .line 207
    .restart local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v4, :cond_14

    .line 208
    const/16 v9, 0x3031

    invoke-virtual {v4, v9}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 212
    .end local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_14
    if-eqz v3, :cond_15

    .line 213
    invoke-virtual {v3}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    .line 214
    .restart local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v4, :cond_15

    .line 215
    const/16 v9, 0x3031

    invoke-virtual {v4, v9}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 221
    .end local v4    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_15
    sget-boolean v9, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_CHECK_ATTVN_ENABLED:Z

    if-eqz v9, :cond_17

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "Source"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_17

    const-string v9, "ATT_VVM"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "Source"

    invoke-virtual {v10, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_17

    .line 223
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    if-eqz v9, :cond_16

    .line 224
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 226
    :cond_16
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    if-eqz v9, :cond_17

    .line 227
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 231
    :cond_17
    return-object v8

    .line 122
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "activity":Landroid/app/Activity;
    :cond_18
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 123
    iget-object v9, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setAlpha(F)V

    goto/16 :goto_0

    .line 150
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "activity":Landroid/app/Activity;
    :cond_19
    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 151
    invoke-virtual {v3, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 258
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mCallbacks:Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment$Callbacks;

    .line 259
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mList_btn:Landroid/view/View;

    .line 260
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mRecord_btn:Landroid/view/View;

    .line 261
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->mSetting_mode_btn:Landroid/widget/ImageView;

    .line 262
    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNRecorderControlButtonFragment;->close_btn:Landroid/widget/ImageView;

    .line 264
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 265
    return-void
.end method
