.class public Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;
.super Landroid/view/animation/Animation;
.source "VNLeverMeterView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field delay:I

.field mFromX:F

.field mToX:F

.field mView:Landroid/view/View;


# direct methods
.method public constructor <init>(IILandroid/view/View;)V
    .locals 1
    .param p1, "fromX"    # I
    .param p2, "toX"    # I
    .param p3, "v"    # Landroid/view/View;

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->delay:I

    .line 36
    iput-object p3, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->mView:Landroid/view/View;

    .line 37
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->mFromX:F

    .line 38
    int-to-float v0, p2

    iput v0, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->mToX:F

    .line 40
    invoke-virtual {p0, p0}, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 41
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 4
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    .line 56
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 57
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    iget v1, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->mFromX:F

    iget v2, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->mToX:F

    iget v3, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->mFromX:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 58
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 59
    return-void
.end method

.method public checkDelay()Z
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->delay:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->delay:I

    rem-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->delay:I

    .line 51
    iget v0, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->delay:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 63
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 64
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    iget v1, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->mToX:F

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 70
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 74
    return-void
.end method

.method public resetAnim(IILandroid/view/View;)V
    .locals 1
    .param p1, "fromX"    # I
    .param p2, "toX"    # I
    .param p3, "v"    # Landroid/view/View;

    .prologue
    .line 44
    iput-object p3, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->mView:Landroid/view/View;

    .line 45
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->mFromX:F

    .line 46
    int-to-float v0, p2

    iput v0, p0, Lcom/sec/android/app/voicenote/main/common/VNLeverMeterView;->mToX:F

    .line 47
    return-void
.end method
