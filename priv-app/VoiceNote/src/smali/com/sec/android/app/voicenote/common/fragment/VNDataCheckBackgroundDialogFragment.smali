.class public Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNDataCheckBackgroundDialogFragment.java"


# instance fields
.field private key:Ljava/lang/String;

.field private mCheckbox:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->key:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->key:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;
    .locals 3
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 26
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;-><init>()V

    .line 27
    .local v1, "frag":Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 28
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "key_mode"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 30
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "key_mode"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->key:Ljava/lang/String;

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f03005f

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 41
    .local v2, "layout":Landroid/view/View;
    const v4, 0x7f0e00c3

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 43
    .local v3, "warningText":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 44
    .local v0, "a":Landroid/app/Activity;
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0b0021

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0b00e0

    new-instance v6, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 69
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 70
    const v4, 0x7f0b0053

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 71
    const v4, 0x7f0b0052

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 72
    const v4, 0x7f0e00ef

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 73
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 74
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    new-instance v5, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;)V

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckBackgroundDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    const v1, 0x7f0b006f

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 90
    :cond_0
    return-void
.end method
