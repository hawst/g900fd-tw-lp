.class public Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNShareViaSTTSelectDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment$ShareViaType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VNShareViaSTT"

.field private static stttextname:Ljava/lang/String;


# instance fields
.field private currentFileId:J

.field private mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field private mListView:Landroid/widget/ListView;

.field private mViewChild:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->stttextname:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->mListView:Landroid/widget/ListView;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->mViewChild:Landroid/view/View;

    .line 43
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->currentFileId:J

    .line 57
    return-void
.end method

.method private initView()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->mViewChild:Landroid/view/View;

    const v1, 0x7f0e0036

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->mListView:Landroid/widget/ListView;

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 100
    return-void
.end method

.method private listBinding()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 104
    const/4 v2, 0x4

    new-array v1, v2, [Ljava/lang/String;

    .line 106
    .local v1, "items":[Ljava/lang/String;
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b016c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0145

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 108
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0141

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 109
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b016f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 111
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f030023

    invoke-direct {v0, v2, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 113
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 114
    return v5
.end method


# virtual methods
.method public SetParameter(JLjava/lang/String;)V
    .locals 7
    .param p1, "id"    # J
    .param p3, "filename"    # Ljava/lang/String;

    .prologue
    .line 90
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 91
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "currentFileId"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 92
    const-string v1, "stttextname"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 94
    const-string v1, "VNShareViaSTT"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[HIYA]SetParameter : id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->currentFileId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / stttextname = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->stttextname:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 85
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->listBinding()Z

    .line 87
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 64
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 65
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 70
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 71
    const-string v2, "currentFileId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->currentFileId:J

    .line 72
    const-string v2, "stttextname"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->stttextname:Ljava/lang/String;

    .line 74
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 75
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0b011a

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030016

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->mViewChild:Landroid/view/View;

    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->initView()V

    .line 78
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->mViewChild:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 79
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-wide/16 v4, 0x0

    .line 120
    const-string v0, "VNShareViaSTT"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[HIYA]stttextname : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->stttextname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    cmp-long v0, p4, v4

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->stttextname:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->dismiss()V

    .line 140
    :goto_0
    return-void

    .line 127
    :cond_0
    cmp-long v0, p4, v4

    if-nez v0, :cond_3

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->currentFileId:J

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->singleSend(Landroid/content/Context;J)V

    .line 137
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v0, :cond_2

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onUpdateState(Z)V

    .line 139
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->dismiss()V

    goto :goto_0

    .line 129
    :cond_3
    const-wide/16 v0, 0x1

    cmp-long v0, p4, v0

    if-nez v0, :cond_4

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->stttextname:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->singleSendText_File(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 131
    :cond_4
    const-wide/16 v0, 0x2

    cmp-long v0, p4, v0

    if-nez v0, :cond_5

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->stttextname:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->singleSendText_String(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 133
    :cond_5
    const-wide/16 v0, 0x3

    cmp-long v0, p4, v0

    if-nez v0, :cond_1

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->currentFileId:J

    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->stttextname:Ljava/lang/String;

    invoke-static {v0, v2, v3, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->singleSendVT(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_1
.end method
