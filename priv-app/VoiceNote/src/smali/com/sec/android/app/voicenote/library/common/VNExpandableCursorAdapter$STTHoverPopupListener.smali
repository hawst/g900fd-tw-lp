.class Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$STTHoverPopupListener;
.super Ljava/lang/Object;
.source "VNExpandableCursorAdapter.java"

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "STTHoverPopupListener"
.end annotation


# instance fields
.field mSttList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 929
    .local p2, "sttList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$STTHoverPopupListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 927
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$STTHoverPopupListener;->mSttList:Ljava/util/ArrayList;

    .line 930
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$STTHoverPopupListener;->mSttList:Ljava/util/ArrayList;

    .line 931
    return-void
.end method


# virtual methods
.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "popup"    # Landroid/widget/HoverPopupWindow;

    .prologue
    const/4 v7, 0x0

    .line 935
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$STTHoverPopupListener;->this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mInflater:Landroid/view/LayoutInflater;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->access$200(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f03001f

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 938
    .local v1, "fullText2":Landroid/widget/TextView;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 939
    .local v3, "ssb":Ljava/lang/StringBuilder;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$STTHoverPopupListener;->mSttList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 940
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 941
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$STTHoverPopupListener;->mSttList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget v4, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    if-nez v4, :cond_0

    .line 942
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$STTHoverPopupListener;->mSttList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v4, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    aget-object v4, v4, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 940
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 945
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 946
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 947
    invoke-virtual {p2, v1}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 948
    const/4 v4, 0x1

    return v4
.end method
