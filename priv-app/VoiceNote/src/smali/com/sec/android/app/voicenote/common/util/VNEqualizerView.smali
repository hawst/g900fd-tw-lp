.class public Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;
.super Landroid/widget/ImageView;
.source "VNEqualizerView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VNEqualizerView"


# instance fields
.field maskLeftMargin:I

.field micMask:Landroid/graphics/Bitmap;

.field micPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->maskLeftMargin:I

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->maskLeftMargin:I

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->maskLeftMargin:I

    .line 28
    return-void
.end method

.method private createBaseBitmap()V
    .locals 5

    .prologue
    .line 119
    monitor-enter p0

    .line 120
    :try_start_0
    const-string v3, "VNEqualizerView"

    const-string v4, "createBaseBitmap()"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->micMask:Landroid/graphics/Bitmap;

    if-nez v3, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090026

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 123
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090027

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 124
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02007d

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 126
    .local v1, "mask":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int v3, v2, v3

    div-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->maskLeftMargin:I

    .line 127
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    const/4 v4, 0x1

    invoke-static {v1, v3, v0, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->micMask:Landroid/graphics/Bitmap;

    .line 128
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 131
    .end local v0    # "height":I
    .end local v1    # "mask":Landroid/graphics/Bitmap;
    .end local v2    # "width":I
    :cond_0
    monitor-exit p0

    .line 132
    return-void

    .line 131
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private getCroppedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "bmp"    # Landroid/graphics/Bitmap;
    .param p2, "viewHeight"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 68
    monitor-enter p0

    .line 69
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->getVisibility()I

    move-result v8

    if-eqz v8, :cond_0

    .line 70
    monitor-exit p0

    move-object v5, v7

    .line 103
    :goto_0
    return-object v5

    .line 72
    :cond_0
    const/4 v3, 0x0

    .line 73
    .local v3, "orgBmpHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->getWidth()I

    move-result v6

    .line 75
    .local v6, "viewWidth":I
    if-nez p1, :cond_1

    .line 76
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v5, v7

    goto :goto_0

    .line 79
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->createBaseBitmap()V

    .line 81
    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->micMask:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v8, :cond_2

    .line 82
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v5, v7

    goto :goto_0

    .line 85
    :cond_2
    :try_start_3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 87
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, p2, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 88
    .local v5, "result":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 89
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, v5}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 90
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 91
    .local v4, "paint":Landroid/graphics/Paint;
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 93
    sub-int v2, p2, v3

    .line 95
    .local v2, "margin":I
    const/4 v8, 0x0

    int-to-float v9, v2

    invoke-virtual {v0, p1, v8, v9, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 96
    new-instance v8, Landroid/graphics/PorterDuffXfermode;

    sget-object v9, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v8, v9}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 97
    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->micMask:Landroid/graphics/Bitmap;

    iget v9, p0, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->maskLeftMargin:I

    int-to-float v9, v9

    const/4 v10, 0x0

    invoke-virtual {v0, v8, v9, v10, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 98
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->recycleBaseBitmap()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 100
    :try_start_4
    monitor-exit p0

    goto :goto_0

    .line 105
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v2    # "margin":I
    .end local v3    # "orgBmpHeight":I
    .end local v4    # "paint":Landroid/graphics/Paint;
    .end local v5    # "result":Landroid/graphics/Bitmap;
    .end local v6    # "viewWidth":I
    :catchall_0
    move-exception v7

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v7

    .line 101
    .restart local v3    # "orgBmpHeight":I
    .restart local v6    # "viewWidth":I
    :catch_0
    move-exception v1

    .line 102
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_5
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->recycleBaseBitmap()V

    .line 103
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v5, v7

    goto :goto_0
.end method

.method private recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x0

    .line 109
    if-nez p1, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-object v1

    .line 112
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 42
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v2, :cond_1

    .line 65
    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    :goto_0
    return-void

    .line 46
    .restart local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->getWidth()I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->getHeight()I

    move-result v6

    if-eqz v6, :cond_0

    .line 49
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 50
    .local v0, "b":Landroid/graphics/Bitmap;
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 52
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->getHeight()I

    move-result v5

    .line 55
    .local v5, "h":I
    :try_start_0
    invoke-direct {p0, v1, v5}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->getCroppedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 56
    .local v4, "equalizerBitmap":Landroid/graphics/Bitmap;
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {p1, v4, v6, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 57
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 58
    const/4 v1, 0x0

    .line 59
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 62
    .end local v4    # "equalizerBitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v3

    .line 63
    .local v3, "e":Ljava/lang/NullPointerException;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->recycleBaseBitmap()V

    goto :goto_0
.end method

.method public recycleBaseBitmap()V
    .locals 2

    .prologue
    .line 135
    monitor-enter p0

    .line 136
    :try_start_0
    const-string v0, "VNEqualizerView"

    const-string v1, "recycleBaseBitmap()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->micMask:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNEqualizerView;->micMask:Landroid/graphics/Bitmap;

    .line 138
    monitor-exit p0

    .line 139
    return-void

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
