.class public Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "VNSingleSelectSimpleCursorAdapter.java"

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private li:Landroid/view/LayoutInflater;

.field private mClearAll:Z

.field private mContextChild:Landroid/content/Context;

.field private mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

.field private mFromVVM:Z

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLayout:I

.field private mPlayingId:J

.field private mPositionID:J

.field private mRadioClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[IILandroid/view/View$OnClickListener;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "from"    # [Ljava/lang/String;
    .param p5, "to"    # [I
    .param p6, "flagRegisterContentObserver"    # I
    .param p7, "radioClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 66
    invoke-direct/range {p0 .. p5}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    .line 47
    const-string v0, "VNSingleSelectSimpleCursorAdapter"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->TAG:Ljava/lang/String;

    .line 49
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->li:Landroid/view/LayoutInflater;

    .line 50
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mContextChild:Landroid/content/Context;

    .line 51
    iput v1, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mLayout:I

    .line 52
    iput-wide v4, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mPositionID:J

    .line 53
    iput-wide v4, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mPlayingId:J

    .line 55
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mClearAll:Z

    .line 58
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mFromVVM:Z

    .line 61
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 67
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mContextChild:Landroid/content/Context;

    .line 68
    iput p2, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mLayout:I

    .line 69
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->LoadLabeltoMap()V

    .line 71
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 72
    iput-object p7, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mRadioClickListener:Landroid/view/View$OnClickListener;

    .line 73
    return-void
.end method

.method private getDateFormatByFormatSetting(J)Ljava/lang/String;
    .locals 5
    .param p1, "date"    # J

    .prologue
    .line 273
    const-string v0, ""

    .line 274
    .local v0, "dateString":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mContextChild:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 275
    .local v1, "shortDateFormat":Ljava/text/DateFormat;
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 277
    return-object v0
.end method

.method private stringForTime(J)Ljava/lang/String;
    .locals 13
    .param p1, "timeMs"    # J

    .prologue
    .line 260
    const-wide/16 v8, 0x3e8

    div-long v6, p1, v8

    .line 261
    .local v6, "totalSeconds":J
    const-wide/16 v8, 0x3c

    rem-long v4, v6, v8

    .line 262
    .local v4, "seconds":J
    const-wide/16 v8, 0x3c

    div-long v8, v6, v8

    const-wide/16 v10, 0x3c

    rem-long v2, v8, v10

    .line 263
    .local v2, "minutes":J
    const-wide/16 v8, 0xe10

    div-long v0, v6, v8

    .line 265
    .local v0, "hours":J
    const-wide/16 v8, 0x0

    cmp-long v8, v0, v8

    if-lez v8, :cond_0

    .line 266
    const-string v8, "%d:%02d:%02d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 268
    :goto_0
    return-object v8

    :cond_0
    const-string v8, "%02d:%02d"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 239
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 24
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 108
    const/4 v9, 0x0

    .line 110
    .local v9, "holder":Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;
    if-nez p2, :cond_1

    .line 111
    new-instance v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;

    .end local v9    # "holder":Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;
    invoke-direct {v9}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;-><init>()V

    .line 112
    .restart local v9    # "holder":Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mLayout:I

    move/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 114
    const v20, 0x7f0e0067

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    move-object/from16 v0, v20

    iput-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    .line 115
    const v20, 0x7f0e0068

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    move-object/from16 v0, v20

    iput-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->dateText:Landroid/widget/TextView;

    .line 116
    const v20, 0x7f0e0069

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    move-object/from16 v0, v20

    iput-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->mmsText:Landroid/widget/TextView;

    .line 117
    const v20, 0x7f0e006a

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/ImageView;

    move-object/from16 v0, v20

    iput-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    .line 119
    const v20, 0x7f0e006d

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/ImageView;

    move-object/from16 v0, v20

    iput-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->nfcicon:Landroid/widget/ImageView;

    .line 120
    const v20, 0x7f0e0073

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/RadioButton;

    move-object/from16 v0, v20

    iput-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->radiobutton:Landroid/widget/RadioButton;

    .line 121
    const v20, 0x7f0e0072

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/LinearLayout;

    move-object/from16 v0, v20

    iput-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->titledatelayout:Landroid/widget/LinearLayout;

    .line 123
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 128
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 129
    .local v2, "cursor":Landroid/database/Cursor;
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v20

    if-eqz v20, :cond_2

    .line 130
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "bindview : cursor is null or closed"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    .end local p2    # "convertView":Landroid/view/View;
    :goto_1
    return-object p2

    .line 125
    .end local v2    # "cursor":Landroid/database/Cursor;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "holder":Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;
    check-cast v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;

    .restart local v9    # "holder":Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;
    goto :goto_0

    .line 134
    .restart local v2    # "cursor":Landroid/database/Cursor;
    :cond_2
    move/from16 v0, p1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 136
    const-wide/16 v6, 0x0

    .line 139
    .local v6, "duration":J
    const-wide/16 v4, 0x0

    .line 140
    .local v4, "date":J
    const-wide/16 v10, -0x1

    .line 141
    .local v10, "id":J
    const/4 v13, -0x1

    .line 142
    .local v13, "labelindex":I
    const/4 v15, 0x0

    .line 143
    .local v15, "mimetype":Ljava/lang/String;
    const/16 v18, 0x0

    .line 146
    .local v18, "tagsData":Ljava/lang/String;
    :try_start_0
    const-string v20, "duration"

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 147
    const-string v20, "title"

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 148
    .local v19, "title":Ljava/lang/String;
    const-string v20, "date_modified"

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 149
    const-string v20, "_id"

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 150
    const-string v20, "label_id"

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 151
    const-string v20, "mime_type"

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 152
    const-string v20, "year_name"

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v18

    .line 161
    sget-boolean v20, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_CHECK_ATTVN_ENABLED:Z

    if-eqz v20, :cond_a

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mFromVVM:Z

    move/from16 v20, v0

    if-eqz v20, :cond_a

    .line 162
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const v21, 0x7f0b0178

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(I)V

    .line 168
    :goto_2
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mPlayingId:J

    move-wide/from16 v20, v0

    cmp-long v20, v10, v20

    if-nez v20, :cond_b

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "getView() id == mPlayingId"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mContextChild:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f080029

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getColor(I)I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setTextColor(I)V

    .line 172
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setSelected(Z)V

    .line 179
    :goto_3
    sget-boolean v20, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_RCS:Z

    if-eqz v20, :cond_3

    .line 180
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->mmsText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const v21, 0x7f0b00a8

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(I)V

    .line 183
    :cond_3
    const-string v20, "%s  |  %s"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->getDateFormatByFormatSetting(J)Ljava/lang/String;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->stringForTime(J)Ljava/lang/String;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 184
    .local v3, "date_duration":Ljava/lang/String;
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->dateText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelColor(I)Ljava/lang/Integer;

    move-result-object v12

    .line 187
    .local v12, "labelcolor":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelTitle(I)Ljava/lang/String;

    move-result-object v14

    .line 189
    .local v14, "labeltitle":Ljava/lang/String;
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    if-eqz v20, :cond_c

    if-eqz v12, :cond_c

    .line 190
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 191
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 192
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 193
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 201
    :cond_4
    :goto_4
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->radiobutton:Landroid/widget/RadioButton;

    move-object/from16 v20, v0

    if-eqz v20, :cond_5

    .line 202
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v20, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v17, v20

    const/16 v20, 0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v17, v20

    .line 205
    .local v17, "tags":[Ljava/lang/Object;
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->radiobutton:Landroid/widget/RadioButton;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setTag(Ljava/lang/Object;)V

    .line 206
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->radiobutton:Landroid/widget/RadioButton;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mRadioClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    .end local v17    # "tags":[Ljava/lang/Object;
    :cond_5
    if-eqz v15, :cond_d

    const-string v20, "audio/amr"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 212
    sget-boolean v20, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_CHECK_ATTVN_ENABLED:Z

    if-eqz v20, :cond_6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mFromVVM:Z

    move/from16 v20, v0

    if-eqz v20, :cond_6

    .line 213
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->mmsText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const v21, 0x7f0b0176

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(I)V

    .line 216
    :cond_6
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->mmsText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setVisibility(I)V

    .line 220
    :goto_5
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->radiobutton:Landroid/widget/RadioButton;

    move-object/from16 v20, v0

    if-eqz v20, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mClearAll:Z

    move/from16 v20, v0

    if-eqz v20, :cond_7

    .line 221
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->radiobutton:Landroid/widget/RadioButton;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 223
    :cond_7
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->radiobutton:Landroid/widget/RadioButton;

    move-object/from16 v20, v0

    if-eqz v20, :cond_8

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mPositionID:J

    move-wide/from16 v20, v0

    cmp-long v20, v20, v10

    if-nez v20, :cond_8

    .line 224
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->radiobutton:Landroid/widget/RadioButton;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 226
    :cond_8
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->nfcicon:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    if-eqz v20, :cond_9

    .line 227
    if-eqz v18, :cond_e

    const-string v20, "NFC"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 228
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->nfcicon:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 234
    :cond_9
    :goto_6
    invoke-super/range {p0 .. p3}, Landroid/widget/SimpleCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_1

    .line 153
    .end local v3    # "date_duration":Ljava/lang/String;
    .end local v12    # "labelcolor":Ljava/lang/Integer;
    .end local v14    # "labeltitle":Ljava/lang/String;
    .end local v19    # "title":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 154
    .local v8, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v8}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto/16 :goto_1

    .line 156
    .end local v8    # "e":Landroid/database/CursorIndexOutOfBoundsException;
    :catch_1
    move-exception v16

    .line 157
    .local v16, "sde":Landroid/database/StaleDataException;
    invoke-virtual/range {v16 .. v16}, Landroid/database/StaleDataException;->printStackTrace()V

    goto/16 :goto_1

    .line 165
    .end local v16    # "sde":Landroid/database/StaleDataException;
    .restart local v19    # "title":Ljava/lang/String;
    :cond_a
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "  "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 174
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "getView() id != mPlayingId : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mPlayingId:J

    move-wide/from16 v22, v0

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mContextChild:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f08002a

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getColor(I)I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setTextColor(I)V

    .line 176
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setSelected(Z)V

    goto/16 :goto_3

    .line 194
    .restart local v3    # "date_duration":Ljava/lang/String;
    .restart local v12    # "labelcolor":Ljava/lang/Integer;
    .restart local v14    # "labeltitle":Ljava/lang/String;
    :cond_c
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    if-eqz v20, :cond_4

    if-nez v12, :cond_4

    .line 195
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mContextChild:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f08001b

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getColor(I)I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 196
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 197
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 198
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    goto/16 :goto_4

    .line 218
    :cond_d
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->mmsText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x8

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 230
    :cond_e
    iget-object v0, v9, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter$ViewHolder;->nfcicon:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x8

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 243
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 245
    .local v1, "view":Landroid/view/View;
    sget-boolean v2, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_FINGER_AIR_VIEW:Z

    if-eqz v2, :cond_1

    .line 246
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/view/View;->setHoverPopupType(I)V

    .line 247
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getHoverPopupWindowByToolType(Landroid/view/View;Z)Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 248
    .local v0, "hoverWindow":Landroid/widget/HoverPopupWindow;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->li:Landroid/view/LayoutInflater;

    if-nez v2, :cond_0

    .line 249
    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->li:Landroid/view/LayoutInflater;

    .line 251
    :cond_0
    if-eqz v0, :cond_1

    .line 252
    invoke-virtual {v0, p0}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 256
    .end local v0    # "hoverWindow":Landroid/widget/HoverPopupWindow;
    :cond_1
    return-object v1
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->LoadLabeltoMap()V

    .line 380
    :cond_0
    invoke-super {p0}, Landroid/widget/SimpleCursorAdapter;->notifyDataSetChanged()V

    .line 381
    return-void
.end method

.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "hpw"    # Landroid/widget/HoverPopupWindow;

    .prologue
    .line 282
    const v3, 0x7f0e0067

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 284
    .local v2, "titleText":Landroid/widget/TextView;
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 286
    .local v1, "text":Ljava/lang/CharSequence;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->li:Landroid/view/LayoutInflater;

    const v4, 0x7f03001f

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 287
    .local v0, "fullText":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    invoke-virtual {v2}, Landroid/widget/TextView;->isEllipsis()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 289
    if-eqz v1, :cond_0

    invoke-virtual {v2}, Landroid/widget/TextView;->isEllipsis()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 290
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 294
    :cond_0
    invoke-virtual {p2, v0}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 296
    const/4 v3, 0x1

    return v3
.end method

.method public selectRadionButton(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 84
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mPositionID:J

    .line 85
    return-void
.end method

.method public setClearAll(Z)V
    .locals 0
    .param p1, "clearall"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mClearAll:Z

    .line 77
    return-void
.end method

.method public setFromVVM(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mFromVVM:Z

    .line 81
    return-void
.end method

.method public setPlayingId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 88
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->mPlayingId:J

    .line 89
    return-void
.end method
