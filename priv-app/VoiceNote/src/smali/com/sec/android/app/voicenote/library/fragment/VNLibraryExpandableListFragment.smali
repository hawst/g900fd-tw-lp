.class public Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
.super Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;
.source "VNLibraryExpandableListFragment.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;
    }
.end annotation


# static fields
.field private static final LISTVIEW:Ljava/lang/String; = "listview"

.field private static final LISTVIEW_ACTIONMODE:Ljava/lang/String; = "actionmenumode"

.field private static final LISTVIEW_ALPHA:Ljava/lang/String; = "alpha"

.field private static final LISTVIEW_CHECKBOX_SIZE:Ljava/lang/String; = "checkbox_size"

.field private static final LISTVIEW_SEARCHTEXT:Ljava/lang/String; = "searchtext"

.field private static final LISTVIEW_TRIMMODE:Ljava/lang/String; = "trim"

.field private static final MENU_CHANGELABEL:I = 0x9

.field private static final MENU_DELETE:I = 0x2

.field private static final MENU_DETAILS:I = 0x7

.field private static final MENU_MOVETOSECRET:I = 0xa

.field private static final MENU_REMOVEFROMSECRET:I = 0xb

.field private static final MENU_RENAME:I = 0x3

.field private static final MENU_SETAS:I = 0x6

.field private static final MENU_SHAREVIA:I = 0x1

.field private static final MSG_CLOSE_OLD_CURSOR:I = 0x66

.field private static final MSG_FIND_POSITION:I = 0x65

.field private static final SCROLLING_STATE:Ljava/lang/String; = "ScrollingState"

.field private static final TAG:Ljava/lang/String; = "VNLibraryExpandableListFragment"

.field private static mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field private static mIsTrimMode:Z

.field private static mSearchText:Ljava/lang/String;

.field private static mSelectedPosition:I

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# instance fields
.field private cb_size:I

.field private checkedItemId:J

.field private isPressEditBtn:Z

.field private mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

.field private mActionStates:I

.field mArray:Landroid/util/SparseBooleanArray;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mCurrntPath:Ljava/lang/String;

.field private mCursor:Landroid/database/Cursor;

.field private mDisableClickSound:Z

.field private mEnableList:Z

.field private mHandler:Landroid/os/Handler;

.field private mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

.field mListView:Landroid/widget/ExpandableListView;

.field private mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

.field private mListviewAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

.field private mMainView:Landroid/view/View;

.field private mNothingCheckedItem:Z

.field private mOldCursor:Landroid/database/Cursor;

.field private mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

.field private mSearchEmptyview:Landroid/view/View;

.field private mSelectedID:J

.field private mbScrollList:Z

.field updateActionModeRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    sput-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 113
    sput-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 114
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    .line 117
    sput-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    .line 133
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mIsTrimMode:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 96
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;-><init>()V

    .line 108
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mMainView:Landroid/view/View;

    .line 109
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    .line 110
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mOldCursor:Landroid/database/Cursor;

    .line 111
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    .line 115
    iput-wide v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedID:J

    .line 116
    iput-wide v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->checkedItemId:J

    .line 118
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCurrntPath:Ljava/lang/String;

    .line 120
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchEmptyview:Landroid/view/View;

    .line 121
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mEnableList:Z

    .line 122
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mbScrollList:Z

    .line 123
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mDisableClickSound:Z

    .line 125
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionStates:I

    .line 126
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    .line 128
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 129
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 149
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    .line 150
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isPressEditBtn:Z

    .line 270
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mArray:Landroid/util/SparseBooleanArray;

    .line 397
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->cb_size:I

    .line 858
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mNothingCheckedItem:Z

    .line 860
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mAudioManager:Landroid/media/AudioManager;

    .line 912
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$3;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListviewAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 1135
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$4;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mHandler:Landroid/os/Handler;

    .line 2072
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$6;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->updateActionModeRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mOldCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mOldCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mNothingCheckedItem:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mNothingCheckedItem:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->checkedItemId:J

    return-wide v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    .param p1, "x1"    # J

    .prologue
    .line 96
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->checkedItemId:J

    return-wide p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCurrntPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCurrntPath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$902(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isPressEditBtn:Z

    return p1
.end method

.method private getAudioManager()Landroid/media/AudioManager;
    .locals 4

    .prologue
    .line 863
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v2, :cond_0

    .line 864
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mAudioManager:Landroid/media/AudioManager;

    .line 872
    :goto_0
    return-object v1

    .line 866
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 867
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_1

    .line 868
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 870
    .local v1, "audioManager":Landroid/media/AudioManager;
    goto :goto_0

    .line 872
    .end local v1    # "audioManager":Landroid/media/AudioManager;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    .locals 2

    .prologue
    .line 1837
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1838
    .local v0, "activity":Landroid/app/Activity;
    instance-of v1, v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v1, :cond_0

    .line 1839
    check-cast v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .end local v0    # "activity":Landroid/app/Activity;
    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 1842
    :cond_0
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    return-object v1
.end method

.method public static final getSearchText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1217
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    return-object v0
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 3
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 1159
    const-string v0, "VNLibraryExpandableListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initService : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 1161
    return-void
.end method

.method private initViews()V
    .locals 3

    .prologue
    .line 633
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "initViews E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mMainView:Landroid/view/View;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    .line 635
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mMainView:Landroid/view/View;

    const v1, 0x7f0e003b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchEmptyview:Landroid/view/View;

    .line 638
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p0}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 639
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p0}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 640
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p0}, Landroid/widget/ExpandableListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setEmptyView(Landroid/view/View;)V

    .line 642
    sget-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mIsTrimMode:Z

    if-eqz v0, :cond_0

    .line 643
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAlpha(F)V

    .line 646
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mEnableList:Z

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setEnabled(Z)V

    .line 647
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "initViews X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    return-void
.end method

.method private listBinding()Z
    .locals 28

    .prologue
    .line 450
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isRemoving()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 451
    const/4 v2, 0x0

    .line 616
    :goto_0
    return v2

    .line 453
    :cond_0
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "listBinding E"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "listBinding E"

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secV(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 455
    const-string v2, "category_text"

    const-wide/16 v26, -0x1

    move-wide/from16 v0, v26

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getLongSettings(Ljava/lang/String;J)J

    move-result-wide v12

    .line 456
    .local v12, "categotyID":J
    const-string v2, "sort_mode"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v22

    .line 457
    .local v22, "sortmode":I
    const/4 v2, 0x5

    move/from16 v0, v22

    if-ne v2, v0, :cond_2

    .line 458
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    .line 459
    .local v8, "activity":Landroid/app/Activity;
    if-eqz v8, :cond_2

    .line 460
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMode()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v8}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMounted(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 461
    :cond_1
    const-string v2, "sort_mode"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 465
    .end local v8    # "activity":Landroid/app/Activity;
    :cond_2
    const/4 v11, 0x0

    .line 466
    .local v11, "builder":Ljava/lang/StringBuilder;
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v23, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v23, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput-object v3, v23, v2

    .line 468
    .local v23, "temp_args":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 470
    .local v9, "arg_index":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-eqz v2, :cond_3

    .line 471
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->clearCursor()V

    .line 472
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->unregisterOnLoadedObserver()V

    .line 473
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    .line 485
    :cond_3
    const/4 v5, 0x0

    .line 486
    .local v5, "selection":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getListQuery(Landroid/content/Context;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 487
    const-wide/16 v2, -0x1

    cmp-long v2, v12, v2

    if-lez v2, :cond_4

    .line 488
    const-string v2, " and (label_id == ?)"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 489
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "arg_index":I
    .local v10, "arg_index":I
    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v23, v9

    move v9, v10

    .line 491
    .end local v10    # "arg_index":I
    .restart local v9    # "arg_index":I
    :cond_4
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 492
    const-string v2, " and (title LIKE ?"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    const/16 v21, 0x0

    .line 494
    .local v21, "searchText":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    .line 495
    .local v20, "isUnderScore":Z
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    .line 496
    .local v19, "isPecent":Z
    if-nez v20, :cond_5

    if-eqz v19, :cond_9

    .line 497
    :cond_5
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    const-string v3, "_"

    const-string v4, "\\_"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "%"

    const-string v4, "\\%"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v21

    .line 499
    const-string v2, " escape \'\\\'"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 503
    :goto_1
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "arg_index":I
    .restart local v10    # "arg_index":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v23, v9

    .line 504
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITH_SVOICE(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITHOUT_SVOICE()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 506
    :cond_6
    const-string v2, " or is_memo == \'2\'"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 507
    :cond_7
    const-string v2, ")"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v9, v10

    .line 510
    .end local v10    # "arg_index":I
    .end local v19    # "isPecent":Z
    .end local v20    # "isUnderScore":Z
    .end local v21    # "searchText":Ljava/lang/String;
    .restart local v9    # "arg_index":I
    :cond_8
    const/4 v6, 0x0

    .line 511
    .local v6, "args":[Ljava/lang/String;
    if-lez v9, :cond_a

    .line 512
    new-array v6, v9, [Ljava/lang/String;

    .line 513
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_2
    move/from16 v0, v18

    if-ge v0, v9, :cond_a

    .line 514
    aget-object v2, v23, v18

    aput-object v2, v6, v18

    .line 513
    add-int/lit8 v18, v18, 0x1

    goto :goto_2

    .line 501
    .end local v6    # "args":[Ljava/lang/String;
    .end local v18    # "i":I
    .restart local v19    # "isPecent":Z
    .restart local v20    # "isUnderScore":Z
    .restart local v21    # "searchText":Ljava/lang/String;
    :cond_9
    sget-object v21, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    goto :goto_1

    .line 517
    .end local v19    # "isPecent":Z
    .end local v20    # "isUnderScore":Z
    .end local v21    # "searchText":Ljava/lang/String;
    .restart local v6    # "args":[Ljava/lang/String;
    :cond_a
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 519
    const/4 v2, 0x5

    new-array v14, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v14, v2

    const/4 v2, 0x1

    const-string v3, "title"

    aput-object v3, v14, v2

    const/4 v2, 0x2

    const-string v3, "date_modified"

    aput-object v3, v14, v2

    const/4 v2, 0x3

    const-string v3, "duration"

    aput-object v3, v14, v2

    const/4 v2, 0x4

    const-string v3, "_data"

    aput-object v3, v14, v2

    .line 525
    .local v14, "cols":[Ljava/lang/String;
    const/4 v2, 0x3

    new-array v0, v2, [I

    move-object/from16 v24, v0

    fill-array-data v24, :array_0

    .line 529
    .local v24, "to":[I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getSortQuery()Ljava/lang/String;

    move-result-object v7

    .line 531
    .local v7, "orderBy":Ljava/lang/String;
    const/4 v15, 0x0

    .line 533
    .local v15, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v15

    .line 549
    :cond_b
    :goto_3
    if-nez v15, :cond_c

    .line 550
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "listBinding() : cursor null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    new-instance v18, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/voicenote/library/subactivity/NoSdCardActivity;

    move-object/from16 v0, v18

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 552
    .local v18, "i":Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 553
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 554
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 535
    .end local v18    # "i":Landroid/content/Intent;
    :catch_0
    move-exception v16

    .line 536
    .local v16, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "VNLibraryExpandableListFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "listBinding - SQLiteException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    if-eqz v15, :cond_b

    invoke-interface {v15}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_b

    .line 538
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 539
    const/4 v15, 0x0

    goto :goto_3

    .line 541
    .end local v16    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v17

    .line 542
    .local v17, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v2, "VNLibraryExpandableListFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "listBinding - UnsupportedOperationException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    if-eqz v15, :cond_b

    invoke-interface {v15}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_b

    .line 544
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 545
    const/4 v15, 0x0

    goto :goto_3

    .line 557
    .end local v17    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_c
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 558
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09002b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setNoItemTopMargin(I)V

    .line 561
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchEmptyview:Landroid/view/View;

    if-eqz v2, :cond_e

    .line 562
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_12

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    if-eqz v2, :cond_12

    .line 563
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09002b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setNoItemTopMargin(I)V

    .line 564
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchEmptyview:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 570
    :cond_e
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-nez v2, :cond_14

    .line 571
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 572
    new-instance v2, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f030030

    const v25, 0x7f03002e

    move/from16 v0, v25

    invoke-direct {v2, v15, v3, v4, v0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;II)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    .line 580
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setLabelCallback(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;)V

    .line 584
    :goto_6
    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    .line 586
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_f

    .line 587
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$2;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;Landroid/os/Handler;)V

    invoke-interface {v2, v3}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 607
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v2, :cond_10

    .line 608
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getLoader()Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ExpandableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 609
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setSearchText(Ljava/lang/String;)V

    .line 610
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setListAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 611
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    if-eqz v2, :cond_11

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 614
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->refreshOptionsMenu()V

    .line 615
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "listBinding X"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 566
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchEmptyview:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    .line 576
    :cond_13
    new-instance v2, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f03002f

    const v25, 0x7f03002e

    move/from16 v0, v25

    invoke-direct {v2, v15, v3, v4, v0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;II)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    goto/16 :goto_5

    .line 582
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v2, v15}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto/16 :goto_6

    .line 525
    nop

    :array_0
    .array-data 4
        0x7f0e0067
        0x7f0e0068
        0x7f0e0069
    .end array-data
.end method

.method public static newInstance(Z)Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    .locals 3
    .param p0, "enableList"    # Z

    .prologue
    .line 153
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;-><init>()V

    .line 154
    .local v1, "f":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 155
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "enableList"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 156
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 157
    return-object v1
.end method

.method public static releaseService()V
    .locals 3

    .prologue
    .line 1164
    const-string v0, "VNLibraryExpandableListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "releaseService : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1165
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 1166
    return-void
.end method

.method private scrolltoHiddenPlayItem()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 691
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "scrolltoHiddenPlayItem E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCurrentPlayState()I

    move-result v0

    const/16 v1, 0x15

    if-ne v0, v1, :cond_1

    .line 703
    :cond_0
    :goto_0
    return-void

    .line 693
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getFirstVisiblePosition()I

    move-result v0

    sget v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    invoke-virtual {p0, v1, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getChangedGroupPosition(II)I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 694
    const-string v0, "VNLibraryExpandableListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "scrolltoHiddenPlayItem1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getChangedGroupPosition(II)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    sget v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    invoke-virtual {p0, v1, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getChangedGroupPosition(II)I

    move-result v1

    invoke-virtual {v0, v1, v3}, Landroid/widget/ExpandableListView;->smoothScrollToPositionFromTop(II)V

    .line 698
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getLastVisiblePosition()I

    move-result v0

    sget v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    invoke-virtual {p0, v1, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getChangedGroupPosition(II)I

    move-result v1

    if-gt v0, v1, :cond_3

    .line 699
    const-string v0, "VNLibraryExpandableListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "scrolltoHiddenPlayItem2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getChangedGroupPosition(II)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    sget v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    invoke-virtual {p0, v1, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getChangedGroupPosition(II)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2}, Landroid/widget/ExpandableListView;->getLastVisiblePosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ExpandableListView;->smoothScrollToPosition(II)V

    .line 702
    :cond_3
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "scrolltoHiddenPlayItem X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setEmptyView()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 652
    const/4 v2, 0x0

    .line 653
    .local v2, "textview_top":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mMainView:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 655
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09002a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setNoItemTopMargin(I)V

    .line 657
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mMainView:Landroid/view/View;

    const v4, 0x7f0e001a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "textview_top":Landroid/widget/TextView;
    check-cast v2, Landroid/widget/TextView;

    .line 658
    .restart local v2    # "textview_top":Landroid/widget/TextView;
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 659
    if-nez v2, :cond_1

    .line 660
    const-string v3, "VNLibraryExpandableListFragment"

    const-string v4, "setEmptyView X - textView is null"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    :goto_0
    return-void

    .line 664
    :cond_0
    const-string v3, "VNLibraryExpandableListFragment"

    const-string v4, "setEmptyView X - MainView is null"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 668
    :cond_1
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 670
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09002b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setNoItemTopMargin(I)V

    .line 671
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 672
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchEmptyview:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 679
    :goto_1
    const-string v3, "category_text"

    const-wide/16 v4, -0x1

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getLongSettings(Ljava/lang/String;J)J

    move-result-wide v0

    .line 681
    .local v0, "category":J
    const-string v3, "VNLibraryExpandableListFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setEmptyView E : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    const-string v3, "VNLibraryExpandableListFragment"

    const-string v4, "setEmptyView X"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 675
    .end local v0    # "category":J
    :cond_2
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 676
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchEmptyview:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private setNoItemTopMargin(I)V
    .locals 4
    .param p1, "margin"    # I

    .prologue
    .line 2134
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mMainView:Landroid/view/View;

    const v3, 0x7f0e003a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2135
    .local v1, "noitembg":Landroid/widget/ImageView;
    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2136
    .local v0, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 2137
    return-void
.end method

.method public static setTrimMode(Z)V
    .locals 3
    .param p0, "isTrimMode"    # Z

    .prologue
    .line 1331
    const-string v0, "VNLibraryExpandableListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTrimMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1332
    sput-boolean p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mIsTrimMode:Z

    .line 1333
    return-void
.end method


# virtual methods
.method public clickNextItem()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1234
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "clickNextItem E"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1235
    sget v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    invoke-virtual {p0, v2, v6}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getChangedGroupPosition(II)I

    move-result v0

    .line 1236
    .local v0, "newPos":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isNextItem()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1237
    const-string v2, "VNLibraryExpandableListFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "next item ID:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    sget v5, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroupId(I)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1238
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getView()Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    sget v5, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroupId(I)J

    move-result-wide v4

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/widget/ExpandableListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 1239
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->showLibraryActionbar()V

    .line 1250
    :cond_0
    :goto_0
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "clickNextItem X"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1251
    return-void

    .line 1240
    :cond_1
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_0

    .line 1241
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRepeatTime()[I

    move-result-object v1

    .line 1242
    .local v1, "repeatTime":[I
    aget v2, v1, v5

    if-ltz v2, :cond_3

    aget v2, v1, v6

    if-ltz v2, :cond_3

    .line 1243
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    aget v2, v1, v5

    aget v4, v1, v6

    if-ge v2, v4, :cond_2

    aget v2, v1, v5

    :goto_1
    invoke-virtual {v3, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 1247
    :goto_2
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1248
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onDoResume()V

    goto :goto_0

    .line 1243
    :cond_2
    aget v2, v1, v6

    goto :goto_1

    .line 1245
    :cond_3
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    goto :goto_2
.end method

.method public clickPreviousItem()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1290
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "clickPreviousItem E"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1291
    sget v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    const/4 v3, -0x1

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getChangedGroupPosition(II)I

    move-result v0

    .line 1292
    .local v0, "newPos":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isPreviousItem()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1293
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getView()Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    sget v5, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroupId(I)J

    move-result-wide v4

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/widget/ExpandableListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 1294
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->showLibraryActionbar()V

    .line 1306
    :cond_0
    :goto_0
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "clickPreviousItem X"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1307
    return-void

    .line 1295
    :cond_1
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_0

    .line 1296
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRepeatTime()[I

    move-result-object v1

    .line 1297
    .local v1, "repeatTime":[I
    aget v2, v1, v5

    if-ltz v2, :cond_3

    aget v2, v1, v6

    if-ltz v2, :cond_3

    .line 1298
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    aget v2, v1, v5

    aget v4, v1, v6

    if-ge v2, v4, :cond_2

    aget v2, v1, v5

    :goto_1
    invoke-virtual {v3, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 1302
    :goto_2
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1303
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onDoResume()V

    goto :goto_0

    .line 1298
    :cond_2
    aget v2, v1, v6

    goto :goto_1

    .line 1300
    :cond_3
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    goto :goto_2
.end method

.method public doPlayTrimNewFile()V
    .locals 7

    .prologue
    .line 1199
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->getLastSavedFileUri()Landroid/net/Uri;

    move-result-object v3

    .line 1200
    .local v3, "uri":Landroid/net/Uri;
    const-string v4, "VNLibraryExpandableListFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doPlayTrimNewFile E : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1201
    if-eqz v3, :cond_1

    .line 1202
    invoke-static {v3}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 1203
    .local v0, "id":J
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->findPosition(Ljava/lang/Object;)I

    move-result v2

    .line 1204
    .local v2, "newPosition":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 1205
    sput v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    .line 1206
    const-string v4, "VNLibraryExpandableListFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doPlayTrimNewFile : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1208
    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mDisableClickSound:Z

    .line 1209
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getView()Landroid/view/View;

    move-result-object v5

    sget v6, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    invoke-virtual {v4, v5, v6, v0, v1}, Landroid/widget/ExpandableListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 1210
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mDisableClickSound:Z

    .line 1211
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    sget v5, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    invoke-virtual {v4, v5}, Landroid/widget/ExpandableListView;->smoothScrollToPosition(I)V

    .line 1213
    .end local v0    # "id":J
    .end local v2    # "newPosition":I
    :cond_1
    const-string v4, "VNLibraryExpandableListFragment"

    const-string v5, "doPlayTrimNewFile X"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214
    return-void
.end method

.method public enableBuildCache()V
    .locals 2

    .prologue
    .line 1891
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-eqz v0, :cond_0

    .line 1892
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->enableBuildCache(Landroid/widget/AbsListView;)V

    .line 1894
    :cond_0
    return-void
.end method

.method public executeTrimMode()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1336
    const-string v0, "VNLibraryExpandableListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "executeTrimMode E : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mIsTrimMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1340
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_0

    .line 1341
    sget-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mIsTrimMode:Z

    if-eqz v0, :cond_1

    .line 1342
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAlpha(F)V

    .line 1343
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v3}, Landroid/widget/ExpandableListView;->setEnabled(Z)V

    .line 1344
    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setFocusListView(Z)V

    .line 1351
    :cond_0
    :goto_0
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "executeTrimMode X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1352
    return-void

    .line 1346
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAlpha(F)V

    .line 1347
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v4}, Landroid/widget/ExpandableListView;->setEnabled(Z)V

    .line 1348
    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setFocusListView(Z)V

    goto :goto_0
.end method

.method protected expandAllChildren()V
    .locals 3

    .prologue
    .line 1367
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-eqz v2, :cond_0

    .line 1368
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroupCount()I

    move-result v0

    .line 1369
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 1370
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 1369
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1372
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method findPosition()V
    .locals 4

    .prologue
    .line 1113
    const-string v1, "VNLibraryExpandableListFragment"

    const-string v2, "findPosition : E"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-nez v1, :cond_0

    .line 1115
    const-string v1, "VNLibraryExpandableListFragment"

    const-string v2, "findPosition : adapter is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1133
    :goto_0
    return-void

    .line 1117
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1118
    const-string v1, "VNLibraryExpandableListFragment"

    const-string v2, "findPosition : fragment is being removed from its activity"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1122
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedID:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->findPosition(Ljava/lang/Object;)I

    move-result v0

    .line 1123
    .local v0, "result":I
    const-string v1, "VNLibraryExpandableListFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Selected pos = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1125
    sget v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    if-eq v0, v1, :cond_2

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 1126
    const-string v1, "VNLibraryExpandableListFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1127
    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    .line 1128
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1129
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onDataChanged()V

    .line 1132
    :cond_2
    const-string v1, "VNLibraryExpandableListFragment"

    const-string v2, "findPosition : X"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getActionMode()Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 1375
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1376
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v0}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    .line 1378
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChangedGroupPosition(II)I
    .locals 4
    .param p1, "groupPosition"    # I
    .param p2, "relative"    # I

    .prologue
    .line 1277
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-nez v1, :cond_0

    .line 1278
    const/4 v0, -0x1

    .line 1287
    :goto_0
    return v0

    .line 1280
    :cond_0
    const/4 v0, -0x1

    .line 1281
    .local v0, "newPosition":I
    const-wide/16 v2, -0x1

    .line 1283
    .local v2, "packed":J
    add-int v0, p1, p2

    .line 1285
    invoke-static {v0}, Landroid/widget/ExpandableListView;->getPackedPositionForGroup(I)J

    move-result-wide v2

    .line 1286
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v2, v3}, Landroid/widget/ExpandableListView;->getFlatListPosition(J)I

    move-result v0

    .line 1287
    goto :goto_0
.end method

.method public getCurrentPlayID()J
    .locals 2

    .prologue
    .line 1016
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_0

    .line 1017
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "getCurrentPlayID - mService is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    const-wide/16 v0, -0x1

    .line 1021
    :goto_0
    return-wide v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getCurrentPlayState()I
    .locals 2

    .prologue
    .line 1026
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "getCurrentPlayState"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_0

    .line 1028
    const/16 v0, 0x15

    .line 1029
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v0

    goto :goto_0
.end method

.method public getItemTitle(I)Ljava/lang/String;
    .locals 4
    .param p1, "directionPos"    # I

    .prologue
    .line 1221
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isNextItem()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isPreviousItem()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1222
    :cond_1
    const/4 p1, 0x0

    .line 1224
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    sget v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    add-int/2addr v3, p1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroupId(I)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getFileTitle(I)Ljava/lang/String;

    move-result-object v0

    .line 1226
    .local v0, "title":Ljava/lang/String;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1227
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    sget v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    add-int/2addr v2, p1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1228
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    const-string v3, "title"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1230
    :cond_4
    return-object v0
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method public getShowFragmentAnimator(Landroid/animation/AnimatorSet;)Landroid/animation/Animator;
    .locals 6
    .param p1, "animatorSet"    # Landroid/animation/AnimatorSet;

    .prologue
    .line 1846
    invoke-virtual {p1}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v1

    .line 1848
    .local v1, "children":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    .line 1849
    .local v0, "animator":Landroid/animation/Animator;
    instance-of v5, v0, Landroid/animation/ObjectAnimator;

    if-eqz v5, :cond_0

    move-object v5, v0

    .line 1850
    check-cast v5, Landroid/animation/ObjectAnimator;

    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->getPropertyName()Ljava/lang/String;

    move-result-object v4

    .line 1851
    .local v4, "propertyName":Ljava/lang/String;
    const-string v5, "translationY"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1852
    new-instance v5, Landroid/view/animation/interpolator/SineInOut80;

    invoke-direct {v5}, Landroid/view/animation/interpolator/SineInOut80;-><init>()V

    invoke-virtual {v0, v5}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    goto :goto_0

    .line 1856
    .end local v0    # "animator":Landroid/animation/Animator;
    .end local v4    # "propertyName":Ljava/lang/String;
    :cond_1
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1857
    .local v3, "newAnimator":Landroid/animation/AnimatorSet;
    invoke-virtual {v3, v1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 1858
    return-object v3
.end method

.method public initListViewSelection()V
    .locals 2

    .prologue
    .line 2121
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2122
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_0

    .line 2123
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setSelection(I)V

    .line 2126
    :cond_0
    return-void
.end method

.method public isItemChecked(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x0

    .line 1034
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActionMode()Landroid/view/ActionMode;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1040
    :cond_0
    :goto_0
    return v0

    .line 1037
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1}, Landroid/widget/ExpandableListView;->getChoiceMode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1040
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    goto :goto_0
.end method

.method public isNextItem()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1254
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "isNextItem E"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1256
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-nez v2, :cond_0

    .line 1257
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "isNextItem : adapter is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    :goto_0
    return v1

    .line 1262
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    sget v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroupId(I)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 1263
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "isNextItem : false"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1266
    :catch_0
    move-exception v0

    .line 1267
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "isNextItem : exception false"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1270
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_1
    const-string v1, "VNLibraryExpandableListFragment"

    const-string v2, "isNextItem X"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isPreviousItem()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1310
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "isPreviousItem E"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1312
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-nez v2, :cond_0

    .line 1313
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "isPreviousItem : adapter is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1327
    :goto_0
    return v1

    .line 1318
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    sget v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroupId(I)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 1319
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "isPreviousItem : false"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1322
    :catch_0
    move-exception v0

    .line 1323
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "isPreviousItem : exception false"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1326
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_1
    const-string v1, "VNLibraryExpandableListFragment"

    const-string v2, "isPreviousItem X"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1327
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected moveSecretBox(J)V
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 1169
    const-string v3, "VNLibraryExpandableListFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "moveSecretBox E : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1171
    .local v1, "filePath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1173
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->newInstance()Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    move-result-object v2

    .line 1174
    .local v2, "fragment":Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 1175
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v3, "target_datas"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1176
    const-string v3, "movetosecret"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1177
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_FILE"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1178
    const-string v3, "VNLibraryExpandableListFragment"

    const-string v4, "moveSecretBox X"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1179
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 400
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "onActivityCreated E"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v2, :cond_1

    .line 402
    const-string v2, "listview"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 403
    .local v1, "state":Landroid/os/Parcelable;
    const-string v2, "alpha"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    .line 404
    .local v0, "alpha":F
    const-string v2, "trim"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mIsTrimMode:Z

    .line 405
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->executeTrimMode()V

    .line 406
    const-string v2, "VNLibraryExpandableListFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onActivityCreated : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mIsTrimMode:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    if-eqz v1, :cond_0

    .line 408
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "onActivityCreated : restore"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v1}, Landroid/widget/ExpandableListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 411
    :cond_0
    const-string v2, "actionmenumode"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionStates:I

    .line 412
    const-string v2, "checkbox_size"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->cb_size:I

    .line 413
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    sget-object v2, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    if-eqz v2, :cond_1

    .line 414
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    sget-object v2, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    const-string v3, "ScrollingState"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->setmScrollingState(I)V

    .line 417
    .end local v0    # "alpha":F
    .end local v1    # "state":Landroid/os/Parcelable;
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 418
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "onActivityCreated X"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 423
    const-string v3, "VNLibraryExpandableListFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onActivityResult E : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    sparse-switch p1, :sswitch_data_0

    .line 445
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 446
    const-string v3, "VNLibraryExpandableListFragment"

    const-string v4, "onActivityResult X"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    return-void

    .line 426
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    .line 428
    .local v0, "actionMode":Landroid/view/ActionMode;
    :try_start_0
    invoke-virtual {v0}, Landroid/view/ActionMode;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 429
    .local v2, "optionId":I
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    invoke-virtual {v3, v2, p3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->doAction(ILandroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 430
    .end local v2    # "optionId":I
    :catch_0
    move-exception v1

    .line 431
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v3, "VNLibraryExpandableListFragment"

    const-string v4, "onActivityResult cannot process requset zero"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 435
    .end local v0    # "actionMode":Landroid/view/ActionMode;
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :sswitch_1
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->updateState(Z)V

    goto :goto_0

    .line 438
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 439
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v3}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->finishCustomActionMode()V

    goto :goto_0

    .line 424
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x64 -> :sswitch_2
    .end sparse-switch
.end method

.method public onAlertDialogPositiveBtn(Z)V
    .locals 4
    .param p1, "positive"    # Z

    .prologue
    .line 2052
    if-eqz p1, :cond_0

    .line 2053
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isPressEditBtn:Z

    if-eqz v0, :cond_1

    .line 2054
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCurrntPath:Ljava/lang/String;

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->checkedItemId:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->showEditDialog(Ljava/lang/String;J)V

    .line 2060
    :cond_0
    :goto_0
    return-void

    .line 2056
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isPressEditBtn:Z

    .line 2057
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showEditDialoginPlay(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Landroid/content/Context;Landroid/app/FragmentManager;)J

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 848
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 850
    invoke-super {p0, p1}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->onAttach(Landroid/app/Activity;)V

    .line 851
    return-void
.end method

.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 6
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I
    .param p5, "id"    # J

    .prologue
    .line 924
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-eqz v1, :cond_1

    .line 925
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v1, p3}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getFileId(I)J

    move-result-wide v2

    .line 926
    .local v2, "fileId":J
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 927
    const/4 v1, 0x0

    .line 935
    .end local v2    # "fileId":J
    :goto_0
    return v1

    .line 929
    .restart local v2    # "fileId":J
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v1, p3, p4}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getElapsed(II)J

    move-result-wide v4

    long-to-int v0, v4

    .line 931
    .local v0, "elapsed":I
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onBookmarkChildPressed(JI)V

    .line 932
    sput p3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    .line 933
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v1, p3}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroupId(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedID:J

    .line 935
    .end local v0    # "elapsed":I
    .end local v2    # "fileId":J
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 13
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/16 v10, 0x15

    const/4 v12, 0x1

    .line 755
    const-string v7, "VNLibraryExpandableListFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onContextItemSelected E : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v5

    check-cast v5, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;

    .line 760
    .local v5, "info":Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 842
    :goto_0
    :pswitch_0
    const-string v7, "VNLibraryExpandableListFragment"

    const-string v8, "onContextItemSelected X"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    return v12

    .line 763
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    iget-wide v8, v5, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->id:J

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkSTT(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 765
    .local v3, "filename":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 766
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;

    invoke-direct {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;-><init>()V

    .local v2, "dialog":Landroid/app/DialogFragment;
    move-object v7, v2

    .line 767
    check-cast v7, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;

    iget-wide v8, v5, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->id:J

    invoke-virtual {v7, v8, v9, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNShareViaSTTSelectDialogFragment;->SetParameter(JLjava/lang/String;)V

    .line 768
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v7, v8}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 770
    .end local v2    # "dialog":Landroid/app/DialogFragment;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    iget-wide v8, v5, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->id:J

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->singleSend(Landroid/content/Context;J)V

    goto :goto_0

    .line 777
    .end local v3    # "filename":Ljava/lang/String;
    :pswitch_2
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;-><init>()V

    .line 778
    .local v1, "deletedialog":Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;
    iget-wide v8, v5, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->id:J

    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-nez v7, :cond_1

    .line 779
    iget-wide v8, v5, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->id:J

    invoke-virtual {v1, v8, v9, v12}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->setArguments(JZ)V

    .line 783
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v7, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 781
    :cond_1
    iget-wide v8, v5, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->id:J

    const/4 v7, 0x0

    invoke-virtual {v1, v8, v9, v7}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->setArguments(JZ)V

    goto :goto_1

    .line 787
    .end local v1    # "deletedialog":Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;
    :pswitch_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 788
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 789
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7, v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasTagData(Landroid/content/Context;Ljava/util/ArrayList;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 790
    .local v4, "hasNFC":Ljava/lang/Boolean;
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 791
    const v7, 0x7f0b0105

    const v8, 0x7f0b013d

    invoke-static {v7, v8, v12}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(III)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    const-string v9, "FRAGMENT_DIALOG"

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 795
    :cond_2
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showEditDialoginPlay(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Landroid/content/Context;Landroid/app/FragmentManager;)J

    goto/16 :goto_0

    .line 800
    .end local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v4    # "hasNFC":Ljava/lang/Boolean;
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    iget-wide v8, v5, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->id:J

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->setArguments(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;

    move-result-object v2

    .line 801
    .restart local v2    # "dialog":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG_SETAS"

    invoke-virtual {v2, v7, v8}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 805
    .end local v2    # "dialog":Landroid/app/DialogFragment;
    :pswitch_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 806
    .local v6, "title":Ljava/lang/String;
    iget-wide v8, v5, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->id:J

    const v7, 0x7f0b006d

    invoke-static {v8, v9, v7}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->setArguments(JI)Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;

    move-result-object v2

    .line 807
    .restart local v2    # "dialog":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v7, v8}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 811
    .end local v2    # "dialog":Landroid/app/DialogFragment;
    .end local v6    # "title":Ljava/lang/String;
    :pswitch_6
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 812
    .restart local v6    # "title":Ljava/lang/String;
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    invoke-direct {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;-><init>()V

    .restart local v2    # "dialog":Landroid/app/DialogFragment;
    move-object v7, v2

    .line 813
    check-cast v7, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    iget-wide v8, v5, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->id:J

    invoke-virtual {v7, v8, v9, v6}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->SetParameter(JLjava/lang/String;)V

    .line 814
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v7, v8}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 818
    .end local v2    # "dialog":Landroid/app/DialogFragment;
    .end local v6    # "title":Ljava/lang/String;
    :pswitch_7
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCurrentPlayState()I

    move-result v7

    if-eq v7, v10, :cond_3

    iget-wide v8, v5, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->id:J

    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-nez v7, :cond_3

    .line 820
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 821
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v7}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onDoStop()V

    .line 825
    :cond_3
    iget-wide v8, v5, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->id:J

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->moveSecretBox(J)V

    goto/16 :goto_0

    .line 829
    :pswitch_8
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v7, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCurrentPlayState()I

    move-result v7

    if-eq v7, v10, :cond_4

    iget-wide v8, v5, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->id:J

    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-nez v7, :cond_4

    .line 831
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v7, :cond_4

    .line 832
    sget-object v7, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v7}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onDoStop()V

    .line 836
    :cond_4
    iget-wide v8, v5, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->id:J

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->restoreSecretBox(J)V

    goto/16 :goto_0

    .line 760
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 235
    const-string v2, "VNLibraryExpandableListFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate() E : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 238
    .local v0, "activity":Landroid/app/Activity;
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 240
    const-string v2, "VNLibraryExpandableListFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate() callback : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    invoke-super {p0, p1}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 243
    if-eqz v0, :cond_0

    .line 247
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "quickconnect"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/quickconnect/QuickConnectManager;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 249
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :cond_0
    :goto_0
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "onCreate() X"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    return-void

    .line 261
    :catch_0
    move-exception v1

    .line 262
    .local v1, "e":Ljava/lang/NoClassDefFoundError;
    const-string v2, "VNLibraryExpandableListFragment"

    const-string v3, "onCreate() : mQuickConnect is not defined"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateAnimator(IZI)Landroid/animation/Animator;
    .locals 2
    .param p1, "transit"    # I
    .param p2, "enter"    # Z
    .param p3, "nextAnim"    # I

    .prologue
    .line 1863
    if-nez p3, :cond_0

    .line 1864
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->onCreateAnimator(IZI)Landroid/animation/Animator;

    move-result-object v0

    .line 1887
    :goto_0
    return-object v0

    .line 1866
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, p3}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v1

    check-cast v1, Landroid/animation/AnimatorSet;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getShowFragmentAnimator(Landroid/animation/AnimatorSet;)Landroid/animation/Animator;

    move-result-object v0

    .line 1868
    .local v0, "anim":Landroid/animation/Animator;
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$5;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 12
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    const/16 v11, 0x15

    const/4 v10, 0x0

    .line 706
    const-string v6, "VNLibraryExpandableListFragment"

    const-string v7, "onCreateContextMenu E"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    move-object v3, p3

    .line 709
    check-cast v3, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;

    .line 710
    .local v3, "mi":Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;
    if-eqz v3, :cond_0

    sget-object v6, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v6, :cond_1

    .line 751
    :cond_0
    :goto_0
    return-void

    .line 713
    :cond_1
    const/4 v4, 0x0

    .line 714
    .local v4, "name":Ljava/lang/String;
    iget-object v6, v3, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->targetView:Landroid/view/View;

    const v7, 0x7f0e0067

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 715
    .local v5, "textview":Landroid/widget/TextView;
    iget-object v6, v3, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->targetView:Landroid/view/View;

    const v7, 0x7f0e006a

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 716
    .local v2, "label":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    iget-wide v8, v3, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->id:J

    invoke-static {v6, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isSecretBoxFile(Landroid/content/Context;J)I

    move-result v0

    .line 718
    .local v0, "bSecretboxFile":I
    if-eqz v5, :cond_2

    .line 719
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 720
    const-string v6, "VNLibraryExpandableListFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onCreateContextMenu : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    :cond_2
    if-nez v4, :cond_3

    .line 724
    const-string v6, "VNLibraryExpandableListFragment"

    const-string v7, "onCreateContextMenu: Can`t load file."

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 728
    :cond_3
    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 729
    const/4 v6, 0x1

    const v7, 0x7f0b011f

    invoke-interface {p1, v10, v6, v10, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 730
    const/4 v6, 0x2

    const v7, 0x7f0b005d

    invoke-interface {p1, v10, v6, v10, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 732
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCurrentPlayState()I

    move-result v6

    if-ne v6, v11, :cond_4

    .line 733
    const/4 v6, 0x3

    const v7, 0x7f0b0105

    invoke-interface {p1, v10, v6, v10, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 735
    :cond_4
    if-eqz v0, :cond_5

    .line 736
    const/4 v6, 0x6

    const v7, 0x7f0b011c

    invoke-interface {p1, v10, v6, v10, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 738
    :cond_5
    if-eqz v2, :cond_6

    .line 739
    const-string v6, "VNLibraryExpandableListFragment"

    const-string v7, "onCreateContextMenu : label is contained"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    const/16 v6, 0x9

    const v7, 0x7f0b002d

    invoke-interface {p1, v10, v6, v10, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 741
    .local v1, "item":Landroid/view/MenuItem;
    if-nez v0, :cond_6

    invoke-interface {v1, v10}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 744
    .end local v1    # "item":Landroid/view/MenuItem;
    :cond_6
    sget-object v6, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isKNOXAvailable()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCurrentPlayState()I

    move-result v6

    if-ne v6, v11, :cond_7

    .line 745
    const-string v6, "VNLibraryExpandableListFragment"

    const-string v7, "onCreateContextMenu : SecretMode"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    const/16 v6, 0xa

    const v7, 0x7f0b00c4

    invoke-interface {p1, v10, v6, v10, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 749
    :cond_7
    const/4 v6, 0x7

    const v7, 0x7f0b006d

    invoke-interface {p1, v10, v6, v10, v7}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 750
    const-string v6, "VNLibraryExpandableListFragment"

    const-string v7, "onCreateContextMenu X"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 164
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 165
    const-string v1, "enableList"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mEnableList:Z

    .line 167
    :cond_0
    const v1, 0x7f030018

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mMainView:Landroid/view/View;

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mMainView:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 170
    if-eqz p3, :cond_1

    .line 171
    const-string v1, "searchtext"

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    .line 173
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->initViews()V

    .line 174
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->listBinding()Z

    .line 176
    if-eqz p3, :cond_2

    .line 177
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setEmptyView()V

    .line 182
    :cond_2
    const-string v1, "VNLibraryExpandableListFragment"

    const-string v2, "onCreateView() X"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mMainView:Landroid/view/View;

    return-object v1
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 208
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "onDestroy() E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->clearCursor()V

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->unregisterOnLoadedObserver()V

    .line 213
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 218
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    if-eqz v0, :cond_2

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 224
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 225
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 228
    :cond_2
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "onDestroy() X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-super {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->onDestroy()V

    .line 231
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 189
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "onDestroyView() E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_0

    .line 191
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "onDestroyView() listview is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->setEmptyView(Landroid/view/View;)V

    .line 193
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mMainView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 198
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mMainView:Landroid/view/View;

    .line 200
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mIsTrimMode:Z

    .line 201
    sput-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    .line 202
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "onDestroyView() X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-super {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->onDestroyView()V

    .line 204
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 855
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 856
    invoke-super {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->onDetach()V

    .line 857
    return-void
.end method

.method public onExpandButtonClicked(JI)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "groupPos"    # I

    .prologue
    .line 989
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_0

    .line 990
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p3}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 991
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p3}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 995
    :cond_0
    :goto_0
    return-void

    .line 993
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p3}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    goto :goto_0
.end method

.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 8
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 876
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-nez v4, :cond_0

    .line 909
    :goto_0
    return v2

    .line 877
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v1

    .line 878
    .local v1, "audioMng":Landroid/media/AudioManager;
    if-eqz v1, :cond_1

    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mDisableClickSound:Z

    if-nez v4, :cond_1

    .line 879
    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 881
    :cond_1
    const-string v4, "VNLibraryExpandableListFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onGroupClick E : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v4, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/widget/ExpandableListView;->isShown()Z

    move-result v4

    if-nez v4, :cond_3

    .line 884
    :cond_2
    const-string v3, "VNLibraryExpandableListFragment"

    const-string v4, "onGroupClick return"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 888
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    .line 889
    .local v0, "actionMode":Landroid/view/ActionMode;
    if-eqz v0, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v4}, Landroid/widget/ExpandableListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 890
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v5}, Landroid/widget/ExpandableListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v5

    invoke-virtual {v5, p3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v5

    if-nez v5, :cond_4

    move v2, v3

    :cond_4
    invoke-virtual {v4, p3, v2}, Landroid/widget/ExpandableListView;->setItemChecked(IZ)V

    .line 892
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    if-eqz v2, :cond_5

    .line 893
    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    :cond_5
    move v2, v3

    .line 895
    goto/16 :goto_0

    .line 898
    :cond_6
    sput p3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    .line 899
    iput-wide p4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedID:J

    .line 901
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 902
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getId()I

    move-result v4

    invoke-interface {v2, v4, p4, p5}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onControlButtonSelected(IJ)Z

    .line 905
    :cond_7
    const/16 v2, 0x80

    const/4 v4, 0x0

    invoke-virtual {p2, v2, v4}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 906
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListviewAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {p1, v2}, Landroid/widget/ExpandableListView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 907
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->scrolltoHiddenPlayItem()V

    .line 908
    const-string v2, "VNLibraryExpandableListFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onGroupClick X : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedID:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 909
    goto/16 :goto_0
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v2, 0x0

    .line 954
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCurrentPlayState()I

    move-result v3

    const/16 v4, 0x15

    if-eq v3, v4, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCurrentPlayState()I

    move-result v3

    const/16 v4, 0x18

    if-eq v3, v4, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCurrentPlayState()I

    move-result v3

    const/16 v4, 0x17

    if-ne v3, v4, :cond_1

    .line 955
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 956
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v3}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getActionMode()Landroid/view/ActionMode;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 974
    :cond_1
    :goto_0
    return v2

    .line 960
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 961
    .local v0, "activity":Landroid/app/Activity;
    instance-of v3, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    if-eqz v3, :cond_3

    .line 962
    check-cast v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionBarIsSearched()Z

    move-result v1

    .line 963
    .local v1, "isSearched":Z
    if-eqz v1, :cond_3

    .line 964
    const-string v3, "VNLibraryExpandableListFragment"

    const-string v4, "search mode enable"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 968
    .end local v1    # "isSearched":Z
    :cond_3
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    .line 969
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    invoke-direct {v2, p0, p3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;I)V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    .line 970
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    invoke-virtual {v3, v4}, Landroid/widget/ExpandableListView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->setActionMode(Landroid/view/ActionMode;)V

    .line 972
    :cond_4
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onLabelButtonClicked(J)V
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 999
    const-string v3, "VNLibraryExpandableListFragment"

    const-string v4, "onLabelButtonClicked E"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1000
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->isShown()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isResumed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1001
    const v3, 0x7f0b002d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1002
    .local v2, "title":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;-><init>()V

    .line 1003
    .local v1, "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;
    invoke-virtual {v1, p1, p2, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->SetParameter(JLjava/lang/String;)V

    .line 1004
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1005
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1007
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1010
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v1    # "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;
    .end local v2    # "title":Ljava/lang/String;
    :cond_0
    const-string v3, "VNLibraryExpandableListFragment"

    const-string v4, "onLabelButtonClicked X"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    return-void
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 979
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "onLayoutChange E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 980
    if-eq p5, p9, :cond_0

    if-ne p4, p8, :cond_0

    .line 981
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "onLayoutChange : scrolltoHiddenPlayItem"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->scrolltoHiddenPlayItem()V

    .line 984
    :cond_0
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "onLayoutChange X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 985
    return-void
.end method

.method public onResume()V
    .locals 15

    .prologue
    const/16 v14, 0x15

    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 273
    const-string v9, "VNLibraryExpandableListFragment"

    const-string v10, "onResume() E"

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-eqz v9, :cond_0

    .line 276
    const-string v9, "VNLibraryExpandableListFragment"

    const-string v10, "onResume : notifyDataSetChanged()"

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->notifyDataSetChanged()V

    .line 280
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v7

    .line 282
    .local v7, "listview":Landroid/widget/ListView;
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v9

    if-eqz v9, :cond_7

    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v9}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getActionMode()Landroid/view/ActionMode;

    move-result-object v9

    if-nez v9, :cond_7

    .line 283
    if-eqz v7, :cond_d

    invoke-virtual {v7}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v9

    if-lez v9, :cond_d

    .line 285
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    .line 286
    new-instance v9, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    const/4 v10, -0x1

    iget v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionStates:I

    invoke-direct {v9, p0, v10, v11}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;II)V

    iput-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    .line 288
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    invoke-virtual {v7, v9}, Landroid/widget/ListView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    .line 289
    .local v0, "actionMode":Landroid/view/ActionMode;
    iget v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionStates:I

    const/4 v10, 0x3

    if-eq v9, v10, :cond_1

    iget v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionStates:I

    if-ne v9, v12, :cond_2

    .line 290
    :cond_1
    const v9, 0x7f0e00f9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/view/ActionMode;->setTag(Ljava/lang/Object;)V

    .line 292
    :cond_2
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v9, v0}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->setActionMode(Landroid/view/ActionMode;)V

    .line 295
    :try_start_0
    invoke-virtual {v7}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v9

    invoke-virtual {v9}, Landroid/util/SparseBooleanArray;->clone()Landroid/util/SparseBooleanArray;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mArray:Landroid/util/SparseBooleanArray;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    :goto_0
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mArray:Landroid/util/SparseBooleanArray;

    invoke-virtual {v9}, Landroid/util/SparseBooleanArray;->size()I

    move-result v8

    .line 300
    .local v8, "posSize":I
    const/4 v1, 0x0

    .line 301
    .local v1, "checkedItemPosCnt":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-ge v6, v8, :cond_4

    .line 302
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mArray:Landroid/util/SparseBooleanArray;

    invoke-virtual {v9, v6}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 303
    add-int/lit8 v1, v1, 0x1

    .line 301
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 296
    .end local v1    # "checkedItemPosCnt":I
    .end local v6    # "i":I
    .end local v8    # "posSize":I
    :catch_0
    move-exception v2

    .line 297
    .local v2, "e":Ljava/lang/NullPointerException;
    new-instance v9, Landroid/util/SparseBooleanArray;

    invoke-direct {v9}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mArray:Landroid/util/SparseBooleanArray;

    goto :goto_0

    .line 306
    .end local v2    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "checkedItemPosCnt":I
    .restart local v6    # "i":I
    .restart local v8    # "posSize":I
    :cond_4
    invoke-virtual {v7}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v9

    if-eq v9, v1, :cond_6

    .line 307
    invoke-virtual {v7}, Landroid/widget/ListView;->clearChoices()V

    .line 308
    const/4 v6, 0x0

    :goto_2
    if-ge v6, v8, :cond_6

    .line 309
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mArray:Landroid/util/SparseBooleanArray;

    invoke-virtual {v9, v6}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 310
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mArray:Landroid/util/SparseBooleanArray;

    invoke-virtual {v9, v6}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v9

    invoke-virtual {v7, v9, v12}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 308
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 315
    :cond_6
    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 316
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-eqz v9, :cond_7

    .line 317
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    iget v10, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->cb_size:I

    invoke-virtual {v9, v10}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setCheckBoxSize(I)V

    .line 318
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v9, v12, v13}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setSelectionMode(ZZ)V

    .line 319
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v9, v13}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setCheckBoxSize(I)V

    .line 335
    .end local v0    # "actionMode":Landroid/view/ActionMode;
    .end local v1    # "checkedItemPosCnt":I
    .end local v6    # "i":I
    .end local v8    # "posSize":I
    :cond_7
    :goto_3
    iget-boolean v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mbScrollList:Z

    if-eqz v9, :cond_8

    .line 336
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->scrolltoHiddenPlayItem()V

    .line 337
    iput-boolean v13, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mbScrollList:Z

    .line 341
    :cond_8
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    if-eqz v9, :cond_9

    .line 342
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v9, v10}, Lcom/samsung/android/quickconnect/QuickConnectManager;->registerListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 344
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCurrentPlayState()I

    move-result v9

    if-ne v9, v14, :cond_a

    .line 345
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v9, :cond_a

    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v9, v12}, Landroid/widget/ExpandableListView;->setEnabled(Z)V

    .line 349
    :cond_a
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_b

    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getFragmentControllerState()Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->BOOKMARK:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    if-ne v9, v10, :cond_b

    .line 351
    invoke-virtual {p0, v13}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setFocusListView(Z)V

    .line 354
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCurrentPlayState()I

    move-result v9

    if-eq v9, v14, :cond_c

    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_c

    .line 355
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v10

    iput-wide v10, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedID:J

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->findPosition()V

    .line 357
    iput-boolean v12, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mbScrollList:Z

    .line 360
    :cond_c
    invoke-super {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->onResume()V

    .line 361
    const-string v9, "VNLibraryExpandableListFragment"

    const-string v10, "onResume() X"

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    return-void

    .line 322
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "VNBookmarkListFragment"

    invoke-virtual {v9, v10}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v4

    .line 324
    .local v4, "fragment":Landroid/app/Fragment;
    if-eqz v4, :cond_e

    invoke-virtual {v4}, Landroid/app/Fragment;->isAdded()Z

    move-result v9

    if-nez v9, :cond_7

    .line 325
    :cond_e
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v9}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->finishCustomActionMode()V

    .line 326
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;->findFragment(Landroid/app/FragmentManager;)Landroid/app/Fragment;

    move-result-object v3

    .line 327
    .local v3, "frag":Landroid/app/Fragment;
    instance-of v9, v3, Lcom/sec/android/app/voicenote/common/fragment/VNSelectAllFragment;

    if-eqz v9, :cond_7

    .line 328
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v5

    .line 329
    .local v5, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {v5, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 330
    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_3
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 375
    const-string v1, "VNLibraryExpandableListFragment"

    const-string v2, "onSaveInstanceState E"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v1, :cond_0

    .line 377
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1}, Landroid/widget/ExpandableListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 378
    .local v0, "state":Landroid/os/Parcelable;
    const-string v1, "listview"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 379
    const-string v1, "alpha"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2}, Landroid/widget/ExpandableListView;->getAlpha()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 380
    const-string v1, "trim"

    sget-boolean v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mIsTrimMode:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 381
    const-string v1, "searchtext"

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-eqz v1, :cond_0

    .line 383
    const-string v1, "checkbox_size"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getCheckBoxSize()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 384
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    sget-object v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    if-eqz v1, :cond_0

    .line 385
    const-string v1, "ScrollingState"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    sget-object v2, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->getmScrollingState()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 389
    .end local v0    # "state":Landroid/os/Parcelable;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    if-eqz v1, :cond_1

    .line 390
    const-string v1, "actionmenumode"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->getMenuMode()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 392
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 393
    const-string v1, "VNLibraryExpandableListFragment"

    const-string v2, "onSaveInstanceState X"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    return-void
.end method

.method public onStartActionMenu(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 939
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 940
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getActionMode()Landroid/view/ActionMode;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 950
    :cond_0
    :goto_0
    return-void

    .line 944
    :cond_1
    iput p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionStates:I

    .line 945
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    .line 946
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    const/4 v2, -0x1

    invoke-direct {v1, p0, v2, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;II)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    .line 947
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;

    invoke-virtual {v1, v2}, Landroid/widget/ExpandableListView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    .line 948
    .local v0, "actionMode":Landroid/view/ActionMode;
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v1, v0}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->setActionMode(Landroid/view/ActionMode;)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 368
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 369
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 371
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/voicenote/common/util/ExpandableListFragment;->onStop()V

    .line 372
    return-void
.end method

.method public refreashAnim(Z)V
    .locals 1
    .param p1, "needAnim"    # Z

    .prologue
    .line 1897
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->refreashAnim(Z)V

    .line 1898
    return-void
.end method

.method public refreshList()V
    .locals 1

    .prologue
    .line 2129
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-eqz v0, :cond_0

    .line 2130
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->notifyDataSetChanged()V

    .line 2131
    :cond_0
    return-void
.end method

.method public refreshOptionsMenu()V
    .locals 3

    .prologue
    .line 2064
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    .line 2065
    .local v0, "actionMode":Landroid/view/ActionMode;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v1, :cond_0

    .line 2066
    const-string v1, "VNLibraryExpandableListFragment"

    const-string v2, "refresh action mode"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 2067
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->updateActionModeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/widget/ExpandableListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2068
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->updateActionModeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/widget/ExpandableListView;->post(Ljava/lang/Runnable;)Z

    .line 2070
    :cond_0
    return-void
.end method

.method public resetSelectedPosition(J)V
    .locals 3
    .param p1, "playItemId"    # J

    .prologue
    .line 2114
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-eqz v1, :cond_0

    .line 2115
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->findPosition(Ljava/lang/Object;)I

    move-result v0

    .line 2116
    .local v0, "result":I
    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedPosition:I

    .line 2118
    .end local v0    # "result":I
    :cond_0
    return-void
.end method

.method protected restoreSecretBox(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 1182
    const-string v0, "VNLibraryExpandableListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "restoreSecretBox E : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1195
    const-string v0, "VNLibraryExpandableListFragment"

    const-string v1, "restoreSecretBox X"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1196
    return-void
.end method

.method public setEmptyList()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 620
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setNoItemTopMargin(I)V

    .line 621
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mMainView:Landroid/view/View;

    const v2, 0x7f0e001a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 622
    .local v0, "textview_top":Landroid/widget/TextView;
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 623
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchEmptyview:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 624
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->clearCursor()V

    .line 625
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->notifyDataSetChanged()V

    .line 626
    return-void
.end method

.method public setEnableListView(Z)V
    .locals 1
    .param p1, "bclick"    # Z

    .prologue
    .line 1363
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p1}, Landroid/widget/ExpandableListView;->setEnabled(Z)V

    .line 1364
    :cond_0
    return-void
.end method

.method public setFocusListView(Z)V
    .locals 1
    .param p1, "bfocus"    # Z

    .prologue
    .line 1359
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p1}, Landroid/widget/ExpandableListView;->setFocusable(Z)V

    .line 1360
    :cond_0
    return-void
.end method

.method public setSearchText(Ljava/lang/String;)V
    .locals 0
    .param p1, "searchText"    # Ljava/lang/String;

    .prologue
    .line 1355
    sput-object p1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    .line 1356
    return-void
.end method

.method public showEditDialog(Ljava/lang/String;J)V
    .locals 6
    .param p1, "currntPath"    # Ljava/lang/String;
    .param p2, "checkedItemId"    # J

    .prologue
    .line 2039
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2040
    .local v2, "prevFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 2041
    .local v3, "selelectedFileName":Ljava/lang/String;
    const/16 v4, 0x2e

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 2043
    .local v0, "dotIndex":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    .line 2044
    const/4 v4, 0x0

    invoke-virtual {v3, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 2046
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, p2, p3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->findLabelID(Landroid/content/Context;J)I

    move-result v5

    invoke-static {v4, v3, v5}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->setArguments(ILjava/lang/String;I)Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;

    move-result-object v1

    .line 2047
    .local v1, "fileDialog":Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/voicenote/main/fragment/VNFileNameDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 2048
    return-void
.end method

.method public startSConnect()V
    .locals 24

    .prologue
    .line 1901
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "startSConnect"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 1903
    sget-object v20, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v20, :cond_3

    sget-object v20, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v20

    if-nez v20, :cond_0

    sget-object v20, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v20

    if-eqz v20, :cond_3

    .line 1904
    :cond_0
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "startSConnect in player"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 1905
    sget-object v20, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v6

    .line 1906
    .local v6, "currentPlayId":J
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-static {v0, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    .line 1907
    .local v8, "currentPlayPath":Ljava/lang/String;
    if-nez v8, :cond_1

    .line 1908
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "currentPlayPath is null"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 2036
    .end local v6    # "currentPlayId":J
    .end local v8    # "currentPlayPath":Ljava/lang/String;
    :goto_0
    return-void

    .line 1911
    .restart local v6    # "currentPlayId":J
    .restart local v8    # "currentPlayPath":Ljava/lang/String;
    :cond_1
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 1912
    .local v19, "uriArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/16 v18, 0x0

    .line 1913
    .local v18, "uri":Landroid/net/Uri;
    move-object/from16 v16, v8

    .line 1914
    .local v16, "path":Ljava/lang/String;
    new-instance v20, Ljava/io/File;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v20 .. v20}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v18

    .line 1915
    if-nez v18, :cond_2

    .line 1916
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "uri is null"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1919
    :cond_2
    new-instance v20, Ljava/io/File;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v20 .. v20}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1921
    new-instance v14, Landroid/content/Intent;

    invoke-direct {v14}, Landroid/content/Intent;-><init>()V

    .line 1922
    .local v14, "intent":Landroid/content/Intent;
    const-string v20, "com.samsung.android.sconnect.START"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1923
    const-string v20, "android.intent.extra.STREAM"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1925
    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1930
    :goto_1
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "startSConnect end"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1926
    :catch_0
    move-exception v9

    .line 1927
    .local v9, "e":Landroid/content/ActivityNotFoundException;
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "ActivityNotFoundException"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1933
    .end local v6    # "currentPlayId":J
    .end local v8    # "currentPlayPath":Ljava/lang/String;
    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    .end local v14    # "intent":Landroid/content/Intent;
    .end local v16    # "path":Ljava/lang/String;
    .end local v18    # "uri":Landroid/net/Uri;
    .end local v19    # "uriArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v10

    check-cast v10, Landroid/widget/ExpandableListView;

    .line 1934
    .local v10, "expandableListView":Landroid/widget/ExpandableListView;
    if-nez v10, :cond_4

    .line 1935
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "expandableListView is null"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 1936
    new-instance v14, Landroid/content/Intent;

    invoke-direct {v14}, Landroid/content/Intent;-><init>()V

    .line 1937
    .restart local v14    # "intent":Landroid/content/Intent;
    const-string v20, "com.samsung.android.sconnect.START"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1939
    :try_start_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1940
    :catch_1
    move-exception v9

    .line 1941
    .restart local v9    # "e":Landroid/content/ActivityNotFoundException;
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "ActivityNotFoundException"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1946
    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    .end local v14    # "intent":Landroid/content/Intent;
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;

    move-result-object v3

    .line 1947
    .local v3, "adapter":Landroid/widget/ExpandableListAdapter;
    if-nez v3, :cond_5

    .line 1948
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "adapter is null"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 1949
    new-instance v14, Landroid/content/Intent;

    invoke-direct {v14}, Landroid/content/Intent;-><init>()V

    .line 1950
    .restart local v14    # "intent":Landroid/content/Intent;
    const-string v20, "com.samsung.android.sconnect.START"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1952
    :try_start_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 1953
    :catch_2
    move-exception v9

    .line 1954
    .restart local v9    # "e":Landroid/content/ActivityNotFoundException;
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "ActivityNotFoundException"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1959
    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    .end local v14    # "intent":Landroid/content/Intent;
    :cond_5
    invoke-virtual {v10}, Landroid/widget/ExpandableListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v4

    .line 1960
    .local v4, "arr":Landroid/util/SparseBooleanArray;
    if-nez v4, :cond_6

    .line 1961
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "arr is null"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 1962
    new-instance v14, Landroid/content/Intent;

    invoke-direct {v14}, Landroid/content/Intent;-><init>()V

    .line 1963
    .restart local v14    # "intent":Landroid/content/Intent;
    const-string v20, "com.samsung.android.sconnect.START"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1965
    :try_start_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 1966
    :catch_3
    move-exception v9

    .line 1967
    .restart local v9    # "e":Landroid/content/ActivityNotFoundException;
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "ActivityNotFoundException"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1972
    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    .end local v14    # "intent":Landroid/content/Intent;
    :cond_6
    invoke-virtual {v4}, Landroid/util/SparseBooleanArray;->size()I

    move-result v15

    .line 1973
    .local v15, "len":I
    if-nez v15, :cond_7

    .line 1974
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "len is 0"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 1975
    new-instance v14, Landroid/content/Intent;

    invoke-direct {v14}, Landroid/content/Intent;-><init>()V

    .line 1976
    .restart local v14    # "intent":Landroid/content/Intent;
    const-string v20, "com.samsung.android.sconnect.START"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1978
    :try_start_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_4
    .catch Landroid/content/ActivityNotFoundException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    .line 1979
    :catch_4
    move-exception v9

    .line 1980
    .restart local v9    # "e":Landroid/content/ActivityNotFoundException;
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "ActivityNotFoundException"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1985
    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    .end local v14    # "intent":Landroid/content/Intent;
    :cond_7
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1986
    .local v5, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const-wide/16 v12, 0x0

    .line 1987
    .local v12, "id":J
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    if-ge v11, v15, :cond_9

    .line 1988
    invoke-virtual {v4, v11}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 1989
    invoke-virtual {v4, v11}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v3, v0}, Landroid/widget/ExpandableListAdapter;->getGroupId(I)J

    move-result-wide v12

    .line 1990
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1987
    :cond_8
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 1994
    :cond_9
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 1995
    .local v17, "size":I
    if-nez v17, :cond_a

    .line 1996
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "size is 0"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 1997
    new-instance v14, Landroid/content/Intent;

    invoke-direct {v14}, Landroid/content/Intent;-><init>()V

    .line 1998
    .restart local v14    # "intent":Landroid/content/Intent;
    const-string v20, "com.samsung.android.sconnect.START"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2000
    :try_start_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_5
    .catch Landroid/content/ActivityNotFoundException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    .line 2001
    :catch_5
    move-exception v9

    .line 2002
    .restart local v9    # "e":Landroid/content/ActivityNotFoundException;
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "ActivityNotFoundException"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2007
    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    .end local v14    # "intent":Landroid/content/Intent;
    :cond_a
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 2008
    .restart local v19    # "uriArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/16 v18, 0x0

    .line 2009
    .restart local v18    # "uri":Landroid/net/Uri;
    const/4 v11, 0x0

    :goto_3
    move/from16 v0, v17

    if-ge v11, v0, :cond_d

    .line 2010
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v21

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Long;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    invoke-static/range {v21 .. v23}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v16

    .line 2011
    .restart local v16    # "path":Ljava/lang/String;
    if-eqz v16, :cond_b

    .line 2012
    new-instance v20, Ljava/io/File;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v20 .. v20}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v18

    .line 2013
    if-nez v18, :cond_c

    .line 2009
    :cond_b
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 2016
    :cond_c
    new-instance v20, Ljava/io/File;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v20 .. v20}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2020
    .end local v16    # "path":Ljava/lang/String;
    :cond_d
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v20

    if-lez v20, :cond_e

    .line 2021
    new-instance v14, Landroid/content/Intent;

    invoke-direct {v14}, Landroid/content/Intent;-><init>()V

    .line 2022
    .restart local v14    # "intent":Landroid/content/Intent;
    const-string v20, "com.samsung.android.sconnect.START"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2023
    const-string v20, "android.intent.extra.STREAM"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2025
    :try_start_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_6
    .catch Landroid/content/ActivityNotFoundException; {:try_start_6 .. :try_end_6} :catch_6

    .line 2030
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActionMode()Landroid/view/ActionMode;

    move-result-object v2

    .line 2031
    .local v2, "actionMode":Landroid/view/ActionMode;
    if-eqz v2, :cond_e

    .line 2032
    invoke-virtual {v2}, Landroid/view/ActionMode;->finish()V

    .line 2035
    .end local v2    # "actionMode":Landroid/view/ActionMode;
    .end local v14    # "intent":Landroid/content/Intent;
    :cond_e
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "startSConnect end"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2026
    .restart local v14    # "intent":Landroid/content/Intent;
    :catch_6
    move-exception v9

    .line 2027
    .restart local v9    # "e":Landroid/content/ActivityNotFoundException;
    const-string v20, "VNLibraryExpandableListFragment"

    const-string v21, "ActivityNotFoundException"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5
.end method

.method public updateCachedBookmarks(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 1
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1108
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1109
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->updateCachedBookmarks(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 1111
    :cond_0
    return-void
.end method

.method public updateState(Z)V
    .locals 11
    .param p1, "bRequeryCursor"    # Z

    .prologue
    const/16 v10, 0x65

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1044
    const-string v5, "VNLibraryExpandableListFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updateState E : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    if-nez v5, :cond_1

    .line 1105
    :cond_0
    :goto_0
    return-void

    .line 1048
    :cond_1
    if-ne p1, v9, :cond_7

    .line 1049
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    .line 1050
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setEmptyView()V

    .line 1051
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->listBinding()Z

    .line 1052
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1055
    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v5, :cond_6

    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v5}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getActionMode()Landroid/view/ActionMode;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 1056
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mArray:Landroid/util/SparseBooleanArray;

    if-eqz v5, :cond_5

    .line 1057
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v5}, Landroid/widget/ExpandableListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v5

    invoke-virtual {v5}, Landroid/util/SparseBooleanArray;->clone()Landroid/util/SparseBooleanArray;

    move-result-object v0

    .line 1058
    .local v0, "arr":Landroid/util/SparseBooleanArray;
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v4

    .line 1059
    .local v4, "posSize":I
    const/4 v1, 0x0

    .line 1060
    .local v1, "checkedItemPosCnt":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_3

    .line 1061
    invoke-virtual {v0, v2}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1062
    add-int/lit8 v1, v1, 0x1

    .line 1060
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1065
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v5}, Landroid/widget/ExpandableListView;->getCheckedItemCount()I

    move-result v5

    if-eq v5, v1, :cond_5

    .line 1066
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v5}, Landroid/widget/ExpandableListView;->clearChoices()V

    .line 1067
    const/4 v2, 0x0

    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mArray:Landroid/util/SparseBooleanArray;

    invoke-virtual {v5}, Landroid/util/SparseBooleanArray;->size()I

    move-result v5

    if-ge v2, v5, :cond_5

    .line 1068
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mArray:Landroid/util/SparseBooleanArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1069
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mArray:Landroid/util/SparseBooleanArray;

    invoke-virtual {v6, v2}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v6

    invoke-virtual {v5, v6, v9}, Landroid/widget/ExpandableListView;->setItemChecked(IZ)V

    .line 1067
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1074
    .end local v0    # "arr":Landroid/util/SparseBooleanArray;
    .end local v1    # "checkedItemPosCnt":I
    .end local v2    # "i":I
    .end local v4    # "posSize":I
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getCheckBoxSize()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setCheckBoxSize(I)V

    .line 1075
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v5, v9, v8}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setSelectionMode(ZZ)V

    .line 1076
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setCheckBoxSize(I)V

    .line 1095
    :cond_6
    :goto_3
    iget-wide v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSelectedID:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gez v5, :cond_b

    .line 1096
    const-string v5, "VNLibraryExpandableListFragment"

    const-string v6, "updateState : mSelectedID is invalid"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1079
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_8

    .line 1080
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->requery()Z

    .line 1082
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    if-eqz v5, :cond_9

    .line 1083
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->notifyDataSetChanged()V

    .line 1085
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchEmptyview:Landroid/view/View;

    if-eqz v5, :cond_6

    .line 1086
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_a

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-nez v5, :cond_a

    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchText:Ljava/lang/String;

    if-eqz v5, :cond_a

    .line 1087
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09002b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setNoItemTopMargin(I)V

    .line 1088
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchEmptyview:Landroid/view/View;

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 1090
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mSearchEmptyview:Landroid/view/View;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 1100
    :cond_b
    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    .line 1101
    .local v3, "msg":Landroid/os/Message;
    iput v10, v3, Landroid/os/Message;->what:I

    .line 1102
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 1103
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x64

    invoke-virtual {v5, v3, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1104
    const-string v5, "VNLibraryExpandableListFragment"

    const-string v6, "updateState X"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
