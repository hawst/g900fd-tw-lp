.class Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;
.super Lcom/samsung/android/privatemode/IPrivateModeClient$Stub;
.source "VNActionModeListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

.field final synthetic val$array:Ljava/util/ArrayList;

.field final synthetic val$fragment:Landroid/app/Fragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;Ljava/util/ArrayList;Landroid/app/Fragment;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;->this$0:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    iput-object p2, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;->val$array:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;->val$fragment:Landroid/app/Fragment;

    invoke-direct {p0}, Lcom/samsung/android/privatemode/IPrivateModeClient$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onStateChange(II)V
    .locals 5
    .param p1, "state"    # I
    .param p2, "extInfo"    # I

    .prologue
    const/4 v4, 0x0

    .line 390
    if-nez p1, :cond_1

    .line 391
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;->this$0:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    if-eqz v2, :cond_0

    .line 392
    const-string v2, "VNLibraryActionModeListener"

    const-string v3, "PrivateModeManager.PREPARED"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;->this$0:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;->this$0:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    iget-object v3, v3, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    invoke-virtual {v3, p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->registerClient(Lcom/samsung/android/privatemode/IPrivateModeClient;)Landroid/os/IBinder;

    move-result-object v3

    # setter for: Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->access$002(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;Landroid/os/IBinder;)Landroid/os/IBinder;

    .line 394
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;->this$0:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->access$000(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;)Landroid/os/IBinder;

    move-result-object v2

    if-nez v2, :cond_0

    .line 395
    const-string v2, "VNLibraryActionModeListener"

    const-string v3, "PrivateModeClient is not registered!!"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 398
    :cond_1
    const/4 v2, 0x1

    if-ne p1, v2, :cond_2

    .line 399
    const-string v2, "VNLibraryActionModeListener"

    const-string v3, "PrivateModeManager.MOUNTED"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;->this$0:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;->this$0:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->access$000(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->newInstance(Lcom/samsung/android/privatemode/PrivateModeManager;Landroid/os/IBinder;)Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    move-result-object v1

    .line 401
    .local v1, "frag":Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 402
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v2, "ids"

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;->val$array:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 403
    const-string v2, "mode"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 404
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;->val$fragment:Landroid/app/Fragment;

    invoke-virtual {v2}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_FILE"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 405
    .end local v0    # "arguments":Landroid/os/Bundle;
    .end local v1    # "frag":Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    :cond_2
    const/4 v2, 0x3

    if-ne p1, v2, :cond_0

    .line 406
    const-string v2, "VNLibraryActionModeListener"

    const-string v3, "PrivateModeManager.CANCELLED"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    sput-boolean v4, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->EXTERNAL_STOP:Z

    .line 408
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;->this$0:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->access$000(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;)Landroid/os/IBinder;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 409
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;->this$0:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;->this$0:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->access$000(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;)Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    goto :goto_0
.end method
