.class public final Lcom/sec/android/app/voicenote/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final action_item_background:I = 0x7f020000

.field public static final actionbar_check_box_animator_list:I = 0x7f020001

.field public static final actionbar_dropdown_selector:I = 0x7f020002

.field public static final actionbar_icon_add:I = 0x7f020003

.field public static final actionbar_icon_categories_edit:I = 0x7f020004

.field public static final actionbar_icon_categories_edit_dim:I = 0x7f020005

.field public static final actionbar_icon_category:I = 0x7f020006

.field public static final actionbar_icon_category_disable:I = 0x7f020007

.field public static final actionbar_icon_category_menu:I = 0x7f020008

.field public static final actionbar_icon_category_menu_dim:I = 0x7f020009

.field public static final actionbar_icon_category_plus:I = 0x7f02000a

.field public static final actionbar_icon_category_plus_dim:I = 0x7f02000b

.field public static final actionbar_icon_category_recycle:I = 0x7f02000c

.field public static final actionbar_icon_category_recycle_dim:I = 0x7f02000d

.field public static final actionbar_icon_category_share:I = 0x7f02000e

.field public static final actionbar_icon_category_share_dim:I = 0x7f02000f

.field public static final actionbar_icon_edit:I = 0x7f020010

.field public static final actionbar_icon_edit_dim:I = 0x7f020011

.field public static final actionbar_icon_edit_rename:I = 0x7f020012

.field public static final actionbar_icon_edit_rename_dim:I = 0x7f020013

.field public static final actionbar_icon_menu_btn:I = 0x7f020014

.field public static final actionbar_icon_search:I = 0x7f020015

.field public static final actionbar_icon_search_btn:I = 0x7f020016

.field public static final actionbar_icon_search_disable:I = 0x7f020017

.field public static final actionbar_icon_stt:I = 0x7f020018

.field public static final category_label:I = 0x7f020019

.field public static final components_icon_a_b:I = 0x7f02001a

.field public static final components_icon_a_b_focus:I = 0x7f02001b

.field public static final components_icon_a_b_normal:I = 0x7f02001c

.field public static final components_icon_a_b_press:I = 0x7f02001d

.field public static final components_icon_bookmark:I = 0x7f02001e

.field public static final components_icon_bookmark_normal:I = 0x7f02001f

.field public static final components_icon_bookmark_normal_focus:I = 0x7f020020

.field public static final components_icon_bookmark_normal_press:I = 0x7f020021

.field public static final components_icon_cut:I = 0x7f020022

.field public static final components_icon_cut_focus:I = 0x7f020023

.field public static final components_icon_cut_normal:I = 0x7f020024

.field public static final components_icon_cut_press:I = 0x7f020025

.field public static final components_icon_skip_silence:I = 0x7f020026

.field public static final components_icon_skip_silence_focus:I = 0x7f020027

.field public static final components_icon_skip_silence_normal:I = 0x7f020028

.field public static final components_icon_skip_silence_on:I = 0x7f020029

.field public static final components_icon_skip_silence_press:I = 0x7f02002a

.field public static final components_icon_text:I = 0x7f02002b

.field public static final components_icon_x05:I = 0x7f02002c

.field public static final components_icon_x05_focus:I = 0x7f02002d

.field public static final components_icon_x05_normal:I = 0x7f02002e

.field public static final components_icon_x05_press:I = 0x7f02002f

.field public static final components_icon_x10:I = 0x7f020030

.field public static final components_icon_x10_focus:I = 0x7f020031

.field public static final components_icon_x10_normal:I = 0x7f020032

.field public static final components_icon_x10_press:I = 0x7f020033

.field public static final components_icon_x15:I = 0x7f020034

.field public static final components_icon_x15_focus:I = 0x7f020035

.field public static final components_icon_x15_normal:I = 0x7f020036

.field public static final components_icon_x15_press:I = 0x7f020037

.field public static final components_icon_x20:I = 0x7f020038

.field public static final components_icon_x20_focus:I = 0x7f020039

.field public static final components_icon_x20_normal:I = 0x7f02003a

.field public static final components_icon_x20_press:I = 0x7f02003b

.field public static final components_icon_x25:I = 0x7f02003c

.field public static final components_icon_x30:I = 0x7f02003d

.field public static final custom_ab_transparent_light_holo:I = 0x7f02003e

.field public static final dark_no_item_v:I = 0x7f02003f

.field public static final easymode_recorded_list_progress_bg:I = 0x7f020040

.field public static final easymode_recorded_list_progress_primary:I = 0x7f020041

.field public static final easymode_recorded_list_progress_secondary:I = 0x7f020042

.field public static final easymode_recorded_list_scrubber_control_focus:I = 0x7f020043

.field public static final easymode_recorded_list_scrubber_control_normal:I = 0x7f020044

.field public static final easymode_recorded_list_scrubber_control_press:I = 0x7f020045

.field public static final edit_label_color_area:I = 0x7f020046

.field public static final edit_label_color_btn:I = 0x7f020047

.field public static final edit_label_color_press:I = 0x7f020048

.field public static final edit_label_color_shadow:I = 0x7f020049

.field public static final edit_label_popup_bg:I = 0x7f02004a

.field public static final edit_label_text_field:I = 0x7f02004b

.field public static final edit_stt_nbest_selector:I = 0x7f02004c

.field public static final f_airview_popup_picker_bg:I = 0x7f02004d

.field public static final f_airview_popup_picker_bg_w:I = 0x7f02004e

.field public static final header_button_icon_cancel_02_selector:I = 0x7f02004f

.field public static final header_button_icon_check_selector:I = 0x7f020050

.field public static final header_button_icon_knox_move_home:I = 0x7f020051

.field public static final header_button_icon_knox_move_home_disable:I = 0x7f020052

.field public static final header_button_icon_move_knox:I = 0x7f020053

.field public static final header_button_icon_move_knox_disable:I = 0x7f020054

.field public static final homescreen_icon_voicerec:I = 0x7f020055

.field public static final hover_popup_black_bg:I = 0x7f020056

.field public static final hover_video_bg_c:I = 0x7f020057

.field public static final hover_video_bg_l:I = 0x7f020058

.field public static final hover_video_bg_r:I = 0x7f020059

.field public static final ic_btn_round_more:I = 0x7f02005a

.field public static final ic_mp_sd_card:I = 0x7f02005b

.field public static final icon_set_as_alarm_tome:I = 0x7f02005c

.field public static final icon_set_as_caller_ringtone:I = 0x7f02005d

.field public static final icon_set_as_phone_ringtone:I = 0x7f02005e

.field public static final library_list_view_bg:I = 0x7f02005f

.field public static final library_list_view_bg_focus:I = 0x7f020060

.field public static final library_list_view_bg_press:I = 0x7f020061

.field public static final library_list_view_color_label:I = 0x7f020062

.field public static final library_list_view_color_label_default:I = 0x7f020063

.field public static final library_list_view_vol_icon_01:I = 0x7f020064

.field public static final library_list_view_vol_icon_02:I = 0x7f020065

.field public static final library_list_view_vol_icon_03:I = 0x7f020066

.field public static final lock_voice_note_btn_cancel:I = 0x7f020067

.field public static final lock_voice_note_btn_pause:I = 0x7f020068

.field public static final lock_voice_note_btn_record:I = 0x7f020069

.field public static final lock_voice_note_btn_stop:I = 0x7f02006a

.field public static final lock_voice_recorder_btn_cancel:I = 0x7f02006b

.field public static final lock_voice_recorder_btn_cancel_focus:I = 0x7f02006c

.field public static final lock_voice_recorder_btn_cancel_press:I = 0x7f02006d

.field public static final lock_voice_recorder_btn_pause:I = 0x7f02006e

.field public static final lock_voice_recorder_btn_pause_focused:I = 0x7f02006f

.field public static final lock_voice_recorder_btn_pause_pressed:I = 0x7f020070

.field public static final lock_voice_recorder_btn_record:I = 0x7f020071

.field public static final lock_voice_recorder_btn_record_focus:I = 0x7f020072

.field public static final lock_voice_recorder_btn_record_press:I = 0x7f020073

.field public static final lock_voice_recorder_btn_stop:I = 0x7f020074

.field public static final lock_voice_recorder_btn_stop_focus:I = 0x7f020075

.field public static final lock_voice_recorder_btn_stop_press:I = 0x7f020076

.field public static final lock_voice_recorder_off:I = 0x7f020077

.field public static final lock_voice_recorder_on:I = 0x7f020078

.field public static final logo_image_setting_add:I = 0x7f020079

.field public static final logo_image_setting_add_btn:I = 0x7f02007a

.field public static final logo_image_setting_add_focus:I = 0x7f02007b

.field public static final logo_image_setting_add_press:I = 0x7f02007c

.field public static final mic_speaker_masking:I = 0x7f02007d

.field public static final no_item_bg_bmp:I = 0x7f02007e

.field public static final option_menu_bookmark:I = 0x7f02007f

.field public static final option_menu_change_label:I = 0x7f020080

.field public static final option_menu_delete:I = 0x7f020081

.field public static final option_menu_end:I = 0x7f020082

.field public static final option_menu_manage_label:I = 0x7f020083

.field public static final option_menu_move_secret:I = 0x7f020084

.field public static final option_menu_remove_secret:I = 0x7f020085

.field public static final option_menu_rename:I = 0x7f020086

.field public static final option_menu_settings:I = 0x7f020087

.field public static final option_menu_share_via:I = 0x7f020088

.field public static final option_menu_sort_by:I = 0x7f020089

.field public static final panel_text_color:I = 0x7f02008a

.field public static final panel_text_stt_color:I = 0x7f02008b

.field public static final personal_list_ic:I = 0x7f02008c

.field public static final popup_color_label:I = 0x7f02008d

.field public static final popup_color_label_default:I = 0x7f02008e

.field public static final popup_dropdown_color_label:I = 0x7f02008f

.field public static final popup_dropdown_color_label_default:I = 0x7f020090

.field public static final popup_interview_image_01:I = 0x7f020091

.field public static final popup_interview_image_02:I = 0x7f020092

.field public static final popup_interview_image_03:I = 0x7f020093

.field public static final quick_panel_button_btn:I = 0x7f020094

.field public static final quick_panel_icon_call_thumbnail_focus:I = 0x7f020095

.field public static final quick_panel_icon_call_thumbnail_press:I = 0x7f020096

.field public static final quick_panel_icon_cancel_btn:I = 0x7f020097

.field public static final quick_panel_icon_mini_rec:I = 0x7f020098

.field public static final quick_panel_icon_pause_40_btn:I = 0x7f020099

.field public static final quick_panel_icon_pause_btn:I = 0x7f02009a

.field public static final quick_panel_icon_play_btn:I = 0x7f02009b

.field public static final quick_panel_icon_rec:I = 0x7f02009c

.field public static final quick_panel_icon_rec_40_btn:I = 0x7f02009d

.field public static final quick_panel_icon_rec_btn:I = 0x7f02009e

.field public static final quick_panel_icon_stop_btn:I = 0x7f02009f

.field public static final quick_panel_icon_stop_btn_40:I = 0x7f0200a0

.field public static final quick_panel_icon_voice_note:I = 0x7f0200a1

.field public static final quick_panel_icon_voice_recorder_btn:I = 0x7f0200a2

.field public static final quick_panel_mini_conrtoller_bg:I = 0x7f0200a3

.field public static final quick_panel_music_pause_40:I = 0x7f0200a4

.field public static final quick_panel_music_pause_50:I = 0x7f0200a5

.field public static final quick_panel_music_pause_dim_50:I = 0x7f0200a6

.field public static final quick_panel_music_pause_dim_focus_50:I = 0x7f0200a7

.field public static final quick_panel_music_pause_focus_40:I = 0x7f0200a8

.field public static final quick_panel_music_pause_focus_50:I = 0x7f0200a9

.field public static final quick_panel_music_pause_press_40:I = 0x7f0200aa

.field public static final quick_panel_music_pause_press_50:I = 0x7f0200ab

.field public static final quick_panel_music_play_40:I = 0x7f0200ac

.field public static final quick_panel_music_play_dim_40:I = 0x7f0200ad

.field public static final quick_panel_music_play_dim_focus_40:I = 0x7f0200ae

.field public static final quick_panel_music_play_focus_40:I = 0x7f0200af

.field public static final quick_panel_music_play_press_40:I = 0x7f0200b0

.field public static final quick_panel_music_rec_40:I = 0x7f0200b1

.field public static final quick_panel_music_rec_50:I = 0x7f0200b2

.field public static final quick_panel_music_rec_dim_50:I = 0x7f0200b3

.field public static final quick_panel_music_rec_dim_focus_40:I = 0x7f0200b4

.field public static final quick_panel_music_rec_dim_focus_50:I = 0x7f0200b5

.field public static final quick_panel_music_rec_focus_40:I = 0x7f0200b6

.field public static final quick_panel_music_rec_focus_50:I = 0x7f0200b7

.field public static final quick_panel_music_rec_press_40:I = 0x7f0200b8

.field public static final quick_panel_music_rec_press_50:I = 0x7f0200b9

.field public static final quick_panel_player_cancel:I = 0x7f0200ba

.field public static final quick_panel_player_cancel_focus:I = 0x7f0200bb

.field public static final quick_panel_player_cancel_press:I = 0x7f0200bc

.field public static final quick_panel_player_stop_40:I = 0x7f0200bd

.field public static final quick_panel_player_stop_50:I = 0x7f0200be

.field public static final quick_panel_player_stop_dim_50:I = 0x7f0200bf

.field public static final quick_panel_player_stop_focus_40:I = 0x7f0200c0

.field public static final quick_panel_player_stop_focus_50:I = 0x7f0200c1

.field public static final quick_panel_player_stop_press_40:I = 0x7f0200c2

.field public static final quick_panel_player_stop_press_50:I = 0x7f0200c3

.field public static final quick_panel_round_bg:I = 0x7f0200c4

.field public static final quick_panel_stat_sys_voice:I = 0x7f0200c5

.field public static final s_help_cancel_btn_focus:I = 0x7f0200c6

.field public static final s_help_cancel_btn_nor:I = 0x7f0200c7

.field public static final s_help_cancel_btn_press:I = 0x7f0200c8

.field public static final select_bestdialog_cancel:I = 0x7f0200c9

.field public static final setting_my_profile_text01_btn:I = 0x7f0200ca

.field public static final setting_my_profile_text01_btn_dark:I = 0x7f0200cb

.field public static final setting_my_profile_text01_btn_dark_focus:I = 0x7f0200cc

.field public static final setting_my_profile_text01_btn_dark_press:I = 0x7f0200cd

.field public static final setting_my_profile_text02_btn:I = 0x7f0200ce

.field public static final setting_my_profile_text02_btn_dark:I = 0x7f0200cf

.field public static final setting_my_profile_text02_btn_dark_focus:I = 0x7f0200d0

.field public static final setting_my_profile_text02_btn_dark_press:I = 0x7f0200d1

.field public static final settings_color_palette:I = 0x7f0200d2

.field public static final settings_color_palette_picker:I = 0x7f0200d3

.field public static final settings_color_select_box:I = 0x7f0200d4

.field public static final settings_color_select_dark_bg:I = 0x7f0200d5

.field public static final settings_colorchip:I = 0x7f0200d6

.field public static final settings_colorchip_btn:I = 0x7f0200d7

.field public static final settings_colorchip_none:I = 0x7f0200d8

.field public static final settings_colorchip_palette:I = 0x7f0200d9

.field public static final settings_colorchip_shadow:I = 0x7f0200da

.field public static final stat_notify_voicerecorder:I = 0x7f0200db

.field public static final stat_pause:I = 0x7f0200dc

.field public static final stat_play:I = 0x7f0200dd

.field public static final stat_sys_voice:I = 0x7f0200de

.field public static final stt_icon_recording_off:I = 0x7f0200df

.field public static final stt_icon_recording_on:I = 0x7f0200e0

.field public static final stt_image_stt_mode:I = 0x7f0200e1

.field public static final stt_processing_ani_01:I = 0x7f0200e2

.field public static final stt_processing_ani_02:I = 0x7f0200e3

.field public static final stt_processing_ani_03:I = 0x7f0200e4

.field public static final stt_processing_icon:I = 0x7f0200e5

.field public static final stt_title_icon_menu:I = 0x7f0200e6

.field public static final stt_title_icon_menu_focus:I = 0x7f0200e7

.field public static final stt_title_icon_menu_press:I = 0x7f0200e8

.field public static final toolbar_help_cancel:I = 0x7f0200e9

.field public static final toolbar_help_cancel_focus:I = 0x7f0200ea

.field public static final toolbar_icon_bookmark:I = 0x7f0200eb

.field public static final trim_seekbar_progressive:I = 0x7f0200ec

.field public static final tw_ab_transparent_dark_holo:I = 0x7f0200ed

.field public static final tw_action_bar_icon_cancel_02_pressed_holo_dark:I = 0x7f0200ee

.field public static final tw_action_bar_icon_check_disabled_holo_dark:I = 0x7f0200ef

.field public static final tw_action_bar_icon_check_pressed_holo_dark:I = 0x7f0200f0

.field public static final tw_action_bar_icon_delete_mtrl:I = 0x7f0200f1

.field public static final tw_action_bar_icon_search_mtrl:I = 0x7f0200f2

.field public static final tw_action_bar_icon_share_mtrl:I = 0x7f0200f3

.field public static final tw_action_item_background_focused_holo_dark:I = 0x7f0200f4

.field public static final tw_action_item_background_pressed_holo_dark:I = 0x7f0200f5

.field public static final tw_action_item_background_selected_holo_dark:I = 0x7f0200f6

.field public static final tw_btn_check_to_on_mtrl_000:I = 0x7f0200f7

.field public static final tw_btn_check_to_on_mtrl_001:I = 0x7f0200f8

.field public static final tw_btn_check_to_on_mtrl_002:I = 0x7f0200f9

.field public static final tw_btn_check_to_on_mtrl_003:I = 0x7f0200fa

.field public static final tw_btn_check_to_on_mtrl_004:I = 0x7f0200fb

.field public static final tw_btn_check_to_on_mtrl_005:I = 0x7f0200fc

.field public static final tw_btn_check_to_on_mtrl_006:I = 0x7f0200fd

.field public static final tw_btn_check_to_on_mtrl_007:I = 0x7f0200fe

.field public static final tw_btn_check_to_on_mtrl_008:I = 0x7f0200ff

.field public static final tw_btn_check_to_on_mtrl_009:I = 0x7f020100

.field public static final tw_btn_check_to_on_mtrl_010:I = 0x7f020101

.field public static final tw_btn_check_to_on_mtrl_011:I = 0x7f020102

.field public static final tw_btn_check_to_on_mtrl_012:I = 0x7f020103

.field public static final tw_btn_check_to_on_mtrl_013:I = 0x7f020104

.field public static final tw_btn_check_to_on_mtrl_014:I = 0x7f020105

.field public static final tw_btn_check_to_on_mtrl_015:I = 0x7f020106

.field public static final tw_btn_next_depth_disabled_focused_holo_dark:I = 0x7f020107

.field public static final tw_btn_next_depth_disabled_holo_dark:I = 0x7f020108

.field public static final tw_btn_next_depth_focused_holo_dark:I = 0x7f020109

.field public static final tw_btn_next_depth_holo_dark:I = 0x7f02010a

.field public static final tw_btn_next_depth_pressed_holo_dark:I = 0x7f02010b

.field public static final tw_buttonbarbutton_selector:I = 0x7f02010c

.field public static final tw_buttonbarbutton_selector_default_holo_dark:I = 0x7f02010d

.field public static final tw_buttonbarbutton_selector_focused_holo_dark:I = 0x7f02010e

.field public static final tw_buttonbarbutton_selector_pressed_holo_dark:I = 0x7f02010f

.field public static final tw_buttonbarbutton_selector_selected_holo_dark:I = 0x7f020110

.field public static final tw_dialog_bottom_medium_holo_dark:I = 0x7f020111

.field public static final tw_divider_ab_holo_dark:I = 0x7f020112

.field public static final tw_ic_ab_back_holo_dark:I = 0x7f020113

.field public static final tw_ic_menu_moreoverflow_mtrl:I = 0x7f020114

.field public static final tw_list_icon_create_disabled_holo_dark:I = 0x7f020115

.field public static final tw_list_icon_create_holo_dark:I = 0x7f020116

.field public static final tw_list_icon_create_mtrl:I = 0x7f020117

.field public static final tw_list_pressed_dark:I = 0x7f020118

.field public static final tw_list_pressed_holo_dark:I = 0x7f020119

.field public static final tw_list_selected_dark:I = 0x7f02011a

.field public static final tw_menu_ab_dropdown_panel_holo_dark:I = 0x7f02011b

.field public static final tw_popup_horizontal_divider_holo_dark:I = 0x7f02011c

.field public static final tw_scrollbar_holo_dark:I = 0x7f02011d

.field public static final tw_spinner_ab_default_holo_am:I = 0x7f02011e

.field public static final tw_spinner_ab_default_holo_dark_am:I = 0x7f02011f

.field public static final tw_spinner_ab_disabled_holo_am:I = 0x7f020120

.field public static final tw_spinner_ab_focused_holo_am:I = 0x7f020121

.field public static final tw_spinner_ab_focused_holo_dark_am:I = 0x7f020122

.field public static final tw_spinner_ab_pressed_holo_am:I = 0x7f020123

.field public static final tw_spinner_ab_pressed_holo_dark_am:I = 0x7f020124

.field public static final tw_spinner_selector:I = 0x7f020125

.field public static final tw_sub_action_bar_bg_holo_dark:I = 0x7f020126

.field public static final voice_delete_allitem_bg:I = 0x7f020127

.field public static final voice_memo_stt_bookmark:I = 0x7f020128

.field public static final voice_memo_trim_components_left_normal:I = 0x7f020129

.field public static final voice_memo_trim_components_right_normal:I = 0x7f02012a

.field public static final voice_note_atob:I = 0x7f02012b

.field public static final voice_note_back_easy:I = 0x7f02012c

.field public static final voice_note_book_mark:I = 0x7f02012d

.field public static final voice_note_book_mark_focus:I = 0x7f02012e

.field public static final voice_note_book_mark_normal:I = 0x7f02012f

.field public static final voice_note_book_mark_press:I = 0x7f020130

.field public static final voice_note_btn_close:I = 0x7f020131

.field public static final voice_note_btn_close_focus:I = 0x7f020132

.field public static final voice_note_btn_close_normal:I = 0x7f020133

.field public static final voice_note_btn_close_press:I = 0x7f020134

.field public static final voice_note_btn_down_focus:I = 0x7f020135

.field public static final voice_note_btn_down_normal:I = 0x7f020136

.field public static final voice_note_btn_down_press:I = 0x7f020137

.field public static final voice_note_btn_easy:I = 0x7f020138

.field public static final voice_note_btn_ff:I = 0x7f020139

.field public static final voice_note_btn_ff_10s:I = 0x7f02013a

.field public static final voice_note_btn_ff_10s_focus:I = 0x7f02013b

.field public static final voice_note_btn_ff_10s_normal:I = 0x7f02013c

.field public static final voice_note_btn_ff_10s_press:I = 0x7f02013d

.field public static final voice_note_btn_ff_30s:I = 0x7f02013e

.field public static final voice_note_btn_ff_30s_focus:I = 0x7f02013f

.field public static final voice_note_btn_ff_30s_normal:I = 0x7f020140

.field public static final voice_note_btn_ff_30s_press:I = 0x7f020141

.field public static final voice_note_btn_ff_5s:I = 0x7f020142

.field public static final voice_note_btn_ff_5s_focus:I = 0x7f020143

.field public static final voice_note_btn_ff_5s_normal:I = 0x7f020144

.field public static final voice_note_btn_ff_5s_press:I = 0x7f020145

.field public static final voice_note_btn_ff_60s:I = 0x7f020146

.field public static final voice_note_btn_ff_60s_focus:I = 0x7f020147

.field public static final voice_note_btn_ff_60s_normal:I = 0x7f020148

.field public static final voice_note_btn_ff_60s_press:I = 0x7f020149

.field public static final voice_note_btn_list:I = 0x7f02014a

.field public static final voice_note_btn_list_focus:I = 0x7f02014b

.field public static final voice_note_btn_list_normal:I = 0x7f02014c

.field public static final voice_note_btn_list_press:I = 0x7f02014d

.field public static final voice_note_btn_next_focus:I = 0x7f02014e

.field public static final voice_note_btn_next_normal:I = 0x7f02014f

.field public static final voice_note_btn_next_press:I = 0x7f020150

.field public static final voice_note_btn_normal:I = 0x7f020151

.field public static final voice_note_btn_pause:I = 0x7f020152

.field public static final voice_note_btn_pause02_focus:I = 0x7f020153

.field public static final voice_note_btn_pause02_normal:I = 0x7f020154

.field public static final voice_note_btn_pause02_press:I = 0x7f020155

.field public static final voice_note_btn_pause_focus:I = 0x7f020156

.field public static final voice_note_btn_pause_normal:I = 0x7f020157

.field public static final voice_note_btn_pause_player:I = 0x7f020158

.field public static final voice_note_btn_pause_press:I = 0x7f020159

.field public static final voice_note_btn_play:I = 0x7f02015a

.field public static final voice_note_btn_play02_focus:I = 0x7f02015b

.field public static final voice_note_btn_play02_normal:I = 0x7f02015c

.field public static final voice_note_btn_play02_press:I = 0x7f02015d

.field public static final voice_note_btn_play_focus:I = 0x7f02015e

.field public static final voice_note_btn_play_normal:I = 0x7f02015f

.field public static final voice_note_btn_play_pause:I = 0x7f020160

.field public static final voice_note_btn_play_pause_focus:I = 0x7f020161

.field public static final voice_note_btn_play_pause_normal:I = 0x7f020162

.field public static final voice_note_btn_play_pause_press:I = 0x7f020163

.field public static final voice_note_btn_play_player:I = 0x7f020164

.field public static final voice_note_btn_play_press:I = 0x7f020165

.field public static final voice_note_btn_pre_focus:I = 0x7f020166

.field public static final voice_note_btn_pre_normal:I = 0x7f020167

.field public static final voice_note_btn_pre_press:I = 0x7f020168

.field public static final voice_note_btn_recording:I = 0x7f020169

.field public static final voice_note_btn_recording_focus:I = 0x7f02016a

.field public static final voice_note_btn_recording_normal:I = 0x7f02016b

.field public static final voice_note_btn_recording_press:I = 0x7f02016c

.field public static final voice_note_btn_resume:I = 0x7f02016d

.field public static final voice_note_btn_resume_focus:I = 0x7f02016e

.field public static final voice_note_btn_resume_normal:I = 0x7f02016f

.field public static final voice_note_btn_resume_press:I = 0x7f020170

.field public static final voice_note_btn_rw:I = 0x7f020171

.field public static final voice_note_btn_rw_10s:I = 0x7f020172

.field public static final voice_note_btn_rw_10s_focus:I = 0x7f020173

.field public static final voice_note_btn_rw_10s_normal:I = 0x7f020174

.field public static final voice_note_btn_rw_10s_press:I = 0x7f020175

.field public static final voice_note_btn_rw_30s:I = 0x7f020176

.field public static final voice_note_btn_rw_30s_focus:I = 0x7f020177

.field public static final voice_note_btn_rw_30s_normal:I = 0x7f020178

.field public static final voice_note_btn_rw_30s_press:I = 0x7f020179

.field public static final voice_note_btn_rw_5s:I = 0x7f02017a

.field public static final voice_note_btn_rw_5s_focus:I = 0x7f02017b

.field public static final voice_note_btn_rw_5s_normal:I = 0x7f02017c

.field public static final voice_note_btn_rw_5s_press:I = 0x7f02017d

.field public static final voice_note_btn_rw_60s:I = 0x7f02017e

.field public static final voice_note_btn_rw_60s_focus:I = 0x7f02017f

.field public static final voice_note_btn_rw_60s_normal:I = 0x7f020180

.field public static final voice_note_btn_rw_60s_press:I = 0x7f020181

.field public static final voice_note_btn_setting_mode:I = 0x7f020182

.field public static final voice_note_btn_setting_mode_focus:I = 0x7f020183

.field public static final voice_note_btn_setting_mode_normal:I = 0x7f020184

.field public static final voice_note_btn_setting_mode_press:I = 0x7f020185

.field public static final voice_note_btn_setting_stt:I = 0x7f020186

.field public static final voice_note_btn_stop:I = 0x7f020187

.field public static final voice_note_btn_stop_focus:I = 0x7f020188

.field public static final voice_note_btn_stop_normal:I = 0x7f020189

.field public static final voice_note_btn_stop_press:I = 0x7f02018a

.field public static final voice_note_btn_trim_focus:I = 0x7f02018b

.field public static final voice_note_btn_trim_normal:I = 0x7f02018c

.field public static final voice_note_btn_trim_press:I = 0x7f02018d

.field public static final voice_note_easy_btn:I = 0x7f02018e

.field public static final voice_note_easy_btn_press:I = 0x7f02018f

.field public static final voice_note_easy_icon_back:I = 0x7f020190

.field public static final voice_note_easy_icon_back_focus:I = 0x7f020191

.field public static final voice_note_easy_icon_back_press:I = 0x7f020192

.field public static final voice_note_easy_icon_list:I = 0x7f020193

.field public static final voice_note_easy_icon_list_focus:I = 0x7f020194

.field public static final voice_note_easy_icon_list_press:I = 0x7f020195

.field public static final voice_note_easy_icon_rec:I = 0x7f020196

.field public static final voice_note_easy_icon_rec_focus:I = 0x7f020197

.field public static final voice_note_easy_icon_rec_press:I = 0x7f020198

.field public static final voice_note_easy_progress:I = 0x7f020199

.field public static final voice_note_easy_progress_thumb:I = 0x7f02019a

.field public static final voice_note_edit:I = 0x7f02019b

.field public static final voice_note_library_list_view_bg:I = 0x7f02019c

.field public static final voice_note_list_bookmark:I = 0x7f02019d

.field public static final voice_note_list_bookmark_icon:I = 0x7f02019e

.field public static final voice_note_list_bookmark_icon_dim:I = 0x7f02019f

.field public static final voice_note_list_easy:I = 0x7f0201a0

.field public static final voice_note_list_personalpage_icon:I = 0x7f0201a1

.field public static final voice_note_list_personalpage_icon_dim:I = 0x7f0201a2

.field public static final voice_note_list_stt_icon:I = 0x7f0201a3

.field public static final voice_note_list_stt_icon_dim:I = 0x7f0201a4

.field public static final voice_note_list_stt_icon_on:I = 0x7f0201a5

.field public static final voice_note_logo_bg_area:I = 0x7f0201a6

.field public static final voice_note_logo_setting_bg:I = 0x7f0201a7

.field public static final voice_note_panel_bookmark:I = 0x7f0201a8

.field public static final voice_note_player_edit_bg:I = 0x7f0201a9

.field public static final voice_note_player_edit_bg_dark:I = 0x7f0201aa

.field public static final voice_note_progress:I = 0x7f0201ab

.field public static final voice_note_progress_bar:I = 0x7f0201ac

.field public static final voice_note_progress_bg:I = 0x7f0201ad

.field public static final voice_note_progress_focus:I = 0x7f0201ae

.field public static final voice_note_progress_normal:I = 0x7f0201af

.field public static final voice_note_progress_press:I = 0x7f0201b0

.field public static final voice_note_progress_thumb:I = 0x7f0201b1

.field public static final voice_note_progress_thumb_point:I = 0x7f0201b2

.field public static final voice_note_progressbar:I = 0x7f0201b3

.field public static final voice_note_rec_easy:I = 0x7f0201b4

.field public static final voice_note_recording_off:I = 0x7f0201b5

.field public static final voice_note_recording_on:I = 0x7f0201b6

.field public static final voice_note_repeat_a:I = 0x7f0201b7

.field public static final voice_note_repeat_b:I = 0x7f0201b8

.field public static final voice_note_repeat_progress_bar:I = 0x7f0201b9

.field public static final voice_note_repeat_progress_normal:I = 0x7f0201ba

.field public static final voice_note_repeat_progress_thumb:I = 0x7f0201bb

.field public static final voice_note_skipsilence:I = 0x7f0201bc

.field public static final voice_note_text_focus:I = 0x7f0201bd

.field public static final voice_note_text_normal:I = 0x7f0201be

.field public static final voice_note_text_press:I = 0x7f0201bf

.field public static final voice_note_trim_components_left_focus:I = 0x7f0201c0

.field public static final voice_note_trim_components_left_normal:I = 0x7f0201c1

.field public static final voice_note_trim_components_left_press:I = 0x7f0201c2

.field public static final voice_note_trim_components_left_thumb:I = 0x7f0201c3

.field public static final voice_note_trim_components_right_focus:I = 0x7f0201c4

.field public static final voice_note_trim_components_right_normal:I = 0x7f0201c5

.field public static final voice_note_trim_components_right_press:I = 0x7f0201c6

.field public static final voice_note_trim_components_right_thumb:I = 0x7f0201c7

.field public static final voice_note_trim_handler:I = 0x7f0201c8

.field public static final voice_note_trim_mode_btn_pause:I = 0x7f0201c9

.field public static final voice_note_trim_mode_btn_play:I = 0x7f0201ca

.field public static final voice_note_trim_mode_btn_trim:I = 0x7f0201cb

.field public static final voice_note_x05:I = 0x7f0201cc

.field public static final voice_note_x10:I = 0x7f0201cd

.field public static final voice_note_x15:I = 0x7f0201ce

.field public static final voice_note_x20:I = 0x7f0201cf

.field public static final voice_now_playing:I = 0x7f0201d0

.field public static final voice_rec_controller_ic_bookmark:I = 0x7f0201d1

.field public static final voice_rec_controller_ic_cancel:I = 0x7f0201d2

.field public static final voice_rec_controller_ic_down:I = 0x7f0201d3

.field public static final voice_rec_controller_ic_edit:I = 0x7f0201d4

.field public static final voice_rec_controller_ic_ff_10s:I = 0x7f0201d5

.field public static final voice_rec_controller_ic_ff_30s:I = 0x7f0201d6

.field public static final voice_rec_controller_ic_ff_5s:I = 0x7f0201d7

.field public static final voice_rec_controller_ic_ff_60s:I = 0x7f0201d8

.field public static final voice_rec_controller_ic_list:I = 0x7f0201d9

.field public static final voice_rec_controller_ic_next:I = 0x7f0201da

.field public static final voice_rec_controller_ic_pause:I = 0x7f0201db

.field public static final voice_rec_controller_ic_play:I = 0x7f0201dc

.field public static final voice_rec_controller_ic_prev:I = 0x7f0201dd

.field public static final voice_rec_controller_ic_recording:I = 0x7f0201de

.field public static final voice_rec_controller_ic_rw_10s:I = 0x7f0201df

.field public static final voice_rec_controller_ic_rw_30s:I = 0x7f0201e0

.field public static final voice_rec_controller_ic_rw_5s:I = 0x7f0201e1

.field public static final voice_rec_controller_ic_rw_60s:I = 0x7f0201e2

.field public static final voice_rec_controller_ic_setting:I = 0x7f0201e3

.field public static final voice_rec_controller_ic_stop:I = 0x7f0201e4

.field public static final voice_rec_controller_sub_ic_pause:I = 0x7f0201e5

.field public static final voice_rec_controller_sub_ic_play:I = 0x7f0201e6

.field public static final voice_rec_controller_sub_ic_resume:I = 0x7f0201e7

.field public static final voice_rec_ic_quick_panel_control_cancel:I = 0x7f0201e8

.field public static final voice_rec_ic_quick_panel_control_light:I = 0x7f0201e9

.field public static final voice_rec_ic_quick_panel_control_pause:I = 0x7f0201ea

.field public static final voice_rec_ic_quick_panel_control_play:I = 0x7f0201eb

.field public static final voice_rec_ic_quick_panel_control_playback_stop:I = 0x7f0201ec

.field public static final voice_rec_ic_quick_panel_control_record:I = 0x7f0201ed

.field public static final voice_rec_ic_quick_panel_control_resume:I = 0x7f0201ee

.field public static final voice_rec_ic_quick_panel_control_stop:I = 0x7f0201ef

.field public static final voice_rec_quick_panel_control_ic_cancel:I = 0x7f0201f0

.field public static final voice_rec_quick_panel_control_ic_pause:I = 0x7f0201f1

.field public static final voice_rec_quick_panel_control_ic_resume:I = 0x7f0201f2

.field public static final voice_rec_quick_panel_control_ic_stop:I = 0x7f0201f3

.field public static final voice_recorder_candidate_bg:I = 0x7f0201f4

.field public static final voice_recorder_candidate_bg_pressed:I = 0x7f0201f5

.field public static final voice_recorder_help_img:I = 0x7f0201f6

.field public static final voice_recorder_ic_cancel:I = 0x7f0201f7

.field public static final voice_recorder_ic_cancel_disable:I = 0x7f0201f8

.field public static final voice_recorder_ic_menu_create:I = 0x7f0201f9

.field public static final voice_recorder_ic_menu_edit:I = 0x7f0201fa

.field public static final voice_recorder_ic_menu_moreoverflow:I = 0x7f0201fb

.field public static final voice_recorder_list_icon_taged:I = 0x7f0201fc

.field public static final voice_recorder_list_write_to_voice_label:I = 0x7f0201fd

.field public static final voice_recorder_message_bottom_bg:I = 0x7f0201fe

.field public static final voice_recorder_no_item:I = 0x7f0201ff

.field public static final voice_trim_handle:I = 0x7f020200

.field public static final voice_write_to_voice_label:I = 0x7f020201

.field public static final voicenote_actionbar_bg:I = 0x7f020202

.field public static final voicerecorder_focused_category:I = 0x7f020203

.field public static final voicerecorder_mic_down_off:I = 0x7f020204

.field public static final voicerecorder_mic_down_on:I = 0x7f020205

.field public static final voicerecorder_mic_up_off:I = 0x7f020206

.field public static final voicerecorder_mic_up_on:I = 0x7f020207

.field public static final voicerecorder_selected_category:I = 0x7f020208

.field public static final voicerecorder_speaker_normal:I = 0x7f020209

.field public static final voicerecorder_speaker_press:I = 0x7f02020a

.field public static final voicerecorder_speaker_sound_off:I = 0x7f02020b

.field public static final voicerecorder_speaker_sound_on:I = 0x7f02020c

.field public static final write_to_voice_label:I = 0x7f02020d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
