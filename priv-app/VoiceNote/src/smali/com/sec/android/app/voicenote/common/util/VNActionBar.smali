.class public Lcom/sec/android/app/voicenote/common/util/VNActionBar;
.super Ljava/lang/Object;
.source "VNActionBar.java"


# instance fields
.field private isSearched:Z

.field private mActionBar:Landroid/app/ActionBar;

.field private mResources:Landroid/content/res/Resources;

.field private mTranslateBG:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->isSearched:Z

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mResources:Landroid/content/res/Resources;

    .line 36
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mTranslateBG:Landroid/graphics/drawable/Drawable;

    .line 40
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    .line 41
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mResources:Landroid/content/res/Resources;

    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->getTransparentBG()Landroid/graphics/drawable/Drawable;

    .line 54
    return-void
.end method

.method private declared-synchronized getTransparentBG()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mTranslateBG:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mTranslateBG:Landroid/graphics/drawable/Drawable;

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mTranslateBG:Landroid/graphics/drawable/Drawable;

    const v1, 0x106000d

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mTranslateBG:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public getIsSearched()Z
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->isSearched:Z

    return v0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 167
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    .line 181
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setActionbarTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 163
    :cond_0
    return-void
.end method

.method public setSearched(Z)V
    .locals 0
    .param p1, "set"    # Z

    .prologue
    .line 174
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->isSearched:Z

    .line 175
    return-void
.end method

.method public show()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f02003e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 75
    :cond_0
    return-void
.end method

.method public show(Ljava/lang/String;)V
    .locals 5
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f02003e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 88
    :cond_0
    return-void
.end method

.method public show_bookmarks()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f02003e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    const v1, 0x7f0b001f

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 145
    :cond_0
    return-void
.end method

.method public show_library(Ljava/lang/String;Z)V
    .locals 6
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "isSearch"    # Z

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 112
    iput-boolean p2, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->isSearched:Z

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f02003e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 118
    if-eqz p2, :cond_1

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v5, v5}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 129
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 131
    :cond_0
    return-void

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    goto :goto_0
.end method

.method public show_main()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 149
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->isSearched:Z

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->getTransparentBG()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 157
    :cond_0
    return-void
.end method

.method public show_mulitselect()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f02003e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v5, v5}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 109
    :cond_0
    return-void
.end method

.method public show_selectMode(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, -0x1

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    new-instance v1, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 96
    :cond_0
    return-void
.end method
