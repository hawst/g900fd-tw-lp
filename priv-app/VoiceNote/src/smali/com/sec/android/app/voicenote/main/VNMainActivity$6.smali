.class Lcom/sec/android/app/voicenote/main/VNMainActivity$6;
.super Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback$Stub;
.source "VNMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 2171
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$6;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public messageCallback(I)V
    .locals 4
    .param p1, "msg"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2174
    const/16 v0, 0x838

    if-eq p1, v0, :cond_0

    const/16 v0, 0x835

    if-eq p1, v0, :cond_0

    .line 2175
    const-string v0, "VoiceNoteMainActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VNMainActivity Message Callback : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2178
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$6;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2179
    const/16 v0, 0x7d1

    if-ne p1, v0, :cond_2

    .line 2180
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$6;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2181
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$6;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x384

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2187
    :cond_1
    :goto_0
    return-void

    .line 2183
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$6;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2184
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$6;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mUIUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$2700(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
