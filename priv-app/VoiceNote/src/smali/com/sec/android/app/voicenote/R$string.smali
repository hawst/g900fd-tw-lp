.class public final Lcom/sec/android/app/voicenote/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Failed_to_write_tag:I = 0x7f0b0000

.field public static final Tag_has_been_written_successfully:I = 0x7f0b0001

.field public static final about_this_service:I = 0x7f0b0002

.field public static final action_select_all:I = 0x7f0b0003

.field public static final action_settings:I = 0x7f0b0004

.field public static final activation_nfc_failed:I = 0x7f0b0005

.field public static final add:I = 0x7f0b0006

.field public static final add_memo:I = 0x7f0b0007

.field public static final addnewlabel:I = 0x7f0b0008

.field public static final advanced:I = 0x7f0b0009

.field public static final advanced_recording:I = 0x7f0b000a

.field public static final advanced_recording_earphone:I = 0x7f0b000b

.field public static final advanced_recording_info:I = 0x7f0b000c

.field public static final advanced_recording_pause:I = 0x7f0b000d

.field public static final advanced_stt_recording:I = 0x7f0b000e

.field public static final advanced_stt_recording_earphone:I = 0x7f0b000f

.field public static final advanced_title:I = 0x7f0b0010

.field public static final agree:I = 0x7f0b0011

.field public static final alert:I = 0x7f0b0012

.field public static final all:I = 0x7f0b0013

.field public static final app_name:I = 0x7f0b0014

.field public static final ask_for_listen_when_record_only_mode:I = 0x7f0b0015

.field public static final attention:I = 0x7f0b0016

.field public static final auto_start:I = 0x7f0b0017

.field public static final back:I = 0x7f0b0018

.field public static final background_colour:I = 0x7f0b0019

.field public static final bookmark:I = 0x7f0b001a

.field public static final bookmark_already_exists:I = 0x7f0b001b

.field public static final bookmark_full:I = 0x7f0b001c

.field public static final bookmark_full_title:I = 0x7f0b001d

.field public static final bookmark_is_added:I = 0x7f0b001e

.field public static final bookmarklist:I = 0x7f0b001f

.field public static final bytes:I = 0x7f0b0020

.field public static final cancel:I = 0x7f0b0021

.field public static final cancel_recording_message:I = 0x7f0b0022

.field public static final cancel_recording_title:I = 0x7f0b0023

.field public static final category:I = 0x7f0b0024

.field public static final category_Phone:I = 0x7f0b0025

.field public static final category_filename:I = 0x7f0b0026

.field public static final category_lecture:I = 0x7f0b0027

.field public static final category_name_already_exists:I = 0x7f0b0028

.field public static final category_none:I = 0x7f0b0029

.field public static final category_secret:I = 0x7f0b002a

.field public static final category_voice_memo:I = 0x7f0b002b

.field public static final change:I = 0x7f0b002c

.field public static final changelabel:I = 0x7f0b002d

.field public static final channel:I = 0x7f0b002e

.field public static final checkPrivate:I = 0x7f0b002f

.field public static final clear_all:I = 0x7f0b0030

.field public static final close:I = 0x7f0b0031

.field public static final cm:I = 0x7f0b0032

.field public static final color_black:I = 0x7f0b0033

.field public static final color_blue:I = 0x7f0b0034

.field public static final color_chartreuse:I = 0x7f0b0035

.field public static final color_darkgreen:I = 0x7f0b0036

.field public static final color_darkgrey:I = 0x7f0b0037

.field public static final color_deeppink:I = 0x7f0b0038

.field public static final color_green:I = 0x7f0b0039

.field public static final color_grey:I = 0x7f0b003a

.field public static final color_hotpink:I = 0x7f0b003b

.field public static final color_limegreen:I = 0x7f0b003c

.field public static final color_orange:I = 0x7f0b003d

.field public static final color_pink:I = 0x7f0b003e

.field public static final color_purple:I = 0x7f0b003f

.field public static final color_red:I = 0x7f0b0040

.field public static final color_royalblue:I = 0x7f0b0041

.field public static final color_seagreen:I = 0x7f0b0042

.field public static final color_silver:I = 0x7f0b0043

.field public static final color_skyblue:I = 0x7f0b0044

.field public static final color_violet:I = 0x7f0b0045

.field public static final color_white:I = 0x7f0b0046

.field public static final color_yellow:I = 0x7f0b0047

.field public static final connect_to_network:I = 0x7f0b0048

.field public static final contextual_filename:I = 0x7f0b0049

.field public static final conversation_mode_msg:I = 0x7f0b004a

.field public static final conversation_mode_title:I = 0x7f0b004b

.field public static final convert_speech_to_text:I = 0x7f0b004c

.field public static final convert_stt:I = 0x7f0b004d

.field public static final copy_fail:I = 0x7f0b004e

.field public static final copy_to_clipboard:I = 0x7f0b004f

.field public static final create_voice_label:I = 0x7f0b0050

.field public static final custom_colour_palette:I = 0x7f0b0051

.field public static final data_check_background_body:I = 0x7f0b0052

.field public static final data_check_background_title:I = 0x7f0b0053

.field public static final data_check_help_dont_show_again:I = 0x7f0b0054

.field public static final data_check_help_mobile_body:I = 0x7f0b0055

.field public static final data_check_help_mobile_title:I = 0x7f0b0056

.field public static final data_check_help_wifi_body:I = 0x7f0b0057

.field public static final data_check_help_wifi_title:I = 0x7f0b0058

.field public static final date_newest:I = 0x7f0b0059

.field public static final date_oldest:I = 0x7f0b005a

.field public static final decline:I = 0x7f0b005b

.field public static final default_name:I = 0x7f0b005c

.field public static final delete:I = 0x7f0b005d

.field public static final delete_bookmark_q:I = 0x7f0b005e

.field public static final delete_item:I = 0x7f0b005f

.field public static final delete_item_title:I = 0x7f0b0060

.field public static final delete_items:I = 0x7f0b0061

.field public static final delete_items_modify:I = 0x7f0b0062

.field public static final delete_items_title:I = 0x7f0b0063

.field public static final delete_label:I = 0x7f0b0064

.field public static final delete_labels:I = 0x7f0b0065

.field public static final delete_memo:I = 0x7f0b0066

.field public static final delete_memo_q:I = 0x7f0b0067

.field public static final delete_nfc_item:I = 0x7f0b0068

.field public static final delete_nfc_items:I = 0x7f0b0069

.field public static final delete_question:I = 0x7f0b006a

.field public static final deleting_failed:I = 0x7f0b006b

.field public static final desc_seconds:I = 0x7f0b006c

.field public static final details:I = 0x7f0b006d

.field public static final device:I = 0x7f0b006e

.field public static final do_not_show_again:I = 0x7f0b006f

.field public static final done:I = 0x7f0b0070

.field public static final drawing_mode:I = 0x7f0b0071

.field public static final duration:I = 0x7f0b0072

.field public static final earphone_mic:I = 0x7f0b0073

.field public static final edit:I = 0x7f0b0074

.field public static final edit_name:I = 0x7f0b0075

.field public static final editlabel:I = 0x7f0b0076

.field public static final empty_label:I = 0x7f0b0077

.field public static final empty_name:I = 0x7f0b0078

.field public static final end:I = 0x7f0b0079

.field public static final end_point:I = 0x7f0b007a

.field public static final enter_category:I = 0x7f0b007b

.field public static final enter_new_text:I = 0x7f0b007c

.field public static final eraser:I = 0x7f0b007d

.field public static final eraser_mode:I = 0x7f0b007e

.field public static final eraser_settings:I = 0x7f0b007f

.field public static final exceed_message_size_limitation:I = 0x7f0b0080

.field public static final ff:I = 0x7f0b0081

.field public static final ff_seconds:I = 0x7f0b0082

.field public static final file_already_exist:I = 0x7f0b0083

.field public static final file_does_not_exist:I = 0x7f0b0084

.field public static final file_does_not_support_trimming:I = 0x7f0b0085

.field public static final file_error:I = 0x7f0b0086

.field public static final file_name_already_exists:I = 0x7f0b0087

.field public static final filename:I = 0x7f0b0088

.field public static final filename_has_been_saved:I = 0x7f0b0089

.field public static final filterby:I = 0x7f0b008a

.field public static final filterby_title:I = 0x7f0b008b

.field public static final for_mms:I = 0x7f0b008c

.field public static final gb:I = 0x7f0b008d

.field public static final general:I = 0x7f0b008e

.field public static final go_to_storage:I = 0x7f0b008f

.field public static final high:I = 0x7f0b0090

.field public static final hold_the_back_of_your_device_over_the_tag_to_write:I = 0x7f0b0091

.field public static final images:I = 0x7f0b0092

.field public static final inches:I = 0x7f0b0093

.field public static final information_provision_agreement:I = 0x7f0b0094

.field public static final information_provision_agreement_mgs:I = 0x7f0b0095

.field public static final interview_mode:I = 0x7f0b0096

.field public static final interview_mode_msg:I = 0x7f0b0097

.field public static final interview_mode_title:I = 0x7f0b0098

.field public static final invalid_character:I = 0x7f0b0099

.field public static final kb:I = 0x7f0b009a

.field public static final knox_toast_failed_to_move:I = 0x7f0b009b

.field public static final language:I = 0x7f0b009c

.field public static final last_modified_time:I = 0x7f0b009d

.field public static final list:I = 0x7f0b009e

.field public static final list_update:I = 0x7f0b009f

.field public static final loading:I = 0x7f0b00a0

.field public static final loading_fonts:I = 0x7f0b00a1

.field public static final location:I = 0x7f0b00a2

.field public static final location_tag:I = 0x7f0b00a3

.field public static final location_tag_msg:I = 0x7f0b00a4

.field public static final logo:I = 0x7f0b00a5

.field public static final logo_summary:I = 0x7f0b00a6

.field public static final longer_than_1_sec:I = 0x7f0b00a7

.field public static final low:I = 0x7f0b00a8

.field public static final low_batt_msg:I = 0x7f0b00a9

.field public static final main_screen:I = 0x7f0b00aa

.field public static final managelabel:I = 0x7f0b00ab

.field public static final max_char_reached_msg:I = 0x7f0b00ac

.field public static final mb:I = 0x7f0b00ad

.field public static final memory_card:I = 0x7f0b00ae

.field public static final memory_full:I = 0x7f0b00af

.field public static final menu_set_alarm_tone:I = 0x7f0b00b0

.field public static final menu_set_caller_ringtone:I = 0x7f0b00b1

.field public static final menu_set_phone_ringtone:I = 0x7f0b00b2

.field public static final mms_listview:I = 0x7f0b00b3

.field public static final mono:I = 0x7f0b00b4

.field public static final monotype_default_font:I = 0x7f0b00b5

.field public static final monotype_dialog_font_applemint:I = 0x7f0b00b6

.field public static final monotype_dialog_font_choco:I = 0x7f0b00b7

.field public static final monotype_dialog_font_cool:I = 0x7f0b00b8

.field public static final monotype_dialog_font_girl:I = 0x7f0b00b9

.field public static final monotype_dialog_font_kaiti:I = 0x7f0b00ba

.field public static final monotype_dialog_font_maruberi:I = 0x7f0b00bb

.field public static final monotype_dialog_font_miao:I = 0x7f0b00bc

.field public static final monotype_dialog_font_mincho:I = 0x7f0b00bd

.field public static final monotype_dialog_font_pop:I = 0x7f0b00be

.field public static final monotype_dialog_font_rose:I = 0x7f0b00bf

.field public static final monotype_dialog_font_tinkerbell:I = 0x7f0b00c0

.field public static final more:I = 0x7f0b00c1

.field public static final move:I = 0x7f0b00c2

.field public static final move_to_personal:I = 0x7f0b00c3

.field public static final move_to_serectbox:I = 0x7f0b00c4

.field public static final moving:I = 0x7f0b00c5

.field public static final msg_recording_limit:I = 0x7f0b00c6

.field public static final msg_recording_mms_limit:I = 0x7f0b00c7

.field public static final name:I = 0x7f0b00c8

.field public static final navigate_up:I = 0x7f0b00c9

.field public static final network_error:I = 0x7f0b00ca

.field public static final new_category:I = 0x7f0b00cb

.field public static final next:I = 0x7f0b00cc

.field public static final nfc_info:I = 0x7f0b00cd

.field public static final nfc_will_be_turned_on:I = 0x7f0b00ce

.field public static final no_categories:I = 0x7f0b00cf

.field public static final no_enought_space_divice_or_sdcard:I = 0x7f0b00d0

.field public static final no_files_description:I = 0x7f0b00d1

.field public static final no_files_description_category:I = 0x7f0b00d2

.field public static final no_network_connection:I = 0x7f0b00d3

.field public static final no_network_connection_mgs:I = 0x7f0b00d4

.field public static final no_network_connection_mgs_for_chn:I = 0x7f0b00d5

.field public static final no_play_during_call:I = 0x7f0b00d6

.field public static final no_play_or_rec_during_call:I = 0x7f0b00d7

.field public static final no_recorded_files:I = 0x7f0b00d8

.field public static final no_result:I = 0x7f0b00d9

.field public static final noise_reduction:I = 0x7f0b00da

.field public static final normal:I = 0x7f0b00db

.field public static final normal_mode:I = 0x7f0b00dc

.field public static final not_enough_memory:I = 0x7f0b00dd

.field public static final number_exceed_message:I = 0x7f0b00de

.field public static final off:I = 0x7f0b00df

.field public static final ok:I = 0x7f0b00e0

.field public static final on:I = 0x7f0b00e1

.field public static final open:I = 0x7f0b00e2

.field public static final open_source_licences:I = 0x7f0b00e3

.field public static final others:I = 0x7f0b00e4

.field public static final panel_time:I = 0x7f0b00e5

.field public static final path:I = 0x7f0b00e6

.field public static final pause:I = 0x7f0b00e7

.field public static final paused:I = 0x7f0b00e8

.field public static final pen:I = 0x7f0b00e9

.field public static final pen_settings:I = 0x7f0b00ea

.field public static final pen_settings_preset_delete_msg:I = 0x7f0b00eb

.field public static final pen_settings_preset_delete_title:I = 0x7f0b00ec

.field public static final pen_settings_preset_empty:I = 0x7f0b00ed

.field public static final personal_page_add_to_personal_page:I = 0x7f0b00ee

.field public static final personal_page_content:I = 0x7f0b00ef

.field public static final personal_page_remove_from_personal_page:I = 0x7f0b00f0

.field public static final phone:I = 0x7f0b00f1

.field public static final play:I = 0x7f0b00f2

.field public static final playback:I = 0x7f0b00f3

.field public static final playseed:I = 0x7f0b00f4

.field public static final popup_speech_to_text:I = 0x7f0b00f5

.field public static final previous:I = 0x7f0b00f6

.field public static final private_tts:I = 0x7f0b00f7

.field public static final processing:I = 0x7f0b00f8

.field public static final recognition_failed:I = 0x7f0b00f9

.field public static final record:I = 0x7f0b00fa

.field public static final recorder:I = 0x7f0b00fb

.field public static final recording:I = 0x7f0b00fc

.field public static final recording_error:I = 0x7f0b00fd

.field public static final recording_failed:I = 0x7f0b00fe

.field public static final recording_now:I = 0x7f0b00ff

.field public static final recording_quality:I = 0x7f0b0100

.field public static final recording_volume:I = 0x7f0b0101

.field public static final recording_with_earphone_mic:I = 0x7f0b0102

.field public static final recordinglist:I = 0x7f0b0103

.field public static final redo:I = 0x7f0b0104

.field public static final rename:I = 0x7f0b0105

.field public static final repeat:I = 0x7f0b0106

.field public static final replace:I = 0x7f0b0107

.field public static final resume:I = 0x7f0b0108

.field public static final rew:I = 0x7f0b0109

.field public static final ringtone_added:I = 0x7f0b010a

.field public static final rw_seconds:I = 0x7f0b010b

.field public static final s_button:I = 0x7f0b010c

.field public static final samsung:I = 0x7f0b010d

.field public static final save:I = 0x7f0b010e

.field public static final save_to_sd_card_description:I = 0x7f0b010f

.field public static final save_to_sd_card_title:I = 0x7f0b0110

.field public static final save_while_pausing:I = 0x7f0b0111

.field public static final sd_card_use_mass_storage:I = 0x7f0b0112

.field public static final sdcard_missing_message:I = 0x7f0b0113

.field public static final search:I = 0x7f0b0114

.field public static final select:I = 0x7f0b0115

.field public static final select_all:I = 0x7f0b0116

.field public static final select_category:I = 0x7f0b0117

.field public static final select_mode:I = 0x7f0b0118

.field public static final select_sim_card:I = 0x7f0b0119

.field public static final select_type:I = 0x7f0b011a

.field public static final selected:I = 0x7f0b011b

.field public static final set_as:I = 0x7f0b011c

.field public static final setting_saveto:I = 0x7f0b011d

.field public static final settings:I = 0x7f0b011e

.field public static final sharevia:I = 0x7f0b011f

.field public static final size:I = 0x7f0b0120

.field public static final skip_interval:I = 0x7f0b0121

.field public static final skip_silence:I = 0x7f0b0122

.field public static final sort_voice_label:I = 0x7f0b0123

.field public static final sortby:I = 0x7f0b0124

.field public static final standby:I = 0x7f0b0125

.field public static final start_point:I = 0x7f0b0126

.field public static final stereo:I = 0x7f0b0127

.field public static final stms_appgroup:I = 0x7f0b0128

.field public static final stop:I = 0x7f0b0129

.field public static final stop_recording_cancel:I = 0x7f0b012a

.field public static final stop_recording_ok:I = 0x7f0b012b

.field public static final stop_recording_title:I = 0x7f0b012c

.field public static final storage:I = 0x7f0b012d

.field public static final stt:I = 0x7f0b012e

.field public static final stt_hint:I = 0x7f0b012f

.field public static final stt_language_value_ch:I = 0x7f0b0130

.field public static final stt_language_value_deu:I = 0x7f0b0131

.field public static final stt_language_value_es:I = 0x7f0b0132

.field public static final stt_language_value_espa:I = 0x7f0b0133

.field public static final stt_language_value_fr:I = 0x7f0b0134

.field public static final stt_language_value_it:I = 0x7f0b0135

.field public static final stt_language_value_jp:I = 0x7f0b0136

.field public static final stt_language_value_ko:I = 0x7f0b0137

.field public static final stt_language_value_pobr:I = 0x7f0b0138

.field public static final stt_language_value_ru:I = 0x7f0b0139

.field public static final stt_language_value_uk:I = 0x7f0b013a

.field public static final stt_language_value_us:I = 0x7f0b013b

.field public static final stt_warning_description:I = 0x7f0b013c

.field public static final tag_will_be_reset:I = 0x7f0b013d

.field public static final take_picture:I = 0x7f0b013e

.field public static final talk_mode:I = 0x7f0b013f

.field public static final tap_to_add_logo:I = 0x7f0b0140

.field public static final text:I = 0x7f0b0141

.field public static final text_colour:I = 0x7f0b0142

.field public static final text_mode:I = 0x7f0b0143

.field public static final text_only:I = 0x7f0b0144

.field public static final text_only_file:I = 0x7f0b0145

.field public static final text_settings:I = 0x7f0b0146

.field public static final time:I = 0x7f0b0147

.field public static final time_shift_recording:I = 0x7f0b0148

.field public static final title_activity_vnmanage_label:I = 0x7f0b0149

.field public static final toast_item_added_to_personal_page_one:I = 0x7f0b014a

.field public static final toast_item_added_to_personal_page_other:I = 0x7f0b014b

.field public static final toast_item_move_to_normal_numerous_private_numerous:I = 0x7f0b014c

.field public static final toast_item_move_to_normal_numerous_private_one:I = 0x7f0b014d

.field public static final toast_item_move_to_normal_one_private_numerous:I = 0x7f0b014e

.field public static final toast_item_move_to_normal_one_private_one:I = 0x7f0b014f

.field public static final toast_item_remove_from_personal_page_one:I = 0x7f0b0150

.field public static final toast_item_remove_from_personal_page_other:I = 0x7f0b0151

.field public static final toast_moveto_multi:I = 0x7f0b0152

.field public static final toast_moveto_single:I = 0x7f0b0153

.field public static final trim:I = 0x7f0b0154

.field public static final trim_save_as_new:I = 0x7f0b0155

.field public static final trim_save_to_original:I = 0x7f0b0156

.field public static final trim_title:I = 0x7f0b0157

.field public static final turn_on_nfc:I = 0x7f0b0158

.field public static final unable_rename_voice_file_contextual_enabled:I = 0x7f0b0159

.field public static final unable_to_play:I = 0x7f0b015a

.field public static final unable_to_read_voice_label:I = 0x7f0b015b

.field public static final unable_to_save_recording:I = 0x7f0b015c

.field public static final unable_to_write_voice_label:I = 0x7f0b015d

.field public static final undo:I = 0x7f0b015e

.field public static final unselect_all:I = 0x7f0b015f

.field public static final use_permission_alert_body:I = 0x7f0b0160

.field public static final use_permission_alert_title:I = 0x7f0b0161

.field public static final voice:I = 0x7f0b0162

.field public static final voice_label_error_msg1:I = 0x7f0b0163

.field public static final voice_label_error_msg2:I = 0x7f0b0164

.field public static final voice_memo_mode:I = 0x7f0b0165

.field public static final voice_memo_popup_description:I = 0x7f0b0166

.field public static final voice_memo_popup_description_charge:I = 0x7f0b0167

.field public static final voice_memo_popup_description_charge_for_chn:I = 0x7f0b0168

.field public static final voice_memo_popup_description_usa:I = 0x7f0b0169

.field public static final voice_memo_popup_title:I = 0x7f0b016a

.field public static final voice_memo_popup_title_charge:I = 0x7f0b016b

.field public static final voice_only:I = 0x7f0b016c

.field public static final voice_only_file:I = 0x7f0b016d

.field public static final voice_pcl:I = 0x7f0b016e

.field public static final voice_text:I = 0x7f0b016f

.field public static final voicememo_link1:I = 0x7f0b0170

.field public static final voicememo_link1_mgs:I = 0x7f0b0171

.field public static final voicememo_link1_title:I = 0x7f0b0172

.field public static final voicememo_link2:I = 0x7f0b0173

.field public static final voicememo_link3:I = 0x7f0b0174

.field public static final vr_unable_delete:I = 0x7f0b0175

.field public static final vvm:I = 0x7f0b0176

.field public static final vvm_attention:I = 0x7f0b0177

.field public static final vvm_greeting:I = 0x7f0b0178

.field public static final vvm_greeting_msg:I = 0x7f0b0179

.field public static final vvm_greeting_msg2:I = 0x7f0b017a

.field public static final warning_trim_quality:I = 0x7f0b017b

.field public static final write_to_nfc_tag:I = 0x7f0b017c

.field public static final write_to_tag:I = 0x7f0b017d

.field public static final written:I = 0x7f0b017e

.field public static final your_location:I = 0x7f0b017f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
