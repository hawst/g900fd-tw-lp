.class Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$1;
.super Ljava/lang/Object;
.source "VNSelectNBestDialogFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 210
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 207
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mDeleteButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mDeleteButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mDeleteButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method
