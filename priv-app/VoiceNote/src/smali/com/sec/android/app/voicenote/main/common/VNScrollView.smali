.class public Lcom/sec/android/app/voicenote/main/common/VNScrollView;
.super Landroid/widget/HorizontalScrollView;
.source "VNScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 49
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/main/common/VNScrollView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 50
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/main/common/VNScrollView;->setVerticalFadingEdgeEnabled(Z)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/main/common/VNScrollView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 44
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/main/common/VNScrollView;->setVerticalFadingEdgeEnabled(Z)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/main/common/VNScrollView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 38
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/main/common/VNScrollView;->setVerticalFadingEdgeEnabled(Z)V

    .line 39
    return-void
.end method


# virtual methods
.method public initViews(Landroid/view/View;Landroid/view/View;)V
    .locals 4
    .param p1, "interview"    # Landroid/view/View;
    .param p2, "conversation"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x4

    .line 59
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/main/common/VNScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 61
    .local v1, "parent":Landroid/view/ViewGroup;
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 62
    invoke-virtual {p2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 63
    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 64
    invoke-virtual {v1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 66
    new-instance v0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;-><init>(Lcom/sec/android/app/voicenote/main/common/VNScrollView;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    .line 67
    .local v0, "listener":Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 68
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method
