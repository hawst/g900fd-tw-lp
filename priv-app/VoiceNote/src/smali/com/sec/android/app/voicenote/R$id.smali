.class public final Lcom/sec/android/app/voicenote/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final DropLabeltitle:I = 0x7f0e0063

.field public static final ErrorMessageTextView:I = 0x7f0e0007

.field public static final ImageView01:I = 0x7f0e0006

.field public static final LabelButton:I = 0x7f0e006a

.field public static final LabelButton_parent:I = 0x7f0e005f

.field public static final Label_filecount:I = 0x7f0e005b

.field public static final Labeltitle:I = 0x7f0e005a

.field public static final PlayspeedIcon:I = 0x7f0e00a2

.field public static final ProgressLayout:I = 0x7f0e00a5

.field public static final RepeatIcon:I = 0x7f0e00a1

.field public static final SkipSilenceIcon:I = 0x7f0e00a3

.field public static final TrimIcon:I = 0x7f0e00a0

.field public static final action_addtopersonalpage:I = 0x7f0e0101

.field public static final action_bookmark_delete:I = 0x7f0e00f5

.field public static final action_bookmark_rename:I = 0x7f0e00f4

.field public static final action_cancel:I = 0x7f0e00f2

.field public static final action_changecategory:I = 0x7f0e00fa

.field public static final action_delete:I = 0x7f0e00f9

.field public static final action_delete_label:I = 0x7f0e00f7

.field public static final action_details:I = 0x7f0e00fe

.field public static final action_edit_file:I = 0x7f0e00fb

.field public static final action_edit_label:I = 0x7f0e00f6

.field public static final action_mode_title:I = 0x7f0e0023

.field public static final action_move_secret:I = 0x7f0e00ff

.field public static final action_ok:I = 0x7f0e00f3

.field public static final action_removefrompersonalpage:I = 0x7f0e0102

.field public static final action_removefromsecret:I = 0x7f0e0100

.field public static final action_set_as:I = 0x7f0e00fc

.field public static final action_sharevia:I = 0x7f0e00f8

.field public static final action_writetonfctag:I = 0x7f0e00fd

.field public static final actionmode_done:I = 0x7f0e0004

.field public static final actionmode_selectview:I = 0x7f0e0003

.field public static final actionmode_title:I = 0x7f0e0005

.field public static final activity_icon:I = 0x7f0e00e9

.field public static final activity_name:I = 0x7f0e00ea

.field public static final addnewlabel:I = 0x7f0e0103

.field public static final bookmarkTitle:I = 0x7f0e0064

.field public static final bookmark_list_textView:I = 0x7f0e0021

.field public static final bookmark_listview_selectall:I = 0x7f0e0038

.field public static final bookmarklabel:I = 0x7f0e006c

.field public static final bottomLabel:I = 0x7f0e0068

.field public static final bottomRightLabel:I = 0x7f0e0069

.field public static final cancel:I = 0x7f0e0107

.field public static final category_list_item_layout:I = 0x7f0e0058

.field public static final category_radioButton:I = 0x7f0e005c

.field public static final center_bar:I = 0x7f0e009d

.field public static final check_warning_show_again:I = 0x7f0e00ef

.field public static final checkbox_layout:I = 0x7f0e00ee

.field public static final checkbox_wrapper:I = 0x7f0e001f

.field public static final checked:I = 0x7f0e00f0

.field public static final color_gradience:I = 0x7f0e000c

.field public static final countText:I = 0x7f0e0092

.field public static final dialog_category_select:I = 0x7f0e0032

.field public static final dialog_filename_edit:I = 0x7f0e0031

.field public static final dialog_select_label_empty:I = 0x7f0e0034

.field public static final dialog_select_label_list:I = 0x7f0e0033

.field public static final dialog_select_sort_list:I = 0x7f0e0035

.field public static final dialog_select_stt_list:I = 0x7f0e0036

.field public static final dialog_text:I = 0x7f0e0024

.field public static final dialog_text_edittext:I = 0x7f0e0025

.field public static final distance_text:I = 0x7f0e00ed

.field public static final easy_progressbar:I = 0x7f0e00a7

.field public static final edit_label_color:I = 0x7f0e000a

.field public static final edit_label_text:I = 0x7f0e000b

.field public static final emptyText:I = 0x7f0e003b

.field public static final emptyView:I = 0x7f0e004f

.field public static final expandable_listview_selectall:I = 0x7f0e0039

.field public static final expandlabel:I = 0x7f0e0071

.field public static final fileName:I = 0x7f0e0090

.field public static final file_name:I = 0x7f0e009f

.field public static final fingerprint_label:I = 0x7f0e006b

.field public static final fontname:I = 0x7f0e0060

.field public static final insert_stt_delete:I = 0x7f0e0030

.field public static final insert_stt_text:I = 0x7f0e002f

.field public static final label_color_1:I = 0x7f0e0026

.field public static final label_color_2:I = 0x7f0e0027

.field public static final label_color_3:I = 0x7f0e0028

.field public static final label_color_4:I = 0x7f0e0029

.field public static final label_color_5:I = 0x7f0e002a

.field public static final label_color_6:I = 0x7f0e002b

.field public static final label_color_7:I = 0x7f0e002c

.field public static final label_color_8:I = 0x7f0e002d

.field public static final label_listview_selectall:I = 0x7f0e0019

.field public static final labelview:I = 0x7f0e0059

.field public static final left_button:I = 0x7f0e0001

.field public static final library_view_fragment:I = 0x7f0e0013

.field public static final library_view_sub_fragment:I = 0x7f0e0014

.field public static final list:I = 0x7f0e001c

.field public static final list_nbest:I = 0x7f0e002e

.field public static final listrow_checkbox_item_checkbox:I = 0x7f0e0020

.field public static final listrow_voiceclip:I = 0x7f0e0065

.field public static final lockscreen_center_buttons:I = 0x7f0e007b

.field public static final lockscreen_close_button:I = 0x7f0e007a

.field public static final lockscreen_controller:I = 0x7f0e0078

.field public static final lockscreen_indicator:I = 0x7f0e0076

.field public static final lockscreen_indicator_on:I = 0x7f0e0077

.field public static final lockscreen_pause_button:I = 0x7f0e007c

.field public static final lockscreen_rec_button:I = 0x7f0e007d

.field public static final lockscreen_recording_timer:I = 0x7f0e0075

.field public static final lockscreen_recording_timer_wrapper:I = 0x7f0e0074

.field public static final lockscreen_stop_button:I = 0x7f0e0079

.field public static final logo_action:I = 0x7f0e003f

.field public static final logo_add:I = 0x7f0e0042

.field public static final logo_add_text:I = 0x7f0e0043

.field public static final logo_bg:I = 0x7f0e0040

.field public static final logo_bottom_layout:I = 0x7f0e0044

.field public static final logo_change:I = 0x7f0e0045

.field public static final logo_color_1:I = 0x7f0e007e

.field public static final logo_color_10:I = 0x7f0e0087

.field public static final logo_color_11:I = 0x7f0e0088

.field public static final logo_color_12:I = 0x7f0e0089

.field public static final logo_color_13:I = 0x7f0e008a

.field public static final logo_color_14:I = 0x7f0e008b

.field public static final logo_color_15:I = 0x7f0e008c

.field public static final logo_color_16:I = 0x7f0e008d

.field public static final logo_color_2:I = 0x7f0e007f

.field public static final logo_color_3:I = 0x7f0e0080

.field public static final logo_color_4:I = 0x7f0e0081

.field public static final logo_color_5:I = 0x7f0e0082

.field public static final logo_color_6:I = 0x7f0e0083

.field public static final logo_color_7:I = 0x7f0e0084

.field public static final logo_color_8:I = 0x7f0e0085

.field public static final logo_color_9:I = 0x7f0e0086

.field public static final logo_no_layout:I = 0x7f0e003c

.field public static final logo_no_used:I = 0x7f0e003d

.field public static final logo_ok_layout:I = 0x7f0e003e

.field public static final logo_text:I = 0x7f0e0041

.field public static final logo_text_bgcolor_btn:I = 0x7f0e0047

.field public static final logo_text_color_btn:I = 0x7f0e0046

.field public static final logo_text_font:I = 0x7f0e0048

.field public static final main_bottom_fragments:I = 0x7f0e0015

.field public static final main_control_button:I = 0x7f0e0017

.field public static final main_logo:I = 0x7f0e0012

.field public static final main_panel_text:I = 0x7f0e0010

.field public static final main_panel_text_easy:I = 0x7f0e0011

.field public static final main_panel_text_stt:I = 0x7f0e000f

.field public static final main_recording_fragments_wrapper:I = 0x7f0e000d

.field public static final main_recording_mode:I = 0x7f0e000e

.field public static final manage_label_list_item_layout:I = 0x7f0e005d

.field public static final manage_label_popup_delete:I = 0x7f0e0106

.field public static final manage_label_popup_select:I = 0x7f0e0105

.field public static final menu_deselect_all:I = 0x7f0e011d

.field public static final menu_select_all:I = 0x7f0e011c

.field public static final menu_selected:I = 0x7f0e011b

.field public static final mic_body:I = 0x7f0e00cf

.field public static final mic_body_off:I = 0x7f0e00d0

.field public static final mic_body_on:I = 0x7f0e00d1

.field public static final mic_body_on_echo:I = 0x7f0e00d2

.field public static final mic_icon_down_off:I = 0x7f0e00d6

.field public static final mic_icon_down_on:I = 0x7f0e00d5

.field public static final mic_icon_up_off:I = 0x7f0e00d4

.field public static final mic_icon_up_on:I = 0x7f0e00d3

.field public static final navigation_bar:I = 0x7f0e0000

.field public static final nfcimage:I = 0x7f0e0008

.field public static final nfclabel:I = 0x7f0e006d

.field public static final no_file_text:I = 0x7f0e001a

.field public static final no_item_bg:I = 0x7f0e003a

.field public static final no_softkey_main:I = 0x7f0e0037

.field public static final ok:I = 0x7f0e0108

.field public static final option_bookmark:I = 0x7f0e0115

.field public static final option_bookmark_delete:I = 0x7f0e0117

.field public static final option_bookmark_select:I = 0x7f0e0116

.field public static final option_convert_stt:I = 0x7f0e0118

.field public static final option_create_voice_label:I = 0x7f0e0109

.field public static final option_delete:I = 0x7f0e0112

.field public static final option_details:I = 0x7f0e011a

.field public static final option_edit_file:I = 0x7f0e0113

.field public static final option_edit_name:I = 0x7f0e010a

.field public static final option_filterby:I = 0x7f0e010e

.field public static final option_managelabel:I = 0x7f0e010f

.field public static final option_movetosecret:I = 0x7f0e0110

.field public static final option_removefromsecret:I = 0x7f0e0111

.field public static final option_select:I = 0x7f0e010c

.field public static final option_set_as:I = 0x7f0e0114

.field public static final option_setting:I = 0x7f0e0104

.field public static final option_settings:I = 0x7f0e010b

.field public static final option_sort:I = 0x7f0e010d

.field public static final option_writetonfctag:I = 0x7f0e0119

.field public static final pauselabel:I = 0x7f0e0070

.field public static final pbHeaderProgress:I = 0x7f0e004d

.field public static final percentText:I = 0x7f0e0093

.field public static final player_control_back_button:I = 0x7f0e0099

.field public static final player_control_bookmark_button:I = 0x7f0e00a4

.field public static final player_control_ff_button:I = 0x7f0e0097

.field public static final player_control_list_button:I = 0x7f0e009a

.field public static final player_control_pause_button:I = 0x7f0e0095

.field public static final player_control_play_button:I = 0x7f0e0096

.field public static final player_control_record_button:I = 0x7f0e009b

.field public static final player_control_rew_button:I = 0x7f0e0094

.field public static final player_control_setting_mode_button:I = 0x7f0e009c

.field public static final player_duration:I = 0x7f0e00a9

.field public static final player_elapse_time:I = 0x7f0e00a8

.field public static final player_idle_control:I = 0x7f0e0098

.field public static final player_toolbar:I = 0x7f0e009e

.field public static final player_toolbar_fragment:I = 0x7f0e0016

.field public static final playlabel:I = 0x7f0e006f

.field public static final prefixFile:I = 0x7f0e008f

.field public static final processing:I = 0x7f0e008e

.field public static final progressBar:I = 0x7f0e0091

.field public static final progressbar:I = 0x7f0e00a6

.field public static final quickpaenl_voicerecord_record_indicator:I = 0x7f0e00e7

.field public static final quickpanel_time:I = 0x7f0e00de

.field public static final quickpanel_voicerecord:I = 0x7f0e00db

.field public static final quickpanel_voicerecord_cancel:I = 0x7f0e00e4

.field public static final quickpanel_voicerecord_clipname:I = 0x7f0e00dd

.field public static final quickpanel_voicerecord_description:I = 0x7f0e00e3

.field public static final quickpanel_voicerecord_launch_recorder:I = 0x7f0e00dc

.field public static final quickpanel_voicerecord_pause:I = 0x7f0e00e1

.field public static final quickpanel_voicerecord_play:I = 0x7f0e00df

.field public static final quickpanel_voicerecord_record_new:I = 0x7f0e00e0

.field public static final quickpanel_voicerecord_record_pause:I = 0x7f0e00e8

.field public static final quickpanel_voicerecord_record_resume:I = 0x7f0e00e6

.field public static final quickpanel_voicerecord_save:I = 0x7f0e00e5

.field public static final quickpanel_voicerecord_stop:I = 0x7f0e00e2

.field public static final radiobutton_check:I = 0x7f0e0073

.field public static final recorder_control_book_mark_button:I = 0x7f0e00cd

.field public static final recorder_control_close_button:I = 0x7f0e00ca

.field public static final recorder_control_list_button:I = 0x7f0e00c5

.field public static final recorder_control_normal_button:I = 0x7f0e00c6

.field public static final recorder_control_pause_button:I = 0x7f0e00ce

.field public static final recorder_control_record_button:I = 0x7f0e00c7

.field public static final recorder_control_resume_button:I = 0x7f0e00cc

.field public static final recorder_control_setting_button_mode:I = 0x7f0e00c9

.field public static final recorder_control_setting_button_stt:I = 0x7f0e00da

.field public static final recorder_control_stop_button:I = 0x7f0e00cb

.field public static final recorder_panel_icon_off:I = 0x7f0e00d7

.field public static final recorder_panel_icon_on:I = 0x7f0e00d9

.field public static final recorder_panel_name:I = 0x7f0e00c8

.field public static final recorder_panel_time:I = 0x7f0e00d8

.field public static final recorder_recording_btn:I = 0x7f0e0052

.field public static final repeat_progressbar:I = 0x7f0e00ad

.field public static final repeat_progressbar_seek:I = 0x7f0e00ab

.field public static final resolver_grid:I = 0x7f0e00eb

.field public static final right_button:I = 0x7f0e0002

.field public static final searchView:I = 0x7f0e0056

.field public static final searchview_ani:I = 0x7f0e0057

.field public static final seek_button_group:I = 0x7f0e00be

.field public static final seek_edit_group:I = 0x7f0e00b0

.field public static final select_title:I = 0x7f0e0062

.field public static final selectall_checkbox:I = 0x7f0e0022

.field public static final setting_view_fragment:I = 0x7f0e001e

.field public static final single_select_view_fragment:I = 0x7f0e001b

.field public static final spinnertext:I = 0x7f0e0061

.field public static final stt_processing_ani:I = 0x7f0e004e

.field public static final stt_scroll:I = 0x7f0e0049

.field public static final stt_trim_end_handler:I = 0x7f0e004c

.field public static final stt_trim_start_handler:I = 0x7f0e004b

.field public static final stt_view:I = 0x7f0e004a

.field public static final sttlabel:I = 0x7f0e006e

.field public static final tagview:I = 0x7f0e0009

.field public static final textLayout:I = 0x7f0e0066

.field public static final textview_layout:I = 0x7f0e005e

.field public static final title:I = 0x7f0e00c4

.field public static final toobar_help_img:I = 0x7f0e0051

.field public static final toolbar_help_cancel_btn:I = 0x7f0e0050

.field public static final tooltip_player_control_ff_button:I = 0x7f0e0055

.field public static final tooltip_player_control_play_button:I = 0x7f0e0054

.field public static final tooltip_player_control_rew_button:I = 0x7f0e0053

.field public static final topLabel:I = 0x7f0e0067

.field public static final trim_close:I = 0x7f0e00bf

.field public static final trim_end_time:I = 0x7f0e00bd

.field public static final trim_left_empty:I = 0x7f0e00b1

.field public static final trim_left_empty_progress:I = 0x7f0e00b5

.field public static final trim_left_handle:I = 0x7f0e00ba

.field public static final trim_pause:I = 0x7f0e00c2

.field public static final trim_play:I = 0x7f0e00c1

.field public static final trim_player_seekbar:I = 0x7f0e00b9

.field public static final trim_player_seekbar_holder:I = 0x7f0e00b7

.field public static final trim_player_seekbar_thumb_upper_part:I = 0x7f0e00b8

.field public static final trim_progress_bar_layout:I = 0x7f0e00b4

.field public static final trim_right_empty:I = 0x7f0e00b2

.field public static final trim_right_empty_image:I = 0x7f0e00b3

.field public static final trim_right_empty_progress:I = 0x7f0e00b6

.field public static final trim_right_handle:I = 0x7f0e00bb

.field public static final trim_save:I = 0x7f0e00c0

.field public static final trim_start_time:I = 0x7f0e00bc

.field public static final trimbar_fragment:I = 0x7f0e0018

.field public static final unchecked:I = 0x7f0e00f1

.field public static final voice_clip_list_item_layout:I = 0x7f0e0072

.field public static final voice_note_bookmark_layout:I = 0x7f0e00ac

.field public static final voice_note_repeat_a:I = 0x7f0e00ae

.field public static final voice_note_repeat_atob:I = 0x7f0e00aa

.field public static final voice_note_repeat_b:I = 0x7f0e00af

.field public static final warning_image:I = 0x7f0e00ec

.field public static final warning_text:I = 0x7f0e00c3

.field public static final webview:I = 0x7f0e001d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
