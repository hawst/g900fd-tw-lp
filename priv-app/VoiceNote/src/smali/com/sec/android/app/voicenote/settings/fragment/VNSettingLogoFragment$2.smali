.class Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment$2;
.super Landroid/view/View$AccessibilityDelegate;
.source "VNSettingLogoFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->setLogoClickable(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;)V
    .locals 0

    .prologue
    .line 820
    iput-object p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment$2;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public sendAccessibilityEvent(Landroid/view/View;I)V
    .locals 2
    .param p1, "host"    # Landroid/view/View;
    .param p2, "eventType"    # I

    .prologue
    .line 823
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment$2;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mTempLogoType:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 824
    const-string v0, "VNSettingLogoFragment"

    const-string v1, "sendAccessibilityEvent() 2"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 826
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->sendAccessibilityEvent(Landroid/view/View;I)V

    .line 831
    :cond_0
    :goto_0
    return-void

    .line 827
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment$2;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogo:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->access$000(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 828
    const-string v0, "VNSettingLogoFragment"

    const-string v1, "sendAccessibilityEvent() 1"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment$2;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->mAddLogoText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;->access$200(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method
