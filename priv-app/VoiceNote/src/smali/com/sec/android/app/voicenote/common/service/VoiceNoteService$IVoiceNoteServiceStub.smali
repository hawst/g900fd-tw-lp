.class Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;
.super Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;
.source "VoiceNoteService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IVoiceNoteServiceStub"
.end annotation


# instance fields
.field private final mRemoteCallbacklist:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mVoiceNoteService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Landroid/os/RemoteCallbackList;)V
    .locals 1
    .param p1, "voiceNoteService"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2271
    .local p2, "remoteCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;>;"
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;-><init>()V

    .line 2272
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    .line 2273
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mRemoteCallbacklist:Ljava/lang/ref/WeakReference;

    .line 2274
    return-void
.end method


# virtual methods
.method public addBookmark()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2407
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->addBookmark()I

    move-result v0

    return v0
.end method

.method public cancelRecording()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2322
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->cancelRecording()Z

    move-result v0

    return v0
.end method

.method public getDateAdded()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2377
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # invokes: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getDateAdded()Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2700(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2372
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # invokes: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getFileName()Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2600(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2367
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # invokes: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getFilePath()Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2500(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFileSize()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2362
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # invokes: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getFileSize()J
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2400(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastSavedFileUriString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2382
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # invokes: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getLastSavedFileUriString()Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2800(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMaxAmplitude()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2387
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # invokes: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMaxAmplitude()I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2900(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v0

    return v0
.end method

.method public getMediaRecorderState()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2327
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v0

    return v0
.end method

.method public getRealFreeSize(I)J
    .locals 2
    .param p1, "warningSize"    # I

    .prologue
    .line 2392
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # invokes: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRealFreeSize(I)J
    invoke-static {v0, p1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$3000(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getRecDuration()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2332
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSTTState()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2402
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSTTState()Z

    move-result v0

    return v0
.end method

.method public hideNotification()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2342
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->hideNotification()Z

    move-result v0

    return v0
.end method

.method public initRecording(Ljava/lang/String;J)Z
    .locals 2
    .param p1, "mimetype"    # Ljava/lang/String;
    .param p2, "sizeMMS"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2292
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->initRecording(Ljava/lang/String;J)Z

    move-result v0

    return v0
.end method

.method public isPaused()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2352
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2347
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public pauseRecording()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2312
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pauseRecording()Z

    move-result v0

    return v0
.end method

.method public registerCallback(Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;)V
    .locals 1
    .param p1, "ServiceCallback"    # Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2278
    if-eqz p1, :cond_0

    .line 2279
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mRemoteCallbacklist:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 2281
    :cond_0
    return-void
.end method

.method public resumeRecording()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2317
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumeRecording()Z

    move-result v0

    return v0
.end method

.method public saveRecording()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2307
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecording()Z

    move-result v0

    return v0
.end method

.method public setFromVVM(Z)V
    .locals 1
    .param p1, "value"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2413
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->SetFromVVM(Z)V

    .line 2414
    return-void
.end method

.method public showNotification()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2337
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->showNotification()Z

    move-result v0

    return v0
.end method

.method public startPreRecording()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2302
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->startPreRecording()Z

    move-result v0

    return v0
.end method

.method public startRecording()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2297
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->startRecording()Z

    move-result v0

    return v0
.end method

.method public stopPlay()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2357
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 2358
    return-void
.end method

.method public turnOnSTT(Z)V
    .locals 1
    .param p1, "on"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2397
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mVoiceNoteService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->turnOnSTT(Z)V

    .line 2398
    return-void
.end method

.method public unregisterCallback(Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;)V
    .locals 1
    .param p1, "ServiceCallback"    # Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2285
    if-eqz p1, :cond_0

    .line 2286
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;->mRemoteCallbacklist:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 2288
    :cond_0
    return-void
.end method
