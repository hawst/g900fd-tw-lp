.class Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;
.super Ljava/lang/Object;
.source "VNSelectModeDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "index"    # I

    .prologue
    .line 124
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->isResumed()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v5

    if-nez v5, :cond_1

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->isResumed()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 129
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->dismissAllowingStateLoss()V

    .line 132
    :cond_2
    const-string v5, "record_mode"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v4

    .line 133
    .local v4, "previousRecordingMode":I
    const-string v5, "record_mode_prev"

    invoke-static {v5, v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 135
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mModeItems:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    const-string v6, "value"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 137
    .local v2, "mode":I
    packed-switch v2, :pswitch_data_0

    .line 189
    :cond_3
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$Callbacks;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->access$200(Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$Callbacks;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 190
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$Callbacks;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->access$200(Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$Callbacks;

    move-result-object v5

    invoke-interface {v5, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$Callbacks;->onModeChanged(I)V

    goto :goto_0

    .line 139
    :pswitch_0
    const-string v5, "record_mode"

    invoke-static {v5, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 140
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "NORM"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 144
    :pswitch_1
    const-string v5, "record_mode"

    invoke-static {v5, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 145
    const-string v5, "show_interview_info"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_3

    .line 146
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    # invokes: Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->isAdvancedAndHeadsetMode()Z
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 147
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->newInstance(I)Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "FRAGMENT_DIALOG"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 148
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "INTE"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 154
    :pswitch_2
    const-string v5, "record_mode"

    invoke-static {v5, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 155
    const-string v5, "show_conversation_info"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_3

    .line 156
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    # invokes: Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->isAdvancedAndHeadsetMode()Z
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 157
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->newInstance(I)Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "FRAGMENT_DIALOG"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 158
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "CONV"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 164
    :pswitch_3
    const-string v5, "show_policy_info"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_5

    .line 165
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    # invokes: Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->isAdvancedAndHeadsetMode()Z
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 166
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;-><init>()V

    .line 167
    .local v1, "dialog":Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v5, v6}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 169
    .end local v1    # "dialog":Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;
    :cond_4
    const-string v5, "record_mode"

    invoke-static {v5, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 172
    :cond_5
    const-string v5, "show_stt_recc_info"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_7

    .line 173
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    # invokes: Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->isAdvancedAndHeadsetMode()Z
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 174
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->newInstance(I)Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "FRAGMENT_DIALOG"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 175
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "MEMO"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 185
    :cond_6
    :goto_2
    const-string v5, "record_mode"

    invoke-static {v5, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 177
    :cond_7
    sget-boolean v5, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_DATA_CHECK_POPUP:Z

    if-eqz v5, :cond_6

    .line 178
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 179
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->needDataCheckHelpDialog(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 180
    .local v3, "needDataCheck":Ljava/lang/String;
    if-eqz v3, :cond_6

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 181
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->newInstance(Ljava/lang/String;)Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "FRAGMENT_DIALOG"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_2

    .line 137
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
