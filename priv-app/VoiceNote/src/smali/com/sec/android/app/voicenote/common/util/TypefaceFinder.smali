.class public Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;
.super Ljava/lang/Object;
.source "TypefaceFinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/util/TypefaceFinder$TypefaceSortByName;
    }
.end annotation


# static fields
.field public static final DEFAULT_FONT_VALUE:Ljava/lang/String; = "default"

.field static final FONT_ASSET_DIR:Ljava/lang/String; = "xml"

.field private static final TAG:Ljava/lang/String; = "TypefaceFinder"


# instance fields
.field public context:Landroid/content/Context;

.field public final mTypefaces:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Typeface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    .line 87
    return-void
.end method


# virtual methods
.method public findMatchingTypeface(Ljava/lang/String;)Lcom/sec/android/app/voicenote/common/util/Typeface;
    .locals 3
    .param p1, "typefaceFilename"    # Ljava/lang/String;

    .prologue
    .line 134
    const/4 v1, 0x0

    .line 135
    .local v1, "typeface":Lcom/sec/android/app/voicenote/common/util/Typeface;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "typeface":Lcom/sec/android/app/voicenote/common/util/Typeface;
    check-cast v1, Lcom/sec/android/app/voicenote/common/util/Typeface;

    .line 137
    .restart local v1    # "typeface":Lcom/sec/android/app/voicenote/common/util/Typeface;
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/Typeface;->getTypefaceFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 143
    :cond_0
    return-object v1

    .line 140
    :cond_1
    const/4 v1, 0x0

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public findMatchingTypefaceByName(Ljava/lang/String;)Lcom/sec/android/app/voicenote/common/util/Typeface;
    .locals 5
    .param p1, "fontName"    # Ljava/lang/String;

    .prologue
    .line 147
    const/4 v1, 0x0

    .line 148
    .local v1, "typeface":Lcom/sec/android/app/voicenote/common/util/Typeface;
    const-string v2, "TypefaceFinder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "findMatchingTypefaceByName:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 151
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "typeface":Lcom/sec/android/app/voicenote/common/util/Typeface;
    check-cast v1, Lcom/sec/android/app/voicenote/common/util/Typeface;

    .line 152
    .restart local v1    # "typeface":Lcom/sec/android/app/voicenote/common/util/Typeface;
    const-string v2, "TypefaceFinder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "findMatchingTypeface:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/Typeface;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/Typeface;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 159
    :cond_0
    return-object v1

    .line 156
    :cond_1
    const/4 v1, 0x0

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public findTypefaces(Landroid/content/res/AssetManager;Ljava/lang/String;)Z
    .locals 6
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;
    .param p2, "fontPackageName"    # Ljava/lang/String;

    .prologue
    .line 50
    const/4 v3, 0x0

    .line 52
    .local v3, "xmlfiles":[Ljava/lang/String;
    :try_start_0
    const-string v4, "xml"

    invoke-virtual {p1, v4}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 57
    const/4 v1, 0x0

    .line 58
    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    .line 60
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "xml/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 61
    .local v2, "in":Ljava/io/InputStream;
    aget-object v4, v3, v1

    invoke-virtual {p0, v4, v2, p2}, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->parseTypefaceXml(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 65
    .end local v2    # "in":Ljava/io/InputStream;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 53
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 54
    .local v0, "ex":Ljava/lang/Exception;
    const/4 v4, 0x0

    .line 67
    .end local v0    # "ex":Ljava/lang/Exception;
    :goto_2
    return v4

    .restart local v1    # "i":I
    :cond_0
    const/4 v4, 0x1

    goto :goto_2

    .line 62
    :catch_1
    move-exception v4

    goto :goto_1
.end method

.method public getMonospaceEntries(Ljava/util/Vector;Ljava/util/Vector;)V
    .locals 3
    .param p1, "entries"    # Ljava/util/Vector;
    .param p2, "entryValues"    # Ljava/util/Vector;

    .prologue
    .line 122
    const-string v2, "default"

    invoke-virtual {p1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 123
    const-string v2, "default"

    invoke-virtual {p2, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 124
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 125
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/Typeface;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/Typeface;->getMonospaceName()Ljava/lang/String;

    move-result-object v1

    .line 126
    .local v1, "s":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 127
    invoke-virtual {p1, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/Typeface;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/Typeface;->getTypefaceFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 124
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 131
    .end local v1    # "s":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public getSansEntries(Ljava/util/Vector;Ljava/util/Vector;Ljava/util/Vector;)V
    .locals 4
    .param p1, "entries"    # Ljava/util/Vector;
    .param p2, "entryValues"    # Ljava/util/Vector;
    .param p3, "fontPackageName"    # Ljava/util/Vector;

    .prologue
    .line 94
    const-string v2, "default"

    invoke-virtual {p1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 95
    const-string v2, "default"

    invoke-virtual {p2, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 96
    const-string v2, ""

    invoke-virtual {p3, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 98
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    new-instance v3, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder$TypefaceSortByName;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder$TypefaceSortByName;-><init>(Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;)V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 99
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/Typeface;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/Typeface;->getSansName()Ljava/lang/String;

    move-result-object v1

    .line 101
    .local v1, "s":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 102
    invoke-virtual {p1, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/Typeface;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/Typeface;->getTypefaceFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/Typeface;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/Typeface;->getFontPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 107
    .end local v1    # "s":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public getSerifEntries(Ljava/util/Vector;Ljava/util/Vector;)V
    .locals 3
    .param p1, "entries"    # Ljava/util/Vector;
    .param p2, "entryValues"    # Ljava/util/Vector;

    .prologue
    .line 110
    const-string v2, "default"

    invoke-virtual {p1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 111
    const-string v2, "default"

    invoke-virtual {p2, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 112
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/Typeface;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/Typeface;->getSerifName()Ljava/lang/String;

    move-result-object v1

    .line 114
    .local v1, "s":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {p1, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 116
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/Typeface;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/Typeface;->getTypefaceFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 112
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    .end local v1    # "s":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public parseTypefaceXml(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 6
    .param p1, "xmlFilename"    # Ljava/lang/String;
    .param p2, "inStream"    # Ljava/io/InputStream;
    .param p3, "fontPackageName"    # Ljava/lang/String;

    .prologue
    .line 72
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v3

    .line 73
    .local v3, "spf":Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v3}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v2

    .line 74
    .local v2, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v4

    .line 75
    .local v4, "xr":Lorg/xml/sax/XMLReader;
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;-><init>()V

    .line 76
    .local v0, "fontParser":Lcom/sec/android/app/voicenote/common/util/TypefaceParser;
    invoke-interface {v4, v0}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 77
    new-instance v5, Lorg/xml/sax/InputSource;

    invoke-direct {v5, p2}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v4, v5}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 78
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/TypefaceParser;->getParsedData()Lcom/sec/android/app/voicenote/common/util/Typeface;

    move-result-object v1

    .line 79
    .local v1, "newTypeface":Lcom/sec/android/app/voicenote/common/util/Typeface;
    invoke-virtual {v1, p1}, Lcom/sec/android/app/voicenote/common/util/Typeface;->setTypefaceFilename(Ljava/lang/String;)V

    .line 80
    invoke-virtual {v1, p3}, Lcom/sec/android/app/voicenote/common/util/Typeface;->setFontPackageName(Ljava/lang/String;)V

    .line 81
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/util/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    .end local v0    # "fontParser":Lcom/sec/android/app/voicenote/common/util/TypefaceParser;
    .end local v1    # "newTypeface":Lcom/sec/android/app/voicenote/common/util/Typeface;
    .end local v2    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v3    # "spf":Ljavax/xml/parsers/SAXParserFactory;
    .end local v4    # "xr":Lorg/xml/sax/XMLReader;
    :goto_0
    return-void

    .line 82
    :catch_0
    move-exception v5

    goto :goto_0
.end method
