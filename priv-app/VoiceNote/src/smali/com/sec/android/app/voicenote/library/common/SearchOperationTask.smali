.class public Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;
.super Landroid/os/AsyncTask;
.source "SearchOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Lcom/sec/android/app/voicenote/library/common/FileOperationProgress;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SearchOperationTask"


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mCountText:Landroid/widget/TextView;

.field protected mFileName:Landroid/widget/TextView;

.field protected mFileOperationListener:Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;

.field protected mPercentText:Landroid/widget/TextView;

.field protected mPrefixFile:Landroid/widget/TextView;

.field protected mProcessing:Landroid/widget/TextView;

.field protected mProgressParam:Lcom/sec/android/app/voicenote/library/common/FileOperationProgress;

.field protected mSearchText:Ljava/lang/String;

.field protected progress:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->progress:I

    .line 57
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mContext:Landroid/content/Context;

    .line 58
    new-instance v0, Lcom/sec/android/app/voicenote/library/common/FileOperationProgress;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/library/common/FileOperationProgress;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mProgressParam:Lcom/sec/android/app/voicenote/library/common/FileOperationProgress;

    .line 59
    return-void
.end method

.method private searchFile(Ljava/lang/String;)V
    .locals 28
    .param p1, "searchText"    # Ljava/lang/String;

    .prologue
    .line 100
    const-string v2, "SearchOperationTask"

    const-string v3, "Search start"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mSearchText:Ljava/lang/String;

    .line 102
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 103
    const-string v5, "recordingtype == \'1\'"

    .line 107
    .local v5, "selection":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v10

    .line 116
    .local v10, "cursor":Landroid/database/Cursor;
    :goto_0
    if-eqz v10, :cond_0

    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 178
    :cond_0
    :goto_1
    return-void

    .line 108
    .end local v10    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v11

    .line 109
    .local v11, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "SearchOperationTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "searchFile - SQLiteException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const/4 v10, 0x0

    .line 114
    .restart local v10    # "cursor":Landroid/database/Cursor;
    goto :goto_0

    .line 111
    .end local v10    # "cursor":Landroid/database/Cursor;
    .end local v11    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v12

    .line 112
    .local v12, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v2, "SearchOperationTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "searchFile - UnsupportedOperationException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const/4 v10, 0x0

    .restart local v10    # "cursor":Landroid/database/Cursor;
    goto :goto_0

    .line 119
    .end local v12    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v19

    .line 120
    .local v19, "item_count":I
    if-nez v19, :cond_2

    .line 121
    const-string v2, "SearchOperationTask"

    const-string v3, "searchFile - cursor count is zero"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 123
    const/4 v10, 0x0

    .line 124
    goto :goto_1

    .line 126
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mProgressParam:Lcom/sec/android/app/voicenote/library/common/FileOperationProgress;

    move/from16 v0, v19

    iput v0, v2, Lcom/sec/android/app/voicenote/library/common/FileOperationProgress;->mTotalTarget:I

    .line 127
    move/from16 v0, v19

    new-array v0, v0, [Landroid/content/ContentValues;

    move-object/from16 v23, v0

    .line 128
    .local v23, "v":[Landroid/content/ContentValues;
    move/from16 v0, v19

    new-array v15, v0, [J

    .line 129
    .local v15, "id_array":[J
    const/16 v16, 0x0

    .line 131
    .local v16, "index":I
    :cond_3
    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 132
    const-string v2, "_data"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 133
    .local v21, "path":Ljava/lang/String;
    if-eqz v21, :cond_6

    .line 134
    const/16 v18, 0x0

    .line 135
    .local v18, "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    const/16 v20, 0x0

    .line 137
    .local v20, "mTextData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    new-instance v13, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    move-object/from16 v0, v21

    invoke-direct {v13, v0}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 138
    .local v13, "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v13}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v18

    .line 139
    if-eqz v18, :cond_6

    move-object/from16 v0, v18

    iget-boolean v2, v0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z

    if-eqz v2, :cond_6

    .line 140
    new-instance v8, Lcom/sec/android/app/voicenote/common/util/SttdHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mContext:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-direct {v8, v0, v2}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;Landroid/content/Context;)V

    .line 142
    .local v8, "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->readSttd()Ljava/io/Serializable;

    move-result-object v20

    .end local v20    # "mTextData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    check-cast v20, Ljava/util/ArrayList;

    .line 143
    .restart local v20    # "mTextData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    if-eqz v20, :cond_3

    .line 146
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    .local v9, "builder_temp":Ljava/lang/StringBuilder;
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_3
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v14, v2, :cond_5

    .line 148
    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget v2, v2, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    if-nez v2, :cond_4

    .line 149
    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    :cond_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 152
    :cond_5
    const-string v2, "_id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    aput-wide v2, v15, v16

    .line 153
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    aput-object v2, v23, v16

    .line 154
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 155
    add-int/lit8 v17, v16, 0x1

    .end local v16    # "index":I
    .local v17, "index":I
    aget-object v2, v23, v16

    const-string v3, "is_memo"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move/from16 v16, v17

    .line 161
    .end local v8    # "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    .end local v9    # "builder_temp":Ljava/lang/StringBuilder;
    .end local v13    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v14    # "i":I
    .end local v17    # "index":I
    .end local v18    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .end local v20    # "mTextData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    .restart local v16    # "index":I
    :cond_6
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mProgressParam:Lcom/sec/android/app/voicenote/library/common/FileOperationProgress;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->progress:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->progress:I

    iput v3, v2, Lcom/sec/android/app/voicenote/library/common/FileOperationProgress;->mCurrentTargetPercentage:I

    goto/16 :goto_2

    .line 157
    .restart local v8    # "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    .restart local v9    # "builder_temp":Ljava/lang/StringBuilder;
    .restart local v13    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .restart local v14    # "i":I
    .restart local v18    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .restart local v20    # "mTextData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    :cond_7
    add-int/lit8 v17, v16, 0x1

    .end local v16    # "index":I
    .restart local v17    # "index":I
    aget-object v2, v23, v16

    const-string v3, "is_memo"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move/from16 v16, v17

    .end local v17    # "index":I
    .restart local v16    # "index":I
    goto :goto_4

    .line 164
    .end local v8    # "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    .end local v9    # "builder_temp":Ljava/lang/StringBuilder;
    .end local v13    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v14    # "i":I
    .end local v18    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .end local v20    # "mTextData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    .end local v21    # "path":Ljava/lang/String;
    :cond_8
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 165
    const/4 v10, 0x0

    .line 167
    const/4 v14, 0x0

    .restart local v14    # "i":I
    :goto_5
    move/from16 v0, v16

    if-ge v14, v0, :cond_9

    .line 169
    :try_start_1
    const-string v24, "_id == ?"

    .line 170
    .local v24, "whereString":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/4 v2, 0x0

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aget-wide v26, v15, v14

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v6, v7

    invoke-static {v3, v4, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v22, v2

    .line 171
    .local v22, "selectionArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aget-object v4, v23, v14

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_2

    .line 167
    .end local v22    # "selectionArgs":[Ljava/lang/String;
    .end local v24    # "whereString":Ljava/lang/String;
    :goto_6
    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    .line 172
    :catch_2
    move-exception v11

    .line 173
    .local v11, "e":Landroid/database/sqlite/SQLiteConstraintException;
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    goto :goto_6

    .line 177
    .end local v11    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    :cond_9
    const-string v2, "SearchOperationTask"

    const-string v3, "search finish"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 40
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 2
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 93
    const/4 v1, 0x0

    aget-object v0, p1, v1

    .line 94
    .local v0, "param":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->searchFile(Ljava/lang/String;)V

    .line 96
    const/4 v1, 0x0

    return-object v1
.end method

.method protected onCancelled()V
    .locals 3

    .prologue
    .line 72
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mFileOperationListener:Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mFileOperationListener:Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;->onCancelled(Ljava/util/ArrayList;I)V

    .line 76
    :cond_0
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 40
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->onCancelled(Ljava/lang/Void;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/Void;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mFileOperationListener:Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mFileOperationListener:Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;->onCancelled(Ljava/util/ArrayList;I)V

    .line 83
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 84
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 40
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const/4 v2, 0x0

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mFileOperationListener:Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mFileOperationListener:Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mSearchText:Ljava/lang/String;

    invoke-interface {v0, v2, v2, v1}, Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;->onCompleted(IILjava/lang/String;)V

    .line 67
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 68
    return-void
.end method

.method public setFileOperationListener(Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/SearchOperationTask;->mFileOperationListener:Lcom/sec/android/app/voicenote/library/common/SearchOperationListener;

    .line 88
    return-void
.end method
