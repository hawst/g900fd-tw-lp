.class public final enum Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;
.super Ljava/lang/Enum;
.source "VNLibraryFragmentController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "librarystate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;

.field public static final enum GRID:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;

.field public static final enum LIST:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 68
    new-instance v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;

    const-string v1, "LIST"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;->LIST:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;

    new-instance v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;

    const-string v1, "GRID"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;->GRID:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;

    .line 67
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;

    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;->LIST:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;->GRID:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;->$VALUES:[Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 67
    const-class v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;->$VALUES:[Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;

    invoke-virtual {v0}, [Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;

    return-object v0
.end method
