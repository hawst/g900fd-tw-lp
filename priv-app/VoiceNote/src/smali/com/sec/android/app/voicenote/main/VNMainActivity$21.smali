.class Lcom/sec/android/app/voicenote/main/VNMainActivity$21;
.super Ljava/lang/Object;
.source "VNMainActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;->displayToolbarHelp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

.field final synthetic val$toolbarHelpCancelBtn:Landroid/widget/ImageButton;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/widget/ImageButton;)V
    .locals 0

    .prologue
    .line 5099
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$21;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    iput-object p2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$21;->val$toolbarHelpCancelBtn:Landroid/widget/ImageButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 5102
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 5103
    .local v1, "keycode":I
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 5104
    .local v0, "action":I
    const/4 v4, 0x4

    if-ne v1, v4, :cond_0

    if-ne v0, v2, :cond_0

    .line 5105
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$21;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$600(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->updateState(Z)V

    .line 5106
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$21;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->onBackPressed()V

    .line 5117
    :goto_0
    return v2

    .line 5110
    :cond_0
    const/16 v4, 0x15

    if-eq v1, v4, :cond_1

    const/16 v4, 0x16

    if-eq v1, v4, :cond_1

    const/16 v4, 0x13

    if-eq v1, v4, :cond_1

    const/16 v4, 0x14

    if-ne v1, v4, :cond_2

    :cond_1
    if-ne v0, v2, :cond_2

    .line 5112
    const-string v3, "VoiceNoteMainActivity"

    const-string v4, "onKey"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 5113
    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$21;->val$toolbarHelpCancelBtn:Landroid/widget/ImageButton;

    const v4, 0x7f0200ea

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    :cond_2
    move v2, v3

    .line 5117
    goto :goto_0
.end method
