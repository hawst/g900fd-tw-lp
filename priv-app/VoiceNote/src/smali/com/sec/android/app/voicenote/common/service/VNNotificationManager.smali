.class public Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
.super Ljava/lang/Object;
.source "VNNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$1;,
        Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;
    }
.end annotation


# static fields
.field public static final REMOTEVIEWSREQ:I = 0x7010002

.field private static final TAG:Ljava/lang/String; = "VNNotificationManager"

.field private static final mSynchronizedObj:Ljava/lang/Object;

.field private static oldtime:Ljava/lang/String;


# instance fields
.field private mIsRepeatBackgroundMode:Z

.field private mNotification:Landroid/app/Notification;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mRemoteViewUpdateThread:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;

.field private mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mSynchronizedObj:Ljava/lang/Object;

    .line 54
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->oldtime:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 1
    .param p1, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mRemoteViewUpdateThread:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mIsRepeatBackgroundMode:Z

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 60
    return-void
.end method

.method private ableShowNoti()Z
    .locals 4

    .prologue
    .line 68
    const/4 v0, 0x0

    .line 70
    .local v0, "retVal":Z
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v2, :cond_0

    move v1, v0

    .line 80
    .end local v0    # "retVal":Z
    .local v1, "retVal":I
    :goto_0
    return v1

    .line 74
    .end local v1    # "retVal":I
    .restart local v0    # "retVal":Z
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v2

    const/16 v3, 0x3e8

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v2

    const/16 v3, 0x3ea

    if-eq v2, v3, :cond_1

    .line 75
    const/4 v0, 0x1

    .line 77
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v2

    const/16 v3, 0x15

    if-eq v2, v3, :cond_2

    .line 78
    const/4 v0, 0x1

    :cond_2
    move v1, v0

    .line 80
    .restart local v1    # "retVal":I
    goto :goto_0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mIsRepeatBackgroundMode:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;)Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-object v0
.end method

.method private createPlayPausedRemoteViews()Landroid/widget/RemoteViews;
    .locals 11

    .prologue
    const v8, 0x7f0e00de

    const v10, 0x7010002

    const/4 v9, 0x0

    .line 313
    new-instance v4, Landroid/widget/RemoteViews;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f030055

    invoke-direct {v4, v6, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 315
    .local v4, "remoteViews":Landroid/widget/RemoteViews;
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPositionForRepeat()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->changeDurationToTimeText(J)Ljava/lang/String;

    move-result-object v5

    .line 316
    .local v5, "timetext":Ljava/lang/String;
    invoke-virtual {v4, v8, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 317
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-static {v6, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->stringForReadTime(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v8, v6}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 319
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSelectedFileName()Ljava/lang/String;

    move-result-object v0

    .line 320
    .local v0, "filename":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 321
    const v6, 0x7f0e00dd

    invoke-virtual {v4, v6, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 324
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->getLaunchIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-static {v6, v10, v7, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 325
    .local v1, "launchRecoderPendingIntent":Landroid/app/PendingIntent;
    const v6, 0x7f0e00dc

    invoke-virtual {v4, v6, v1}, Landroid/widget/RemoteViews;->setLaunchPendingIntent(ILandroid/app/PendingIntent;)V

    .line 327
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.sec.android.app.voicenote.play"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v10, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 328
    .local v2, "playPendingIntent":Landroid/app/PendingIntent;
    const v6, 0x7f0e00df

    invoke-virtual {v4, v6, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 330
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.sec.android.app.voicenote.rec_new"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v10, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 331
    .local v3, "recordPendingIntent":Landroid/app/PendingIntent;
    const v6, 0x7f0e00e0

    invoke-virtual {v4, v6, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 333
    return-object v4
.end method

.method private createPlayPlayingRemoteViews()Landroid/widget/RemoteViews;
    .locals 11

    .prologue
    const v8, 0x7f0e00de

    const v10, 0x7010002

    const/4 v9, 0x0

    .line 337
    new-instance v4, Landroid/widget/RemoteViews;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f030056

    invoke-direct {v4, v6, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 339
    .local v4, "remoteViews":Landroid/widget/RemoteViews;
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->changeDurationToTimeText(J)Ljava/lang/String;

    move-result-object v5

    .line 340
    .local v5, "timetext":Ljava/lang/String;
    invoke-virtual {v4, v8, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 341
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-static {v6, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->stringForReadTime(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v8, v6}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 343
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSelectedFileName()Ljava/lang/String;

    move-result-object v0

    .line 344
    .local v0, "filename":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 345
    const v6, 0x7f0e00dd

    invoke-virtual {v4, v6, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 348
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->getLaunchIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-static {v6, v10, v7, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 349
    .local v1, "launchRecoderPendingIntent":Landroid/app/PendingIntent;
    const v6, 0x7f0e00dc

    invoke-virtual {v4, v6, v1}, Landroid/widget/RemoteViews;->setLaunchPendingIntent(ILandroid/app/PendingIntent;)V

    .line 351
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.sec.android.app.voicenote.play_pause"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v10, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 352
    .local v2, "playPausePendingIntent":Landroid/app/PendingIntent;
    const v6, 0x7f0e00e1

    invoke-virtual {v4, v6, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 354
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.sec.android.app.voicenote.play_stop"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v10, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 355
    .local v3, "playStopPendingIntent":Landroid/app/PendingIntent;
    const v6, 0x7f0e00e2

    invoke-virtual {v4, v6, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 357
    return-object v4
.end method

.method private createRecordPausedRemoteViews()Landroid/widget/RemoteViews;
    .locals 9

    .prologue
    const v6, 0x7f0e00de

    const v8, 0x7010002

    const/4 v7, 0x0

    .line 294
    new-instance v2, Landroid/widget/RemoteViews;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f030057

    invoke-direct {v2, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 296
    .local v2, "remoteViews":Landroid/widget/RemoteViews;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecDurationinLockscreen()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->changeDurationToTimeText(J)Ljava/lang/String;

    move-result-object v3

    .line 297
    .local v3, "timetext":Ljava/lang/String;
    invoke-virtual {v2, v6, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 298
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-static {v4, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->stringForReadTime(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v6, v4}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 301
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->getLaunchIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-static {v4, v8, v5, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 302
    .local v0, "launchRecoderPendingIntent":Landroid/app/PendingIntent;
    const v4, 0x7f0e00dc

    invoke-virtual {v2, v4, v0}, Landroid/widget/RemoteViews;->setLaunchPendingIntent(ILandroid/app/PendingIntent;)V

    .line 304
    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->setSaveAndCancelPendingIntent(Landroid/widget/RemoteViews;)V

    .line 306
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.sec.android.app.voicenote.rec_reume"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v8, v5, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 307
    .local v1, "recordResumePendingIntent":Landroid/app/PendingIntent;
    const v4, 0x7f0e00e6

    invoke-virtual {v2, v4, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 309
    return-object v2
.end method

.method private createRecordRecordingRemoteViews()Landroid/widget/RemoteViews;
    .locals 9

    .prologue
    const v6, 0x7f0e00de

    const v8, 0x7010002

    const/4 v7, 0x0

    .line 276
    new-instance v2, Landroid/widget/RemoteViews;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f030058

    invoke-direct {v2, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 278
    .local v2, "remoteViews":Landroid/widget/RemoteViews;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecDurationinLockscreen()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->changeDurationToTimeText(J)Ljava/lang/String;

    move-result-object v3

    .line 279
    .local v3, "timetext":Ljava/lang/String;
    invoke-virtual {v2, v6, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 280
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-static {v4, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->stringForReadTime(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v6, v4}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 282
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->getLaunchIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-static {v4, v8, v5, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 283
    .local v0, "launchRecoderPendingIntent":Landroid/app/PendingIntent;
    const v4, 0x7f0e00dc

    invoke-virtual {v2, v4, v0}, Landroid/widget/RemoteViews;->setLaunchPendingIntent(ILandroid/app/PendingIntent;)V

    .line 285
    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->setSaveAndCancelPendingIntent(Landroid/widget/RemoteViews;)V

    .line 287
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.sec.android.app.voicenote.rec_pause"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v8, v5, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 288
    .local v1, "recordPausePendingIntent":Landroid/app/PendingIntent;
    const v4, 0x7f0e00e8

    invoke-virtual {v2, v4, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 290
    return-object v2
.end method

.method private createStandByRemoteViews()Landroid/widget/RemoteViews;
    .locals 8

    .prologue
    const v7, 0x7010002

    const/4 v6, 0x0

    .line 361
    new-instance v2, Landroid/widget/RemoteViews;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f030059

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 362
    .local v2, "remoteViews":Landroid/widget/RemoteViews;
    const v3, 0x7f0e00e3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const v5, 0x7f0b0125

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 364
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->getLaunchIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-static {v3, v7, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 365
    .local v0, "launchRecoderPendingIntent":Landroid/app/PendingIntent;
    const v3, 0x7f0e00dc

    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setLaunchPendingIntent(ILandroid/app/PendingIntent;)V

    .line 367
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.sec.android.app.voicenote.rec_new"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v7, v4, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 368
    .local v1, "recordPendingIntent":Landroid/app/PendingIntent;
    const v3, 0x7f0e00e0

    invoke-virtual {v2, v3, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 370
    return-object v2
.end method

.method private makeRecNotification(I)Landroid/app/Notification;
    .locals 7
    .param p1, "icon"    # I

    .prologue
    const/4 v6, 0x1

    .line 259
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-direct {v0, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 261
    .local v0, "builder":Landroid/app/Notification$Builder;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 262
    invoke-virtual {v0, v6}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 263
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const v3, 0x7010002

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->getLaunchIntent()Landroid/content/Intent;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 264
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0014

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 265
    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 266
    invoke-virtual {v0, v6}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    .line 267
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 269
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 270
    .local v1, "tN":Landroid/app/Notification;
    const/4 v2, 0x4

    iput v2, v1, Landroid/app/Notification;->twQuickPanelEvent:I

    .line 272
    return-object v1
.end method

.method private setSaveAndCancelPendingIntent(Landroid/widget/RemoteViews;)V
    .locals 7
    .param p1, "remoteViews"    # Landroid/widget/RemoteViews;

    .prologue
    const v6, 0x7010002

    const/4 v5, 0x0

    .line 453
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.voicenote.rec_save_keygard"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v6, v3, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 454
    .local v1, "savePendingIntent":Landroid/app/PendingIntent;
    const v2, 0x7f0e00e5

    invoke-virtual {p1, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 460
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.voicenote.rec_cancel.keygard"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v6, v3, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 461
    .local v0, "cancelPendingIntent":Landroid/app/PendingIntent;
    const v2, 0x7f0e00e4

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 462
    return-void
.end method


# virtual methods
.method public CreateRemoteView()Landroid/widget/RemoteViews;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v0

    const/16 v1, 0x3eb

    if-ne v0, v1, :cond_0

    .line 85
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->createRecordRecordingRemoteViews()Landroid/widget/RemoteViews;

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v0

    const/16 v1, 0x3ec

    if-ne v0, v1, :cond_1

    .line 87
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->createRecordPausedRemoteViews()Landroid/widget/RemoteViews;

    move-result-object v0

    goto :goto_0

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v0

    const/16 v1, 0x18

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v0

    const/16 v1, 0x16

    if-ne v0, v1, :cond_3

    .line 89
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->createPlayPausedRemoteViews()Landroid/widget/RemoteViews;

    move-result-object v0

    goto :goto_0

    .line 90
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v0

    const/16 v1, 0x17

    if-ne v0, v1, :cond_4

    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->createPlayPlayingRemoteViews()Landroid/widget/RemoteViews;

    move-result-object v0

    goto :goto_0

    .line 93
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->createStandByRemoteViews()Landroid/widget/RemoteViews;

    move-result-object v0

    goto :goto_0
.end method

.method public UpdateStart()V
    .locals 3

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_1

    .line 387
    :cond_0
    :goto_0
    return-void

    .line 377
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v0

    const/16 v1, 0x3eb

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v0

    const/16 v1, 0x17

    if-ne v0, v1, :cond_0

    .line 378
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mIsRepeatBackgroundMode:Z

    .line 379
    sget-object v1, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mSynchronizedObj:Ljava/lang/Object;

    monitor-enter v1

    .line 380
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mRemoteViewUpdateThread:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;

    if-nez v0, :cond_3

    .line 381
    new-instance v0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;-><init>(Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$1;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mRemoteViewUpdateThread:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mRemoteViewUpdateThread:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->setDaemon(Z)V

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mRemoteViewUpdateThread:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->start()V

    .line 385
    :cond_3
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public UpdateStop()V
    .locals 2

    .prologue
    .line 390
    sget-object v1, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mSynchronizedObj:Ljava/lang/Object;

    monitor-enter v1

    .line 391
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mRemoteViewUpdateThread:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mRemoteViewUpdateThread:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mRemoteViewUpdateThread:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;->interrupt()V

    .line 393
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mRemoteViewUpdateThread:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager$RemoteViewUpdateThread;

    .line 395
    :cond_0
    monitor-exit v1

    .line 396
    return-void

    .line 395
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public changeDurationToTimeText(J)Ljava/lang/String;
    .locals 11
    .param p1, "time"    # J

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 466
    const-wide/16 v6, 0x3e8

    div-long v6, p1, v6

    long-to-int v4, v6

    .line 467
    .local v4, "second":I
    div-int/lit16 v0, v4, 0xe10

    .line 468
    .local v0, "h":I
    div-int/lit8 v5, v4, 0x3c

    rem-int/lit8 v1, v5, 0x3c

    .line 469
    .local v1, "m":I
    rem-int/lit8 v3, v4, 0x3c

    .line 470
    .local v3, "s":I
    if-lez v0, :cond_0

    .line 471
    const-string v5, "%02d:%02d:%02d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 475
    .local v2, "retString":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 473
    .end local v2    # "retString":Ljava/lang/String;
    :cond_0
    const-string v5, "%02d:%02d"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "retString":Ljava/lang/String;
    goto :goto_0
.end method

.method public getIcon()I
    .locals 3

    .prologue
    const v0, 0x7f0200db

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v1

    const/16 v2, 0x3eb

    if-ne v1, v2, :cond_1

    .line 154
    :cond_0
    :goto_0
    return v0

    .line 147
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v1

    const/16 v2, 0x3ec

    if-eq v1, v2, :cond_0

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v1

    const/16 v2, 0x18

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v1

    const/16 v2, 0x16

    if-ne v1, v2, :cond_3

    .line 150
    :cond_2
    const v0, 0x7f0200dc

    goto :goto_0

    .line 151
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v1

    const/16 v2, 0x17

    if-ne v1, v2, :cond_0

    .line 152
    const v0, 0x7f0200dd

    goto :goto_0
.end method

.method public getLaunchIntent()Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x10000000

    const/high16 v3, 0x4000000

    .line 97
    const/4 v0, 0x0

    .line 98
    .local v0, "launchRecorderIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v1

    const/16 v2, 0x3eb

    if-ne v1, v2, :cond_0

    .line 99
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "launchRecorderIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const-class v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 100
    .restart local v0    # "launchRecorderIntent":Landroid/content/Intent;
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 103
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 104
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 105
    const-string v1, "activitymode"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 106
    const-string v1, "from"

    const-string v2, "com.sec.android.app.voicenote.vrremoteview"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    :goto_0
    return-object v0

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v1

    const/16 v2, 0x3ec

    if-ne v1, v2, :cond_1

    .line 108
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "launchRecorderIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const-class v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 109
    .restart local v0    # "launchRecorderIntent":Landroid/content/Intent;
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 112
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 113
    const-string v1, "activitymode"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 114
    const-string v1, "from"

    const-string v2, "com.sec.android.app.voicenote.vrremoteview"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 115
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v1

    const/16 v2, 0x18

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v1

    const/16 v2, 0x16

    if-ne v1, v2, :cond_3

    .line 116
    :cond_2
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "launchRecorderIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const-class v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 117
    .restart local v0    # "launchRecorderIntent":Landroid/content/Intent;
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 120
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 121
    const-string v1, "activitymode"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 122
    const-string v1, "from"

    const-string v2, "com.sec.android.app.voicenote.vrremoteview"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 123
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v1

    const/16 v2, 0x17

    if-ne v1, v2, :cond_4

    .line 124
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "launchRecorderIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const-class v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 125
    .restart local v0    # "launchRecorderIntent":Landroid/content/Intent;
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 128
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 129
    const-string v1, "activitymode"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 130
    const-string v1, "from"

    const-string v2, "com.sec.android.app.voicenote.vrremoteview"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 132
    :cond_4
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "launchRecorderIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const-class v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 133
    .restart local v0    # "launchRecorderIntent":Landroid/content/Intent;
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 136
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 137
    const-string v1, "activitymode"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 138
    const-string v1, "from"

    const-string v2, "com.sec.android.app.voicenote.vrremoteview"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0
.end method

.method public hideNotification()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 214
    const-string v0, "VNNotificationManager"

    const-string v1, "hideNotification()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->UpdateStop()V

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotificationManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotificationManager:Landroid/app/NotificationManager;

    const v1, 0x7010002

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 218
    iput-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotificationManager:Landroid/app/NotificationManager;

    .line 219
    iput-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_1

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopForeground(Z)V

    .line 224
    :cond_1
    return v2
.end method

.method public isNotificationShow()Z
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotificationManager:Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    if-nez v0, :cond_0

    .line 480
    const/4 v0, 0x0

    .line 482
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public refreshNotification()Z
    .locals 6

    .prologue
    const v5, 0x7010002

    const/4 v2, 0x0

    .line 182
    const-string v3, "VNNotificationManager"

    const-string v4, "refreshNotification()"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotificationManager:Landroid/app/NotificationManager;

    if-nez v3, :cond_1

    .line 210
    :cond_0
    :goto_0
    return v2

    .line 186
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->UpdateStop()V

    .line 192
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->getIcon()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->makeRecNotification(I)Landroid/app/Notification;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->CreateRemoteView()Landroid/widget/RemoteViews;

    move-result-object v1

    .line 196
    .local v1, "remoteViews":Landroid/widget/RemoteViews;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    if-eqz v3, :cond_0

    .line 199
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    iput-object v1, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 201
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    invoke-virtual {v2, v5, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 204
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const v3, 0x7010002

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->startForeground(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->UpdateStart()V

    .line 210
    const/4 v2, 0x1

    goto :goto_0

    .line 205
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 207
    const-string v2, "VNNotificationManager"

    const-string v3, "error occurred while playStop() : "

    invoke-static {v2, v3, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public release()V
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->UpdateStop()V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 65
    return-void
.end method

.method public repeatPlaybackBackground()V
    .locals 1

    .prologue
    .line 487
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mIsRepeatBackgroundMode:Z

    .line 488
    return-void
.end method

.method public showNotification()Z
    .locals 5

    .prologue
    const v4, 0x7010002

    .line 158
    const-string v2, "VNNotificationManager"

    const-string v3, "showNotification()"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->ableShowNoti()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 177
    :goto_0
    return v2

    .line 161
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const-string v3, "notification"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotificationManager:Landroid/app/NotificationManager;

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->getIcon()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->makeRecNotification(I)Landroid/app/Notification;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->CreateRemoteView()Landroid/widget/RemoteViews;

    move-result-object v1

    .line 167
    .local v1, "remoteViews":Landroid/widget/RemoteViews;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    iput-object v1, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    invoke-virtual {v2, v4, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 171
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const v3, 0x7010002

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->startForeground(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->UpdateStart()V

    .line 177
    const/4 v2, 0x1

    goto :goto_0

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 174
    const-string v2, "VNNotificationManager"

    const-string v3, "error in : mVNService.startForeground"

    invoke-static {v2, v3, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public updateNotification()V
    .locals 5

    .prologue
    .line 228
    const-string v2, "VNNotificationManager"

    const-string v3, "updateNotification()"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    const-string v1, ""

    .line 230
    .local v1, "timetext":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotificationManager:Landroid/app/NotificationManager;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v2, :cond_1

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v2

    const/16 v3, 0x3eb

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v2

    const/16 v3, 0x3ec

    if-ne v2, v3, :cond_5

    .line 234
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecDurationinLockscreen()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->changeDurationToTimeText(J)Ljava/lang/String;

    move-result-object v1

    .line 241
    :goto_1
    sget-object v2, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->oldtime:Ljava/lang/String;

    if-eqz v2, :cond_3

    sget-object v2, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->oldtime:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 244
    :cond_3
    sput-object v1, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->oldtime:Ljava/lang/String;

    .line 246
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isOpenStatusBar()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isLockScreen()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 248
    :cond_4
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    iget-object v2, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v3, 0x7f0e00de

    invoke-virtual {v2, v3, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 249
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    iget-object v2, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v3, 0x7f0e00de

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-static {v4, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->stringForReadTime(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 250
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotificationManager:Landroid/app/NotificationManager;

    const v3, 0x7010002

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mNotification:Landroid/app/Notification;

    invoke-virtual {v2, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 251
    :catch_0
    move-exception v0

    .line 252
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "VNNotificationManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notification exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 235
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v2

    const/16 v3, 0x18

    if-eq v2, v3, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v2

    const/16 v3, 0x16

    if-eq v2, v3, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v2

    const/16 v3, 0x17

    if-ne v2, v3, :cond_7

    .line 237
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->mVNService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPositionForRepeat()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->changeDurationToTimeText(J)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 239
    :cond_7
    const-wide/16 v2, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->changeDurationToTimeText(J)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1
.end method
