.class Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;
.super Ljava/lang/Object;
.source "VNSTTFragment.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getDialogCallback()Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)V
    .locals 0

    .prologue
    .line 483
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized onDeleteWord()V
    .locals 6

    .prologue
    .line 527
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    if-nez v2, :cond_0

    .line 528
    const-string v2, "VNSTTFragment"

    const-string v3, "onDeleteWord: callback is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548
    :goto_0
    monitor-exit p0

    return-void

    .line 531
    :cond_0
    :try_start_1
    const-string v2, "VNSTTFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDeleteWord select index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$200(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", text size : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$200(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I

    move-result v2

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 534
    const-string v2, "VNSTTFragment"

    const-string v3, "onDeleteWord: mSelectIndex is larger than mTextData size"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 527
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 537
    :cond_1
    :try_start_2
    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 538
    .local v1, "mTextDatasize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 539
    const-string v3, "VNSTTFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "before mTextData : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v2, v2, v5

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " - "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 541
    :cond_2
    const-string v3, "VNSTTFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "remove :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$200(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v2, v2, v5

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 544
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v1, :cond_3

    .line 545
    const-string v3, "VNSTTFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "after mTextData : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v2, v2, v5

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " - "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 547
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->updateAllWordOnScreen()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public onFinish()V
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$502(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    const/4 v1, -0x1

    # setter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$202(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;I)I

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->updateAllWordOnScreen()V

    .line 499
    return-void
.end method

.method public declared-synchronized onResult(I)V
    .locals 3
    .param p1, "selectFromDialog"    # I

    .prologue
    .line 486
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    if-nez v0, :cond_0

    .line 487
    const-string v0, "VNSTTFragment"

    const-string v1, "onResult1: callback is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 492
    :goto_0
    monitor-exit p0

    return-void

    .line 490
    :cond_0
    :try_start_1
    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$200(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/TextData;

    const/4 v1, 0x0

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/TextData;->swapText(II)V

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->updateAllWordOnScreen()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 486
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onResult(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "newLine"    # Z

    .prologue
    .line 503
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    if-nez v0, :cond_0

    .line 504
    const-string v0, "VNSTTFragment"

    const-string v1, "onResult2: callback is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 523
    :goto_0
    monitor-exit p0

    return-void

    .line 507
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$200(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I

    move-result v0

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$200(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I

    move-result v0

    if-gez v0, :cond_2

    .line 508
    :cond_1
    const-string v0, "VNSTTFragment"

    const-string v1, "onResult: mSelectIndex is larger than mTextData size or mSelectIndex is less than 0"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 503
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 511
    :cond_2
    :try_start_2
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 512
    const-string v0, "VNSTTFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResult2: data is empty selectid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$200(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$200(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 517
    :goto_1
    if-eqz p2, :cond_3

    .line 518
    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$200(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v1, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$200(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v0, v0, v4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    .line 522
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->updateAllWordOnScreen()V

    goto/16 :goto_0

    .line 515
    :cond_4
    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$300()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->access$200(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
