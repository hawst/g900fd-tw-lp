.class Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;
.super Landroid/os/Handler;
.source "VoiceNoteService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0

    .prologue
    .line 2438
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v8, 0x7d0

    const/16 v2, 0x3ec

    const/4 v6, 0x0

    const/4 v1, -0x2

    const/4 v5, 0x1

    .line 2442
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 2468
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 2469
    return-void

    .line 2444
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 2445
    const-string v0, "VoiceNoteService"

    const-string v1, "enough pause time is over"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2446
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setWakeLock(Landroid/content/Context;I)V

    .line 2447
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const v2, 0x7f0b0111

    new-array v3, v5, [Ljava/lang/Object;

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 2449
    const-wide/16 v0, 0x2710

    invoke-virtual {p0, v5, v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 2453
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 2454
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecording()Z

    .line 2455
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    .line 2457
    :cond_1
    invoke-virtual {p0, v1, v8, v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 2460
    :pswitch_2
    invoke-virtual {p0, v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;->removeMessages(I)V

    .line 2461
    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;->removeMessages(I)V

    .line 2462
    invoke-virtual {p0, v1, v8, v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 2465
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setWakeLock(Landroid/content/Context;I)V

    goto :goto_0

    .line 2442
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
