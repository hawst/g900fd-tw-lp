.class public Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;
.super Landroid/app/Fragment;
.source "VNPlayerControlButtonFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "VNPlayerControlFragment"

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# instance fields
.field final hoverPopupListener:Landroid/widget/HoverPopupWindow$HoverPopupListener;

.field mAudioManager:Landroid/media/AudioManager;

.field private mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field private mModeButton:Landroid/widget/ImageButton;

.field private mNextButton:Landroid/widget/ImageButton;

.field private mNextSelectedTitle:Ljava/lang/String;

.field private mPauseButton:Landroid/widget/ImageButton;

.field private mPlayButton:Landroid/widget/ImageButton;

.field private mPreSelectedTitle:Ljava/lang/String;

.field private mPreviousButton:Landroid/widget/ImageButton;

.field private onHoverListener:Landroid/view/View$OnHoverListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPlayButton:Landroid/widget/ImageButton;

    .line 57
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPauseButton:Landroid/widget/ImageButton;

    .line 59
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    .line 60
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    .line 61
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mModeButton:Landroid/widget/ImageButton;

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreSelectedTitle:Ljava/lang/String;

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextSelectedTitle:Ljava/lang/String;

    .line 66
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mAudioManager:Landroid/media/AudioManager;

    .line 167
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->onHoverListener:Landroid/view/View$OnHoverListener;

    .line 179
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->hoverPopupListener:Landroid/widget/HoverPopupWindow$HoverPopupListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->setPlayIds()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreSelectedTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextSelectedTitle:Ljava/lang/String;

    return-object v0
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 269
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 270
    return-void
.end method

.method public static releaseService()V
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 274
    return-void
.end method

.method private setPlayIds()V
    .locals 2

    .prologue
    .line 442
    const-string v0, "VNPlayerControlFragment"

    const-string v1, "setPlayIds()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getFileTitle(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreSelectedTitle:Ljava/lang/String;

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getFileTitle(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextSelectedTitle:Ljava/lang/String;

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextSelectedTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreSelectedTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 447
    return-void
.end method

.method private updateButtons()V
    .locals 8

    .prologue
    const/16 v7, 0xa

    const v3, 0x7f0b010b

    const v6, 0x7f0b0082

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 385
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    if-nez v1, :cond_1

    .line 439
    :cond_0
    :goto_0
    return-void

    .line 387
    :cond_1
    const-string v1, "skip_interval"

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 388
    const/4 v0, 0x0

    .line 390
    .local v0, "contentdescription":Ljava/lang/String;
    const-string v1, "skip_interval_value"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 424
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 425
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    const v2, 0x7f020171

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 426
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 427
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 428
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    const v2, 0x7f020139

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 429
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 392
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 393
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    const v2, 0x7f02017a

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 394
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 395
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 396
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    const v2, 0x7f020142

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 397
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 400
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 401
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    const v2, 0x7f020172

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 402
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 403
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 404
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    const v2, 0x7f02013a

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 405
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 408
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const/16 v3, 0x1e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 409
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    const v2, 0x7f020176

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 410
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const/16 v3, 0x1e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 412
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    const v2, 0x7f02013e

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 413
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 416
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const/16 v3, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 417
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    const v2, 0x7f02017e

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 418
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 419
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const/16 v3, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 420
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    const v2, 0x7f020146

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 421
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 434
    .end local v0    # "contentdescription":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    const v2, 0x7f020171

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 435
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0b00f6

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 436
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    const v2, 0x7f020139

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 437
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0b00cc

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 390
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 252
    move-object v1, p1

    check-cast v1, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 253
    const-string v1, "audio"

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mAudioManager:Landroid/media/AudioManager;

    move-object v1, p1

    .line 254
    check-cast v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionMode()Landroid/view/ActionMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 255
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 256
    .local v0, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {v0, p0}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 257
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 259
    .end local v0    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 260
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v6, 0x7f0e0096

    const v5, 0x7f0e0095

    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 229
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 230
    .local v1, "id":I
    const/4 v0, 0x0

    .line 231
    .local v0, "bchangeFocus":Z
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallIdle(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallActive(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eq v5, v1, :cond_0

    if-ne v6, v1, :cond_1

    .line 232
    :cond_0
    const-string v2, "VNPlayerControlFragment"

    const-string v3, "onControlButtonSelected : library_view_fragment : isCallIdle"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0b00d6

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 248
    :goto_0
    return-void

    .line 235
    :cond_1
    if-ne v5, v1, :cond_4

    .line 236
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    .line 237
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 239
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 246
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onControlButtonSelected(IJ)Z

    goto :goto_0

    .line 240
    :cond_4
    if-ne v6, v1, :cond_3

    .line 241
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v0, 0x1

    .line 242
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 243
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 244
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x3031

    const/4 v7, 0x1

    .line 70
    const-string v5, "VNPlayerControlFragment"

    const-string v6, "onCreateView"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const v1, 0x7f03003c

    .line 73
    .local v1, "layout":I
    const/4 v5, 0x0

    invoke-virtual {p1, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 75
    .local v4, "view":Landroid/view/View;
    const v5, 0x7f0e0095

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPauseButton:Landroid/widget/ImageButton;

    .line 76
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPauseButton:Landroid/widget/ImageButton;

    if-eqz v5, :cond_0

    .line 77
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    :cond_0
    const v5, 0x7f0e0096

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPlayButton:Landroid/widget/ImageButton;

    .line 80
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPlayButton:Landroid/widget/ImageButton;

    if-eqz v5, :cond_1

    .line 81
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    :cond_1
    const v5, 0x7f0e0097

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    .line 84
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    if-eqz v5, :cond_2

    .line 85
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 89
    :cond_2
    const v5, 0x7f0e0094

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    .line 90
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    if-eqz v5, :cond_3

    .line 91
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 95
    :cond_3
    const v5, 0x7f0e009c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mModeButton:Landroid/widget/ImageButton;

    .line 96
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mModeButton:Landroid/widget/ImageButton;

    if-eqz v5, :cond_6

    .line 97
    sget-boolean v5, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_BEAMFORMING:Z

    if-nez v5, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITH_SVOICE(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITHOUT_SVOICE()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 100
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 103
    :cond_5
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 104
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, v9}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 105
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mModeButton:Landroid/widget/ImageButton;

    const v6, 0x3ecccccd    # 0.4f

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 110
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isHoveringUI(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 114
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPauseButton:Landroid/widget/ImageButton;

    if-eqz v5, :cond_7

    .line 115
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 116
    .local v0, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_7

    .line 117
    invoke-virtual {v0, v8}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 121
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPlayButton:Landroid/widget/ImageButton;

    if-eqz v5, :cond_8

    .line 122
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 123
    .restart local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_8

    .line 124
    invoke-virtual {v0, v8}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 128
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    if-eqz v5, :cond_9

    .line 129
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 130
    .restart local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_9

    .line 131
    invoke-virtual {v0, v8}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 135
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    if-eqz v5, :cond_a

    .line 136
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 137
    .restart local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_a

    .line 138
    invoke-virtual {v0, v8}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 143
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    .line 144
    .local v2, "nextHpw":Landroid/widget/HoverPopupWindow;
    if-eqz v2, :cond_b

    .line 145
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->onHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 146
    invoke-virtual {v2, v7}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 147
    invoke-virtual {v2, v7}, Landroid/widget/HoverPopupWindow;->setInfoPickerMoveEabled(Z)V

    .line 148
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->hoverPopupListener:Landroid/widget/HoverPopupWindow$HoverPopupListener;

    invoke-virtual {v2, v5}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 151
    :cond_b
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    .line 152
    .local v3, "preHpw":Landroid/widget/HoverPopupWindow;
    if-eqz v3, :cond_c

    .line 153
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->onHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 154
    invoke-virtual {v3, v7}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 155
    invoke-virtual {v3, v7}, Landroid/widget/HoverPopupWindow;->setInfoPickerMoveEabled(Z)V

    .line 156
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->hoverPopupListener:Landroid/widget/HoverPopupWindow$HoverPopupListener;

    invoke-virtual {v3, v5}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 159
    :cond_c
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachOnlyMode()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 160
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mModeButton:Landroid/widget/ImageButton;

    if-eqz v5, :cond_d

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mModeButton:Landroid/widget/ImageButton;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 164
    .end local v2    # "nextHpw":Landroid/widget/HoverPopupWindow;
    .end local v3    # "preHpw":Landroid/widget/HoverPopupWindow;
    :cond_d
    return-object v4

    .line 107
    :cond_e
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 108
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mModeButton:Landroid/widget/ImageButton;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setAlpha(F)V

    goto/16 :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 214
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPauseButton:Landroid/widget/ImageButton;

    .line 215
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPlayButton:Landroid/widget/ImageButton;

    .line 216
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    .line 217
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    .line 218
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 219
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 265
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 266
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 14
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 298
    iget-object v12, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    if-eqz v12, :cond_10

    iget-object v12, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    if-eqz v12, :cond_10

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v12

    iget-object v13, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    invoke-virtual {v13}, Landroid/widget/ImageButton;->getId()I

    move-result v13

    if-eq v12, v13, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v12

    iget-object v13, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v13}, Landroid/widget/ImageButton;->getId()I

    move-result v13

    if-ne v12, v13, :cond_10

    .line 300
    :cond_0
    const/4 v0, 0x0

    .line 301
    .local v0, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    iget-object v12, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v12}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getBookmark()Ljava/util/ArrayList;

    move-result-object v0

    .line 303
    if-nez v0, :cond_1

    .line 304
    const/4 v12, 0x1

    .line 381
    .end local v0    # "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    :goto_0
    return v12

    .line 306
    .restart local v0    # "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    :cond_1
    sget-object v12, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v12, :cond_2

    .line 307
    const/4 v12, 0x0

    goto :goto_0

    .line 310
    :cond_2
    sget-object v12, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v12}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v12

    const/16 v13, 0x15

    if-ne v12, v13, :cond_3

    .line 311
    const-string v12, "VNPlayerControlFragment"

    const-string v13, "Button is return when The ( Player Event ) State is STOPPED and COMPLETE"

    invoke-static {v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const/4 v12, 0x0

    goto :goto_0

    .line 315
    :cond_3
    sget-object v12, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v12}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v7

    .line 316
    .local v7, "isPlaying":Z
    sget-object v12, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v12}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPlayerDuration()I

    move-result v12

    int-to-long v2, v12

    .line 317
    .local v2, "endTime":J
    sget-object v12, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v12}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v12

    int-to-long v8, v12

    .line 319
    .local v8, "position":J
    const-wide/16 v12, 0x0

    cmp-long v12, v2, v12

    if-nez v12, :cond_4

    .line 320
    const/4 v12, 0x0

    goto :goto_0

    .line 323
    :cond_4
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 324
    iget-object v12, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    if-eqz v12, :cond_9

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v12

    iget-object v13, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v13}, Landroid/widget/ImageButton;->getId()I

    move-result v13

    if-ne v12, v13, :cond_9

    .line 325
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 326
    .local v1, "bookmarkssize":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-ge v6, v1, :cond_5

    .line 327
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v12}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v12

    int-to-long v4, v12

    .line 328
    .local v4, "first":J
    cmp-long v12, v4, v8

    if-lez v12, :cond_8

    .line 329
    move-wide v8, v4

    .line 343
    .end local v1    # "bookmarkssize":I
    .end local v4    # "first":J
    .end local v6    # "i":I
    :cond_5
    :goto_2
    cmp-long v12, v8, v2

    if-ltz v12, :cond_c

    .line 344
    if-eqz v7, :cond_b

    .line 345
    const-wide/16 v12, 0xbb8

    sub-long v8, v2, v12

    .line 350
    :goto_3
    const-wide/16 v12, 0x0

    cmp-long v12, v8, v12

    if-gez v12, :cond_6

    .line 351
    const-wide/16 v8, 0x0

    .line 357
    :cond_6
    :goto_4
    const/4 v10, 0x0

    .line 358
    .local v10, "repeatStartTime":I
    sget-object v12, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v12}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRepeatTime()[I

    move-result-object v11

    .line 359
    .local v11, "repeatTime":[I
    const/4 v12, 0x0

    aget v12, v11, v12

    if-ltz v12, :cond_f

    .line 360
    const/4 v12, 0x1

    aget v12, v11, v12

    const/4 v13, 0x0

    aget v13, v11, v13

    if-le v12, v13, :cond_d

    .line 361
    const/4 v12, 0x0

    aget v10, v11, v12

    .line 362
    const/4 v12, 0x0

    aget v12, v11, v12

    int-to-long v12, v12

    cmp-long v12, v12, v8

    if-gtz v12, :cond_7

    const/4 v12, 0x1

    aget v12, v11, v12

    int-to-long v12, v12

    cmp-long v12, v12, v8

    if-gez v12, :cond_f

    .line 363
    :cond_7
    const-string v12, "VNPlayerControlFragment"

    const-string v13, "onLongClick : bookmark jump is cancel, out of repeat range"

    invoke-static {v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 326
    .end local v10    # "repeatStartTime":I
    .end local v11    # "repeatTime":[I
    .restart local v1    # "bookmarkssize":I
    .restart local v4    # "first":J
    .restart local v6    # "i":I
    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 333
    .end local v1    # "bookmarkssize":I
    .end local v4    # "first":J
    .end local v6    # "i":I
    :cond_9
    iget-object v12, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    if-eqz v12, :cond_5

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v12

    iget-object v13, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreviousButton:Landroid/widget/ImageButton;

    invoke-virtual {v13}, Landroid/widget/ImageButton;->getId()I

    move-result v13

    if-ne v12, v13, :cond_5

    .line 334
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v12

    add-int/lit8 v6, v12, -0x1

    .restart local v6    # "i":I
    :goto_5
    if-ltz v6, :cond_5

    .line 335
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v12}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v12

    int-to-long v4, v12

    .line 336
    .restart local v4    # "first":J
    cmp-long v12, v4, v8

    if-gez v12, :cond_a

    .line 337
    move-wide v8, v4

    .line 338
    goto :goto_2

    .line 334
    :cond_a
    add-int/lit8 v6, v6, -0x1

    goto :goto_5

    .line 347
    .end local v4    # "first":J
    .end local v6    # "i":I
    :cond_b
    move-wide v8, v2

    goto :goto_3

    .line 353
    :cond_c
    const-wide/16 v12, 0x0

    cmp-long v12, v8, v12

    if-gez v12, :cond_6

    .line 354
    const-wide/16 v8, 0x0

    goto :goto_4

    .line 367
    .restart local v10    # "repeatStartTime":I
    .restart local v11    # "repeatTime":[I
    :cond_d
    const/4 v12, 0x1

    aget v10, v11, v12

    .line 368
    const/4 v12, 0x1

    aget v12, v11, v12

    int-to-long v12, v12

    cmp-long v12, v12, v8

    if-gtz v12, :cond_e

    const/4 v12, 0x0

    aget v12, v11, v12

    int-to-long v12, v12

    cmp-long v12, v12, v8

    if-gez v12, :cond_f

    .line 369
    :cond_e
    const-string v12, "VNPlayerControlFragment"

    const-string v13, "onLongClick : bookmark jump is cancel, out of repeat range"

    invoke-static {v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 374
    :cond_f
    if-eqz v7, :cond_11

    .line 375
    sget-object v12, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    long-to-int v13, v8

    invoke-virtual {v12, v13}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 381
    .end local v0    # "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    .end local v2    # "endTime":J
    .end local v7    # "isPlaying":Z
    .end local v8    # "position":J
    .end local v10    # "repeatStartTime":I
    .end local v11    # "repeatTime":[I
    :cond_10
    :goto_6
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 377
    .restart local v0    # "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    .restart local v2    # "endTime":J
    .restart local v7    # "isPlaying":Z
    .restart local v8    # "position":J
    .restart local v10    # "repeatStartTime":I
    .restart local v11    # "repeatTime":[I
    :cond_11
    iget-object v12, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    long-to-int v13, v8

    sub-int/2addr v13, v10

    invoke-interface {v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onMoveToBookmark(I)V

    .line 378
    sget-object v12, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    long-to-int v13, v8

    invoke-virtual {v12, v13}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    goto :goto_6
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 223
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 224
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->refreshControl()V

    .line 225
    return-void
.end method

.method public refreshControl()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 277
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v2, :cond_1

    .line 294
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v0

    .line 282
    .local v0, "id":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 284
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v2

    const/16 v3, 0x17

    if-eq v2, v3, :cond_2

    .line 285
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 286
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 292
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->updateButtons()V

    goto :goto_0

    .line 288
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 289
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1
.end method
