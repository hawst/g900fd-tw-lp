.class Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$1;
.super Ljava/lang/Object;
.source "VNEditSTTFragment.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 119
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 128
    :goto_0
    return v0

    .line 122
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsLongPressed:Z
    invoke-static {v2, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$102(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Z)Z

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsTouching:Z
    invoke-static {v2, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$202(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Z)Z

    .line 124
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onLongClick : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsLongPressed:Z
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 126
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onSTTEditPopup(Z)V

    :cond_1
    move v0, v1

    .line 128
    goto :goto_0
.end method
