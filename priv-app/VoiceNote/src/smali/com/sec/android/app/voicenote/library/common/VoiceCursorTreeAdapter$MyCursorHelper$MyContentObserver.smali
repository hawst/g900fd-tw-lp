.class Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyContentObserver;
.super Landroid/database/ContentObserver;
.source "VoiceCursorTreeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyContentObserver"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;)V
    .locals 1

    .prologue
    .line 339
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyContentObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    .line 340
    iget-object v0, p1, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->this$0:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->access$100(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;)Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 341
    return-void
.end method


# virtual methods
.method public deliverSelfNotifications()Z
    .locals 1

    .prologue
    .line 345
    const/4 v0, 0x1

    return v0
.end method

.method public onChange(Z)V
    .locals 6
    .param p1, "selfChange"    # Z

    .prologue
    .line 350
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyContentObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->this$0:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mAutoRequery:Z
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->access$200(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyContentObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->mCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->access$300(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 353
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyContentObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyContentObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->mCursor:Landroid/database/Cursor;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->access$300(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;)Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3}, Landroid/database/Cursor;->requery()Z

    move-result v3

    # setter for: Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->mDataValid:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->access$402(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;Z)Z

    .line 355
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyContentObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->mCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->access$300(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyContentObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->this$0:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->access$500(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkMediaScannerRunning(Landroid/content/ContentResolver;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 356
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 357
    .local v0, "currentTime":J
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyContentObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->this$0:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->updatetime:J
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->access$600(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;)J

    move-result-wide v2

    sub-long v2, v0, v2

    const-wide/16 v4, 0x258

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 358
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyContentObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyContentObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->mCursor:Landroid/database/Cursor;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->access$300(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;)Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3}, Landroid/database/Cursor;->requery()Z

    move-result v3

    # setter for: Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->mDataValid:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->access$402(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;Z)Z

    .line 359
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper$MyContentObserver;->this$1:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->this$0:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;

    # setter for: Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->updatetime:J
    invoke-static {v2, v0, v1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->access$602(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;J)J

    .line 363
    .end local v0    # "currentTime":J
    :cond_1
    return-void
.end method
