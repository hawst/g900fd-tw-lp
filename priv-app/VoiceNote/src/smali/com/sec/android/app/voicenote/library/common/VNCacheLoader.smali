.class public Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;
.super Ljava/lang/Object;
.source "VNCacheLoader.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$STTLoaderTask;,
        Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;,
        Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;
    }
.end annotation


# static fields
.field private static final BOOKMARKS_CACHE_SIZE:I = 0x1e

.field private static final CACHE_ADVANCE:I = 0x3

.field private static final STT_CACHE_SIZE:I = 0x1e

.field private static final TAG:Ljava/lang/String; = "VNCacheLoader"


# instance fields
.field mAdapter:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;

.field private mBookmarksCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;>;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mSTTCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;>;"
        }
    .end annotation
.end field

.field private mScrollingState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v1, 0x1e

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mBookmarksCache:Landroid/util/LruCache;

    .line 31
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mSTTCache:Landroid/util/LruCache;

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mAdapter:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mContext:Landroid/content/Context;

    .line 36
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mScrollingState:I

    .line 47
    new-instance v0, Landroid/util/LruCache;

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mBookmarksCache:Landroid/util/LruCache;

    .line 48
    new-instance v0, Landroid/util/LruCache;

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mSTTCache:Landroid/util/LruCache;

    .line 49
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mContext:Landroid/content/Context;

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;)Landroid/util/LruCache;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mBookmarksCache:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mScrollingState:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;)Landroid/util/LruCache;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mSTTCache:Landroid/util/LruCache;

    return-object v0
.end method

.method private flatToGroupPosition(Landroid/widget/ExpandableListView;I)I
    .locals 4
    .param p1, "view"    # Landroid/widget/ExpandableListView;
    .param p2, "flat"    # I

    .prologue
    .line 220
    const/4 v0, -0x1

    .line 221
    .local v0, "groupPos":I
    const-wide/16 v2, -0x1

    .line 223
    .local v2, "packed":J
    invoke-virtual {p1, p2}, Landroid/widget/ExpandableListView;->getExpandableListPosition(I)J

    move-result-wide v2

    .line 224
    invoke-static {v2, v3}, Landroid/widget/ExpandableListView;->getPackedPositionGroup(J)I

    move-result v0

    .line 225
    return v0
.end method


# virtual methods
.method public containsBookmarks(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mBookmarksCache:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 74
    const/4 v0, 0x1

    .line 76
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsSTT(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mSTTCache:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    const/4 v0, 0x1

    .line 83
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public enableBuildCache(Landroid/widget/AbsListView;)V
    .locals 1
    .param p1, "view"    # Landroid/widget/AbsListView;

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 70
    return-void
.end method

.method public getBookmark(Ljava/lang/String;I)Lcom/sec/android/app/voicenote/common/util/Bookmark;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "position"    # I

    .prologue
    .line 125
    const/4 v1, 0x0

    .line 126
    .local v1, "bookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    const/4 v3, 0x0

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->getBookmarksForFile(Ljava/lang/String;Z)Ljava/util/ArrayList;

    move-result-object v2

    .line 127
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    const/4 v0, 0x0

    .line 128
    .local v0, "bookCount":I
    if-eqz v2, :cond_0

    .line 129
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 130
    if-ltz p2, :cond_0

    if-ge p2, v0, :cond_0

    .line 131
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "bookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    check-cast v1, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 133
    .restart local v1    # "bookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    :cond_0
    return-object v1
.end method

.method public getBookmarkCache()Landroid/util/LruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mBookmarksCache:Landroid/util/LruCache;

    return-object v0
.end method

.method public getBookmarksForFile(Ljava/lang/String;Z)Ljava/util/ArrayList;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "notify"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mBookmarksCache:Landroid/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 88
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    if-nez v0, :cond_0

    iget v1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mScrollingState:I

    if-nez v1, :cond_0

    .line 89
    new-instance v1, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;-><init>(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;Ljava/lang/String;Z)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 91
    :cond_0
    return-object v0
.end method

.method public getBookmarksForFile(Ljava/lang/String;ZLcom/sec/android/app/voicenote/common/util/M4aInfo;)Ljava/util/ArrayList;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "notify"    # Z
    .param p3, "info"    # Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Lcom/sec/android/app/voicenote/common/util/M4aInfo;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mBookmarksCache:Landroid/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 96
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    if-nez v0, :cond_0

    iget v1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mScrollingState:I

    if-nez v1, :cond_0

    .line 97
    new-instance v1, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;-><init>(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;Ljava/lang/String;ZLcom/sec/android/app/voicenote/common/util/M4aInfo;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$BookmarksLoaderTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 99
    :cond_0
    return-object v0
.end method

.method public getSTTCache()Landroid/util/LruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mSTTCache:Landroid/util/LruCache;

    return-object v0
.end method

.method public getSTTForFile(Ljava/lang/String;Z)Ljava/util/ArrayList;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "notify"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mSTTCache:Landroid/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 104
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    if-nez v0, :cond_0

    iget v1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mScrollingState:I

    if-nez v1, :cond_0

    .line 105
    new-instance v1, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$STTLoaderTask;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$STTLoaderTask;-><init>(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;Ljava/lang/String;Z)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$STTLoaderTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 107
    :cond_0
    return-object v0
.end method

.method public getSTTForFile(Ljava/lang/String;ZLcom/sec/android/app/voicenote/common/util/M4aInfo;)Ljava/util/ArrayList;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "notify"    # Z
    .param p3, "info"    # Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Lcom/sec/android/app/voicenote/common/util/M4aInfo;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mSTTCache:Landroid/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 118
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    if-nez v0, :cond_0

    iget v1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mScrollingState:I

    if-nez v1, :cond_0

    .line 119
    new-instance v1, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$STTLoaderTask;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$STTLoaderTask;-><init>(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;Ljava/lang/String;ZLcom/sec/android/app/voicenote/common/util/M4aInfo;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$STTLoaderTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 121
    :cond_0
    return-object v0
.end method

.method public getmScrollingState()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mScrollingState:I

    return v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 148
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 9
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    const/4 v8, 0x1

    .line 161
    iput p2, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mScrollingState:I

    .line 162
    const-string v5, "VNCacheLoader"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onScroll E"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const/4 v1, -0x1

    .line 164
    .local v1, "groupPos":I
    const/4 v4, -0x1

    .line 165
    .local v4, "maxGroupPos":I
    const/4 v0, 0x0

    .line 166
    .local v0, "filePath":Ljava/lang/String;
    if-eqz p1, :cond_0

    instance-of v5, p1, Landroid/widget/ExpandableListView;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mAdapter:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;

    if-nez v5, :cond_1

    .line 167
    :cond_0
    const-string v5, "VNCacheLoader"

    const-string v6, "onScroll RETURN"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :goto_0
    return-void

    .line 171
    :cond_1
    if-nez p2, :cond_3

    move-object v3, p1

    .line 172
    check-cast v3, Landroid/widget/ExpandableListView;

    .line 173
    .local v3, "listView":Landroid/widget/ExpandableListView;
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mAdapter:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;

    invoke-interface {v5}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;->getItemsCount()I

    move-result v5

    add-int/lit8 v4, v5, -0x1

    .line 204
    invoke-virtual {v3}, Landroid/widget/ExpandableListView;->getLastVisiblePosition()I

    move-result v5

    invoke-direct {p0, v3, v5}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->flatToGroupPosition(Landroid/widget/ExpandableListView;I)I

    move-result v5

    add-int/lit8 v1, v5, 0x3

    .line 205
    invoke-virtual {v3}, Landroid/widget/ExpandableListView;->getFirstVisiblePosition()I

    move-result v5

    invoke-direct {p0, v3, v5}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->flatToGroupPosition(Landroid/widget/ExpandableListView;I)I

    move-result v5

    add-int/lit8 v2, v5, -0x3

    .local v2, "i":I
    :goto_1
    if-gt v2, v1, :cond_3

    .line 206
    if-ltz v2, :cond_2

    if-gt v2, v4, :cond_2

    .line 207
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mAdapter:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;

    invoke-interface {v5, v2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;->getItemFileName(I)Ljava/lang/String;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_2

    .line 210
    invoke-virtual {p0, v0, v8}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->getBookmarksForFile(Ljava/lang/String;Z)Ljava/util/ArrayList;

    .line 211
    invoke-virtual {p0, v0, v8}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->getSTTForFile(Ljava/lang/String;Z)Ljava/util/ArrayList;

    .line 205
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 216
    .end local v2    # "i":I
    .end local v3    # "listView":Landroid/widget/ExpandableListView;
    :cond_3
    const-string v5, "VNCacheLoader"

    const-string v6, "onScroll X"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public registerOnLoadObserver(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;)V
    .locals 0
    .param p1, "observer"    # Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mAdapter:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;

    .line 230
    return-void
.end method

.method public removePathFromCache(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 137
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mBookmarksCache:Landroid/util/LruCache;

    invoke-virtual {v2, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 138
    .local v0, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mSTTCache:Landroid/util/LruCache;

    invoke-virtual {v2, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 140
    .local v1, "textDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mAdapter:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;

    if-eqz v2, :cond_0

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mAdapter:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;

    invoke-interface {v2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;->notifyOnCacheLoaded()V

    .line 143
    :cond_0
    return-void
.end method

.method public resetSttForFile(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "notify"    # Z

    .prologue
    .line 111
    new-instance v0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$STTLoaderTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$STTLoaderTask;-><init>(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;Ljava/lang/String;Z)V

    .line 112
    .local v0, "sttLoaderTask":Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$STTLoaderTask;
    iput-boolean p2, v0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$STTLoaderTask;->mDataChanged:Z

    .line 113
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$STTLoaderTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 114
    return-void
.end method

.method public setContex(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mContext:Landroid/content/Context;

    .line 238
    return-void
.end method

.method public setmScrollingState(I)V
    .locals 0
    .param p1, "mScrollingState"    # I

    .prologue
    .line 156
    iput p1, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mScrollingState:I

    .line 157
    return-void
.end method

.method public unregisterOnLoadedObserver()V
    .locals 1

    .prologue
    .line 233
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mAdapter:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;

    .line 234
    return-void
.end method

.method public updateCachedBookmarks(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 2
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mBookmarksCache:Landroid/util/LruCache;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, p2, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mAdapter:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->mAdapter:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;

    invoke-interface {v0}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;->notifyOnCacheLoaded()V

    .line 66
    :cond_0
    return-void
.end method
