.class public Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;
.super Landroid/app/Fragment;
.source "VNRecordingLightModeFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 34
    return-void
.end method

.method public static newInstance(II)Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;
    .locals 3
    .param p0, "resource"    # I
    .param p1, "recordState"    # I

    .prologue
    .line 37
    new-instance v1, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;-><init>()V

    .line 38
    .local v1, "f":Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 39
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "resource"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 40
    const-string v2, "recordstate"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 41
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;->setArguments(Landroid/os/Bundle;)V

    .line 42
    return-object v1
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    const v2, 0x7f03004e

    .line 48
    .local v2, "resource":I
    const/16 v1, 0x3e8

    .line 49
    .local v1, "recordState":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingLightModeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 50
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 51
    const-string v4, "resource"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 52
    const-string v4, "recordstate"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 55
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 60
    .local v3, "view":Landroid/view/View;
    return-object v3
.end method
