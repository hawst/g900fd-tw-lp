.class Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$1;
.super Ljava/lang/Object;
.source "AudioRecognizer.java"

# interfaces
.implements Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->startRecognition()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;)V
    .locals 11
    .param p1, "recognitionError"    # Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;

    .prologue
    .line 166
    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;->getType()I

    move-result v1

    .line 167
    .local v1, "errorType":I
    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;->getTransactionError()Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;

    move-result-object v6

    .line 168
    .local v6, "transactionError":Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;
    invoke-virtual {v6}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getType()I

    move-result v7

    .line 170
    .local v7, "transactionType":I
    invoke-virtual {v6}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getErrorCode()I

    move-result v0

    .line 171
    .local v0, "code":I
    invoke-virtual {v6}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getErrorText()Ljava/lang/String;

    move-result-object v2

    .line 172
    .local v2, "strErrorText":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getPrompt()Ljava/lang/String;

    move-result-object v4

    .line 173
    .local v4, "strPrompt":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getParameterWithError()Ljava/lang/String;

    move-result-object v3

    .line 174
    .local v3, "strParameter":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;->getSessionId()Ljava/lang/String;

    move-result-object v5

    .line 176
    .local v5, "strSession":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$000()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "\nError type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Ttype: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " code: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " text: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " prompt: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " parameter: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " session: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    # invokes: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->addEmptyMemoData()V
    invoke-static {v8}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$400(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;)V

    .line 181
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;
    invoke-static {v8}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$200(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;)Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    move-result-object v8

    invoke-virtual {v8}, Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;->release()V

    .line 182
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;
    invoke-static {v8, v9}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$202(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;)Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    .line 184
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mAudioConverter:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;
    invoke-static {v8}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$300(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;)Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->increaseAudioRecognizerResponseCount()V

    .line 185
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mAudioConverter:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;
    invoke-static {v8}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$300(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;)Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->checkFinishCondition()V

    .line 186
    return-void
.end method

.method public onResult(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;)V
    .locals 26
    .param p1, "result"    # Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;

    .prologue
    .line 93
    invoke-virtual/range {p1 .. p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;->getDictionary()Lcom/nuance/dragon/toolkit/data/Data$Dictionary;

    move-result-object v16

    .line 94
    .local v16, "processedResult":Lcom/nuance/dragon/toolkit/data/Data$Dictionary;
    const-string v21, "audio_transfer_info"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->getSequence(Ljava/lang/String;)Lcom/nuance/dragon/toolkit/data/Data$Sequence;

    move-result-object v4

    .line 95
    .local v4, "audioTransferInfo":Lcom/nuance/dragon/toolkit/data/Data$Sequence;
    const-string v21, "audio_transfer_info"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->getDictionary(Ljava/lang/String;)Lcom/nuance/dragon/toolkit/data/Data$Dictionary;

    move-result-object v6

    .line 96
    .local v6, "dic":Lcom/nuance/dragon/toolkit/data/Data$Dictionary;
    const/16 v18, 0x0

    .line 97
    .local v18, "startTime":Ljava/lang/String;
    const/4 v10, 0x0

    .line 99
    .local v10, "endTime":Ljava/lang/String;
    :try_start_0
    const-string v21, "start_time"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->getString(Ljava/lang/String;)Lcom/nuance/dragon/toolkit/data/Data$String;

    move-result-object v21

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/data/Data$String;->value:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 100
    const-string v21, "end_time"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->getString(Ljava/lang/String;)Lcom/nuance/dragon/toolkit/data/Data$String;

    move-result-object v21

    move-object/from16 v0, v21

    iget-object v10, v0, Lcom/nuance/dragon/toolkit/data/Data$String;->value:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :goto_0
    const/4 v7, 0x0

    .line 106
    .local v7, "dicResult":Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;
    invoke-virtual/range {p1 .. p1}, Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;->getDictionary()Lcom/nuance/dragon/toolkit/data/Data$Dictionary;

    move-result-object v8

    .line 108
    .local v8, "dictaionResult":Lcom/nuance/dragon/toolkit/data/Data$Dictionary;
    const/4 v5, 0x0

    .line 110
    .local v5, "buffer":Lcom/nuance/dragon/toolkit/data/Data$Bytes;
    :try_start_1
    const-string v21, "transcription"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Lcom/nuance/dragon/toolkit/data/Data$Dictionary;->getBytes(Ljava/lang/String;)Lcom/nuance/dragon/toolkit/data/Data$Bytes;

    move-result-object v5

    .line 111
    iget-object v0, v5, Lcom/nuance/dragon/toolkit/data/Data$Bytes;->value:[B

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResultManager;->createDictationResult([B)Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v7

    .line 116
    :goto_1
    if-eqz v5, :cond_4

    .line 118
    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$000()Ljava/lang/String;

    move-result-object v21

    const-string v22, "============================================"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$000()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "start_time : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$000()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "end_time   : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-virtual {v7}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;->getExtraInformation()Ljava/util/Map;

    move-result-object v15

    .line 123
    .local v15, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v7}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;->getExtraInformation()Ljava/util/Map;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v14

    .line 125
    .local v14, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 126
    .local v12, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 127
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 128
    .local v13, "key":Ljava/lang/String;
    invoke-interface {v15, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    .line 129
    .local v20, "value":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$000()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "key: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " value: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 101
    .end local v5    # "buffer":Lcom/nuance/dragon/toolkit/data/Data$Bytes;
    .end local v7    # "dicResult":Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;
    .end local v8    # "dictaionResult":Lcom/nuance/dragon/toolkit/data/Data$Dictionary;
    .end local v12    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v13    # "key":Ljava/lang/String;
    .end local v14    # "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v15    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v20    # "value":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 102
    .local v9, "e":Ljava/lang/NullPointerException;
    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$000()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "NPE : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ","

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ","

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 112
    .end local v9    # "e":Ljava/lang/NullPointerException;
    .restart local v5    # "buffer":Lcom/nuance/dragon/toolkit/data/Data$Bytes;
    .restart local v7    # "dicResult":Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;
    .restart local v8    # "dictaionResult":Lcom/nuance/dragon/toolkit/data/Data$Dictionary;
    :catch_1
    move-exception v9

    .line 113
    .local v9, "e":Ljava/lang/Exception;
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "empty result"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 132
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v12    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v14    # "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v15    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$000()Ljava/lang/String;

    move-result-object v21

    const-string v22, "----------------------------------------------"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-virtual {v7}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;->size()I

    move-result v21

    if-lez v21, :cond_1

    .line 135
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;->sentenceAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;

    move-result-object v17

    .line 136
    .local v17, "sentence":Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;
    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$000()Ljava/lang/String;

    move-result-object v21

    const-string v22, "\n-------------------------------"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$000()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "\n  Result : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-interface/range {v17 .. v17}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$000()Ljava/lang/String;

    move-result-object v21

    const-string v22, "\n-------------------------------"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    # invokes: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->addToMemoData(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$100(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;)V

    .line 143
    .end local v17    # "sentence":Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;
    :cond_1
    const/4 v11, 0x0

    .local v11, "index":I
    :goto_3
    invoke-virtual {v7}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;->size()I

    move-result v21

    move/from16 v0, v21

    if-ge v11, v0, :cond_3

    .line 144
    invoke-virtual {v7, v11}, Lcom/nuance/dragon/toolkit/recognition/dictation/DictationResult;->sentenceAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;

    move-result-object v17

    .line 145
    .restart local v17    # "sentence":Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;
    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$000()Ljava/lang/String;

    move-result-object v21

    invoke-interface/range {v17 .. v17}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const/16 v19, 0x0

    .local v19, "t":I
    :goto_4
    invoke-interface/range {v17 .. v17}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->size()I

    move-result v21

    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_2

    .line 148
    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$000()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "    "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->tokenAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Token;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "("

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->tokenAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Token;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getStartTime()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "-"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;->tokenAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Token;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lcom/nuance/dragon/toolkit/recognition/dictation/Token;->getEndTime()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ")"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    add-int/lit8 v19, v19, 0x1

    goto :goto_4

    .line 151
    :cond_2
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, ""

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 143
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_3

    .line 154
    .end local v17    # "sentence":Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;
    .end local v19    # "t":I
    :cond_3
    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$000()Ljava/lang/String;

    move-result-object v21

    const-string v22, "============================================"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    .end local v11    # "index":I
    .end local v12    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v14    # "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v15    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$200(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;)Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;->release()V

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mCloudServices:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;
    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$202(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;)Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    .line 160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mAudioConverter:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$300(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;)Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->increaseAudioRecognizerResponseCount()V

    .line 161
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->mAudioConverter:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$300(Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;)Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->checkFinishCondition()V

    .line 162
    return-void
.end method
