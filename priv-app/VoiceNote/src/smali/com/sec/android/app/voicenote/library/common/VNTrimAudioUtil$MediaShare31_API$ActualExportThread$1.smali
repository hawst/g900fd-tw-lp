.class Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread$1;
.super Ljava/lang/Object;
.source "VNTrimAudioUtil.java"

# interfaces
.implements Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;)V
    .locals 0

    .prologue
    .line 672
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread$1;->this$2:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgress(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;I)V
    .locals 5
    .param p1, "ve"    # Lcom/lifevibes/videoeditor/VideoEditor;
    .param p2, "outFileName"    # Ljava/lang/String;
    .param p3, "progress"    # I

    .prologue
    const/4 v4, 0x1

    .line 675
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread$1;->this$2:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mExportStarted:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->access$1502(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;Z)Z

    .line 676
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread$1;->this$2:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mCanceled:Z
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->access$1600(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 677
    new-instance v0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread$1$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread$1$1;-><init>(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread$1;)V

    .line 684
    .local v0, "runnable":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread$1;->this$2:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    iget-object v1, v1, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExpertCancelHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$1700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 692
    .end local v0    # "runnable":Ljava/lang/Runnable;
    :cond_0
    :goto_0
    return-void

    .line 687
    :cond_1
    const-string v1, "VNTrimAudioUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "**** export() - progress : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread$1;->this$2:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    iget-object v1, v1, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->this$1:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mListener:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->access$1300(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2, p3}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;->onProgress(II)V

    .line 689
    const/16 v1, 0x64

    if-lt p3, v1, :cond_0

    .line 690
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread$1;->this$2:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->mCompleted:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->access$1802(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;Z)Z

    goto :goto_0
.end method
