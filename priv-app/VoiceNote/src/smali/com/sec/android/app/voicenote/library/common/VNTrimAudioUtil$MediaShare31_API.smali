.class public Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;
.super Ljava/lang/Object;
.source "VNTrimAudioUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MediaShare31_API"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;
    }
.end annotation


# static fields
.field public static final ERROR_LOW_MEMORY:I = 0x88

.field public static final ERROR_NO_DATA:I = 0x8a

.field public static final ERROR_NO_LIBRARY:I = 0xc8

.field public static final ERROR_NO_SPACE:I = 0x89

.field public static final MAX_FILESIZE_SUPPORTED:J = 0x7fffffffL

.field public static final RESULT_OK:I = 0x0

.field public static final RESULT_PROPERTIES_ERROR:I = 0x40000000

.field public static final RESULT_TRIMMING_ERROR:I = -0x80000000

.field public static final STATUS_TRIMMING_DONE:I = 0x3

.field public static final STATUS_TRIMMING_NOT_STARTED:I = 0x1

.field public static final STATUS_TRIMMING_RUNNING:I = 0x2

.field public static final STATUS_TRIMMING_UNKNOWN:I = 0x0

.field public static final UNEXPECTED_ARGUMENT:I = -0x7ffffffc

.field public static final UNSUPPORTED_AUDIOCODEC:I = 0x4000000c

.field public static final UNSUPPORTED_BITRATE:I = 0x4000000e

.field public static final UNSUPPORTED_FILESIZE:I = 0x40000006

.field public static final UNSUPPORTED_INPUT:I = -0x7ffffffe

.field public static final UNSUPPORTED_PROFILE_LEVEL:I = 0x40000008

.field public static final UNSUPPORTED_RESOLUTION:I = 0x4000000a


# instance fields
.field private mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

.field private final mExpertCancelHandler:Landroid/os/Handler;

.field private mExportThread:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

.field private mListener:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;

.field private mOutput:Ljava/lang/String;

.field public mTrimmingStatus:I

.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)V
    .locals 1

    .prologue
    .line 420
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 426
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExpertCancelHandler:Landroid/os/Handler;

    .line 644
    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    .prologue
    .line 420
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mListener:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    .prologue
    .line 420
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mOutput:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    .prologue
    .line 420
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExpertCancelHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)Lcom/lifevibes/videoeditor/VideoEditor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;

    .prologue
    .line 420
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;Lcom/lifevibes/videoeditor/VideoEditor;)Lcom/lifevibes/videoeditor/VideoEditor;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;
    .param p1, "x1"    # Lcom/lifevibes/videoeditor/VideoEditor;

    .prologue
    .line 420
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    return-object p1
.end method

.method static synthetic access$2002(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;
    .param p1, "x1"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    .prologue
    .line 420
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExportThread:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    return-object p1
.end method


# virtual methods
.method public getProperties(Ljava/lang/String;Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;)I
    .locals 13
    .param p1, "input"    # Ljava/lang/String;
    .param p2, "properties"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const v4, 0x4000000e    # 2.0000033f

    const v10, 0x7f0b0085

    const/4 v5, 0x0

    .line 587
    iput v11, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mTrimmingStatus:I

    .line 589
    if-nez p1, :cond_0

    .line 590
    const v4, -0x7ffffffe

    .line 641
    :goto_0
    return v4

    .line 592
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 593
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/32 v8, 0x7fffffff

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    .line 594
    const v4, 0x40000006    # 2.0000014f

    goto :goto_0

    .line 596
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    if-nez v6, :cond_3

    .line 597
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->loadLibrary()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    if-nez v6, :cond_3

    .line 598
    :cond_2
    const v4, -0x7ffffffc

    goto :goto_0

    .line 601
    :cond_3
    const/4 v2, 0x0

    .line 603
    .local v2, "mvi":Lcom/lifevibes/videoeditor/MediaAudioItem;
    :try_start_0
    new-instance v3, Lcom/lifevibes/videoeditor/MediaAudioItem;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    const-string v7, "m1"

    invoke-direct {v3, v6, v7, p1}, Lcom/lifevibes/videoeditor/MediaAudioItem;-><init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 614
    .end local v2    # "mvi":Lcom/lifevibes/videoeditor/MediaAudioItem;
    .local v3, "mvi":Lcom/lifevibes/videoeditor/MediaAudioItem;
    if-eqz v3, :cond_7

    .line 615
    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getFileType()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mFileType:I

    .line 616
    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getDuration()J

    move-result-wide v6

    long-to-int v6, v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mDurationMillis:I

    .line 617
    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioType()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioType:I

    .line 618
    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioBitrate()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    .line 619
    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioChannels()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioChannels:I

    .line 620
    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioSamplingFrequency()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioSamplingFrequency:I

    .line 622
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioType:I

    if-ne v6, v11, :cond_4

    .line 623
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    const/16 v7, 0x2fa8

    if-eq v6, v7, :cond_6

    .line 624
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 627
    :cond_4
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioType:I

    if-ne v6, v12, :cond_5

    .line 628
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    const v7, 0x1f400

    if-eq v6, v7, :cond_6

    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    const v7, 0x17700

    if-eq v6, v7, :cond_6

    .line 629
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 633
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 634
    const v4, 0x4000000c    # 2.0000029f

    goto/16 :goto_0

    :cond_6
    move v4, v5

    .line 636
    goto/16 :goto_0

    .line 638
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    move-object v2, v3

    .line 641
    .end local v3    # "mvi":Lcom/lifevibes/videoeditor/MediaAudioItem;
    .restart local v2    # "mvi":Lcom/lifevibes/videoeditor/MediaAudioItem;
    :goto_1
    const/high16 v4, 0x40000000    # 2.0f

    goto/16 :goto_0

    .line 609
    :catch_0
    move-exception v0

    .line 610
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    :try_start_1
    const-string v6, "VNTrimAudioUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "new MediaVideoItem\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614
    if-eqz v2, :cond_b

    .line 615
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getFileType()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mFileType:I

    .line 616
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getDuration()J

    move-result-wide v6

    long-to-int v6, v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mDurationMillis:I

    .line 617
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioType()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioType:I

    .line 618
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioBitrate()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    .line 619
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioChannels()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioChannels:I

    .line 620
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioSamplingFrequency()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioSamplingFrequency:I

    .line 622
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioType:I

    if-ne v6, v11, :cond_8

    .line 623
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    const/16 v7, 0x2fa8

    if-eq v6, v7, :cond_a

    .line 624
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 627
    :cond_8
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioType:I

    if-ne v6, v12, :cond_9

    .line 628
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    const v7, 0x1f400

    if-eq v6, v7, :cond_a

    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    const v7, 0x17700

    if-eq v6, v7, :cond_a

    .line 629
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 633
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 634
    const v4, 0x4000000c    # 2.0000029f

    goto/16 :goto_0

    :cond_a
    move v4, v5

    .line 636
    goto/16 :goto_0

    .line 638
    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 611
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 612
    .local v0, "ex":Ljava/io/IOException;
    :try_start_2
    const-string v6, "VNTrimAudioUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "new MediaVideoItem\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 614
    if-eqz v2, :cond_f

    .line 615
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getFileType()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mFileType:I

    .line 616
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getDuration()J

    move-result-wide v6

    long-to-int v6, v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mDurationMillis:I

    .line 617
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioType()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioType:I

    .line 618
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioBitrate()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    .line 619
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioChannels()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioChannels:I

    .line 620
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioSamplingFrequency()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioSamplingFrequency:I

    .line 622
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioType:I

    if-ne v6, v11, :cond_c

    .line 623
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    const/16 v7, 0x2fa8

    if-eq v6, v7, :cond_e

    .line 624
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 627
    :cond_c
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioType:I

    if-ne v6, v12, :cond_d

    .line 628
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    const v7, 0x1f400

    if-eq v6, v7, :cond_e

    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    const v7, 0x17700

    if-eq v6, v7, :cond_e

    .line 629
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 633
    :cond_d
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 634
    const v4, 0x4000000c    # 2.0000029f

    goto/16 :goto_0

    :cond_e
    move v4, v5

    .line 636
    goto/16 :goto_0

    .line 638
    :cond_f
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 614
    .end local v0    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    if-eqz v2, :cond_13

    .line 615
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getFileType()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mFileType:I

    .line 616
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getDuration()J

    move-result-wide v6

    long-to-int v6, v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mDurationMillis:I

    .line 617
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioType()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioType:I

    .line 618
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioBitrate()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    .line 619
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioChannels()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioChannels:I

    .line 620
    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getAudioSamplingFrequency()I

    move-result v6

    iput v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioSamplingFrequency:I

    .line 622
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioType:I

    if-ne v6, v11, :cond_10

    .line 623
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    const/16 v7, 0x2fa8

    if-eq v6, v7, :cond_12

    .line 624
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 627
    :cond_10
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioType:I

    if-ne v6, v12, :cond_11

    .line 628
    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    const v7, 0x1f400

    if-eq v6, v7, :cond_12

    iget v6, p2, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareProperties;->mAudioBitrate:I

    const v7, 0x17700

    if-eq v6, v7, :cond_12

    .line 629
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 633
    :cond_11
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 634
    const v4, 0x4000000c    # 2.0000029f

    goto/16 :goto_0

    :cond_12
    move v4, v5

    .line 636
    goto/16 :goto_0

    .line 638
    :cond_13
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$700(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v10, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    throw v6
.end method

.method public loadLibrary()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 555
    sget-object v1, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->OUTPUT_DIR:Ljava/lang/String;

    .line 558
    .local v1, "projectPath":Ljava/lang/String;
    :try_start_0
    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mErrorStr:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$1100()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 559
    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLibrary:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$1200()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 560
    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mLibrary:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$1200()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/System;->load(Ljava/lang/String;)V

    .line 566
    :cond_0
    invoke-static {v1}, Lcom/lifevibes/videoeditor/VideoEditorFactory;->create(Ljava/lang/String;)Lcom/lifevibes/videoeditor/VideoEditor;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    .line 567
    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mErrorStr:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$1102(Ljava/lang/String;)Ljava/lang/String;

    .line 568
    const/4 v2, 0x1

    .line 583
    :goto_0
    return v2

    .line 562
    :cond_1
    const-string v3, "VNTrimAudioUtil"

    const-string v4, "There is no trim library"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 569
    :catch_0
    move-exception v0

    .line 570
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v3, "VNTrimAudioUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "loading AudioEditorImpl JNI\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mErrorStr:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$1102(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 572
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 573
    .local v0, "ex":Ljava/io/FileNotFoundException;
    const-string v3, "VNTrimAudioUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "loading AudioEditorImpl JNI\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mErrorStr:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$1102(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 575
    .end local v0    # "ex":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v0

    .line 576
    .local v0, "ex":Ljava/lang/ExceptionInInitializerError;
    const-string v3, "VNTrimAudioUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "loading AudioEditorImpl JNI\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/ExceptionInInitializerError;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    invoke-virtual {v0}, Ljava/lang/ExceptionInInitializerError;->toString()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mErrorStr:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$1102(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 578
    .end local v0    # "ex":Ljava/lang/ExceptionInInitializerError;
    :catch_3
    move-exception v0

    .line 579
    .local v0, "ex":Ljava/io/IOException;
    const-string v3, "VNTrimAudioUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "loading AudioEditorImpl JNI\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mErrorStr:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$1102(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public releaseTrimming()Z
    .locals 3

    .prologue
    .line 539
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExportThread:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    if-eqz v0, :cond_0

    .line 540
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExportThread:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->cancel()V

    .line 543
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->this$0:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->mSyncKey:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;->access$1000(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 544
    :try_start_0
    iget v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mTrimmingStatus:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    if-eqz v0, :cond_1

    .line 545
    const-string v0, "VNTrimAudioUtil"

    const-string v2, "**** Trim AudioEditorImpl release() call."

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    invoke-interface {v0}, Lcom/lifevibes/videoeditor/VideoEditor;->release()V

    .line 547
    const-string v0, "VNTrimAudioUtil"

    const-string v2, "**** Trim AudioEditorImpl release() done."

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    .line 550
    :cond_1
    monitor-exit v1

    .line 551
    const/4 v0, 0x1

    return v0

    .line 550
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public startTrimming(Ljava/lang/String;Ljava/lang/String;IILcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;)I
    .locals 10
    .param p1, "input"    # Ljava/lang/String;
    .param p2, "output"    # Ljava/lang/String;
    .param p3, "beginMillis"    # I
    .param p4, "endMillis"    # I
    .param p5, "listener"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;

    .prologue
    .line 463
    const-string v4, "VNTrimAudioUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "**** Trimming beginCutTime : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " endCutTime : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    if-nez v4, :cond_1

    .line 466
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->loadLibrary()Z

    move-result v4

    if-nez v4, :cond_0

    .line 467
    const v4, -0x7ffffffc

    .line 535
    :goto_0
    return v4

    .line 469
    :cond_0
    const v4, -0x7ffffffc

    goto :goto_0

    .line 472
    :cond_1
    const/4 v0, 0x0

    .line 473
    .local v0, "MediaAudioItem":Lcom/lifevibes/videoeditor/MediaAudioItem;
    const/4 v2, 0x1

    .line 475
    .local v2, "err":Z
    :try_start_0
    new-instance v1, Lcom/lifevibes/videoeditor/MediaAudioItem;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    const-string v5, "ms3"

    invoke-direct {v1, v4, v5, p1}, Lcom/lifevibes/videoeditor/MediaAudioItem;-><init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 481
    .end local v0    # "MediaAudioItem":Lcom/lifevibes/videoeditor/MediaAudioItem;
    .local v1, "MediaAudioItem":Lcom/lifevibes/videoeditor/MediaAudioItem;
    const/4 v2, 0x0

    move-object v0, v1

    .line 487
    .end local v1    # "MediaAudioItem":Lcom/lifevibes/videoeditor/MediaAudioItem;
    .restart local v0    # "MediaAudioItem":Lcom/lifevibes/videoeditor/MediaAudioItem;
    :goto_1
    if-eqz v2, :cond_2

    .line 488
    const v4, -0x7ffffffc

    goto :goto_0

    .line 482
    :catch_0
    move-exception v3

    .line 483
    .local v3, "ex":Ljava/lang/IllegalArgumentException;
    const-string v4, "VNTrimAudioUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "new MediaAudioItem\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 484
    .end local v3    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v3

    .line 485
    .local v3, "ex":Ljava/io/IOException;
    const-string v4, "VNTrimAudioUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "new MediaAudioItem\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 491
    .end local v3    # "ex":Ljava/io/IOException;
    :cond_2
    const/4 v2, 0x1

    .line 492
    :goto_2
    if-eqz v2, :cond_5

    int-to-long v4, p4

    int-to-long v6, p3

    const-wide/16 v8, 0x44b

    add-long/2addr v6, v8

    const-wide/16 v8, 0xa

    sub-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-ltz v4, :cond_5

    .line 494
    int-to-long v4, p4

    :try_start_1
    invoke-virtual {v0}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getDuration()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 495
    invoke-virtual {v0}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getDuration()J

    move-result-wide v4

    long-to-int p4, v4

    .line 497
    :cond_3
    int-to-long v4, p3

    int-to-long v6, p4

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/lifevibes/videoeditor/MediaAudioItem;->setExtractBoundaries(JJ)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    .line 498
    const/4 v2, 0x0

    goto :goto_2

    .line 499
    :catch_2
    move-exception v3

    .line 500
    .local v3, "ex":Ljava/lang/IllegalArgumentException;
    add-int/lit8 p4, p4, -0x1

    .line 501
    int-to-long v4, p4

    int-to-long v6, p3

    const-wide/16 v8, 0x44b

    add-long/2addr v6, v8

    const-wide/16 v8, 0xa

    sub-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-gez v4, :cond_4

    .line 502
    const-string v4, "VNTrimAudioUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MediaAudioItem setExtractBoundaries()\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 504
    :cond_4
    const-string v4, "VNTrimAudioUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MediaAudioItem setExtractBoundaries() : retry\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    const/4 v2, 0x0

    goto :goto_2

    .line 511
    .end local v3    # "ex":Ljava/lang/IllegalArgumentException;
    :cond_5
    if-eqz v2, :cond_6

    .line 512
    const v4, -0x7ffffffc

    goto/16 :goto_0

    .line 515
    :cond_6
    const/4 v2, 0x1

    .line 517
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    if-eqz v4, :cond_7

    .line 518
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mAudioEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    invoke-interface {v4, v0}, Lcom/lifevibes/videoeditor/VideoEditor;->addMediaAudioItem(Lcom/lifevibes/videoeditor/MediaAudioItem;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_4

    .line 519
    const/4 v2, 0x0

    .line 526
    :cond_7
    :goto_3
    if-eqz v2, :cond_8

    .line 527
    const v4, -0x7ffffffc

    goto/16 :goto_0

    .line 521
    :catch_3
    move-exception v3

    .line 522
    .restart local v3    # "ex":Ljava/lang/IllegalArgumentException;
    const-string v4, "VNTrimAudioUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AudioEditorImpl addMediaItem()\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 523
    .end local v3    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v3

    .line 524
    .local v3, "ex":Ljava/lang/IllegalStateException;
    const-string v4, "VNTrimAudioUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AudioEditorImpl addMediaItem()\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 530
    .end local v3    # "ex":Ljava/lang/IllegalStateException;
    :cond_8
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mOutput:Ljava/lang/String;

    .line 531
    iput-object p5, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mListener:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShareListener;

    .line 532
    new-instance v4, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;-><init>(Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;)V

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExportThread:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    .line 533
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API;->mExportThread:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil$MediaShare31_API$ActualExportThread;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 535
    const/4 v4, 0x0

    goto/16 :goto_0
.end method
