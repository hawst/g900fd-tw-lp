.class public Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;
.super Landroid/app/Activity;
.source "VNEditLabelActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final MAX_LENGTH:I = 0x2e

.field public static final RENAME_MAX_LENGTH:I = 0x32

.field private static final TAG:Ljava/lang/String; = "VNEditLabelActivity"


# instance fields
.field private final COLORS:[I

.field private colorChangedListener:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;

.field private gradientView:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;

.field private mColor:[Landroid/widget/ImageView;

.field private mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

.field private mLabelColor:Landroid/widget/ImageView;

.field private mLabelName:Landroid/widget/EditText;

.field private mOK_BTN_Enabled:Z

.field private mOK_Btn:Landroid/view/MenuItem;

.field private mRetColor:I

.field private mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

.field private originColor:I


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 48
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 53
    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->gradientView:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;

    .line 54
    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;

    .line 55
    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelColor:Landroid/widget/ImageView;

    .line 56
    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mOK_Btn:Landroid/view/MenuItem;

    .line 57
    iput-boolean v6, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mOK_BTN_Enabled:Z

    .line 58
    iput v6, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    .line 59
    iput v6, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->originColor:I

    .line 60
    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mColor:[Landroid/widget/ImageView;

    .line 61
    const/16 v0, 0x8

    new-array v0, v0, [I

    const/16 v1, 0x7c

    const/16 v2, 0x51

    const/16 v3, 0xa1

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v6

    const/4 v1, 0x1

    const/16 v2, 0x5d

    const/16 v3, 0x66

    const/16 v4, 0xc0

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x3f

    const/16 v3, 0xa9

    const/16 v4, 0xf6

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x94

    const/16 v3, 0x79

    invoke-static {v6, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x8f

    const/16 v3, 0xb9

    const/16 v4, 0x3b

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0xff

    const/16 v3, 0x97

    invoke-static {v2, v3, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0xc3

    const/16 v3, 0x38

    const/16 v4, 0x15

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0xef

    const/16 v3, 0x4d

    const/16 v4, 0x9c

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    .line 64
    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    .line 65
    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 213
    new-instance v0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity$2;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->colorChangedListener:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setEnableOKButton(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelColor:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;
    .param p1, "x1"    # I

    .prologue
    .line 48
    iput p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;
    .param p1, "x1"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setSelectedBtn(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;Landroid/widget/EditText;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;
    .param p1, "x1"    # Landroid/widget/EditText;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setEnableOKButton(Landroid/widget/EditText;)V

    return-void
.end method

.method public static getNameFilter(Landroid/app/Activity;ZI)[Landroid/text/InputFilter;
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "isIncludeCount"    # Z
    .param p2, "fixedMaxLength"    # I

    .prologue
    .line 318
    const/4 v2, 0x1

    new-array v0, v2, [Landroid/text/InputFilter;

    .line 320
    .local v0, "filterArray":[Landroid/text/InputFilter;
    const/16 v1, 0x2e

    .line 321
    .local v1, "max_length":I
    if-eqz p1, :cond_0

    .line 322
    const/16 v2, 0x32

    if-ge v2, p2, :cond_1

    .line 323
    move v1, p2

    .line 328
    :cond_0
    :goto_0
    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity$3;

    invoke-direct {v3, v1, p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity$3;-><init>(ILandroid/app/Activity;)V

    aput-object v3, v0, v2

    .line 341
    return-object v0

    .line 325
    :cond_1
    const/16 v1, 0x32

    goto :goto_0
.end method

.method private setEnableOKButton(Landroid/widget/EditText;)V
    .locals 1
    .param p1, "label"    # Landroid/widget/EditText;

    .prologue
    .line 345
    if-eqz p1, :cond_0

    .line 346
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setEnableOKButton(Ljava/lang/String;)V

    .line 347
    :cond_0
    return-void
.end method

.method private setEnableOKButton(Ljava/lang/String;)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 350
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mOK_BTN_Enabled:Z

    .line 351
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 353
    .local v0, "trimmedStr":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 354
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mOK_BTN_Enabled:Z

    .line 357
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 358
    iget v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->originColor:I

    iget v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    if-ne v1, v2, :cond_1

    .line 359
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mOK_BTN_Enabled:Z

    .line 362
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->invalidateOptionsMenu()V

    .line 363
    return-void
.end method

.method private setSelectedBtn(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 308
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 309
    if-ne v0, p1, :cond_0

    .line 310
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mColor:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 308
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 312
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mColor:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_1

    .line 315
    :cond_1
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 269
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 304
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setEnableOKButton(Landroid/widget/EditText;)V

    .line 305
    return-void

    .line 271
    :pswitch_0
    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setSelectedBtn(I)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelColor:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    aget v1, v1, v2

    iput v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 275
    :pswitch_1
    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setSelectedBtn(I)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelColor:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    aget v1, v1, v3

    iput v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 279
    :pswitch_2
    invoke-direct {p0, v4}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setSelectedBtn(I)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelColor:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    aget v1, v1, v4

    iput v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 283
    :pswitch_3
    invoke-direct {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setSelectedBtn(I)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelColor:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    aget v1, v1, v5

    iput v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 287
    :pswitch_4
    invoke-direct {p0, v6}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setSelectedBtn(I)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelColor:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    aget v1, v1, v6

    iput v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 291
    :pswitch_5
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setSelectedBtn(I)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelColor:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    const/4 v2, 0x5

    aget v1, v1, v2

    iput v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 295
    :pswitch_6
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setSelectedBtn(I)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelColor:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    const/4 v2, 0x6

    aget v1, v1, v2

    iput v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 299
    :pswitch_7
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setSelectedBtn(I)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelColor:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    const/4 v2, 0x7

    aget v1, v1, v2

    iput v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto/16 :goto_0

    .line 269
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0026
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->invalidateOptionsMenu()V

    .line 264
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 265
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 69
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    const v5, 0x7f030004

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setContentView(I)V

    .line 72
    new-instance v5, Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-direct {v5, p0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 74
    const/4 v4, 0x0

    .line 75
    .local v4, "labelName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 76
    .local v0, "color":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 77
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 78
    const-string v5, "title"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 79
    const-string v5, "color"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 80
    iput v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->originColor:I

    .line 83
    :cond_0
    new-instance v5, Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {v5, p0}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;-><init>(Landroid/app/Activity;)V

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    .line 85
    const v5, 0x7f0e000b

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;

    .line 86
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 87
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;

    invoke-virtual {v5, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;

    invoke-virtual {v5, v4}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 89
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0076

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show(Ljava/lang/String;)V

    .line 94
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;

    if-nez v4, :cond_3

    move v5, v6

    :goto_1
    invoke-static {p0, v9, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->getNameFilter(Landroid/app/Activity;ZI)[Landroid/text/InputFilter;

    move-result-object v5

    invoke-virtual {v7, v5}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 95
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;

    invoke-virtual {v5, v9}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 96
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;

    new-instance v7, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity$1;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;)V

    invoke-virtual {v5, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 109
    const v5, 0x7f0e000a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelColor:Landroid/widget/ImageView;

    .line 111
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    array-length v5, v5

    new-array v5, v5, [Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mColor:[Landroid/widget/ImageView;

    .line 112
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mColor:[Landroid/widget/ImageView;

    const v5, 0x7f0e0026

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v7, v6

    .line 113
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mColor:[Landroid/widget/ImageView;

    const v5, 0x7f0e0027

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v7, v9

    .line 114
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mColor:[Landroid/widget/ImageView;

    const/4 v8, 0x2

    const v5, 0x7f0e0028

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v7, v8

    .line 115
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mColor:[Landroid/widget/ImageView;

    const/4 v8, 0x3

    const v5, 0x7f0e0029

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v7, v8

    .line 116
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mColor:[Landroid/widget/ImageView;

    const/4 v8, 0x4

    const v5, 0x7f0e002a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v7, v8

    .line 117
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mColor:[Landroid/widget/ImageView;

    const/4 v8, 0x5

    const v5, 0x7f0e002b

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v7, v8

    .line 118
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mColor:[Landroid/widget/ImageView;

    const/4 v8, 0x6

    const v5, 0x7f0e002c

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v7, v8

    .line 119
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mColor:[Landroid/widget/ImageView;

    const/4 v8, 0x7

    const v5, 0x7f0e002d

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v7, v8

    .line 121
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    array-length v5, v5

    if-ge v2, v5, :cond_4

    .line 122
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mColor:[Landroid/widget/ImageView;

    aget-object v5, v5, v2

    invoke-virtual {v5, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->originColor:I

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    aget v7, v7, v2

    if-ne v5, v7, :cond_1

    .line 124
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mColor:[Landroid/widget/ImageView;

    aget-object v5, v5, v2

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 121
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 91
    .end local v2    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0008

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 94
    :cond_3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    goto/16 :goto_1

    .line 128
    .restart local v2    # "i":I
    :cond_4
    const v5, 0x7f0e000c

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->gradientView:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;

    .line 129
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->gradientView:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;

    const/high16 v7, -0x1000000

    invoke-virtual {v5, v7}, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->setBackgroundColor(I)V

    .line 130
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->gradientView:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->colorChangedListener:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->setColorchangedListener(Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;)V

    .line 134
    if-eqz p1, :cond_7

    .line 135
    const-string v5, "labelcolor"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 136
    const/4 v3, 0x0

    .line 137
    .local v3, "index":I
    const/4 v2, 0x0

    :goto_3
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    array-length v5, v5

    if-ge v2, v5, :cond_6

    .line 138
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    aget v5, v5, v2

    if-ne v5, v0, :cond_5

    .line 139
    move v3, v2

    .line 137
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 142
    :cond_6
    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setSelectedBtn(I)V

    .line 145
    .end local v3    # "index":I
    :cond_7
    if-nez v0, :cond_8

    .line 146
    invoke-direct {p0, v6}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setSelectedBtn(I)V

    .line 147
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelColor:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I

    aget v6, v7, v6

    iput v6, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 153
    :goto_4
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;

    invoke-direct {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setEnableOKButton(Landroid/widget/EditText;)V

    .line 154
    return-void

    .line 149
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelColor:Landroid/widget/ImageView;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 150
    iput v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    goto :goto_4
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 165
    .local v0, "inflater":Landroid/view/MenuInflater;
    const/high16 v1, 0x7f0d0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 166
    const v1, 0x7f0e00f3

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mOK_Btn:Landroid/view/MenuItem;

    .line 167
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->gradientView:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->clear()V

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    if-eqz v0, :cond_0

    .line 236
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 238
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 239
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 9
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 179
    const-string v7, "input_method"

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 180
    .local v1, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    .line 209
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v5

    :goto_0
    return v5

    .line 182
    :sswitch_0
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 183
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 185
    .local v4, "trimmedStr":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v7, v4}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->isExitSameTitle(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 186
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0b0028

    invoke-static {v7, v8, v6}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showManagedToast(Landroid/content/Context;II)V

    goto :goto_0

    .line 191
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 192
    .local v0, "extra":Landroid/os/Bundle;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 194
    .local v2, "intent":Landroid/content/Intent;
    const-string v6, "color"

    iget v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 195
    const-string v6, "title"

    invoke-virtual {v0, v6, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 197
    const/4 v6, -0x1

    invoke-virtual {p0, v6, v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setResult(ILandroid/content/Intent;)V

    .line 198
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->finish()V

    goto :goto_0

    .line 204
    .end local v0    # "extra":Landroid/os/Bundle;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "str":Ljava/lang/String;
    .end local v4    # "trimmedStr":Ljava/lang/String;
    :sswitch_1
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->finish()V

    move v5, v6

    .line 206
    goto :goto_0

    .line 180
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_1
        0x7f0e00f2 -> :sswitch_1
        0x7f0e00f3 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mOK_Btn:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mOK_Btn:Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mOK_BTN_Enabled:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 174
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 225
    const-string v0, "VNEditLabelActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->finish()V

    .line 229
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 230
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 158
    const-string v0, "labelcolor"

    iget v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 159
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 160
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 243
    const-string v0, "VNEditLabelActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTrimMemory() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    sparse-switch p1, :sswitch_data_0

    .line 257
    :sswitch_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 258
    return-void

    .line 244
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method
