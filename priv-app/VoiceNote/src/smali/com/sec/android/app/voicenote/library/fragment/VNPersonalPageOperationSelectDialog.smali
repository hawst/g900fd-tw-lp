.class public Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;
.super Landroid/app/DialogFragment;
.source "VNPersonalPageOperationSelectDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$ICallback;
    }
.end annotation


# static fields
.field static final filenameKey:Ljava/lang/String; = "filename"

.field static iCallback:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$ICallback;


# instance fields
.field private callback:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$ICallback;

.field dialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->dialog:Landroid/app/AlertDialog;

    return-void
.end method

.method public static newInstance(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$ICallback;)Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;
    .locals 2
    .param p0, "Callback"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$ICallback;

    .prologue
    .line 36
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->iCallback:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$ICallback;

    .line 37
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;-><init>()V

    .line 38
    .local v1, "fragment":Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 39
    .local v0, "argument":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->setArguments(Landroid/os/Bundle;)V

    .line 40
    return-object v1
.end method


# virtual methods
.method public isshowing()Z
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->dialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 22
    const/4 v0, 0x0

    .line 24
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 88
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 79
    .local v0, "fragment":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->getTargetRequestCode()I

    move-result v1

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 82
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->dialog:Landroid/app/AlertDialog;

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->dialog:Landroid/app/AlertDialog;

    const v1, 0x7f0b00c2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->dialog:Landroid/app/AlertDialog;

    const v1, 0x7f0b002f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "filename"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->dialog:Landroid/app/AlertDialog;

    const/4 v1, -0x2

    const v2, 0x7f0b0021

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->dialog:Landroid/app/AlertDialog;

    const/4 v1, -0x3

    const v2, 0x7f0b0107

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->dialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    const v2, 0x7f0b0105

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$3;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->dialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public setCallback(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$ICallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$ICallback;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->callback:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$ICallback;

    .line 18
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 44
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 45
    .local v0, "argument":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "filename"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-void
.end method
