.class public Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$SendVTDialogAdapter;
.super Landroid/widget/BaseAdapter;
.source "VNMultiShareActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SendVTDialogAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$SendVTDialogAdapter;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$SendVTDialogAdapter;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mItem:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->access$000(Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$SendVTDialogAdapter;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mItem:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->access$000(Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 147
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    .line 154
    if-nez p2, :cond_1

    .line 155
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$SendVTDialogAdapter;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mInflater:Landroid/view/LayoutInflater;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->access$100(Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f03005a

    invoke-virtual {v7, v8, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 157
    new-instance v0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$ViewHolder;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$ViewHolder;-><init>()V

    .line 158
    .local v0, "holder":Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$ViewHolder;
    const v7, 0x7f0e00e9

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$ViewHolder;->icon:Landroid/widget/ImageView;

    .line 159
    const v7, 0x7f0e00ea

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$ViewHolder;->name:Landroid/widget/TextView;

    .line 161
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 166
    :goto_0
    const/4 v6, 0x0

    .line 167
    .local v6, "title":Ljava/lang/CharSequence;
    const/4 v1, 0x0

    .line 169
    .local v1, "icon":Landroid/graphics/drawable/Drawable;
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$SendVTDialogAdapter;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->mItem:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->access$000(Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    .line 170
    .local v2, "intent":Landroid/content/Intent;
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$SendVTDialogAdapter;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 171
    .local v4, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v4, v2, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 173
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_0

    .line 174
    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 175
    .local v5, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v7, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v7, v4}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 176
    iget-object v7, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v7, v4}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 179
    .end local v5    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_0
    invoke-virtual {v2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    .line 180
    iget-object v7, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v7, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$ViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 183
    return-object p2

    .line 163
    .end local v0    # "holder":Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$ViewHolder;
    .end local v1    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    .end local v6    # "title":Ljava/lang/CharSequence;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$ViewHolder;

    .restart local v0    # "holder":Lcom/sec/android/app/voicenote/library/subactivity/VNMultiShareActivity$ViewHolder;
    goto :goto_0
.end method
