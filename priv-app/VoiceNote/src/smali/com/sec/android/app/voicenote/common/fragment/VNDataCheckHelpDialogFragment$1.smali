.class Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$1;
.super Ljava/lang/Object;
.source "VNDataCheckHelpDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;

.field final synthetic val$needDataCheck:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;

    iput-object p2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$1;->val$needDataCheck:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v2, 0x1

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->mCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$1;->val$needDataCheck:Ljava/lang/String;

    const-string v1, "need_wifi_check"

    if-ne v0, v1, :cond_1

    .line 80
    const-string v0, "connect_via_wifi_help"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 85
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment;->dismiss()V

    .line 86
    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNDataCheckHelpDialogFragment$1;->val$needDataCheck:Ljava/lang/String;

    const-string v1, "need_mobile_check"

    if-ne v0, v1, :cond_0

    .line 82
    const-string v0, "connect_via_mobile_help"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    goto :goto_0
.end method
