.class Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$12;
.super Ljava/lang/Object;
.source "VoiceNoteService.java"

# interfaces
.implements Lcom/samsung/android/motion/MRListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0

    .prologue
    .line 2014
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$12;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMotionListener(Lcom/samsung/android/motion/MREvent;)V
    .locals 3
    .param p1, "motionEvent"    # Lcom/samsung/android/motion/MREvent;

    .prologue
    .line 2017
    invoke-virtual {p1}, Lcom/samsung/android/motion/MREvent;->getMotion()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2034
    :cond_0
    :goto_0
    return-void

    .line 2019
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$12;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2020
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$12;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2000(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNIntent;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$12;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2000(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNIntent;

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->isHeadsetConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2021
    :cond_1
    const-string v0, "VoiceNoteService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "headset is connected : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$12;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2000(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2023
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$12;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 2024
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$12;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 2025
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$12;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->isNotificationShow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2026
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$12;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_0

    .line 2017
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method
