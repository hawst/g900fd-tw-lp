.class public final Lcom/sec/android/app/voicenote/common/util/VNConsts$STT$ERR_CODE;
.super Ljava/lang/Object;
.source "VNConsts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/util/VNConsts$STT;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ERR_CODE"
.end annotation


# static fields
.field public static final BCP_INVALID_REQUEST:I = 0x258

.field public static final CANCELLED:I = 0xb

.field public static final FAILURE_REASON_UNKNOWN:I = 0x0

.field public static final GENERIC_ERROR:I = 0x6

.field public static final GRAM_COMP_FAILURE:I = 0x5

.field public static final GRAM_LOAD_FAILURE:I = 0x4

.field public static final LANUAGE_UNSUPPORTED:I = 0xa

.field public static final NO_GRAMMAR_DEFIND:I = 0xd

.field public static final NO_INPUT_TIMEOUT:I = 0x2

.field public static final NO_MATCH:I = 0x1

.field public static final NSS_FAILED_TO_LOAD:I = 0x259

.field public static final NSS_RESOURCE_UNAVAILABLE:I = 0x1e0

.field public static final RECOGNITION_FAILURE_REASON_IS_MISSING:I = 0x59

.field public static final RECOGNITION_RESULTS_ARE_MISSING:I = 0x58

.field public static final RECOGNITION_TIMEOUT:I = 0x3

.field public static final SEMATICS_FAILURE:I = 0xc

.field public static final SPEECH_TOO_EARLY:I = 0x7

.field public static final TOO_MUCH_SPEECH_TIMEOUT:I = 0x8

.field public static final URI_FAILURE:I = 0x9


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
