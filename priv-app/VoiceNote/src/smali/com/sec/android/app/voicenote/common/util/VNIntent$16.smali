.class Lcom/sec/android/app/voicenote/common/util/VNIntent$16;
.super Landroid/content/BroadcastReceiver;
.source "VNIntent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverCoverOnOff(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V
    .locals 0

    .prologue
    .line 680
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$16;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v3, 0x1324

    .line 683
    const-string v1, "VNIntent"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    const-string v1, "coverOpen"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 685
    .local v0, "isCoverOpen":Z
    if-nez v0, :cond_0

    .line 686
    invoke-static {p1}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sendUpdateAppWidgetOnBtnClick(Landroid/content/Context;)V

    .line 688
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$16;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 689
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$16;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {v1, v3, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 690
    return-void
.end method
