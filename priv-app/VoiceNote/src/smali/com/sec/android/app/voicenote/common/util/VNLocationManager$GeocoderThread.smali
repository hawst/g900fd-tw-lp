.class Lcom/sec/android/app/voicenote/common/util/VNLocationManager$GeocoderThread;
.super Landroid/os/AsyncTask;
.source "VNLocationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/util/VNLocationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GeocoderThread"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/location/Location;",
        "Ljava/lang/Void;",
        "Landroid/location/Address;",
        ">;"
    }
.end annotation


# instance fields
.field private mAddresses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 142
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager$GeocoderThread;->mAddresses:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/common/util/VNLocationManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/common/util/VNLocationManager$1;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager$GeocoderThread;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/location/Location;)Landroid/location/Address;
    .locals 10
    .param p1, "location"    # [Landroid/location/Location;

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 148
    :try_start_0
    new-instance v1, Landroid/location/Geocoder;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->access$000()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager$GeocoderThread;->mAddresses:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 155
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager$GeocoderThread;->mAddresses:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager$GeocoderThread;->mAddresses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 156
    aget-object v1, p1, v8

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    # setter for: Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLatitude:D
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->access$102(D)D

    .line 157
    aget-object v1, p1, v8

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    # setter for: Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLongitude:D
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->access$202(D)D

    .line 158
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNLocationManager$GeocoderThread;->mAddresses:Ljava/util/List;

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Address;

    .line 162
    :goto_1
    return-object v1

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v1, "VNLocationManager"

    const-string v2, "GeocoderThread, ArrayIndexOutOfBoundsException"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v7

    .line 151
    goto :goto_1

    .line 152
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :catch_1
    move-exception v0

    .line 153
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 160
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    invoke-virtual {p0, v9}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager$GeocoderThread;->cancel(Z)Z

    move-object v1, v7

    .line 162
    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 142
    check-cast p1, [Landroid/location/Location;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager$GeocoderThread;->doInBackground([Landroid/location/Location;)Landroid/location/Address;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 195
    # invokes: Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->removeUpdate()V
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->access$500()V

    .line 196
    return-void
.end method

.method protected onPostExecute(Landroid/location/Address;)V
    .locals 6
    .param p1, "result"    # Landroid/location/Address;

    .prologue
    .line 167
    if-nez p1, :cond_0

    .line 168
    const-string v4, "VNLocationManager"

    const-string v5, " GeocoderThread, Address return null"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :goto_0
    return-void

    .line 171
    :cond_0
    invoke-virtual {p1}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v1

    .line 172
    .local v1, "CountryName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "AdminArea":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v2

    .line 174
    .local v2, "Locality":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v3

    .line 176
    .local v3, "Thoroughfare":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "|"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "|"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mAddrTag:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->access$302(Ljava/lang/String;)Ljava/lang/String;

    .line 179
    # setter for: Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationTag:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->access$402(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 180
    # invokes: Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->removeUpdate()V
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->access$500()V

    .line 188
    :cond_1
    :goto_1
    const-string v4, "VNLocationManager"

    const-string v5, "Find location "

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    goto :goto_0

    .line 181
    :cond_2
    # setter for: Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationTag:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->access$402(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 182
    # invokes: Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->removeUpdate()V
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->access$500()V

    goto :goto_1

    .line 183
    :cond_3
    # setter for: Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationTag:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->access$402(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 184
    # invokes: Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->removeUpdate()V
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->access$500()V

    goto :goto_1

    .line 185
    :cond_4
    # setter for: Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->mLocationTag:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->access$402(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 186
    # invokes: Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->removeUpdate()V
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->access$500()V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 142
    check-cast p1, Landroid/location/Address;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager$GeocoderThread;->onPostExecute(Landroid/location/Address;)V

    return-void
.end method
