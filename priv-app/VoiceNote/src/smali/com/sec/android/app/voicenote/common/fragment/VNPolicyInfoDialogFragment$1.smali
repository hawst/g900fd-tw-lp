.class Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;
.super Ljava/lang/Object;
.source "VNPolicyInfoDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I

    .prologue
    const/4 v2, 0x3

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->dismissAllowingStateLoss()V

    .line 99
    :cond_0
    const-string v0, "show_policy_info"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    if-eqz v0, :cond_3

    .line 101
    const-string v0, "record_mode"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 103
    const-string v0, "show_stt_recc_info"

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 105
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->newInstance(I)Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "MEMO"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 120
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$Callbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$Callbacks;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$Callbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$Callbacks;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$Callbacks;->onVoiceMemoAgree(I)V

    .line 123
    :cond_2
    return-void

    .line 109
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    if-eqz v0, :cond_1

    .line 110
    const-string v0, "show_stt_recc_info"

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "FRAGMENT_DIALOG_2"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_1

    .line 112
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->newInstance(I)Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DIALOG_2"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "MEMO"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 117
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment$1;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;->access$000(Lcom/sec/android/app/voicenote/common/fragment/VNPolicyInfoDialogFragment;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->startBind()V

    goto :goto_0
.end method
