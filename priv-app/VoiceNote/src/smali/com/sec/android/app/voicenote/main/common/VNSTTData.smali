.class public Lcom/sec/android/app/voicenote/main/common/VNSTTData;
.super Ljava/lang/Object;
.source "VNSTTData.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;
    }
.end annotation


# instance fields
.field public words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "wordsize"    # I

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-array v0, p1, [Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    .line 29
    return-void
.end method


# virtual methods
.method public clone()Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    .locals 6

    .prologue
    .line 86
    const/4 v3, 0x0

    .line 88
    .local v3, "temp":Lcom/sec/android/app/voicenote/main/common/VNSTTData;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    move-object v3, v0

    .line 89
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    array-length v4, v4

    if-ge v2, v4, :cond_0

    .line 90
    iget-object v5, v3, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    check-cast v4, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aput-object v4, v5, v2

    .line 91
    iget-object v4, v3, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v4, v4, v2

    iget-object v4, v4, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->word:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->words:[Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    aget-object v5, v5, v2

    iget-object v5, v5, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->word:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 93
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 94
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v1}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    .line 96
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    :cond_0
    return-object v3
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSTTData;->clone()Lcom/sec/android/app/voicenote/main/common/VNSTTData;

    move-result-object v0

    return-object v0
.end method
