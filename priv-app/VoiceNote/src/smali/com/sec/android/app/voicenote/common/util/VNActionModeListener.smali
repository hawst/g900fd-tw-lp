.class public abstract Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;
.super Ljava/lang/Object;
.source "VNActionModeListener.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;
.implements Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$Callbacks;
    }
.end annotation


# static fields
.field public static final ACTION_MODE_ALL:I = 0x0

.field public static final ACTION_MODE_DELETE:I = 0x3

.field public static final ACTION_MODE_PERSONA:I = 0x2

.field public static final ACTION_MODE_SELECT:I = 0x1

.field public static final NONE_SELECTED:I = -0x1

.field public static final REQUEST_ACTION_MODE_FINISH:I = 0x64

.field protected static final TAG:Ljava/lang/String; = "VNLibraryActionModeListener"


# instance fields
.field protected mActionCheckBox:Landroid/widget/CheckBox;

.field protected mActionTitle:Landroid/widget/TextView;

.field private mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$Callbacks;

.field private mCheckedItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDeleteDialgFragmet:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/DialogFragment;",
            ">;"
        }
    .end annotation
.end field

.field protected mMenuMode:I

.field protected mMode:Landroid/view/ActionMode;

.field protected mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

.field private mPrivateModeBinder:Landroid/os/IBinder;

.field private mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

.field public mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewPosition"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mMenuMode:I

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$Callbacks;

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mMode:Landroid/view/ActionMode;

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeBinder:Landroid/os/IBinder;

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mContext:Landroid/content/Context;

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mActionTitle:Landroid/widget/TextView;

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mActionCheckBox:Landroid/widget/CheckBox;

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mDeleteDialgFragmet:Ljava/lang/ref/WeakReference;

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mCheckedItemList:Ljava/util/ArrayList;

    .line 497
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$4;-><init>(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    .line 72
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mContext:Landroid/content/Context;

    .line 75
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 76
    const/4 v1, -0x1

    if-eq p2, v1, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p2, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "VNLibraryActionModeListener"

    const-string v2, "VNLibraryActionModeListener() : list is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;)Landroid/os/IBinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;Landroid/os/IBinder;)Landroid/os/IBinder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;
    .param p1, "x1"    # Landroid/os/IBinder;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeBinder:Landroid/os/IBinder;

    return-object p1
.end method


# virtual methods
.method public doAction(ILandroid/content/Intent;)V
    .locals 12
    .param p1, "optionId"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 449
    const-string v10, "VNLibraryActionModeListener"

    const-string v11, "doAction()"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getParentFragment()Landroid/app/Fragment;

    move-result-object v2

    .line 451
    .local v2, "fragment":Landroid/app/Fragment;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/app/Fragment;->isResumed()Z

    move-result v10

    if-nez v10, :cond_2

    .line 452
    :cond_0
    const-string v10, "VNLibraryActionModeListener"

    const-string v11, "doAction() : activity is not resumed"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    :cond_1
    :goto_0
    return-void

    .line 456
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v7

    .line 457
    .local v7, "listView":Landroid/widget/ListView;
    if-nez v7, :cond_3

    .line 458
    const-string v10, "VNLibraryActionModeListener"

    const-string v11, "doAction() : list view is null"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 462
    :cond_3
    invoke-virtual {v7}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    .line 463
    .local v0, "arr":Landroid/util/SparseBooleanArray;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 465
    .local v1, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v6

    .line 467
    .local v6, "len":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v6, :cond_5

    .line 468
    invoke-virtual {v0, v3}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v10

    invoke-virtual {v0, v10}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 469
    invoke-virtual {v0, v3}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getItemId(I)J

    move-result-wide v4

    .line 470
    .local v4, "id":J
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467
    .end local v4    # "id":J
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 474
    :cond_5
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 476
    :pswitch_0
    const-string v10, "VNMultiShareViaSTTSelectDialogFragment"

    const/4 v11, -0x1

    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 477
    .local v9, "shareMode":I
    invoke-static {v2, v9}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->getInstance(Landroid/app/Fragment;I)Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    move-result-object v8

    .line 478
    .local v8, "operator":Lcom/sec/android/app/voicenote/common/util/IVNOperation;
    if-eqz v8, :cond_1

    .line 479
    iget-object v10, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    invoke-interface {v8, v2, v1, v10}, Lcom/sec/android/app/voicenote/common/util/IVNOperation;->startOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V

    goto :goto_0

    .line 485
    .end local v8    # "operator":Lcom/sec/android/app/voicenote/common/util/IVNOperation;
    .end local v9    # "shareMode":I
    :pswitch_1
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->getInstance(Landroid/app/Fragment;)Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;

    move-result-object v8

    .line 486
    .restart local v8    # "operator":Lcom/sec/android/app/voicenote/common/util/IVNOperation;
    if-eqz v8, :cond_1

    .line 487
    iget-object v10, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    invoke-interface {v8, v2, v1, v10}, Lcom/sec/android/app/voicenote/common/util/IVNOperation;->startOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V

    goto :goto_0

    .line 474
    :pswitch_data_0
    .packed-switch 0x7f0e00f8
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected abstract finishActionMode()V
.end method

.method protected abstract getItemCount()I
.end method

.method protected abstract getItemId(I)J
.end method

.method public abstract getList()Landroid/widget/ListView;
.end method

.method public getMenuMode()I
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mMenuMode:I

    return v0
.end method

.method protected abstract getParentFragment()Landroid/app/Fragment;
.end method

.method public invalidateActionMode()V
    .locals 0

    .prologue
    .line 505
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->invalidateTitle()V

    .line 506
    return-void
.end method

.method public invalidateTitle()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 510
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v0

    .line 511
    .local v0, "listView":Landroid/widget/ListView;
    if-nez v0, :cond_1

    .line 512
    const-string v3, "VNLibraryActionModeListener"

    const-string v4, "invalidataTitle() : list view is null"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    :cond_0
    :goto_0
    return-void

    .line 516
    :cond_1
    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v1

    .line 517
    .local v1, "selectedCount":I
    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v2

    .line 519
    .local v2, "validListItemCount":I
    if-nez v2, :cond_2

    .line 520
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->stopActionMode()V

    goto :goto_0

    .line 524
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mMode:Landroid/view/ActionMode;

    if-eqz v3, :cond_0

    .line 525
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mMode:Landroid/view/ActionMode;

    invoke-virtual {v3}, Landroid/view/ActionMode;->getCustomView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0e0023

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mActionTitle:Landroid/widget/TextView;

    .line 526
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mActionTitle:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    .line 527
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mActionTitle:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b011b

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 532
    if-eq v1, v2, :cond_3

    .line 533
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mActionCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 535
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mActionCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method protected abstract isCursorValid()Z
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 28
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 262
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getParentFragment()Landroid/app/Fragment;

    move-result-object v13

    .line 263
    .local v13, "fragment":Landroid/app/Fragment;
    if-eqz v13, :cond_0

    invoke-virtual {v13}, Landroid/app/Fragment;->isResumed()Z

    move-result v24

    if-nez v24, :cond_1

    .line 264
    :cond_0
    const-string v24, "VNLibraryActionModeListener"

    const-string v25, "onActionItemClicked() : activity is not resumed"

    invoke-static/range {v24 .. v25}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const/16 v24, 0x0

    .line 445
    :goto_0
    return v24

    .line 268
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v19

    .line 269
    .local v19, "listView":Landroid/widget/ListView;
    if-nez v19, :cond_2

    .line 270
    const-string v24, "VNLibraryActionModeListener"

    const-string v25, "onActionItemClicked() : list view is null"

    invoke-static/range {v24 .. v25}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const/16 v24, 0x0

    goto :goto_0

    .line 274
    :cond_2
    invoke-virtual/range {v19 .. v19}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v5

    .line 275
    .local v5, "arr":Landroid/util/SparseBooleanArray;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 277
    .local v6, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {v5}, Landroid/util/SparseBooleanArray;->size()I

    move-result v18

    .line 279
    .local v18, "len":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    move/from16 v0, v18

    if-ge v14, v0, :cond_4

    .line 280
    invoke-virtual {v5, v14}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v24

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 281
    invoke-virtual {v5, v14}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v24

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getItemId(I)J

    move-result-wide v16

    .line 282
    .local v16, "id":J
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    .end local v16    # "id":J
    :cond_3
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 285
    :cond_4
    invoke-static {v6}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 287
    const-wide/16 v8, -0x1

    .line 288
    .local v8, "checkedItemId":J
    invoke-virtual/range {v19 .. v19}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v24

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_5

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v24

    if-lez v24, :cond_5

    .line 289
    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Long;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 292
    :cond_5
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v15

    .line 293
    .local v15, "itemId":I
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/ActionMode;->setTag(Ljava/lang/Object;)V

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$Callbacks;

    move-object/from16 v24, v0

    if-eqz v24, :cond_6

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$Callbacks;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-interface {v0, v15, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$Callbacks;->onActionItemSelected(IJ)Z

    .line 299
    :cond_6
    packed-switch v15, :pswitch_data_0

    .line 442
    :pswitch_0
    const/16 v24, 0x0

    goto/16 :goto_0

    .line 301
    :pswitch_1
    const/16 v24, -0x1

    move/from16 v0, v24

    invoke-static {v13, v0}, Lcom/sec/android/app/voicenote/common/util/FileShareOperation;->getInstance(Landroid/app/Fragment;I)Lcom/sec/android/app/voicenote/common/util/FileShareOperation;

    move-result-object v20

    .line 302
    .local v20, "operator":Lcom/sec/android/app/voicenote/common/util/IVNOperation;
    if-eqz v20, :cond_7

    .line 303
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getParentFragment()Landroid/app/Fragment;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    move-object/from16 v25, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-interface {v0, v1, v6, v2}, Lcom/sec/android/app/voicenote/common/util/IVNOperation;->askOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)Ljava/lang/ref/WeakReference;

    .line 445
    .end local v20    # "operator":Lcom/sec/android/app/voicenote/common/util/IVNOperation;
    :cond_7
    :goto_2
    const/16 v24, 0x0

    goto/16 :goto_0

    .line 311
    :pswitch_2
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v23

    .line 312
    .local v23, "title":Ljava/lang/String;
    new-instance v10, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    invoke-direct {v10}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;-><init>()V

    .line 313
    .local v10, "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;
    move-object/from16 v0, v23

    invoke-virtual {v10, v6, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->SetParameter(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 314
    invoke-virtual {v13}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v24

    const-string v25, "FRAGMENT_DIALOG"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_2

    .line 319
    .end local v10    # "dialog":Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;
    .end local v23    # "title":Ljava/lang/String;
    :pswitch_3
    invoke-static {v13}, Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;->getInstance(Landroid/app/Fragment;)Lcom/sec/android/app/voicenote/common/util/FileDeleteOperation;

    move-result-object v20

    .line 320
    .restart local v20    # "operator":Lcom/sec/android/app/voicenote/common/util/IVNOperation;
    if-eqz v20, :cond_7

    .line 321
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getParentFragment()Landroid/app/Fragment;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    move-object/from16 v25, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-interface {v0, v1, v6, v2}, Lcom/sec/android/app/voicenote/common/util/IVNOperation;->askOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)Ljava/lang/ref/WeakReference;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mDeleteDialgFragmet:Ljava/lang/ref/WeakReference;

    .line 322
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mCheckedItemList:Ljava/util/ArrayList;

    goto :goto_2

    .line 328
    .end local v20    # "operator":Lcom/sec/android/app/voicenote/common/util/IVNOperation;
    :pswitch_4
    instance-of v0, v13, Lcom/sec/android/app/voicenote/common/util/IVNOperation;

    move/from16 v24, v0

    if-eqz v24, :cond_7

    move-object/from16 v20, v13

    .line 329
    check-cast v20, Lcom/sec/android/app/voicenote/common/util/IVNOperation;

    .line 330
    .restart local v20    # "operator":Lcom/sec/android/app/voicenote/common/util/IVNOperation;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    move-object/from16 v24, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-interface {v0, v13, v6, v1}, Lcom/sec/android/app/voicenote/common/util/IVNOperation;->askOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)Ljava/lang/ref/WeakReference;

    goto :goto_2

    .line 336
    .end local v20    # "operator":Lcom/sec/android/app/voicenote/common/util/IVNOperation;
    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getParentFragment()Landroid/app/Fragment;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v24

    const-string v25, "VNBookmarkListFragment"

    invoke-virtual/range {v24 .. v25}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    .line 337
    .local v7, "bl":Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    if-eqz v7, :cond_7

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->isResumed()Z

    move-result v24

    if-eqz v24, :cond_7

    .line 338
    const v24, 0x7f0e00f4

    long-to-int v0, v8

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->onMenuItemClick(II)Z

    goto/16 :goto_2

    .line 344
    .end local v7    # "bl":Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    :pswitch_6
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_7

    .line 346
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->newInstance()Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    move-result-object v12

    .line 348
    .local v12, "frag":Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 349
    .local v21, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 350
    .local v22, "targetID":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v14, 0x0

    :goto_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v24

    move/from16 v0, v24

    if-ge v14, v0, :cond_8

    .line 351
    invoke-virtual {v13}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v25

    invoke-virtual {v6, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Long;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    invoke-static/range {v25 .. v27}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    invoke-virtual {v6, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 350
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 354
    :cond_8
    invoke-virtual {v12}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    .line 355
    .local v4, "arguments":Landroid/os/Bundle;
    const-string v24, "target_datas"

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 356
    const-string v24, "target_id"

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 357
    const-string v24, "movetosecret"

    const/16 v25, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 358
    const-string v24, "multi"

    const/16 v25, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 359
    invoke-virtual {v13}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v24

    const-string v25, "FRAGMENT_FILE"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v12, v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 360
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    move-object/from16 v24, v0

    if-eqz v24, :cond_7

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;->finishOperation()V

    goto/16 :goto_2

    .line 367
    .end local v4    # "arguments":Landroid/os/Bundle;
    .end local v12    # "frag":Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
    .end local v21    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v22    # "targetID":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;->finishOperation()V

    goto/16 :goto_2

    .line 371
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;->finishOperation()V

    goto/16 :goto_2

    .line 375
    :pswitch_9
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMode()Z

    move-result v24

    if-eqz v24, :cond_a

    .line 376
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->newInstance()Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    move-result-object v12

    .line 377
    .local v12, "frag":Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    invoke-virtual {v12}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    .line 378
    .restart local v4    # "arguments":Landroid/os/Bundle;
    const-string v24, "ids"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 379
    const-string v24, "mode"

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 380
    invoke-virtual {v13}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v24

    const-string v25, "FRAGMENT_FILE"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v12, v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 381
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    move-object/from16 v24, v0

    if-eqz v24, :cond_9

    .line 382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;->finishOperation()V

    .line 425
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const-string v25, "MOVP"

    invoke-static/range {v24 .. v25}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 386
    .end local v4    # "arguments":Landroid/os/Bundle;
    .end local v12    # "frag":Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    :cond_a
    :try_start_0
    new-instance v24, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6, v13}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$3;-><init>(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;Ljava/util/ArrayList;Landroid/app/Fragment;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    move-object/from16 v25, v0

    invoke-static/range {v24 .. v25}, Lcom/samsung/android/privatemode/PrivateModeManager;->getInstance(Landroid/content/Context;Lcom/samsung/android/privatemode/IPrivateModeClient;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    move-object/from16 v24, v0

    if-eqz v24, :cond_7

    .line 416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;->finishOperation()V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_2

    .line 418
    :catch_0
    move-exception v11

    .line 419
    .local v11, "e":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v11}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto/16 :goto_2

    .line 420
    .end local v11    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v11

    .line 421
    .local v11, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v11}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto/16 :goto_2

    .line 430
    .end local v11    # "e":Ljava/lang/NoSuchMethodError;
    :pswitch_a
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->newInstance()Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    move-result-object v12

    .line 431
    .restart local v12    # "frag":Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    invoke-virtual {v12}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    .line 432
    .restart local v4    # "arguments":Landroid/os/Bundle;
    const-string v24, "ids"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 433
    const-string v24, "mode"

    const/16 v25, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 434
    invoke-virtual {v13}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v24

    const-string v25, "FRAGMENT_FILE"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v12, v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 435
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    move-object/from16 v24, v0

    if-eqz v24, :cond_7

    .line 436
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mOpearationCallback:Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;->finishOperation()V

    goto/16 :goto_2

    .line 299
    :pswitch_data_0
    .packed-switch 0x7f0e00f4
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_6
        :pswitch_6
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public onActionModeItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)Z
    .locals 3
    .param p1, "parent"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->isCursorValid()Z

    move-result v2

    if-nez v2, :cond_0

    .line 208
    const-string v1, "VNLibraryActionModeListener"

    const-string v2, "onActionModeItemClick() : cursor is invalid"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :goto_0
    return v0

    .line 211
    :cond_0
    invoke-virtual {p1}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    :cond_1
    invoke-virtual {p1, p3, v0}, Landroid/widget/ListView;->setItemChecked(IZ)V

    move v0, v1

    .line 212
    goto :goto_0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 6
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v4, 0x0

    .line 86
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mMode:Landroid/view/ActionMode;

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getParentFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 89
    .local v0, "fragment":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Fragment;->isResumed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 90
    :cond_0
    const-string v3, "VNLibraryActionModeListener"

    const-string v5, "onCreateActionMode() : activity is not resumed"

    invoke-static {v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 107
    :goto_0
    return v3

    .line 94
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getParentFragment()Landroid/app/Fragment;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 95
    .local v2, "mActivity":Landroid/app/Activity;
    instance-of v3, v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    if-eqz v3, :cond_2

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getParentFragment()Landroid/app/Fragment;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$Callbacks;

    iput-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$Callbacks;

    .line 99
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v1

    .line 100
    .local v1, "listView":Landroid/widget/ListView;
    if-nez v1, :cond_3

    .line 101
    const-string v3, "VNLibraryActionModeListener"

    const-string v5, "onCreateActionMode() : list view is null"

    invoke-static {v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 102
    goto :goto_0

    .line 105
    :cond_3
    invoke-virtual {p0, p1, v2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->setAllViews(Landroid/view/ActionMode;Landroid/app/Activity;)V

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->invalidateTitle()V

    .line 107
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 3
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    .line 133
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->clearChoices()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 142
    :goto_0
    return-void

    .line 135
    :catch_0
    move-exception v0

    .line 136
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "VNLibraryActionModeListener"

    const-string v2, "onDestroyActionMode() : list is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 138
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 139
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "VNLibraryActionModeListener"

    const-string v2, "onDestroyActionMode() : Content view not yet created"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 13
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v7, 0x0

    const/4 v12, 0x0

    .line 152
    const-string v8, "VNLibraryActionModeListener"

    const-string v9, "onPrepareActionMode()"

    invoke-static {v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v6

    .line 154
    .local v6, "listView":Landroid/widget/ListView;
    if-nez v6, :cond_0

    .line 155
    const-string v8, "VNLibraryActionModeListener"

    const-string v9, "onPrepareActionMode() : list view is null"

    invoke-static {v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    .line 199
    :goto_0
    return v7

    .line 160
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getItemCount()I

    move-result v8

    if-nez v8, :cond_1

    .line 161
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    goto :goto_0

    .line 165
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mDeleteDialgFragmet:Ljava/lang/ref/WeakReference;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mDeleteDialgFragmet:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 166
    invoke-virtual {v6}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v7

    if-nez v7, :cond_3

    .line 167
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mDeleteDialgFragmet:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/DialogFragment;

    invoke-virtual {v7}, Landroid/app/DialogFragment;->dismiss()V

    .line 168
    iput-object v12, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mDeleteDialgFragmet:Ljava/lang/ref/WeakReference;

    .line 198
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->invalidateTitle()V

    .line 199
    const/4 v7, 0x1

    goto :goto_0

    .line 170
    :cond_3
    invoke-virtual {v6}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    .line 171
    .local v0, "arr":Landroid/util/SparseBooleanArray;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 173
    .local v1, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v3

    .line 175
    .local v3, "len":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v3, :cond_5

    .line 176
    invoke-virtual {v0, v2}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v7

    invoke-virtual {v0, v7}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 177
    invoke-virtual {v0, v2}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getItemId(I)J

    move-result-wide v4

    .line 178
    .local v4, "id":J
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    .end local v4    # "id":J
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 181
    :cond_5
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 183
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mCheckedItemList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ne v7, v3, :cond_6

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eq v7, v3, :cond_7

    .line 184
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mDeleteDialgFragmet:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/DialogFragment;

    invoke-virtual {v7}, Landroid/app/DialogFragment;->dismiss()V

    .line 185
    iput-object v12, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mDeleteDialgFragmet:Ljava/lang/ref/WeakReference;

    goto :goto_1

    .line 187
    :cond_7
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v3, :cond_2

    .line 188
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mCheckedItemList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_8

    .line 189
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mDeleteDialgFragmet:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/DialogFragment;

    invoke-virtual {v7}, Landroid/app/DialogFragment;->dismiss()V

    .line 190
    iput-object v12, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mDeleteDialgFragmet:Ljava/lang/ref/WeakReference;

    goto :goto_1

    .line 187
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public onVNItemSelected(Landroid/widget/AdapterView;I)V
    .locals 7
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;I)V"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v4

    .line 236
    .local v4, "listView":Landroid/widget/ListView;
    if-nez v4, :cond_0

    .line 237
    const-string v5, "VNLibraryActionModeListener"

    const-string v6, "onVNItemSelected() : list view is null"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :goto_0
    return-void

    .line 241
    :cond_0
    const/4 v2, 0x0

    .line 242
    .local v2, "checkedNothing":Z
    const/4 v0, 0x0

    .line 244
    .local v0, "checkedAll":Z
    invoke-virtual {v4}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v1

    .line 245
    .local v1, "checkedItemCount":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getItemCount()I

    move-result v3

    .line 247
    .local v3, "groupCount":I
    if-nez v1, :cond_3

    .line 248
    const/4 v2, 0x1

    .line 253
    :cond_1
    :goto_1
    if-nez p2, :cond_4

    if-nez v2, :cond_2

    if-nez v0, :cond_4

    .line 254
    :cond_2
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->selectAll(Z)V

    goto :goto_0

    .line 249
    :cond_3
    if-ne v1, v3, :cond_1

    .line 250
    const/4 v0, 0x1

    goto :goto_1

    .line 256
    :cond_4
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->selectAll(Z)V

    goto :goto_0
.end method

.method protected abstract refreashActionMode()V
.end method

.method public selectAll(Z)V
    .locals 9
    .param p1, "check"    # Z

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v1

    .line 217
    .local v1, "listView":Landroid/widget/ListView;
    if-nez v1, :cond_0

    .line 218
    const-string v3, "VNLibraryActionModeListener"

    const-string v4, "selectAll() : list view is null"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :goto_0
    return-void

    .line 222
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->getItemCount()I

    move-result v2

    .line 224
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 225
    invoke-virtual {v1, v0, p1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 224
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 227
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mActionTitle:Landroid/widget/TextView;

    if-eqz v3, :cond_2

    .line 228
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mActionTitle:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b011b

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->refreashActionMode()V

    goto :goto_0
.end method

.method protected setAllViews(Landroid/view/ActionMode;Landroid/app/Activity;)V
    .locals 4
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 111
    invoke-virtual {p2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03000c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 112
    .local v0, "actionModeView":Landroid/view/View;
    const v1, 0x7f0e0023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mActionTitle:Landroid/widget/TextView;

    .line 113
    const v1, 0x7f0e0022

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mActionCheckBox:Landroid/widget/CheckBox;

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mActionTitle:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$1;-><init>(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mActionCheckBox:Landroid/widget/CheckBox;

    new-instance v2, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener$2;-><init>(Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    invoke-virtual {p1, v0}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    .line 128
    return-void
.end method

.method public stopActionMode()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->mMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 148
    :cond_0
    return-void
.end method
