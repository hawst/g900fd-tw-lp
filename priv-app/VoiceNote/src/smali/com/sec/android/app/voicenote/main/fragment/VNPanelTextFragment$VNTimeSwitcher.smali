.class Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNTimeSwitcher;
.super Landroid/widget/ImageSwitcher;
.source "VNPanelTextFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VNTimeSwitcher"
.end annotation


# static fields
.field private static mBlinkAni:Landroid/view/animation/AlphaAnimation;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 295
    invoke-direct {p0, p1}, Landroid/widget/ImageSwitcher;-><init>(Landroid/content/Context;)V

    .line 296
    return-void
.end method

.method public static declared-synchronized getBlinkAnimation()Landroid/view/animation/AlphaAnimation;
    .locals 4

    .prologue
    .line 299
    const-class v1, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNTimeSwitcher;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNTimeSwitcher;->mBlinkAni:Landroid/view/animation/AlphaAnimation;

    if-nez v0, :cond_0

    .line 300
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNTimeSwitcher;->mBlinkAni:Landroid/view/animation/AlphaAnimation;

    .line 301
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNTimeSwitcher;->mBlinkAni:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 302
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNTimeSwitcher;->mBlinkAni:Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/animation/AlphaAnimation;->setRepeatCount(I)V

    .line 303
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNTimeSwitcher;->mBlinkAni:Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/view/animation/AlphaAnimation;->setRepeatMode(I)V

    .line 304
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNTimeSwitcher;->mBlinkAni:Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 306
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNTimeSwitcher;->mBlinkAni:Landroid/view/animation/AlphaAnimation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 299
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
