.class public Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;
.super Lcom/nuance/dragon/toolkit/audio/AudioChunk;
.source "SamsungAudioChunk.java"


# instance fields
.field public endTimeStamp:J

.field public timeStamp:J


# direct methods
.method public constructor <init>(Lcom/nuance/dragon/toolkit/audio/AudioType;[BI)V
    .locals 2
    .param p1, "type"    # Lcom/nuance/dragon/toolkit/audio/AudioType;
    .param p2, "data"    # [B
    .param p3, "duration"    # I

    .prologue
    const-wide/16 v0, 0x0

    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/nuance/dragon/toolkit/audio/AudioChunk;-><init>(Lcom/nuance/dragon/toolkit/audio/AudioType;[BI)V

    .line 25
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;->timeStamp:J

    .line 26
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;->endTimeStamp:J

    .line 30
    return-void
.end method


# virtual methods
.method public setTimeStamp(JI)V
    .locals 5
    .param p1, "aTimeStamp"    # J
    .param p3, "aDuration"    # I

    .prologue
    .line 33
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;->timeStamp:J

    .line 34
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;->timeStamp:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/main/stt/SamsungAudioChunk;->endTimeStamp:J

    .line 35
    return-void
.end method
