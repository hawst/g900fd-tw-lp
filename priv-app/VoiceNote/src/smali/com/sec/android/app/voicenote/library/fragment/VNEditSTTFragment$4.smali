.class Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;
.super Ljava/lang/Object;
.source "VNEditSTTFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private onTrimHandlerChanged(II)Z
    .locals 1
    .param p1, "leftTrim"    # I
    .param p2, "rightTrim"    # I

    .prologue
    .line 292
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimCallback:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1800()Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimCallback:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1800()Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;->onTrimHandlerChanged(II)V

    .line 294
    const/4 v0, 0x1

    .line 296
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 159
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    if-ne v7, v9, :cond_7

    .line 160
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 161
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;
    invoke-static {v7, v11}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$002(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Landroid/view/View;)Landroid/view/View;

    .line 162
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    invoke-interface {v7, v8}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    move v7, v8

    .line 246
    .end local p1    # "view":Landroid/view/View;
    :goto_0
    return v7

    .line 165
    .restart local p1    # "view":Landroid/view/View;
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsTouching:Z
    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$202(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Z)Z

    .line 166
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsLongPressed:Z
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 167
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsLongPressed:Z
    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$102(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Z)Z

    move v7, v9

    .line 168
    goto :goto_0

    .line 171
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    check-cast p1, Landroid/widget/TextView;

    .end local p1    # "view":Landroid/view/View;
    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getOffset(Landroid/widget/TextView;Landroid/view/MotionEvent;)I
    invoke-static {v7, p1, p2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$800(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Landroid/widget/TextView;Landroid/view/MotionEvent;)I

    move-result v5

    .line 172
    .local v5, "offset":I
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v11

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getSelectStringIndex(Ljava/util/ArrayList;I)I
    invoke-static {v10, v11, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1100(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Ljava/util/ArrayList;I)I

    move-result v10

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I
    invoke-static {v7, v10}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$902(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;I)I

    .line 173
    const/4 v7, -0x1

    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I
    invoke-static {v10}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v10

    if-ne v7, v10, :cond_2

    .line 174
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "can not find word at offset : "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    move v7, v9

    .line 175
    goto :goto_0

    .line 178
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    move-result-object v7

    if-nez v7, :cond_5

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->isResumed()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 179
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    new-instance v10, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-direct {v10}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;-><init>()V

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    invoke-static {v7, v10}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1202(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    .line 180
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    move-result-object v10

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v7

    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v11

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v7, v7, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    invoke-virtual {v10, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->setNBestStrings([Ljava/lang/String;)V

    .line 181
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    move-result-object v7

    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    iget-object v10, v10, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    invoke-virtual {v7, v10}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->setCallback(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;)V

    .line 182
    new-array v2, v12, [I

    .line 183
    .local v2, "loc":[I
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const v10, 0x7f0e0016

    invoke-virtual {v7, v10}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    .line 184
    .local v1, "fragment":Landroid/app/Fragment;
    if-eqz v1, :cond_6

    .line 185
    invoke-virtual {v1}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 193
    :cond_3
    :goto_1
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    move-result-object v7

    invoke-virtual {v1}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v10

    invoke-virtual {v7, v10}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->setYLocation(I)V

    .line 194
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    move-result-object v7

    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-virtual {v10}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    const-string v11, "FRAGMENT_DIALOG"

    invoke-virtual {v7, v10, v11}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 199
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v7

    invoke-interface {v7, v9}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onSTTEditPopup(Z)V

    .line 201
    :cond_4
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1300()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v7

    if-eqz v7, :cond_5

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1300()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 202
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->updateAllWordOnScreen()V

    .end local v1    # "fragment":Landroid/app/Fragment;
    .end local v2    # "loc":[I
    .end local v5    # "offset":I
    :cond_5
    :goto_3
    move v7, v8

    .line 246
    goto/16 :goto_0

    .line 187
    .restart local v1    # "fragment":Landroid/app/Fragment;
    .restart local v2    # "loc":[I
    .restart local v5    # "offset":I
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const v10, 0x7f0e0018

    invoke-virtual {v7, v10}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    .line 188
    if-eqz v1, :cond_3

    .line 189
    invoke-virtual {v1}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    goto :goto_1

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 206
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "fragment":Landroid/app/Fragment;
    .end local v2    # "loc":[I
    .end local v5    # "offset":I
    .restart local p1    # "view":Landroid/view/View;
    :cond_7
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    if-nez v7, :cond_a

    .line 207
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 208
    .local v4, "mTrimStartHandlerRect":Landroid/graphics/Rect;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 209
    .local v3, "mTrimEndHandlerRect":Landroid/graphics/Rect;
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimStartHandler:Landroid/view/View;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 210
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimEndHandler:Landroid/view/View;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1500(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 211
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v10, 0x7f090023

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    .line 212
    .local v6, "padding":I
    iget v7, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v7, v6

    iput v7, v4, Landroid/graphics/Rect;->left:I

    .line 213
    iget v7, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v7, v6

    iput v7, v4, Landroid/graphics/Rect;->right:I

    .line 214
    iget v7, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v7, v6

    iput v7, v3, Landroid/graphics/Rect;->left:I

    .line 215
    iget v7, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v7, v6

    iput v7, v3, Landroid/graphics/Rect;->right:I

    .line 216
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    float-to-int v10, v10

    invoke-virtual {v4, v7, v10}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 217
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    invoke-interface {v7, v9}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 218
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v7

    const-string v8, "onTouch() start handler"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimStartHandler:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v8

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;
    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$002(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Landroid/view/View;)Landroid/view/View;

    .line 220
    invoke-virtual {p0, p2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->setHandlerPosition(Landroid/view/MotionEvent;)Z

    move v7, v9

    .line 221
    goto/16 :goto_0

    .line 222
    :cond_8
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    float-to-int v10, v10

    invoke-virtual {v3, v7, v10}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 223
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    invoke-interface {v7, v9}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 224
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v7

    const-string v8, "onTouch() end handler"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimEndHandler:Landroid/view/View;
    invoke-static {v8}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1500(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v8

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;
    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$002(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Landroid/view/View;)Landroid/view/View;

    .line 226
    invoke-virtual {p0, p2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->setHandlerPosition(Landroid/view/MotionEvent;)Z

    move v7, v9

    .line 227
    goto/16 :goto_0

    .line 229
    :cond_9
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v7

    const-string v10, "onTouch() ~~3"

    invoke-static {v7, v10}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;
    invoke-static {v7, v11}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$002(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Landroid/view/View;)Landroid/view/View;

    .line 231
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsTouching:Z
    invoke-static {v7, v9}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$202(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Z)Z

    goto/16 :goto_3

    .line 233
    .end local v3    # "mTrimEndHandlerRect":Landroid/graphics/Rect;
    .end local v4    # "mTrimStartHandlerRect":Landroid/graphics/Rect;
    .end local v6    # "padding":I
    :cond_a
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    const/4 v10, 0x3

    if-ne v7, v10, :cond_b

    .line 234
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsTouching:Z
    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$202(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Z)Z

    .line 235
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;
    invoke-static {v7, v11}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$002(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Landroid/view/View;)Landroid/view/View;

    .line 237
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    const/4 v9, 0x0

    invoke-interface {v7, v9}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_3

    .line 238
    :catch_1
    move-exception v0

    .line 239
    .restart local v0    # "e":Ljava/lang/Exception;
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 241
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_b
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    if-ne v7, v12, :cond_5

    .line 242
    invoke-virtual {p0, p2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->setHandlerPosition(Landroid/view/MotionEvent;)Z

    move-result v7

    if-eqz v7, :cond_5

    move v7, v9

    .line 243
    goto/16 :goto_0
.end method

.method public setHandlerPosition(Landroid/view/MotionEvent;)Z
    .locals 20
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 250
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v4

    if-nez v4, :cond_0

    .line 251
    const/4 v4, 0x0

    .line 288
    :goto_0
    return v4

    .line 253
    :cond_0
    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;
    invoke-static {v11}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/View;->getHeight()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    sub-float/2addr v10, v11

    const/4 v11, 0x0

    invoke-static/range {v4 .. v11}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v13

    .line 255
    .local v13, "ev":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;

    move-result-object v5

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getOffset(Landroid/widget/TextView;Landroid/view/MotionEvent;)I
    invoke-static {v4, v5, v13}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$800(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Landroid/widget/TextView;Landroid/view/MotionEvent;)I

    move-result v17

    .line 256
    .local v17, "offset":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v5

    move/from16 v0, v17

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getSelectStringIndex(Ljava/util/ArrayList;I)I
    invoke-static {v4, v5, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1100(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Ljava/util/ArrayList;I)I

    move-result v16

    .line 257
    .local v16, "index":I
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setHandlerPosition() index : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const/4 v4, -0x1

    move/from16 v0, v16

    if-ne v4, v0, :cond_1

    .line 259
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "can not find word at offset : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 263
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimLeft:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1600(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v18

    .line 264
    .local v18, "trimLeft":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimRight:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v19

    .line 265
    .local v19, "trimRight":I
    const-wide/16 v14, 0x0

    .line 266
    .local v14, "elapsedTime":J
    if-lez v16, :cond_2

    .line 267
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-wide v14, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    .line 270
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v4

    const v5, 0x7f0e004b

    if-ne v4, v5, :cond_5

    .line 271
    if-nez v16, :cond_4

    .line 272
    const/16 v18, -0x1

    .line 288
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->onTrimHandlerChanged(II)Z

    move-result v4

    goto/16 :goto_0

    .line 274
    :cond_4
    long-to-int v0, v14

    move/from16 v18, v0

    goto :goto_1

    .line 276
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v4

    const v5, 0x7f0e004c

    if-ne v4, v5, :cond_3

    .line 277
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setHandlerPosition() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, v16

    if-ne v0, v4, :cond_6

    .line 279
    const/16 v19, -0x1

    goto :goto_1

    .line 282
    :cond_6
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-wide v6, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-wide v4, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->duration:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-long/2addr v4, v6

    long-to-int v0, v4

    move/from16 v19, v0

    goto/16 :goto_1

    .line 283
    :catch_0
    move-exception v12

    .line 284
    .local v12, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-wide v4, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->duration:J

    add-long/2addr v4, v14

    long-to-int v0, v4

    move/from16 v19, v0

    goto/16 :goto_1
.end method
