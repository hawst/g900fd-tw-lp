.class Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;
.super Landroid/os/AsyncTask;
.source "VNTrimFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShowBookmarkIndicatorsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V
    .locals 0

    .prologue
    .line 871
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;

    .prologue
    .line 871
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 871
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v4, 0x0

    .line 874
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;
    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$2002(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 875
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$000()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 877
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getWidth()I

    move-result v1

    if-nez v1, :cond_0

    .line 879
    const-wide/16 v2, 0x32

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 880
    :catch_0
    move-exception v0

    .line 881
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 884
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->prepareParams()V
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$2100(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V

    .line 886
    :cond_1
    return-object v4
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 871
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 890
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->setIndicators()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$2200(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V

    .line 891
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 892
    return-void
.end method
