.class Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$AppInfo;
.super Ljava/lang/Object;
.source "AudioRecognizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AppInfo"
.end annotation


# static fields
.field public static final AppId:Ljava/lang/String; = "Samsung_Android_VoiceNoteV2_20130911"

.field public static final AppKey:[B

.field public static final Port:I = 0x1bb


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 304
    const/16 v0, 0x40

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer$AppInfo;->AppKey:[B

    return-void

    :array_0
    .array-data 1
        -0x3bt
        -0x33t
        0x28t
        -0x23t
        -0x33t
        -0x58t
        0x10t
        -0x30t
        0x34t
        -0x49t
        -0x10t
        0x4dt
        -0x71t
        0x5ft
        0x31t
        0x35t
        -0x11t
        -0x4ct
        -0x14t
        0x67t
        -0x65t
        0x1bt
        -0x3ft
        -0x7t
        -0x1t
        0x31t
        -0x19t
        -0x3ct
        -0x1bt
        -0x15t
        -0x7ct
        0x25t
        -0x1et
        -0x2t
        0x3ft
        0x2t
        0x12t
        -0x7ft
        0x12t
        0xbt
        0x7t
        -0x31t
        0x64t
        0x42t
        -0x2t
        -0x4et
        0x56t
        -0x44t
        0x53t
        -0x43t
        0x5dt
        -0x58t
        0x36t
        -0x8t
        0x10t
        -0x70t
        -0x61t
        -0x3et
        0x42t
        -0x21t
        0x7et
        -0xdt
        0x53t
        -0x5et
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHostName(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 315
    const/4 v1, 0x0

    .line 316
    .local v1, "hostCode":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/stt/Utils;->readLanguageSetting(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 318
    .local v0, "countryCode":Ljava/lang/String;
    const-string v2, "kor-KOR"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, "ko-kr"

    .line 331
    :cond_0
    :goto_0
    if-eqz v1, :cond_c

    .line 332
    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "hostCode:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "eo.nvc."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".nuancemobility.net"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 335
    :goto_1
    return-object v2

    .line 319
    :cond_1
    const-string v2, "eng-USA"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v1, "en-us"

    goto :goto_0

    .line 320
    :cond_2
    const-string v2, "eng-GBR"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v1, "en-gb"

    goto :goto_0

    .line 321
    :cond_3
    const-string v2, "fra-FRA"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v1, "fr-fr"

    goto :goto_0

    .line 322
    :cond_4
    const-string v2, "deu-DEU"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v1, "de-de"

    goto :goto_0

    .line 323
    :cond_5
    const-string v2, "spa-ESP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v1, "es-es"

    goto :goto_0

    .line 324
    :cond_6
    const-string v2, "spa-XLA"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v1, "es-us"

    goto :goto_0

    .line 325
    :cond_7
    const-string v2, "ita-ITA"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v1, "it-it"

    goto/16 :goto_0

    .line 326
    :cond_8
    const-string v2, "rus-RUS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v1, "ru-ru"

    goto/16 :goto_0

    .line 327
    :cond_9
    const-string v2, "cmn-CHN"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v1, "cn-ma"

    goto/16 :goto_0

    .line 328
    :cond_a
    const-string v2, "jpn-JPN"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v1, "jp-jp"

    goto/16 :goto_0

    .line 329
    :cond_b
    const-string v2, "por-BRA"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v1, "pt-br"

    goto/16 :goto_0

    .line 335
    :cond_c
    const-string v2, "eo.nvc.en-us.nuancemobility.net"

    goto/16 :goto_1
.end method
