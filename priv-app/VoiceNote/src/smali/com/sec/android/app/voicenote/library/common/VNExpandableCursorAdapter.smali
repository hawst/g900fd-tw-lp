.class public Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
.super Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;
.source "VNExpandableCursorAdapter.java"

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;
.implements Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$STTHoverPopupListener;,
        Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;,
        Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;,
        Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;
    }
.end annotation


# static fields
.field private static final ELLIPSIS:Ljava/lang/String; = "..."

.field private static final STT_CHILD_CHARS_COUNT:I = 0x22

.field private static final TAG:Ljava/lang/String; = "VNExpandableCursorAdapter"

.field private static final keyNotifyHandler:Ljava/lang/Object;

.field public static mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader; = null

.field private static mCheckBoxes:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/CheckBox;",
            ">;"
        }
    .end annotation
.end field

.field private static final mVisibleItemsCount:I = 0x7


# instance fields
.field private childSttSearchResults:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;",
            ">;>;"
        }
    .end annotation
.end field

.field private mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

.field private mCheckBoxSize:I

.field private mChildLayout:I

.field private mContext:Landroid/content/Context;

.field private mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

.field private mGroupLayout:I

.field private mGroupViewTemplate:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mHasfadeinAnimation:Z

.field private mInflater:Landroid/view/LayoutInflater;

.field private mIsCheckBoxAnimating:Z

.field private mIsSelectionMode:Z

.field private mSearchMode:Z

.field private mSearchText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 78
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    .line 766
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->keyNotifyHandler:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Landroid/content/Context;II)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "groupLayout"    # I
    .param p4, "childLayout"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 132
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;Z)V

    .line 66
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 67
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    .line 68
    iput v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mGroupLayout:I

    .line 69
    iput v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mChildLayout:I

    .line 70
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->childSttSearchResults:Ljava/util/HashMap;

    .line 73
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    .line 75
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    .line 76
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchMode:Z

    .line 80
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mIsSelectionMode:Z

    .line 81
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mIsCheckBoxAnimating:Z

    .line 83
    iput v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxSize:I

    .line 84
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mHasfadeinAnimation:Z

    .line 86
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mGroupViewTemplate:Ljava/util/ArrayList;

    .line 133
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 134
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    .line 135
    iput p3, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mGroupLayout:I

    .line 136
    iput p4, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mChildLayout:I

    .line 137
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-direct {v0, p2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->LoadLabeltoMap()V

    .line 139
    sget-object v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->keyNotifyHandler:Ljava/lang/Object;

    monitor-enter v1

    .line 140
    :try_start_0
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    if-nez v0, :cond_0

    .line 141
    new-instance v0, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    .line 145
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->registerOnLoadObserver(Lcom/sec/android/app/voicenote/library/common/VNCacheLoader$LoaderAdapter;)V

    .line 147
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->createNewGroupViewTemplate()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mGroupViewTemplate:Ljava/util/ArrayList;

    .line 148
    return-void

    .line 143
    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->setContex(Landroid/content/Context;)V

    goto :goto_0

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mIsCheckBoxAnimating:Z

    return p1
.end method

.method static synthetic access$400()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mIsSelectionMode:Z

    return v0
.end method

.method private addCheckBox(Landroid/widget/CheckBox;)V
    .locals 2
    .param p1, "cb"    # Landroid/widget/CheckBox;

    .prologue
    .line 666
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 667
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0xe

    if-le v0, v1, :cond_0

    .line 668
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 670
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 672
    :cond_1
    return-void
.end method

.method private declared-synchronized controlCheckbox(Landroid/widget/CheckBox;I)V
    .locals 6
    .param p1, "cb"    # Landroid/widget/CheckBox;
    .param p2, "groupPosition"    # I

    .prologue
    .line 675
    monitor-enter p0

    if-nez p1, :cond_0

    .line 706
    :goto_0
    monitor-exit p0

    return-void

    .line 679
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->addCheckBox(Landroid/widget/CheckBox;)V

    .line 681
    const/4 v2, 0x0

    .line 682
    .local v2, "marginRight":I
    const/4 v1, 0x0

    .line 683
    .local v1, "marginLeft":I
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mIsCheckBoxAnimating:Z

    if-nez v3, :cond_2

    .line 685
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mIsSelectionMode:Z

    if-eqz v3, :cond_3

    .line 686
    const/4 v2, 0x0

    .line 687
    const/4 v1, 0x0

    .line 693
    :goto_1
    invoke-virtual {p1}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 694
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-nez v2, :cond_1

    .line 695
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    const v4, 0x418b3333    # 17.4f

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->convertDPtoPX(Landroid/content/Context;FLandroid/util/DisplayMetrics;)I

    move-result v1

    .line 697
    :cond_1
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMarginsRelative(IIII)V

    .line 698
    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 701
    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    invoke-interface {v3, p2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;->isItemChecked(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 702
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 675
    .end local v1    # "marginLeft":I
    .end local v2    # "marginRight":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 689
    .restart local v1    # "marginLeft":I
    .restart local v2    # "marginRight":I
    :cond_3
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_1
    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {p1, v3, v4}, Landroid/widget/CheckBox;->measure(II)V

    .line 691
    invoke-virtual {p1}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v3

    neg-int v2, v3

    goto :goto_1

    .line 704
    :cond_4
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private createNewGroupViewTemplate()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1121
    .local v0, "groupViewTemplate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x7

    if-ge v1, v2, :cond_0

    .line 1122
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getNewGroupView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1121
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1124
    :cond_0
    return-object v0
.end method

.method public static getCacheLoader()Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;
    .locals 1

    .prologue
    .line 176
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    return-object v0
.end method

.method private getDateFormatByFormatSetting(J)Ljava/lang/String;
    .locals 5
    .param p1, "date"    # J

    .prologue
    .line 779
    const-string v0, ""

    .line 780
    .local v0, "dateString":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 781
    .local v1, "shortDateFormat":Ljava/text/DateFormat;
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 783
    return-object v0
.end method

.method private getNewGroupView()Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1089
    new-instance v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;-><init>()V

    .line 1090
    .local v1, "holder":Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v4, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mGroupLayout:I

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1092
    .local v0, "convertView":Landroid/view/View;
    const v3, 0x7f0e0065

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->listrowLayout:Landroid/widget/LinearLayout;

    .line 1093
    const v3, 0x7f0e0067

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    .line 1094
    const v3, 0x7f0e0068

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->dateText:Landroid/widget/TextView;

    .line 1095
    const v3, 0x7f0e0069

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->mmsText:Landroid/widget/TextView;

    .line 1096
    const v3, 0x7f0e006a

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    .line 1098
    const v3, 0x7f0e001f

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->checkbox_wrapper:Landroid/widget/LinearLayout;

    .line 1100
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ar"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "fa"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ur"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1101
    :cond_0
    iget-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->checkbox_wrapper:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1102
    .local v2, "lpView":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, -0x181

    const/4 v4, 0x5

    invoke-virtual {v2, v3, v6, v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1103
    iget-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->checkbox_wrapper:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1106
    .end local v2    # "lpView":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    const v3, 0x7f0e006c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->bookmarkicon:Landroid/widget/ImageView;

    .line 1107
    const v3, 0x7f0e006e

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->stticon:Landroid/widget/ImageView;

    .line 1108
    const v3, 0x7f0e006f

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->playicon:Landroid/widget/ImageView;

    .line 1109
    const v3, 0x7f0e0070

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->pauseicon:Landroid/widget/ImageView;

    .line 1110
    const v3, 0x7f0e006b

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->secreticon:Landroid/widget/ImageView;

    .line 1111
    const v3, 0x7f0e0071

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->expandicon:Landroid/widget/ImageView;

    .line 1112
    const v3, 0x7f0e0066

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->textLayout:Landroid/widget/LinearLayout;

    .line 1113
    const v3, 0x7f0e006d

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->nfcicon:Landroid/widget/ImageView;

    .line 1114
    const v3, 0x7f0e0020

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->checkbox:Landroid/widget/CheckBox;

    .line 1115
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1116
    return-object v0
.end method

.method private setLableHoverPopup(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 626
    if-nez p1, :cond_1

    .line 663
    :cond_0
    :goto_0
    return-void

    .line 629
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 630
    .local v0, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_0

    .line 634
    new-instance v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$2;-><init>(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 641
    invoke-virtual {v0, v2}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 642
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 643
    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    .line 644
    invoke-virtual {v0, v2}, Landroid/widget/HoverPopupWindow;->setGuideLineEnabled(Z)V

    .line 645
    new-instance v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$3;-><init>(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    goto :goto_0
.end method

.method private setPrivateItemDescription(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;)V
    .locals 5
    .param p1, "holder"    # Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;

    .prologue
    .line 612
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 613
    .local v0, "desc":Ljava/lang/StringBuilder;
    if-nez p1, :cond_1

    .line 623
    :cond_0
    :goto_0
    return-void

    .line 614
    :cond_1
    iget-object v3, p1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->secreticon:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->secreticon:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 615
    iget-object v3, p1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 617
    iget-object v3, p1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->secreticon:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0b00f7

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 619
    iget-object v3, p1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 620
    .local v2, "view":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    .line 621
    .local v1, "originalDesc":Ljava/lang/CharSequence;
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 622
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private startCheckBoxAnimation()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1128
    const/4 v0, 0x0

    .line 1129
    .local v0, "endPosition":I
    sget-object v2, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    iget v2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxSize:I

    if-lez v2, :cond_0

    .line 1130
    sget-object v2, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->getWidth()I

    move-result v0

    .line 1135
    const/4 v2, 0x1

    new-array v2, v2, [I

    aput v0, v2, v3

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 1136
    .local v1, "varl":Landroid/animation/ValueAnimator;
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1137
    new-instance v2, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$5;-><init>(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1165
    new-instance v2, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$6;-><init>(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1201
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 1202
    .end local v1    # "varl":Landroid/animation/ValueAnimator;
    :cond_0
    return-void
.end method

.method private stringForTime(J)Ljava/lang/String;
    .locals 13
    .param p1, "timeMs"    # J

    .prologue
    const-wide/16 v10, 0x3c

    .line 769
    const-wide/16 v8, 0x3e8

    div-long v6, p1, v8

    .line 770
    .local v6, "totalSeconds":J
    rem-long v4, v6, v10

    .line 771
    .local v4, "seconds":J
    div-long v8, v6, v10

    rem-long v2, v8, v10

    .line 772
    .local v2, "minutes":J
    const-wide/16 v8, 0xe10

    div-long v0, v6, v8

    .line 774
    .local v0, "hours":J
    const-string v8, "%02d:%02d:%02d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method


# virtual methods
.method protected bindChildView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/content/Context;
    .param p3, "arg2"    # Landroid/database/Cursor;
    .param p4, "arg3"    # Z

    .prologue
    .line 1062
    return-void
.end method

.method protected bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/content/Context;
    .param p3, "arg2"    # Landroid/database/Cursor;
    .param p4, "arg3"    # Z

    .prologue
    .line 1068
    return-void
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 152
    invoke-super {p0, p1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    invoke-interface {v0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;->refreshOptionsMenu()V

    .line 156
    :cond_0
    return-void
.end method

.method public clearCursor()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 180
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroupCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 181
    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setChildrenCursor(ILandroid/database/Cursor;)V

    .line 180
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 183
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setGroupCursor(Landroid/database/Cursor;)V

    .line 184
    return-void
.end method

.method protected ellipsize(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "fullText"    # Ljava/lang/String;
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    const/4 v7, 0x0

    .line 990
    const/4 v2, 0x0

    .line 991
    .local v2, "result":Ljava/lang/String;
    const/4 v4, 0x0

    .line 992
    .local v4, "temp":Ljava/lang/String;
    const/16 v3, 0x31

    .line 994
    .local v3, "sign":C
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-lt v5, p4, :cond_1

    .line 995
    invoke-virtual {p2, p4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 996
    invoke-static {v3}, Ljava/lang/Character;->isLetter(C)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 997
    :cond_0
    const-string v5, " "

    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 998
    .local v1, "lastSpace":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "..."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 999
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1000
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "..."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1004
    .end local v1    # "lastSpace":I
    :cond_1
    if-eqz p3, :cond_3

    .line 1005
    add-int/lit8 v5, p3, -0x1

    invoke-virtual {p2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 1006
    invoke-static {v3}, Ljava/lang/Character;->isLetter(C)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1007
    :cond_2
    const-string v5, " "

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1008
    .local v0, "firstSpace":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "..."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1009
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1010
    move-object p1, v4

    .line 1014
    .end local v0    # "firstSpace":I
    :cond_3
    new-instance v2, Ljava/lang/String;

    .end local v2    # "result":Ljava/lang/String;
    invoke-direct {v2, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 1015
    .restart local v2    # "result":Ljava/lang/String;
    return-object v2
.end method

.method public enableBuildCache(Landroid/widget/AbsListView;)V
    .locals 1
    .param p1, "view"    # Landroid/widget/AbsListView;

    .prologue
    .line 1028
    if-eqz p1, :cond_0

    .line 1029
    new-instance v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$4;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$4;-><init>(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;Landroid/widget/AbsListView;)V

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->post(Ljava/lang/Runnable;)Z

    .line 1038
    :cond_0
    return-void
.end method

.method public findElapsedTime(ILjava/util/ArrayList;)J
    .locals 4
    .param p1, "charIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 884
    .local p2, "sttData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    const/4 v0, 0x0

    .line 885
    .local v0, "checkedIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 886
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget v2, v2, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    if-nez v2, :cond_0

    .line 887
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    .line 888
    if-ge p1, v0, :cond_0

    .line 889
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-wide v2, v2, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    .line 893
    :goto_1
    return-wide v2

    .line 885
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 893
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-wide v2, v2, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    goto :goto_1
.end method

.method public findPosition(Ljava/lang/Object;)I
    .locals 12
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 953
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v3

    .line 954
    .local v3, "cursor":Landroid/database/Cursor;
    if-eqz v3, :cond_0

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 955
    :cond_0
    const-string v10, "VNExpandableCursorAdapter"

    const-string v11, "findPosition : cursor is null"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 956
    const/4 v9, -0x1

    .line 986
    :cond_1
    :goto_0
    return v9

    .line 959
    :cond_2
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 961
    .local v2, "count":I
    const/4 v9, -0x1

    .line 963
    .local v9, "returnedPos":I
    const/4 v8, 0x0

    .local v8, "pos":I
    :goto_1
    if-ge v8, v2, :cond_1

    .line 965
    :try_start_0
    invoke-interface {v3, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 966
    instance-of v10, p1, Ljava/lang/Long;

    if-eqz v10, :cond_3

    .line 967
    move-object v0, p1

    check-cast v0, Ljava/lang/Long;

    move-object v10, v0

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 968
    .local v6, "id":J
    const-string v10, "_id"

    invoke-interface {v3, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v3, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    cmp-long v10, v10, v6

    if-nez v10, :cond_4

    .line 969
    move v9, v8

    goto :goto_0

    .line 972
    .end local v6    # "id":J
    :cond_3
    instance-of v10, p1, Ljava/lang/String;

    if-eqz v10, :cond_4

    .line 973
    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    move-object v5, v0

    .line 974
    .local v5, "path":Ljava/lang/String;
    const-string v10, "_data"

    invoke-interface {v3, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v3, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v10

    if-eqz v10, :cond_4

    .line 975
    move v9, v8

    goto :goto_0

    .line 980
    .end local v5    # "path":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 963
    :cond_4
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 982
    :catch_1
    move-exception v4

    .line 983
    .local v4, "e":Landroid/database/StaleDataException;
    goto :goto_2
.end method

.method public getBookmark(II)Lcom/sec/android/app/voicenote/common/util/Bookmark;
    .locals 3
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 787
    const/4 v0, 0x0

    .line 788
    .local v0, "bookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getFileName(I)Ljava/lang/String;

    move-result-object v1

    .line 789
    .local v1, "path":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    invoke-virtual {v2, v1, p2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->getBookmark(Ljava/lang/String;I)Lcom/sec/android/app/voicenote/common/util/Bookmark;

    move-result-object v0

    .line 790
    return-object v0
.end method

.method public getCheckBoxSize()I
    .locals 1

    .prologue
    .line 1221
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getChild(II)Landroid/database/Cursor;
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 212
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic getChild(II)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # I

    .prologue
    .line 60
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getChild(II)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 218
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 226
    if-nez p4, :cond_1

    .line 227
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v6, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mChildLayout:I

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 231
    .local v3, "v":Landroid/view/View;
    :goto_0
    const v5, 0x7f0e0064

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 232
    .local v1, "childText":Landroid/widget/TextView;
    const/4 v0, 0x0

    .line 233
    .local v0, "bookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getBookmark(II)Lcom/sec/android/app/voicenote/common/util/Bookmark;

    move-result-object v0

    .line 234
    iget-boolean v5, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchMode:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->childSttSearchResults:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->childSttSearchResults:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, p2, :cond_2

    .line 236
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->childSttSearchResults:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    invoke-virtual {v5, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;

    .line 238
    .local v4, "viewedResult":Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 239
    .local v2, "spannable":Landroid/text/Spannable;
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080029

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;->getHighlightStartIndex()I

    move-result v6

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;->getHighlightStartIndex()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    const/4 v8, 0x0

    invoke-interface {v2, v5, v6, v7, v8}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 244
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    const-string v5, "#000000"

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 246
    const-string v5, "#E6E6E6"

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 252
    .end local v2    # "spannable":Landroid/text/Spannable;
    .end local v4    # "viewedResult":Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;
    :cond_0
    :goto_1
    return-object v3

    .line 229
    .end local v0    # "bookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    .end local v1    # "childText":Landroid/widget/TextView;
    .end local v3    # "v":Landroid/view/View;
    :cond_1
    move-object v3, p4

    .restart local v3    # "v":Landroid/view/View;
    goto/16 :goto_0

    .line 247
    .restart local v0    # "bookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    .restart local v1    # "childText":Landroid/widget/TextView;
    :cond_2
    if-eqz v0, :cond_0

    .line 248
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public getChildrenCount(I)I
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchMode:Z

    if-eqz v0, :cond_0

    .line 271
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected getChildrenCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1
    .param p1, "arg0"    # Landroid/database/Cursor;

    .prologue
    .line 1073
    const/4 v0, 0x0

    return-object v0
.end method

.method public getElapsed(II)J
    .locals 4
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 868
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchMode:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->childSttSearchResults:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 869
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->childSttSearchResults:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, p2, :cond_1

    .line 870
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->childSttSearchResults:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;->getElapsedTime()J

    move-result-wide v2

    .line 879
    :goto_0
    return-wide v2

    .line 872
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchMode:Z

    if-nez v1, :cond_1

    .line 873
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getBookmark(II)Lcom/sec/android/app/voicenote/common/util/Bookmark;

    move-result-object v0

    .line 874
    .local v0, "bookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    if-eqz v0, :cond_1

    .line 875
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v1

    int-to-long v2, v1

    goto :goto_0

    .line 879
    .end local v0    # "bookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getFileId(I)J
    .locals 4
    .param p1, "groupPosition"    # I

    .prologue
    .line 897
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroup(I)Landroid/database/Cursor;

    move-result-object v0

    .line 898
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 899
    :cond_0
    const-string v1, "VNExpandableCursorAdapter"

    const-string v2, "getFileId : cursor is null or closed"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    const-wide/16 v2, -0x1

    .line 902
    :goto_0
    return-wide v2

    :cond_1
    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    goto :goto_0
.end method

.method public getFileName(I)Ljava/lang/String;
    .locals 3
    .param p1, "groupPosition"    # I

    .prologue
    .line 906
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroup(I)Landroid/database/Cursor;

    move-result-object v0

    .line 907
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 908
    :cond_0
    const-string v1, "VNExpandableCursorAdapter"

    const-string v2, "getFileName : cursor is null or closed"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 909
    const-string v1, ""

    .line 911
    :goto_0
    return-object v1

    :cond_1
    const-string v1, "_data"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getFileTitle(I)Ljava/lang/String;
    .locals 5
    .param p1, "groupPosition"    # I

    .prologue
    .line 915
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroup(I)Landroid/database/Cursor;

    move-result-object v0

    .line 916
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 917
    :cond_0
    const-string v2, "VNExpandableCursorAdapter"

    const-string v3, "getFileTitle : cursor is null or closed"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    const-string v1, ""

    .line 922
    :goto_0
    return-object v1

    .line 920
    :cond_1
    const-string v2, "title"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 921
    .local v1, "result":Ljava/lang/String;
    const-string v2, "VNExpandableCursorAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFileTitle() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 47
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 277
    const-string v43, "VNExpandableCursorAdapter"

    const-string v44, "getGroupView()"

    invoke-static/range {v43 .. v44}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    if-eqz p3, :cond_0

    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v43

    if-nez v43, :cond_5

    .line 281
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mGroupViewTemplate:Ljava/util/ArrayList;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Ljava/util/ArrayList;->size()I

    move-result v43

    if-lez v43, :cond_4

    .line 282
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mGroupViewTemplate:Ljava/util/ArrayList;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-virtual/range {v43 .. v44}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object p3

    .end local p3    # "convertView":Landroid/view/View;
    check-cast p3, Landroid/view/View;

    .line 283
    .restart local p3    # "convertView":Landroid/view/View;
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;

    .line 309
    .local v21, "holder":Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;
    :goto_0
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->mmsText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    const/16 v44, 0x1

    invoke-virtual/range {v43 .. v44}, Landroid/widget/TextView;->setSelected(Z)V

    .line 310
    const/16 v43, 0x7

    move/from16 v0, p1

    move/from16 v1, v43

    if-ge v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mHasfadeinAnimation:Z

    move/from16 v43, v0

    if-eqz v43, :cond_7

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v43, v0

    const/high16 v44, 0x10a0000

    invoke-static/range {v43 .. v44}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v8

    .line 312
    .local v8, "anim":Landroid/view/animation/Animation;
    mul-int/lit8 v43, p1, 0x32

    move/from16 v0, v43

    int-to-long v0, v0

    move-wide/from16 v44, v0

    move-wide/from16 v0, v44

    invoke-virtual {v8, v0, v1}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 313
    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 315
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroupCount()I

    move-result v43

    const/16 v44, 0x7

    move/from16 v0, v43

    move/from16 v1, v44

    if-ge v0, v1, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroupCount()I

    move-result v43

    add-int/lit8 v43, v43, -0x1

    move/from16 v0, p1

    move/from16 v1, v43

    if-ne v0, v1, :cond_6

    .line 316
    const/16 v43, 0x0

    move/from16 v0, v43

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mHasfadeinAnimation:Z

    .line 323
    .end local v8    # "anim":Landroid/view/animation/Animation;
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mIsSelectionMode:Z

    move/from16 v43, v0

    if-eqz v43, :cond_2

    .line 324
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->checkbox:Landroid/widget/CheckBox;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-virtual/range {v43 .. v44}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 326
    :cond_2
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->checkbox:Landroid/widget/CheckBox;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v43

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->controlCheckbox(Landroid/widget/CheckBox;I)V

    .line 328
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroup(I)Landroid/database/Cursor;

    move-result-object v10

    .line 329
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_3

    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v43

    if-eqz v43, :cond_8

    .line 330
    :cond_3
    const-string v43, "VNExpandableCursorAdapter"

    const-string v44, "getGroupView : cursor is null or closed"

    invoke-static/range {v43 .. v44}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    :goto_2
    return-object p3

    .line 285
    .end local v10    # "cursor":Landroid/database/Cursor;
    .end local v21    # "holder":Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;
    :cond_4
    new-instance v21, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;

    invoke-direct/range {v21 .. v21}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;-><init>()V

    .line 286
    .restart local v21    # "holder":Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mGroupLayout:I

    move/from16 v44, v0

    const/16 v45, 0x0

    invoke-virtual/range {v43 .. v45}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    .line 288
    const v43, 0x7f0e0065

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/LinearLayout;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->listrowLayout:Landroid/widget/LinearLayout;

    .line 289
    const v43, 0x7f0e0067

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/TextView;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    .line 290
    const v43, 0x7f0e0068

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/TextView;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->dateText:Landroid/widget/TextView;

    .line 291
    const v43, 0x7f0e0069

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/TextView;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->mmsText:Landroid/widget/TextView;

    .line 292
    const v43, 0x7f0e006a

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/ImageView;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    .line 294
    const v43, 0x7f0e001f

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/LinearLayout;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->checkbox_wrapper:Landroid/widget/LinearLayout;

    .line 295
    const v43, 0x7f0e006c

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/ImageView;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->bookmarkicon:Landroid/widget/ImageView;

    .line 296
    const v43, 0x7f0e006e

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/ImageView;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->stticon:Landroid/widget/ImageView;

    .line 297
    const v43, 0x7f0e006f

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/ImageView;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->playicon:Landroid/widget/ImageView;

    .line 298
    const v43, 0x7f0e0070

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/ImageView;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->pauseicon:Landroid/widget/ImageView;

    .line 299
    const v43, 0x7f0e006b

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/ImageView;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->secreticon:Landroid/widget/ImageView;

    .line 300
    const v43, 0x7f0e0071

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/ImageView;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->expandicon:Landroid/widget/ImageView;

    .line 301
    const v43, 0x7f0e0066

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/LinearLayout;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->textLayout:Landroid/widget/LinearLayout;

    .line 302
    const v43, 0x7f0e006d

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/ImageView;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->nfcicon:Landroid/widget/ImageView;

    .line 303
    const v43, 0x7f0e0020

    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v43

    check-cast v43, Landroid/widget/CheckBox;

    move-object/from16 v0, v43

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->checkbox:Landroid/widget/CheckBox;

    .line 304
    move-object/from16 v0, p3

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 307
    .end local v21    # "holder":Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;
    :cond_5
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;

    .restart local v21    # "holder":Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;
    goto/16 :goto_0

    .line 317
    .restart local v8    # "anim":Landroid/view/animation/Animation;
    :cond_6
    const/16 v43, 0x6

    move/from16 v0, p1

    move/from16 v1, v43

    if-ne v0, v1, :cond_1

    .line 318
    const/16 v43, 0x0

    move/from16 v0, v43

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mHasfadeinAnimation:Z

    goto/16 :goto_1

    .line 321
    .end local v8    # "anim":Landroid/view/animation/Animation;
    :cond_7
    const/16 v43, 0x0

    move/from16 v0, v43

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mHasfadeinAnimation:Z

    goto/16 :goto_1

    .line 334
    .restart local v10    # "cursor":Landroid/database/Cursor;
    :cond_8
    const-wide/16 v16, 0x0

    .line 338
    .local v16, "duration":J
    const-wide/16 v12, 0x0

    .line 339
    .local v12, "date":J
    const-wide/16 v24, -0x1

    .line 340
    .local v24, "id":J
    const/16 v30, -0x1

    .line 341
    .local v30, "labelindex":I
    const/16 v28, 0x0

    .line 342
    .local v28, "is_secretbox":I
    const/16 v27, 0x0

    .line 343
    .local v27, "is_memo":I
    const/16 v33, 0x0

    .line 344
    .local v33, "mimetype":Ljava/lang/String;
    const/16 v40, 0x0

    .line 347
    .local v40, "tagsData":Ljava/lang/String;
    :try_start_0
    const-string v43, "duration"

    move-object/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v43

    move/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 348
    const-string v43, "title"

    move-object/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v43

    move/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v41

    .line 349
    .local v41, "title":Ljava/lang/String;
    const-string v43, "date_modified"

    move-object/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v43

    move/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 350
    const-string v43, "_id"

    move-object/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v43

    move/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 351
    const-string v43, "label_id"

    move-object/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v43

    move/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v30

    .line 352
    const-string v43, "is_secretbox"

    move-object/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v43

    move/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    .line 353
    const-string v43, "mime_type"

    move-object/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v43

    move/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    .line 354
    const-string v43, "_data"

    move-object/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v43

    move/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v35

    .line 355
    .local v35, "path":Ljava/lang/String;
    const-string v43, "is_memo"

    move-object/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v43

    move/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    .line 356
    const-string v43, "year_name"

    move-object/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v43

    move/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v40

    .line 368
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v43, v0

    if-nez v43, :cond_f

    .line 369
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v44

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string v45, "  "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    invoke-virtual/range {v43 .. v44}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 398
    :goto_3
    const-string v43, "%s  |  %s"

    const/16 v44, 0x2

    move/from16 v0, v44

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v44, v0

    const/16 v45, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getDateFormatByFormatSetting(J)Ljava/lang/String;

    move-result-object v46

    aput-object v46, v44, v45

    const/16 v45, 0x1

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->stringForTime(J)Ljava/lang/String;

    move-result-object v46

    aput-object v46, v44, v45

    invoke-static/range {v43 .. v44}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 400
    .local v11, "date_duration":Ljava/lang/String;
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->dateText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 402
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-static {v0, v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->stringForReadDate(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 403
    .local v6, "DescDate":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->stringForTime(J)Ljava/lang/String;

    move-result-object v44

    invoke-static/range {v43 .. v44}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->stringForReadTime(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 405
    .local v7, "DescDuration":Ljava/lang/String;
    const-string v43, "%s, %s"

    const/16 v44, 0x2

    move/from16 v0, v44

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v44, v0

    const/16 v45, 0x0

    aput-object v6, v44, v45

    const/16 v45, 0x1

    aput-object v7, v44, v45

    invoke-static/range {v43 .. v44}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 406
    .local v14, "date_duration_desc":Ljava/lang/String;
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->dateText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 408
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->textLayout:Landroid/widget/LinearLayout;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v32

    check-cast v32, Landroid/widget/RelativeLayout$LayoutParams;

    .line 409
    .local v32, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mIsSelectionMode:Z

    move/from16 v43, v0

    if-eqz v43, :cond_12

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v43, v0

    const/high16 v44, 0x41400000    # 12.0f

    const/16 v45, 0x0

    invoke-static/range {v43 .. v45}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->convertDPtoPX(Landroid/content/Context;FLandroid/util/DisplayMetrics;)I

    move-result v43

    const/16 v44, 0x0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/RelativeLayout$LayoutParams;->getMarginEnd()I

    move-result v45

    const/16 v46, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v43

    move/from16 v2, v44

    move/from16 v3, v45

    move/from16 v4, v46

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginsRelative(IIII)V

    .line 415
    :goto_4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v43

    const-string v44, "ar_AE"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v43

    if-eqz v43, :cond_9

    .line 416
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->dateText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Landroid/widget/LinearLayout$LayoutParams;

    .line 417
    .local v15, "dlp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v15}, Landroid/widget/LinearLayout$LayoutParams;->getMarginStart()I

    move-result v43

    const/16 v44, 0x0

    invoke-virtual {v15}, Landroid/widget/LinearLayout$LayoutParams;->getMarginEnd()I

    move-result v45

    const/16 v46, 0x0

    move/from16 v0, v43

    move/from16 v1, v44

    move/from16 v2, v45

    move/from16 v3, v46

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMarginsRelative(IIII)V

    .line 420
    .end local v15    # "dlp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_9
    :try_start_1
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->textLayout:Landroid/widget/LinearLayout;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_3

    .line 428
    :goto_5
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->textLayout:Landroid/widget/LinearLayout;

    move-object/from16 v43, v0

    const/16 v44, 0x2

    invoke-virtual/range {v43 .. v44}, Landroid/widget/LinearLayout;->setHoverPopupType(I)V

    .line 429
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->textLayout:Landroid/widget/LinearLayout;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/widget/LinearLayout;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v22

    .line 430
    .local v22, "hoverpopup":Landroid/widget/HoverPopupWindow;
    if-eqz v22, :cond_a

    .line 431
    const/16 v43, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    .line 432
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 435
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelColor(I)Ljava/lang/Integer;

    move-result-object v29

    .line 436
    .local v29, "labelcolor":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelTitle(I)Ljava/lang/String;

    move-result-object v31

    .line 438
    .local v31, "labeltitle":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v43, v0

    invoke-static/range {v43 .. v43}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v43

    if-eqz v43, :cond_13

    .line 439
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    const/16 v44, 0x1

    const/high16 v45, 0x41c00000    # 24.0f

    invoke-virtual/range {v43 .. v45}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 440
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->dateText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    const/16 v44, 0x1

    const/high16 v45, 0x41980000    # 19.0f

    invoke-virtual/range {v43 .. v45}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 441
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 442
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v42

    check-cast v42, Landroid/view/View;

    .line 443
    .local v42, "view":Landroid/view/View;
    if-eqz v42, :cond_b

    .line 444
    const-string v43, ""

    invoke-virtual/range {v42 .. v43}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 493
    .end local v42    # "view":Landroid/view/View;
    :cond_b
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    move-object/from16 v43, v0

    if-eqz v43, :cond_c

    .line 494
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->playicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v19

    check-cast v19, Landroid/graphics/drawable/AnimationDrawable;

    .line 495
    .local v19, "frameAnimation":Landroid/graphics/drawable/AnimationDrawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    move-object/from16 v43, v0

    invoke-interface/range {v43 .. v43}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;->getCurrentPlayID()J

    move-result-wide v44

    cmp-long v43, v44, v24

    if-nez v43, :cond_1a

    .line 496
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    move-object/from16 v43, v0

    invoke-interface/range {v43 .. v43}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;->getCurrentPlayState()I

    move-result v43

    const/16 v44, 0x17

    move/from16 v0, v43

    move/from16 v1, v44

    if-ne v0, v1, :cond_17

    .line 497
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 498
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->playicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 499
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->pauseicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 500
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v44

    const v45, 0x7f080029

    invoke-virtual/range {v44 .. v45}, Landroid/content/res/Resources;->getColor(I)I

    move-result v44

    invoke-virtual/range {v43 .. v44}, Landroid/widget/TextView;->setTextColor(I)V

    .line 502
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    const/16 v44, 0x1

    invoke-virtual/range {v43 .. v44}, Landroid/widget/TextView;->setSelected(Z)V

    .line 527
    .end local v19    # "frameAnimation":Landroid/graphics/drawable/AnimationDrawable;
    :cond_c
    :goto_7
    const/16 v43, 0x1

    move/from16 v0, v28

    move/from16 v1, v43

    if-ne v0, v1, :cond_1b

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->secreticon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    if-eqz v43, :cond_1b

    .line 528
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->secreticon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 533
    :cond_d
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v43, v0

    invoke-static/range {v43 .. v43}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v43

    if-eqz v43, :cond_1c

    .line 534
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->mmsText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/TextView;->setVisibility(I)V

    .line 535
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->bookmarkicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 536
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->expandicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 537
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 538
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->nfcicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 607
    :cond_e
    :goto_9
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setPrivateItemDescription(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;)V

    goto/16 :goto_2

    .line 357
    .end local v6    # "DescDate":Ljava/lang/String;
    .end local v7    # "DescDuration":Ljava/lang/String;
    .end local v11    # "date_duration":Ljava/lang/String;
    .end local v14    # "date_duration_desc":Ljava/lang/String;
    .end local v22    # "hoverpopup":Landroid/widget/HoverPopupWindow;
    .end local v29    # "labelcolor":Ljava/lang/Integer;
    .end local v31    # "labeltitle":Ljava/lang/String;
    .end local v32    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v35    # "path":Ljava/lang/String;
    .end local v41    # "title":Ljava/lang/String;
    :catch_0
    move-exception v18

    .line 358
    .local v18, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual/range {v18 .. v18}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto/16 :goto_2

    .line 360
    .end local v18    # "e":Landroid/database/CursorIndexOutOfBoundsException;
    :catch_1
    move-exception v18

    .line 361
    .local v18, "e":Ljava/lang/IllegalStateException;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_2

    .line 363
    .end local v18    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v18

    .line 364
    .local v18, "e":Landroid/database/StaleDataException;
    invoke-virtual/range {v18 .. v18}, Landroid/database/StaleDataException;->printStackTrace()V

    goto/16 :goto_2

    .line 372
    .end local v18    # "e":Landroid/database/StaleDataException;
    .restart local v35    # "path":Ljava/lang/String;
    .restart local v41    # "title":Ljava/lang/String;
    :cond_f
    const/16 v26, -0x1

    .line 373
    .local v26, "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Ljava/lang/String;->length()I

    move-result v20

    .line 375
    .local v20, "highlightStrLength":I
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v43

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Ljava/lang/String;->toCharArray()[C

    move-result-object v44

    move-object/from16 v0, v43

    move-object/from16 v1, v41

    move-object/from16 v2, v44

    invoke-static {v0, v1, v2}, Landroid/text/TextUtils;->getPrefixCharForIndian(Landroid/text/TextPaint;Ljava/lang/CharSequence;[C)[C

    move-result-object v23

    .line 377
    .local v23, "iQueryForIndian":[C
    if-eqz v23, :cond_10

    .line 378
    new-instance v37, Ljava/lang/String;

    move-object/from16 v0, v37

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    .line 379
    .local v37, "s":Ljava/lang/String;
    invoke-virtual/range {v41 .. v41}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v44

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v26

    .line 380
    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->length()I

    move-result v20

    .line 385
    .end local v37    # "s":Ljava/lang/String;
    :goto_a
    if-ltz v26, :cond_11

    add-int v43, v26, v20

    invoke-virtual/range {v41 .. v41}, Ljava/lang/String;->length()I

    move-result v44

    move/from16 v0, v43

    move/from16 v1, v44

    if-gt v0, v1, :cond_11

    .line 386
    new-instance v38, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v38

    move-object/from16 v1, v41

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 387
    .local v38, "spannable":Landroid/text/Spannable;
    new-instance v43, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v44

    const v45, 0x7f080029

    invoke-virtual/range {v44 .. v45}, Landroid/content/res/Resources;->getColor(I)I

    move-result v44

    invoke-direct/range {v43 .. v44}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int v44, v26, v20

    const/16 v45, 0x0

    move-object/from16 v0, v38

    move-object/from16 v1, v43

    move/from16 v2, v26

    move/from16 v3, v44

    move/from16 v4, v45

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 391
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 382
    .end local v38    # "spannable":Landroid/text/Spannable;
    :cond_10
    invoke-virtual/range {v41 .. v41}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v44

    invoke-virtual/range {v43 .. v44}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v26

    goto :goto_a

    .line 393
    :cond_11
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v44

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string v45, "  "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    invoke-virtual/range {v43 .. v44}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 412
    .end local v20    # "highlightStrLength":I
    .end local v23    # "iQueryForIndian":[C
    .end local v26    # "index":I
    .restart local v6    # "DescDate":Ljava/lang/String;
    .restart local v7    # "DescDuration":Ljava/lang/String;
    .restart local v11    # "date_duration":Ljava/lang/String;
    .restart local v14    # "date_duration_desc":Ljava/lang/String;
    .restart local v32    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v43, v0

    const/high16 v44, 0x41c00000    # 24.0f

    const/16 v45, 0x0

    invoke-static/range {v43 .. v45}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->convertDPtoPX(Landroid/content/Context;FLandroid/util/DisplayMetrics;)I

    move-result v43

    const/16 v44, 0x0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/RelativeLayout$LayoutParams;->getMarginEnd()I

    move-result v45

    const/16 v46, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v43

    move/from16 v2, v44

    move/from16 v3, v45

    move/from16 v4, v46

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginsRelative(IIII)V

    goto/16 :goto_4

    .line 421
    :catch_3
    move-exception v18

    .line 422
    .local v18, "e":Ljava/lang/ClassCastException;
    const-string v43, "VNExpandableCursorAdapter"

    const-string v44, " ClassCastException is hanlded"

    invoke-static/range {v43 .. v44}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    invoke-virtual/range {v18 .. v18}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto/16 :goto_5

    .line 448
    .end local v18    # "e":Ljava/lang/ClassCastException;
    .restart local v22    # "hoverpopup":Landroid/widget/HoverPopupWindow;
    .restart local v29    # "labelcolor":Ljava/lang/Integer;
    .restart local v31    # "labeltitle":Ljava/lang/String;
    :cond_13
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    if-eqz v43, :cond_b

    .line 449
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v34

    check-cast v34, Landroid/view/View;

    .line 450
    .local v34, "parentLabel":Landroid/view/View;
    if-eqz v29, :cond_15

    .line 451
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Integer;->intValue()I

    move-result v44

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 452
    if-eqz v34, :cond_14

    .line 454
    const/16 v43, 0x1

    move-object/from16 v0, v34

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->setHovered(Z)V

    .line 455
    move-object/from16 v0, v34

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 456
    const/16 v43, 0x1

    move-object/from16 v0, v34

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 457
    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setLableHoverPopup(Landroid/view/View;)V

    .line 477
    :goto_b
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v44

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 478
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    .line 459
    :cond_14
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 460
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x1

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 461
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setLableHoverPopup(Landroid/view/View;)V

    goto :goto_b

    .line 464
    :cond_15
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v44

    const v45, 0x7f08001b

    invoke-virtual/range {v44 .. v45}, Landroid/content/res/Resources;->getColor(I)I

    move-result v44

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 466
    if-eqz v34, :cond_16

    .line 467
    const-string v43, ""

    move-object/from16 v0, v34

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 468
    const/16 v43, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 469
    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setLableHoverPopup(Landroid/view/View;)V

    goto :goto_b

    .line 471
    :cond_16
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const-string v44, ""

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 472
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 473
    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setLableHoverPopup(Landroid/view/View;)V

    goto/16 :goto_b

    .line 503
    .end local v34    # "parentLabel":Landroid/view/View;
    .restart local v19    # "frameAnimation":Landroid/graphics/drawable/AnimationDrawable;
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    move-object/from16 v43, v0

    invoke-interface/range {v43 .. v43}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;->getCurrentPlayState()I

    move-result v43

    const/16 v44, 0x18

    move/from16 v0, v43

    move/from16 v1, v44

    if-eq v0, v1, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    move-object/from16 v43, v0

    invoke-interface/range {v43 .. v43}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;->getCurrentPlayState()I

    move-result v43

    const/16 v44, 0x16

    move/from16 v0, v43

    move/from16 v1, v44

    if-ne v0, v1, :cond_19

    .line 505
    :cond_18
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 506
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->playicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 507
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->pauseicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 508
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v44

    const v45, 0x7f080029

    invoke-virtual/range {v44 .. v45}, Landroid/content/res/Resources;->getColor(I)I

    move-result v44

    invoke-virtual/range {v43 .. v44}, Landroid/widget/TextView;->setTextColor(I)V

    .line 510
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    const/16 v44, 0x1

    invoke-virtual/range {v43 .. v44}, Landroid/widget/TextView;->setSelected(Z)V

    goto/16 :goto_7

    .line 512
    :cond_19
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 513
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->playicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 514
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->pauseicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 515
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v44

    const v45, 0x7f08002a

    invoke-virtual/range {v44 .. v45}, Landroid/content/res/Resources;->getColor(I)I

    move-result v44

    invoke-virtual/range {v43 .. v44}, Landroid/widget/TextView;->setTextColor(I)V

    .line 516
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-virtual/range {v43 .. v44}, Landroid/widget/TextView;->setSelected(Z)V

    goto/16 :goto_7

    .line 519
    :cond_1a
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 520
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->playicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 521
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->pauseicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 522
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v44

    const v45, 0x7f08002a

    invoke-virtual/range {v44 .. v45}, Landroid/content/res/Resources;->getColor(I)I

    move-result v44

    invoke-virtual/range {v43 .. v44}, Landroid/widget/TextView;->setTextColor(I)V

    .line 523
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-virtual/range {v43 .. v44}, Landroid/widget/TextView;->setSelected(Z)V

    goto/16 :goto_7

    .line 529
    .end local v19    # "frameAnimation":Landroid/graphics/drawable/AnimationDrawable;
    :cond_1b
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->secreticon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    if-eqz v43, :cond_d

    .line 530
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->secreticon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_8

    .line 540
    :cond_1c
    if-eqz v33, :cond_20

    const-string v43, "audio/amr"

    move-object/from16 v0, v33

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v43

    if-eqz v43, :cond_20

    .line 541
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->mmsText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-virtual/range {v43 .. v44}, Landroid/widget/TextView;->setVisibility(I)V

    .line 545
    :goto_c
    if-eqz v35, :cond_24

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isM4A(Ljava/lang/String;)Z

    move-result v43

    if-eqz v43, :cond_24

    .line 546
    sget-object v43, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    const/16 v44, 0x1

    move-object/from16 v0, v43

    move-object/from16 v1, v35

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->getBookmarksForFile(Ljava/lang/String;Z)Ljava/util/ArrayList;

    move-result-object v9

    .line 547
    .local v9, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    if-eqz v9, :cond_21

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v43

    if-lez v43, :cond_21

    .line 548
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->bookmarkicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 556
    :goto_d
    if-eqz v9, :cond_1d

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v43

    if-lez v43, :cond_1d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchMode:Z

    move/from16 v43, v0

    if-nez v43, :cond_1d

    :cond_1d
    const/16 v43, 0x2

    move/from16 v0, v27

    move/from16 v1, v43

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchMode:Z

    move/from16 v43, v0

    if-eqz v43, :cond_1e

    .line 568
    :cond_1e
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->expandicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 571
    sget-object v43, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    const/16 v44, 0x1

    move-object/from16 v0, v43

    move-object/from16 v1, v35

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->getSTTForFile(Ljava/lang/String;Z)Ljava/util/ArrayList;

    move-result-object v39

    .line 572
    .local v39, "sttList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    if-eqz v39, :cond_23

    invoke-virtual/range {v39 .. v39}, Ljava/util/ArrayList;->size()I

    move-result v43

    if-lez v43, :cond_23

    .line 573
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 574
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const-string v44, ""

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 575
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x3

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 576
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v36

    .line 577
    .local v36, "popup":Landroid/widget/HoverPopupWindow;
    if-eqz v36, :cond_1f

    .line 578
    const/16 v43, 0x1

    move-object/from16 v0, v36

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 579
    const/16 v43, 0x0

    move-object/from16 v0, v36

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 580
    new-instance v43, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$STTHoverPopupListener;

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    move-object/from16 v2, v39

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$STTHoverPopupListener;-><init>(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;Ljava/util/ArrayList;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 581
    const/16 v43, 0x3133

    move-object/from16 v0, v36

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 585
    :cond_1f
    const/16 v43, 0x2

    move/from16 v0, v27

    move/from16 v1, v43

    if-ne v0, v1, :cond_22

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchMode:Z

    move/from16 v43, v0

    if-eqz v43, :cond_22

    .line 586
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const v44, 0x7f0201a5

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 598
    .end local v9    # "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    .end local v36    # "popup":Landroid/widget/HoverPopupWindow;
    .end local v39    # "sttList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    :goto_e
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->nfcicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    if-eqz v43, :cond_e

    .line 599
    if-eqz v40, :cond_25

    const-string v43, "NFC"

    move-object/from16 v0, v40

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v43

    if-eqz v43, :cond_25

    .line 600
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->nfcicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_9

    .line 543
    :cond_20
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->mmsText:Landroid/widget/TextView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_c

    .line 550
    .restart local v9    # "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    :cond_21
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->bookmarkicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_d

    .line 588
    .restart local v36    # "popup":Landroid/widget/HoverPopupWindow;
    .restart local v39    # "sttList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    :cond_22
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const v44, 0x7f0201a3

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_e

    .line 591
    .end local v36    # "popup":Landroid/widget/HoverPopupWindow;
    :cond_23
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_e

    .line 594
    .end local v9    # "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    .end local v39    # "sttList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    :cond_24
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->bookmarkicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 595
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 596
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->expandicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_e

    .line 602
    :cond_25
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$GroupViewHolder;->nfcicon:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    const/16 v44, 0x8

    invoke-virtual/range {v43 .. v44}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_9
.end method

.method public getItemFileName(I)Ljava/lang/String;
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 1042
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getFileName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItemsCount()I
    .locals 1

    .prologue
    .line 1047
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroupCount()I

    move-result v0

    return v0
.end method

.method public getLoader()Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;
    .locals 1

    .prologue
    .line 1051
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    return-object v0
.end method

.method protected getSttText(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 857
    .local p1, "sttData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 858
    .local v2, "ssb":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 859
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 860
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget v3, v3, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    if-nez v3, :cond_0

    .line 861
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v3, v3, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 859
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 864
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 740
    const/4 v0, 0x1

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 745
    const/4 v0, 0x1

    return v0
.end method

.method protected newChildView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/database/Cursor;
    .param p3, "arg2"    # Z
    .param p4, "arg3"    # Landroid/view/ViewGroup;

    .prologue
    .line 1079
    const/4 v0, 0x0

    return-object v0
.end method

.method protected newGroupView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/database/Cursor;
    .param p3, "arg2"    # Z
    .param p4, "arg3"    # Landroid/view/ViewGroup;

    .prologue
    .line 1085
    const/4 v0, 0x0

    return-object v0
.end method

.method public notifyDataSetChanged()V
    .locals 3

    .prologue
    .line 750
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 751
    .local v0, "cursor":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkMediaScannerRunning(Landroid/content/ContentResolver;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setmAutoRequery(Z)V

    .line 752
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 763
    :cond_0
    :goto_1
    return-void

    .line 751
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 755
    :cond_2
    const-string v1, "VNExpandableCursorAdapter"

    const-string v2, "NotifyDataSetChanged()"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    if-eqz v1, :cond_3

    .line 757
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->LoadLabeltoMap()V

    .line 759
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->notifyDataSetChanged()V

    .line 760
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    if-eqz v1, :cond_0

    .line 761
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;->refreshOptionsMenu()V

    goto :goto_1
.end method

.method public notifyOnCacheLoaded()V
    .locals 2

    .prologue
    .line 1020
    const-string v0, "VNExpandableCursorAdapter"

    const-string v1, "notifyOnCacheLoaded()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1024
    invoke-super {p0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->notifyDataSetChanged()V

    .line 1025
    return-void
.end method

.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "hpw"    # Landroid/widget/HoverPopupWindow;

    .prologue
    .line 710
    const-string v6, "VNExpandableCursorAdapter"

    const-string v7, "makeDefaultContentView"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    const/4 v5, 0x0

    .line 712
    .local v5, "v":Landroid/widget/TextView;
    const/4 v2, 0x7

    .line 714
    .local v2, "maxLines":I
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 715
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f03001f

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .end local v5    # "v":Landroid/widget/TextView;
    check-cast v5, Landroid/widget/TextView;

    .line 716
    .restart local v5    # "v":Landroid/widget/TextView;
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setHoverPopupType(I)V

    .line 718
    const v6, 0x7f0e0067

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 720
    .local v4, "titleText":Landroid/widget/TextView;
    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    .line 721
    .local v3, "text":Ljava/lang/CharSequence;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 722
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 723
    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 725
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 726
    .local v0, "d":Landroid/util/DisplayMetrics;
    iget v6, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    iget v7, v0, Landroid/util/DisplayMetrics;->density:F

    cmpl-float v6, v6, v7

    if-lez v6, :cond_0

    .line 727
    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 733
    .end local v0    # "d":Landroid/util/DisplayMetrics;
    :cond_0
    :goto_0
    invoke-virtual {p2, v5}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 735
    const/4 v6, 0x1

    return v6

    .line 730
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public refreashAnim(Z)V
    .locals 0
    .param p1, "needAnim"    # Z

    .prologue
    .line 1217
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mHasfadeinAnimation:Z

    .line 1218
    return-void
.end method

.method protected searchStt(I)V
    .locals 22
    .param p1, "groupPosition"    # I

    .prologue
    .line 794
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchMode:Z

    move/from16 v18, v0

    if-nez v18, :cond_1

    .line 854
    :cond_0
    :goto_0
    return-void

    .line 796
    :cond_1
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getFileName(I)Ljava/lang/String;

    move-result-object v10

    .line 797
    .local v10, "path":Ljava/lang/String;
    new-instance v5, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    invoke-direct {v5, v10}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 798
    .local v5, "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v8

    .line 799
    .local v8, "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    if-nez v8, :cond_2

    .line 800
    const-string v18, "VNExpandableCursorAdapter"

    const-string v19, "searchStt : inf is null"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 803
    :cond_2
    sget-object v18, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v10, v1, v8}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->getSTTForFile(Ljava/lang/String;ZLcom/sec/android/app/voicenote/common/util/M4aInfo;)Ljava/util/ArrayList;

    move-result-object v12

    .line 804
    .local v12, "sttList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v18, v0

    sget-object v19, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    .line 805
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->childSttSearchResults:Ljava/util/HashMap;

    move-object/from16 v18, v0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 806
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->childSttSearchResults:Ljava/util/HashMap;

    move-object/from16 v18, v0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 807
    :cond_3
    iget-boolean v0, v8, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z

    move/from16 v18, v0

    if-nez v18, :cond_4

    if-eqz v12, :cond_0

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-lez v18, :cond_0

    .line 808
    :cond_4
    if-eqz v12, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    if-lez v18, :cond_0

    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_0

    .line 809
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getSttText(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v13

    .line 810
    .local v13, "sttText":Ljava/lang/String;
    sget-object v18, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v15

    .line 811
    .local v15, "tempSttText":Ljava/lang/String;
    const/4 v11, 0x0

    .line 812
    .local v11, "searchShift":I
    const/4 v7, -0x1

    .line 813
    .local v7, "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    rsub-int/lit8 v18, v18, 0x22

    div-int/lit8 v4, v18, 0x2

    .line 814
    .local v4, "charsMargin":I
    const/4 v6, 0x0

    .line 815
    .local v6, "highlightStartIndex":I
    const/4 v14, 0x0

    .line 816
    .local v14, "sttTextTrunc":Ljava/lang/String;
    const/16 v16, 0x0

    .line 817
    .local v16, "tempSttTextTrunc":Ljava/lang/String;
    const/16 v17, 0x0

    .line 818
    .local v17, "textToDisplay":Ljava/lang/String;
    const/4 v9, 0x0

    .line 819
    .local v9, "newArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;>;"
    :goto_1
    invoke-virtual {v15, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 820
    invoke-virtual {v13, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 821
    sget-object v18, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v16

    .line 822
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 823
    const/4 v6, 0x0

    .line 824
    const/16 v17, 0x0

    .line 825
    add-int v18, v7, v11

    move/from16 v0, v18

    if-ge v0, v4, :cond_6

    add-int v18, v7, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v18, v18, v19

    add-int v18, v18, v4

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_6

    .line 826
    move-object/from16 v17, v14

    .line 827
    move v6, v7

    .line 839
    :goto_2
    sget-object v18, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 840
    const/16 v18, -0x1

    move/from16 v0, v18

    if-ne v6, v0, :cond_5

    .line 841
    const-string v18, "VNExpandableCursorAdapter"

    const-string v19, "search Stt: wrong index value!"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->childSttSearchResults:Ljava/util/HashMap;

    move-object/from16 v18, v0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_9

    .line 844
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "newArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;>;"
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 845
    .restart local v9    # "newArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;>;"
    new-instance v18, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;

    add-int v19, v7, v11

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v12}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->findElapsedTime(ILjava/util/ArrayList;)J

    move-result-wide v20

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    move-wide/from16 v2, v20

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;-><init>(Ljava/lang/String;IJ)V

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 846
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->childSttSearchResults:Ljava/util/HashMap;

    move-object/from16 v18, v0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 850
    :goto_3
    add-int v18, v7, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v11, v18, v19

    goto/16 :goto_1

    .line 828
    :cond_6
    add-int v18, v7, v11

    move/from16 v0, v18

    if-ge v0, v4, :cond_7

    .line 829
    const/16 v18, 0x0

    add-int v19, v7, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    add-int v19, v19, v20

    add-int v19, v19, v4

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    .line 830
    const/16 v18, 0x0

    add-int v19, v7, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    add-int v19, v19, v20

    add-int v19, v19, v4

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v0, v1, v13, v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->ellipsize(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_2

    .line 831
    :cond_7
    add-int v18, v7, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int v18, v18, v19

    add-int v18, v18, v4

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_8

    .line 832
    add-int v18, v7, v11

    sub-int v18, v18, v4

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    .line 833
    add-int v18, v7, v11

    sub-int v18, v18, v4

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v19

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v0, v1, v13, v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->ellipsize(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_2

    .line 835
    :cond_8
    add-int v18, v7, v11

    sub-int v18, v18, v4

    add-int v19, v7, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    add-int v19, v19, v20

    add-int v19, v19, v4

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    .line 836
    add-int v18, v7, v11

    sub-int v18, v18, v4

    add-int v19, v7, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    add-int v19, v19, v20

    add-int v19, v19, v4

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v0, v1, v13, v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->ellipsize(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_2

    .line 848
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->childSttSearchResults:Ljava/util/HashMap;

    move-object/from16 v18, v0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/util/ArrayList;

    new-instance v19, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;

    add-int v20, v7, v11

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1, v12}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->findElapsedTime(ILjava/util/ArrayList;)J

    move-result-wide v20

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    move-wide/from16 v2, v20

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;-><init>(Ljava/lang/String;IJ)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3
.end method

.method public setCheckBoxSize(I)V
    .locals 0
    .param p1, "checkBoxSize"    # I

    .prologue
    .line 1225
    iput p1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxSize:I

    .line 1226
    return-void
.end method

.method public setExpandCallback(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;)V
    .locals 0
    .param p1, "callbacks"    # Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    .line 188
    return-void
.end method

.method public setLabelCallback(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;)V
    .locals 0
    .param p1, "callbacks"    # Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;

    .line 161
    return-void
.end method

.method public setSearchText(Ljava/lang/String;)V
    .locals 2
    .param p1, "searchText"    # Ljava/lang/String;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchText:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchMode:Z

    .line 169
    :goto_0
    return-void

    .line 168
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mSearchMode:Z

    goto :goto_0
.end method

.method public setSelectionMode(ZZ)V
    .locals 2
    .param p1, "isSelectionMode"    # Z
    .param p2, "needAnim"    # Z

    .prologue
    const/4 v1, 0x1

    .line 1205
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxSize:I

    if-ge v0, v1, :cond_1

    .line 1214
    :cond_0
    :goto_0
    return-void

    .line 1209
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mIsSelectionMode:Z

    .line 1211
    if-eqz p2, :cond_0

    .line 1212
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->startCheckBoxAnimation()V

    goto :goto_0
.end method

.method public unregisterOnLoadedObserver()V
    .locals 1

    .prologue
    .line 172
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->unregisterOnLoadedObserver()V

    .line 173
    return-void
.end method

.method public updateCachedBookmarks(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 1
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1055
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    sget-object v0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCacheLoader:Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->updateCachedBookmarks(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 1056
    return-void
.end method
