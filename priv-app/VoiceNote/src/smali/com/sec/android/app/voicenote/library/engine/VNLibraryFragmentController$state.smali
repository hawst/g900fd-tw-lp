.class public final enum Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;
.super Ljava/lang/Enum;
.source "VNLibraryFragmentController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "state"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

.field public static final enum BOOKMARK:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

.field public static final enum HIDDEN:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

.field public static final enum PLAYER:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

.field public static final enum TRIM:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 65
    new-instance v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->HIDDEN:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    new-instance v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    const-string v1, "PLAYER"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->PLAYER:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    new-instance v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    const-string v1, "TRIM"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->TRIM:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    new-instance v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    const-string v1, "BOOKMARK"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->BOOKMARK:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    .line 64
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->HIDDEN:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->PLAYER:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->TRIM:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->BOOKMARK:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->$VALUES:[Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    const-class v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->$VALUES:[Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    invoke-virtual {v0}, [Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    return-object v0
.end method
