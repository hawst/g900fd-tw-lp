.class Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment$2;
.super Ljava/lang/Object;
.source "VNPlayerControlButtonFragment.java"

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "hpw"    # Landroid/widget/HoverPopupWindow;

    .prologue
    const/4 v4, 0x0

    .line 183
    const-string v5, "skip_interval"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getBooleanSettings(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 208
    :cond_0
    :goto_0
    return v4

    .line 186
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 187
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 190
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEnableAirViewInforPreview(Landroid/content/ContentResolver;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 191
    if-eqz p2, :cond_0

    .line 192
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->setPlayIds()V
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->access$000(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;)V

    .line 193
    const/4 v3, 0x0

    .line 194
    .local v3, "v":Landroid/widget/TextView;
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 195
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f03001f

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .end local v3    # "v":Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 197
    .restart local v3    # "v":Landroid/widget/TextView;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 198
    .local v1, "id":I
    const v5, 0x7f0e0094

    if-ne v1, v5, :cond_3

    .line 199
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mPreSelectedTitle:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    :cond_2
    :goto_1
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    invoke-virtual {p2, v3}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 205
    const/4 v4, 0x1

    goto :goto_0

    .line 200
    :cond_3
    const v5, 0x7f0e0097

    if-ne v1, v5, :cond_2

    .line 201
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mNextSelectedTitle:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
