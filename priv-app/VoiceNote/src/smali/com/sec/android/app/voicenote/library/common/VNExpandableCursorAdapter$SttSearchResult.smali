.class public Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;
.super Ljava/lang/Object;
.source "VNExpandableCursorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SttSearchResult"
.end annotation


# instance fields
.field private elapsedTime:J

.field private highlightStartIndex:I

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;IJ)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "highlightStartIndex"    # I
    .param p3, "elapsedTime"    # J

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;->text:Ljava/lang/String;

    .line 113
    iput p2, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;->highlightStartIndex:I

    .line 114
    iput-wide p3, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;->elapsedTime:J

    .line 115
    return-void
.end method


# virtual methods
.method public getElapsedTime()J
    .locals 2

    .prologue
    .line 126
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;->elapsedTime:J

    return-wide v0
.end method

.method public getHighlightStartIndex()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;->highlightStartIndex:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$SttSearchResult;->text:Ljava/lang/String;

    return-object v0
.end method
