.class Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;
.super Ljava/lang/Thread;
.source "VNSttForThirdActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TimeBlinkThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V
    .locals 0

    .prologue
    .line 588
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$1;

    .prologue
    .line 588
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 593
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mIService:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$000(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    move-result-object v2

    if-nez v2, :cond_1

    .line 606
    :cond_0
    :goto_1
    return-void

    .line 597
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mUIUpdateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$900(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x83d

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 598
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity$TimeBlinkThread;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->mUIUpdateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->access$900(Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x83d

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 599
    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 601
    :catch_0
    move-exception v1

    .line 602
    .local v1, "ne":Ljava/lang/NullPointerException;
    const-string v2, "VNSttForThirdActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ignore pause time blink message after destroyed : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 603
    .end local v1    # "ne":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 604
    .local v0, "ie":Ljava/lang/InterruptedException;
    const-string v2, "VNSttForThirdActivity"

    const-string v3, "TimeBlinkThread InterruptedException"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
