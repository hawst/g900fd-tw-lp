.class Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;
.super Ljava/lang/Object;
.source "VNSettingLogoTextFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field beforeText:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)V
    .locals 1

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 320
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->beforeText:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    .line 324
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "charSequence"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 328
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->beforeText:Ljava/lang/String;

    .line 329
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5
    .param p1, "charSequence"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 333
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getLineCount()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->beforeText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 334
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->beforeText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int v0, v1, v2

    .line 335
    .local v0, "position":I
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->beforeText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 337
    if-lez v0, :cond_0

    .line 338
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 340
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 341
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$800(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/Toast;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 342
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$800(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->cancel()V

    .line 343
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mToast:Landroid/widget/Toast;
    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$802(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 345
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$800(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/Toast;

    move-result-object v1

    if-nez v1, :cond_2

    .line 346
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0b00ac

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    # setter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mToast:Landroid/widget/Toast;
    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$802(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 347
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$800(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 353
    .end local v0    # "position":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # invokes: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->enableDoneButton()Z
    invoke-static {v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1000(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Z

    move-result v2

    # setter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$902(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Z)Z

    .line 354
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/Button;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 355
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/Button;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;->this$0:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    # getter for: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z
    invoke-static {v3}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$900(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Z

    move-result v3

    # invokes: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setEnableButton(Landroid/widget/Button;Z)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->access$1200(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Landroid/widget/Button;Z)V

    .line 357
    :cond_3
    return-void
.end method
