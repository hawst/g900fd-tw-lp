.class public Lcom/sec/android/app/voicenote/common/util/VNCscCustomerParser;
.super Ljava/lang/Object;
.source "VNCscCustomerParser.java"


# static fields
.field private static final CSC_PATH:Ljava/lang/String; = "/system/csc/customer.xml"

.field private static final MMS_MAX_SIZE_TAG:Ljava/lang/String; = "MessageSize"

.field private static final TAG:Ljava/lang/String; = "CscCustomerParser"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getMmsMaxSize()Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 66
    const-string v10, "CscCustomerParser"

    const-string v11, "getMmsMaxSize()"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const/4 v7, 0x0

    .line 69
    .local v7, "mmsMaxSize":Ljava/lang/String;
    const/4 v5, 0x0

    .line 72
    .local v5, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v3, Ljava/io/File;

    const-string v10, "/system/csc/customer.xml"

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 73
    .local v3, "f":Ljava/io/File;
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .local v6, "fis":Ljava/io/FileInputStream;
    move-object v5, v6

    .line 78
    .end local v3    # "f":Ljava/io/File;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :goto_0
    if-nez v5, :cond_0

    .line 79
    const/4 v7, 0x0

    .line 117
    .end local v7    # "mmsMaxSize":Ljava/lang/String;
    :goto_1
    return-object v7

    .line 74
    .restart local v7    # "mmsMaxSize":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 75
    .local v1, "e1":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 82
    .end local v1    # "e1":Ljava/io/FileNotFoundException;
    :cond_0
    move-object v8, v5

    .line 86
    .local v8, "stream":Ljava/io/InputStream;
    :try_start_1
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 87
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v10, 0x1

    invoke-virtual {v4, v10}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 88
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v9

    .line 90
    .local v9, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    const-string v10, "UTF-8"

    invoke-interface {v9, v8, v10}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 91
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    .line 93
    .local v2, "eventType":I
    :goto_2
    if-eq v2, v12, :cond_2

    .line 94
    const/4 v10, 0x2

    if-ne v2, v10, :cond_1

    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "MessageSize"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 95
    const-string v10, "CscCustomerParser"

    const-string v11, "CSC_PATH"

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 97
    const/4 v2, 0x1

    .line 98
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v7

    goto :goto_2

    .line 100
    :cond_1
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v2

    goto :goto_2

    .line 103
    .end local v2    # "eventType":I
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v9    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_1
    move-exception v0

    .line 104
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 109
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :cond_2
    :goto_3
    if-eqz v8, :cond_3

    .line 111
    :try_start_2
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 116
    :cond_3
    :goto_4
    const-string v10, "CscCustomerParser"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mmsMaxSize : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 105
    :catch_2
    move-exception v0

    .line 106
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 112
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 113
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4
.end method

.method public static setMmsMaxSizeFromCustomer(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    const-string v3, "CscCustomerParser"

    const-string v4, "setMmsMaxSizeFromCustomer()"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const-wide/32 v0, 0x49c00

    .line 45
    .local v0, "lMmsMaxSize":J
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNCscCustomerParser;->getMmsMaxSize()Ljava/lang/String;

    move-result-object v2

    .line 47
    .local v2, "mms_max_size":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v3, "1m"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 48
    const-wide/32 v0, 0xf8c00

    .line 49
    invoke-static {p0, v2, v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setMmsMaxSize(Landroid/content/Context;Ljava/lang/String;J)V

    .line 63
    :goto_0
    return-void

    .line 51
    :cond_0
    if-eqz v2, :cond_1

    const-string v3, "600"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 52
    const-wide/32 v0, 0x94c00

    .line 53
    invoke-static {p0, v2, v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setMmsMaxSize(Landroid/content/Context;Ljava/lang/String;J)V

    goto :goto_0

    .line 55
    :cond_1
    if-eqz v2, :cond_2

    const-string v3, "1.2m"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 56
    const-wide/32 v0, 0x12ac00

    .line 57
    invoke-static {p0, v2, v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setMmsMaxSize(Landroid/content/Context;Ljava/lang/String;J)V

    goto :goto_0

    .line 61
    :cond_2
    const-string v3, "CscCustomerParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CscCustomerParser cannot catch the mms_max_size : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cannot catch the mms max size : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setMmsMaxSize(Landroid/content/Context;Ljava/lang/String;J)V

    goto :goto_0
.end method
