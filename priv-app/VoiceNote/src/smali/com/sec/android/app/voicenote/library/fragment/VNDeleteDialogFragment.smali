.class public Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNDeleteDialogFragment.java"


# static fields
.field private static final BACKUP_BOOKMARK:Ljava/lang/String; = "bookmark"

.field private static final BACKUP_ID:Ljava/lang/String; = "id"

.field private static final BACKUP_PALY:Ljava/lang/String; = "bPlay"

.field private static final TAG:Ljava/lang/String; = "VNDeleteDialogFragment"


# instance fields
.field mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field private mCurrentSelectedIds:[J

.field private mHandler:Landroid/os/Handler;

.field private mIsBookmark:Z

.field private mbPlayingFile:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 45
    new-array v0, v1, [J

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mCurrentSelectedIds:[J

    .line 46
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mbPlayingFile:Z

    .line 47
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 48
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mIsBookmark:Z

    .line 49
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mHandler:Landroid/os/Handler;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mIsBookmark:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;)[J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mCurrentSelectedIds:[J

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mbPlayingFile:Z

    return v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 76
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 77
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 91
    const-string v3, "VNDeleteDialogFragment"

    const-string v4, "onCreateDialog"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    if-eqz p1, :cond_0

    .line 94
    const-string v3, "id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mCurrentSelectedIds:[J

    .line 95
    const-string v3, "bPlay"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mbPlayingFile:Z

    .line 96
    const-string v3, "bookmark"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mIsBookmark:Z

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "VNBookmarkListFragment"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    .line 98
    .local v1, "frag":Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    if-eqz v1, :cond_0

    .line 99
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getBookmakrHandler()Landroid/os/Handler;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mHandler:Landroid/os/Handler;

    .line 103
    .end local v1    # "frag":Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 104
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0b005d

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 105
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mCurrentSelectedIds:[J

    array-length v2, v3

    .line 106
    .local v2, "len":I
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mIsBookmark:Z

    if-eqz v3, :cond_1

    .line 107
    const v3, 0x7f0b005e

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 114
    :goto_0
    const v3, 0x104000a

    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 144
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 145
    const/high16 v3, 0x1040000

    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 153
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3

    .line 108
    :cond_1
    if-ne v2, v5, :cond_2

    .line 109
    const v3, 0x7f0b005f

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 111
    :cond_2
    const v3, 0x7f0b0061

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "saveInstance"    # Landroid/os/Bundle;

    .prologue
    .line 83
    const-string v0, "id"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mCurrentSelectedIds:[J

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 84
    const-string v0, "bPlay"

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mbPlayingFile:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 85
    const-string v0, "bookmark"

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mIsBookmark:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 86
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 87
    return-void
.end method

.method public setArguments(JZ)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "bPlay"    # Z

    .prologue
    .line 56
    const/4 v0, 0x1

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mCurrentSelectedIds:[J

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mCurrentSelectedIds:[J

    const/4 v1, 0x0

    aput-wide p1, v0, v1

    .line 58
    iput-boolean p3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mbPlayingFile:Z

    .line 59
    return-void
.end method

.method public setArguments([JZ)V
    .locals 1
    .param p1, "ids"    # [J
    .param p2, "bPlay"    # Z

    .prologue
    .line 62
    invoke-virtual {p1}, [J->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mCurrentSelectedIds:[J

    .line 63
    iput-boolean p2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mbPlayingFile:Z

    .line 64
    return-void
.end method

.method public setBookmarkMode(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mIsBookmark:Z

    .line 68
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->mHandler:Landroid/os/Handler;

    .line 69
    return-void
.end method
