.class public Lcom/sec/android/app/voicenote/library/stt/M4aReader;
.super Ljava/lang/Object;
.source "M4aReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;
    }
.end annotation


# instance fields
.field private final INVALID_NAME:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private mByteBuffer:Ljava/nio/ByteBuffer;

.field private mFileChannel:Ljava/nio/channels/FileChannel;

.field private mFileInputStream:Ljava/io/FileInputStream;

.field private final mPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-string v0, "M4aReader"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->TAG:Ljava/lang/String;

    .line 16
    const-string v0, "Invalid_name"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->INVALID_NAME:Ljava/lang/String;

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mPath:Ljava/lang/String;

    .line 24
    return-void
.end method

.method private arrToAscii([B)Ljava/lang/String;
    .locals 3
    .param p1, "data"    # [B

    .prologue
    .line 371
    if-nez p1, :cond_0

    .line 372
    const/4 v2, 0x0

    .line 383
    :goto_0
    return-object v2

    .line 375
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 376
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 377
    aget-byte v2, p1, v0

    if-gez v2, :cond_1

    .line 378
    const-string v2, "Invalid_name"

    goto :goto_0

    .line 381
    :cond_1
    aget-byte v2, p1, v0

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 376
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 383
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private findBOOK(Lcom/sec/android/app/voicenote/library/stt/M4aInfo;)V
    .locals 14
    .param p1, "inf"    # Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x8

    .line 131
    const-wide/16 v6, 0x0

    .line 132
    .local v6, "skip":J
    const-string v0, ""

    .line 135
    .local v0, "boxName":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->udtaPos:J

    add-long/2addr v8, v12

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 136
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 137
    :cond_0
    :goto_0
    const-string v3, "book"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 138
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 139
    .local v2, "readCount":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 140
    const-string v3, "M4aReader"

    const-string v8, "EOF - no book atom found"

    invoke-static {v3, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    const/4 v3, 0x0

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasBookmarks:Z

    .line 142
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 143
    const/4 v3, 0x0

    iput v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileBookLength:I

    .line 204
    :cond_1
    :goto_1
    return-void

    .line 147
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 148
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    int-to-long v6, v3

    .line 149
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 151
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 152
    if-ltz v2, :cond_1

    .line 155
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 156
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->arrToAscii([B)Ljava/lang/String;

    move-result-object v0

    .line 159
    const-string v3, "book"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 160
    sub-long/2addr v6, v12

    .line 161
    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v6

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v10

    cmp-long v3, v8, v10

    if-gtz v3, :cond_3

    .line 162
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v6

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 199
    .end local v2    # "readCount":I
    :catch_0
    move-exception v1

    .line 200
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "M4aReader"

    const-string v8, "Error reading the file"

    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 202
    throw v1

    .line 164
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "readCount":I
    :cond_3
    :try_start_1
    const-string v3, "M4aReader"

    const-string v8, "Wrong skip value finding Bookmark! Returning from function"

    invoke-static {v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    const/4 v3, 0x0

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasBookmarks:Z

    goto :goto_1

    .line 171
    .end local v2    # "readCount":I
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v4

    .line 173
    .local v4, "savePosition":J
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    sub-long/2addr v8, v12

    iput-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->bookPos:J

    .line 175
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->bookPos:J

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 176
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 177
    .restart local v2    # "readCount":I
    if-ltz v2, :cond_1

    .line 181
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 182
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileBookLength:I

    .line 183
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 184
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 187
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    iput-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->bnumPos:J

    .line 188
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 189
    if-ltz v2, :cond_1

    .line 192
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 193
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileBnumLength:I

    .line 194
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 195
    const/4 v3, 0x1

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasBookmarks:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private findMEMO(Lcom/sec/android/app/voicenote/library/stt/M4aInfo;)V
    .locals 14
    .param p1, "inf"    # Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x8

    .line 207
    const-wide/16 v6, 0x0

    .line 210
    .local v6, "skip":J
    const-string v0, ""

    .line 213
    .local v0, "boxName":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->udtaPos:J

    add-long/2addr v8, v12

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 214
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 215
    :cond_0
    :goto_0
    const-string v3, "memo"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 216
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 217
    .local v2, "readCount":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 218
    const/4 v3, 0x0

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasMemo:Z

    .line 305
    :cond_1
    :goto_1
    return-void

    .line 222
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 223
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    int-to-long v6, v3

    .line 224
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 226
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 227
    if-ltz v2, :cond_1

    .line 231
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 232
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->arrToAscii([B)Ljava/lang/String;

    move-result-object v0

    .line 235
    const-string v3, "memo"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 236
    sub-long/2addr v6, v12

    .line 237
    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v6

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v10

    cmp-long v3, v8, v10

    if-gtz v3, :cond_3

    .line 238
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v6

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 300
    .end local v2    # "readCount":I
    :catch_0
    move-exception v1

    .line 301
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "M4aReader"

    const-string v8, "Error reading the file"

    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 303
    throw v1

    .line 240
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "readCount":I
    :cond_3
    :try_start_1
    const-string v3, "M4aReader"

    const-string v8, "Wrong skip value finding Memo! Possibly we are out of the file. Returning from function"

    invoke-static {v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    const/4 v3, 0x0

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasMemo:Z

    goto :goto_1

    .line 247
    .end local v2    # "readCount":I
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    sub-long/2addr v8, v12

    iput-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->memoPos:J

    .line 249
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v4

    .line 250
    .local v4, "save":J
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->memoPos:J

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 251
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 252
    .restart local v2    # "readCount":I
    if-ltz v2, :cond_1

    .line 256
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 257
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileMemoLength:I

    .line 258
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 261
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->memoPos:J

    add-long/2addr v8, v12

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 263
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    iput-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->filePos:J

    .line 264
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 265
    if-ltz v2, :cond_1

    .line 269
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 270
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileImagLength:I

    .line 271
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 273
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->filePos:J

    iget v10, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileImagLength:I

    int-to-long v10, v10

    add-long/2addr v8, v10

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 275
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    iput-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->thumPos:J

    .line 276
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 277
    if-ltz v2, :cond_1

    .line 281
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 282
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileThumLength:I

    .line 283
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 285
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->thumPos:J

    iget v10, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileThumLength:I

    int-to-long v10, v10

    add-long/2addr v8, v10

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 287
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    iput-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->textPos:J

    .line 288
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 289
    if-ltz v2, :cond_1

    .line 293
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 294
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileTextLength:I

    .line 295
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 297
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 299
    const/4 v3, 0x1

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasMemo:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private findOuterAtoms(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;
    .locals 14
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x8

    const/4 v6, 0x0

    .line 75
    const-wide/16 v4, 0x0

    .line 76
    .local v4, "skip":J
    const-string v0, ""

    .line 77
    .local v0, "boxName":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;

    invoke-direct {v3}, Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;-><init>()V

    .line 78
    .local v3, "pair":Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;
    const/4 v1, 0x0

    .line 81
    .local v1, "countRead":I
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 82
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v7, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 83
    if-gez v1, :cond_1

    move-object v3, v6

    .line 121
    .end local v3    # "pair":Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;
    :goto_1
    return-object v3

    .line 87
    .restart local v3    # "pair":Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 88
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    int-to-long v4, v7

    .line 89
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 91
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v7, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 92
    if-gez v1, :cond_2

    move-object v3, v6

    .line 93
    goto :goto_1

    .line 96
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 97
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->arrToAscii([B)Ljava/lang/String;

    move-result-object v0

    .line 99
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 100
    sub-long/2addr v4, v12

    .line 101
    const-wide/16 v8, 0x0

    cmp-long v7, v4, v8

    if-lez v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v4

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-gtz v7, :cond_3

    .line 102
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v4

    invoke-virtual {v7, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 123
    :catch_0
    move-exception v2

    .line 124
    .local v2, "e":Ljava/io/IOException;
    const-string v6, "M4aReader"

    const-string v7, "Error reading the file"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 126
    throw v2

    .line 104
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_1
    const-string v7, "M4aReader"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Wrong skip value finding OuterAtom: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ! Returning from function"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v6

    .line 105
    goto/16 :goto_1

    .line 109
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    sub-long/2addr v8, v12

    iput-wide v8, v3, Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;->position:J

    .line 110
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 111
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-wide v8, v3, Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;->position:J

    invoke-virtual {v7, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 112
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v7, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 113
    if-gez v1, :cond_5

    move-object v3, v6

    .line 114
    goto/16 :goto_1

    .line 117
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 118
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    iput v6, v3, Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;->length:I

    .line 119
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 120
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-wide v8, v3, Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;->position:J

    add-long/2addr v8, v12

    invoke-virtual {v6, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private findSTTD(Lcom/sec/android/app/voicenote/library/stt/M4aInfo;)V
    .locals 14
    .param p1, "inf"    # Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x8

    .line 308
    const-wide/16 v6, 0x0

    .line 311
    .local v6, "skip":J
    const-string v0, ""

    .line 314
    .local v0, "boxName":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->udtaPos:J

    add-long/2addr v8, v12

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 315
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 316
    :cond_0
    :goto_0
    const-string v3, "sttd"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 317
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 318
    .local v2, "readCount":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 319
    const/4 v3, 0x0

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasSttd:Z

    .line 368
    :cond_1
    :goto_1
    return-void

    .line 323
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 324
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    int-to-long v6, v3

    .line 325
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 327
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 328
    if-ltz v2, :cond_1

    .line 332
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 333
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->arrToAscii([B)Ljava/lang/String;

    move-result-object v0

    .line 336
    const-string v3, "sttd"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 337
    sub-long/2addr v6, v12

    .line 338
    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v6

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v10

    cmp-long v3, v8, v10

    if-gtz v3, :cond_3

    .line 339
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v6

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 363
    .end local v2    # "readCount":I
    :catch_0
    move-exception v1

    .line 364
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "M4aReader"

    const-string v8, "Error reading the file"

    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 366
    throw v1

    .line 341
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "readCount":I
    :cond_3
    :try_start_1
    const-string v3, "M4aReader"

    const-string v8, "Wrong skip value finding STTD! Possibly we are out of the file. Returning from function"

    invoke-static {v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    const/4 v3, 0x0

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasSttd:Z

    goto :goto_1

    .line 348
    .end local v2    # "readCount":I
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    sub-long/2addr v8, v12

    iput-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->sttdPos:J

    .line 350
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v4

    .line 351
    .local v4, "save":J
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->sttdPos:J

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 352
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 353
    .restart local v2    # "readCount":I
    if-ltz v2, :cond_1

    .line 357
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 358
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileSttdLength:I

    .line 359
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 360
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 362
    const/4 v3, 0x1

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasSttd:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private isM4A()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 387
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mPath:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 394
    :cond_0
    :goto_0
    return v0

    .line 391
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mPath:Ljava/lang/String;

    const-string v2, ".m4a"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mPath:Ljava/lang/String;

    const-string v2, ".M4A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 392
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isM4A(Ljava/lang/String;)Z
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 404
    if-nez p0, :cond_1

    .line 411
    :cond_0
    :goto_0
    return v0

    .line 408
    :cond_1
    const-string v1, ".m4a"

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ".M4A"

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 409
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public readFile()Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 27
    const-string v6, "M4aReader"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "readFile() - start. Path: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    const/4 v2, 0x0

    .line 30
    .local v2, "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->isM4A()Z

    move-result v6

    if-nez v6, :cond_0

    .line 31
    const-string v6, "M4aReader"

    const-string v7, "readFile() : this file is not m4a"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    :goto_0
    return-object v5

    .line 35
    :cond_0
    const/4 v6, 0x4

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mByteBuffer:Ljava/nio/ByteBuffer;

    .line 37
    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mPath:Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 39
    .local v1, "f":Ljava/io/File;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileInputStream:Ljava/io/FileInputStream;

    .line 40
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v6}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileChannel:Ljava/nio/channels/FileChannel;

    .line 42
    new-instance v3, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    invoke-direct {v3}, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;-><init>()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 43
    .end local v2    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .local v3, "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mPath:Ljava/lang/String;

    iput-object v6, v3, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->path:Ljava/lang/String;

    .line 44
    const/4 v4, 0x0

    .line 45
    .local v4, "pair":Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;
    const-string v6, "moov"

    invoke-direct {p0, v6}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->findOuterAtoms(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;

    move-result-object v4

    .line 46
    if-nez v4, :cond_1

    move-object v2, v3

    .line 47
    .end local v3    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .restart local v2    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    goto :goto_0

    .line 49
    .end local v2    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .restart local v3    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    :cond_1
    iget v6, v4, Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;->length:I

    iput v6, v3, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileMoovLength:I

    .line 50
    iget-wide v6, v4, Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;->position:J

    iput-wide v6, v3, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->moovPos:J

    .line 51
    const-string v6, "udta"

    invoke-direct {p0, v6}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->findOuterAtoms(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;

    move-result-object v4

    .line 52
    if-nez v4, :cond_2

    move-object v2, v3

    .line 53
    .end local v3    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .restart local v2    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    goto :goto_0

    .line 54
    .end local v2    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .restart local v3    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    :cond_2
    iget v5, v4, Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;->length:I

    iput v5, v3, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileUdtaLength:I

    .line 55
    iget-wide v6, v4, Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;->position:J

    iput-wide v6, v3, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->udtaPos:J

    .line 57
    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->findBOOK(Lcom/sec/android/app/voicenote/library/stt/M4aInfo;)V

    .line 58
    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->findMEMO(Lcom/sec/android/app/voicenote/library/stt/M4aInfo;)V

    .line 59
    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->findSTTD(Lcom/sec/android/app/voicenote/library/stt/M4aInfo;)V

    .line 61
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v2, v3

    .end local v3    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .end local v4    # "pair":Lcom/sec/android/app/voicenote/library/stt/M4aReader$Pair;
    .restart local v2    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    :goto_1
    move-object v5, v2

    .line 71
    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_2
    const/4 v2, 0x0

    .line 65
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 66
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 67
    .local v0, "e":Ljava/io/IOException;
    :goto_3
    const/4 v2, 0x0

    .line 68
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 66
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .restart local v3    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    :catch_2
    move-exception v0

    move-object v2, v3

    .end local v3    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .restart local v2    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    goto :goto_3

    .line 63
    .end local v2    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .restart local v3    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    :catch_3
    move-exception v0

    move-object v2, v3

    .end local v3    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .restart local v2    # "inf":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    goto :goto_2
.end method
