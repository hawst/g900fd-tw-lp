.class public Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNAlertDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;
    }
.end annotation


# instance fields
.field private mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->doPositiveAction()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->doNegativeAction()V

    return-void
.end method

.method private doNegativeAction()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;->onAlertDialogPositiveBtn(Z)V

    .line 162
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->dismissAllowingStateLoss()V

    .line 165
    :cond_1
    return-void
.end method

.method private doPositiveAction()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;->onAlertDialogPositiveBtn(Z)V

    .line 152
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->dismissAllowingStateLoss()V

    .line 155
    :cond_1
    return-void
.end method

.method private getButtonName(I)I
    .locals 1
    .param p1, "button"    # I

    .prologue
    .line 122
    packed-switch p1, :pswitch_data_0

    .line 127
    const v0, 0x104000a

    :goto_0
    return v0

    .line 125
    :pswitch_0
    const v0, 0x7f0b005d

    goto :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static setArguments(III)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;
    .locals 4
    .param p0, "title"    # I
    .param p1, "message"    # I
    .param p2, "positiveButton"    # I

    .prologue
    .line 42
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;-><init>()V

    .line 43
    .local v1, "dialogFragment":Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 44
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 45
    const-string v2, "message"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 46
    const-string v2, "isdelete"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 47
    const-string v2, "positiveButton"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 48
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 49
    return-object v1
.end method

.method public static setArguments(IIIZI)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;
    .locals 3
    .param p0, "title"    # I
    .param p1, "message"    # I
    .param p2, "count"    # I
    .param p3, "bool"    # Z
    .param p4, "positiveButton"    # I

    .prologue
    .line 52
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;-><init>()V

    .line 53
    .local v1, "dialogFragment":Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 54
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 55
    const-string v2, "message"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 56
    const-string v2, "count"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 57
    const-string v2, "isdelete"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 58
    const-string v2, "positiveButton"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 59
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 60
    return-object v1
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;

    instance-of v0, v0, Landroid/app/Fragment;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 142
    check-cast v0, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;

    .line 144
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 145
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 73
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 74
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "title"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "isdelete"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 76
    .local v3, "isDelete":Z
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "positiveButton"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 78
    .local v4, "positiveButton":I
    if-eqz v3, :cond_1

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "count"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 80
    .local v2, "delcout":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "message"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, "delDilogMsg":Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 87
    .end local v1    # "delDilogMsg":Ljava/lang/String;
    .end local v2    # "delcout":I
    :goto_0
    invoke-direct {p0, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->getButtonName(I)I

    move-result v5

    new-instance v6, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 93
    const/high16 v5, 0x1040000

    new-instance v6, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 102
    sget-boolean v5, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_CHECK_ATTVN_ENABLED:Z

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "message"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    const v6, 0x7f0b0179

    if-ne v5, v6, :cond_0

    .line 104
    new-instance v5, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$3;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;)V

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 117
    :cond_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    return-object v5

    .line 84
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "message"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v1, 0x100

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 136
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public registerCallback(Landroid/app/Fragment;)V
    .locals 0
    .param p1, "fragment"    # Landroid/app/Fragment;

    .prologue
    .line 168
    check-cast p1, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;

    .end local p1    # "fragment":Landroid/app/Fragment;
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment$Callbacks;

    .line 169
    return-void
.end method
