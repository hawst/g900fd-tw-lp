.class Lcom/sec/android/app/voicenote/main/VNMainActivity$3;
.super Ljava/lang/Object;
.source "VNMainActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 977
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 980
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsFromAttachMode:Z

    if-nez v2, :cond_1

    .line 981
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionBarIsSearched()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 982
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$000(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 983
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mModeItem:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$000(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 984
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDeleteItem:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$100(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 985
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->setTitleText()Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$200(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Ljava/lang/String;

    move-result-object v1

    .line 986
    .local v1, "setTitle":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 987
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mSearchView:Landroid/widget/SearchView;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$300(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/widget/SearchView;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 988
    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v2

    if-eqz v2, :cond_2

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$400()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v2

    const/16 v3, 0x17

    if-ne v2, v3, :cond_2

    .line 1007
    .end local v1    # "setTitle":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 991
    .restart local v1    # "setTitle":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mLibEngine:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    move-result-object v2

    invoke-virtual {v2, v4, v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->setSearchMode(ZZ)V

    goto :goto_0

    .line 995
    .end local v1    # "setTitle":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$600(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$600(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mExpandableListFragment:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$600(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/ExpandableListAdapter;->getGroupCount()I

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionBarIsSearched()Z

    move-result v2

    if-nez v2, :cond_4

    .line 997
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mDeleteItem:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$100(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 999
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1002
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 1003
    .local v0, "ft":Landroid/app/FragmentTransaction;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$3;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # invokes: Lcom/sec/android/app/voicenote/main/VNMainActivity;->drawControlButton(Landroid/app/FragmentTransaction;)V
    invoke-static {v2, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$700(Lcom/sec/android/app/voicenote/main/VNMainActivity;Landroid/app/FragmentTransaction;)V

    .line 1004
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method
