.class public Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;
.super Ljava/lang/Object;
.source "VNLibraryFragmentController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$librarystate;,
        Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;
    }
.end annotation


# static fields
.field public static final BOOKMARKLISTFRAGMENT:Ljava/lang/String; = "VNBookmarkListFragment"

.field public static final LIBRARYLIST:Ljava/lang/String; = "VNLibraryExpandableListFragment"

.field private static final MSG_CLICK_NEXTITEM:I = 0x4e23

.field private static final MSG_CLICK_PREVIOUSITEM:I = 0x4e24

.field public static final PLAYERCONTROLBAR:Ljava/lang/String; = "VNPlayerControlButtonFragment"

.field public static final PLAYERIDLEBAR:Ljava/lang/String; = "VNPlayerControlIdleFragment"

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# instance fields
.field private final PLAYERTOOLBAR:Ljava/lang/String;

.field private final STTFRAGMENT:Ljava/lang/String;

.field private final TRIMFRAGMENT:Ljava/lang/String;

.field private bActionModeOn:Z

.field private bSearchOn:Z

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-string v0, "VNPlyayerToolbarFragment"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->PLAYERTOOLBAR:Ljava/lang/String;

    .line 50
    const-string v0, "VNTrimFragment"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->TRIMFRAGMENT:Ljava/lang/String;

    .line 52
    const-string v0, "VNEditSTTFragment"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->STTFRAGMENT:Ljava/lang/String;

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->bSearchOn:Z

    .line 63
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->bActionModeOn:Z

    .line 655
    new-instance v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$1;-><init>(Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mHandler:Landroid/os/Handler;

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-string v0, "VNPlyayerToolbarFragment"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->PLAYERTOOLBAR:Ljava/lang/String;

    .line 50
    const-string v0, "VNTrimFragment"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->TRIMFRAGMENT:Ljava/lang/String;

    .line 52
    const-string v0, "VNEditSTTFragment"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->STTFRAGMENT:Ljava/lang/String;

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->bSearchOn:Z

    .line 63
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->bActionModeOn:Z

    .line 655
    new-instance v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$1;-><init>(Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mHandler:Landroid/os/Handler;

    .line 84
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    .line 85
    return-void
.end method

.method private enableRepeatButton(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 637
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNPlyayerToolbarFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .line 638
    .local v0, "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    if-eqz v0, :cond_0

    .line 639
    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->enableRepeatButton(Z)V

    .line 641
    :cond_0
    return-void
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 72
    sput-object p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 73
    return-void
.end method

.method public static releaseService()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 77
    return-void
.end method


# virtual methods
.method public addBookmarkList(JLjava/util/ArrayList;)V
    .locals 7
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 536
    .local p3, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->isFinishing()Z

    move-result v4

    if-nez v4, :cond_1

    .line 537
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 538
    .local v2, "fragmentTransaction":Landroid/app/FragmentTransaction;
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    invoke-direct {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;-><init>()V

    .line 539
    .local v0, "bf":Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    invoke-virtual {v0, p3}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->setBookmark(Ljava/util/ArrayList;)V

    .line 540
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 541
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v4, "select_id"

    invoke-virtual {v1, v4, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 542
    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 543
    const v4, 0x7f0e0014

    const-string v5, "VNBookmarkListFragment"

    invoke-virtual {v2, v4, v0, v5}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 544
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 545
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 546
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "VNLibraryExpandableListFragment"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 547
    .local v3, "list":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-eqz v3, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setFocusListView(Z)V

    .line 548
    :cond_0
    sget-object v4, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v4, :cond_1

    .line 549
    sget-object v4, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    sget-object v5, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->BOOKMARK:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setFragmentControllerState(Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;)V

    .line 552
    .end local v0    # "bf":Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    .end local v3    # "list":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :cond_1
    return-void
.end method

.method public addIdleContorl()V
    .locals 4

    .prologue
    .line 179
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isDestroyed()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 184
    .local v0, "fragmentTransaction":Landroid/app/FragmentTransaction;
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;-><init>()V

    .line 185
    .local v1, "playerIdleFrag":Landroid/app/Fragment;
    const v2, 0x7f0e0017

    const-string v3, "VNPlayerControlIdleFragment"

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 186
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    .line 187
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 189
    :cond_2
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method public addPlayer()V
    .locals 5

    .prologue
    .line 96
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isDestroyed()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->getCurrentState()Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->PLAYER:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    if-eq v2, v3, :cond_0

    .line 102
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 103
    .local v0, "fm":Landroid/app/FragmentManager;
    const-string v2, "FRAGMENT_DIALOG"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 104
    const-string v2, "FRAGMENT_DIALOG_FILEORENAME"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 105
    const-string v2, "FRAGMENT_MULTI_CATEGORY"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 106
    const-string v2, "FRAGMENT_FILE"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 107
    const-string v2, "FRAGMENT_DO_NOT_BOOKMARK"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 108
    const-string v2, "FRAGMENT_PRIVATE_SELECT"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 110
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 111
    .local v1, "fragmentTransaction":Landroid/app/FragmentTransaction;
    const v2, 0x7f0e0016

    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-direct {v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;-><init>()V

    const-string v4, "VNPlyayerToolbarFragment"

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 112
    const v2, 0x7f0e0017

    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    invoke-direct {v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;-><init>()V

    const-string v4, "VNPlayerControlButtonFragment"

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 113
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 115
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_0

    .line 116
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    sget-object v3, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->PLAYER:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setFragmentControllerState(Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;)V

    goto :goto_0
.end method

.method public addTrim()V
    .locals 9

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->getCurrentState()Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->TRIM:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    if-ne v6, v7, :cond_1

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    if-eqz v6, :cond_0

    .line 264
    const/4 v6, 0x1

    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setTrimMode(Z)V

    .line 266
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 267
    .local v3, "fragmentTransaction":Landroid/app/FragmentTransaction;
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "VNPlayerControlButtonFragment"

    invoke-virtual {v6, v7}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 268
    .local v0, "controlbar":Landroid/app/Fragment;
    if-eqz v0, :cond_2

    .line 269
    invoke-virtual {v3, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 272
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "VNPlyayerToolbarFragment"

    invoke-virtual {v6, v7}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v5

    .line 273
    .local v5, "toolbar":Landroid/app/Fragment;
    if-eqz v5, :cond_3

    .line 274
    invoke-virtual {v3, v5}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 277
    :cond_3
    const v6, 0x7f0e0018

    new-instance v7, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-direct {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;-><init>()V

    const-string v8, "VNTrimFragment"

    invoke-virtual {v3, v6, v7, v8}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 278
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->isBookmarkList()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->isFinishing()Z

    move-result v6

    if-nez v6, :cond_4

    .line 282
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/FragmentManager;->popBackStackImmediate()Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    :cond_4
    :goto_1
    const/4 v4, 0x0

    .line 288
    .local v4, "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "VNLibraryExpandableListFragment"

    invoke-virtual {v6, v7}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 289
    .local v2, "frag":Landroid/app/Fragment;
    instance-of v6, v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v6, :cond_5

    move-object v4, v2

    .line 290
    check-cast v4, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 292
    :cond_5
    if-eqz v4, :cond_6

    .line 293
    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->executeTrimMode()V

    .line 295
    :cond_6
    sget-object v6, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v6, :cond_0

    .line 296
    sget-object v6, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    sget-object v7, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->TRIM:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setFragmentControllerState(Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;)V

    goto/16 :goto_0

    .line 283
    .end local v2    # "frag":Landroid/app/Fragment;
    .end local v4    # "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :catch_0
    move-exception v1

    .line 284
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public changeSkipSilenceButton()V
    .locals 3

    .prologue
    .line 683
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNPlyayerToolbarFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .line 684
    .local v0, "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    if-eqz v0, :cond_0

    .line 685
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->changeSkipSilenceButton()V

    .line 687
    :cond_0
    return-void
.end method

.method public clickNextItem()V
    .locals 4

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->removeBookmarkList()V

    .line 396
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNLibraryExpandableListFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 397
    .local v1, "listFragment":Landroid/app/Fragment;
    instance-of v2, v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v2, :cond_0

    .line 398
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNLibraryExpandableListFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 399
    .local v0, "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-eqz v0, :cond_0

    .line 400
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->clickNextItem()V

    .line 403
    .end local v0    # "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :cond_0
    return-void
.end method

.method public clickNextItemAsync()V
    .locals 4

    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->removeBookmarkList()V

    .line 381
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 382
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x4e23

    iput v1, v0, Landroid/os/Message;->what:I

    .line 383
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 384
    return-void
.end method

.method public clickPreviousItem()V
    .locals 4

    .prologue
    .line 406
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->removeBookmarkList()V

    .line 407
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNLibraryExpandableListFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 408
    .local v1, "listFragment":Landroid/app/Fragment;
    instance-of v2, v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v2, :cond_0

    .line 409
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNLibraryExpandableListFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 410
    .local v0, "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-eqz v0, :cond_0

    .line 411
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->clickPreviousItem()V

    .line 414
    .end local v0    # "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :cond_0
    return-void
.end method

.method public clickPreviousItemAsync()V
    .locals 4

    .prologue
    .line 387
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->removeBookmarkList()V

    .line 389
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 390
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x4e24

    iput v1, v0, Landroid/os/Message;->what:I

    .line 391
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 392
    return-void
.end method

.method public doPauseForTrim()V
    .locals 3

    .prologue
    .line 690
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNTrimFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .line 691
    .local v0, "trim":Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;
    if-eqz v0, :cond_0

    .line 692
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->doPause()V

    .line 694
    :cond_0
    return-void
.end method

.method public doPlayTrimNewFile()V
    .locals 3

    .prologue
    .line 673
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->isSTTMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 674
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->switchSTT(Ljava/util/ArrayList;)V

    .line 676
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNLibraryExpandableListFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 677
    .local v0, "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-eqz v0, :cond_1

    .line 678
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->doPlayTrimNewFile()V

    .line 680
    :cond_1
    return-void
.end method

.method public doResumeForTrim()V
    .locals 3

    .prologue
    .line 697
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNTrimFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .line 698
    .local v0, "trim":Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;
    if-eqz v0, :cond_0

    .line 699
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->doResume()V

    .line 701
    :cond_0
    return-void
.end method

.method public doUpdateTrimView()V
    .locals 3

    .prologue
    .line 704
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNTrimFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .line 705
    .local v0, "trim":Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;
    if-eqz v0, :cond_0

    .line 706
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->updateTrimView()V

    .line 708
    :cond_0
    return-void
.end method

.method public getCurrentState()Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;
    .locals 2

    .prologue
    .line 340
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->HIDDEN:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    .line 341
    .local v0, "s":Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;
    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v1, :cond_0

    .line 342
    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getFragmentControllerState()Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    move-result-object v0

    .line 357
    :cond_0
    return-object v0
.end method

.method public hidePlayer()V
    .locals 6

    .prologue
    .line 589
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->isFinishing()Z

    move-result v4

    if-nez v4, :cond_3

    .line 590
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 591
    .local v1, "fragmentTransaction":Landroid/app/FragmentTransaction;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "VNPlayerControlIdleFragment"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 592
    .local v2, "idlebar":Landroid/app/Fragment;
    if-eqz v2, :cond_0

    .line 593
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 595
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "VNPlyayerToolbarFragment"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .line 596
    .local v3, "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    if-eqz v3, :cond_1

    .line 597
    invoke-virtual {v1, v3}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 599
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "VNPlayerControlButtonFragment"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 600
    .local v0, "controlbar":Landroid/app/Fragment;
    if-eqz v0, :cond_2

    .line 601
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 603
    :cond_2
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 605
    .end local v0    # "controlbar":Landroid/app/Fragment;
    .end local v1    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    .end local v2    # "idlebar":Landroid/app/Fragment;
    .end local v3    # "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    :cond_3
    return-void
.end method

.method public init()V
    .locals 5

    .prologue
    .line 227
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 230
    .local v0, "fragmentTransaction":Landroid/app/FragmentTransaction;
    const v2, 0x7f0e0013

    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-direct {v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;-><init>()V

    const-string v4, "VNLibraryExpandableListFragment"

    invoke-virtual {v0, v2, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 236
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;-><init>()V

    .line 237
    .local v1, "playerIdleFrag":Landroid/app/Fragment;
    const v2, 0x7f0e0017

    const-string v3, "VNPlayerControlIdleFragment"

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 238
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 239
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 242
    :cond_0
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 243
    return-void
.end method

.method public isBookmarkList()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 569
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v4, "VNBookmarkListFragment"

    invoke-virtual {v2, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 570
    .local v1, "fragment":Landroid/app/Fragment;
    if-nez v1, :cond_0

    move v2, v3

    .line 576
    .end local v1    # "fragment":Landroid/app/Fragment;
    :goto_0
    return v2

    .line 573
    .restart local v1    # "fragment":Landroid/app/Fragment;
    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    .line 574
    .end local v1    # "fragment":Landroid/app/Fragment;
    :catch_0
    move-exception v0

    .line 575
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    move v2, v3

    .line 576
    goto :goto_0
.end method

.method public isBookmarkListSelectionMode()Z
    .locals 3

    .prologue
    .line 581
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNBookmarkListFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    .line 582
    .local v0, "fragment":Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    if-nez v0, :cond_0

    .line 583
    const/4 v1, 0x0

    .line 585
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->isSelectionMode()Z

    move-result v1

    goto :goto_0
.end method

.method public isNextItem()Z
    .locals 4

    .prologue
    .line 417
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNLibraryExpandableListFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 418
    .local v1, "listFragment":Landroid/app/Fragment;
    instance-of v2, v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v2, :cond_0

    .line 419
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNLibraryExpandableListFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 420
    .local v0, "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-eqz v0, :cond_0

    .line 421
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isNextItem()Z

    move-result v2

    .line 424
    .end local v0    # "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isPreviousItem()Z
    .locals 4

    .prologue
    .line 428
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNLibraryExpandableListFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 429
    .local v1, "listFragment":Landroid/app/Fragment;
    instance-of v2, v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v2, :cond_0

    .line 430
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNLibraryExpandableListFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 431
    .local v0, "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-eqz v0, :cond_0

    .line 432
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isPreviousItem()Z

    move-result v2

    .line 435
    .end local v0    # "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isRepeateMode()Z
    .locals 3

    .prologue
    .line 481
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNPlyayerToolbarFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .line 482
    .local v0, "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 484
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->isRepeatMode()Z

    move-result v1

    goto :goto_0
.end method

.method public isSTTMode()Z
    .locals 3

    .prologue
    .line 523
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNEditSTTFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 524
    .local v0, "fragment":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public refreashAnim(Z)V
    .locals 3
    .param p1, "needAnim"    # Z

    .prologue
    .line 372
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNLibraryExpandableListFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 373
    .local v0, "Librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-eqz v0, :cond_0

    .line 374
    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->refreashAnim(Z)V

    .line 376
    :cond_0
    return-void
.end method

.method public refreshControlBar()V
    .locals 3

    .prologue
    .line 219
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNPlayerControlButtonFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    .line 220
    .local v0, "controlbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;
    if-nez v0, :cond_0

    .line 224
    :goto_0
    return-void

    .line 223
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->refreshControl()V

    goto :goto_0
.end method

.method public refreshToolBar(Z)V
    .locals 4
    .param p1, "updateBookmark"    # Z

    .prologue
    .line 207
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNPlyayerToolbarFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .line 208
    .local v1, "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    if-nez v1, :cond_0

    .line 209
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 210
    .local v0, "fragmentTransaction":Landroid/app/FragmentTransaction;
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .end local v1    # "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;-><init>()V

    .line 211
    .restart local v1    # "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    const v2, 0x7f0e0016

    const-string v3, "VNPlyayerToolbarFragment"

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 212
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 214
    .end local v0    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    :cond_0
    invoke-virtual {v1, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->refreshToolbar(Z)V

    .line 215
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->updateLibrary(Z)V

    .line 216
    return-void
.end method

.method public removeBookmarkList()V
    .locals 4

    .prologue
    .line 555
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 556
    .local v1, "fragmentTransaction":Landroid/app/FragmentTransaction;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNBookmarkListFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 557
    .local v0, "bookmakrlist":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    .line 558
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 559
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 560
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->popBackStack()V

    .line 561
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_0

    .line 562
    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    sget-object v3, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->PLAYER:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setFragmentControllerState(Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;)V

    .line 565
    :cond_0
    return-void
.end method

.method public removeIdleControl()V
    .locals 4

    .prologue
    .line 193
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 198
    .local v0, "fragmentTransaction":Landroid/app/FragmentTransaction;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNPlayerControlIdleFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 199
    .local v1, "idlebar":Landroid/app/Fragment;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 200
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 201
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method public removePlayer()V
    .locals 11

    .prologue
    .line 122
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->isDestroyed()Z

    move-result v9

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->isFinishing()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 127
    .local v3, "fragmentTransaction":Landroid/app/FragmentTransaction;
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "VNPlyayerToolbarFragment"

    invoke-virtual {v9, v10}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    .line 128
    .local v7, "toolbar":Landroid/app/Fragment;
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "VNPlayerControlButtonFragment"

    invoke-virtual {v9, v10}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 129
    .local v1, "controlbar":Landroid/app/Fragment;
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "VNTrimFragment"

    invoke-virtual {v9, v10}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v8

    .line 130
    .local v8, "trim":Landroid/app/Fragment;
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "VNBookmarkListFragment"

    invoke-virtual {v9, v10}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 132
    .local v0, "bookmarkList":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->getCurrentState()Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->HIDDEN:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    if-ne v9, v10, :cond_2

    .line 133
    if-nez v7, :cond_2

    if-nez v1, :cond_2

    if-nez v8, :cond_2

    if-nez v0, :cond_2

    .line 134
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0

    .line 139
    :cond_2
    if-eqz v7, :cond_3

    .line 140
    invoke-virtual {v3, v7}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 141
    :cond_3
    if-eqz v1, :cond_4

    .line 142
    invoke-virtual {v3, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 143
    :cond_4
    if-eqz v8, :cond_6

    .line 144
    const/4 v9, 0x0

    invoke-static {v9}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setTrimMode(Z)V

    .line 145
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "VNLibraryExpandableListFragment"

    invoke-virtual {v9, v10}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 146
    .local v4, "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-eqz v4, :cond_5

    .line 147
    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->executeTrimMode()V

    .line 149
    :cond_5
    if-eqz v8, :cond_6

    .line 150
    invoke-virtual {v3, v8}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 154
    .end local v4    # "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :cond_6
    if-eqz v0, :cond_7

    .line 155
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/FragmentManager;->popBackStack()V

    .line 157
    :cond_7
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "VNEditSTTFragment"

    invoke-virtual {v9, v10}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 158
    .local v2, "frag_stt":Landroid/app/Fragment;
    if-eqz v2, :cond_8

    .line 159
    invoke-virtual {v3, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 160
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "VNLibraryExpandableListFragment"

    invoke-virtual {v9, v10}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 161
    .local v5, "list":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-eqz v5, :cond_8

    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setEnableListView(Z)V

    .line 164
    .end local v5    # "list":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :cond_8
    const/4 v6, 0x0

    .line 165
    .local v6, "playerIdleFrag":Landroid/app/Fragment;
    iget-boolean v9, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->bSearchOn:Z

    if-nez v9, :cond_9

    .line 166
    new-instance v6, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;

    .end local v6    # "playerIdleFrag":Landroid/app/Fragment;
    invoke-direct {v6}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;-><init>()V

    .line 167
    .restart local v6    # "playerIdleFrag":Landroid/app/Fragment;
    const v9, 0x7f0e0017

    const-string v10, "VNPlayerControlIdleFragment"

    invoke-virtual {v3, v9, v6, v10}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 169
    :cond_9
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v9

    if-eqz v9, :cond_a

    if-eqz v6, :cond_a

    .line 170
    invoke-virtual {v3, v6}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 172
    :cond_a
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 173
    sget-object v9, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_0

    .line 174
    sget-object v9, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    sget-object v10, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->HIDDEN:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    invoke-virtual {v9, v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setFragmentControllerState(Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;)V

    goto/16 :goto_0
.end method

.method public removeTrim(Z)V
    .locals 7
    .param p1, "bCancel"    # Z

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->getCurrentState()Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->HIDDEN:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    if-ne v4, v5, :cond_1

    .line 330
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    if-nez p1, :cond_2

    .line 304
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->updateSttFragment()V

    .line 306
    :cond_2
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setTrimMode(Z)V

    .line 308
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 309
    .local v1, "fragmentTransaction":Landroid/app/FragmentTransaction;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "VNTrimFragment"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    .line 310
    .local v3, "trimlbar":Landroid/app/Fragment;
    if-eqz v3, :cond_3

    .line 311
    invoke-virtual {v1, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 313
    :cond_3
    const v4, 0x7f0e0016

    new-instance v5, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-direct {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;-><init>()V

    const-string v6, "VNPlyayerToolbarFragment"

    invoke-virtual {v1, v4, v5, v6}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 314
    const v4, 0x7f0e0017

    new-instance v5, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    invoke-direct {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;-><init>()V

    const-string v6, "VNPlayerControlButtonFragment"

    invoke-virtual {v1, v4, v5, v6}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 315
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 317
    const/4 v2, 0x0

    .line 319
    .local v2, "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "VNLibraryExpandableListFragment"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 320
    .local v0, "frag":Landroid/app/Fragment;
    instance-of v4, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v4, :cond_4

    move-object v2, v0

    .line 321
    check-cast v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 324
    :cond_4
    if-eqz v2, :cond_5

    .line 325
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->executeTrimMode()V

    .line 327
    :cond_5
    sget-object v4, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v4, :cond_0

    .line 328
    sget-object v4, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    sget-object v5, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->PLAYER:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setFragmentControllerState(Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;)V

    goto :goto_0
.end method

.method public resetRepeatMode()V
    .locals 4

    .prologue
    .line 719
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNPlyayerToolbarFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .line 720
    .local v1, "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    if-nez v1, :cond_0

    .line 721
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 722
    .local v0, "fragmentTransaction":Landroid/app/FragmentTransaction;
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .end local v1    # "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;-><init>()V

    .line 723
    .restart local v1    # "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    const v2, 0x7f0e0016

    const-string v3, "VNPlyayerToolbarFragment"

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 724
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 726
    .end local v0    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->resetRepeatMode()V

    .line 727
    return-void
.end method

.method public searchLibrary(Ljava/lang/String;)V
    .locals 3
    .param p1, "searchText"    # Ljava/lang/String;

    .prologue
    .line 644
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNLibraryExpandableListFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 645
    .local v0, "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-eqz p1, :cond_0

    .line 646
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->removePlayer()V

    .line 648
    :cond_0
    if-eqz v0, :cond_1

    .line 649
    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setSearchText(Ljava/lang/String;)V

    .line 650
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->updateState(Z)V

    .line 653
    :cond_1
    return-void
.end method

.method public selectBookmark(II)V
    .locals 3
    .param p1, "idxNearByCurPos"    # I
    .param p2, "optionId"    # I

    .prologue
    .line 711
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNBookmarkListFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    .line 713
    .local v0, "bl":Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 714
    invoke-virtual {v0, p2, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->onMenuItemClick(II)Z

    .line 716
    :cond_0
    return-void
.end method

.method public setActionModehOn(Z)V
    .locals 0
    .param p1, "bOn"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->bActionModeOn:Z

    .line 93
    return-void
.end method

.method public setProgress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 630
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNPlyayerToolbarFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .line 631
    .local v0, "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    if-eqz v0, :cond_0

    .line 632
    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setProgress(I)V

    .line 634
    :cond_0
    return-void
.end method

.method public setSearchOn(Z)V
    .locals 0
    .param p1, "bOn"    # Z

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->bSearchOn:Z

    .line 89
    return-void
.end method

.method public showPlayer()V
    .locals 6

    .prologue
    .line 608
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->isFinishing()Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->isDestroyed()Z

    move-result v4

    if-nez v4, :cond_4

    .line 609
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 610
    .local v1, "fragmentTransaction":Landroid/app/FragmentTransaction;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "VNPlayerControlIdleFragment"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 611
    .local v2, "idlebar":Landroid/app/Fragment;
    if-eqz v2, :cond_0

    .line 612
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 614
    :cond_0
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v2, :cond_1

    .line 615
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 617
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "VNPlyayerToolbarFragment"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .line 618
    .local v3, "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    if-eqz v3, :cond_2

    .line 619
    invoke-virtual {v1, v3}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 621
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "VNPlayerControlButtonFragment"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 622
    .local v0, "controlbar":Landroid/app/Fragment;
    if-eqz v0, :cond_3

    .line 623
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 625
    :cond_3
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 627
    .end local v0    # "controlbar":Landroid/app/Fragment;
    .end local v1    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    .end local v2    # "idlebar":Landroid/app/Fragment;
    .end local v3    # "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    :cond_4
    return-void
.end method

.method public startProgess()V
    .locals 3

    .prologue
    .line 246
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNPlyayerToolbarFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .line 247
    .local v0, "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    if-nez v0, :cond_0

    .line 249
    :goto_0
    return-void

    .line 248
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->startProgress()V

    goto :goto_0
.end method

.method public startRepeatModeTrim()V
    .locals 3

    .prologue
    .line 488
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNPlyayerToolbarFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .line 489
    .local v0, "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    if-nez v0, :cond_0

    .line 498
    :goto_0
    return-void

    .line 491
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->isBookmarkList()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 492
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 494
    :cond_1
    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v1, :cond_2

    .line 495
    sget-object v1, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->PLAYER:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setFragmentControllerState(Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;)V

    .line 497
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->startRepeatModeTrim()V

    goto :goto_0
.end method

.method public stopProgress()V
    .locals 3

    .prologue
    .line 252
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNPlyayerToolbarFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .line 253
    .local v0, "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    if-nez v0, :cond_0

    .line 255
    :goto_0
    return-void

    .line 254
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->stopProgress()V

    goto :goto_0
.end method

.method public stopTrimPlay()V
    .locals 3

    .prologue
    .line 333
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNTrimFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .line 334
    .local v0, "trim":Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;
    if-eqz v0, :cond_0

    .line 335
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->stopPlay()V

    .line 337
    :cond_0
    return-void
.end method

.method public switchSTT(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 502
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_1

    .line 503
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 504
    .local v1, "fragmentTransaction":Landroid/app/FragmentTransaction;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "VNEditSTTFragment"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 505
    .local v0, "fragment":Landroid/app/Fragment;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "VNLibraryExpandableListFragment"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 507
    .local v2, "list":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-nez v0, :cond_2

    .line 508
    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->setBookmark(Ljava/util/ArrayList;)V

    .line 509
    const v3, 0x7f0e0014

    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-direct {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;-><init>()V

    const-string v5, "VNEditSTTFragment"

    invoke-virtual {v1, v3, v4, v5}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 510
    if-eqz v2, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setEnableListView(Z)V

    .line 517
    :cond_0
    :goto_0
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 518
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 520
    .end local v0    # "fragment":Landroid/app/Fragment;
    .end local v1    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    .end local v2    # "list":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :cond_1
    return-void

    .line 512
    .restart local v0    # "fragment":Landroid/app/Fragment;
    .restart local v1    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    .restart local v2    # "list":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :cond_2
    if-eqz v0, :cond_3

    .line 513
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 515
    :cond_3
    if-eqz v2, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->setEnableListView(Z)V

    goto :goto_0
.end method

.method public updateBookmarkLibrary(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 4
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 455
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNLibraryExpandableListFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 456
    .local v0, "fragment":Landroid/app/Fragment;
    instance-of v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 457
    check-cast v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 458
    .local v1, "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 459
    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->updateCachedBookmarks(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 462
    .end local v1    # "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :cond_0
    return-void
.end method

.method public updateBookmarkList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 465
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNBookmarkListFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    .line 466
    .local v0, "bl":Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 467
    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->updateBookmarks(Ljava/util/ArrayList;)V

    .line 469
    :cond_0
    return-void
.end method

.method public updateBookmarks(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 0
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 448
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->updateBookmarksPlayer(Ljava/util/ArrayList;)V

    .line 449
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->updateBookmarkList(Ljava/util/ArrayList;)V

    .line 450
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->updateBookmarksOnSttView(Ljava/util/ArrayList;)V

    .line 451
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->updateBookmarkLibrary(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 452
    return-void
.end method

.method public updateBookmarksOnSttView(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 472
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNEditSTTFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .line 473
    .local v0, "fragment":Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
    if-eqz v0, :cond_0

    .line 474
    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->updateBoookmarks(Ljava/util/ArrayList;)V

    .line 478
    :goto_0
    return-void

    .line 476
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->setBookmark(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public updateBookmarksPlayer(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 439
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->getCurrentState()Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->PLAYER:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->getCurrentState()Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->BOOKMARK:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    if-ne v1, v2, :cond_1

    .line 440
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNPlyayerToolbarFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    .line 441
    .local v0, "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    if-nez v0, :cond_2

    .line 444
    .end local v0    # "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    :cond_1
    :goto_0
    return-void

    .line 442
    .restart local v0    # "toolbar":Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    :cond_2
    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->updateBookmarks(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public updateLibrary(Z)V
    .locals 4
    .param p1, "bRequeryCursor"    # Z

    .prologue
    .line 361
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "VNLibraryExpandableListFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 362
    .local v0, "fragment":Landroid/app/Fragment;
    instance-of v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 363
    check-cast v1, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 364
    .local v1, "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    if-eqz v1, :cond_0

    .line 365
    invoke-virtual {v1, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->updateState(Z)V

    .line 369
    .end local v1    # "librarylist":Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
    :cond_0
    return-void
.end method

.method public updateSttFragment()V
    .locals 3

    .prologue
    .line 528
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "VNEditSTTFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .line 529
    .local v0, "fragment":Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
    if-eqz v0, :cond_0

    .line 530
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->initList()V

    .line 531
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->updateAllWordOnScreen()V

    .line 533
    :cond_0
    return-void
.end method
