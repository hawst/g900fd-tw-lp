.class public Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;
.super Ljava/lang/Object;
.source "MediaDecoder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;
    }
.end annotation


# static fields
.field private static final AUDIO:Ljava/lang/String; = "audio"

.field private static final RECORDER_AUDIO_ENCODING:I = 0x2

.field private static final RECORDER_BPP:I = 0x10

.field private static final RECORDER_CHANNELS:I = 0xc

.field private static final RECORDER_SAMPLERATE:I = 0x3e80

.field private static final TAG:Ljava/lang/String; = "MediaDecoder"


# instance fields
.field private bufferSize:I

.field private mOnDecodeFinishedListener:Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 290
    return-void
.end method

.method private WriteWaveFileHeader(Ljava/io/FileOutputStream;JJJIJ)V
    .locals 6
    .param p1, "out"    # Ljava/io/FileOutputStream;
    .param p2, "totalAudioLen"    # J
    .param p4, "totalDataLen"    # J
    .param p6, "longSampleRate"    # J
    .param p8, "channels"    # I
    .param p9, "byteRate"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 240
    const/16 v1, 0x2c

    new-array v0, v1, [B

    .line 242
    .local v0, "header":[B
    const/4 v1, 0x0

    const/16 v2, 0x52

    aput-byte v2, v0, v1

    .line 243
    const/4 v1, 0x1

    const/16 v2, 0x49

    aput-byte v2, v0, v1

    .line 244
    const/4 v1, 0x2

    const/16 v2, 0x46

    aput-byte v2, v0, v1

    .line 245
    const/4 v1, 0x3

    const/16 v2, 0x46

    aput-byte v2, v0, v1

    .line 246
    const/4 v1, 0x4

    const-wide/16 v2, 0xff

    and-long/2addr v2, p4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 247
    const/4 v1, 0x5

    const/16 v2, 0x8

    shr-long v2, p4, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 248
    const/4 v1, 0x6

    const/16 v2, 0x10

    shr-long v2, p4, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 249
    const/4 v1, 0x7

    const/16 v2, 0x18

    shr-long v2, p4, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 250
    const/16 v1, 0x8

    const/16 v2, 0x57

    aput-byte v2, v0, v1

    .line 251
    const/16 v1, 0x9

    const/16 v2, 0x41

    aput-byte v2, v0, v1

    .line 252
    const/16 v1, 0xa

    const/16 v2, 0x56

    aput-byte v2, v0, v1

    .line 253
    const/16 v1, 0xb

    const/16 v2, 0x45

    aput-byte v2, v0, v1

    .line 254
    const/16 v1, 0xc

    const/16 v2, 0x66

    aput-byte v2, v0, v1

    .line 255
    const/16 v1, 0xd

    const/16 v2, 0x6d

    aput-byte v2, v0, v1

    .line 256
    const/16 v1, 0xe

    const/16 v2, 0x74

    aput-byte v2, v0, v1

    .line 257
    const/16 v1, 0xf

    const/16 v2, 0x20

    aput-byte v2, v0, v1

    .line 258
    const/16 v1, 0x10

    const/16 v2, 0x10

    aput-byte v2, v0, v1

    .line 259
    const/16 v1, 0x11

    const/4 v2, 0x0

    aput-byte v2, v0, v1

    .line 260
    const/16 v1, 0x12

    const/4 v2, 0x0

    aput-byte v2, v0, v1

    .line 261
    const/16 v1, 0x13

    const/4 v2, 0x0

    aput-byte v2, v0, v1

    .line 262
    const/16 v1, 0x14

    const/4 v2, 0x1

    aput-byte v2, v0, v1

    .line 263
    const/16 v1, 0x15

    const/4 v2, 0x0

    aput-byte v2, v0, v1

    .line 264
    const/16 v1, 0x16

    int-to-byte v2, p8

    aput-byte v2, v0, v1

    .line 265
    const/16 v1, 0x17

    const/4 v2, 0x0

    aput-byte v2, v0, v1

    .line 266
    const/16 v1, 0x18

    const-wide/16 v2, 0xff

    and-long/2addr v2, p6

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 267
    const/16 v1, 0x19

    const/16 v2, 0x8

    shr-long v2, p6, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 268
    const/16 v1, 0x1a

    const/16 v2, 0x10

    shr-long v2, p6, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 269
    const/16 v1, 0x1b

    const/16 v2, 0x18

    shr-long v2, p6, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 270
    const/16 v1, 0x1c

    const-wide/16 v2, 0xff

    and-long/2addr v2, p9

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 271
    const/16 v1, 0x1d

    const/16 v2, 0x8

    shr-long v2, p9, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 272
    const/16 v1, 0x1e

    const/16 v2, 0x10

    shr-long v2, p9, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 273
    const/16 v1, 0x1f

    const/16 v2, 0x18

    shr-long v2, p9, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 274
    const/16 v1, 0x20

    const/4 v2, 0x4

    aput-byte v2, v0, v1

    .line 275
    const/16 v1, 0x21

    const/4 v2, 0x0

    aput-byte v2, v0, v1

    .line 276
    const/16 v1, 0x22

    const/16 v2, 0x10

    aput-byte v2, v0, v1

    .line 277
    const/16 v1, 0x23

    const/4 v2, 0x0

    aput-byte v2, v0, v1

    .line 278
    const/16 v1, 0x24

    const/16 v2, 0x64

    aput-byte v2, v0, v1

    .line 279
    const/16 v1, 0x25

    const/16 v2, 0x61

    aput-byte v2, v0, v1

    .line 280
    const/16 v1, 0x26

    const/16 v2, 0x74

    aput-byte v2, v0, v1

    .line 281
    const/16 v1, 0x27

    const/16 v2, 0x61

    aput-byte v2, v0, v1

    .line 282
    const/16 v1, 0x28

    const-wide/16 v2, 0xff

    and-long/2addr v2, p2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 283
    const/16 v1, 0x29

    const/16 v2, 0x8

    shr-long v2, p2, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 284
    const/16 v1, 0x2a

    const/16 v2, 0x10

    shr-long v2, p2, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 285
    const/16 v1, 0x2b

    const/16 v2, 0x18

    shr-long v2, p2, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 287
    const/4 v1, 0x0

    const/16 v2, 0x2c

    invoke-virtual {p1, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 288
    return-void
.end method


# virtual methods
.method public copyWaveFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 20
    .param p1, "inFilename"    # Ljava/lang/String;
    .param p2, "outFilename"    # Ljava/lang/String;

    .prologue
    .line 204
    const/4 v15, 0x0

    .line 205
    .local v15, "in":Ljava/io/FileInputStream;
    const/16 v17, 0x0

    .line 206
    .local v17, "out":Ljava/io/FileOutputStream;
    const-wide/16 v4, 0x0

    .line 207
    .local v4, "totalAudioLen":J
    const-wide/16 v18, 0x24

    add-long v6, v4, v18

    .line 208
    .local v6, "totalDataLen":J
    const-wide/16 v8, 0x3e80

    .line 209
    .local v8, "longSampleRate":J
    const/4 v10, 0x2

    .line 210
    .local v10, "channels":I
    const v2, 0xfa00

    int-to-long v11, v2

    .line 212
    .local v11, "byteRate":J
    const/16 v2, 0x3e80

    const/16 v18, 0xc

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v2, v0, v1}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->bufferSize:I

    .line 213
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->bufferSize:I

    new-array v13, v2, [B

    .line 216
    .local v13, "data":[B
    :try_start_0
    new-instance v16, Ljava/io/FileInputStream;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 217
    .end local v15    # "in":Ljava/io/FileInputStream;
    .local v16, "in":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 218
    .end local v17    # "out":Ljava/io/FileOutputStream;
    .local v3, "out":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    .line 219
    const-wide/16 v18, 0x24

    add-long v6, v4, v18

    move-object/from16 v2, p0

    .line 223
    invoke-direct/range {v2 .. v12}, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->WriteWaveFileHeader(Ljava/io/FileOutputStream;JJJIJ)V

    .line 225
    :goto_0
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/io/FileInputStream;->read([B)I

    move-result v2

    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v2, v0, :cond_0

    .line 226
    invoke-virtual {v3, v13}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 231
    :catch_0
    move-exception v14

    move-object/from16 v15, v16

    .line 232
    .end local v16    # "in":Ljava/io/FileInputStream;
    .local v14, "e":Ljava/io/FileNotFoundException;
    .restart local v15    # "in":Ljava/io/FileInputStream;
    :goto_1
    invoke-virtual {v14}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 236
    .end local v14    # "e":Ljava/io/FileNotFoundException;
    :goto_2
    return-void

    .line 229
    .end local v15    # "in":Ljava/io/FileInputStream;
    .restart local v16    # "in":Ljava/io/FileInputStream;
    :cond_0
    :try_start_3
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V

    .line 230
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move-object/from16 v15, v16

    .line 235
    .end local v16    # "in":Ljava/io/FileInputStream;
    .restart local v15    # "in":Ljava/io/FileInputStream;
    goto :goto_2

    .line 233
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v17    # "out":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v14

    move-object/from16 v3, v17

    .line 234
    .end local v17    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    .local v14, "e":Ljava/io/IOException;
    :goto_3
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 233
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .end local v14    # "e":Ljava/io/IOException;
    .end local v15    # "in":Ljava/io/FileInputStream;
    .restart local v16    # "in":Ljava/io/FileInputStream;
    .restart local v17    # "out":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v14

    move-object/from16 v3, v17

    .end local v17    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    move-object/from16 v15, v16

    .end local v16    # "in":Ljava/io/FileInputStream;
    .restart local v15    # "in":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v15    # "in":Ljava/io/FileInputStream;
    .restart local v16    # "in":Ljava/io/FileInputStream;
    :catch_3
    move-exception v14

    move-object/from16 v15, v16

    .end local v16    # "in":Ljava/io/FileInputStream;
    .restart local v15    # "in":Ljava/io/FileInputStream;
    goto :goto_3

    .line 231
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v17    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v14

    move-object/from16 v3, v17

    .end local v17    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    goto :goto_1

    .end local v3    # "out":Ljava/io/FileOutputStream;
    .end local v15    # "in":Ljava/io/FileInputStream;
    .restart local v16    # "in":Ljava/io/FileInputStream;
    .restart local v17    # "out":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v14

    move-object/from16 v3, v17

    .end local v17    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    move-object/from16 v15, v16

    .end local v16    # "in":Ljava/io/FileInputStream;
    .restart local v15    # "in":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public declared-synchronized decode(Ljava/lang/String;)V
    .locals 41
    .param p1, "srcPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    monitor-enter p0

    const/16 v36, 0x0

    .line 37
    .local v36, "pcmPath":Ljava/lang/String;
    :try_start_0
    const-string v4, "MediaDecoder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "srcPath :"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/voicenote/library/stt/Utils;->getOutputPCMFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 39
    const-string v4, "MediaDecoder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "decode pcmPath:"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v36

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    new-instance v24, Ljava/io/File;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 42
    .local v24, "file":Ljava/io/File;
    new-instance v38, Ljava/io/FileInputStream;

    move-object/from16 v0, v38

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    .local v38, "srcfileInputStream":Ljava/io/FileInputStream;
    const/16 v25, 0x0

    .line 46
    .local v25, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->length()J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-wide v6

    .line 47
    .local v6, "srcLength":J
    const/4 v3, 0x0

    .line 50
    .local v3, "fd":Ljava/io/FileDescriptor;
    :try_start_2
    invoke-virtual/range {v38 .. v38}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-result-object v3

    .line 56
    :try_start_3
    new-instance v2, Landroid/media/MediaExtractor;

    invoke-direct {v2}, Landroid/media/MediaExtractor;-><init>()V

    .line 58
    .local v2, "extractor":Landroid/media/MediaExtractor;
    const-wide/16 v4, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/media/MediaExtractor;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 60
    const/16 v40, -0x1

    .line 61
    .local v40, "trackIndex":I
    const/16 v27, 0x0

    .line 62
    .local v27, "format":Landroid/media/MediaFormat;
    const/16 v31, 0x0

    .line 63
    .local v31, "mime":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/media/MediaExtractor;->getTrackCount()I

    move-result v39

    .line 65
    .local v39, "trackCount":I
    const/16 v29, 0x0

    .local v29, "i":I
    :goto_0
    move/from16 v0, v29

    move/from16 v1, v39

    if-ge v0, v1, :cond_0

    .line 66
    move/from16 v0, v29

    invoke-virtual {v2, v0}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v27

    .line 67
    const-string v4, "mime"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 68
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "audio"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 69
    move/from16 v40, v29

    .line 74
    :cond_0
    const/4 v4, -0x1

    move/from16 v0, v40

    if-ne v0, v4, :cond_3

    .line 75
    const-string v4, "MediaDecoder"

    const-string v5, "invalid trackIndex"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 181
    :try_start_4
    invoke-virtual/range {v25 .. v25}, Ljava/io/FileOutputStream;->close()V

    .line 182
    const/16 v25, 0x0

    .line 183
    invoke-virtual/range {v38 .. v38}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 191
    :goto_1
    :try_start_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->mOnDecodeFinishedListener:Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;

    if-eqz v4, :cond_1

    .line 192
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->mOnDecodeFinishedListener:Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;

    move-object/from16 v0, v36

    invoke-interface {v4, v0}, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;->onFinished(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 195
    .end local v2    # "extractor":Landroid/media/MediaExtractor;
    .end local v3    # "fd":Ljava/io/FileDescriptor;
    .end local v6    # "srcLength":J
    .end local v27    # "format":Landroid/media/MediaFormat;
    .end local v29    # "i":I
    .end local v31    # "mime":Ljava/lang/String;
    .end local v39    # "trackCount":I
    .end local v40    # "trackIndex":I
    :cond_1
    :goto_2
    monitor-exit p0

    return-void

    .line 51
    .restart local v3    # "fd":Ljava/io/FileDescriptor;
    .restart local v6    # "srcLength":J
    :catch_0
    move-exception v20

    .line 52
    .local v20, "e":Ljava/io/IOException;
    :try_start_6
    invoke-virtual/range {v20 .. v20}, Ljava/io/IOException;->printStackTrace()V

    .line 53
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    throw v4
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 176
    .end local v3    # "fd":Ljava/io/FileDescriptor;
    .end local v6    # "srcLength":J
    .end local v20    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v20

    .line 177
    .local v20, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_7
    const-string v4, "MediaDecoder"

    const-string v5, "Exception occurred"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 181
    :try_start_8
    invoke-virtual/range {v25 .. v25}, Ljava/io/FileOutputStream;->close()V

    .line 182
    const/16 v25, 0x0

    .line 183
    invoke-virtual/range {v38 .. v38}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_8
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_9
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 191
    .end local v20    # "e":Ljava/lang/Exception;
    :goto_4
    :try_start_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->mOnDecodeFinishedListener:Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;

    if-eqz v4, :cond_1

    .line 192
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->mOnDecodeFinishedListener:Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;

    move-object/from16 v0, v36

    invoke-interface {v4, v0}, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;->onFinished(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_2

    .line 35
    .end local v24    # "file":Ljava/io/File;
    .end local v25    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v38    # "srcfileInputStream":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 65
    .restart local v2    # "extractor":Landroid/media/MediaExtractor;
    .restart local v3    # "fd":Ljava/io/FileDescriptor;
    .restart local v6    # "srcLength":J
    .restart local v24    # "file":Ljava/io/File;
    .restart local v25    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v27    # "format":Landroid/media/MediaFormat;
    .restart local v29    # "i":I
    .restart local v31    # "mime":Ljava/lang/String;
    .restart local v38    # "srcfileInputStream":Ljava/io/FileInputStream;
    .restart local v39    # "trackCount":I
    .restart local v40    # "trackIndex":I
    :cond_2
    add-int/lit8 v29, v29, 0x1

    goto :goto_0

    .line 184
    :catch_2
    move-exception v20

    .line 185
    .local v20, "e":Ljava/io/IOException;
    :try_start_a
    invoke-virtual/range {v20 .. v20}, Ljava/io/IOException;->printStackTrace()V

    .line 186
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    throw v4

    .line 187
    .end local v20    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v20

    .line 188
    .local v20, "e":Ljava/lang/NullPointerException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_1

    .line 79
    .end local v20    # "e":Ljava/lang/NullPointerException;
    :cond_3
    :try_start_b
    invoke-static/range {v31 .. v31}, Landroid/media/MediaCodec;->createDecoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v8

    .line 80
    .local v8, "codec":Landroid/media/MediaCodec;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v8, v0, v4, v5, v10}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 81
    invoke-virtual {v8}, Landroid/media/MediaCodec;->start()V

    .line 83
    invoke-virtual {v8}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v17

    .line 84
    .local v17, "codecInputBuffers":[Ljava/nio/ByteBuffer;
    invoke-virtual {v8}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v18

    .line 86
    .local v18, "codecOutputBuffers":[Ljava/nio/ByteBuffer;
    move/from16 v0, v40

    invoke-virtual {v2, v0}, Landroid/media/MediaExtractor;->selectTrack(I)V

    .line 89
    const-wide/16 v32, 0x1388

    .line 90
    .local v32, "kTimeOutUs":J
    new-instance v30, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct/range {v30 .. v30}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 91
    .local v30, "info":Landroid/media/MediaCodec$BufferInfo;
    const/16 v21, 0x0

    .line 92
    .local v21, "endOfInputStream":Z
    const/16 v22, 0x0

    .line 94
    .local v22, "endOfOutputStream":Z
    new-instance v26, Ljava/io/FileOutputStream;

    const/4 v4, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, v36

    invoke-direct {v0, v1, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 96
    .end local v25    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v26, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_c
    invoke-virtual/range {v26 .. v26}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v23

    .line 98
    .local v23, "fdOut":Ljava/io/FileDescriptor;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fdOut :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v23 .. v23}, Ljava/io/FileDescriptor;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;)V

    .line 100
    :cond_4
    :goto_5
    if-nez v22, :cond_d

    .line 102
    if-nez v21, :cond_5

    .line 103
    const-wide/16 v4, 0x1388

    invoke-virtual {v8, v4, v5}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v9

    .line 105
    .local v9, "inputBufIndex":I
    if-ltz v9, :cond_5

    .line 106
    aget-object v19, v17, v9

    .line 107
    .local v19, "dstBuf":Ljava/nio/ByteBuffer;
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v4}, Landroid/media/MediaExtractor;->readSampleData(Ljava/nio/ByteBuffer;I)I

    move-result v11

    .line 108
    .local v11, "sampleSize":I
    const-wide/16 v12, 0x0

    .line 110
    .local v12, "presentationTimeUs":J
    if-gez v11, :cond_7

    .line 111
    const-string v4, "MediaDecoder"

    const-string v5, "input sample size < 0"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const/16 v21, 0x1

    .line 113
    const/4 v11, 0x0

    .line 118
    :goto_6
    const/4 v10, 0x0

    if-eqz v21, :cond_8

    const/4 v14, 0x4

    :goto_7
    invoke-virtual/range {v8 .. v14}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 121
    if-nez v21, :cond_5

    .line 122
    invoke-virtual {v2}, Landroid/media/MediaExtractor;->advance()Z

    .line 128
    .end local v9    # "inputBufIndex":I
    .end local v11    # "sampleSize":I
    .end local v12    # "presentationTimeUs":J
    .end local v19    # "dstBuf":Ljava/nio/ByteBuffer;
    :cond_5
    const-wide/16 v4, 0x1388

    move-object/from16 v0, v30

    invoke-virtual {v8, v0, v4, v5}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v37

    .line 130
    .local v37, "res":I
    if-ltz v37, :cond_a

    .line 131
    move/from16 v34, v37

    .line 132
    .local v34, "outputBufIndex":I
    aget-object v15, v18, v34

    .line 134
    .local v15, "buf":Ljava/nio/ByteBuffer;
    move-object/from16 v0, v30

    iget v4, v0, Landroid/media/MediaCodec$BufferInfo;->size:I

    if-lez v4, :cond_6

    .line 135
    move-object/from16 v0, v30

    iget v4, v0, Landroid/media/MediaCodec$BufferInfo;->size:I

    new-array v0, v4, [B

    move-object/from16 v16, v0

    .line 137
    .local v16, "buffer":[B
    const/4 v4, 0x0

    invoke-virtual {v15, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 138
    const/4 v4, 0x0

    move-object/from16 v0, v30

    iget v5, v0, Landroid/media/MediaCodec$BufferInfo;->size:I

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v4, v5}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 139
    new-instance v28, Ljava/io/FileOutputStream;

    move-object/from16 v0, v28

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 141
    .local v28, "fos":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    :try_start_d
    move-object/from16 v0, v30

    iget v5, v0, Landroid/media/MediaCodec$BufferInfo;->size:I

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v4, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 146
    if-eqz v28, :cond_6

    .line 147
    :try_start_e
    invoke-virtual/range {v28 .. v28}, Ljava/io/FileOutputStream;->close()V

    .line 152
    .end local v16    # "buffer":[B
    .end local v28    # "fos":Ljava/io/FileOutputStream;
    :cond_6
    const/4 v4, 0x0

    move/from16 v0, v34

    invoke-virtual {v8, v0, v4}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 154
    move-object/from16 v0, v30

    iget v4, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_4

    .line 155
    const-string v4, "MediaDecoder"

    const-string v5, "output stream size < 0"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const/16 v22, 0x1

    goto/16 :goto_5

    .line 115
    .end local v15    # "buf":Ljava/nio/ByteBuffer;
    .end local v34    # "outputBufIndex":I
    .end local v37    # "res":I
    .restart local v9    # "inputBufIndex":I
    .restart local v11    # "sampleSize":I
    .restart local v12    # "presentationTimeUs":J
    .restart local v19    # "dstBuf":Ljava/nio/ByteBuffer;
    :cond_7
    invoke-virtual {v2}, Landroid/media/MediaExtractor;->getSampleTime()J
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_5
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    move-result-wide v12

    goto :goto_6

    .line 118
    :cond_8
    const/4 v14, 0x0

    goto :goto_7

    .line 142
    .end local v9    # "inputBufIndex":I
    .end local v11    # "sampleSize":I
    .end local v12    # "presentationTimeUs":J
    .end local v19    # "dstBuf":Ljava/nio/ByteBuffer;
    .restart local v15    # "buf":Ljava/nio/ByteBuffer;
    .restart local v16    # "buffer":[B
    .restart local v28    # "fos":Ljava/io/FileOutputStream;
    .restart local v34    # "outputBufIndex":I
    .restart local v37    # "res":I
    :catch_4
    move-exception v20

    .line 143
    .local v20, "e":Ljava/io/IOException;
    :try_start_f
    invoke-virtual/range {v20 .. v20}, Ljava/io/IOException;->printStackTrace()V

    .line 144
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    throw v4
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 146
    .end local v20    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v4

    if-eqz v28, :cond_9

    .line 147
    :try_start_10
    invoke-virtual/range {v28 .. v28}, Ljava/io/FileOutputStream;->close()V

    :cond_9
    throw v4

    .line 176
    .end local v15    # "buf":Ljava/nio/ByteBuffer;
    .end local v16    # "buffer":[B
    .end local v23    # "fdOut":Ljava/io/FileDescriptor;
    .end local v28    # "fos":Ljava/io/FileOutputStream;
    .end local v34    # "outputBufIndex":I
    .end local v37    # "res":I
    :catch_5
    move-exception v20

    move-object/from16 v25, v26

    .end local v26    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v25    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_3

    .line 158
    .end local v25    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v23    # "fdOut":Ljava/io/FileDescriptor;
    .restart local v26    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v37    # "res":I
    :cond_a
    const/4 v4, -0x3

    move/from16 v0, v37

    if-ne v0, v4, :cond_c

    .line 159
    invoke-virtual {v8}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v18

    .line 161
    const-string v4, "MediaDecoder"

    const-string v5, "INFO_OUTPUT_BUFFERS_CHANGED"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_5
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    goto/16 :goto_5

    .line 180
    .end local v23    # "fdOut":Ljava/io/FileDescriptor;
    .end local v37    # "res":I
    :catchall_2
    move-exception v4

    move-object/from16 v25, v26

    .line 181
    .end local v2    # "extractor":Landroid/media/MediaExtractor;
    .end local v3    # "fd":Ljava/io/FileDescriptor;
    .end local v6    # "srcLength":J
    .end local v8    # "codec":Landroid/media/MediaCodec;
    .end local v17    # "codecInputBuffers":[Ljava/nio/ByteBuffer;
    .end local v18    # "codecOutputBuffers":[Ljava/nio/ByteBuffer;
    .end local v21    # "endOfInputStream":Z
    .end local v22    # "endOfOutputStream":Z
    .end local v26    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v27    # "format":Landroid/media/MediaFormat;
    .end local v29    # "i":I
    .end local v30    # "info":Landroid/media/MediaCodec$BufferInfo;
    .end local v31    # "mime":Ljava/lang/String;
    .end local v32    # "kTimeOutUs":J
    .end local v39    # "trackCount":I
    .end local v40    # "trackIndex":I
    .restart local v25    # "fileOutputStream":Ljava/io/FileOutputStream;
    :goto_8
    :try_start_11
    invoke-virtual/range {v25 .. v25}, Ljava/io/FileOutputStream;->close()V

    .line 182
    const/16 v25, 0x0

    .line 183
    invoke-virtual/range {v38 .. v38}, Ljava/io/FileInputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a
    .catch Ljava/lang/NullPointerException; {:try_start_11 .. :try_end_11} :catch_b
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 191
    :goto_9
    :try_start_12
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->mOnDecodeFinishedListener:Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;

    if-eqz v5, :cond_b

    .line 192
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->mOnDecodeFinishedListener:Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;

    move-object/from16 v0, v36

    invoke-interface {v5, v0}, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;->onFinished(Ljava/lang/String;)V

    :cond_b
    throw v4
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 162
    .end local v25    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v2    # "extractor":Landroid/media/MediaExtractor;
    .restart local v3    # "fd":Ljava/io/FileDescriptor;
    .restart local v6    # "srcLength":J
    .restart local v8    # "codec":Landroid/media/MediaCodec;
    .restart local v17    # "codecInputBuffers":[Ljava/nio/ByteBuffer;
    .restart local v18    # "codecOutputBuffers":[Ljava/nio/ByteBuffer;
    .restart local v21    # "endOfInputStream":Z
    .restart local v22    # "endOfOutputStream":Z
    .restart local v23    # "fdOut":Ljava/io/FileDescriptor;
    .restart local v26    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v27    # "format":Landroid/media/MediaFormat;
    .restart local v29    # "i":I
    .restart local v30    # "info":Landroid/media/MediaCodec$BufferInfo;
    .restart local v31    # "mime":Ljava/lang/String;
    .restart local v32    # "kTimeOutUs":J
    .restart local v37    # "res":I
    .restart local v39    # "trackCount":I
    .restart local v40    # "trackIndex":I
    :cond_c
    const/4 v4, -0x2

    move/from16 v0, v37

    if-ne v0, v4, :cond_4

    .line 163
    :try_start_13
    invoke-virtual {v8}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v35

    .line 165
    .local v35, "outputFormat":Landroid/media/MediaFormat;
    const-string v4, "MediaDecoder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "INFO_OUTPUT_FORMAT_CHANGED"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v35

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 170
    .end local v35    # "outputFormat":Landroid/media/MediaFormat;
    .end local v37    # "res":I
    :cond_d
    invoke-virtual {v8}, Landroid/media/MediaCodec;->stop()V

    .line 171
    invoke-virtual {v8}, Landroid/media/MediaCodec;->release()V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_5
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    .line 181
    :try_start_14
    invoke-virtual/range {v26 .. v26}, Ljava/io/FileOutputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_6
    .catch Ljava/lang/NullPointerException; {:try_start_14 .. :try_end_14} :catch_7
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    .line 182
    const/16 v25, 0x0

    .line 183
    .end local v26    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v25    # "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_15
    invoke-virtual/range {v38 .. v38}, Ljava/io/FileInputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_d
    .catch Ljava/lang/NullPointerException; {:try_start_15 .. :try_end_15} :catch_c
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    .line 191
    :goto_a
    :try_start_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->mOnDecodeFinishedListener:Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;

    if-eqz v4, :cond_1

    .line 192
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->mOnDecodeFinishedListener:Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;

    move-object/from16 v0, v36

    invoke-interface {v4, v0}, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;->onFinished(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 184
    .end local v25    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v26    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v20

    move-object/from16 v25, v26

    .line 185
    .end local v26    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v20    # "e":Ljava/io/IOException;
    .restart local v25    # "fileOutputStream":Ljava/io/FileOutputStream;
    :goto_b
    invoke-virtual/range {v20 .. v20}, Ljava/io/IOException;->printStackTrace()V

    .line 186
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    throw v4

    .line 187
    .end local v20    # "e":Ljava/io/IOException;
    .end local v25    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v26    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v20

    move-object/from16 v25, v26

    .line 188
    .end local v26    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v20, "e":Ljava/lang/NullPointerException;
    .restart local v25    # "fileOutputStream":Ljava/io/FileOutputStream;
    :goto_c
    invoke-virtual/range {v20 .. v20}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_a

    .line 184
    .end local v2    # "extractor":Landroid/media/MediaExtractor;
    .end local v3    # "fd":Ljava/io/FileDescriptor;
    .end local v6    # "srcLength":J
    .end local v8    # "codec":Landroid/media/MediaCodec;
    .end local v17    # "codecInputBuffers":[Ljava/nio/ByteBuffer;
    .end local v18    # "codecOutputBuffers":[Ljava/nio/ByteBuffer;
    .end local v21    # "endOfInputStream":Z
    .end local v22    # "endOfOutputStream":Z
    .end local v23    # "fdOut":Ljava/io/FileDescriptor;
    .end local v27    # "format":Landroid/media/MediaFormat;
    .end local v29    # "i":I
    .end local v30    # "info":Landroid/media/MediaCodec$BufferInfo;
    .end local v31    # "mime":Ljava/lang/String;
    .end local v32    # "kTimeOutUs":J
    .end local v39    # "trackCount":I
    .end local v40    # "trackIndex":I
    .local v20, "e":Ljava/lang/Exception;
    :catch_8
    move-exception v20

    .line 185
    .local v20, "e":Ljava/io/IOException;
    invoke-virtual/range {v20 .. v20}, Ljava/io/IOException;->printStackTrace()V

    .line 186
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    throw v4

    .line 187
    .local v20, "e":Ljava/lang/Exception;
    :catch_9
    move-exception v20

    .line 188
    .local v20, "e":Ljava/lang/NullPointerException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_4

    .line 184
    .end local v20    # "e":Ljava/lang/NullPointerException;
    :catch_a
    move-exception v20

    .line 185
    .local v20, "e":Ljava/io/IOException;
    invoke-virtual/range {v20 .. v20}, Ljava/io/IOException;->printStackTrace()V

    .line 186
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    throw v4

    .line 187
    .end local v20    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v20

    .line 188
    .local v20, "e":Ljava/lang/NullPointerException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    goto/16 :goto_9

    .line 180
    .end local v20    # "e":Ljava/lang/NullPointerException;
    :catchall_3
    move-exception v4

    goto/16 :goto_8

    .line 187
    .restart local v2    # "extractor":Landroid/media/MediaExtractor;
    .restart local v3    # "fd":Ljava/io/FileDescriptor;
    .restart local v6    # "srcLength":J
    .restart local v8    # "codec":Landroid/media/MediaCodec;
    .restart local v17    # "codecInputBuffers":[Ljava/nio/ByteBuffer;
    .restart local v18    # "codecOutputBuffers":[Ljava/nio/ByteBuffer;
    .restart local v21    # "endOfInputStream":Z
    .restart local v22    # "endOfOutputStream":Z
    .restart local v23    # "fdOut":Ljava/io/FileDescriptor;
    .restart local v27    # "format":Landroid/media/MediaFormat;
    .restart local v29    # "i":I
    .restart local v30    # "info":Landroid/media/MediaCodec$BufferInfo;
    .restart local v31    # "mime":Ljava/lang/String;
    .restart local v32    # "kTimeOutUs":J
    .restart local v39    # "trackCount":I
    .restart local v40    # "trackIndex":I
    :catch_c
    move-exception v20

    goto :goto_c

    .line 184
    :catch_d
    move-exception v20

    goto :goto_b
.end method

.method public setOnDecodeFinishedListener(Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;

    .prologue
    .line 297
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->mOnDecodeFinishedListener:Lcom/sec/android/app/voicenote/library/stt/MediaDecoder$OnDecodeFinishedListener;

    .line 298
    return-void
.end method

.method public startDecode(Ljava/lang/String;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 27
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/stt/MediaDecoder;->decode(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    :goto_0
    return-void

    .line 28
    :catch_0
    move-exception v0

    .line 29
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "MediaDecoder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IOException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/library/stt/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
