.class Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;
.super Ljava/lang/Object;
.source "VNPlayerToolbarFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V
    .locals 0

    .prologue
    .line 404
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 408
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 409
    .local v0, "action":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    float-to-int v1, v4

    .line 411
    .local v1, "x":I
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Landroid/widget/ImageView;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$600(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Landroid/widget/ImageView;

    move-result-object v4

    if-nez v4, :cond_2

    .line 412
    :cond_0
    const-string v2, "VNPlyayerToolbarFragment"

    const-string v4, "mRepeatAIndicator, mRepeatBIndicator return null"

    invoke-static {v2, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 435
    :cond_1
    :goto_0
    return v2

    .line 415
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAIndicator:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBIndicator:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$600(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    move v2, v3

    .line 417
    goto :goto_0

    .line 420
    :cond_4
    if-nez v0, :cond_5

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    if-eqz v4, :cond_5

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 421
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 422
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->stopProgress()V

    .line 423
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsResumePlay:Z
    invoke-static {v4, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$702(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;Z)Z

    .line 424
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$800(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onUpdateStateNoModeChange(Z)V

    .line 427
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v4, p1, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->moveRepeatButtons(Landroid/view/View;I)V

    .line 429
    if-ne v2, v0, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsResumePlay:Z
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Z

    move-result v4

    if-eqz v4, :cond_1

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 430
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumePlay()V

    .line 431
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->startProgress()V

    .line 432
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mIsResumePlay:Z
    invoke-static {v4, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$702(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;Z)Z

    .line 433
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$800(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onUpdateStateNoModeChange(Z)V

    goto :goto_0
.end method
