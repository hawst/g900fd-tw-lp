.class public Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
.super Landroid/app/ListFragment;
.source "VNBookmarkListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;
.implements Lcom/sec/android/app/voicenote/common/util/IVNOperation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;,
        Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;,
        Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$EventHandler;
    }
.end annotation


# static fields
.field public static final ADD_MEMO_MODE:I = 0x1

.field public static final ARGUMENT_SELECTID:Ljava/lang/String; = "select_id"

.field private static final BOOKMARKLIST_ACTIONMODE:Ljava/lang/String; = "actionmenumode"

.field public static final EDIT_MEMO_MODE:I = 0x0

.field public static final MENU_DELETE:I = 0x0

.field public static final MENU_DETAILS:I = 0x3

.field public static final MENU_RENAME:I = 0x1

.field public static final MSG_DELETE_BOOKMARK:I = 0x2329

.field public static final MSG_UPDATE_LIST:I = 0x232a

.field private static final SELECT_ITEM_ID:Ljava/lang/String; = "selectitemid"

.field private static final TAG:Ljava/lang/String; = "VNBookmarkListFragment"

.field private static mBookmarks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field private static mCheckBoxes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/CheckBox;",
            ">;"
        }
    .end annotation
.end field

.field private static mIsActivated:Z


# instance fields
.field private mActionMode:I

.field private mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;

.field private mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

.field private final mBookmarks_cursor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field private mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field public mCurrentSelectedFileId:J

.field private mDeletedialog:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

.field private final mEventHandler:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$EventHandler;

.field private mMainView:Landroid/view/View;

.field private mNothingCheckedItem:Z

.field private mRenameDialog:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

.field private mRenameMenuEnable:Z

.field private mSelectedItem:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 76
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    .line 90
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mIsActivated:Z

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCheckBoxes:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 67
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 72
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mMainView:Landroid/view/View;

    .line 74
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCurrentSelectedFileId:J

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    .line 78
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    .line 82
    iput v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mSelectedItem:I

    .line 83
    iput v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionMode:I

    .line 84
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 87
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mDeletedialog:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    .line 88
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mRenameDialog:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    .line 89
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$EventHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$EventHandler;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mEventHandler:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$EventHandler;

    .line 93
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;

    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mNothingCheckedItem:Z

    .line 286
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mRenameMenuEnable:Z

    .line 641
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->handleMessage(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    .param p1, "x1"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionMode:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mNothingCheckedItem:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mNothingCheckedItem:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mRenameMenuEnable:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mRenameMenuEnable:Z

    return p1
.end method

.method static synthetic access$600()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCheckBoxes:Ljava/util/ArrayList;

    return-object v0
.end method

.method private deleteBookmark()V
    .locals 2

    .prologue
    .line 247
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mSelectedItem:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mSelectedItem:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 251
    :cond_0
    return-void
.end method

.method private deleteBookmarks([J)V
    .locals 8
    .param p1, "ids"    # [J

    .prologue
    .line 254
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 265
    :cond_0
    return-void

    .line 256
    :cond_1
    array-length v1, p1

    .line 257
    .local v1, "len":I
    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    const/4 v3, -0x1

    if-le v0, v3, :cond_0

    .line 258
    aget-wide v4, p1, v0

    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    .line 259
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    aget-wide v4, p1, v0

    long-to-int v4, v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 260
    .local v2, "tmp":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 261
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    .line 262
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 257
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public static getBookmarks()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 343
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    return-object v0
.end method

.method private handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 380
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 419
    :cond_0
    :goto_0
    return-void

    .line 382
    :pswitch_0
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 383
    .local v2, "obj":Ljava/lang/Object;
    instance-of v3, v2, [J

    if-eqz v3, :cond_4

    .line 384
    check-cast v2, [J

    .end local v2    # "obj":Ljava/lang/Object;
    move-object v1, v2

    check-cast v1, [J

    .line 385
    .local v1, "ids":[J
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->deleteBookmarks([J)V

    .line 386
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v3, :cond_1

    .line 387
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v3}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->finishCustomActionMode()V

    .line 388
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-interface {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onDeleteBookmark(Ljava/util/ArrayList;)V

    .line 390
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    if-eqz v3, :cond_2

    .line 391
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->notifyDataSetChanged()V

    .line 393
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 394
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 395
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_0

    .line 396
    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0

    .line 400
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v1    # "ids":[J
    .restart local v2    # "obj":Ljava/lang/Object;
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->deleteBookmark()V

    .line 401
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v3, :cond_5

    .line 402
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-interface {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onDeleteBookmark(Ljava/util/ArrayList;)V

    .line 404
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->notifyDataSetChanged()V

    .line 405
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 406
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 407
    .restart local v0    # "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_0

    .line 408
    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0

    .line 414
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v2    # "obj":Ljava/lang/Object;
    :pswitch_1
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/util/ArrayList;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->updateBookmarkByMessage(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 380
    :pswitch_data_0
    .packed-switch 0x2329
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private initViews()V
    .locals 7

    .prologue
    .line 158
    const-string v0, "VNBookmarkListFragment"

    const-string v1, "initViews"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 161
    const-string v0, "VNBookmarkListFragment"

    const-string v1, "activity is destoyed"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mMainView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 167
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f03000b

    const v4, 0x7f0e0021

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;Landroid/content/Context;IILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mMainView:Landroid/view/View;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    .line 170
    .local v6, "list":Landroid/widget/ListView;
    if-eqz v6, :cond_0

    .line 171
    invoke-virtual {v6, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public static isActivated()Z
    .locals 1

    .prologue
    .line 456
    sget-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mIsActivated:Z

    return v0
.end method


# virtual methods
.method public askOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)Ljava/lang/ref/WeakReference;
    .locals 6
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p3, "callback"    # Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/DialogFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 622
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 623
    .local v2, "size":I
    new-array v1, v2, [J

    .line 624
    .local v1, "ids":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 625
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v1, v0

    .line 624
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 628
    :cond_0
    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    invoke-direct {v3}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mDeletedialog:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    .line 629
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mDeletedialog:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->setArguments([JZ)V

    .line 630
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mDeletedialog:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mEventHandler:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$EventHandler;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->setBookmarkMode(Landroid/os/Handler;)V

    .line 631
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mDeletedialog:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "FRAGMENT_DIALOG"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 633
    new-instance v3, Ljava/lang/ref/WeakReference;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mDeletedialog:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    invoke-direct {v3, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    return-object v3
.end method

.method public getActionMode()Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v0}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    .line 470
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBookmakrHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mEventHandler:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$EventHandler;

    return-object v0
.end method

.method public getCallback()Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;
    .locals 0

    .prologue
    .line 802
    return-object p0
.end method

.method public isSelectionMode()Z
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    if-nez v0, :cond_0

    .line 461
    const/4 v0, 0x0

    .line 463
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->isSelectionMode()Z

    move-result v0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 348
    const/16 v1, 0xc1d

    if-ne p1, v1, :cond_0

    .line 349
    packed-switch p2, :pswitch_data_0

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 351
    :pswitch_0
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "description"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 352
    .local v0, "newDescription":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mSelectedItem:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->setDescription(Ljava/lang/String;)V

    .line 353
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-interface {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onBookmarksRenamed(Ljava/util/ArrayList;)V

    .line 354
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 349
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 179
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 180
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onAttach(Landroid/app/Activity;)V

    .line 181
    return-void
.end method

.method public onBookmarkMemuClick(I)V
    .locals 4
    .param p1, "itemId"    # I

    .prologue
    .line 314
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v1, :cond_0

    .line 315
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getActionMode()Landroid/view/ActionMode;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 326
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    const/4 v0, 0x1

    .line 320
    .local v0, "selectMode":I
    const v1, 0x7f0e0117

    if-ne p1, v1, :cond_2

    .line 321
    const/4 v0, 0x3

    .line 323
    :cond_2
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;

    const/4 v2, -0x1

    const/4 v3, 0x1

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;IIZ)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;

    .line 324
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->setActionMode(Landroid/view/ActionMode;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 99
    const-string v1, "VNBookmarkListFragment"

    const-string v2, "onCreateView"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const v1, 0x7f030017

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mMainView:Landroid/view/View;

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "select_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCurrentSelectedFileId:J

    .line 103
    if-eqz p3, :cond_0

    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 104
    const-string v1, "selectitemid"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mSelectedItem:I

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 106
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->updateBookmarkByMessage(Ljava/util/ArrayList;)V

    .line 107
    const-string v1, "actionmenumode"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionMode:I

    .line 110
    :cond_0
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    if-nez v1, :cond_2

    .line 111
    const-string v1, "VNBookmarkListFragment"

    const-string v2, "onCreateView : mBookmarks is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DIALOG"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    .line 117
    .local v0, "dialog":Landroid/app/DialogFragment;
    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    .line 118
    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 120
    :cond_1
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mIsActivated:Z

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mMainView:Landroid/view/View;

    return-object v1

    .line 113
    .end local v0    # "dialog":Landroid/app/DialogFragment;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->initViews()V

    .line 114
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->updateBookmarkByMessage(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 135
    const-string v0, "VNBookmarkListFragment"

    const-string v1, "onDestroyView"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mDeletedialog:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mDeletedialog:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->isDetached()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mDeletedialog:Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNDeleteDialogFragment;->dismiss()V

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mRenameDialog:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mRenameDialog:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->isDetached()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mRenameDialog:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->dismiss()V

    .line 143
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mMainView:Landroid/view/View;

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_2

    .line 146
    sput-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v0, :cond_3

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v0}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->finishBookmarkActionMode()V

    .line 151
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 152
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    .line 153
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mIsActivated:Z

    .line 154
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroyView()V

    .line 155
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 186
    invoke-super {p0}, Landroid/app/ListFragment;->onDetach()V

    .line 187
    return-void
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v0, 0x1

    .line 330
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v1, :cond_0

    .line 331
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getActionMode()Landroid/view/ActionMode;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 332
    const/4 v0, 0x0

    .line 339
    :cond_0
    :goto_0
    return v0

    .line 335
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;

    invoke-direct {v1, p0, p3, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;IZ)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;

    .line 336
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->setActionMode(Landroid/view/ActionMode;)V

    goto :goto_0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 289
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 290
    :cond_0
    const-string v3, "VNBookmarkListFragment"

    const-string v4, "onItemClick mBookmarks is empty"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    :goto_0
    return-void

    .line 296
    :cond_1
    :try_start_0
    sget-object v3, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 302
    .local v2, "elapsedTime":I
    const-string v3, "VNBookmarkListFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onItemClick pos : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " milisecond : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    .line 304
    .local v0, "actionMode":Landroid/view/ActionMode;
    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 305
    invoke-virtual {p1}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    invoke-virtual {p1, p3, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 306
    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    goto :goto_0

    .line 297
    .end local v0    # "actionMode":Landroid/view/ActionMode;
    .end local v2    # "elapsedTime":I
    :catch_0
    move-exception v1

    .line 298
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v3, "VNBookmarkListFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onListItemClick : exception - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 309
    .end local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    .restart local v0    # "actionMode":Landroid/view/ActionMode;
    .restart local v2    # "elapsedTime":I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v3, v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onBookmarkItemClick(I)V

    .line 310
    invoke-super/range {p0 .. p5}, Landroid/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    goto :goto_0
.end method

.method public onMenuItemClick(II)Z
    .locals 5
    .param p1, "itemId"    # I
    .param p2, "idxNearByCurPos"    # I

    .prologue
    .line 430
    iput p2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mSelectedItem:I

    .line 431
    sparse-switch p1, :sswitch_data_0

    .line 448
    const-string v0, "VNBookmarkListFragment"

    const-string v1, "onContextItemSelected : invalid value"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->notifyDataSetChanged()V

    .line 452
    const/4 v0, 0x1

    return v0

    .line 435
    :sswitch_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->onBookmarkMemuClick(I)V

    goto :goto_0

    .line 439
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 440
    const/4 v1, 0x2

    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mSelectedItem:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getTitle()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, -0x1

    const v4, 0x7f0b0105

    invoke-static {v1, v0, v2, v3, v4}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->setArguments(ILjava/lang/String;JI)Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mRenameDialog:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mRenameDialog:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->setCallback(Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment$Callbacks;)V

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mRenameDialog:Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNRenameDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 431
    :sswitch_data_0
    .sparse-switch
        0x7f0e00f4 -> :sswitch_1
        0x7f0e0116 -> :sswitch_0
        0x7f0e0117 -> :sswitch_0
    .end sparse-switch
.end method

.method public onRenameDialogNagativeBtn()V
    .locals 0

    .prologue
    .line 283
    return-void
.end method

.method public onRenameDialogPositiveBtn(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 269
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mSelectedItem:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 270
    .local v0, "tempBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->setTitle(Ljava/lang/String;)V

    .line 271
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mSelectedItem:I

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "tempBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    check-cast v0, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 272
    .restart local v0    # "tempBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 273
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mSelectedItem:I

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 275
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-interface {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onBookmarksRenamed(Ljava/util/ArrayList;)V

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->notifyDataSetChanged()V

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->finishActionMode()V

    .line 278
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getActionMode()Landroid/view/ActionMode;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 193
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;

    const/4 v2, -0x1

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionMode:I

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;IIZ)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;

    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    .line 195
    .local v0, "actionMode":Landroid/view/ActionMode;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v1, v0}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->setActionMode(Landroid/view/ActionMode;)V

    .line 196
    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    if-eqz v1, :cond_0

    .line 198
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->setSelectionMode(ZZ)V

    .line 209
    .end local v0    # "actionMode":Landroid/view/ActionMode;
    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 210
    return-void

    .line 200
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getActionMode()Landroid/view/ActionMode;

    move-result-object v1

    if-nez v1, :cond_0

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->finishCustomActionMode()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 126
    const-string v0, "selectitemid"

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mSelectedItem:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;

    if-eqz v0, :cond_0

    .line 128
    const-string v0, "actionmenumode"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mActionModeListener:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$ActionModeListener;->getMenuMode()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 130
    :cond_0
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 131
    return-void
.end method

.method public setBookmark(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 422
    .local p1, "bookmark":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    sput-object p1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 427
    :cond_0
    return-void
.end method

.method public startOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V
    .locals 0
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p3, "callback"    # Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 639
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    return-void
.end method

.method public updateBookmarkByMessage(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 221
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    if-eqz p1, :cond_1

    .line 222
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 223
    sput-object p1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    .line 224
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mAdapterChild:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$BookmarkAdapter;->notifyDataSetChanged()V

    .line 244
    :cond_1
    :goto_0
    return-void

    .line 234
    :cond_2
    sput-object p1, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    .line 235
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mBookmarks_cursor:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 241
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->initViews()V

    goto :goto_0
.end method

.method public updateBookmarks(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    const-string v1, "VNBookmarkListFragment"

    const-string v2, "updateBookmarks"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 215
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x232a

    iput v1, v0, Landroid/os/Message;->what:I

    .line 216
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 217
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->mEventHandler:Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$EventHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment$EventHandler;->sendMessage(Landroid/os/Message;)Z

    .line 218
    return-void
.end method
