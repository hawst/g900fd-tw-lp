.class Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;
.super Ljava/lang/Object;
.source "VNPlayerToolbarFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 357
    const/4 v1, 0x0

    .line 358
    .local v1, "pos":I
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 359
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_0

    .line 360
    packed-switch p2, :pswitch_data_0

    .line 372
    :cond_0
    :goto_0
    if-eqz v1, :cond_9

    .line 373
    const/4 v2, 0x0

    .line 374
    .local v2, "repeatSeekbarPos":I
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    .line 375
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v5

    if-ge v4, v5, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v3

    .line 376
    .local v3, "startRepeatTime":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v5

    if-le v4, v5, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatAPosition:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v0

    .line 378
    .local v0, "endRepeatTime":I
    :goto_2
    if-gt v1, v0, :cond_1

    if-ge v1, v3, :cond_2

    .line 379
    :cond_1
    move v1, v3

    .line 381
    :cond_2
    move v2, v3

    .line 384
    .end local v0    # "endRepeatTime":I
    .end local v3    # "startRepeatTime":I
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v4

    if-lt v1, v4, :cond_7

    .line 385
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mDuration:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setProgress(I)V

    .line 392
    :cond_4
    :goto_3
    const/4 v4, 0x1

    .line 394
    .end local v2    # "repeatSeekbarPos":I
    :goto_4
    return v4

    .line 362
    :pswitch_0
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPositionForRepeat()I

    move-result v4

    add-int/lit16 v1, v4, -0x3e8

    .line 363
    goto :goto_0

    .line 365
    :pswitch_1
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPositionForRepeat()I

    move-result v4

    add-int/lit16 v1, v4, 0x3e8

    .line 366
    goto :goto_0

    .line 375
    .restart local v2    # "repeatSeekbarPos":I
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v3

    goto :goto_1

    .line 376
    .restart local v3    # "startRepeatTime":I
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mRepeatBPosition:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)I

    move-result v0

    goto :goto_2

    .line 386
    .end local v3    # "startRepeatTime":I
    :cond_7
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 387
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    sub-int v5, v1, v2

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setProgress(I)V

    .line 388
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    goto :goto_3

    .line 389
    :cond_8
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 390
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    goto :goto_3

    .line 394
    .end local v2    # "repeatSeekbarPos":I
    :cond_9
    const/4 v4, 0x0

    goto :goto_4

    .line 360
    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
