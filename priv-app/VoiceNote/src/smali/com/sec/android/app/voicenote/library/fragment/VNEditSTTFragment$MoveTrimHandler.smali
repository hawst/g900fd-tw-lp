.class Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;
.super Ljava/lang/Object;
.source "VNEditSTTFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MoveTrimHandler"
.end annotation


# instance fields
.field mLenthLeft:I

.field mLenthRight:I

.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;II)V
    .locals 1
    .param p2, "lenLeft"    # I
    .param p3, "lenRight"    # I

    .prologue
    const/4 v0, 0x0

    .line 792
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 789
    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->mLenthLeft:I

    .line 790
    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->mLenthRight:I

    .line 793
    iput p2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->mLenthLeft:I

    .line 794
    iput p3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->mLenthRight:I

    .line 795
    return-void
.end method

.method private getTrimRectWithText(II)Landroid/graphics/Rect;
    .locals 18
    .param p1, "lengthLeft"    # I
    .param p2, "lengthRight"    # I

    .prologue
    .line 819
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 820
    .local v5, "parentTextViewRect":Landroid/graphics/Rect;
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 821
    .local v8, "parentTextViewRect1":Landroid/graphics/Rect;
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 824
    .local v9, "parentTextViewRect2":Landroid/graphics/Rect;
    const/4 v12, 0x0

    .line 826
    .local v12, "textViewLayout":Landroid/text/Layout;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 832
    move/from16 v0, p1

    invoke-virtual {v12, v0}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v13

    float-to-double v10, v13

    .line 833
    .local v10, "startXCoordinatesOfTrimmedText":D
    move/from16 v0, p2

    invoke-virtual {v12, v0}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v13

    float-to-double v6, v13

    .line 836
    .local v6, "endXCoordinatesOfTrimmedText":D
    move/from16 v0, p1

    invoke-virtual {v12, v0}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v3

    .line 837
    .local v3, "currentLineStartOffset":I
    move/from16 v0, p2

    invoke-virtual {v12, v0}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v2

    .line 838
    .local v2, "currentLineEndOffset":I
    invoke-virtual {v12, v3, v8}, Landroid/text/Layout;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 839
    invoke-virtual {v12, v2, v9}, Landroid/text/Layout;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 841
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    move-result v13

    iget v14, v8, Landroid/graphics/Rect;->top:I

    add-int/2addr v13, v14

    iput v13, v5, Landroid/graphics/Rect;->top:I

    .line 842
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    move-result v13

    iget v14, v9, Landroid/graphics/Rect;->top:I

    add-int/2addr v13, v14

    iput v13, v5, Landroid/graphics/Rect;->bottom:I

    .line 844
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getTrimRectWithText()-1 : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimLeft:I
    invoke-static {v15}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1600(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getTrimRectWithText()0 : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getTrimRectWithText()1 : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    move-result v13

    int-to-double v14, v13

    add-double/2addr v14, v10

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/TextView;->getScrollX()I

    move-result v13

    int-to-double v0, v13

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    int-to-double v0, v3

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    double-to-int v13, v14

    iput v13, v5, Landroid/graphics/Rect;->left:I

    .line 849
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    move-result v13

    add-int/2addr v13, v2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;
    invoke-static {v14}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/TextView;->getScrollX()I

    move-result v14

    sub-int/2addr v13, v14

    int-to-double v14, v13

    add-double/2addr v14, v6

    double-to-int v13, v14

    iput v13, v5, Landroid/graphics/Rect;->right:I

    .line 852
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getTrimRectWithText()2 : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    iget v13, v5, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimStartHandler:Landroid/view/View;
    invoke-static {v14}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v14

    invoke-virtual {v14}, Landroid/view/View;->getWidth()I

    move-result v14

    if-ge v13, v14, :cond_1

    .line 854
    const/4 v13, 0x0

    iput v13, v5, Landroid/graphics/Rect;->left:I

    .line 859
    :goto_0
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getTrimRectWithText()3 : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    invoke-virtual {v12}, Landroid/text/Layout;->getWidth()I

    move-result v13

    iget v14, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimStartHandler:Landroid/view/View;
    invoke-static {v14}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v14

    invoke-virtual {v14}, Landroid/view/View;->getWidth()I

    move-result v14

    if-ge v13, v14, :cond_0

    .line 861
    invoke-virtual {v12}, Landroid/text/Layout;->getWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimStartHandler:Landroid/view/View;
    invoke-static {v14}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v14

    invoke-virtual {v14}, Landroid/view/View;->getWidth()I

    move-result v14

    sub-int/2addr v13, v14

    iput v13, v5, Landroid/graphics/Rect;->right:I

    .line 864
    .end local v2    # "currentLineEndOffset":I
    .end local v3    # "currentLineStartOffset":I
    .end local v5    # "parentTextViewRect":Landroid/graphics/Rect;
    .end local v6    # "endXCoordinatesOfTrimmedText":D
    .end local v10    # "startXCoordinatesOfTrimmedText":D
    :cond_0
    :goto_1
    return-object v5

    .line 827
    .restart local v5    # "parentTextViewRect":Landroid/graphics/Rect;
    :catch_0
    move-exception v4

    .line 828
    .local v4, "e":Ljava/lang/Exception;
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    new-instance v5, Landroid/graphics/Rect;

    .end local v5    # "parentTextViewRect":Landroid/graphics/Rect;
    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    goto :goto_1

    .line 856
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v2    # "currentLineEndOffset":I
    .restart local v3    # "currentLineStartOffset":I
    .restart local v5    # "parentTextViewRect":Landroid/graphics/Rect;
    .restart local v6    # "endXCoordinatesOfTrimmedText":D
    .restart local v10    # "startXCoordinatesOfTrimmedText":D
    :cond_1
    iget v13, v5, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimStartHandler:Landroid/view/View;
    invoke-static {v14}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v14

    invoke-virtual {v14}, Landroid/view/View;->getWidth()I

    move-result v14

    sub-int/2addr v13, v14

    iput v13, v5, Landroid/graphics/Rect;->left:I

    goto :goto_0
.end method

.method private setHandlerPosition(Landroid/view/View;II)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I

    .prologue
    const/4 v1, 0x0

    .line 868
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 869
    .local v0, "lp":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v0, p2, p3, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 870
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 871
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 799
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimLeft:I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1600(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimRight:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimLeft:I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1600(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v1

    if-nez v1, :cond_1

    .line 800
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimStartHandler:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 801
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimStartHandler:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 802
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimEndHandler:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1500(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 815
    :cond_0
    :goto_0
    return-void

    .line 806
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimStartHandler:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_2

    .line 807
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimStartHandler:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 808
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimEndHandler:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1500(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 811
    :cond_2
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->mLenthLeft:I

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->mLenthRight:I

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->getTrimRectWithText(II)Landroid/graphics/Rect;

    move-result-object v0

    .line 813
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimStartHandler:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->setHandlerPosition(Landroid/view/View;II)V

    .line 814
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimEndHandler:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1500(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;

    move-result-object v1

    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;->setHandlerPosition(Landroid/view/View;II)V

    goto :goto_0
.end method
