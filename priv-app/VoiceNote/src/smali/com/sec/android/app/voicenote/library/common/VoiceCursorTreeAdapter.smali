.class public abstract Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "VoiceCursorTreeAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;
.implements Lcom/sec/android/app/voicenote/library/common/CursorFilter$CursorFilterClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$1;,
        Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;
    }
.end annotation


# instance fields
.field private mAutoRequery:Z

.field mChildrenCursorHelpers:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field mCursorFilter:Lcom/sec/android/app/voicenote/library/common/CursorFilter;

.field mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

.field mGroupCursorHelper:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

.field private mHandler:Landroid/os/Handler;

.field final refreshTimems:I

.field private updatetime:J


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Landroid/content/Context;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 26
    const/16 v0, 0x258

    iput v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->refreshTimems:I

    .line 35
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->updatetime:J

    .line 38
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->init(Landroid/database/Cursor;Landroid/content/Context;Z)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Landroid/content/Context;Z)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "autoRequery"    # Z

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 26
    const/16 v0, 0x258

    iput v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->refreshTimems:I

    .line 35
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->updatetime:J

    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->init(Landroid/database/Cursor;Landroid/content/Context;Z)V

    .line 43
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mAutoRequery:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->updatetime:J

    return-wide v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;
    .param p1, "x1"    # J

    .prologue
    .line 21
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->updatetime:J

    return-wide p1
.end method

.method private init(Landroid/database/Cursor;Landroid/content/Context;Z)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "autoRequery"    # Z

    .prologue
    .line 46
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mContext:Landroid/content/Context;

    .line 47
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mHandler:Landroid/os/Handler;

    .line 48
    iput-boolean p3, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mAutoRequery:Z

    .line 50
    new-instance v0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;-><init>(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mGroupCursorHelper:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    .line 51
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mChildrenCursorHelpers:Landroid/util/SparseArray;

    .line 52
    return-void
.end method

.method private declared-synchronized releaseCursorHelpers()V
    .locals 2

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mChildrenCursorHelpers:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "pos":I
    :goto_0
    if-ltz v0, :cond_0

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mChildrenCursorHelpers:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->deactivate()V

    .line 174
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 178
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mChildrenCursorHelpers:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    monitor-exit p0

    return-void

    .line 174
    .end local v0    # "pos":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method protected abstract bindChildView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
.end method

.method protected abstract bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mGroupCursorHelper:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->changeCursor(Landroid/database/Cursor;Z)V

    .line 241
    return-void
.end method

.method public bridge synthetic convertToString(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->convertToString(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public convertToString(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 213
    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method declared-synchronized deactivateChildrenCursorHelper(I)V
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 207
    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, p1, v1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->getChildrenCursorHelper(IZ)Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    move-result-object v0

    .line 208
    .local v0, "cursorHelper":Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mChildrenCursorHelpers:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 209
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->deactivate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    monitor-exit p0

    return-void

    .line 207
    .end local v0    # "cursorHelper":Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getChild(II)Landroid/database/Cursor;
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 92
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->getChildrenCursorHelper(IZ)Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->moveTo(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getChild(II)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # I

    .prologue
    .line 21
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->getChild(II)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 96
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->getChildrenCursorHelper(IZ)Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->getId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 142
    const/4 v3, 0x1

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->getChildrenCursorHelper(IZ)Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    move-result-object v1

    .line 144
    .local v1, "cursorHelper":Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;
    invoke-virtual {v1, p2}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->moveTo(I)Landroid/database/Cursor;

    move-result-object v0

    .line 145
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 146
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "this should only be called when the cursor is valid"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 150
    :cond_0
    if-nez p4, :cond_1

    .line 151
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v3, v0, p3, p5}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->newChildView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 155
    .local v2, "v":Landroid/view/View;
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v2, v3, v0, p3}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->bindChildView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 156
    return-object v2

    .line 153
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    move-object v2, p4

    .restart local v2    # "v":Landroid/view/View;
    goto :goto_0
.end method

.method public getChildrenCount(I)I
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 100
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->getChildrenCursorHelper(IZ)Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    move-result-object v0

    .line 101
    .local v0, "helper":Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mGroupCursorHelper:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->getCount()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected abstract getChildrenCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
.end method

.method declared-synchronized getChildrenCursorHelper(IZ)Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;
    .locals 3
    .param p1, "groupPosition"    # I
    .param p2, "requestCursor"    # Z

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mChildrenCursorHelpers:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    .line 57
    .local v1, "cursorHelper":Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;
    if-nez v1, :cond_1

    .line 58
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mGroupCursorHelper:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->moveTo(I)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 65
    :goto_0
    monitor-exit p0

    return-object v2

    .line 60
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mGroupCursorHelper:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->getChildrenCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 61
    .local v0, "cursor":Landroid/database/Cursor;
    new-instance v1, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    .end local v1    # "cursorHelper":Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;
    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;-><init>(Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;Landroid/database/Cursor;)V

    .line 62
    .restart local v1    # "cursorHelper":Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mChildrenCursorHelpers:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_1
    move-object v2, v1

    .line 65
    goto :goto_0

    .line 55
    .end local v1    # "cursorHelper":Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public getCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mGroupCursorHelper:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mCursorFilter:Lcom/sec/android/app/voicenote/library/common/CursorFilter;

    if-nez v0, :cond_0

    .line 226
    new-instance v0, Lcom/sec/android/app/voicenote/library/common/CursorFilter;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/common/CursorFilter;-><init>(Lcom/sec/android/app/voicenote/library/common/CursorFilter$CursorFilterClient;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mCursorFilter:Lcom/sec/android/app/voicenote/library/common/CursorFilter;

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mCursorFilter:Lcom/sec/android/app/voicenote/library/common/CursorFilter;

    return-object v0
.end method

.method public getFilterQueryProvider()Landroid/widget/FilterQueryProvider;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

    return-object v0
.end method

.method public getGroup(I)Landroid/database/Cursor;
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mGroupCursorHelper:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->moveTo(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getGroup(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->getGroup(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mGroupCursorHelper:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->getCount()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mGroupCursorHelper:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->getId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 119
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mGroupCursorHelper:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->moveTo(I)Landroid/database/Cursor;

    move-result-object v0

    .line 120
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 121
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "this should only be called when the cursor is valid"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 125
    :cond_0
    if-nez p3, :cond_1

    .line 126
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v2, v0, p2, p4}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->newGroupView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 130
    .local v1, "v":Landroid/view/View;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1, v2, v0, p2}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 131
    return-object v1

    .line 128
    .end local v1    # "v":Landroid/view/View;
    :cond_1
    move-object v1, p3

    .restart local v1    # "v":Landroid/view/View;
    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x1

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 166
    const/4 v0, 0x1

    return v0
.end method

.method protected abstract newChildView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
.end method

.method protected abstract newGroupView(Landroid/content/Context;Landroid/database/Cursor;ZLandroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->notifyDataSetChanged(Z)V

    .line 184
    return-void
.end method

.method public notifyDataSetChanged(Z)V
    .locals 0
    .param p1, "releaseCursors"    # Z

    .prologue
    .line 188
    if-eqz p1, :cond_0

    .line 189
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->releaseCursorHelpers()V

    .line 192
    :cond_0
    invoke-super {p0}, Landroid/widget/BaseExpandableListAdapter;->notifyDataSetChanged()V

    .line 193
    return-void
.end method

.method public notifyDataSetInvalidated()V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->releaseCursorHelpers()V

    .line 198
    invoke-super {p0}, Landroid/widget/BaseExpandableListAdapter;->notifyDataSetInvalidated()V

    .line 199
    return-void
.end method

.method public onGroupCollapsed(I)V
    .locals 0
    .param p1, "groupPosition"    # I

    .prologue
    .line 203
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->deactivateChildrenCursorHelper(I)V

    .line 204
    return-void
.end method

.method public runQueryOnBackgroundThread(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .locals 1
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

    invoke-interface {v0, p1}, Landroid/widget/FilterQueryProvider;->runQuery(Ljava/lang/CharSequence;)Landroid/database/Cursor;

    move-result-object v0

    .line 221
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mGroupCursorHelper:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method public setChildrenCursor(ILandroid/database/Cursor;)V
    .locals 2
    .param p1, "groupPosition"    # I
    .param p2, "childrenCursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-virtual {p0, p1, v1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->getChildrenCursorHelper(IZ)Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    move-result-object v0

    .line 85
    .local v0, "childrenCursorHelper":Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;
    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {v0, p2, v1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->changeCursor(Landroid/database/Cursor;Z)V

    .line 88
    :cond_0
    return-void
.end method

.method public setFilterQueryProvider(Landroid/widget/FilterQueryProvider;)V
    .locals 0
    .param p1, "filterQueryProvider"    # Landroid/widget/FilterQueryProvider;

    .prologue
    .line 236
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

    .line 237
    return-void
.end method

.method public setGroupCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mGroupCursorHelper:Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter$MyCursorHelper;->changeCursor(Landroid/database/Cursor;Z)V

    .line 71
    return-void
.end method

.method public setmAutoRequery(Z)V
    .locals 0
    .param p1, "mAutoRequery"    # Z

    .prologue
    .line 248
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/common/VoiceCursorTreeAdapter;->mAutoRequery:Z

    .line 249
    return-void
.end method
