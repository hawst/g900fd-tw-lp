.class Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$3;
.super Ljava/lang/Object;
.source "VNSelectLabelDialogFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;)V
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 320
    const-string v1, "VNSelectLabelDialogFragment"

    const-string v2, "onClick"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->isResumed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 323
    const-string v1, "VNSelectLabelDialogFragment"

    const-string v2, "onClick fail - fragment is not resumed"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    :goto_0
    return-void

    .line 327
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 328
    .local v0, "tag":I
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 333
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->dismissAllowingStateLoss()V

    goto :goto_0

    .line 330
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$3;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 328
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
