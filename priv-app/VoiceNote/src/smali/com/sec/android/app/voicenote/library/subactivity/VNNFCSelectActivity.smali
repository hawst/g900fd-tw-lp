.class public Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;
.super Landroid/app/Activity;
.source "VNNFCSelectActivity.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;


# static fields
.field private static final MENU_CANCEL:I = 0x1

.field private static final MENU_DONE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "VNNFCSelectActivity"


# instance fields
.field private mCurrentId:J

.field private mDoneItem:Landroid/view/MenuItem;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 41
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->mCurrentId:J

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->mDoneItem:Landroid/view/MenuItem;

    return-void
.end method

.method private startNFCWritingActivity()V
    .locals 4

    .prologue
    .line 150
    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->mCurrentId:J

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentLabelInfo(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 151
    .local v1, "labelinfo":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 152
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x20000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 153
    const-string v2, "labelinfo"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setNFCfilename(Ljava/lang/String;)V

    .line 155
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->startActivity(Landroid/content/Intent;)V

    .line 156
    return-void
.end method


# virtual methods
.method public initViews()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 59
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;-><init>(Landroid/app/Activity;)V

    .line 60
    .local v0, "actionbar":Lcom/sec/android/app/voicenote/common/util/VNActionBar;
    invoke-virtual {v0, v4}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 63
    .local v1, "ft":Landroid/app/FragmentTransaction;
    const v2, 0x7f0e001b

    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    invoke-direct {v3}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;-><init>()V

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 64
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 65
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    const-string v1, "VNNFCSelectActivity"

    const-string v2, "OnCreate"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 50
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;-><init>(Landroid/app/Activity;)V

    .line 51
    .local v0, "mVNActionBar":Lcom/sec/android/app/voicenote/common/util/VNActionBar;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show(Ljava/lang/String;)V

    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const v1, 0x7f030007

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->setContentView(I)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->initViews()V

    .line 56
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 120
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 134
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 122
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->finish()V

    goto :goto_0

    .line 126
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->startNFCWritingActivity()V

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->finish()V

    goto :goto_0

    .line 131
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->finish()V

    goto :goto_0

    .line 120
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x102002c -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 100
    const-string v0, "VNNFCSelectActivity"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 102
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 139
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 140
    const v1, 0x7f0b0021

    invoke-interface {p1, v2, v3, v3, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 141
    .local v0, "menuItem":Landroid/view/MenuItem;
    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 142
    const v1, 0x7f0b0070

    invoke-interface {p1, v2, v4, v4, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 143
    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 144
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 145
    invoke-interface {p1, v3}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->mDoneItem:Landroid/view/MenuItem;

    .line 146
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 106
    const-string v0, "VNNFCSelectActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 108
    return-void
.end method

.method public onSingleSelect(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->mDoneItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->mDoneItem:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 114
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCSelectActivity;->mCurrentId:J

    .line 116
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 69
    const-string v0, "VNNFCSelectActivity"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 71
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 75
    const-string v0, "VNNFCSelectActivity"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 77
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 81
    const-string v0, "VNNFCSelectActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTrimMemory() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    sparse-switch p1, :sswitch_data_0

    .line 95
    :sswitch_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 96
    return-void

    .line 82
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method
