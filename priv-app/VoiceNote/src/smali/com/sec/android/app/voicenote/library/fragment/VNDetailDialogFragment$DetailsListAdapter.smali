.class Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "VNDetailDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DetailsListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 159
    .local p2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 160
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 164
    move-object v3, p2

    .line 165
    .local v3, "v":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;

    .line 166
    .local v1, "ti":Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;
    if-nez v3, :cond_0

    .line 167
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 168
    .local v4, "vi":Landroid/view/LayoutInflater;
    const v5, 0x7f03000d

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 170
    .end local v4    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    if-eqz v1, :cond_2

    if-eqz v3, :cond_2

    .line 171
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/view/View;->setClickable(Z)V

    .line 172
    const v5, 0x1020014

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 173
    .local v2, "title":Landroid/widget/TextView;
    const v5, 0x1020015

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 174
    .local v0, "summary":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 175
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    :cond_1
    if-eqz v0, :cond_2

    .line 178
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;->getSummary()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;->getDescription()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 182
    .end local v0    # "summary":Landroid/widget/TextView;
    .end local v2    # "title":Landroid/widget/TextView;
    :cond_2
    return-object v3
.end method
