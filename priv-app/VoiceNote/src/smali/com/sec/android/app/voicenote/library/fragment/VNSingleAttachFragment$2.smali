.class Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$2;
.super Ljava/lang/Object;
.source "VNSingleAttachFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)V
    .locals 0

    .prologue
    .line 338
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "radio"    # Landroid/view/View;

    .prologue
    .line 341
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v3, v4

    check-cast v3, [Ljava/lang/Object;

    .line 343
    .local v3, "tags":[Ljava/lang/Object;
    if-eqz v3, :cond_0

    array-length v4, v3

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 344
    const/4 v4, 0x0

    aget-object v4, v3, v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 345
    .local v2, "position":I
    const/4 v4, 0x1

    aget-object v4, v3, v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 347
    .local v0, "id":J
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J
    invoke-static {v4, v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$202(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;J)J

    .line 348
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonPos:I
    invoke-static {v4, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$302(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;I)I

    .line 349
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonPos:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)I

    move-result v5

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->selectRadioButton(I)V
    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;I)V

    .line 350
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 351
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$2;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)J

    move-result-wide v6

    invoke-interface {v4, v6, v7}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;->onSingleSelect(J)V

    .line 354
    .end local v0    # "id":J
    .end local v2    # "position":I
    :cond_0
    return-void
.end method
