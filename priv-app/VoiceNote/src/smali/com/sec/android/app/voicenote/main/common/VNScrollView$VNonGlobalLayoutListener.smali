.class Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;
.super Ljava/lang/Object;
.source "VNScrollView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/common/VNScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "VNonGlobalLayoutListener"
.end annotation


# instance fields
.field conversation:Landroid/view/View;

.field interview:Landroid/view/View;

.field parent:Landroid/view/ViewGroup;

.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/common/VNScrollView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/main/common/VNScrollView;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "interview"    # Landroid/view/View;
    .param p4, "conversation"    # Landroid/view/View;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;->this$0:Lcom/sec/android/app/voicenote/main/common/VNScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p2, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;->parent:Landroid/view/ViewGroup;

    .line 76
    iput-object p3, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;->interview:Landroid/view/View;

    .line 77
    iput-object p4, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;->conversation:Landroid/view/View;

    .line 78
    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 5

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;->this$0:Lcom/sec/android/app/voicenote/main/common/VNScrollView;

    .line 84
    .local v0, "scrollView":Landroid/widget/HorizontalScrollView;
    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;->parent:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewGroup;->removeViewsInLayout(II)V

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;->parent:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;->interview:Landroid/view/View;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;->parent:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;->conversation:Landroid/view/View;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 90
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener$1;-><init>(Lcom/sec/android/app/voicenote/main/common/VNScrollView$VNonGlobalLayoutListener;Landroid/widget/HorizontalScrollView;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 102
    return-void
.end method
