.class public Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;
.super Landroid/app/Fragment;
.source "VNSingleAttachFragment.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;
    }
.end annotation


# static fields
.field public static final RADIO_BUTTON_ID:Ljava/lang/String; = "radio_button_id"

.field public static final RADIO_BUTTON_POS:Ljava/lang/String; = "radio_button_pos"

.field private static final TAG:Ljava/lang/String; = "VNSingleAttachFragment"

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# instance fields
.field private mATTVVMEnabled:Z

.field private mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;

.field private mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field private mEnableList:Z

.field private mLibraryCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field private mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mPlayingId:J

.field mRadioClickListener:Landroid/view/View$OnClickListener;

.field private mRadiobuttonId:J

.field private mRadiobuttonPos:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListView:Landroid/widget/ListView;

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mContext:Landroid/content/Context;

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    .line 69
    iput-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    .line 70
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonPos:I

    .line 71
    iput-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mPlayingId:J

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;

    .line 74
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mLibraryCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mEnableList:Z

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mATTVVMEnabled:Z

    .line 338
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadioClickListener:Landroid/view/View$OnClickListener;

    .line 82
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mLibraryCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    return-wide v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;
    .param p1, "x1"    # J

    .prologue
    .line 59
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    return-wide p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonPos:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;
    .param p1, "x1"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonPos:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;
    .param p1, "x1"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->selectRadioButton(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;

    return-object v0
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 2
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 402
    const-string v0, "VNSingleAttachFragment"

    const-string v1, "initService()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 404
    return-void
.end method

.method private initViews()V
    .locals 3

    .prologue
    .line 233
    const-string v0, "VNSingleAttachFragment"

    const-string v1, "initViews()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListView:Landroid/widget/ListView;

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListView:Landroid/widget/ListView;

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mEnableList:Z

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 239
    return-void
.end method

.method private static isServiceValid()Z
    .locals 1

    .prologue
    .line 412
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_0

    .line 413
    const/4 v0, 0x0

    .line 416
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private listBinding()Z
    .locals 21

    .prologue
    .line 242
    const-string v2, "VNSingleAttachFragment"

    const-string v3, "listBinding E"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 245
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 246
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    .line 250
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getListQuery(Landroid/content/Context;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 251
    .local v16, "builder":Ljava/lang/StringBuilder;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 253
    .local v5, "selection":Ljava/lang/String;
    const/4 v2, 0x5

    new-array v12, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v12, v2

    const/4 v2, 0x1

    const-string v3, "title"

    aput-object v3, v12, v2

    const/4 v2, 0x2

    const-string v3, "date_modified"

    aput-object v3, v12, v2

    const/4 v2, 0x3

    const-string v3, "duration"

    aput-object v3, v12, v2

    const/4 v2, 0x4

    const-string v3, "_data"

    aput-object v3, v12, v2

    .line 259
    .local v12, "cols":[Ljava/lang/String;
    const/4 v2, 0x4

    new-array v13, v2, [I

    fill-array-data v13, :array_0

    .line 263
    .local v13, "to":[I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getSortQuery()Ljava/lang/String;

    move-result-object v7

    .line 267
    .local v7, "orderBy":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mATTVVMEnabled:Z

    if-eqz v2, :cond_1

    .line 268
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 269
    .local v20, "sb":Ljava/lang/StringBuilder;
    const-string v2, "date_modified"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DESC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " LIMIT 1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 273
    .end local v20    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 289
    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    if-nez v2, :cond_3

    .line 290
    const-string v2, "VNSingleAttachFragment"

    const-string v3, "listBinding() : cursor null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    new-instance v19, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/voicenote/library/subactivity/NoSdCardActivity;

    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 292
    .local v19, "i":Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 293
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 294
    const/4 v2, 0x0

    .line 318
    .end local v19    # "i":Landroid/content/Intent;
    :goto_1
    return v2

    .line 275
    :catch_0
    move-exception v17

    .line 276
    .local v17, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "VNSingleAttachFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "listBinding - SQLiteException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 278
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 279
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    goto :goto_0

    .line 281
    .end local v17    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v18

    .line 282
    .local v18, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v2, "VNSingleAttachFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "listBinding - UnsupportedOperationException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 284
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 285
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    goto/16 :goto_0

    .line 297
    .end local v18    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_3
    new-instance v8, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v10, 0x7f030031

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadioClickListener:Landroid/view/View$OnClickListener;

    invoke-direct/range {v8 .. v15}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[IILandroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    .line 300
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 302
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    invoke-static {v2, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    .line 303
    const-wide/16 v2, -0x1

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    .line 305
    :cond_4
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    const-wide/16 v8, -0x1

    cmp-long v2, v2, v8

    if-eqz v2, :cond_5

    .line 306
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->setClearAll(Z)V

    .line 307
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    invoke-virtual {v2, v8, v9}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->selectRadionButton(J)V

    .line 308
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 309
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;

    if-eqz v2, :cond_5

    .line 310
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    invoke-interface {v2, v8, v9}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;->onSingleSelect(J)V

    .line 314
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mATTVVMEnabled:Z

    if-eqz v2, :cond_6

    .line 315
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->setFromVVM(Z)V

    .line 318
    :cond_6
    const/4 v2, 0x1

    goto/16 :goto_1

    .line 259
    nop

    :array_0
    .array-data 4
        0x7f0e0067
        0x7f0e0068
        0x7f0e0069
        0x7f0e0073
    .end array-data
.end method

.method public static newInstance(Z)Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;
    .locals 3
    .param p0, "enableList"    # Z

    .prologue
    .line 85
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;-><init>()V

    .line 86
    .local v1, "f":Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 87
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "enableList"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 88
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->setArguments(Landroid/os/Bundle;)V

    .line 89
    return-object v1
.end method

.method private playItem(JZ)Z
    .locals 5
    .param p1, "id"    # J
    .param p3, "directPlay"    # Z

    .prologue
    .line 420
    const-string v1, "VNSingleAttachFragment"

    const-string v2, "playItem()"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->isServiceValid()Z

    move-result v1

    if-nez v1, :cond_0

    .line 422
    const/4 v0, 0x0

    .line 436
    :goto_0
    return v0

    .line 425
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->stopPlay()V

    .line 426
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1, p1, p2, p0, p3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->startPlay(JLjava/lang/Object;Z)Z

    move-result v0

    .line 428
    .local v0, "started":Z
    if-nez v0, :cond_1

    .line 429
    const-string v1, "VNSingleAttachFragment"

    const-string v2, "playItem failed"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 431
    :cond_1
    const-string v1, "VNSingleAttachFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "playItem() id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mPlayingId:J

    .line 433
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mPlayingId:J

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->setPlayingId(J)V

    goto :goto_0
.end method

.method public static releaseService()V
    .locals 2

    .prologue
    .line 407
    const-string v0, "VNSingleAttachFragment"

    const-string v1, "releaseService()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 409
    return-void
.end method

.method private selectRadioButton(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 358
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->selectRadionButton(J)V

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->setClearAll(Z)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 363
    return-void
.end method

.method private stopPlay()V
    .locals 2

    .prologue
    .line 440
    const-string v0, "VNSingleAttachFragment"

    const-string v1, "stopPlay()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->isServiceValid()Z

    move-result v0

    if-nez v0, :cond_1

    .line 447
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPrepared()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    :cond_2
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    goto :goto_0
.end method

.method private togglePlaying(J)V
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 450
    const-string v2, "VNSingleAttachFragment"

    const-string v3, "togglePlaying()"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->isServiceValid()Z

    move-result v2

    if-nez v2, :cond_1

    .line 468
    :cond_0
    :goto_0
    return-void

    .line 455
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 456
    .local v1, "activity":Landroid/app/Activity;
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 458
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "android.provider.MediaStore.RECORD_SOUND"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 459
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-eqz v2, :cond_3

    .line 460
    const/4 v2, 0x1

    invoke-direct {p0, p1, p2, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->playItem(JZ)Z

    .line 466
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 461
    :cond_3
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 462
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumePlay()V

    goto :goto_1

    .line 463
    :cond_4
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 464
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    goto :goto_1
.end method


# virtual methods
.method public doAttach()V
    .locals 6

    .prologue
    .line 396
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mContext:Landroid/content/Context;

    iget-wide v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getContentURI(Landroid/content/Context;J)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 397
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 398
    return-void
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method public getSelectedRadioButton()J
    .locals 2

    .prologue
    .line 382
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    return-wide v0
.end method

.method public getShowFragmentAnimator(Landroid/animation/AnimatorSet;)Landroid/animation/Animator;
    .locals 6
    .param p1, "animatorSet"    # Landroid/animation/AnimatorSet;

    .prologue
    .line 217
    invoke-virtual {p1}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v1

    .line 219
    .local v1, "children":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    .line 220
    .local v0, "animator":Landroid/animation/Animator;
    instance-of v5, v0, Landroid/animation/ObjectAnimator;

    if-eqz v5, :cond_0

    move-object v5, v0

    .line 221
    check-cast v5, Landroid/animation/ObjectAnimator;

    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->getPropertyName()Ljava/lang/String;

    move-result-object v4

    .line 222
    .local v4, "propertyName":Ljava/lang/String;
    const-string v5, "translationY"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 223
    new-instance v5, Landroid/view/animation/interpolator/SineInOut80;

    invoke-direct {v5}, Landroid/view/animation/interpolator/SineInOut80;-><init>()V

    invoke-virtual {v0, v5}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    goto :goto_0

    .line 227
    .end local v0    # "animator":Landroid/animation/Animator;
    .end local v4    # "propertyName":Ljava/lang/String;
    :cond_1
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 228
    .local v3, "newAnimator":Landroid/animation/AnimatorSet;
    invoke-virtual {v3, v1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 229
    return-object v3
.end method

.method public initListViewSelection()V
    .locals 2

    .prologue
    .line 480
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 485
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 111
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 113
    if-eqz p1, :cond_0

    .line 114
    const-string v0, "radio_button_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    .line 115
    const-string v0, "radio_button_pos"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonPos:I

    .line 117
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 387
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mContext:Landroid/content/Context;

    move-object v0, p1

    .line 388
    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;

    .line 389
    instance-of v0, p1, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 390
    check-cast v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mLibraryCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 392
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 393
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1, "arg0"    # Landroid/media/MediaPlayer;

    .prologue
    .line 472
    const-string v0, "VNSingleAttachFragment"

    const-string v1, "onCompletion()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 474
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->setPlayingId(J)V

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 476
    return-void
.end method

.method public onCreateAnimator(IZI)Landroid/animation/Animator;
    .locals 6
    .param p1, "transit"    # I
    .param p2, "enter"    # Z
    .param p3, "nextAnim"    # I

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->stopPlay()V

    .line 181
    if-nez p3, :cond_1

    .line 182
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onCreateAnimator(IZI)Landroid/animation/Animator;

    move-result-object v0

    .line 213
    :cond_0
    :goto_0
    return-object v0

    .line 184
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, p3}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v1

    check-cast v1, Landroid/animation/AnimatorSet;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getShowFragmentAnimator(Landroid/animation/AnimatorSet;)Landroid/animation/Animator;

    move-result-object v0

    .line 185
    .local v0, "anim":Landroid/animation/Animator;
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 209
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mATTVVMEnabled:Z

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 210
    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->togglePlaying(J)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 99
    const-string v2, "VNSingleAttachFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreateView() "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 101
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 102
    const-string v2, "enableList"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mEnableList:Z

    .line 105
    :cond_0
    const v2, 0x7f030019

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 106
    .local v1, "v":Landroid/view/View;
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 128
    const-string v0, "VNSingleAttachFragment"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 133
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 134
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 327
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iput-wide p4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    .line 328
    iput p3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonPos:I

    .line 329
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonPos:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->selectRadioButton(I)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    invoke-interface {v0, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;->onSingleSelect(J)V

    .line 335
    :cond_0
    long-to-int v0, p4

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->togglePlaying(J)V

    .line 336
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 173
    const-string v0, "VNSingleAttachFragment"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->stopPlay()V

    .line 175
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 176
    return-void
.end method

.method public onReset()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 160
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->listBinding()Z

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->setClearAll(Z)V

    .line 163
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 164
    iput-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->selectRadionButton(J)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 169
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 138
    const-string v0, "VNSingleAttachFragment"

    const-string v2, "onResume()"

    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 141
    sget-boolean v0, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_CHECK_ATTVN_ENABLED:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "Source"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ATT_VVM"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "Source"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mATTVVMEnabled:Z

    .line 145
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->initViews()V

    .line 146
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->listBinding()Z

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->setClearAll(Z)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->selectRadionButton(J)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonPos:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->getItemId(I)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 154
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    .line 157
    :cond_0
    return-void

    .line 141
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 121
    const-string v0, "radio_button_id"

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 122
    const-string v0, "radio_button_pos"

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonPos:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 123
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 124
    return-void
.end method

.method public selectRadioButtonId(J)V
    .locals 5
    .param p1, "id"    # J

    .prologue
    const-wide/16 v2, -0x1

    .line 366
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    .line 367
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    .line 368
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonPos:I

    .line 369
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->setPlayingId(J)V

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->setClearAll(Z)V

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonId:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->selectRadionButton(J)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VNSingleSelectSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->mRadiobuttonPos:I

    int-to-long v2, v1

    invoke-interface {v0, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment$Callbacks;->onSingleSelect(J)V

    .line 379
    :cond_0
    return-void
.end method
