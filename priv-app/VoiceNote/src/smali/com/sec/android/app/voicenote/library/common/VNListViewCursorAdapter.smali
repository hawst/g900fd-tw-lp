.class public Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "VNListViewCursorAdapter.java"

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$STTHoverPopupListener;,
        Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;,
        Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;

.field private mContext:Landroid/content/Context;

.field private mContextChild:Landroid/content/Context;

.field private mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mIsTrimMode:Z

.field private mLayout:I

.field private mSearchText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "from"    # [Ljava/lang/String;
    .param p5, "to"    # [I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 81
    invoke-direct/range {p0 .. p5}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    .line 58
    const-string v0, "VRSimpleCursorAdapter"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->TAG:Ljava/lang/String;

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContextChild:Landroid/content/Context;

    .line 61
    iput v2, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mLayout:I

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;

    .line 68
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mIsTrimMode:Z

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mSearchText:Ljava/lang/String;

    .line 82
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContext:Landroid/content/Context;

    .line 83
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContextChild:Landroid/content/Context;

    .line 84
    iput p2, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mLayout:I

    .line 85
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->LoadLabeltoMap()V

    .line 87
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "from"    # [Ljava/lang/String;
    .param p5, "to"    # [I
    .param p6, "flagRegisterContentObserver"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 92
    invoke-direct/range {p0 .. p6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 58
    const-string v0, "VRSimpleCursorAdapter"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->TAG:Ljava/lang/String;

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContextChild:Landroid/content/Context;

    .line 61
    iput v2, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mLayout:I

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;

    .line 68
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mIsTrimMode:Z

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mSearchText:Ljava/lang/String;

    .line 93
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContextChild:Landroid/content/Context;

    .line 94
    iput p2, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mLayout:I

    .line 95
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->LoadLabeltoMap()V

    .line 97
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 98
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;)Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContextChild:Landroid/content/Context;

    return-object v0
.end method

.method private getDateFormatByFormatSetting(J)Ljava/lang/String;
    .locals 5
    .param p1, "date"    # J

    .prologue
    .line 349
    const-string v0, ""

    .line 350
    .local v0, "dateString":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContextChild:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 351
    .local v1, "shortDateFormat":Ljava/text/DateFormat;
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 353
    return-object v0
.end method

.method private stringForTime(J)Ljava/lang/String;
    .locals 13
    .param p1, "timeMs"    # J

    .prologue
    const-wide/16 v10, 0x3c

    .line 339
    const-wide/16 v8, 0x3e8

    div-long v6, p1, v8

    .line 340
    .local v6, "totalSeconds":J
    rem-long v4, v6, v10

    .line 341
    .local v4, "seconds":J
    div-long v8, v6, v10

    rem-long v2, v8, v10

    .line 342
    .local v2, "minutes":J
    const-wide/16 v8, 0xe10

    div-long v0, v6, v8

    .line 344
    .local v0, "hours":J
    const-string v8, "%02d:%02d:%02d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 330
    return-void
.end method

.method public findPosition(J)I
    .locals 9
    .param p1, "id"    # J

    .prologue
    .line 470
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 471
    .local v1, "cursor":Landroid/database/Cursor;
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 472
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->TAG:Ljava/lang/String;

    const-string v6, "bindview : cursor is null"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    const/4 v4, -0x1

    .line 492
    :cond_1
    :goto_0
    return v4

    .line 476
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 478
    .local v0, "count":I
    const/4 v4, -0x1

    .line 480
    .local v4, "returnedPos":I
    const/4 v3, 0x0

    .local v3, "pos":I
    :goto_1
    if-ge v3, v0, :cond_1

    .line 482
    :try_start_0
    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 483
    const-string v5, "_id"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    cmp-long v5, v6, p1

    if-nez v5, :cond_3

    .line 484
    move v4, v3

    goto :goto_0

    .line 488
    :catch_0
    move-exception v2

    .line 480
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 39
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 129
    if-nez p2, :cond_1

    .line 130
    new-instance v18, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;

    invoke-direct/range {v18 .. v18}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;-><init>()V

    .line 131
    .local v18, "holder":Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mLayout:I

    move/from16 v35, v0

    const/16 v36, 0x0

    invoke-virtual/range {v34 .. v36}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 133
    const v34, 0x7f0e0065

    move-object/from16 v0, p2

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/LinearLayout;

    move-object/from16 v0, v34

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->listrowLayout:Landroid/widget/LinearLayout;

    .line 134
    const v34, 0x7f0e0067

    move-object/from16 v0, p2

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/TextView;

    move-object/from16 v0, v34

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    .line 135
    const v34, 0x7f0e0068

    move-object/from16 v0, p2

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/TextView;

    move-object/from16 v0, v34

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->dateText:Landroid/widget/TextView;

    .line 136
    const v34, 0x7f0e0069

    move-object/from16 v0, p2

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/TextView;

    move-object/from16 v0, v34

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->mmsText:Landroid/widget/TextView;

    .line 137
    const v34, 0x7f0e006a

    move-object/from16 v0, p2

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/ImageView;

    move-object/from16 v0, v34

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    .line 139
    const v34, 0x7f0e006e

    move-object/from16 v0, p2

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/ImageView;

    move-object/from16 v0, v34

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    .line 140
    const v34, 0x7f0e006f

    move-object/from16 v0, p2

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/ImageView;

    move-object/from16 v0, v34

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->playicon:Landroid/widget/ImageView;

    .line 141
    const v34, 0x7f0e0070

    move-object/from16 v0, p2

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/ImageView;

    move-object/from16 v0, v34

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->pauseicon:Landroid/widget/ImageView;

    .line 142
    const v34, 0x7f0e006b

    move-object/from16 v0, p2

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/ImageView;

    move-object/from16 v0, v34

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->secreticon:Landroid/widget/ImageView;

    .line 143
    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 148
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v8

    .line 149
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v34

    if-eqz v34, :cond_2

    .line 150
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->TAG:Ljava/lang/String;

    move-object/from16 v34, v0

    const-string v35, "bindview : cursor is null or closed"

    invoke-static/range {v34 .. v35}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    .end local p2    # "convertView":Landroid/view/View;
    :goto_1
    return-object p2

    .line 145
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v18    # "holder":Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;

    .restart local v18    # "holder":Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;
    goto :goto_0

    .line 154
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :cond_2
    move/from16 v0, p1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 156
    const-wide/16 v14, 0x0

    .line 160
    .local v14, "duration":J
    const-wide/16 v10, 0x0

    .line 161
    .local v10, "date":J
    const-wide/16 v20, -0x1

    .line 162
    .local v20, "id":J
    const/16 v26, -0x1

    .line 163
    .local v26, "labelindex":I
    const/16 v24, 0x0

    .line 164
    .local v24, "is_secretbox":I
    const/16 v23, 0x0

    .line 165
    .local v23, "is_memo":I
    const/16 v28, 0x0

    .line 168
    .local v28, "mimetype":Ljava/lang/String;
    :try_start_0
    const-string v34, "duration"

    move-object/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 169
    const-string v34, "title"

    move-object/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    .line 170
    .local v33, "title":Ljava/lang/String;
    const-string v34, "date_modified"

    move-object/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 171
    const-string v34, "_id"

    move-object/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 172
    const-string v34, "label_id"

    move-object/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    .line 173
    const-string v34, "is_secretbox"

    move-object/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    .line 174
    const-string v34, "mime_type"

    move-object/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 175
    const-string v34, "_data"

    move-object/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 176
    .local v29, "path":Ljava/lang/String;
    const-string v34, "is_memo"

    move-object/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v23

    .line 185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v34, v0

    if-nez v34, :cond_6

    .line 186
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, "  "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    :goto_2
    const-string v34, "%s  |  %s"

    const/16 v35, 0x2

    move/from16 v0, v35

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v35, v0

    const/16 v36, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->getDateFormatByFormatSetting(J)Ljava/lang/String;

    move-result-object v37

    aput-object v37, v35, v36

    const/16 v36, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->stringForTime(J)Ljava/lang/String;

    move-result-object v37

    aput-object v37, v35, v36

    invoke-static/range {v34 .. v35}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 203
    .local v9, "date_duration":Ljava/lang/String;
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->dateText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-static {v0, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->stringForReadDate(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 206
    .local v6, "DescDate":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->stringForTime(J)Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->stringForReadTime(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 208
    .local v7, "DescDuration":Ljava/lang/String;
    const-string v34, "%s, %s"

    const/16 v35, 0x2

    move/from16 v0, v35

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v35, v0

    const/16 v36, 0x0

    aput-object v6, v35, v36

    const/16 v36, 0x1

    aput-object v7, v35, v36

    invoke-static/range {v34 .. v35}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 209
    .local v12, "date_duration_desc":Ljava/lang/String;
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->dateText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelColor(I)Ljava/lang/Integer;

    move-result-object v25

    .line 212
    .local v25, "labelcolor":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelTitle(I)Ljava/lang/String;

    move-result-object v27

    .line 214
    .local v27, "labeltitle":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContextChild:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v34

    if-eqz v34, :cond_8

    .line 215
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->listrowLayout:Landroid/widget/LinearLayout;

    move-object/from16 v34, v0

    new-instance v35, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v36, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f090022

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v37

    invoke-direct/range {v35 .. v37}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual/range {v34 .. v35}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 218
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    const/16 v35, 0x1

    const/high16 v36, 0x42000000    # 32.0f

    invoke-virtual/range {v34 .. v36}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 219
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->dateText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    const/16 v35, 0x1

    const/high16 v36, 0x41a00000    # 20.0f

    invoke-virtual/range {v34 .. v36}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 220
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 251
    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;

    move-object/from16 v34, v0

    if-eqz v34, :cond_4

    .line 252
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->playicon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v16

    check-cast v16, Landroid/graphics/drawable/AnimationDrawable;

    .line 253
    .local v16, "frameAnimation":Landroid/graphics/drawable/AnimationDrawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;

    move-object/from16 v34, v0

    invoke-interface/range {v34 .. v34}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;->getCurrentPlayID()J

    move-result-wide v34

    cmp-long v34, v34, v20

    if-nez v34, :cond_d

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;

    move-object/from16 v34, v0

    invoke-interface/range {v34 .. v34}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;->getCurrentPlayState()I

    move-result v34

    const/16 v35, 0x17

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_a

    .line 255
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 256
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->playicon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 257
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->pauseicon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 258
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContextChild:Landroid/content/Context;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v35

    const v36, 0x7f080029

    invoke-virtual/range {v35 .. v36}, Landroid/content/res/Resources;->getColor(I)I

    move-result v35

    invoke-virtual/range {v34 .. v35}, Landroid/widget/TextView;->setTextColor(I)V

    .line 259
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    const/16 v35, 0x1

    invoke-virtual/range {v34 .. v35}, Landroid/widget/TextView;->setSelected(Z)V

    .line 282
    .end local v16    # "frameAnimation":Landroid/graphics/drawable/AnimationDrawable;
    :cond_4
    :goto_4
    const/16 v34, 0x1

    move/from16 v0, v24

    move/from16 v1, v34

    if-ne v0, v1, :cond_e

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->secreticon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    if-eqz v34, :cond_e

    .line 283
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->secreticon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 288
    :cond_5
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContextChild:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v34

    if-eqz v34, :cond_f

    .line 289
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->mmsText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/widget/TextView;->setVisibility(I)V

    .line 290
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 325
    :goto_6
    invoke-super/range {p0 .. p3}, Landroid/widget/SimpleCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_1

    .line 177
    .end local v6    # "DescDate":Ljava/lang/String;
    .end local v7    # "DescDuration":Ljava/lang/String;
    .end local v9    # "date_duration":Ljava/lang/String;
    .end local v12    # "date_duration_desc":Ljava/lang/String;
    .end local v25    # "labelcolor":Ljava/lang/Integer;
    .end local v27    # "labeltitle":Ljava/lang/String;
    .end local v29    # "path":Ljava/lang/String;
    .end local v33    # "title":Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 178
    .local v13, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v13}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto/16 :goto_1

    .line 180
    .end local v13    # "e":Landroid/database/CursorIndexOutOfBoundsException;
    :catch_1
    move-exception v13

    .line 181
    .local v13, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v13}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_1

    .line 188
    .end local v13    # "e":Ljava/lang/IllegalStateException;
    .restart local v29    # "path":Ljava/lang/String;
    .restart local v33    # "title":Ljava/lang/String;
    :cond_6
    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v32

    .line 189
    .local v32, "temp_title":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v34, v0

    sget-object v35, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v19

    .line 191
    .local v19, "index":I
    if-ltz v19, :cond_7

    .line 192
    new-instance v31, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v31

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 193
    .local v31, "spannable":Landroid/text/Spannable;
    new-instance v34, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v35

    const v36, 0x7f080029

    invoke-virtual/range {v35 .. v36}, Landroid/content/res/Resources;->getColor(I)I

    move-result v35

    invoke-direct/range {v34 .. v35}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->length()I

    move-result v35

    add-int v35, v35, v19

    const/16 v36, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, v34

    move/from16 v2, v19

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 195
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 197
    .end local v31    # "spannable":Landroid/text/Spannable;
    :cond_7
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, "  "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 222
    .end local v19    # "index":I
    .end local v32    # "temp_title":Ljava/lang/String;
    .restart local v6    # "DescDate":Ljava/lang/String;
    .restart local v7    # "DescDuration":Ljava/lang/String;
    .restart local v9    # "date_duration":Ljava/lang/String;
    .restart local v12    # "date_duration_desc":Ljava/lang/String;
    .restart local v25    # "labelcolor":Ljava/lang/Integer;
    .restart local v27    # "labeltitle":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->listrowLayout:Landroid/widget/LinearLayout;

    move-object/from16 v34, v0

    new-instance v35, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v36, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f090021

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v37

    invoke-direct/range {v35 .. v37}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual/range {v34 .. v35}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 225
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    if-eqz v34, :cond_3

    .line 226
    if-eqz v25, :cond_9

    .line 227
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Integer;->intValue()I

    move-result v35

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 228
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 229
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x1

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 236
    :goto_7
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 237
    if-nez v24, :cond_3

    .line 238
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    new-instance v35, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$1;

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$1;-><init>(Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;)V

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    .line 231
    :cond_9
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContextChild:Landroid/content/Context;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v35

    const v36, 0x7f08001b

    invoke-virtual/range {v35 .. v36}, Landroid/content/res/Resources;->getColor(I)I

    move-result v35

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 232
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const-string v35, ""

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 233
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->button:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    goto :goto_7

    .line 260
    .restart local v16    # "frameAnimation":Landroid/graphics/drawable/AnimationDrawable;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;

    move-object/from16 v34, v0

    invoke-interface/range {v34 .. v34}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;->getCurrentPlayState()I

    move-result v34

    const/16 v35, 0x18

    move/from16 v0, v34

    move/from16 v1, v35

    if-eq v0, v1, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;

    move-object/from16 v34, v0

    invoke-interface/range {v34 .. v34}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;->getCurrentPlayState()I

    move-result v34

    const/16 v35, 0x16

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_c

    .line 261
    :cond_b
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 262
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->playicon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 263
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->pauseicon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 264
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContextChild:Landroid/content/Context;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v35

    const v36, 0x7f080029

    invoke-virtual/range {v35 .. v36}, Landroid/content/res/Resources;->getColor(I)I

    move-result v35

    invoke-virtual/range {v34 .. v35}, Landroid/widget/TextView;->setTextColor(I)V

    .line 265
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    const/16 v35, 0x1

    invoke-virtual/range {v34 .. v35}, Landroid/widget/TextView;->setSelected(Z)V

    goto/16 :goto_4

    .line 267
    :cond_c
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 268
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->playicon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 269
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->pauseicon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 270
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v35

    const v36, 0x7f08002a

    invoke-virtual/range {v35 .. v36}, Landroid/content/res/Resources;->getColor(I)I

    move-result v35

    invoke-virtual/range {v34 .. v35}, Landroid/widget/TextView;->setTextColor(I)V

    .line 271
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    invoke-virtual/range {v34 .. v35}, Landroid/widget/TextView;->setSelected(Z)V

    goto/16 :goto_4

    .line 274
    :cond_d
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 275
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->playicon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 276
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->pauseicon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 277
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v35

    const v36, 0x7f08002a

    invoke-virtual/range {v35 .. v36}, Landroid/content/res/Resources;->getColor(I)I

    move-result v35

    invoke-virtual/range {v34 .. v35}, Landroid/widget/TextView;->setTextColor(I)V

    .line 278
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->titleText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    invoke-virtual/range {v34 .. v35}, Landroid/widget/TextView;->setSelected(Z)V

    goto/16 :goto_4

    .line 284
    .end local v16    # "frameAnimation":Landroid/graphics/drawable/AnimationDrawable;
    :cond_e
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->secreticon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    if-eqz v34, :cond_5

    .line 285
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->secreticon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 292
    :cond_f
    if-eqz v28, :cond_11

    const-string v34, "audio/amr"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_11

    .line 293
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->mmsText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    invoke-virtual/range {v34 .. v35}, Landroid/widget/TextView;->setVisibility(I)V

    .line 297
    :goto_8
    if-eqz v29, :cond_14

    invoke-static/range {v29 .. v29}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isM4A(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_14

    .line 298
    new-instance v17, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    move-object/from16 v0, v17

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 299
    .local v17, "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v22

    .line 300
    .local v22, "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    if-eqz v22, :cond_13

    move-object/from16 v0, v22

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z

    move/from16 v34, v0

    if-eqz v34, :cond_13

    .line 301
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 302
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const-string v35, ""

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 303
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x3

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 304
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v30

    .line 305
    .local v30, "popup":Landroid/widget/HoverPopupWindow;
    if-eqz v30, :cond_10

    .line 306
    const/16 v34, 0x1

    move-object/from16 v0, v30

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 307
    const/16 v34, 0x0

    move-object/from16 v0, v30

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 308
    new-instance v34, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$STTHoverPopupListener;

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$STTHoverPopupListener;-><init>(Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 309
    const/16 v34, 0x3133

    move-object/from16 v0, v30

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 312
    :cond_10
    const/16 v34, 0x2

    move/from16 v0, v23

    move/from16 v1, v34

    if-ne v0, v1, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mSearchText:Ljava/lang/String;

    move-object/from16 v34, v0

    if-eqz v34, :cond_12

    .line 313
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const v35, 0x7f0201a5

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_6

    .line 295
    .end local v17    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v22    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .end local v30    # "popup":Landroid/widget/HoverPopupWindow;
    :cond_11
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->mmsText:Landroid/widget/TextView;

    move-object/from16 v34, v0

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_8

    .line 315
    .restart local v17    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .restart local v22    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .restart local v30    # "popup":Landroid/widget/HoverPopupWindow;
    :cond_12
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const v35, 0x7f0201a3

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_6

    .line 317
    .end local v30    # "popup":Landroid/widget/HoverPopupWindow;
    :cond_13
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    .line 321
    .end local v17    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v22    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :cond_14
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$ViewHolder;->stticon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 497
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mIsTrimMode:Z

    if-eqz v0, :cond_0

    .line 498
    const/4 v0, 0x0

    .line 500
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/SimpleCursorAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 334
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 335
    .local v0, "view":Landroid/view/View;
    return-object v0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    if-eqz v0, :cond_0

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->LoadLabeltoMap()V

    .line 466
    :cond_0
    invoke-super {p0}, Landroid/widget/SimpleCursorAdapter;->notifyDataSetChanged()V

    .line 467
    return-void
.end method

.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "hpw"    # Landroid/widget/HoverPopupWindow;

    .prologue
    .line 358
    const v3, 0x7f0e0067

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 360
    .local v2, "titleText":Landroid/widget/TextView;
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 362
    .local v1, "text":Ljava/lang/CharSequence;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f03001f

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 363
    .local v0, "fullText":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    invoke-virtual {v2}, Landroid/widget/TextView;->isEllipsis()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 365
    if-eqz v1, :cond_0

    invoke-virtual {v2}, Landroid/widget/TextView;->isEllipsis()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 366
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 370
    :cond_0
    invoke-virtual {p2, v0}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 372
    const/4 v3, 0x1

    return v3
.end method

.method public setLabelCallback(Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;)V
    .locals 0
    .param p1, "callbacks"    # Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mCallbacks:Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter$Callbacks;

    .line 102
    return-void
.end method

.method public setSearchText(Ljava/lang/String;)V
    .locals 0
    .param p1, "searchText"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mSearchText:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public setTrimMode(Z)V
    .locals 0
    .param p1, "isTrimMode"    # Z

    .prologue
    .line 504
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/common/VNListViewCursorAdapter;->mIsTrimMode:Z

    .line 505
    return-void
.end method
