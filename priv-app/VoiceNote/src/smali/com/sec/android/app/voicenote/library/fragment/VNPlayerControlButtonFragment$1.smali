.class Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment$1;
.super Ljava/lang/Object;
.source "VNPlayerControlButtonFragment.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 170
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 171
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 172
    .local v0, "event":I
    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {v1, v2, p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hoverHapticAndFeedback(Landroid/content/Context;Landroid/media/AudioManager;Landroid/view/View;)V

    .line 175
    :cond_0
    const/4 v1, 0x0

    return v1
.end method
