.class public Lcom/sec/android/app/voicenote/common/util/TextData;
.super Ljava/lang/Object;
.source "TextData.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/sec/android/app/voicenote/common/util/TextData;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x622d451692fc707dL


# instance fields
.field public ConfidenceScore:D

.field public dataType:I

.field public duration:J

.field public elapsedTime:J

.field public mText:[Ljava/lang/String;

.field public timeStamp:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    .line 35
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    .line 36
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/voicenote/common/util/TextData;)V
    .locals 4
    .param p1, "model"    # Lcom/sec/android/app/voicenote/common/util/TextData;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    .line 39
    iget-object v1, p1, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    array-length v1, v1

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    .line 40
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p1, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 41
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    iget-object v2, p1, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    aget-object v2, v2, v0

    aput-object v2, v1, v0

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43
    :cond_0
    iget-wide v2, p1, Lcom/sec/android/app/voicenote/common/util/TextData;->ConfidenceScore:D

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->ConfidenceScore:D

    .line 44
    iget-wide v2, p1, Lcom/sec/android/app/voicenote/common/util/TextData;->timeStamp:J

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->timeStamp:J

    .line 45
    iget-wide v2, p1, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    .line 46
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/sec/android/app/voicenote/common/util/TextData;)I
    .locals 4
    .param p1, "another"    # Lcom/sec/android/app/voicenote/common/util/TextData;

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->timeStamp:J

    iget-wide v2, p1, Lcom/sec/android/app/voicenote/common/util/TextData;->timeStamp:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, Lcom/sec/android/app/voicenote/common/util/TextData;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/common/util/TextData;->compareTo(Lcom/sec/android/app/voicenote/common/util/TextData;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 55
    instance-of v2, p1, Lcom/sec/android/app/voicenote/common/util/TextData;

    if-nez v2, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 58
    check-cast v0, Lcom/sec/android/app/voicenote/common/util/TextData;

    .line 59
    .local v0, "comparedTo":Lcom/sec/android/app/voicenote/common/util/TextData;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->ConfidenceScore:D

    iget-wide v4, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->ConfidenceScore:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->timeStamp:J

    iget-wide v4, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->timeStamp:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    iget-wide v4, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 61
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public swapText(II)V
    .locals 3
    .param p1, "index1"    # I
    .param p2, "index2"    # I

    .prologue
    .line 68
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    aget-object v0, v1, p1

    .line 69
    .local v0, "temp":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    aget-object v2, v2, p2

    aput-object v2, v1, p1

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    aput-object v0, v1, p2

    .line 71
    return-void
.end method
