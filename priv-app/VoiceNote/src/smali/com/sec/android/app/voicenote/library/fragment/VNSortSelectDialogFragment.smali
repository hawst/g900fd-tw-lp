.class public Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNSortSelectDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "VNSortSelectDialogFragment"


# instance fields
.field private mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field private mListView:Landroid/widget/ListView;

.field private mViewChild:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->mListView:Landroid/widget/ListView;

    .line 50
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->mViewChild:Landroid/view/View;

    .line 55
    return-void
.end method

.method private initView()Landroid/view/View;
    .locals 5

    .prologue
    .line 100
    const/4 v2, 0x0

    .line 101
    .local v2, "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 102
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 104
    .local v1, "inflater":Landroid/view/LayoutInflater;
    if-eqz v1, :cond_0

    .line 105
    const v3, 0x7f030015

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 106
    if-eqz v2, :cond_0

    .line 107
    const v3, 0x7f0e0035

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->mListView:Landroid/widget/ListView;

    .line 108
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 112
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    return-object v2
.end method

.method private listBinding()Z
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 116
    const-string v8, "VNSortSelectDialogFragment"

    const-string v9, "listBinding"

    invoke-static {v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 119
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_1

    .line 169
    :cond_0
    :goto_0
    return v6

    .line 122
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 123
    .local v5, "resources":Landroid/content/res/Resources;
    if-eqz v5, :cond_0

    .line 127
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v6, 0x7f0b0059

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    const v6, 0x7f0b005a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    const v6, 0x7f0b00c8

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    const v6, 0x7f0b0024

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    const v6, 0x7f0b0123

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMode()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMounted(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 135
    const v6, 0x7f0b00ef

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v4, v6, [Ljava/lang/String;

    .line 139
    .local v4, "items":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    array-length v6, v4

    if-ge v3, v6, :cond_3

    .line 140
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    aput-object v6, v4, v3

    .line 139
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 143
    :cond_3
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment$2;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v8, 0x7f030024

    invoke-direct {v1, p0, v6, v8, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;Landroid/content/Context;I[Ljava/lang/String;)V

    .line 164
    .local v1, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 165
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 167
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->mListView:Landroid/widget/ListView;

    const-string v8, "sort_mode"

    invoke-static {v8}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v6, v8, v7}, Landroid/widget/ListView;->setItemChecked(IZ)V

    move v6, v7

    .line 169
    goto/16 :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 94
    const-string v0, "VNSortSelectDialogFragment"

    const-string v1, "onActivityCreated"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 96
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->listBinding()Z

    .line 97
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 59
    const-string v2, "VNSortSelectDialogFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v2, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 63
    :cond_0
    const-string v2, "sort_mode"

    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v1

    .line 64
    .local v1, "sortmode":I
    const/4 v2, 0x5

    if-ne v2, v1, :cond_2

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 66
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_2

    .line 67
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMode()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMounted(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 68
    :cond_1
    const-string v2, "sort_mode"

    invoke-static {v2, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 72
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_2
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 73
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->initView()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->mViewChild:Landroid/view/View;

    .line 79
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 80
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0b0124

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->mViewChild:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 82
    const v1, 0x7f0b0021

    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 89
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v2, 0x1

    .line 174
    const-string v0, "sort_mode"

    long-to-int v1, p4

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 176
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onUpdateState(Z)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->refreashAnim(Z)V

    .line 181
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSortSelectDialogFragment;->dismiss()V

    .line 183
    :cond_1
    return-void
.end method
