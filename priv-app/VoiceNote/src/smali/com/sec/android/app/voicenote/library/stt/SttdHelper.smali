.class public Lcom/sec/android/app/voicenote/library/stt/SttdHelper;
.super Ljava/lang/Object;
.source "SttdHelper.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final TEMP_NAME:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mInvalidInit:Z

.field private mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

.field private final mNewSTTD:[B

.field private mNewSttdLength:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/stt/M4aInfo;Landroid/content/Context;)V
    .locals 3
    .param p1, "inf"    # Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mNewSTTD:[B

    .line 28
    const-string v0, "SttdHelper"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->TAG:Ljava/lang/String;

    .line 29
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mInvalidInit:Z

    .line 30
    const-string v0, "temp3223293.m4a"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->TEMP_NAME:Ljava/lang/String;

    .line 31
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    .line 32
    iput v1, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mNewSttdLength:I

    .line 34
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mContext:Landroid/content/Context;

    .line 37
    if-eqz p1, :cond_0

    .line 38
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    .line 39
    iget-boolean v0, p1, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->usedToWrite:Z

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mInvalidInit:Z

    .line 43
    :goto_0
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mContext:Landroid/content/Context;

    .line 44
    return-void

    .line 41
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mInvalidInit:Z

    goto :goto_0

    .line 27
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x73t
        0x74t
        0x74t
        0x64t
    .end array-data
.end method

.method private exportToFile(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    const/4 v8, 0x0

    .line 263
    if-nez p1, :cond_1

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    const/16 v7, 0x2e

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    invoke-virtual {p1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 266
    .local v2, "export_filepath":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_memo.txt"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 268
    const/4 v5, 0x0

    .line 269
    .local v5, "os":Ljava/io/FileOutputStream;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 271
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 272
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 275
    :cond_2
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 278
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 281
    .local v0, "builder":Ljava/lang/StringBuilder;
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 282
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .local v6, "os":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    :try_start_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v4, v7, :cond_4

    .line 284
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget v7, v7, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    if-nez v7, :cond_3

    .line 285
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v7, v7, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 288
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/FileOutputStream;->write([B)V

    .line 290
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mContext:Landroid/content/Context;

    if-eqz v7, :cond_5

    .line 291
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mContext:Landroid/content/Context;

    invoke-static {v7, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->sendScan(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 302
    :cond_5
    :try_start_2
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-object v5, v6

    .line 304
    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 303
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v7

    move-object v5, v6

    .line 305
    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 294
    .end local v4    # "i":I
    :catch_1
    move-exception v1

    .line 296
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 302
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 303
    :catch_2
    move-exception v7

    goto/16 :goto_0

    .line 297
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v1

    .line 299
    .local v1, "e":Ljava/io/IOException;
    :goto_3
    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 302
    :try_start_6
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_0

    .line 303
    :catch_4
    move-exception v7

    goto/16 :goto_0

    .line 301
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 302
    :goto_4
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 304
    :goto_5
    throw v7

    .line 303
    :catch_5
    move-exception v8

    goto :goto_5

    .line 301
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v4    # "i":I
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 297
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v1

    move-object v5, v6

    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 294
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v1

    move-object v5, v6

    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method private updateOuterAtomsLengths(Ljava/nio/channels/FileChannel;)V
    .locals 8
    .param p1, "channel"    # Ljava/nio/channels/FileChannel;

    .prologue
    .line 235
    const-wide/16 v4, 0x0

    .line 236
    .local v4, "savePos":J
    const/4 v6, 0x4

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 237
    .local v0, "buff":Ljava/nio/ByteBuffer;
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget v6, v6, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileUdtaLength:I

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget v7, v7, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileSttdLength:I

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mNewSttdLength:I

    add-int v3, v6, v7

    .line 238
    .local v3, "newUdtaLength":I
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget v6, v6, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileMoovLength:I

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget v7, v7, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileUdtaLength:I

    sub-int/2addr v6, v7

    add-int v2, v6, v3

    .line 239
    .local v2, "newMoovLength":I
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iput v3, v6, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileUdtaLength:I

    .line 240
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iput v2, v6, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileMoovLength:I

    .line 242
    :try_start_0
    invoke-virtual {p1}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v4

    .line 244
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 245
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 246
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-wide v6, v6, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->udtaPos:J

    invoke-virtual {p1, v6, v7}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 247
    invoke-virtual {p1, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 248
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 250
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 251
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 252
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-wide v6, v6, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->moovPos:J

    invoke-virtual {p1, v6, v7}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 253
    invoke-virtual {p1, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 254
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 256
    invoke-virtual {p1, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 260
    :goto_0
    return-void

    .line 257
    :catch_0
    move-exception v1

    .line 258
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public overwriteSttd(Ljava/io/Serializable;)V
    .locals 21
    .param p1, "serializable"    # Ljava/io/Serializable;

    .prologue
    .line 47
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mInvalidInit:Z

    if-eqz v4, :cond_1

    .line 134
    .end local p1    # "serializable":Ljava/io/Serializable;
    :cond_0
    :goto_0
    return-void

    .line 51
    .restart local p1    # "serializable":Ljava/io/Serializable;
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->usedToWrite:Z

    .line 53
    new-instance v9, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v9}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 54
    .local v9, "bos":Ljava/io/ByteArrayOutputStream;
    const/4 v12, 0x0

    .line 55
    .local v12, "out":Ljava/io/ObjectOutput;
    const/4 v10, 0x0

    .line 56
    .local v10, "dataBytes":[B
    const/16 v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mNewSttdLength:I

    .line 58
    :try_start_0
    new-instance v13, Ljava/io/ObjectOutputStream;

    invoke-direct {v13, v9}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    .end local v12    # "out":Ljava/io/ObjectOutput;
    .local v13, "out":Ljava/io/ObjectOutput;
    :try_start_1
    move-object/from16 v0, p1

    invoke-interface {v13, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    .line 60
    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    .line 61
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mNewSttdLength:I

    array-length v5, v10

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mNewSttdLength:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 66
    :try_start_2
    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 67
    invoke-interface {v13}, Ljava/io/ObjectOutput;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v12, v13

    .line 72
    .end local v13    # "out":Ljava/io/ObjectOutput;
    .restart local v12    # "out":Ljava/io/ObjectOutput;
    :goto_1
    if-eqz v10, :cond_0

    .line 75
    invoke-static {v10}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v16

    .line 77
    .local v16, "sttData":Ljava/nio/ByteBuffer;
    const/4 v14, 0x0

    .line 78
    .local v14, "raf":Ljava/io/RandomAccessFile;
    const/16 v18, 0x0

    .line 79
    .local v18, "temp":Ljava/io/RandomAccessFile;
    const/4 v3, 0x0

    .line 80
    .local v3, "srcWrite":Ljava/nio/channels/FileChannel;
    const/4 v2, 0x0

    .line 82
    .local v2, "dst":Ljava/nio/channels/FileChannel;
    :try_start_3
    new-instance v15, Ljava/io/RandomAccessFile;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-object v4, v4, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->path:Ljava/lang/String;

    const-string v5, "rw"

    invoke-direct {v15, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 83
    .end local v14    # "raf":Ljava/io/RandomAccessFile;
    .local v15, "raf":Ljava/io/RandomAccessFile;
    :try_start_4
    new-instance v19, Ljava/io/RandomAccessFile;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "temp3223293.m4a"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "rw"

    move-object/from16 v0, v19

    invoke-direct {v0, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 84
    .end local v18    # "temp":Ljava/io/RandomAccessFile;
    .local v19, "temp":Ljava/io/RandomAccessFile;
    :try_start_5
    invoke-virtual {v15}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    .line 85
    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 87
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-boolean v4, v4, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasSttd:Z

    if-eqz v4, :cond_2

    .line 88
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-wide v4, v4, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->sttdPos:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget v6, v6, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileSttdLength:I

    int-to-long v6, v6

    add-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 89
    const-wide/16 v4, 0x0

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 90
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-wide v4, v4, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->sttdPos:J

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 98
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v6

    iput-wide v6, v4, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->sttdPos:J

    .line 99
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mNewSTTD:[B

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v17

    .line 100
    .local v17, "sttdBuff":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mNewSttdLength:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 101
    invoke-virtual/range {v17 .. v17}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 102
    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 105
    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 107
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v5

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v7

    move-object v4, v2

    invoke-virtual/range {v3 .. v8}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 111
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->updateOuterAtomsLengths(Ljava/nio/channels/FileChannel;)V

    .line 112
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasSttd:Z

    .line 113
    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->close()V

    .line 114
    invoke-virtual {v15}, Ljava/io/RandomAccessFile;->close()V

    .line 116
    new-instance v20, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "temp3223293.m4a"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 117
    .local v20, "toDelete":Ljava/io/File;
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 124
    :try_start_6
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 125
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 126
    invoke-virtual {v15}, Ljava/io/RandomAccessFile;->close()V

    .line 127
    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    move-object/from16 v18, v19

    .end local v19    # "temp":Ljava/io/RandomAccessFile;
    .restart local v18    # "temp":Ljava/io/RandomAccessFile;
    move-object v14, v15

    .line 131
    .end local v15    # "raf":Ljava/io/RandomAccessFile;
    .end local v17    # "sttdBuff":Ljava/nio/ByteBuffer;
    .end local v20    # "toDelete":Ljava/io/File;
    .restart local v14    # "raf":Ljava/io/RandomAccessFile;
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-object v4, v4, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->path:Ljava/lang/String;

    check-cast p1, Ljava/util/ArrayList;

    .end local p1    # "serializable":Ljava/io/Serializable;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->exportToFile(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 133
    const-string v4, "SttdHelper"

    const-string v5, "overwriteSttd has ended"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 68
    .end local v2    # "dst":Ljava/nio/channels/FileChannel;
    .end local v3    # "srcWrite":Ljava/nio/channels/FileChannel;
    .end local v12    # "out":Ljava/io/ObjectOutput;
    .end local v14    # "raf":Ljava/io/RandomAccessFile;
    .end local v16    # "sttData":Ljava/nio/ByteBuffer;
    .end local v18    # "temp":Ljava/io/RandomAccessFile;
    .restart local v13    # "out":Ljava/io/ObjectOutput;
    .restart local p1    # "serializable":Ljava/io/Serializable;
    :catch_0
    move-exception v11

    .line 69
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    move-object v12, v13

    .line 71
    .end local v13    # "out":Ljava/io/ObjectOutput;
    .restart local v12    # "out":Ljava/io/ObjectOutput;
    goto/16 :goto_1

    .line 62
    .end local v11    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v11

    .line 63
    .restart local v11    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_7
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 66
    :try_start_8
    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 67
    invoke-interface {v12}, Ljava/io/ObjectOutput;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_1

    .line 68
    :catch_2
    move-exception v11

    .line 69
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 65
    .end local v11    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 66
    :goto_5
    :try_start_9
    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 67
    invoke-interface {v12}, Ljava/io/ObjectOutput;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 70
    :goto_6
    throw v4

    .line 68
    :catch_3
    move-exception v11

    .line 69
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 92
    .end local v11    # "e":Ljava/io/IOException;
    .restart local v2    # "dst":Ljava/nio/channels/FileChannel;
    .restart local v3    # "srcWrite":Ljava/nio/channels/FileChannel;
    .restart local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v16    # "sttData":Ljava/nio/ByteBuffer;
    .restart local v19    # "temp":Ljava/io/RandomAccessFile;
    :cond_2
    :try_start_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-wide v4, v4, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->udtaPos:J

    const-wide/16 v6, 0x8

    add-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 94
    const-wide/16 v4, 0x0

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 95
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-wide v4, v4, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->udtaPos:J

    const-wide/16 v6, 0x8

    add-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    goto/16 :goto_2

    .line 119
    :catch_4
    move-exception v11

    move-object/from16 v18, v19

    .end local v19    # "temp":Ljava/io/RandomAccessFile;
    .restart local v18    # "temp":Ljava/io/RandomAccessFile;
    move-object v14, v15

    .line 120
    .end local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v11    # "e":Ljava/io/IOException;
    .restart local v14    # "raf":Ljava/io/RandomAccessFile;
    :goto_7
    :try_start_b
    const-string v4, "SttdHelper"

    const-string v5, "Error writing STTD to file"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 124
    :try_start_c
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 125
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 126
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V

    .line 127
    invoke-virtual/range {v18 .. v18}, Ljava/io/RandomAccessFile;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    goto :goto_3

    .line 128
    :catch_5
    move-exception v4

    goto :goto_3

    .end local v11    # "e":Ljava/io/IOException;
    .end local v14    # "raf":Ljava/io/RandomAccessFile;
    .end local v18    # "temp":Ljava/io/RandomAccessFile;
    .restart local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v17    # "sttdBuff":Ljava/nio/ByteBuffer;
    .restart local v19    # "temp":Ljava/io/RandomAccessFile;
    .restart local v20    # "toDelete":Ljava/io/File;
    :catch_6
    move-exception v4

    move-object/from16 v18, v19

    .end local v19    # "temp":Ljava/io/RandomAccessFile;
    .restart local v18    # "temp":Ljava/io/RandomAccessFile;
    move-object v14, v15

    .line 129
    .end local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v14    # "raf":Ljava/io/RandomAccessFile;
    goto/16 :goto_3

    .line 123
    .end local v17    # "sttdBuff":Ljava/nio/ByteBuffer;
    .end local v20    # "toDelete":Ljava/io/File;
    :catchall_1
    move-exception v4

    .line 124
    :goto_8
    :try_start_d
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 125
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 126
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V

    .line 127
    invoke-virtual/range {v18 .. v18}, Ljava/io/RandomAccessFile;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_7

    .line 128
    :goto_9
    throw v4

    :catch_7
    move-exception v5

    goto :goto_9

    .line 123
    .end local v14    # "raf":Ljava/io/RandomAccessFile;
    .restart local v15    # "raf":Ljava/io/RandomAccessFile;
    :catchall_2
    move-exception v4

    move-object v14, v15

    .end local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v14    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_8

    .end local v14    # "raf":Ljava/io/RandomAccessFile;
    .end local v18    # "temp":Ljava/io/RandomAccessFile;
    .restart local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v19    # "temp":Ljava/io/RandomAccessFile;
    :catchall_3
    move-exception v4

    move-object/from16 v18, v19

    .end local v19    # "temp":Ljava/io/RandomAccessFile;
    .restart local v18    # "temp":Ljava/io/RandomAccessFile;
    move-object v14, v15

    .end local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v14    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_8

    .line 119
    :catch_8
    move-exception v11

    goto :goto_7

    .end local v14    # "raf":Ljava/io/RandomAccessFile;
    .restart local v15    # "raf":Ljava/io/RandomAccessFile;
    :catch_9
    move-exception v11

    move-object v14, v15

    .end local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v14    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_7

    .line 65
    .end local v2    # "dst":Ljava/nio/channels/FileChannel;
    .end local v3    # "srcWrite":Ljava/nio/channels/FileChannel;
    .end local v12    # "out":Ljava/io/ObjectOutput;
    .end local v14    # "raf":Ljava/io/RandomAccessFile;
    .end local v16    # "sttData":Ljava/nio/ByteBuffer;
    .end local v18    # "temp":Ljava/io/RandomAccessFile;
    .restart local v13    # "out":Ljava/io/ObjectOutput;
    :catchall_4
    move-exception v4

    move-object v12, v13

    .end local v13    # "out":Ljava/io/ObjectOutput;
    .restart local v12    # "out":Ljava/io/ObjectOutput;
    goto :goto_5

    .line 62
    .end local v12    # "out":Ljava/io/ObjectOutput;
    .restart local v13    # "out":Ljava/io/ObjectOutput;
    :catch_a
    move-exception v11

    move-object v12, v13

    .end local v13    # "out":Ljava/io/ObjectOutput;
    .restart local v12    # "out":Ljava/io/ObjectOutput;
    goto/16 :goto_4
.end method

.method public readSttd()Ljava/io/Serializable;
    .locals 18

    .prologue
    .line 137
    const/4 v11, 0x0

    .line 139
    .local v11, "result":Ljava/io/Serializable;
    const/4 v4, 0x0

    .line 140
    .local v4, "countRead":I
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mInvalidInit:Z

    if-eqz v13, :cond_1

    move-object v13, v11

    .line 188
    :cond_0
    :goto_0
    return-object v13

    .line 143
    :cond_1
    const/4 v9, 0x0

    .line 144
    .local v9, "raf":Ljava/io/RandomAccessFile;
    const/4 v12, 0x0

    .line 146
    .local v12, "srcRead":Ljava/nio/channels/FileChannel;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-boolean v13, v13, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasSttd:Z

    if-eqz v13, :cond_6

    .line 147
    new-instance v10, Ljava/io/RandomAccessFile;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-object v13, v13, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->path:Ljava/lang/String;

    const-string v14, "r"

    invoke-direct {v10, v13, v14}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 148
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .local v10, "raf":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v12

    .line 149
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget v13, v13, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileSttdLength:I

    add-int/lit8 v13, v13, -0x8

    invoke-static {v13}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 150
    .local v3, "buff":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-wide v14, v13, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->sttdPos:J

    const-wide/16 v16, 0x8

    add-long v14, v14, v16

    invoke-virtual {v12, v14, v15}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 151
    invoke-virtual {v12, v3}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    .line 152
    if-gez v4, :cond_3

    .line 153
    const/4 v13, 0x0

    .line 181
    if-eqz v12, :cond_2

    .line 182
    :try_start_2
    invoke-virtual {v12}, Ljava/nio/channels/FileChannel;->close()V

    .line 183
    :cond_2
    if-eqz v10, :cond_0

    .line 184
    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 185
    :catch_0
    move-exception v14

    goto :goto_0

    .line 155
    :cond_3
    :try_start_3
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 156
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    .line 157
    .local v5, "dataBytes":[B
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 158
    .local v2, "bis":Ljava/io/ByteArrayInputStream;
    const/4 v7, 0x0

    .line 160
    .local v7, "in":Ljava/io/ObjectInput;
    :try_start_4
    new-instance v8, Ljava/io/ObjectInputStream;

    invoke-direct {v8, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 161
    .end local v7    # "in":Ljava/io/ObjectInput;
    .local v8, "in":Ljava/io/ObjectInput;
    :try_start_5
    invoke-interface {v8}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v13

    move-object v0, v13

    check-cast v0, Ljava/io/Serializable;

    move-object v11, v0
    :try_end_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 165
    if-eqz v2, :cond_4

    .line 166
    :try_start_6
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->close()V

    .line 168
    :cond_4
    if-eqz v8, :cond_f

    .line 169
    const-string v13, "SttdHelper"

    const-string v14, "Error reading object data from file"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    invoke-interface {v8}, Ljava/io/ObjectInput;->close()V

    move-object v7, v8

    .line 173
    .end local v8    # "in":Ljava/io/ObjectInput;
    .restart local v7    # "in":Ljava/io/ObjectInput;
    :cond_5
    :goto_1
    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-object v9, v10

    .line 181
    .end local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .end local v3    # "buff":Ljava/nio/ByteBuffer;
    .end local v5    # "dataBytes":[B
    .end local v7    # "in":Ljava/io/ObjectInput;
    .end local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    :cond_6
    if-eqz v12, :cond_7

    .line 182
    :try_start_7
    invoke-virtual {v12}, Ljava/nio/channels/FileChannel;->close()V

    .line 183
    :cond_7
    if-eqz v9, :cond_8

    .line 184
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    :cond_8
    :goto_2
    move-object v13, v11

    .line 188
    goto/16 :goto_0

    .line 162
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "buff":Ljava/nio/ByteBuffer;
    .restart local v5    # "dataBytes":[B
    .restart local v7    # "in":Ljava/io/ObjectInput;
    .restart local v10    # "raf":Ljava/io/RandomAccessFile;
    :catch_1
    move-exception v6

    .line 163
    .local v6, "e":Ljava/lang/ClassNotFoundException;
    :goto_3
    :try_start_8
    invoke-virtual {v6}, Ljava/lang/ClassNotFoundException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 165
    if-eqz v2, :cond_9

    .line 166
    :try_start_9
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->close()V

    .line 168
    :cond_9
    if-eqz v7, :cond_5

    .line 169
    const-string v13, "SttdHelper"

    const-string v14, "Error reading object data from file"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    invoke-interface {v7}, Ljava/io/ObjectInput;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_1

    .line 175
    .end local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .end local v3    # "buff":Ljava/nio/ByteBuffer;
    .end local v5    # "dataBytes":[B
    .end local v6    # "e":Ljava/lang/ClassNotFoundException;
    .end local v7    # "in":Ljava/io/ObjectInput;
    :catch_2
    move-exception v6

    move-object v9, v10

    .line 176
    .end local v10    # "raf":Ljava/io/RandomAccessFile;
    .local v6, "e":Ljava/io/IOException;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    :goto_4
    :try_start_a
    const-string v13, "SttdHelper"

    const-string v14, "Error reading data from file"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    const/4 v11, 0x0

    .line 178
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 181
    if-eqz v12, :cond_a

    .line 182
    :try_start_b
    invoke-virtual {v12}, Ljava/nio/channels/FileChannel;->close()V

    .line 183
    :cond_a
    if-eqz v9, :cond_8

    .line 184
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3

    goto :goto_2

    .line 185
    :catch_3
    move-exception v13

    goto :goto_2

    .line 165
    .end local v6    # "e":Ljava/io/IOException;
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "buff":Ljava/nio/ByteBuffer;
    .restart local v5    # "dataBytes":[B
    .restart local v7    # "in":Ljava/io/ObjectInput;
    .restart local v10    # "raf":Ljava/io/RandomAccessFile;
    :catchall_0
    move-exception v13

    :goto_5
    if-eqz v2, :cond_b

    .line 166
    :try_start_c
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->close()V

    .line 168
    :cond_b
    if-eqz v7, :cond_c

    .line 169
    const-string v14, "SttdHelper"

    const-string v15, "Error reading object data from file"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    invoke-interface {v7}, Ljava/io/ObjectInput;->close()V

    :cond_c
    throw v13
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 180
    .end local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .end local v3    # "buff":Ljava/nio/ByteBuffer;
    .end local v5    # "dataBytes":[B
    .end local v7    # "in":Ljava/io/ObjectInput;
    :catchall_1
    move-exception v13

    move-object v9, v10

    .line 181
    .end local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    :goto_6
    if-eqz v12, :cond_d

    .line 182
    :try_start_d
    invoke-virtual {v12}, Ljava/nio/channels/FileChannel;->close()V

    .line 183
    :cond_d
    if-eqz v9, :cond_e

    .line 184
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4

    .line 185
    :cond_e
    :goto_7
    throw v13

    :catch_4
    move-exception v14

    goto :goto_7

    .line 180
    :catchall_2
    move-exception v13

    goto :goto_6

    .line 175
    :catch_5
    move-exception v6

    goto :goto_4

    .line 185
    :catch_6
    move-exception v13

    goto :goto_2

    .line 165
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "buff":Ljava/nio/ByteBuffer;
    .restart local v5    # "dataBytes":[B
    .restart local v8    # "in":Ljava/io/ObjectInput;
    .restart local v10    # "raf":Ljava/io/RandomAccessFile;
    :catchall_3
    move-exception v13

    move-object v7, v8

    .end local v8    # "in":Ljava/io/ObjectInput;
    .restart local v7    # "in":Ljava/io/ObjectInput;
    goto :goto_5

    .line 162
    .end local v7    # "in":Ljava/io/ObjectInput;
    .restart local v8    # "in":Ljava/io/ObjectInput;
    :catch_7
    move-exception v6

    move-object v7, v8

    .end local v8    # "in":Ljava/io/ObjectInput;
    .restart local v7    # "in":Ljava/io/ObjectInput;
    goto :goto_3

    .end local v7    # "in":Ljava/io/ObjectInput;
    .restart local v8    # "in":Ljava/io/ObjectInput;
    :cond_f
    move-object v7, v8

    .end local v8    # "in":Ljava/io/ObjectInput;
    .restart local v7    # "in":Ljava/io/ObjectInput;
    goto :goto_1
.end method

.method public removeSttd()V
    .locals 13

    .prologue
    .line 192
    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mInvalidInit:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-boolean v2, v2, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasSttd:Z

    if-nez v2, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    const/4 v8, 0x0

    .line 195
    .local v8, "raf":Ljava/io/RandomAccessFile;
    const/4 v10, 0x0

    .line 196
    .local v10, "temp":Ljava/io/RandomAccessFile;
    const/4 v1, 0x0

    .line 197
    .local v1, "srcWrite":Ljava/nio/channels/FileChannel;
    const/4 v0, 0x0

    .line 199
    .local v0, "dst":Ljava/nio/channels/FileChannel;
    :try_start_0
    new-instance v9, Ljava/io/RandomAccessFile;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->path:Ljava/lang/String;

    const-string v3, "rw"

    invoke-direct {v9, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    .end local v8    # "raf":Ljava/io/RandomAccessFile;
    .local v9, "raf":Ljava/io/RandomAccessFile;
    :try_start_1
    new-instance v11, Ljava/io/RandomAccessFile;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "temp3223293.m4a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "rw"

    invoke-direct {v11, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 201
    .end local v10    # "temp":Ljava/io/RandomAccessFile;
    .local v11, "temp":Ljava/io/RandomAccessFile;
    :try_start_2
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 202
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 204
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-wide v2, v2, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->sttdPos:J

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget v4, v4, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->fileSttdLength:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 205
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 207
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    iget-wide v2, v2, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->sttdPos:J

    invoke-virtual {v1, v2, v3}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 208
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v3

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v5

    move-object v2, v0

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 209
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/nio/channels/FileChannel;->truncate(J)Ljava/nio/channels/FileChannel;

    .line 211
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mNewSttdLength:I

    .line 212
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->updateOuterAtomsLengths(Ljava/nio/channels/FileChannel;)V

    .line 213
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->hasSttd:Z

    .line 214
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mM4aInfo:Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/sec/android/app/voicenote/library/stt/M4aInfo;->usedToWrite:Z

    .line 215
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->mInvalidInit:Z

    .line 217
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->close()V

    .line 218
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V

    .line 219
    new-instance v12, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "temp3223293.m4a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v12, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 220
    .local v12, "toDelete":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 226
    :try_start_3
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 227
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V

    .line 228
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V

    .line 229
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-object v10, v11

    .end local v11    # "temp":Ljava/io/RandomAccessFile;
    .restart local v10    # "temp":Ljava/io/RandomAccessFile;
    move-object v8, v9

    .line 230
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "raf":Ljava/io/RandomAccessFile;
    goto/16 :goto_0

    .end local v8    # "raf":Ljava/io/RandomAccessFile;
    .end local v10    # "temp":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v11    # "temp":Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v2

    move-object v10, v11

    .end local v11    # "temp":Ljava/io/RandomAccessFile;
    .restart local v10    # "temp":Ljava/io/RandomAccessFile;
    move-object v8, v9

    .line 231
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "raf":Ljava/io/RandomAccessFile;
    goto/16 :goto_0

    .line 221
    .end local v12    # "toDelete":Ljava/io/File;
    :catch_1
    move-exception v7

    .line 222
    .local v7, "e":Ljava/io/IOException;
    :goto_1
    :try_start_4
    const-string v2, "SttdHelper"

    const-string v3, "Error removing Sttd from file"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 226
    :try_start_5
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 227
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V

    .line 228
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V

    .line 229
    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 230
    :catch_2
    move-exception v2

    goto/16 :goto_0

    .line 225
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 226
    :goto_2
    :try_start_6
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 227
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V

    .line 228
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V

    .line 229
    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 230
    :goto_3
    throw v2

    :catch_3
    move-exception v3

    goto :goto_3

    .line 225
    .end local v8    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v2

    move-object v8, v9

    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_2

    .end local v8    # "raf":Ljava/io/RandomAccessFile;
    .end local v10    # "temp":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v11    # "temp":Ljava/io/RandomAccessFile;
    :catchall_2
    move-exception v2

    move-object v10, v11

    .end local v11    # "temp":Ljava/io/RandomAccessFile;
    .restart local v10    # "temp":Ljava/io/RandomAccessFile;
    move-object v8, v9

    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_2

    .line 221
    .end local v8    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    :catch_4
    move-exception v7

    move-object v8, v9

    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_1

    .end local v8    # "raf":Ljava/io/RandomAccessFile;
    .end local v10    # "temp":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v11    # "temp":Ljava/io/RandomAccessFile;
    :catch_5
    move-exception v7

    move-object v10, v11

    .end local v11    # "temp":Ljava/io/RandomAccessFile;
    .restart local v10    # "temp":Ljava/io/RandomAccessFile;
    move-object v8, v9

    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_1
.end method
