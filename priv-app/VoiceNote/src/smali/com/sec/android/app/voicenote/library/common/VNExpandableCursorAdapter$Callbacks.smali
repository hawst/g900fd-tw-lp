.class public interface abstract Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$Callbacks;
.super Ljava/lang/Object;
.source "VNExpandableCursorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract getCurrentPlayID()J
.end method

.method public abstract getCurrentPlayState()I
.end method

.method public abstract isItemChecked(I)Z
.end method

.method public abstract onExpandButtonClicked(JI)V
.end method

.method public abstract onLabelButtonClicked(J)V
.end method

.method public abstract refreshOptionsMenu()V
.end method
