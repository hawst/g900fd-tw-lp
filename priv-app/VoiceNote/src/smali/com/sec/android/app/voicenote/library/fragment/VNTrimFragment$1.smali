.class Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "VNTrimFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    const v8, 0x3d4ccccd    # 0.05f

    .line 174
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v6

    float-to-int v5, v6

    .line 175
    .local v5, "x":I
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$000()Ljava/util/ArrayList;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 176
    const/4 v2, 0x0

    .line 177
    .local v2, "closestBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    const/4 v3, 0x0

    .line 178
    .local v3, "closestBookmarkTime":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$000()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v4, v6, :cond_2

    .line 179
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$000()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v6

    invoke-virtual {v7, v6}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getPositionForHander(I)I

    move-result v6

    sub-int/2addr v6, v5

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-long v0, v6

    .line 180
    .local v0, "bookmarkToClickEventDist":J
    if-nez v2, :cond_1

    .line 181
    long-to-float v6, v0

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v8

    cmpg-float v6, v6, v7

    if-gez v6, :cond_0

    .line 182
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$000()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "closestBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    check-cast v2, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 183
    .restart local v2    # "closestBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v3

    .line 178
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 186
    :cond_1
    long-to-float v6, v0

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$100(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v8

    cmpg-float v6, v6, v7

    if-gez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v6, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getPositionForHander(I)I

    move-result v6

    sub-int/2addr v6, v5

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-long v6, v6

    cmp-long v6, v0, v6

    if-gez v6, :cond_0

    .line 188
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$000()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "closestBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    check-cast v2, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 189
    .restart local v2    # "closestBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v3

    goto :goto_1

    .line 193
    .end local v0    # "bookmarkToClickEventDist":J
    :cond_2
    if-eqz v2, :cond_3

    .line 194
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v6

    sub-int v6, v3, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getEndTime()I

    move-result v7

    sub-int v7, v3, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    if-ge v6, v7, :cond_4

    .line 195
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/ImageView;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v8, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getPositionForHander(I)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;
    invoke-static {v9}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/SeekBar;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    div-int/lit8 v9, v9, 0x4

    sub-int/2addr v8, v9

    invoke-virtual {v6, v7, v8, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->moveHandler(Landroid/view/View;II)V

    .line 201
    .end local v2    # "closestBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    .end local v3    # "closestBookmarkTime":I
    .end local v4    # "i":I
    :cond_3
    :goto_2
    const/4 v6, 0x0

    return v6

    .line 197
    .restart local v2    # "closestBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    .restart local v3    # "closestBookmarkTime":I
    .restart local v4    # "i":I
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightHandle:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/ImageView;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v8, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getPositionForHander(I)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;
    invoke-static {v9}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/SeekBar;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    sub-int/2addr v8, v9

    invoke-virtual {v6, v7, v8, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->moveHandler(Landroid/view/View;II)V

    goto :goto_2
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 169
    const/4 v0, 0x1

    return v0
.end method
