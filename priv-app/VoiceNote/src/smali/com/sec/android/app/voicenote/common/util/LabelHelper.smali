.class public Lcom/sec/android/app/voicenote/common/util/LabelHelper;
.super Ljava/lang/Object;
.source "LabelHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/util/LabelHelper$LabelDataBase;,
        Lcom/sec/android/app/voicenote/common/util/LabelHelper$LabelColumn;
    }
.end annotation


# static fields
.field public static final LabelDBName:Ljava/lang/String; = "label.db"

.field public static final NO_MATCH_LABEL:I = -0x1

.field private static final TAG:Ljava/lang/String; = "LabelHelper"

.field public static final _TABLENAME:Ljava/lang/String; = "labels"

.field private static final labelList:Ljava/util/Map;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/voicenote/library/common/LabelInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static mDB:Landroid/database/sqlite/SQLiteDatabase;


# instance fields
.field private labeldbHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper$LabelDataBase;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labeldbHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper$LabelDataBase;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mContext:Landroid/content/Context;

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mContext:Landroid/content/Context;

    .line 59
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper$LabelDataBase;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper$LabelDataBase;-><init>(Lcom/sec/android/app/voicenote/common/util/LabelHelper;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labeldbHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper$LabelDataBase;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/util/LabelHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getListQuery()Ljava/lang/StringBuilder;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "select _id, COLOR, TITLE from labels"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    return-object v0
.end method

.method public static getListQueryfromSecondRecord()Ljava/lang/StringBuilder;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "select _id, COLOR, TITLE from labels where not _id==0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    return-object v0
.end method


# virtual methods
.method public LoadLabeltoMap()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v3, 0x0

    .line 147
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 152
    const-string v0, "LabelHelper"

    const-string v1, "LoadLabeltoMap"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 157
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v10

    const-string v0, "COLOR"

    aput-object v0, v2, v11

    const-string v0, "TITLE"

    aput-object v0, v2, v12

    .line 158
    .local v2, "columns":[Ljava/lang/String;
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "labels"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 160
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 162
    :cond_2
    new-instance v9, Lcom/sec/android/app/voicenote/library/common/LabelInfo;

    invoke-interface {v8, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-interface {v8, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v9, v0, v1}, Lcom/sec/android/app/voicenote/library/common/LabelInfo;-><init>(ILjava/lang/String;)V

    .line 163
    .local v9, "labelItem":Lcom/sec/android/app/voicenote/library/common/LabelInfo;
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 167
    .end local v9    # "labelItem":Lcom/sec/android/app/voicenote/library/common/LabelInfo;
    :cond_3
    if-eqz v8, :cond_4

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_4

    .line 168
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 169
    const/4 v8, 0x0

    .line 171
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    goto :goto_0
.end method

.method public close()V
    .locals 2

    .prologue
    .line 83
    const-string v0, "LabelHelper"

    const-string v1, "close"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 85
    return-void
.end method

.method public deleteColumn(I)Z
    .locals 5
    .param p1, "id"    # I

    .prologue
    const/4 v0, 0x0

    .line 126
    const-string v1, "LabelHelper"

    const-string v2, "deleteColumn"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v1

    if-nez v1, :cond_1

    .line 128
    const-string v1, "LabelHelper"

    const-string v2, "DB did not opened"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_0
    :goto_0
    return v0

    .line 131
    :cond_1
    const-string v1, "LabelHelper"

    const-string v2, "deleteColumn"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "labels"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_2

    const/4 v0, 0x1

    .line 134
    .local v0, "delSuccess":Z
    :cond_2
    if-eqz v0, :cond_0

    .line 135
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getDB()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method public getLabelColor(I)Ljava/lang/Integer;
    .locals 4
    .param p1, "id"    # I

    .prologue
    const/4 v2, 0x0

    .line 176
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    move-object v0, v2

    .line 183
    :cond_1
    :goto_0
    return-object v0

    .line 178
    :cond_2
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3

    move-object v0, v2

    .line 179
    goto :goto_0

    .line 181
    :cond_3
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/library/common/LabelInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/common/LabelInfo;->getColor()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 182
    .local v0, "retVal":Ljava/lang/Integer;
    if-nez v0, :cond_1

    move-object v0, v2

    goto :goto_0
.end method

.method public getLabelTitle(I)Ljava/lang/String;
    .locals 4
    .param p1, "id"    # I

    .prologue
    const/4 v2, 0x0

    .line 188
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    move-object v0, v2

    .line 195
    :cond_1
    :goto_0
    return-object v0

    .line 190
    :cond_2
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3

    move-object v0, v2

    .line 191
    goto :goto_0

    .line 193
    :cond_3
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/library/common/LabelInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/common/LabelInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 194
    .local v0, "retVal":Ljava/lang/String;
    if-nez v0, :cond_1

    move-object v0, v2

    goto :goto_0
.end method

.method public insertColumn(Ljava/lang/String;Ljava/lang/String;)J
    .locals 8
    .param p1, "color"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, -0x1

    .line 89
    const-string v3, "LabelHelper"

    const-string v6, "insertColumn"

    invoke-static {v3, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    sget-object v3, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v3

    if-nez v3, :cond_1

    .line 91
    const-string v3, "LabelHelper"

    const-string v6, "DB did not opened"

    invoke-static {v3, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v4

    .line 103
    :cond_0
    :goto_0
    return-wide v0

    .line 95
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 96
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "COLOR"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v3, "TITLE"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    sget-object v3, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "labels"

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 100
    .local v0, "insertedId":J
    cmp-long v3, v0, v4

    if-lez v3, :cond_0

    .line 101
    sget-object v3, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    long-to-int v4, v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/voicenote/library/common/LabelInfo;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {v5, v6, p2}, Lcom/sec/android/app/voicenote/library/common/LabelInfo;-><init>(ILjava/lang/String;)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public isExitSameTitle(Ljava/lang/String;)Z
    .locals 14
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v13, 0x0

    .line 200
    const-string v0, "LabelHelper"

    const-string v1, "isExitSameTitle"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const/4 v3, 0x0

    .line 203
    .local v3, "selection":Ljava/lang/String;
    const/4 v12, 0x0

    .line 204
    .local v12, "retVal":Z
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 205
    .local v8, "builder":Ljava/lang/StringBuilder;
    const-string v0, "TITLE"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIKE ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 208
    new-array v4, v5, [Ljava/lang/String;

    aput-object p1, v4, v13

    .line 210
    .local v4, "selectionArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 211
    new-array v2, v5, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v13

    .line 212
    .local v2, "columns":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 214
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "labels"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v9

    .line 228
    :cond_0
    :goto_0
    if-nez v9, :cond_1

    move v0, v13

    .line 234
    :goto_1
    return v0

    .line 215
    :catch_0
    move-exception v10

    .line 216
    .local v10, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "LabelHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isExitSameTitle - SQLiteException :"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 218
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 219
    const/4 v9, 0x0

    goto :goto_0

    .line 221
    .end local v10    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v11

    .line 222
    .local v11, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v0, "LabelHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isExitSameTitle - UnsupportedOperationException :"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 224
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 225
    const/4 v9, 0x0

    goto :goto_0

    .line 230
    .end local v11    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v12, 0x1

    .line 232
    :cond_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    move v0, v12

    .line 234
    goto :goto_1
.end method

.method public open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 77
    const-string v0, "LabelHelper"

    const-string v1, "open"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labeldbHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper$LabelDataBase;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper$LabelDataBase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 79
    return-object p0
.end method

.method public updateColumn(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "id"    # I
    .param p2, "color"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 108
    const-string v2, "LabelHelper"

    const-string v3, "updateColumn"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-nez v2, :cond_1

    .line 110
    const-string v2, "LabelHelper"

    const-string v3, "DB did not opened"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    :goto_0
    return v0

    .line 113
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 114
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "COLOR"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v2, "TITLE"

    invoke-virtual {v1, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "labels"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_2

    const/4 v0, 0x1

    .line 118
    .local v0, "rowAffected":Z
    :cond_2
    if-eqz v0, :cond_0

    .line 119
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->labelList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/voicenote/library/common/LabelInfo;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {v4, v5, p3}, Lcom/sec/android/app/voicenote/library/common/LabelInfo;-><init>(ILjava/lang/String;)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
