.class public Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "LabelSimpleCursorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;,
        Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$ManageLabelCallback;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LabelSimpleCursorAdapter"

.field private static mCheckBoxes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/CheckBox;",
            ">;"
        }
    .end annotation
.end field

.field private static mlabelCountHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCategorySelected:Z

.field private mClearAll:Z

.field mContentObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mFilterbyCallback:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;

.field private mFrom:[Ljava/lang/String;

.field private mIsCheckBoxAnimating:Z

.field private mIsSelectionMode:Z

.field private mLayoutResourceId:I

.field private mMode:Z

.field private mPosition:I

.field private mTo:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mlabelCountHashMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "from"    # [Ljava/lang/String;
    .param p5, "to"    # [I

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 90
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 51
    iput-boolean v6, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mClearAll:Z

    .line 52
    iput-boolean v6, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mMode:Z

    .line 53
    iput v8, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mPosition:I

    .line 54
    iput-boolean v6, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mCategorySelected:Z

    .line 55
    iput v8, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mLayoutResourceId:I

    .line 57
    iput-boolean v6, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mIsSelectionMode:Z

    .line 58
    iput-boolean v6, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mIsCheckBoxAnimating:Z

    .line 59
    iput-object v7, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mFilterbyCallback:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;

    .line 62
    iput-object v7, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mFrom:[Ljava/lang/String;

    .line 63
    iput-object v7, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mTo:[I

    .line 121
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$1;-><init>(Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mContentObserver:Landroid/database/ContentObserver;

    .line 91
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mContext:Landroid/content/Context;

    .line 92
    iput p2, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mLayoutResourceId:I

    .line 93
    iput-object p4, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mFrom:[Ljava/lang/String;

    .line 94
    iput-object p5, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mTo:[I

    .line 95
    invoke-static {p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileCountGroupByLabel(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mlabelCountHashMap:Ljava/util/HashMap;

    .line 96
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-interface {p3, v0, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 97
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mContentObserver:Landroid/database/ContentObserver;

    invoke-interface {p3, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 100
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "from"    # [Ljava/lang/String;
    .param p5, "to"    # [I
    .param p6, "flagRegisterContentObserver"    # I

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 104
    invoke-direct/range {p0 .. p6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 51
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mClearAll:Z

    .line 52
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mMode:Z

    .line 53
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mPosition:I

    .line 54
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mCategorySelected:Z

    .line 55
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mLayoutResourceId:I

    .line 57
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mIsSelectionMode:Z

    .line 58
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mIsCheckBoxAnimating:Z

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mFilterbyCallback:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mFrom:[Ljava/lang/String;

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mTo:[I

    .line 121
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$1;-><init>(Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mContentObserver:Landroid/database/ContentObserver;

    .line 105
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mContext:Landroid/content/Context;

    .line 106
    iput p2, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mLayoutResourceId:I

    .line 107
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;)Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mFilterbyCallback:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mIsCheckBoxAnimating:Z

    return p1
.end method

.method static synthetic access$200()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mIsSelectionMode:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private addCheckBox(Landroid/widget/CheckBox;)V
    .locals 2
    .param p1, "cb"    # Landroid/widget/CheckBox;

    .prologue
    .line 284
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 285
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0xe

    if-le v0, v1, :cond_0

    .line 286
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 288
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 290
    :cond_1
    return-void
.end method

.method private declared-synchronized controlCheckbox(Landroid/widget/CheckBox;I)V
    .locals 6
    .param p1, "cb"    # Landroid/widget/CheckBox;
    .param p2, "position"    # I

    .prologue
    .line 293
    monitor-enter p0

    if-nez p1, :cond_0

    .line 325
    :goto_0
    monitor-exit p0

    return-void

    .line 297
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->addCheckBox(Landroid/widget/CheckBox;)V

    .line 299
    const/4 v1, 0x0

    .line 300
    .local v1, "marginLeft":I
    const/4 v2, 0x0

    .line 301
    .local v2, "marginRight":I
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mIsCheckBoxAnimating:Z

    if-nez v3, :cond_2

    .line 303
    iget-boolean v3, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mIsSelectionMode:Z

    if-eqz v3, :cond_3

    .line 304
    const/4 v2, 0x0

    .line 305
    const/4 v1, 0x0

    .line 311
    :goto_1
    invoke-virtual {p1}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 312
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-nez v2, :cond_1

    .line 313
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mContext:Landroid/content/Context;

    const v4, 0x418b3333    # 17.4f

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->convertDPtoPX(Landroid/content/Context;FLandroid/util/DisplayMetrics;)I

    move-result v1

    .line 315
    :cond_1
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 316
    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 319
    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mContext:Landroid/content/Context;

    instance-of v3, v3, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$ManageLabelCallback;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$ManageLabelCallback;

    invoke-interface {v3, p2}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$ManageLabelCallback;->isItemChecked(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 321
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 293
    .end local v1    # "marginLeft":I
    .end local v2    # "marginRight":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 307
    .restart local v1    # "marginLeft":I
    .restart local v2    # "marginRight":I
    :cond_3
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_1
    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {p1, v3, v4}, Landroid/widget/CheckBox;->measure(II)V

    .line 309
    invoke-virtual {p1}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v3

    neg-int v2, v3

    goto :goto_1

    .line 323
    :cond_4
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private startCheckBoxAnimation()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 328
    const/4 v1, 0x0

    .line 329
    .local v1, "endPosition":I
    sget-object v3, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 330
    sget-object v3, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 331
    .local v0, "cb":Landroid/widget/CheckBox;
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/CheckBox;->measure(II)V

    .line 333
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v1

    .line 338
    const/4 v3, 0x1

    new-array v3, v3, [I

    aput v1, v3, v5

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 339
    .local v2, "varl":Landroid/animation/ValueAnimator;
    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 340
    new-instance v3, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$2;-><init>(Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 361
    new-instance v3, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$3;-><init>(Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 397
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 398
    .end local v0    # "cb":Landroid/widget/CheckBox;
    .end local v2    # "varl":Landroid/animation/ValueAnimator;
    :cond_0
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 21
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 166
    if-nez p3, :cond_1

    .line 167
    const-string v17, "LabelSimpleCursorAdapter"

    const-string v18, "bindview : cursor is null"

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v17

    if-eqz v17, :cond_2

    .line 171
    const-string v17, "LabelSimpleCursorAdapter"

    const-string v18, "bindview : cursor is closed"

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 175
    :cond_2
    const/4 v5, -0x1

    .line 176
    .local v5, "color":I
    const/4 v11, -0x1

    .line 178
    .local v11, "labelid":I
    const v17, 0x7f0e0020

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 181
    .local v4, "checkbox":Landroid/widget/CheckBox;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mTo:[I

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_7

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v17

    if-nez v17, :cond_7

    .line 182
    const v17, 0x7f0b0029

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 186
    .local v15, "title":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mFrom:[Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aget-object v17, v17, v18

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 188
    const v17, 0x7f0b0029

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 189
    if-eqz v4, :cond_3

    .line 190
    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Landroid/widget/CheckBox;->measure(II)V

    .line 192
    invoke-virtual {v4}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v17

    move/from16 v0, v17

    neg-int v13, v0

    .line 193
    .local v13, "marginRight":I
    invoke-virtual {v4}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout$LayoutParams;

    .line 194
    .local v12, "lp":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v12, v0, v1, v13, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 195
    invoke-virtual {v4, v12}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 196
    sget-object v17, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 202
    .end local v12    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v13    # "marginRight":I
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mFrom:[Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    aget-object v17, v17, v18

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v11

    .line 213
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mTo:[I

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aget v17, v17, v18

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 214
    .local v10, "label":Landroid/view/View;
    invoke-virtual {v10, v5}, Landroid/view/View;->setBackgroundColor(I)V

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mTo:[I

    move-object/from16 v17, v0

    const/16 v18, 0x1

    aget v17, v17, v18

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 217
    .local v16, "titleText":Landroid/widget/TextView;
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout$LayoutParams;

    .line 219
    .restart local v12    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mIsSelectionMode:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mTo:[I

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_9

    .line 220
    :cond_4
    const/16 v17, 0x0

    const/16 v18, 0x0

    iget v0, v12, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    move/from16 v19, v0

    const/16 v20, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 224
    :goto_3
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mTo:[I

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_5

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mTo:[I

    move-object/from16 v17, v0

    const/16 v18, 0x2

    aget v17, v17, v18

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 228
    .local v7, "fileCount":Landroid/widget/TextView;
    if-eqz v7, :cond_5

    .line 229
    sget-object v17, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mlabelCountHashMap:Ljava/util/HashMap;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_a

    .line 230
    const-string v17, "(0)"

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    .end local v7    # "fileCount":Landroid/widget/TextView;
    :cond_5
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mTo:[I

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_0

    .line 237
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mTo:[I

    move-object/from16 v17, v0

    const/16 v18, 0x3

    aget v17, v17, v18

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/RadioButton;

    .line 238
    .local v14, "radioButton":Landroid/widget/RadioButton;
    if-eqz v14, :cond_0

    .line 239
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mMode:Z

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_c

    .line 240
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mClearAll:Z

    move/from16 v17, v0

    if-eqz v17, :cond_6

    .line 241
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 242
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mCategorySelected:Z

    move/from16 v17, v0

    if-eqz v17, :cond_b

    .line 243
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mPosition:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_0

    .line 244
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    .line 184
    .end local v10    # "label":Landroid/view/View;
    .end local v12    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v14    # "radioButton":Landroid/widget/RadioButton;
    .end local v15    # "title":Ljava/lang/String;
    .end local v16    # "titleText":Landroid/widget/TextView;
    :cond_7
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mFrom:[Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    aget-object v17, v17, v18

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "title":Ljava/lang/String;
    goto/16 :goto_1

    .line 199
    :cond_8
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v17

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v4, v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->controlCheckbox(Landroid/widget/CheckBox;I)V
    :try_end_1
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/StaleDataException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_2

    .line 203
    .end local v15    # "title":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 204
    .local v6, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v6}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto/16 :goto_0

    .line 206
    .end local v6    # "e":Landroid/database/CursorIndexOutOfBoundsException;
    :catch_1
    move-exception v6

    .line 207
    .local v6, "e":Landroid/database/StaleDataException;
    invoke-virtual {v6}, Landroid/database/StaleDataException;->printStackTrace()V

    goto/16 :goto_0

    .line 209
    .end local v6    # "e":Landroid/database/StaleDataException;
    :catch_2
    move-exception v6

    .line 210
    .local v6, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v6}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_0

    .line 222
    .end local v6    # "e":Ljava/lang/IllegalStateException;
    .restart local v10    # "label":Landroid/view/View;
    .restart local v12    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .restart local v15    # "title":Ljava/lang/String;
    .restart local v16    # "titleText":Landroid/widget/TextView;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const/high16 v18, 0x41800000    # 16.0f

    const/16 v19, 0x0

    invoke-static/range {v17 .. v19}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->convertDPtoPX(Landroid/content/Context;FLandroid/util/DisplayMetrics;)I

    move-result v17

    const/16 v18, 0x0

    iget v0, v12, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    move/from16 v19, v0

    const/16 v20, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    goto/16 :goto_3

    .line 232
    .restart local v7    # "fileCount":Landroid/widget/TextView;
    :cond_a
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "("

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    sget-object v18, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mlabelCountHashMap:Ljava/util/HashMap;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ")"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 247
    .end local v7    # "fileCount":Landroid/widget/TextView;
    .restart local v14    # "radioButton":Landroid/widget/RadioButton;
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mPosition:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v0, v11, :cond_0

    .line 248
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    .line 251
    :cond_c
    const-string v17, "category_text"

    const-wide/16 v18, -0x1

    invoke-static/range {v17 .. v19}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getLongSettings(Ljava/lang/String;J)J

    move-result-wide v8

    .line 252
    .local v8, "id":J
    int-to-long v0, v11

    move-wide/from16 v18, v0

    cmp-long v17, v18, v8

    if-nez v17, :cond_d

    .line 253
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    .line 255
    :cond_d
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0
.end method

.method public getItemId(I)J
    .locals 5
    .param p1, "position"    # I

    .prologue
    const-wide/16 v2, 0x0

    .line 270
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 273
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/SimpleCursorAdapter;->getItemId(I)J
    :try_end_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 279
    :cond_0
    :goto_0
    return-wide v2

    .line 274
    :catch_0
    move-exception v0

    .line 275
    .local v0, "e":Landroid/database/StaleDataException;
    const-string v1, "LabelSimpleCursorAdapter"

    const-string v4, "getItemId : StableDataException"

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 141
    const/4 v1, 0x0

    .line 143
    .local v1, "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 144
    const-string v2, "LabelSimpleCursorAdapter"

    const-string v3, "getView : cursor is closed! It shouldn\'t happen here"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_2

    .line 149
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v3

    invoke-virtual {p0, v2, v3, p3}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 154
    :goto_0
    if-nez v1, :cond_1

    .line 155
    const-string v2, "LabelSimpleCursorAdapter"

    const-string v3, "if view is null, create new view temporary"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 158
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget v2, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mLayoutResourceId:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 161
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_1
    return-object v1

    .line 151
    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 263
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 265
    .local v0, "view":Landroid/view/View;
    return-object v0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileCountGroupByLabel(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mlabelCountHashMap:Ljava/util/HashMap;

    .line 112
    invoke-super {p0}, Landroid/widget/SimpleCursorAdapter;->notifyDataSetChanged()V

    .line 113
    return-void
.end method

.method public notifyDataSetInvalidated()V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileCountGroupByLabel(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mlabelCountHashMap:Ljava/util/HashMap;

    .line 118
    invoke-super {p0}, Landroid/widget/SimpleCursorAdapter;->notifyDataSetInvalidated()V

    .line 119
    return-void
.end method

.method public selectMode(Z)V
    .locals 0
    .param p1, "mode"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mMode:Z

    .line 86
    return-void
.end method

.method public selectRadionButton(I)V
    .locals 0
    .param p1, "postion"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mPosition:I

    .line 79
    return-void
.end method

.method public setCategorySelected(Z)V
    .locals 0
    .param p1, "isSelected"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mCategorySelected:Z

    .line 82
    return-void
.end method

.method public setClearAll(Z)V
    .locals 0
    .param p1, "clearall"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mClearAll:Z

    .line 75
    return-void
.end method

.method public setFilterbyCallback(Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;)V
    .locals 0
    .param p1, "filterbyCallback"    # Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;

    .prologue
    .line 409
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mFilterbyCallback:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;

    .line 410
    return-void
.end method

.method public setSelectionMode(ZZ)V
    .locals 0
    .param p1, "isSelectionMode"    # Z
    .param p2, "needAnim"    # Z

    .prologue
    .line 401
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mIsSelectionMode:Z

    .line 403
    if-eqz p2, :cond_0

    .line 404
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->startCheckBoxAnimation()V

    .line 406
    :cond_0
    return-void
.end method
