.class public Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNInfoDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 61
    return-void
.end method

.method public static newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;
    .locals 3
    .param p0, "title"    # I
    .param p1, "message"    # I

    .prologue
    .line 32
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;-><init>()V

    .line 33
    .local v1, "dialogFragment":Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 34
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 35
    const-string v2, "message"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 36
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 37
    return-object v1
.end method

.method public static newInstance(III)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;
    .locals 3
    .param p0, "title"    # I
    .param p1, "message"    # I
    .param p2, "count"    # I

    .prologue
    .line 50
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;-><init>()V

    .line 51
    .local v1, "dialogFragment":Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 52
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 53
    const-string v2, "info"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 54
    const-string v2, "count"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 55
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 56
    return-object v1
.end method

.method public static newInstance(ILjava/lang/String;)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;
    .locals 3
    .param p0, "title"    # I
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 41
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;-><init>()V

    .line 42
    .local v1, "dialogFragment":Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 43
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 44
    const-string v2, "text"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 46
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    const-string v6, "TAG"

    const-string v7, "onCreateDialog Dialog"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "title"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 78
    .local v5, "title":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "message"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 79
    .local v3, "message":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "info"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 80
    .local v2, "info":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "count"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 81
    .local v1, "count":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "text"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 82
    .local v4, "text":Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 84
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 85
    if-eqz v2, :cond_0

    .line 86
    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 92
    :goto_0
    const v6, 0x104000a

    new-instance v7, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 100
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    return-object v6

    .line 87
    :cond_0
    if-eqz v4, :cond_1

    .line 88
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 90
    :cond_1
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 65
    const-string v0, "TAG"

    const-string v1, "onSaveInstanceState Dialog"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v0, "title"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 67
    const-string v0, "message"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "message"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 68
    const-string v0, "info"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "info"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 69
    const-string v0, "count"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "count"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 70
    const-string v0, "text"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "text"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 72
    return-void
.end method
