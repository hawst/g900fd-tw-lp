.class public Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;
.super Landroid/app/Activity;
.source "VNOpenSourceLicenseInfoActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "VNOpenSourceLicenseInfoActivity"


# instance fields
.field private bDisplayNotice:Z

.field private mListView:Landroid/widget/ListView;

.field private mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mListView:Landroid/widget/ListView;

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->bDisplayNotice:Z

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    return-void
.end method

.method private listBinding()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 128
    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const v3, 0x7f0b00e3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 129
    .local v1, "items":[Ljava/lang/String;
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v2, 0x7f03003a

    invoke-direct {v0, p0, v2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 130
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 131
    return v4
.end method


# virtual methods
.method public onBackPressed()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getVisibility()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 150
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->bDisplayNotice:Z

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show(Ljava/lang/String;)V

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 159
    :goto_0
    return-void

    .line 157
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 45
    const-string v0, "VNOpenSourceLicenseInfoActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const v0, 0x7f030008

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->setContentView(I)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show(Ljava/lang/String;)V

    .line 54
    const v0, 0x7f0e001d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setLongClickable(Z)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity$1;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setClickable(Z)V

    .line 64
    const v0, 0x7f0e001c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mListView:Landroid/widget/ListView;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    const-string v1, "file:///android_asset/text/NOTICE"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->listBinding()Z

    .line 71
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 72
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->bDisplayNotice:Z

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00e3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show(Ljava/lang/String;)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mListView:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 145
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 91
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 105
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 93
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getVisibility()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 94
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->bDisplayNotice:Z

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mVNActionBar:Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show(Ljava/lang/String;)V

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    .line 101
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->finish()V

    goto :goto_0

    .line 91
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 76
    const-string v0, "VNOpenSourceLicenseInfoActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->bDisplayNotice:Z

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 86
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 87
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNOpenSourceLicenseInfoActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 110
    const-string v0, "VNOpenSourceLicenseInfoActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTrimMemory() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    sparse-switch p1, :sswitch_data_0

    .line 124
    :sswitch_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 125
    return-void

    .line 111
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method
