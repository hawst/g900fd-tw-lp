.class Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;
.super Ljava/lang/Object;
.source "VNPlayerToolbarFragment.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setProgressHoverWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final CENTER_M_SEEK_VIEW:I = 0x1

.field private static final HOVER_IMAGE_TAIL_GAP:I = 0x50

.field private static final LEFT_M_SEEK_VIEW:I = 0x2

.field private static final RIGHT_M_SEEK_VIEW:I = 0x3


# instance fields
.field dp17p5:I

.field dp18:I

.field private mDefaultBottomPadding:I

.field private mDurationTime:D

.field private mHoverState:I

.field private mHoverWin:Landroid/widget/HoverPopupWindow;

.field private mMarginPoint:I

.field private mSeekBarMax:I

.field private mTime:Landroid/widget/TextView;

.field px65:I

.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x43200000    # 160.0f

    const/4 v4, 0x0

    .line 1337
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1338
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    mul-int/lit8 v0, v0, 0x12

    int-to-float v0, v0

    div-float/2addr v0, v5

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->dp18:I

    .line 1340
    const-wide v0, 0x4031800000000000L    # 17.5

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-double v2, v2

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4064000000000000L    # 160.0

    div-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->dp17p5:I

    .line 1342
    const v0, 0x41accccd    # 21.6f

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    div-float/2addr v0, v5

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->px65:I

    .line 1348
    iput v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mSeekBarMax:I

    .line 1349
    iput v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mMarginPoint:I

    .line 1350
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mDurationTime:D

    .line 1351
    iput v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverState:I

    .line 1352
    iput v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mDefaultBottomPadding:I

    .line 1353
    iput-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mTime:Landroid/widget/TextView;

    .line 1354
    iput-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverWin:Landroid/widget/HoverPopupWindow;

    return-void
.end method

.method private updateProgressbarPreviewView(I)V
    .locals 9
    .param p1, "progress"    # I

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 1396
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mTime:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v0, v1, -0x50

    .line 1398
    .local v0, "offset_x":I
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mSeekBarMax:I

    div-int/lit8 v1, v1, 0xa

    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mMarginPoint:I

    .line 1400
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mMarginPoint:I

    if-ge p1, v1, :cond_3

    .line 1401
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverState:I

    if-ne v1, v7, :cond_1

    .line 1429
    :cond_0
    :goto_0
    return-void

    .line 1403
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mTime:Landroid/widget/TextView;

    const v2, 0x7f020058

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 1404
    iput v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverState:I

    .line 1421
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverWin:Landroid/widget/HoverPopupWindow;

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->px65:I

    invoke-virtual {v1, v0, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 1422
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverWin:Landroid/widget/HoverPopupWindow;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mTime:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 1423
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mDefaultBottomPadding:I

    if-nez v1, :cond_2

    .line 1424
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mTime:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->dp17p5:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mDefaultBottomPadding:I

    .line 1426
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mTime:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->dp18:I

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->dp17p5:I

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->dp18:I

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mDefaultBottomPadding:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1427
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mTime:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->invalidate()V

    .line 1428
    const-string v1, "VNPlyayerToolbarFragment"

    const-string v2, "AIR_VIEW progress : %d,   offset_x : %d,   mHoverState : %d"

    new-array v3, v8, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverState:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1405
    :cond_3
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mSeekBarMax:I

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mMarginPoint:I

    sub-int/2addr v1, v2

    if-le p1, v1, :cond_4

    .line 1406
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverState:I

    if-eq v1, v8, :cond_0

    .line 1408
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mTime:Landroid/widget/TextView;

    const v2, 0x7f020059

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 1409
    iput v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverState:I

    .line 1410
    mul-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 1412
    :cond_4
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverState:I

    if-ne v1, v6, :cond_5

    .line 1413
    const/4 v0, 0x0

    goto :goto_1

    .line 1415
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mTime:Landroid/widget/TextView;

    const v2, 0x7f020057

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 1416
    iput v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverState:I

    .line 1417
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onHoverChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "hoverLevel"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 1387
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverWin:Landroid/widget/HoverPopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mTime:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 1392
    :cond_0
    :goto_0
    return-void

    .line 1390
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mTime:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mDurationTime:D

    int-to-double v4, p2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->convertTimeFormat(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$2200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1391
    invoke-direct {p0, p2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->updateProgressbarPreviewView(I)V

    goto :goto_0
.end method

.method public onStartTrackingHover(Landroid/widget/SeekBar;I)V
    .locals 8
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "hoverLevel"    # I

    .prologue
    .line 1358
    const-string v1, "VNPlyayerToolbarFragment"

    const-string v2, "AIR_VIEW(com.sec.android.app.voicerecorder/VoiceListActivity) : ACTION_HOVER_ENTER"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secV(Ljava/lang/String;Ljava/lang/String;)V

    .line 1359
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1378
    :cond_0
    :goto_0
    return-void

    .line 1362
    :cond_1
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPlayerDuration()I

    move-result v1

    int-to-double v2, v1

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    int-to-double v4, v1

    div-double/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mDurationTime:D

    .line 1363
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mSeekBarMax:I

    .line 1364
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverState:I

    .line 1366
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1367
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030020

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mTime:Landroid/widget/TextView;

    .line 1369
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverWin:Landroid/widget/HoverPopupWindow;

    .line 1371
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mHoverWin:Landroid/widget/HoverPopupWindow;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mTime:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 1374
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mTime:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    iget-wide v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->mDurationTime:D

    int-to-double v6, p2

    mul-double/2addr v4, v6

    double-to-int v3, v4

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->convertTimeFormat(I)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$2200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1376
    invoke-direct {p0, p2}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$5;->updateProgressbarPreviewView(I)V

    .line 1377
    const-string v1, "VNPlyayerToolbarFragment"

    const-string v2, "AIR_VIEW(com.sec.android.app.voicerecorder/VoiceListActivity) : ACTION_HOVER_ENTER OUT"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secV(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStopTrackingHover(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1382
    return-void
.end method
