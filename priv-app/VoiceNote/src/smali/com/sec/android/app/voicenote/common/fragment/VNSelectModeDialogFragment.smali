.class public Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNSelectModeDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$Callbacks;
    }
.end annotation


# instance fields
.field private mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$Callbacks;

.field private mModeItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mStringItems:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$Callbacks;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mStringItems:[Ljava/lang/String;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mModeItems:Ljava/util/ArrayList;

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mModeItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->isAdvancedAndHeadsetMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$Callbacks;

    return-object v0
.end method

.method private isAdvancedAndHeadsetMode()Z
    .locals 3

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 201
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->is4PinHeadsetConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    const/4 v1, 0x1

    .line 204
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 59
    instance-of v0, p1, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$Callbacks;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 60
    check-cast v0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$Callbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$Callbacks;

    .line 62
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 63
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    const/4 v1, 0x0

    .line 77
    .local v1, "checkedItem":I
    const-string v6, "record_mode"

    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v4

    .line 78
    .local v4, "recordMode":I
    const/4 v3, 0x0

    .line 80
    .local v3, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v3, Ljava/util/HashMap;

    .end local v3    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 81
    .restart local v3    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "value"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    const-string v6, "text"

    const v7, 0x7f0b00dc

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mModeItems:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    sget-boolean v6, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_BEAMFORMING:Z

    if-eqz v6, :cond_0

    .line 85
    new-instance v3, Ljava/util/HashMap;

    .end local v3    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 86
    .restart local v3    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "value"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    const-string v6, "text"

    const v7, 0x7f0b0096

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mModeItems:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    new-instance v3, Ljava/util/HashMap;

    .end local v3    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 90
    .restart local v3    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "value"

    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    const-string v6, "text"

    const v7, 0x7f0b013f

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mModeItems:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITH_SVOICE(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITHOUT_SVOICE()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 96
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    .end local v3    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 97
    .restart local v3    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "value"

    const/4 v7, 0x3

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    const-string v6, "text"

    const v7, 0x7f0b0165

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mModeItems:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mModeItems:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    iput-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mStringItems:[Ljava/lang/String;

    .line 103
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mModeItems:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 104
    .local v5, "size":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v5, :cond_4

    .line 105
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mStringItems:[Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mModeItems:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/HashMap;

    const-string v8, "text"

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    aput-object v6, v7, v2

    .line 106
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mModeItems:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/HashMap;

    const-string v7, "value"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-ne v4, v6, :cond_3

    .line 107
    move v1, v2

    .line 104
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 111
    :cond_4
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 112
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    const v6, 0x7f0b0118

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 113
    const v6, 0x7f0b0021

    new-instance v7, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 120
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;->mStringItems:[Ljava/lang/String;

    new-instance v7, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;

    invoke-direct {v7, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNSelectModeDialogFragment;)V

    invoke-virtual {v0, v6, v1, v7}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 194
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    return-object v6
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
