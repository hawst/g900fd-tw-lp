.class public Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;
.super Landroid/app/Activity;
.source "VNBackPlayDialogActivity.java"


# static fields
.field private static final SAVE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "VNBackPlayDialogActivity"

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# instance fields
.field private getName:Ljava/lang/String;

.field private mDialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mDialog:Landroid/app/Dialog;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->getName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-object v0
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 50
    sput-object p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 51
    return-void
.end method

.method public static releaseService()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 55
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 59
    const-string v3, "VNBackPlayDialogActivity"

    const-string v4, "onCreate()"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    const-string v3, "change_notification_resume"

    invoke-static {v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 63
    const-string v3, "change_notification_service"

    invoke-static {v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 64
    sget-object v3, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v3, :cond_2

    .line 65
    sget-object v3, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecordingOnBackground()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->getName:Ljava/lang/String;

    .line 66
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->getName:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->finish()V

    .line 96
    :goto_0
    return-void

    .line 71
    :cond_0
    sget-object v3, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getStartVoiceLabelAfterRecord()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 72
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 73
    .local v0, "i":Landroid/content/Intent;
    const-string v3, "isFromQuick"

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 74
    const-string v3, "activitymode"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 75
    const/high16 v3, 0x4000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 76
    const/high16 v3, 0x20000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 77
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->startActivity(Landroid/content/Intent;)V

    .line 79
    .end local v0    # "i":Landroid/content/Intent;
    :cond_1
    sget-object v3, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->hideNotification()Z

    .line 82
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/Window;->requestFeature(I)Z

    .line 83
    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->showDialog(I)V

    .line 85
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 86
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 87
    const-string v3, "file"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 89
    const-string v3, "search"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/SearchManager;

    .line 90
    .local v2, "searchManager":Landroid/app/SearchManager;
    new-instance v3, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity$1;-><init>(Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;)V

    invoke-virtual {v2, v3}, Landroid/app/SearchManager;->setOnDismissListener(Landroid/app/SearchManager$OnDismissListener;)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .param p1, "id"    # I

    .prologue
    .line 108
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mDialog:Landroid/app/Dialog;

    .line 162
    :goto_0
    return-object v1

    .line 112
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 114
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    packed-switch p1, :pswitch_data_0

    .line 162
    const/4 v1, 0x0

    goto :goto_0

    .line 117
    :pswitch_0
    const v1, 0x7f0b012c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b0015

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->getName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity$2;-><init>(Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 129
    const v1, 0x7f0b012b

    new-instance v2, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity$3;-><init>(Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 144
    const v1, 0x7f0b012a

    new-instance v2, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity$4;-><init>(Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 151
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mDialog:Landroid/app/Dialog;

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mDialog:Landroid/app/Dialog;

    new-instance v2, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity$5;-><init>(Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 168
    const-string v0, "VNBackPlayDialogActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 174
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 175
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 100
    const-string v0, "VNBackPlayDialogActivity"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 104
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 179
    const-string v0, "VNBackPlayDialogActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTrimMemory() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    sparse-switch p1, :sswitch_data_0

    .line 193
    :sswitch_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 194
    return-void

    .line 180
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method
