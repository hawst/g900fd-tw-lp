.class Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;
.super Landroid/media/session/MediaSession$Callback;
.source "VoiceNoteService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaSessionCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0

    .prologue
    .line 2554
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-direct {p0}, Landroid/media/session/MediaSession$Callback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$1;

    .prologue
    .line 2554
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    return-void
.end method


# virtual methods
.method public onMediaButtonEvent(Landroid/content/Intent;)Z
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v9, 0x28

    const/4 v8, 0x4

    const/4 v3, 0x0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 2558
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaSession:Landroid/media/session/MediaSession;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$3200(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Landroid/media/session/MediaSession;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string v4, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2560
    const-string v4, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/view/KeyEvent;

    .line 2561
    .local v2, "ke":Landroid/view/KeyEvent;
    if-eqz v2, :cond_0

    .line 2562
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_1

    const/4 v1, 0x1

    .line 2563
    .local v1, "isKeyDown":Z
    :goto_0
    if-eqz v1, :cond_9

    .line 2564
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 2647
    .end local v1    # "isKeyDown":Z
    .end local v2    # "ke":Landroid/view/KeyEvent;
    :cond_0
    :goto_1
    invoke-super {p0, p1}, Landroid/media/session/MediaSession$Callback;->onMediaButtonEvent(Landroid/content/Intent;)Z

    move-result v3

    return v3

    .restart local v2    # "ke":Landroid/view/KeyEvent;
    :cond_1
    move v1, v3

    .line 2562
    goto :goto_0

    .line 2569
    .restart local v1    # "isKeyDown":Z
    :sswitch_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v3

    const/16 v4, 0x3eb

    if-ne v3, v4, :cond_2

    .line 2570
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pauseRecording()Z

    .line 2571
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_1

    .line 2572
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v3

    const/16 v4, 0x3ec

    if-ne v3, v4, :cond_3

    .line 2573
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumeRecording()Z

    .line 2574
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_1

    .line 2575
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2576
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 2577
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 2579
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_1

    .line 2580
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2582
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumePlay()V

    .line 2583
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 2585
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_1

    .line 2590
    :sswitch_1
    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2200()I

    move-result v3

    if-le v3, v9, :cond_6

    .line 2591
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I
    invoke-static {v3, v8}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2302(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)I

    .line 2595
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v0

    .line 2596
    .local v0, "cpos":I
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v3

    int-to-double v4, v3

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-int v3, v4

    mul-int/lit16 v3, v3, 0x3e8

    add-int/2addr v0, v3

    .line 2597
    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2200()I

    move-result v3

    rem-int/lit8 v3, v3, 0xa

    if-nez v3, :cond_5

    .line 2598
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 2599
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 2602
    :cond_5
    # operator++ for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2208()I

    goto/16 :goto_1

    .line 2593
    .end local v0    # "cpos":I
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2200()I

    move-result v4

    div-int/lit8 v4, v4, 0xa

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I
    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2302(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)I

    goto :goto_2

    .line 2606
    :sswitch_2
    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2200()I

    move-result v3

    if-le v3, v9, :cond_8

    .line 2607
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I
    invoke-static {v3, v8}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2302(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)I

    .line 2611
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v0

    .line 2612
    .restart local v0    # "cpos":I
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v3

    int-to-double v4, v3

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-int v3, v4

    mul-int/lit16 v3, v3, 0x3e8

    sub-int/2addr v0, v3

    .line 2613
    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2200()I

    move-result v3

    rem-int/lit8 v3, v3, 0xa

    if-nez v3, :cond_7

    .line 2614
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 2615
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 2618
    :cond_7
    # operator++ for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2208()I

    goto/16 :goto_1

    .line 2609
    .end local v0    # "cpos":I
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2200()I

    move-result v4

    div-int/lit8 v4, v4, 0xa

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I
    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2302(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)I

    goto :goto_3

    .line 2622
    :sswitch_3
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.android.voicenote.VOICE_INTENT_PREV"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 2626
    :sswitch_4
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.android.voicenote.VOICE_INTENT_NEXT"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 2633
    :cond_9
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_1

    .line 2635
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    goto/16 :goto_1

    .line 2639
    :pswitch_2
    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$2202(I)I

    goto/16 :goto_1

    .line 2564
    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_0
        0x55 -> :sswitch_0
        0x57 -> :sswitch_4
        0x58 -> :sswitch_3
        0x59 -> :sswitch_2
        0x5a -> :sswitch_1
        0x7e -> :sswitch_0
        0x7f -> :sswitch_0
    .end sparse-switch

    .line 2633
    :pswitch_data_0
    .packed-switch 0x56
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
