.class public Lcom/sec/android/app/voicenote/library/stt/AudioConverter;
.super Ljava/lang/Object;
.source "AudioConverter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/stt/AudioConverter$OnConvertingFinishedListener;
    }
.end annotation


# static fields
.field static final DEFAULT_VAD_BEGIN_THRESHOLD:I = 0x3

.field static final DEFAULT_VAD_END_LEN:I = 0x1e

.field static final DEFAULT_VAD_END_THRESHOLD:I = 0x1

.field private static final FILE_INPUT_STREAM_READ_INTERVAL:I = 0x64

.field public static final TAG:Ljava/lang/String; = "AudioConverter"

.field private static mM4aFilePath:Ljava/lang/String;

.field private static mTextFilePath:Ljava/lang/String;


# instance fields
.field private mAudioRecognizer:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

.field private mAudioRecorder:Landroid/media/AudioRecord;

.field private mContext:Landroid/content/Context;

.field private mCountOfAudioRecognizerResponse:I

.field private mCountOfEndOfSpeech:I

.field private mEPDSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

.field private mEndPointerPipe:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

.field private mEnded:Z

.field private mFileInputStream:Ljava/io/FileInputStream;

.field private mFileOutputStream:Ljava/io/FileOutputStream;

.field private mLastCountOfEndOfSpeech:I

.field private mMemoData:Lcom/sec/android/app/voicenote/library/stt/MemoData;

.field public mOnConvertingFinishedListener:Lcom/sec/android/app/voicenote/library/stt/AudioConverter$OnConvertingFinishedListener;

.field private mOrderedSentenceMap:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mReadBufferSize:I

.field private mRecentTimeStamp:J

.field private mRecognizerSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

.field private mSpeechDetectListener:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

.field private mSpeexEncoderPipe:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

.field private mSrcFilePath:Ljava/lang/String;

.field private mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

.field private mWriteHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mTextFilePath:Ljava/lang/String;

    .line 40
    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mM4aFilePath:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSrcFilePath:Ljava/lang/String;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mMemoData:Lcom/sec/android/app/voicenote/library/stt/MemoData;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mAudioRecognizer:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mAudioRecorder:Landroid/media/AudioRecord;

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mRecognizerSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    .line 61
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mContext:Landroid/content/Context;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mAudioRecognizer:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mCountOfEndOfSpeech:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/sec/android/app/voicenote/library/stt/AudioSource;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mEPDSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    return-object v0
.end method

.method static synthetic access$108(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mCountOfEndOfSpeech:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mCountOfEndOfSpeech:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mWriteHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mEnded:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    .prologue
    .line 26
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mRecentTimeStamp:J

    return-wide v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;
    .param p1, "x1"    # J

    .prologue
    .line 26
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mRecentTimeStamp:J

    return-wide p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/sec/android/app/voicenote/library/stt/AudioSource;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mRecognizerSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSpeechDetectListener:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;
    .param p1, "x1"    # Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;
    .param p2, "x2"    # J

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->initializeRecognizer(Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;J)V

    return-void
.end method

.method static synthetic access$602(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mLastCountOfEndOfSpeech:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mReadBufferSize:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Ljava/io/FileInputStream;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mFileInputStream:Ljava/io/FileInputStream;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/samsung/voiceshell/VoiceEngine;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    return-object v0
.end method

.method private checkValidationMemoData()Z
    .locals 5

    .prologue
    .line 384
    const/4 v2, 0x0

    .line 386
    .local v2, "isValid":Z
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mOrderedSentenceMap:Ljava/util/TreeMap;

    invoke-virtual {v4}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 387
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 388
    .local v3, "sentence":Ljava/lang/String;
    const-string v4, "...."

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 389
    const/4 v2, 0x1

    .line 394
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/String;>;"
    .end local v3    # "sentence":Ljava/lang/String;
    :cond_1
    return v2
.end method

.method private initializeRecognizer(Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;J)V
    .locals 10
    .param p1, "listener"    # Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;
    .param p2, "timeStamp"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/Utils;->readNetworkState(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->NETWORK_NOT_AVAILABLE:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->notifyConvertingFinished(Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;)V

    .line 222
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSpeexEncoderPipe:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    if-eqz v0, :cond_1

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSpeexEncoderPipe:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;->disconnectAudioSource()Lcom/nuance/dragon/toolkit/audio/AudioSource;

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSpeexEncoderPipe:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;->release()V

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mEndPointerPipe:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    if-eqz v0, :cond_2

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mEndPointerPipe:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;->disconnectAudioSource()Lcom/nuance/dragon/toolkit/audio/AudioSource;

    .line 206
    :cond_2
    new-instance v0, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    const/16 v3, 0x32

    const/16 v4, 0xf

    const/4 v5, 0x3

    const/16 v7, 0x1e

    const/16 v9, 0x23

    move v6, v2

    move v8, v1

    invoke-direct/range {v0 .. v9}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;-><init>(IIIIIIIII)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSpeexEncoderPipe:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSpeexEncoderPipe:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mEPDSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    invoke-virtual {v0, v1}, Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 218
    new-instance v0, Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    invoke-direct {v0, p1}, Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;-><init>(Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mEndPointerPipe:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mEndPointerPipe:Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSpeexEncoderPipe:Lcom/nuance/dragon/toolkit/audio/pipes/SpeexEncoderPipe;

    invoke-virtual {v0, v1}, Lcom/nuance/dragon/toolkit/audio/pipes/EndPointerPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 221
    new-instance v0, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mRecognizerSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mOrderedSentenceMap:Ljava/util/TreeMap;

    move-wide v2, p2

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;-><init>(Landroid/content/Context;JLcom/sec/android/app/voicenote/library/stt/AudioSource;Ljava/util/TreeMap;Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mAudioRecognizer:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    goto :goto_0
.end method

.method private runFileInputStream()V
    .locals 6

    .prologue
    .line 229
    :try_start_0
    const-string v3, "AudioConverter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "path = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSrcFilePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    new-instance v3, Ljava/io/FileInputStream;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSrcFilePath:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mFileInputStream:Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    invoke-static {}, Lcom/samsung/voiceshell/VoiceEngineWrapper;->getInstance()Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    .line 237
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v3}, Lcom/samsung/voiceshell/VoiceEngine;->initializeNS()I

    move-result v2

    .line 238
    .local v2, "ret":I
    if-eqz v2, :cond_0

    .line 239
    const-string v3, "AudioConverter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "voiceEngine.initializeNS returned "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    :cond_0
    new-instance v3, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$2;-><init>(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)V

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mWriteHandler:Landroid/os/Handler;

    .line 249
    new-instance v1, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;-><init>(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)V

    .line 303
    .local v1, "readRunnable":Ljava/lang/Runnable;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mWriteHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x64

    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 304
    .end local v1    # "readRunnable":Ljava/lang/Runnable;
    .end local v2    # "ret":I
    :goto_0
    return-void

    .line 231
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private saveTextFile()V
    .locals 7

    .prologue
    .line 324
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mOrderedSentenceMap:Ljava/util/TreeMap;

    if-eqz v4, :cond_1

    .line 325
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mOrderedSentenceMap:Ljava/util/TreeMap;

    invoke-virtual {v4}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 326
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 328
    .local v3, "sentence":Ljava/lang/String;
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mFileOutputStream:Ljava/io/FileOutputStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 329
    :catch_0
    move-exception v0

    .line 330
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "IOException occured on text file output"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/stt/Utils;->loge(Ljava/lang/String;)V

    .line 331
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 334
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/String;>;"
    .end local v3    # "sentence":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mOrderedSentenceMap:Ljava/util/TreeMap;

    invoke-virtual {v4}, Ljava/util/TreeMap;->clear()V

    .line 335
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mOrderedSentenceMap:Ljava/util/TreeMap;

    .line 338
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mFileOutputStream:Ljava/io/FileOutputStream;

    if-eqz v4, :cond_2

    .line 340
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mFileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 345
    :cond_2
    :goto_1
    return-void

    .line 341
    :catch_1
    move-exception v0

    .line 342
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private setRecognitionFailMemoData()V
    .locals 8

    .prologue
    .line 398
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mContext:Landroid/content/Context;

    const v5, 0x7f0b00f9

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 399
    .local v0, "recognitionFail":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 400
    .local v2, "time":J
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mOrderedSentenceMap:Ljava/util/TreeMap;

    invoke-virtual {v4}, Ljava/util/TreeMap;->clear()V

    .line 401
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mOrderedSentenceMap:Ljava/util/TreeMap;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mMemoData:Lcom/sec/android/app/voicenote/library/stt/MemoData;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/stt/MemoData;->initMemoData()V

    .line 405
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/TextData;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/util/TextData;-><init>()V

    .line 406
    .local v1, "textData":Lcom/sec/android/app/voicenote/common/util/TextData;
    iput-wide v2, v1, Lcom/sec/android/app/voicenote/common/util/TextData;->timeStamp:J

    .line 407
    const-wide/16 v4, 0x0

    iput-wide v4, v1, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    .line 408
    iget-object v4, v1, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    .line 409
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/stt/MemoData;->addSingleTextData(Lcom/sec/android/app/voicenote/common/util/TextData;)V

    .line 410
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/MemoData;->sortCollections()V

    .line 411
    return-void
.end method

.method private start()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 95
    const-wide/16 v2, 0x0

    .line 96
    .local v2, "chunkTimeStamp":J
    iput-boolean v5, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mEnded:Z

    .line 98
    const/16 v0, 0x1e00

    .line 99
    .local v0, "adjustedBufferSizeInBytes":I
    const/16 v1, 0x780

    iput v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mReadBufferSize:I

    .line 101
    new-instance v1, Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    sget-object v4, Lcom/nuance/dragon/toolkit/audio/AudioType;->PCM_16k:Lcom/nuance/dragon/toolkit/audio/AudioType;

    invoke-direct {v1, v4, v2, v3, v5}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;-><init>(Lcom/nuance/dragon/toolkit/audio/AudioType;JZ)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mEPDSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mEPDSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->startRecording()V

    .line 104
    new-instance v1, Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    sget-object v4, Lcom/nuance/dragon/toolkit/audio/AudioType;->PCM_16k:Lcom/nuance/dragon/toolkit/audio/AudioType;

    invoke-direct {v1, v4, v2, v3, v5}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;-><init>(Lcom/nuance/dragon/toolkit/audio/AudioType;JZ)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mRecognizerSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mRecognizerSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->startRecording()V

    .line 110
    new-instance v1, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;-><init>(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSpeechDetectListener:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    .line 170
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSpeechDetectListener:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->initializeRecognizer(Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;J)V

    .line 172
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->runFileInputStream()V

    .line 173
    return-void
.end method


# virtual methods
.method public checkFinishCondition()V
    .locals 4

    .prologue
    .line 307
    iget v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mLastCountOfEndOfSpeech:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 308
    const-string v1, "AudioConverter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkFinishCondition() mLastCountOfEndOfSpeech:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mLastCountOfEndOfSpeech:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mCountOfAudioRecognizerResponse:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mCountOfAudioRecognizerResponse:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iget v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mLastCountOfEndOfSpeech:I

    iget v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mCountOfAudioRecognizerResponse:I

    if-ne v1, v2, :cond_1

    .line 310
    const-string v1, "AudioConverter"

    const-string v2, "The LAST ONE RECEIVED !!!! "

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->checkValidationMemoData()Z

    move-result v0

    .line 313
    .local v0, "isValid":Z
    if-nez v0, :cond_0

    .line 314
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->setRecognitionFailMemoData()V

    .line 317
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->saveTextFile()V

    .line 318
    sget-object v1, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->NORMAL:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->notifyConvertingFinished(Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;)V

    .line 321
    .end local v0    # "isValid":Z
    :cond_1
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mEnded:Z

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mAudioRecorder:Landroid/media/AudioRecord;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mAudioRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mAudioRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 180
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mAudioRecorder:Landroid/media/AudioRecord;

    .line 183
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mMemoData:Lcom/sec/android/app/voicenote/library/stt/MemoData;

    .line 184
    sput-object v1, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mM4aFilePath:Ljava/lang/String;

    .line 185
    sput-object v1, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mTextFilePath:Ljava/lang/String;

    .line 186
    return-void
.end method

.method public go(Ljava/lang/String;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 69
    const-string v2, "AudioConverter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "go path:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSrcFilePath:Ljava/lang/String;

    .line 71
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSrcFilePath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/stt/Utils;->getOutputTextFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mTextFilePath:Ljava/lang/String;

    .line 72
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSrcFilePath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/stt/Utils;->getOriginalM4APath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mM4aFilePath:Ljava/lang/String;

    .line 75
    :try_start_0
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mTextFilePath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 76
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 77
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 79
    :cond_0
    new-instance v2, Ljava/io/FileOutputStream;

    sget-object v3, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mTextFilePath:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mFileOutputStream:Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mOrderedSentenceMap:Ljava/util/TreeMap;

    .line 86
    new-instance v2, Lcom/sec/android/app/voicenote/library/stt/MemoData;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/voicenote/library/stt/MemoData;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mMemoData:Lcom/sec/android/app/voicenote/library/stt/MemoData;

    .line 87
    iput v5, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mCountOfEndOfSpeech:I

    .line 88
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mLastCountOfEndOfSpeech:I

    .line 89
    iput v5, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mCountOfAudioRecognizerResponse:I

    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->start()V

    .line 92
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "AudioConverter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file output stream has exception :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/stt/Utils;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    sget-object v2, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->TEXT_CONVERT_ERROR:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->notifyConvertingFinished(Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;)V

    goto :goto_0
.end method

.method public increaseAudioRecognizerResponseCount()V
    .locals 3

    .prologue
    .line 353
    iget v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mCountOfAudioRecognizerResponse:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mCountOfAudioRecognizerResponse:I

    .line 354
    const-string v0, "AudioConverter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mCountOfAudioRecognizerResponse:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mCountOfAudioRecognizerResponse:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    return-void
.end method

.method public increaseEndOfSpeechCount()V
    .locals 3

    .prologue
    .line 348
    iget v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mLastCountOfEndOfSpeech:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mLastCountOfEndOfSpeech:I

    .line 349
    const-string v0, "AudioConverter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mLastCountOfEndOfSpeech:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mLastCountOfEndOfSpeech:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    return-void
.end method

.method public notifyConvertingFinished(Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;)V
    .locals 2
    .param p1, "type"    # Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mMemoData:Lcom/sec/android/app/voicenote/library/stt/MemoData;

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mMemoData:Lcom/sec/android/app/voicenote/library/stt/MemoData;

    sget-object v1, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mM4aFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/stt/MemoData;->saveMemoData(Ljava/lang/String;)V

    .line 372
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->NORMAL:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 373
    sget-object v0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mTextFilePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/Utils;->isFileExist(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 374
    sget-object p1, Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;->TEXT_CONVERT_ERROR:Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;

    .line 378
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mOnConvertingFinishedListener:Lcom/sec/android/app/voicenote/library/stt/AudioConverter$OnConvertingFinishedListener;

    if-eqz v0, :cond_2

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mOnConvertingFinishedListener:Lcom/sec/android/app/voicenote/library/stt/AudioConverter$OnConvertingFinishedListener;

    sget-object v1, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mTextFilePath:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$OnConvertingFinishedListener;->onFinished(Ljava/lang/String;Lcom/sec/android/app/voicenote/library/stt/Const$FinishType;)V

    .line 381
    :cond_2
    return-void
.end method

.method public setOnConvertingFinishedListener(Lcom/sec/android/app/voicenote/library/stt/AudioConverter$OnConvertingFinishedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/voicenote/library/stt/AudioConverter$OnConvertingFinishedListener;

    .prologue
    .line 364
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mOnConvertingFinishedListener:Lcom/sec/android/app/voicenote/library/stt/AudioConverter$OnConvertingFinishedListener;

    .line 365
    return-void
.end method
