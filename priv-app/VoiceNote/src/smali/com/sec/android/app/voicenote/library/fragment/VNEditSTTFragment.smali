.class public Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
.super Landroid/app/Fragment;
.source "VNEditSTTFragment.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;,
        Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;,
        Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;
    }
.end annotation


# static fields
.field protected static final MSG_RELEASE_SCREEN_ON:I = 0xf4241

.field private static TAG:Ljava/lang/String;

.field private static mBookmarks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

.field private static mTrimCallback:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;


# instance fields
.field private mCallback:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field public mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

.field public mEventHandler:Landroid/os/Handler;

.field private mFilePath:Ljava/lang/String;

.field private mIsKeepScreenOn:Z

.field private mIsLongPressed:Z

.field private mIsMoving:Z

.field private mIsTouching:Z

.field private mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

.field private mProgress:I

.field private mScrollDepender:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;

.field private mScrollHandler:Landroid/os/Handler;

.field private mScrollView:Landroid/widget/ScrollView;

.field private mSearchTextColor:I

.field private mSelectIndex:I

.field private mTextData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;"
        }
    .end annotation
.end field

.field private mTextData_orig:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;"
        }
    .end annotation
.end field

.field private mTextView:Landroid/widget/TextView;

.field private mTouchingHandler:Landroid/view/View;

.field private mTrimEndHandler:Landroid/view/View;

.field private mTrimLeft:I

.field private mTrimRight:I

.field private mTrimStartHandler:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    const-string v0, "VNEditSTTFragment"

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;

    .line 74
    sput-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 88
    sput-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimCallback:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData_orig:Ljava/util/ArrayList;

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mFilePath:Ljava/lang/String;

    .line 79
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mProgress:I

    .line 80
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimLeft:I

    .line 81
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimRight:I

    .line 82
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsLongPressed:Z

    .line 83
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsTouching:Z

    .line 84
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsMoving:Z

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;

    .line 87
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 89
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollView:Landroid/widget/ScrollView;

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollHandler:Landroid/os/Handler;

    .line 91
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSearchTextColor:I

    .line 93
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsKeepScreenOn:Z

    .line 762
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$6;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mEventHandler:Landroid/os/Handler;

    .line 895
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$1;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollDepender:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsLongPressed:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsLongPressed:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Ljava/util/ArrayList;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getSelectStringIndex(Ljava/util/ArrayList;I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
    .param p1, "x1"    # Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    return-object p1
.end method

.method static synthetic access$1300()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimStartHandler:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimEndHandler:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimLeft:I

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimRight:I

    return v0
.end method

.method static synthetic access$1800()Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimCallback:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsKeepScreenOn:Z

    return p1
.end method

.method static synthetic access$2000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsTouching:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollDepender:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsMoving:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Landroid/widget/TextView;Landroid/view/MotionEvent;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
    .param p1, "x1"    # Landroid/widget/TextView;
    .param p2, "x2"    # Landroid/view/MotionEvent;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getOffset(Landroid/widget/TextView;Landroid/view/MotionEvent;)I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
    .param p1, "x1"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I

    return p1
.end method

.method private getOffset(Landroid/widget/TextView;Landroid/view/MotionEvent;)I
    .locals 6
    .param p1, "text"    # Landroid/widget/TextView;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 387
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v3, v5

    .line 388
    .local v3, "positionX":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v4, v5

    .line 390
    .local v4, "positionY":I
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v5

    sub-int/2addr v3, v5

    .line 391
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v5

    sub-int/2addr v4, v5

    .line 393
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 394
    .local v0, "layout":Landroid/text/Layout;
    invoke-virtual {v0, v4}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v1

    .line 395
    .local v1, "line":I
    int-to-float v5, v3

    invoke-virtual {v0, v1, v5}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v2

    .line 397
    .local v2, "off":I
    return v2
.end method

.method private getSelectStringIndex(Ljava/util/ArrayList;I)I
    .locals 6
    .param p2, "offsetToFind"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .local p1, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    const/4 v5, 0x0

    .line 401
    if-nez p1, :cond_1

    .line 402
    const/4 v2, -0x1

    .line 417
    :cond_0
    :goto_0
    return v2

    .line 404
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 405
    .local v0, "count":I
    const/4 v1, 0x0

    .line 406
    .local v1, "currentOffset":I
    const/4 v3, -0x1

    .line 408
    .local v3, "lastSttText":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_3

    .line 409
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget v4, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    if-nez v4, :cond_2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v4, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    aget-object v4, v4, v5

    if-eqz v4, :cond_2

    .line 410
    move v3, v2

    .line 411
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v4, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v1, v4

    .line 412
    if-gt v1, p2, :cond_0

    .line 408
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move v2, v3

    .line 417
    goto :goto_0
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 722
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 723
    return-void
.end method

.method public static releaseService()V
    .locals 1

    .prologue
    .line 726
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 727
    return-void
.end method

.method public static seTrimCallback(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;)V
    .locals 0
    .param p0, "listener"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;

    .prologue
    .line 310
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimCallback:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;

    .line 311
    return-void
.end method

.method public static setBookmark(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 758
    .local p0, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;

    const-string v1, "setBookmark"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mBookmarks:Ljava/util/ArrayList;

    .line 760
    return-void
.end method


# virtual methods
.method public getDialogCallback()Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;
    .locals 1

    .prologue
    .line 466
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)V

    .line 540
    .local v0, "temDialogCallback":Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;
    return-object v0
.end method

.method public initList()V
    .locals 8

    .prologue
    .line 422
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v4, :cond_1

    .line 423
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;

    const-string v5, "initList:mService is null !!!"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    :cond_0
    :goto_0
    return-void

    .line 426
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mFilePath:Ljava/lang/String;

    .line 428
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mFilePath:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mFilePath:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isM4A(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 429
    sget-object v5, Lcom/sec/android/app/voicenote/common/util/M4aConsts;->FILE_LOCK:Ljava/lang/Object;

    monitor-enter v5

    .line 430
    :try_start_0
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mFilePath:Ljava/lang/String;

    invoke-direct {v1, v4}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 431
    .local v1, "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v3

    .line 432
    .local v3, "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;Landroid/content/Context;)V

    .line 433
    .local v0, "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->readSttd()Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    .line 434
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 435
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    if-nez v4, :cond_2

    .line 436
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData_orig:Ljava/util/ArrayList;

    goto :goto_0

    .line 434
    .end local v0    # "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    .end local v1    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 438
    .restart local v0    # "SttdHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    .restart local v1    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .restart local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData_orig:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 439
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 440
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData_orig:Ljava/util/ArrayList;

    new-instance v6, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/TextData;

    invoke-direct {v6, v4}, Lcom/sec/android/app/voicenote/common/util/TextData;-><init>(Lcom/sec/android/app/voicenote/common/util/TextData;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 439
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public initTextView()V
    .locals 2

    .prologue
    .line 447
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 448
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;

    const-string v1, "initTextView : mTextData is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    :goto_0
    return-void

    .line 460
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->updateAllWordOnScreen()V

    .line 462
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getDialogCallback()Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 101
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;

    const-string v2, "onCreateView"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const v1, 0x7f03001d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 103
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0e0049

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollView:Landroid/widget/ScrollView;

    .line 104
    const v1, 0x7f0e004a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    .line 105
    const v1, 0x7f0e004b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimStartHandler:Landroid/view/View;

    .line 106
    const v1, 0x7f0e004c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimEndHandler:Landroid/view/View;

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextIsSelectable(Z)V

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setRawInputType(I)V

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setWritingBuddyEnabled(Z)V

    .line 112
    if-eqz p3, :cond_0

    .line 113
    const-string v1, "selectindex"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I

    .line 115
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 132
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollHandler:Landroid/os/Handler;

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollView:Landroid/widget/ScrollView;

    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$3;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$4;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 300
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080029

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSearchTextColor:I

    .line 301
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setProgressCallback(Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;)V

    .line 302
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->setProgressCallback(Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;)V

    .line 303
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->initList()V

    .line 304
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->initTextView()V

    .line 305
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 306
    return-object v0
.end method

.method public onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 333
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;

    const-string v3, "onDestroyView"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 335
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData_orig:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 336
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->saveData()V

    .line 338
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 339
    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    .line 342
    :cond_1
    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 344
    .local v0, "activity":Landroid/app/Activity;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v2

    if-nez v2, :cond_2

    .line 346
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->dismissAllowingStateLoss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 352
    :cond_2
    :goto_0
    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    .line 353
    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mFilePath:Ljava/lang/String;

    .line 354
    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollView:Landroid/widget/ScrollView;

    .line 355
    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mEventHandler:Landroid/os/Handler;

    .line 358
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 359
    return-void

    .line 347
    :catch_0
    move-exception v1

    .line 348
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 315
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 317
    return-void
.end method

.method public onProgressChanged(I)V
    .locals 4
    .param p1, "progress"    # I

    .prologue
    const/4 v2, 0x1

    .line 363
    iput p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mProgress:I

    .line 364
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->updateAllWordOnScreen()V

    .line 366
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsKeepScreenOn:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNBookmarkListFragment;->isActivated()Z

    move-result v1

    if-nez v1, :cond_0

    .line 367
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setKeepScreenOn(Z)V

    .line 368
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsKeepScreenOn:Z

    .line 370
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 371
    .local v0, "msg":Landroid/os/Message;
    const v1, 0xf4241

    iput v1, v0, Landroid/os/Message;->what:I

    .line 372
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mEventHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 373
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mEventHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 376
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 321
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 323
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 327
    const-string v0, "selectindex"

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 328
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 329
    return-void
.end method

.method public onTrimChanged(II)V
    .locals 0
    .param p1, "leftTrim"    # I
    .param p2, "rightTrim"    # I

    .prologue
    .line 381
    iput p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimLeft:I

    .line 382
    iput p2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimRight:I

    .line 383
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->updateAllWordOnScreen()V

    .line 384
    return-void
.end method

.method public saveData()V
    .locals 7

    .prologue
    .line 730
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;

    const-string v5, "saveData"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    sget-object v5, Lcom/sec/android/app/voicenote/common/util/M4aConsts;->FILE_LOCK:Ljava/lang/Object;

    monitor-enter v5

    .line 732
    :try_start_0
    new-instance v2, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mFilePath:Ljava/lang/String;

    invoke-direct {v2, v4}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 733
    .local v2, "reader":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v1

    .line 734
    .local v1, "info":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    .line 735
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 736
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getCacheLoader()Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    move-result-object v0

    .line 737
    .local v0, "cacheLoader":Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;
    if-eqz v0, :cond_0

    .line 738
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mFilePath:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-virtual {v0, v4, v6}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->resetSttForFile(Ljava/lang/String;Z)V

    .line 741
    .end local v0    # "cacheLoader":Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;
    :cond_0
    if-eqz v1, :cond_1

    .line 742
    new-instance v3, Lcom/sec/android/app/voicenote/common/util/SttdHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;Landroid/content/Context;)V

    .line 743
    .local v3, "sttHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->overwriteSttd(Ljava/io/Serializable;)V

    .line 746
    .end local v3    # "sttHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    :cond_1
    monitor-exit v5

    .line 747
    return-void

    .line 746
    .end local v1    # "info":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .end local v2    # "reader":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public updateAllWordOnScreen()V
    .locals 46

    .prologue
    .line 544
    sget-object v40, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v40, :cond_1

    .line 545
    sget-object v40, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;

    const-string v41, "updateAllWordOnScreen : mService is null"

    invoke-static/range {v40 .. v41}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    :cond_0
    :goto_0
    return-void

    .line 548
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    if-eqz v40, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    move-object/from16 v40, v0

    if-nez v40, :cond_3

    .line 549
    :cond_2
    sget-object v40, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;

    const-string v41, "updateAllWordOnScreen : mTextData or mTextView is null"

    invoke-static/range {v40 .. v41}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 553
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsTouching:Z

    move/from16 v40, v0

    if-nez v40, :cond_0

    .line 557
    new-instance v34, Landroid/text/SpannableStringBuilder;

    invoke-direct/range {v34 .. v34}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 558
    .local v34, "ssb":Landroid/text/SpannableStringBuilder;
    const/16 v21, 0x0

    .line 559
    .local v21, "lengthLeft":I
    const/16 v22, 0x0

    .line 560
    .local v22, "lengthRight":I
    const/16 v19, 0x0

    .line 561
    .local v19, "length":I
    const/16 v23, 0x0

    .line 562
    .local v23, "lengthTotal":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 563
    .local v10, "count":I
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 565
    .local v17, "impossibleRecognition":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_1
    move/from16 v0, v16

    if-ge v0, v10, :cond_9

    .line 566
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    if-eqz v40, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    move/from16 v40, v0

    if-nez v40, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    if-eqz v40, :cond_7

    .line 567
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 569
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimLeft:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-long v0, v0

    move-wide/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    move-wide/from16 v44, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->duration:J

    move-wide/from16 v40, v0

    add-long v40, v40, v44

    cmp-long v40, v42, v40

    if-lez v40, :cond_8

    .line 570
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->length()I

    move-result v40

    add-int v21, v21, v40

    .line 576
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimRight:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-long v0, v0

    move-wide/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    move-wide/from16 v40, v0

    cmp-long v40, v42, v40

    if-lez v40, :cond_5

    .line 577
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->length()I

    move-result v40

    add-int v22, v22, v40

    .line 580
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mProgress:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-long v0, v0

    move-wide/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    move-wide/from16 v40, v0

    cmp-long v40, v42, v40

    if-gez v40, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    const-string v41, ".... "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_6

    .line 581
    invoke-virtual/range {v34 .. v34}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v42, 0x0

    aget-object v40, v40, v42

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->length()I

    move-result v40

    sub-int v40, v41, v40

    invoke-static/range {v40 .. v40}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v40

    move-object/from16 v0, v17

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 583
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->length()I

    move-result v40

    add-int v23, v23, v40

    .line 565
    :cond_7
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .line 572
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mProgress:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-long v0, v0

    move-wide/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    move-wide/from16 v40, v0

    cmp-long v40, v42, v40

    if-lez v40, :cond_4

    .line 573
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->length()I

    move-result v40

    add-int v19, v19, v40

    goto/16 :goto_2

    .line 586
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-virtual/range {v34 .. v34}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/String;->toCharArray()[C

    move-result-object v41

    const/16 v42, 0x0

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    move/from16 v2, v42

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->setText([CII)V

    .line 587
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/TextView;->getLineCount()I

    move-result v11

    .line 589
    .local v11, "currentLine":I
    const/16 v16, 0x0

    :goto_3
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v40

    move/from16 v0, v16

    move/from16 v1, v40

    if-ge v0, v1, :cond_b

    .line 590
    const/4 v15, 0x4

    .line 591
    .local v15, "endLength":I
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Ljava/lang/Integer;

    invoke-virtual/range {v40 .. v40}, Ljava/lang/Integer;->intValue()I

    move-result v40

    add-int v40, v40, v15

    invoke-virtual/range {v34 .. v34}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v41

    move/from16 v0, v40

    move/from16 v1, v41

    if-ge v0, v1, :cond_a

    .line 592
    new-instance v41, Landroid/text/style/ForegroundColorSpan;

    const/16 v40, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    move/from16 v0, v40

    move/from16 v1, v42

    move/from16 v2, v43

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v40

    move-object/from16 v0, v41

    move/from16 v1, v40

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Ljava/lang/Integer;

    invoke-virtual/range {v40 .. v40}, Ljava/lang/Integer;->intValue()I

    move-result v42

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Ljava/lang/Integer;

    invoke-virtual/range {v40 .. v40}, Ljava/lang/Integer;->intValue()I

    move-result v40

    add-int v40, v40, v15

    const/16 v43, 0x21

    move-object/from16 v0, v34

    move-object/from16 v1, v41

    move/from16 v2, v42

    move/from16 v3, v40

    move/from16 v4, v43

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 589
    :cond_a
    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    .line 596
    .end local v15    # "endLength":I
    :cond_b
    sget-object v40, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRepeatTime()[I

    move-result-object v28

    .line 597
    .local v28, "repeatTime":[I
    const/16 v40, 0x0

    aget v40, v28, v40

    if-ltz v40, :cond_10

    .line 598
    const/16 v20, 0x0

    .line 599
    .local v20, "lengthBeforeRepeat":I
    const/16 v27, 0x0

    .line 600
    .local v27, "repeatLength":I
    const/16 v16, 0x0

    .line 601
    :goto_4
    move/from16 v0, v16

    if-ge v0, v10, :cond_d

    .line 602
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    move/from16 v40, v0

    if-nez v40, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    if-eqz v40, :cond_c

    .line 603
    const/16 v40, 0x0

    aget v40, v28, v40

    move/from16 v0, v40

    int-to-long v0, v0

    move-wide/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    move-wide/from16 v40, v0

    cmp-long v40, v42, v40

    if-lez v40, :cond_d

    .line 604
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->length()I

    move-result v40

    add-int v20, v20, v40

    .line 609
    :cond_c
    add-int/lit8 v16, v16, 0x1

    goto :goto_4

    .line 611
    :cond_d
    :goto_5
    move/from16 v0, v16

    if-ge v0, v10, :cond_f

    .line 612
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    move/from16 v40, v0

    if-nez v40, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    if-eqz v40, :cond_e

    .line 613
    const/16 v40, 0x1

    aget v40, v28, v40

    move/from16 v0, v40

    int-to-long v0, v0

    move-wide/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    move-wide/from16 v40, v0

    cmp-long v40, v42, v40

    if-lez v40, :cond_f

    .line 614
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->length()I

    move-result v40

    add-int v27, v27, v40

    .line 619
    :cond_e
    add-int/lit8 v16, v16, 0x1

    goto :goto_5

    .line 621
    :cond_f
    new-instance v40, Landroid/text/style/ForegroundColorSpan;

    const/16 v41, 0x8f

    const/16 v42, 0xb9

    const/16 v43, 0x3b

    invoke-static/range {v41 .. v43}, Landroid/graphics/Color;->rgb(III)I

    move-result v41

    invoke-direct/range {v40 .. v41}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int v41, v20, v27

    const/16 v42, 0x21

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    move/from16 v2, v20

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 623
    .end local v20    # "lengthBeforeRepeat":I
    .end local v27    # "repeatLength":I
    :cond_10
    new-instance v40, Landroid/text/style/ForegroundColorSpan;

    const/16 v41, 0x7d

    const/16 v42, 0x7d

    const/16 v43, 0x7d

    invoke-static/range {v41 .. v43}, Landroid/graphics/Color;->rgb(III)I

    move-result v41

    invoke-direct/range {v40 .. v41}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v41, 0x0

    const/16 v42, 0x21

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    move/from16 v2, v41

    move/from16 v3, v21

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 624
    new-instance v40, Landroid/text/style/ForegroundColorSpan;

    const/16 v41, 0x0

    const/16 v42, 0x7a

    const/16 v43, 0xd2

    invoke-static/range {v41 .. v43}, Landroid/graphics/Color;->rgb(III)I

    move-result v41

    invoke-direct/range {v40 .. v41}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int v41, v21, v19

    const/16 v42, 0x21

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    move/from16 v2, v21

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 625
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTrimRight:I

    move/from16 v40, v0

    if-lez v40, :cond_11

    .line 626
    new-instance v40, Landroid/text/style/ForegroundColorSpan;

    const/16 v41, 0x7d

    const/16 v42, 0x7d

    const/16 v43, 0x7d

    invoke-static/range {v41 .. v43}, Landroid/graphics/Color;->rgb(III)I

    move-result v41

    invoke-direct/range {v40 .. v41}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v41, 0x21

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v41

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 628
    :cond_11
    sget-object v40, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v40, :cond_15

    .line 629
    sget-object v40, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual/range {v40 .. v40}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 630
    .local v7, "bookmarkSize":I
    const/4 v6, 0x0

    .line 631
    .local v6, "bookmarkPosition":I
    const/16 v35, 0x0

    .line 632
    .local v35, "tempBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    const/16 v36, 0x0

    .line 634
    .local v36, "tempTextData":Lcom/sec/android/app/voicenote/common/util/TextData;
    const/16 v16, 0x0

    :goto_6
    move/from16 v0, v16

    if-ge v0, v7, :cond_15

    .line 635
    const/4 v6, 0x0

    .line 636
    const/16 v18, 0x0

    .local v18, "j":I
    :goto_7
    move/from16 v0, v18

    if-ge v0, v10, :cond_12

    .line 637
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    move/from16 v40, v0

    if-nez v40, :cond_14

    .line 638
    sget-object v40, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mBookmarks:Ljava/util/ArrayList;

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v35

    .end local v35    # "tempBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    check-cast v35, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    .line 639
    .restart local v35    # "tempBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v36

    .end local v36    # "tempTextData":Lcom/sec/android/app/voicenote/common/util/TextData;
    check-cast v36, Lcom/sec/android/app/voicenote/common/util/TextData;

    .line 641
    .restart local v36    # "tempTextData":Lcom/sec/android/app/voicenote/common/util/TextData;
    invoke-virtual/range {v35 .. v35}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v40

    move/from16 v0, v40

    int-to-long v0, v0

    move-wide/from16 v40, v0

    move-object/from16 v0, v36

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    move-wide/from16 v42, v0

    move-object/from16 v0, v36

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->duration:J

    move-wide/from16 v44, v0

    add-long v42, v42, v44

    cmp-long v40, v40, v42

    if-gez v40, :cond_13

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    if-eqz v40, :cond_13

    .line 642
    new-instance v40, Landroid/text/style/ForegroundColorSpan;

    const/16 v41, 0xdc

    const/16 v42, 0xaa

    const/16 v43, 0x0

    invoke-static/range {v41 .. v43}, Landroid/graphics/Color;->rgb(III)I

    move-result v41

    invoke-direct/range {v40 .. v41}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v41, v0

    const/16 v42, 0x0

    aget-object v41, v41, v42

    invoke-virtual/range {v41 .. v41}, Ljava/lang/String;->length()I

    move-result v41

    add-int v41, v41, v6

    const/16 v42, 0x21

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    move/from16 v2, v41

    move/from16 v3, v42

    invoke-virtual {v0, v1, v6, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 634
    :cond_12
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_6

    .line 644
    :cond_13
    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    if-eqz v40, :cond_14

    .line 645
    move-object/from16 v0, v36

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->length()I

    move-result v40

    add-int v6, v6, v40

    .line 636
    :cond_14
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_7

    .line 653
    .end local v6    # "bookmarkPosition":I
    .end local v7    # "bookmarkSize":I
    .end local v18    # "j":I
    .end local v35    # "tempBookmark":Lcom/sec/android/app/voicenote/common/util/Bookmark;
    .end local v36    # "tempTextData":Lcom/sec/android/app/voicenote/common/util/TextData;
    :cond_15
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getSearchText()Ljava/lang/String;

    move-result-object v30

    .line 654
    .local v30, "searchText":Ljava/lang/String;
    if-eqz v30, :cond_17

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->isEmpty()Z

    move-result v40

    if-nez v40, :cond_17

    .line 655
    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v30

    .line 656
    const/16 v29, 0x0

    .line 657
    .local v29, "searchPosition":I
    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v31

    .line 658
    .local v31, "searchTextLength":I
    invoke-virtual/range {v34 .. v34}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v37

    .line 659
    .local v37, "totalText":Ljava/lang/String;
    :cond_16
    :goto_8
    const/16 v40, -0x1

    move/from16 v0, v40

    move/from16 v1, v29

    if-eq v0, v1, :cond_17

    .line 660
    move-object/from16 v0, v37

    move-object/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v29

    .line 661
    const/16 v40, -0x1

    move/from16 v0, v40

    move/from16 v1, v29

    if-eq v0, v1, :cond_16

    .line 662
    new-instance v40, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSearchTextColor:I

    move/from16 v41, v0

    invoke-direct/range {v40 .. v41}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int v41, v29, v31

    const/16 v42, 0x21

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    move/from16 v2, v29

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 664
    new-instance v40, Landroid/text/style/StyleSpan;

    const/16 v41, 0x1

    invoke-direct/range {v40 .. v41}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int v41, v29, v31

    const/16 v42, 0x21

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    move/from16 v2, v29

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 665
    add-int v29, v29, v31

    goto :goto_8

    .line 670
    .end local v29    # "searchPosition":I
    .end local v31    # "searchTextLength":I
    .end local v37    # "totalText":Ljava/lang/String;
    :cond_17
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I

    move/from16 v40, v0

    const/16 v41, -0x1

    move/from16 v0, v40

    move/from16 v1, v41

    if-eq v0, v1, :cond_1b

    .line 671
    const/16 v33, 0x0

    .line 672
    .local v33, "selectWordOffset":I
    const/16 v32, 0x0

    .line 674
    .local v32, "selectWordLength":I
    const/16 v16, 0x0

    :goto_9
    move/from16 v0, v16

    if-ge v0, v10, :cond_1a

    .line 675
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    move/from16 v40, v0

    if-nez v40, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    if-eqz v40, :cond_18

    .line 676
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I

    move/from16 v40, v0

    move/from16 v0, v16

    move/from16 v1, v40

    if-ge v0, v1, :cond_19

    .line 677
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->length()I

    move-result v40

    add-int v33, v33, v40

    .line 674
    :cond_18
    :goto_a
    add-int/lit8 v16, v16, 0x1

    goto :goto_9

    .line 678
    :cond_19
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I

    move/from16 v40, v0

    move/from16 v0, v16

    move/from16 v1, v40

    if-ne v0, v1, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    if-eqz v40, :cond_18

    .line 679
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/app/voicenote/common/util/TextData;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    aget-object v40, v40, v41

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v32

    goto :goto_a

    .line 683
    :catch_0
    move-exception v14

    .line 684
    .local v14, "e":Ljava/lang/IndexOutOfBoundsException;
    sget-object v40, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "updateAllWordOnScreen : IndexOutOfBoundsException occurs during displaying Selected Word : "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I

    move/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    .end local v14    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_1a
    if-eqz v32, :cond_1b

    .line 688
    new-instance v40, Landroid/text/style/ForegroundColorSpan;

    const/high16 v41, -0x10000

    invoke-direct/range {v40 .. v41}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int v41, v33, v32

    const/16 v42, 0x21

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    move/from16 v2, v33

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 689
    new-instance v40, Landroid/text/style/StyleSpan;

    const/16 v41, 0x1

    invoke-direct/range {v40 .. v41}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int v41, v33, v32

    const/16 v42, 0x21

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    move/from16 v2, v33

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 693
    .end local v32    # "selectWordLength":I
    .end local v33    # "selectWordOffset":I
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/TextView;->clearFocus()V

    .line 694
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 695
    if-lez v11, :cond_1d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsMoving:Z

    move/from16 v40, v0

    if-nez v40, :cond_1d

    .line 696
    const/4 v12, 0x0

    .line 697
    .local v12, "currentLineStartOffset":I
    const/4 v13, 0x0

    .line 698
    .local v13, "delay":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;

    move-object/from16 v40, v0

    if-eqz v40, :cond_1f

    .line 699
    const/16 v13, 0x1f4

    .line 700
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTouchingHandler:Landroid/view/View;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/view/View;->getId()I

    move-result v40

    const v41, 0x7f0e004b

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_1e

    .line 701
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v40

    move-object/from16 v0, v40

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v12

    .line 708
    :goto_b
    new-instance v26, Landroid/graphics/Rect;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/Rect;-><init>()V

    .line 709
    .local v26, "rect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v26

    invoke-virtual {v0, v12, v1}, Landroid/widget/TextView;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 710
    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollView:Landroid/widget/ScrollView;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v41

    sub-int v40, v40, v41

    move/from16 v0, v40

    int-to-double v0, v0

    move-wide/from16 v24, v0

    .line 711
    .local v24, "readingLine":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollView:Landroid/widget/ScrollView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/ScrollView;->getHeight()I

    move-result v40

    move/from16 v0, v40

    int-to-double v0, v0

    move-wide/from16 v40, v0

    const-wide v42, 0x3fd3333333333333L    # 0.3

    mul-double v38, v40, v42

    .line 712
    .local v38, "topLine":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollView:Landroid/widget/ScrollView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/ScrollView;->getHeight()I

    move-result v40

    move/from16 v0, v40

    int-to-double v0, v0

    move-wide/from16 v40, v0

    const-wide v42, 0x3fe6666666666666L    # 0.7

    mul-double v8, v40, v42

    .line 713
    .local v8, "bottomLine":D
    cmpl-double v40, v24, v8

    if-gtz v40, :cond_1c

    cmpg-double v40, v24, v38

    if-gez v40, :cond_1d

    .line 714
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollView:Landroid/widget/ScrollView;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v42, v0

    move/from16 v0, v42

    int-to-double v0, v0

    move-wide/from16 v42, v0

    sub-double v42, v42, v8

    move-wide/from16 v0, v42

    double-to-int v0, v0

    move/from16 v42, v0

    invoke-virtual/range {v40 .. v42}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    .line 718
    .end local v8    # "bottomLine":D
    .end local v12    # "currentLineStartOffset":I
    .end local v13    # "delay":I
    .end local v24    # "readingLine":D
    .end local v26    # "rect":Landroid/graphics/Rect;
    .end local v38    # "topLine":D
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    move-object/from16 v40, v0

    new-instance v41, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$MoveTrimHandler;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;II)V

    invoke-virtual/range {v40 .. v41}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 703
    .restart local v12    # "currentLineStartOffset":I
    .restart local v13    # "delay":I
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v40

    move-object/from16 v0, v40

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v12

    goto/16 :goto_b

    .line 706
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextView:Landroid/widget/TextView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v40

    add-int v41, v21, v19

    invoke-virtual/range {v40 .. v41}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v12

    goto/16 :goto_b
.end method

.method public updateBoookmarks(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 750
    .local p1, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;

    const-string v1, "updateBoookmarks"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    if-eqz p1, :cond_0

    .line 752
    sput-object p1, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mBookmarks:Ljava/util/ArrayList;

    .line 753
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->updateAllWordOnScreen()V

    .line 755
    :cond_0
    return-void
.end method
