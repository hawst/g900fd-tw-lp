.class public Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;
.super Landroid/view/View;
.source "GradientColorPicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;
    }
.end annotation


# instance fields
.field private HEIGHT:I

.field private WIDTH:I

.field private mColorChangedListener:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;

.field private selector:Landroid/graphics/Bitmap;

.field private spectrum:Landroid/graphics/Bitmap;

.field private x:I

.field private y:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 51
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 38
    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->WIDTH:I

    .line 39
    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->HEIGHT:I

    .line 40
    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->x:I

    .line 41
    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->y:I

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->spectrum:Landroid/graphics/Bitmap;

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->selector:Landroid/graphics/Bitmap;

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 55
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->WIDTH:I

    .line 39
    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->HEIGHT:I

    .line 40
    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->x:I

    .line 41
    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->y:I

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->spectrum:Landroid/graphics/Bitmap;

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->selector:Landroid/graphics/Bitmap;

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->WIDTH:I

    .line 39
    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->HEIGHT:I

    .line 40
    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->x:I

    .line 41
    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->y:I

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->spectrum:Landroid/graphics/Bitmap;

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->selector:Landroid/graphics/Bitmap;

    .line 60
    return-void
.end method

.method private makeSpectrum()V
    .locals 24

    .prologue
    .line 109
    const/4 v2, 0x6

    new-array v7, v2, [I

    fill-array-data v7, :array_0

    .line 113
    .local v7, "colors":[I
    const/4 v2, 0x6

    new-array v8, v2, [F

    fill-array-data v8, :array_1

    .line 117
    .local v8, "positions":[F
    const/4 v2, 0x3

    new-array v15, v2, [F

    fill-array-data v15, :array_2

    .line 121
    .local v15, "pos1":[F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->WIDTH:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->HEIGHT:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 122
    .local v23, "spec":Landroid/graphics/Bitmap;
    new-instance v17, Landroid/graphics/Canvas;

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 123
    .local v17, "c":Landroid/graphics/Canvas;
    new-instance v21, Landroid/graphics/Paint;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Paint;-><init>()V

    .line 124
    .local v21, "mOvalHueSat":Landroid/graphics/Paint;
    new-instance v2, Landroid/graphics/LinearGradient;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->WIDTH:I

    int-to-float v5, v5

    const/4 v6, 0x0

    sget-object v9, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v2 .. v9}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 125
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 126
    const/4 v2, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 127
    new-instance v2, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->WIDTH:I

    const/4 v6, 0x1

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 129
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->WIDTH:I

    const/4 v3, 0x3

    filled-new-array {v2, v3}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, [[I

    .line 131
    .local v18, "drawnColors1":[[I
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->WIDTH:I

    move/from16 v0, v19

    if-ge v0, v2, :cond_0

    .line 132
    aget-object v2, v18, v19

    const/4 v3, 0x0

    const/4 v4, -0x1

    aput v4, v2, v3

    .line 133
    aget-object v2, v18, v19

    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v4

    aput v4, v2, v3

    .line 134
    aget-object v2, v18, v19

    const/4 v3, 0x2

    const/high16 v4, -0x1000000

    aput v4, v2, v3

    .line 131
    add-int/lit8 v19, v19, 0x1

    goto :goto_0

    .line 136
    :cond_0
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->recycle()V

    .line 138
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->WIDTH:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->HEIGHT:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->spectrum:Landroid/graphics/Bitmap;

    .line 139
    new-instance v20, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->spectrum:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 141
    .local v20, "last":Landroid/graphics/Canvas;
    new-instance v22, Landroid/graphics/Paint;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Paint;-><init>()V

    .line 142
    .local v22, "satu":Landroid/graphics/Paint;
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 143
    const/4 v2, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 145
    const/16 v19, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->WIDTH:I

    move/from16 v0, v19

    if-ge v0, v2, :cond_1

    .line 146
    new-instance v9, Landroid/graphics/LinearGradient;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->HEIGHT:I

    int-to-float v13, v2

    aget-object v14, v18, v19

    sget-object v16, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v9 .. v16}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 147
    move/from16 v0, v19

    int-to-float v10, v0

    const/4 v11, 0x0

    move/from16 v0, v19

    int-to-float v12, v0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->HEIGHT:I

    int-to-float v13, v2

    move-object/from16 v9, v20

    move-object/from16 v14, v22

    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 145
    add-int/lit8 v19, v19, 0x1

    goto :goto_1

    .line 149
    :cond_1
    return-void

    .line 109
    :array_0
    .array-data 4
        -0x10000
        -0x100
        -0xff0100
        -0xff0001
        -0xffff01
        -0xff01
    .end array-data

    .line 113
    :array_1
    .array-data 4
        0x0
        0x3e4ccccd    # 0.2f
        0x3ecccccd    # 0.4f
        0x3f19999a    # 0.6f
        0x3f4ccccd    # 0.8f
        0x3f800000    # 1.0f
    .end array-data

    .line 117
    :array_2
    .array-data 4
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->spectrum:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->spectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->spectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->selector:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->selector:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->selector:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 104
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->spectrum:Landroid/graphics/Bitmap;

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->selector:Landroid/graphics/Bitmap;

    .line 106
    return-void
.end method

.method public getColor()I
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->spectrum:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->x:I

    iget v2, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x40f00000    # 7.5f

    const/4 v2, 0x0

    .line 83
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->spectrum:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->spectrum:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v2, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 89
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 90
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->x:I

    int-to-float v0, v0

    sub-float/2addr v0, v3

    iget v1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->y:I

    int-to-float v1, v1

    sub-float/2addr v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->selector:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->selector:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v2, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 95
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 96
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 64
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 66
    if-eqz p1, :cond_1

    .line 67
    sub-int v0, p4, p2

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->WIDTH:I

    .line 68
    sub-int v0, p5, p3

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->HEIGHT:I

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->selector:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200d3

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->selector:Landroid/graphics/Bitmap;

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->spectrum:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->makeSpectrum()V

    .line 75
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 153
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->x:I

    .line 154
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->y:I

    .line 156
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 173
    :goto_0
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 160
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->x:I

    if-gez v0, :cond_0

    .line 161
    iput v1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->x:I

    .line 162
    :cond_0
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->y:I

    if-gez v0, :cond_1

    .line 163
    iput v1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->y:I

    .line 164
    :cond_1
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->y:I

    iget v1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->HEIGHT:I

    if-lt v0, v1, :cond_2

    .line 165
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->HEIGHT:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->y:I

    .line 166
    :cond_2
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->x:I

    iget v1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->WIDTH:I

    if-lt v0, v1, :cond_3

    .line 167
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->WIDTH:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->x:I

    .line 168
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->mColorChangedListener:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->getColor()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;->onColorChanged(I)V

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->invalidate()V

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 156
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setColorchangedListener(Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->mColorChangedListener:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;

    .line 178
    return-void
.end method
