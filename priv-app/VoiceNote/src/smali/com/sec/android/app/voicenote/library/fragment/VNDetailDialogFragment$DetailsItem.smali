.class Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;
.super Ljava/lang/Object;
.source "VNDetailDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DetailsItem"
.end annotation


# instance fields
.field private mDescription:Ljava/lang/String;

.field private mSummary:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "summary"    # Ljava/lang/String;

    .prologue
    .line 281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;->mTitle:Ljava/lang/String;

    .line 283
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;->mSummary:Ljava/lang/String;

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;->mSummary:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;->mDescription:Ljava/lang/String;

    .line 285
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "summary"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;->mTitle:Ljava/lang/String;

    .line 289
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;->mSummary:Ljava/lang/String;

    .line 290
    iput-object p3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;->mDescription:Ljava/lang/String;

    .line 291
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;->mSummary:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment$DetailsItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method
