.class public Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;
.super Landroid/app/Fragment;
.source "VNTrimFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;,
        Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;
    }
.end annotation


# static fields
.field private static final BOOKMARK_DOUBLE_TAP_DIST_THRESHOLD:F = 0.05f

.field private static final INTERNAL_EVENT_REPEAT_SET_PROGRESS:I = 0x73

.field private static final SAVESTATE_TRIMBAR_LEFT:Ljava/lang/String; = "savestate_trimbar_left"

.field private static final SAVESTATE_TRIMBAR_RIGHT:Ljava/lang/String; = "savestate_trimbar_right"

.field private static final TAG:Ljava/lang/String; = "VNTrimFragment"

.field private static mBookmarks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field private static mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

.field private static mSaveMode:I

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

.field private static mTrimResult:I


# instance fields
.field private final START_OF_ACTUAL_IMAGE_ON_BACKGROUND:I

.field private final TRIMSAVE_REQUESTCODE:I

.field private mBookmarkIndicators:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field private mCloseButton:Landroid/widget/ImageButton;

.field private mDuration:I

.field private mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mEndTime:Landroid/widget/TextView;

.field private mEventHandler:Landroid/os/Handler;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mIsResumePlay:Z

.field private mLeftEmpty:Landroid/widget/LinearLayout;

.field private mLeftEmptyProgress:Landroid/widget/LinearLayout;

.field private mLeftEmptyWidth:I

.field private mLeftHandle:Landroid/widget/ImageView;

.field private mOnGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

.field private mOneSecondWidth:I

.field private mParams:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/RelativeLayout$LayoutParams;",
            ">;"
        }
    .end annotation
.end field

.field private mPauseButton:Landroid/widget/ImageButton;

.field private mPlayButton:Landroid/widget/ImageButton;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mProgressLayout:Landroid/widget/RelativeLayout;

.field private mProgressTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;

.field private mRightEmpty:Landroid/widget/LinearLayout;

.field private mRightEmptyImage:Landroid/widget/ImageView;

.field private mRightEmptyProgress:Landroid/widget/LinearLayout;

.field private mRightEmptyWidth:I

.field private mRightHandle:Landroid/widget/ImageView;

.field private mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

.field private mSavetButton:Landroid/widget/ImageButton;

.field private mSeekbar:Landroid/widget/SeekBar;

.field private mSeekbarHolder:Landroid/widget/RelativeLayout;

.field private mSeekbarThumbWidth:I

.field private mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;

.field private mStartTime:Landroid/widget/TextView;

.field private mSystemMaxWidth:I

.field private mTempBitmap:Landroid/graphics/Bitmap;

.field private mThumbTopPart:Landroid/widget/ImageView;

.field private mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

.field private mTrimBgGray:Landroid/graphics/Bitmap;

.field private mTrimBgHeight:I

.field private mTrimBgWidth:I

.field private mTrimOffset:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 106
    sput-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 114
    sput-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;

    .line 120
    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveMode:I

    .line 121
    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimResult:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 131
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 79
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->TRIMSAVE_REQUESTCODE:I

    .line 80
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->START_OF_ACTUAL_IMAGE_ON_BACKGROUND:I

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbarHolder:Landroid/widget/RelativeLayout;

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCloseButton:Landroid/widget/ImageButton;

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmpty:Landroid/widget/LinearLayout;

    .line 87
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightHandle:Landroid/widget/ImageView;

    .line 88
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmpty:Landroid/widget/LinearLayout;

    .line 89
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyImage:Landroid/widget/ImageView;

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgGray:Landroid/graphics/Bitmap;

    .line 91
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 92
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 93
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 94
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    .line 95
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgHeight:I

    .line 96
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyWidth:I

    .line 97
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyWidth:I

    .line 98
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSystemMaxWidth:I

    .line 99
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyProgress:Landroid/widget/LinearLayout;

    .line 100
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyProgress:Landroid/widget/LinearLayout;

    .line 101
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 102
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mStartTime:Landroid/widget/TextView;

    .line 103
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEndTime:Landroid/widget/TextView;

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSavetButton:Landroid/widget/ImageButton;

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEventHandler:Landroid/os/Handler;

    .line 107
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I

    .line 108
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    .line 109
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    .line 110
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    .line 111
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;

    .line 112
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mIsResumePlay:Z

    .line 113
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    .line 115
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;

    .line 116
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mParams:Ljava/util/ArrayList;

    .line 117
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressLayout:Landroid/widget/RelativeLayout;

    .line 118
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mThumbTopPart:Landroid/widget/ImageView;

    .line 119
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mOneSecondWidth:I

    .line 127
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    .line 128
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbarThumbWidth:I

    .line 132
    return-void
.end method

.method static synthetic access$000()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->setProgressDialog()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$1302(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 73
    sput p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimResult:I

    return p0
.end method

.method static synthetic access$1400()Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;
    .param p1, "x1"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->convertIntegerToTimeText(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->prepareParams()V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->setIndicators()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightHandle:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSavetButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$700()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;)Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;
    .param p1, "x1"    # Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    return-object p1
.end method

.method static synthetic access$902(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 73
    sput p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveMode:I

    return p0
.end method

.method private convertIntegerToTimeText(I)Ljava/lang/String;
    .locals 7
    .param p1, "millisec"    # I

    .prologue
    .line 617
    div-int/lit16 v2, p1, 0x3e8

    .line 618
    .local v2, "sec":I
    div-int/lit8 v3, v2, 0x3c

    rem-int/lit8 v1, v3, 0x3c

    .line 619
    .local v1, "min":I
    div-int/lit16 v0, v2, 0xe10

    .line 620
    .local v0, "hour":I
    rem-int/lit8 v2, v2, 0x3c

    .line 622
    const-string v3, "%02d:%02d:%02d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static getSaveMode()I
    .locals 1

    .prologue
    .line 503
    sget v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveMode:I

    return v0
.end method

.method public static getTrimResult()I
    .locals 1

    .prologue
    .line 506
    sget v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimResult:I

    return v0
.end method

.method private hideBookmarkIndicators()V
    .locals 4

    .prologue
    .line 952
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_0

    .line 953
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 954
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 955
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 954
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 958
    .end local v0    # "i":I
    .end local v1    # "size":I
    :cond_0
    return-void
.end method

.method private initEventHandler()V
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEventHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 382
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$3;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEventHandler:Landroid/os/Handler;

    .line 478
    :cond_0
    return-void
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 611
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 612
    return-void
.end method

.method private declared-synchronized prepareParams()V
    .locals 14

    .prologue
    .line 895
    monitor-enter p0

    :try_start_0
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v9, :cond_1

    .line 922
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 898
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0201a8

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 899
    .local v4, "indicatorImg":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 900
    .local v5, "indicatorImgWidth":I
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v9

    sub-int v6, v9, v5

    .line 901
    .local v6, "length":I
    const/4 v7, 0x0

    .line 902
    .local v7, "padding":I
    const-wide/16 v2, 0x0

    .line 903
    .local v2, "first":J
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mParams:Ljava/util/ArrayList;

    .line 904
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 905
    .local v8, "size":I
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v9, :cond_0

    .line 908
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPlayerDuration()I

    move-result v0

    .line 909
    .local v0, "duration":I
    if-nez v0, :cond_2

    .line 910
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 895
    .end local v0    # "duration":I
    .end local v2    # "first":J
    .end local v4    # "indicatorImg":Landroid/graphics/drawable/Drawable;
    .end local v5    # "indicatorImgWidth":I
    .end local v6    # "length":I
    .end local v7    # "padding":I
    .end local v8    # "size":I
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 913
    .restart local v0    # "duration":I
    .restart local v2    # "first":J
    .restart local v4    # "indicatorImg":Landroid/graphics/drawable/Drawable;
    .restart local v5    # "indicatorImgWidth":I
    .restart local v6    # "length":I
    .restart local v7    # "padding":I
    .restart local v8    # "size":I
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v8, :cond_0

    .line 914
    int-to-long v10, v6

    :try_start_2
    sget-object v9, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v9}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v9

    int-to-long v12, v9

    mul-long v2, v10, v12

    .line 915
    int-to-long v10, v0

    div-long v10, v2, v10

    long-to-float v9, v10

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 916
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mParams:Ljava/util/ArrayList;

    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v11, -0x2

    const/4 v12, -0x2

    invoke-direct {v10, v11, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 919
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mParams:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v10, 0x8

    const v11, 0x7f0e00a6

    invoke-virtual {v9, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 920
    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mParams:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v10, 0x9

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v9, v7, v10, v11, v12}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 913
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static releaseService()V
    .locals 1

    .prologue
    .line 614
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 615
    return-void
.end method

.method private restoreInstance(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v7, 0xf

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1100
    const-string v2, "savestate_trimbar_left"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    .line 1101
    .local v0, "leftValue":[I
    const-string v2, "savestate_trimbar_right"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    .line 1102
    .local v1, "rightValue":[I
    if-eqz v0, :cond_3

    .line 1103
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmpty:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1104
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1105
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyProgress:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1106
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    aget v3, v0, v6

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    mul-int/2addr v3, v4

    aget v4, v0, v5

    div-int/2addr v3, v4

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1107
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyWidth:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mOneSecondWidth:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    if-le v2, v3, :cond_0

    .line 1108
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyWidth:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mOneSecondWidth:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1110
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v3, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1111
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gt v2, v7, :cond_1

    .line 1112
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1113
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v3, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1114
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    if-lez v2, :cond_1

    .line 1115
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1118
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmpty:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1119
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1120
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v3, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    add-int/lit8 v3, v3, -0xd

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1121
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    if-gez v2, :cond_2

    .line 1122
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1124
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyProgress:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1125
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyWidth:I

    .line 1126
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mStartTime:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->convertIntegerToTimeText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1128
    :cond_3
    if-eqz v1, :cond_9

    .line 1129
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmpty:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1130
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1131
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyProgress:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1132
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    aget v3, v1, v6

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    mul-int/2addr v3, v4

    aget v4, v1, v5

    div-int/2addr v3, v4

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1133
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyWidth:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mOneSecondWidth:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    if-le v2, v3, :cond_4

    .line 1134
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyWidth:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mOneSecondWidth:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1136
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v3, v3, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1137
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    if-gt v2, v7, :cond_5

    .line 1138
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1140
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmpty:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1141
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightHandle:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1142
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    if-lez v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    if-ge v2, v3, :cond_7

    .line 1143
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTempBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_6

    .line 1144
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTempBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1146
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgGray:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgGray:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgHeight:I

    invoke-static {v2, v3, v6, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 1148
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyImage:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTempBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1150
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v3, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    add-int/lit8 v3, v3, -0xd

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1151
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    if-gez v2, :cond_8

    .line 1152
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1154
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyProgress:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1155
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyWidth:I

    .line 1156
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getEndTime()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->convertIntegerToTimeText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1158
    :cond_9
    return-void
.end method

.method public static setBookmark(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1160
    .local p0, "bookmarks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/Bookmark;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;

    .line 1161
    if-eqz p0, :cond_0

    .line 1162
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1163
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-direct {v3, v1}, Lcom/sec/android/app/voicenote/common/util/Bookmark;-><init>(Lcom/sec/android/app/voicenote/common/util/Bookmark;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1162
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1166
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private setIndicators()V
    .locals 10

    .prologue
    .line 924
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    .line 925
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->hideBookmarkIndicators()V

    .line 927
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 928
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 930
    .local v1, "size":I
    if-nez v1, :cond_1

    .line 950
    .end local v1    # "size":I
    :cond_0
    :goto_0
    return-void

    .line 933
    .restart local v1    # "size":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mParams:Ljava/util/ArrayList;

    monitor-enter v4

    .line 934
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mParams:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 935
    const-string v2, "VNTrimFragment"

    const-string v3, "mParams is empty"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    monitor-exit v4

    goto :goto_0

    .line 948
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 938
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 939
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/Bookmark;->getElapsed()I

    move-result v2

    int-to-long v8, v2

    invoke-direct {v5, v6, v8, v9}, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;-><init>(Landroid/content/Context;J)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 940
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;

    const v3, 0x7f0201a8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;->setImageResource(I)V

    .line 941
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;->setClickable(Z)V

    .line 942
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;->setFocusable(Z)V

    .line 943
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 944
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;->bringToFront()V

    .line 945
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;

    const v3, 0x7f0b001a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/common/BookmarkImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 946
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarkIndicators:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mParams:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v5, v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 938
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 948
    :cond_3
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public static setProgressCallback(Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;)V
    .locals 0
    .param p0, "callback"    # Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    .prologue
    .line 1188
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    .line 1189
    return-void
.end method

.method private setProgressDialog()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1169
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1170
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 1171
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f0b0157

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 1172
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 1173
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 1174
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1175
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, -0x2

    const v2, 0x7f0b0021

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$5;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1184
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1185
    return-void
.end method

.method private setProgressHoverWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 774
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.feature.hovering_ui"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 775
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 776
    .local v0, "hpw":Landroid/widget/HoverPopupWindow;
    if-nez v0, :cond_1

    .line 870
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_0
    :goto_0
    return-void

    .line 779
    .restart local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_1
    invoke-virtual {v0, v3}, Landroid/widget/HoverPopupWindow;->setInstanceOfProgressBar(Z)V

    .line 780
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->enableProgressAirView(Landroid/content/ContentResolver;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 781
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setHoverPopupType(I)V

    .line 782
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarHoverListener(Landroid/widget/SeekBar$OnSeekBarHoverListener;)V

    goto :goto_0

    .line 785
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setHoverPopupType(I)V

    .line 786
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$4;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarHoverListener(Landroid/widget/SeekBar$OnSeekBarHoverListener;)V

    .line 868
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setHoverPopupType(I)V

    goto :goto_0
.end method


# virtual methods
.method public doPause()V
    .locals 2

    .prologue
    .line 1192
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    .line 1193
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 1194
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->stopProgress()V

    .line 1195
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1196
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1197
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1199
    :cond_0
    return-void
.end method

.method public doResume()V
    .locals 2

    .prologue
    .line 1202
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    .line 1203
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumePlay()V

    .line 1204
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->startProgress()V

    .line 1205
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1206
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1208
    :cond_0
    return-void
.end method

.method public getEndTime()I
    .locals 6

    .prologue
    .line 554
    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyWidth:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    if-nez v2, :cond_1

    .line 555
    :cond_0
    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I

    .line 559
    :goto_0
    return v2

    .line 558
    :cond_1
    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyWidth:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    int-to-double v4, v4

    div-double v0, v2, v4

    .line 559
    .local v0, "percentage":D
    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I

    int-to-double v2, v2

    mul-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    goto :goto_0
.end method

.method public getPositionForHander(I)I
    .locals 3
    .param p1, "time"    # I

    .prologue
    .line 563
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    int-to-float v0, v0

    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimOffset:I

    add-int/2addr v1, v0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmpty:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v0, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getStartTime()I
    .locals 6

    .prologue
    .line 545
    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyWidth:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    if-nez v2, :cond_1

    .line 546
    :cond_0
    const/4 v2, 0x0

    .line 550
    :goto_0
    return v2

    .line 549
    :cond_1
    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyWidth:I

    int-to-double v2, v2

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    int-to-double v4, v4

    div-double v0, v2, v4

    .line 550
    .local v0, "percentage":D
    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I

    int-to-double v2, v2

    mul-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    goto :goto_0
.end method

.method protected isAmrFile()Z
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 481
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 482
    .local v6, "activity":Landroid/app/Activity;
    if-nez v6, :cond_0

    move v0, v9

    .line 498
    :goto_0
    return v0

    .line 485
    :cond_0
    const/4 v7, 0x0

    .line 486
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 487
    .local v8, "mimetype":Ljava/lang/String;
    new-array v4, v10, [Ljava/lang/String;

    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v9

    .line 488
    .local v4, "args":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "_id == ?"

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 489
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 490
    const-string v0, "mime_type"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 492
    :cond_1
    if-eqz v7, :cond_2

    .line 493
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 495
    :cond_2
    if-eqz v8, :cond_3

    const-string v0, "audio/amr"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v10

    .line 496
    goto :goto_0

    :cond_3
    move v0, v9

    .line 498
    goto :goto_0
.end method

.method public moveHandler(Landroid/view/View;II)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "x"    # I
    .param p3, "exactTime"    # I

    .prologue
    const/4 v8, 0x0

    .line 989
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    .line 990
    .local v1, "handleWidth":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 991
    .local v2, "id":I
    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSystemMaxWidth:I

    sub-int/2addr v4, v1

    if-le p2, v4, :cond_6

    .line 992
    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSystemMaxWidth:I

    sub-int p2, v4, v1

    .line 997
    :cond_0
    :goto_0
    const v4, 0x7f0e00ba

    if-ne v2, v4, :cond_8

    .line 998
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmpty:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 999
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1000
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyProgress:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1001
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    sub-int v5, p2, v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1002
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyWidth:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mOneSecondWidth:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    if-le v4, v5, :cond_1

    .line 1003
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    iget v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyWidth:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mOneSecondWidth:I

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1005
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v6, v6, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v6, v6, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1006
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v4, :cond_2

    .line 1007
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iput v8, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1009
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmpty:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1010
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1011
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    add-int/lit8 v5, v5, -0xd

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1012
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    if-gez v4, :cond_3

    .line 1013
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    iput v8, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1015
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyProgress:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1016
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iput v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyWidth:I

    .line 1017
    if-gez p3, :cond_7

    .line 1018
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v3

    .line 1019
    .local v3, "startTime":I
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mStartTime:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->convertIntegerToTimeText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1020
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v4

    if-gt v4, v3, :cond_4

    .line 1021
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1022
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v4, :cond_4

    .line 1023
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 1084
    .end local v3    # "startTime":I
    :cond_4
    :goto_1
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    if-eqz v4, :cond_5

    .line 1085
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getEndTime()I

    move-result v6

    invoke-interface {v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;->onTrimChanged(II)V

    .line 1087
    :cond_5
    return-void

    .line 994
    :cond_6
    if-ge p2, v1, :cond_0

    .line 995
    move p2, v1

    goto/16 :goto_0

    .line 1027
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mStartTime:Landroid/widget/TextView;

    invoke-direct {p0, p3}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->convertIntegerToTimeText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1028
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v4

    if-gt v4, p3, :cond_4

    .line 1029
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v4, p3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1030
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v4, :cond_4

    .line 1031
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4, p3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    goto :goto_1

    .line 1035
    :cond_8
    const v4, 0x7f0e00bb

    if-ne v2, v4, :cond_4

    .line 1036
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmpty:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1037
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1038
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyProgress:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1039
    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSystemMaxWidth:I

    sub-int p2, v4, p2

    .line 1040
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    sub-int v5, p2, v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1041
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyWidth:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mOneSecondWidth:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    if-le v4, v5, :cond_9

    .line 1042
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    iget v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyWidth:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mOneSecondWidth:I

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1044
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v6, v6, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v6, v6, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1045
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    if-gez v4, :cond_a

    .line 1046
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    iput v8, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1048
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmpty:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1049
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightHandle:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mHandleParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1050
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    if-lez v4, :cond_c

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    if-ge v4, v5, :cond_c

    .line 1051
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTempBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_b

    .line 1052
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTempBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 1054
    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgGray:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgGray:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v6, v6, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v6, v6, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgHeight:I

    invoke-static {v4, v5, v8, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 1056
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyImage:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTempBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1058
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    add-int/lit8 v5, v5, -0xd

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1059
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    if-gez v4, :cond_d

    .line 1060
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    iput v8, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1062
    :cond_d
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyProgress:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyProgressParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1063
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEmptyParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iput v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyWidth:I

    .line 1064
    if-gez p3, :cond_e

    .line 1065
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getEndTime()I

    move-result v0

    .line 1066
    .local v0, "endTime":I
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->convertIntegerToTimeText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1067
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v4

    if-lt v4, v0, :cond_4

    .line 1068
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1069
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v4, :cond_4

    .line 1070
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    goto/16 :goto_1

    .line 1074
    .end local v0    # "endTime":I
    :cond_e
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, p3}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->convertIntegerToTimeText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1075
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v4

    if-lt v4, p3, :cond_4

    .line 1076
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v4, p3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1077
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v4, :cond_4

    .line 1078
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4, p3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    goto/16 :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 522
    if-ne p1, v3, :cond_2

    if-ne p2, v2, :cond_2

    .line 523
    const-string v1, "what"

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 524
    .local v0, "what":I
    if-lez v0, :cond_2

    .line 525
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEventHandler:Landroid/os/Handler;

    if-nez v1, :cond_1

    .line 526
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->isResumed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 527
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->initEventHandler()V

    .line 532
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 533
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 537
    .end local v0    # "what":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_3

    .line 538
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 541
    :cond_3
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 542
    :cond_4
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 510
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 511
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 512
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 290
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 351
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 293
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    const/4 v0, 0x0

    .line 294
    .local v0, "bchangeFocus":Z
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v2, :cond_0

    .line 298
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 299
    .local v1, "id":I
    const v2, 0x7f0e00c0

    if-ne v1, v2, :cond_4

    .line 300
    check-cast p1, Landroid/widget/ImageButton;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSavetButton:Landroid/widget/ImageButton;

    .line 301
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSavetButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 302
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSavetButton:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/widget/ImageButton;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 310
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-direct {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    .line 311
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getTag()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->setTargetFragmentTag(Ljava/lang/String;I)V

    .line 312
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getEndTime()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->setExpectedFileSize(Ljava/lang/String;II)V

    .line 314
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 315
    const-string v2, "VNTrimFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onClick: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "- state : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->isFinishing()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 318
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "FRAGMENT_DIALOG"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 321
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 322
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 323
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 324
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v2, v7}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onUpdateState(Z)V

    .line 325
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->stopProgress()V

    .line 350
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v1, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onControlButtonSelected(IJ)Z

    goto/16 :goto_0

    .line 326
    .restart local p1    # "view":Landroid/view/View;
    :cond_4
    const v2, 0x7f0e00c1

    if-ne v1, v2, :cond_9

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallIdle(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallActive(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 328
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v0, 0x1

    .line 329
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 330
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 331
    if-eqz v0, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 332
    :cond_6
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v2

    const/16 v3, 0x18

    if-ne v2, v3, :cond_7

    .line 333
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumePlay()V

    .line 335
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->startProgress()V

    .line 336
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v2, v7}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onUpdateStateNoModeChange(Z)V

    goto :goto_1

    .line 338
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b00d6

    invoke-static {v2, v3, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 341
    :cond_9
    const v2, 0x7f0e00c2

    if-ne v1, v2, :cond_3

    .line 342
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v0, 0x1

    .line 343
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 344
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 345
    if-eqz v0, :cond_b

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 346
    :cond_b
    sget-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 347
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->stopProgress()V

    .line 348
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v2, v7}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onUpdateStateNoModeChange(Z)V

    goto/16 :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 135
    const v4, 0x7f030041

    invoke-virtual {p1, v4, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 136
    .local v3, "view":Landroid/view/View;
    const v4, 0x7f0e00b9

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SeekBar;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    .line 137
    const v4, 0x7f0e00bf

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCloseButton:Landroid/widget/ImageButton;

    .line 138
    const v4, 0x7f0e00ba

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;

    .line 139
    const v4, 0x7f0e00b1

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmpty:Landroid/widget/LinearLayout;

    .line 140
    const v4, 0x7f0e00bb

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightHandle:Landroid/widget/ImageView;

    .line 141
    const v4, 0x7f0e00b2

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmpty:Landroid/widget/LinearLayout;

    .line 142
    const v4, 0x7f0e00b3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyImage:Landroid/widget/ImageView;

    .line 143
    const v4, 0x7f0e00b5

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyProgress:Landroid/widget/LinearLayout;

    .line 144
    const v4, 0x7f0e00b6

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyProgress:Landroid/widget/LinearLayout;

    .line 145
    const v4, 0x7f0e00bc

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mStartTime:Landroid/widget/TextView;

    .line 146
    const v4, 0x7f0e00bd

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEndTime:Landroid/widget/TextView;

    .line 147
    const v4, 0x7f0e00c0

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSavetButton:Landroid/widget/ImageButton;

    .line 148
    const v4, 0x7f0e00c1

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    .line 149
    const v4, 0x7f0e00c2

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    .line 150
    const v4, 0x7f0e00b4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressLayout:Landroid/widget/RelativeLayout;

    .line 151
    const v4, 0x7f0e00b8

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mThumbTopPart:Landroid/widget/ImageView;

    .line 152
    const v4, 0x7f0e00b7

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbarHolder:Landroid/widget/RelativeLayout;

    .line 153
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v4, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 155
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v4, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 156
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mThumbTopPart:Landroid/widget/ImageView;

    invoke-virtual {v4, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 157
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSavetButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v4, p0}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 161
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v4, p0}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 163
    const v4, 0x7f0e00b0

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 164
    .local v2, "seekEditGroupLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v2, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 165
    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mOnGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 204
    new-instance v4, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mOnGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v4, v5, v6}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mGestureDetector:Landroid/view/GestureDetector;

    .line 205
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mGestureDetector:Landroid/view/GestureDetector;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mOnGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-virtual {v4, v5}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 207
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 208
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 209
    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSystemMaxWidth:I

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 211
    .local v1, "rotation":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0201aa

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgGray:Landroid/graphics/Bitmap;

    .line 212
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgGray:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    .line 213
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgGray:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgHeight:I

    .line 214
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v4, :cond_0

    .line 215
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPlayerDuration()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I

    .line 217
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mStartTime:Landroid/widget/TextView;

    invoke-direct {p0, v7}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->convertIntegerToTimeText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEndTime:Landroid/widget/TextView;

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I

    invoke-direct {p0, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->convertIntegerToTimeText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->initEventHandler()V

    .line 220
    new-instance v4, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEventHandler:Landroid/os/Handler;

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimAudioUtil:Lcom/sec/android/app/voicenote/library/common/VNTrimAudioUtil;

    .line 221
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v4, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 222
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setMax(I)V

    .line 223
    sget-object v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v4, :cond_1

    .line 224
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 226
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->setProgressHoverWindow()V

    .line 227
    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;

    invoke-direct {v4, p0, v8}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;)V

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;

    .line 228
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;

    new-array v5, v7, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09003c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimOffset:I

    .line 230
    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I

    if-eqz v4, :cond_3

    .line 231
    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I

    int-to-float v5, v5

    const/high16 v6, 0x447a0000    # 1000.0f

    div-float/2addr v5, v6

    div-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mOneSecondWidth:I

    .line 235
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbarThumbWidth:I

    .line 236
    if-eqz p3, :cond_2

    .line 237
    invoke-direct {p0, p3}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->restoreInstance(Landroid/os/Bundle;)V

    .line 239
    :cond_2
    sput v7, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveMode:I

    .line 241
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->seTrimCallback(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;)V

    .line 244
    return-object v3

    .line 233
    :cond_3
    iput v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mOneSecondWidth:I

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 248
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->seTrimCallback(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$OnSttTrimHandlerPosListener;)V

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->stopProgress()V

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;->cancel(Z)Z

    .line 252
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ShowBookmarkIndicatorsTask;

    .line 254
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    .line 255
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCloseButton:Landroid/widget/ImageButton;

    .line 256
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;

    .line 257
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmpty:Landroid/widget/LinearLayout;

    .line 258
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightHandle:Landroid/widget/ImageView;

    .line 259
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmpty:Landroid/widget/LinearLayout;

    .line 260
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyImage:Landroid/widget/ImageView;

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgGray:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgGray:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 263
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgGray:Landroid/graphics/Bitmap;

    .line 265
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyProgress:Landroid/widget/LinearLayout;

    .line 266
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyProgress:Landroid/widget/LinearLayout;

    .line 267
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mStartTime:Landroid/widget/TextView;

    .line 268
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEndTime:Landroid/widget/TextView;

    .line 269
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSavetButton:Landroid/widget/ImageButton;

    .line 270
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    .line 271
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    .line 272
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressLayout:Landroid/widget/RelativeLayout;

    .line 273
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEventHandler:Landroid/os/Handler;

    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_2

    .line 276
    sput-object v2, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mBookmarks:Ljava/util/ArrayList;

    .line 278
    :cond_2
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_3

    .line 279
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0, v3, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setTrimTime(II)V

    .line 282
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->dismissAllowingStateLoss()V

    .line 284
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSaveDialog:Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;

    .line 286
    :cond_4
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 287
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 516
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 517
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 518
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 961
    const/4 v0, 0x0

    .line 962
    .local v0, "move":I
    const/4 v1, 0x0

    .line 963
    .local v1, "result":I
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    .line 964
    sparse-switch p2, :sswitch_data_0

    .line 975
    const/4 v2, 0x0

    .line 986
    :cond_0
    :goto_0
    return v2

    .line 966
    :sswitch_0
    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    mul-int/lit8 v3, v3, -0x1

    div-int/lit8 v0, v3, 0x32

    .line 980
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f0e00ba

    if-ne v3, v4, :cond_1

    .line 981
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getRight()I

    move-result v3

    add-int v1, v3, v0

    .line 985
    :goto_2
    const/4 v3, -0x1

    invoke-virtual {p0, p1, v1, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->moveHandler(Landroid/view/View;II)V

    goto :goto_0

    .line 969
    :sswitch_1
    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    mul-int/lit8 v3, v3, 0x1

    div-int/lit8 v0, v3, 0x32

    .line 970
    goto :goto_1

    .line 972
    :sswitch_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    const v4, 0x7f0e00bf

    const-wide/16 v6, 0x0

    invoke-interface {v3, v4, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onControlButtonSelected(IJ)Z

    goto :goto_0

    .line 983
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLeft()I

    move-result v3

    add-int v1, v3, v0

    goto :goto_2

    .line 964
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
    .end sparse-switch
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 8
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 643
    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v5, :cond_0

    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v5

    if-eqz v5, :cond_0

    if-nez p2, :cond_0

    .line 644
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v5

    sget-object v6, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v6

    if-eq v5, v6, :cond_0

    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v5

    if-eq p2, v5, :cond_0

    .line 646
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v5

    invoke-virtual {p1, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 674
    :goto_0
    return-void

    .line 650
    :cond_0
    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    if-eqz v5, :cond_1

    .line 651
    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    invoke-interface {v5, p2}, Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;->onProgressChanged(I)V

    .line 653
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getWidth()I

    move-result v2

    .line 654
    .local v2, "seekbarWidth":I
    if-gtz v2, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEventHandler:Landroid/os/Handler;

    if-eqz v5, :cond_2

    .line 655
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 656
    .local v1, "msg":Landroid/os/Message;
    const/16 v5, 0x73

    iput v5, v1, Landroid/os/Message;->what:I

    .line 657
    iput p2, v1, Landroid/os/Message;->arg1:I

    .line 658
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mEventHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x64

    invoke-virtual {v5, v1, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 661
    .end local v1    # "msg":Landroid/os/Message;
    :cond_2
    if-eqz p3, :cond_5

    .line 662
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v3

    .line 663
    .local v3, "startTime":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getEndTime()I

    move-result v0

    .line 664
    .local v0, "endTime":I
    if-lt p2, v0, :cond_4

    .line 665
    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 672
    .end local v0    # "endTime":I
    .end local v3    # "startTime":I
    :cond_3
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v5

    const/high16 v6, 0x3f800000    # 1.0f

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getThumbOffset()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mThumbTopPart:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 673
    .local v4, "xPos":I
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mThumbTopPart:Landroid/widget/ImageView;

    int-to-float v6, v4

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setX(F)V

    goto :goto_0

    .line 666
    .end local v4    # "xPos":I
    .restart local v0    # "endTime":I
    .restart local v3    # "startTime":I
    :cond_4
    if-gt p2, v3, :cond_3

    .line 667
    invoke-virtual {p1, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_1

    .line 669
    .end local v0    # "endTime":I
    .end local v3    # "startTime":I
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;

    if-nez v5, :cond_3

    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->isResumed()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 670
    sget-object v5, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5, p2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1091
    const-string v2, "VNTrimFragment"

    const-string v3, "onSaveInstanceState:ChangingConfigurations"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    new-array v0, v6, [I

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftEmptyWidth:I

    aput v2, v0, v4

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    aput v2, v0, v5

    .line 1093
    .local v0, "leftValue":[I
    new-array v1, v6, [I

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightEmptyWidth:I

    aput v2, v1, v4

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mTrimBgWidth:I

    aput v2, v1, v5

    .line 1094
    .local v1, "rightValue":[I
    const-string v2, "savestate_trimbar_left"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 1095
    const-string v2, "savestate_trimbar_right"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 1097
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1098
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 354
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    .line 355
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v0

    const/16 v1, 0x17

    if-ne v0, v1, :cond_2

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->startProgress()V

    .line 366
    :cond_0
    :goto_0
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    if-eqz v0, :cond_1

    .line 367
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getEndTime()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;->onTrimChanged(II)V

    .line 369
    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 370
    return-void

    .line 361
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 677
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 678
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->stopProgress()V

    .line 679
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onUpdateState(Z)V

    .line 681
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mIsResumePlay:Z

    .line 683
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1231
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    .line 1232
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getEndTime()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setTrimTime(II)V

    .line 1233
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    if-eqz v0, :cond_0

    .line 1234
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressCallback:Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;

    invoke-interface {v0, v3, v3}, Lcom/sec/android/app/voicenote/common/util/ProgressChangedCallback;->onTrimChanged(II)V

    .line 1237
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 1238
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v2, 0x0

    .line 686
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_1

    .line 696
    :cond_0
    :goto_0
    return-void

    .line 689
    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 690
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mIsResumePlay:Z

    if-eqz v0, :cond_0

    .line 691
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumePlay()V

    .line 692
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->startProgress()V

    .line 693
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onUpdateState(Z)V

    .line 694
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mIsResumePlay:Z

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v12, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 568
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {p1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightHandle:Landroid/widget/ImageView;

    invoke-virtual {p1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 569
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 570
    .local v0, "action":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v7

    float-to-int v6, v7

    .line 571
    .local v6, "x":I
    if-ne v12, v0, :cond_2

    .line 572
    const/4 v7, -0x1

    invoke-virtual {p0, p1, v6, v7}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->moveHandler(Landroid/view/View;II)V

    .line 606
    .end local v0    # "action":I
    .end local v6    # "x":I
    :cond_1
    :goto_0
    return v8

    .line 573
    .restart local v0    # "action":I
    .restart local v6    # "x":I
    :cond_2
    if-ne v8, v0, :cond_4

    .line 574
    if-nez v0, :cond_3

    move v7, v8

    :goto_1
    invoke-virtual {p1, v7}, Landroid/view/View;->setPressed(Z)V

    .line 575
    invoke-virtual {p1, v9}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    :cond_3
    move v7, v9

    .line 574
    goto :goto_1

    .line 576
    :cond_4
    if-nez v0, :cond_1

    .line 577
    invoke-virtual {p1, v8}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 580
    .end local v0    # "action":I
    .end local v6    # "x":I
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mThumbTopPart:Landroid/widget/ImageView;

    invoke-virtual {p1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 581
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 582
    .restart local v0    # "action":I
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbarHolder:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 583
    .local v2, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I

    int-to-float v7, v7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v10

    iget v11, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    int-to-float v11, v11

    sub-float/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v11}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v11

    int-to-float v11, v11

    sub-float/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v11}, Landroid/widget/SeekBar;->getThumbOffset()I

    move-result v11

    int-to-float v11, v11

    add-float/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mThumbTopPart:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    add-float/2addr v10, v11

    mul-float/2addr v7, v10

    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v10}, Landroid/widget/SeekBar;->getWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v11}, Landroid/widget/SeekBar;->getThumbOffset()I

    move-result v11

    add-int/2addr v10, v11

    int-to-float v10, v10

    div-float/2addr v7, v10

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 584
    .local v5, "time":I
    if-ne v12, v0, :cond_8

    .line 585
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v4

    .line 586
    .local v4, "startTime":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getEndTime()I

    move-result v1

    .line 587
    .local v1, "endTime":I
    if-lt v5, v1, :cond_6

    .line 588
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 589
    :cond_6
    if-gt v5, v4, :cond_7

    .line 590
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 592
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_0

    .line 594
    .end local v1    # "endTime":I
    .end local v4    # "startTime":I
    :cond_8
    if-ne v8, v0, :cond_1

    .line 595
    if-nez v0, :cond_9

    move v9, v8

    :cond_9
    invoke-virtual {p1, v9}, Landroid/view/View;->setPressed(Z)V

    goto/16 :goto_0

    .line 599
    .end local v0    # "action":I
    .end local v2    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v5    # "time":I
    :cond_a
    new-array v3, v12, [I

    .line 600
    .local v3, "seekbarLocation":[I
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    if-nez v7, :cond_b

    move v8, v9

    .line 601
    goto/16 :goto_0

    .line 602
    :cond_b
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v3}, Landroid/widget/SeekBar;->getLocationOnScreen([I)V

    .line 603
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v7

    aget v8, v3, v8

    int-to-float v8, v8

    cmpl-float v7, v7, v8

    if-lez v7, :cond_c

    .line 604
    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v7, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    goto/16 :goto_0

    :cond_c
    move v8, v9

    .line 606
    goto/16 :goto_0
.end method

.method public onTrimHandlerChanged(II)V
    .locals 5
    .param p1, "leftTrim"    # I
    .param p2, "rightTrim"    # I

    .prologue
    const/4 v4, -0x1

    .line 1242
    if-ne p1, v4, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v2

    if-eq p1, v2, :cond_2

    .line 1243
    const/4 v0, 0x0

    .line 1244
    .local v0, "leftX":I
    if-le p1, v4, :cond_1

    .line 1245
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getPositionForHander(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    div-int/lit8 v3, v3, 0x4

    sub-int v0, v2, v3

    .line 1247
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {p0, v2, v0, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->moveHandler(Landroid/view/View;II)V

    .line 1249
    .end local v0    # "leftX":I
    :cond_2
    if-ne p2, v4, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getEndTime()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mDuration:I

    if-eq v2, v3, :cond_4

    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getEndTime()I

    move-result v2

    if-eq p2, v2, :cond_4

    .line 1250
    const/4 v1, 0x0

    .line 1251
    .local v1, "rightX":I
    if-ne p2, v4, :cond_5

    .line 1252
    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSystemMaxWidth:I

    add-int/lit8 v1, v2, -0x1

    .line 1256
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mRightHandle:Landroid/widget/ImageView;

    invoke-virtual {p0, v2, v1, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->moveHandler(Landroid/view/View;II)V

    .line 1258
    .end local v1    # "rightX":I
    :cond_4
    return-void

    .line 1254
    .restart local v1    # "rightX":I
    :cond_5
    invoke-virtual {p0, p2}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getPositionForHander(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int v1, v2, v3

    goto :goto_0
.end method

.method public startProgress()V
    .locals 2

    .prologue
    .line 704
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;

    .line 705
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 706
    return-void
.end method

.method public stopPlay()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 375
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onUpdateState(Z)V

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 378
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 379
    return-void
.end method

.method public stopProgress()V
    .locals 2

    .prologue
    .line 698
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 699
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->cancel(Z)Z

    .line 700
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mProgressTask:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;

    .line 702
    :cond_0
    return-void
.end method

.method public updateTrimView()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1211
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v1, :cond_0

    .line 1212
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getState()I

    move-result v0

    .line 1213
    .local v0, "state":I
    packed-switch v0, :pswitch_data_0

    .line 1228
    .end local v0    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 1215
    .restart local v0    # "state":I
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->startProgress()V

    .line 1216
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1217
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 1220
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->stopProgress()V

    .line 1221
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1222
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 1213
    nop

    :pswitch_data_0
    .packed-switch 0x17
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
