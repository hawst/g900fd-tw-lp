.class public Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "VNWidgetProvider.java"


# static fields
.field public static final ACTION_APPWIDGET_UPDATE:Ljava/lang/String; = "com.sec.android.app.voicenote.appwidget.update"

.field public static final ACTION_TICKER_REQUEST_STATE:Ljava/lang/String; = "com.sec.android.app.voicenote.ticker.request_state"

.field private static final CLASS_FULLNAME:Ljava/lang/String; = "com.sec.android.app.voicenote.common.service.VNWidgetProvider"

.field public static final RECEIVER_PERMISSION:Ljava/lang/String; = "com.sec.android.app.voicenote.appwidget.permission"

.field private static final TAG:Ljava/lang/String; = "VNWidgetProvider"

.field private static isSetContextualEvent:Z

.field private static mLastAction:Ljava/lang/String;

.field private static mLockscreenIntentFilter:Landroid/content/IntentFilter;

.field private static mRecCancelPandingIntent:Landroid/app/PendingIntent;

.field private static mRecDirectCancelPandingIntent:Landroid/app/PendingIntent;

.field private static mRecNewPandingIntent:Landroid/app/PendingIntent;

.field private static mRecPausePandingIntent:Landroid/app/PendingIntent;

.field private static mRecResumePandingIntent:Landroid/app/PendingIntent;

.field private static mRecSavePandingIntent:Landroid/app/PendingIntent;

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

.field private static sContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sContext:Landroid/content/Context;

    .line 39
    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 41
    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLastAction:Ljava/lang/String;

    .line 43
    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLockscreenIntentFilter:Landroid/content/IntentFilter;

    .line 45
    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecPausePandingIntent:Landroid/app/PendingIntent;

    .line 46
    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecResumePandingIntent:Landroid/app/PendingIntent;

    .line 47
    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecSavePandingIntent:Landroid/app/PendingIntent;

    .line 48
    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecCancelPandingIntent:Landroid/app/PendingIntent;

    .line 49
    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecNewPandingIntent:Landroid/app/PendingIntent;

    .line 51
    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecDirectCancelPandingIntent:Landroid/app/PendingIntent;

    .line 53
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->isSetContextualEvent:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method public static buildUpdate(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 14
    .param p0, "time"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x1

    const/4 v13, 0x0

    const/high16 v12, 0x40000000    # 2.0f

    .line 358
    sget-object v9, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f09003f

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 360
    .local v7, "textsize":I
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 362
    .local v0, "boundsText":Landroid/graphics/Rect;
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 363
    .local v5, "paint":Landroid/graphics/Paint;
    const-string v6, "/system/fonts/SamsungSans-Num3T.ttf"

    .line 364
    .local v6, "path":Ljava/lang/String;
    invoke-static {v6}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 366
    .local v1, "clock":Landroid/graphics/Typeface;
    invoke-virtual {v5, v11}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 367
    invoke-virtual {v5, v11}, Landroid/graphics/Paint;->setSubpixelText(Z)V

    .line 368
    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 369
    sget-object v9, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 370
    const/4 v9, -0x1

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 371
    int-to-float v9, v7

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 372
    sget-object v9, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 373
    const/4 v9, 0x0

    sget-object v10, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f080006

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v5, v12, v9, v12, v10}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 374
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v10, 0x5

    if-le v9, v10, :cond_0

    .line 375
    const-string v9, "00:00:00"

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v5, v9, v13, v10, v0}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 379
    :goto_0
    iget v9, v0, Landroid/graphics/Rect;->right:I

    iget v10, v0, Landroid/graphics/Rect;->left:I

    sub-int v8, v9, v10

    .line 380
    .local v8, "width":I
    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    iget v10, v0, Landroid/graphics/Rect;->top:I

    sub-int v2, v9, v10

    .line 382
    .local v2, "height":I
    add-int/lit8 v9, v2, 0x5

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 383
    .local v3, "myBitmap":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 384
    .local v4, "myCanvas":Landroid/graphics/Canvas;
    div-int/lit8 v9, v8, 0x2

    int-to-float v9, v9

    int-to-float v10, v2

    invoke-virtual {v4, p0, v9, v10, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 385
    return-object v3

    .line 377
    .end local v2    # "height":I
    .end local v3    # "myBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "myCanvas":Landroid/graphics/Canvas;
    .end local v8    # "width":I
    :cond_0
    const-string v9, "00:00"

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v5, v9, v13, v10, v0}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    goto :goto_0
.end method

.method private static changeDurationToTimeText(J)Ljava/lang/String;
    .locals 12
    .param p0, "time"    # J

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 299
    const-wide/16 v6, 0x3e8

    div-long v6, p0, v6

    long-to-int v4, v6

    .line 300
    .local v4, "second":I
    div-int/lit16 v0, v4, 0xe10

    .line 301
    .local v0, "h":I
    div-int/lit8 v5, v4, 0x3c

    rem-int/lit8 v1, v5, 0x3c

    .line 302
    .local v1, "m":I
    rem-int/lit8 v3, v4, 0x3c

    .line 303
    .local v3, "s":I
    if-lez v0, :cond_0

    .line 304
    const-string v5, "%02d:%02d:%02d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 308
    .local v2, "retString":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 306
    .end local v2    # "retString":Ljava/lang/String;
    :cond_0
    const-string v5, "%02d:%02d"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "retString":Ljava/lang/String;
    goto :goto_0
.end method

.method private checkService(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    .line 124
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    .line 125
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLastAction:Ljava/lang/String;

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    sput-object p2, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLastAction:Ljava/lang/String;

    .line 130
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private static getMediaRecorderState()I
    .locals 1

    .prologue
    .line 282
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_0

    .line 283
    const/16 v0, 0x3e8

    .line 285
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v0

    goto :goto_0
.end method

.method private static getRecTime()J
    .locals 2

    .prologue
    .line 290
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v0, :cond_0

    .line 291
    const-wide/16 v0, 0x0

    .line 293
    :goto_0
    return-wide v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecDurationinLockscreen()J

    move-result-wide v0

    goto :goto_0
.end method

.method private static init(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const v3, 0x7010002

    const/4 v2, 0x0

    .line 57
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLockscreenIntentFilter:Landroid/content/IntentFilter;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLockscreenIntentFilter:Landroid/content/IntentFilter;

    .line 59
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLockscreenIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.app.voicenote.rec_pause"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 60
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLockscreenIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.app.voicenote.rec_reume"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 61
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLockscreenIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.app.voicenote.rec_save"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 62
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLockscreenIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.app.voicenote.rec_cancel"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 63
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLockscreenIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.app.voicenote.rec_new"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 66
    :cond_0
    if-eqz p0, :cond_1

    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 67
    sput-object p0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sContext:Landroid/content/Context;

    .line 69
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.voicenote.rec_pause"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v3, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecPausePandingIntent:Landroid/app/PendingIntent;

    .line 72
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.voicenote.rec_reume"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v3, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecResumePandingIntent:Landroid/app/PendingIntent;

    .line 75
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.voicenote.rec_save_keygard"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v3, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecSavePandingIntent:Landroid/app/PendingIntent;

    .line 78
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v3, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecCancelPandingIntent:Landroid/app/PendingIntent;

    .line 81
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.voicenote.rec_new"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v3, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecNewPandingIntent:Landroid/app/PendingIntent;

    .line 85
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.voicenote.rec_cancel.keygard"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v3, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecDirectCancelPandingIntent:Landroid/app/PendingIntent;

    .line 89
    :cond_1
    return-void
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 1
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 134
    if-eqz p0, :cond_0

    .line 135
    sput-object p0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 136
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->retryLastAction(Landroid/content/Context;)V

    .line 138
    :cond_0
    return-void
.end method

.method public static refreashWidget()V
    .locals 10

    .prologue
    .line 153
    sget-object v7, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sContext:Landroid/content/Context;

    if-nez v7, :cond_0

    .line 154
    const-string v7, "VNWidgetProvider"

    const-string v8, "refreshWidget() :: mContext is null."

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    .local v4, "mAppWidgetIds":[I
    .local v5, "mAppWidgetManager":Landroid/appwidget/AppWidgetManager;
    :goto_0
    return-void

    .line 158
    .end local v4    # "mAppWidgetIds":[I
    .end local v5    # "mAppWidgetManager":Landroid/appwidget/AppWidgetManager;
    :cond_0
    const/4 v4, 0x0

    .line 159
    .restart local v4    # "mAppWidgetIds":[I
    sget-object v7, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v5

    .line 161
    .restart local v5    # "mAppWidgetManager":Landroid/appwidget/AppWidgetManager;
    if-eqz v5, :cond_3

    .line 162
    new-instance v7, Landroid/content/ComponentName;

    sget-object v8, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sContext:Landroid/content/Context;

    const-class v9, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;

    invoke-direct {v7, v8, v9}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v7}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v4

    .line 164
    if-nez v4, :cond_1

    .line 165
    const-string v7, "VNWidgetProvider"

    const-string v8, "mAppWidgetIds is null"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :cond_1
    :goto_1
    if-eqz v5, :cond_4

    if-eqz v4, :cond_4

    array-length v7, v4

    if-lez v7, :cond_4

    .line 173
    move-object v0, v4

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_2
    if-ge v1, v3, :cond_5

    aget v2, v0, v1

    .line 174
    .local v2, "id":I
    new-instance v6, Landroid/widget/RemoteViews;

    sget-object v7, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f030032

    invoke-direct {v6, v7, v8}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 175
    .local v6, "rv":Landroid/widget/RemoteViews;
    if-eqz v6, :cond_2

    .line 176
    sget-object v7, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sContext:Landroid/content/Context;

    const/4 v8, 0x0

    invoke-static {v7, v6, v8}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->updateWidget(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    .line 177
    invoke-virtual {v5, v2, v6}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 173
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 168
    .end local v0    # "arr$":[I
    .end local v1    # "i$":I
    .end local v2    # "id":I
    .end local v3    # "len$":I
    .end local v6    # "rv":Landroid/widget/RemoteViews;
    :cond_3
    const-string v7, "VNWidgetProvider"

    const-string v8, "mAppWidgetManager is null"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 181
    :cond_4
    const-string v7, "VNWidgetProvider"

    const-string v8, "refreshWidget :: Ignored updating"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_5
    sget-object v7, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->updateContextualEventWidget(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static releaseService()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 141
    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 142
    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLastAction:Ljava/lang/String;

    .line 143
    return-void
.end method

.method public static removeContextualEvent(Landroid/app/KeyguardManager;)V
    .locals 3
    .param p0, "km"    # Landroid/app/KeyguardManager;

    .prologue
    .line 347
    const/4 v1, 0x0

    :try_start_0
    sput-boolean v1, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->isSetContextualEvent:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    .line 354
    :goto_0
    return-void

    .line 349
    :catch_0
    move-exception v0

    .line 350
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "VNWidgetProvider"

    const-string v2, "removeContextualEvent() exception"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 351
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 352
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v1, "VNWidgetProvider"

    const-string v2, "removeContextualEvent() NoSuchMethodError"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static retryLastAction(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 146
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLastAction:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 147
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLastAction:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 148
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLastAction:Ljava/lang/String;

    .line 150
    :cond_0
    return-void
.end method

.method public static sendUpdateAppWidget(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 312
    const-string v1, "keyguard"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 313
    .local v0, "km":Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 314
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.voicenote.appwidget.update"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.sec.android.app.voicenote.appwidget.permission"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 318
    :goto_0
    return-void

    .line 316
    :cond_1
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->isSetContextualEvent:Z

    goto :goto_0
.end method

.method public static sendUpdateAppWidgetOnBtnClick(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 322
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->init(Landroid/content/Context;)V

    .line 323
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->refreashWidget()V

    .line 324
    return-void
.end method

.method public static setContextualEvent(Landroid/app/KeyguardManager;Landroid/widget/RemoteViews;)V
    .locals 0
    .param p0, "km"    # Landroid/app/KeyguardManager;
    .param p1, "views"    # Landroid/widget/RemoteViews;

    .prologue
    .line 339
    .line 340
    return-void
.end method

.method private static setPendingIntent(Landroid/widget/RemoteViews;ZZ)V
    .locals 3
    .param p0, "remoteViews"    # Landroid/widget/RemoteViews;
    .param p1, "aleadyRec"    # Z
    .param p2, "isContextualEvent"    # Z

    .prologue
    const v2, 0x7f0e007d

    const v1, 0x7f0e007a

    .line 234
    if-eqz p2, :cond_0

    .line 235
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecDirectCancelPandingIntent:Landroid/app/PendingIntent;

    invoke-virtual {p0, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 241
    :goto_0
    const v0, 0x7f0e0079

    sget-object v1, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecSavePandingIntent:Landroid/app/PendingIntent;

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 242
    const v0, 0x7f0e007c

    sget-object v1, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecPausePandingIntent:Landroid/app/PendingIntent;

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 244
    if-eqz p1, :cond_1

    .line 245
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecResumePandingIntent:Landroid/app/PendingIntent;

    invoke-virtual {p0, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 250
    :goto_1
    return-void

    .line 238
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecCancelPandingIntent:Landroid/app/PendingIntent;

    invoke-virtual {p0, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_0

    .line 248
    :cond_1
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mRecNewPandingIntent:Landroid/app/PendingIntent;

    invoke-virtual {p0, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_1
.end method

.method private static setRecPauseViewVisibility(Landroid/widget/RemoteViews;)V
    .locals 3
    .param p0, "remoteViews"    # Landroid/widget/RemoteViews;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 263
    const v0, 0x7f0e007a

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 264
    const v0, 0x7f0e0079

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 265
    const v0, 0x7f0e007d

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 267
    const v0, 0x7f0e0077

    invoke-virtual {p0, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 268
    const v0, 0x7f0e007c

    invoke-virtual {p0, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 269
    return-void
.end method

.method private static setRecTime(Landroid/widget/RemoteViews;I)V
    .locals 4
    .param p0, "remoteViews"    # Landroid/widget/RemoteViews;
    .param p1, "state"    # I

    .prologue
    .line 229
    const v0, 0x7f0e0075

    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->getRecTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->changeDurationToTimeText(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->buildUpdate(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 230
    return-void
.end method

.method private static setRecordingViewVisibility(Landroid/widget/RemoteViews;)V
    .locals 2
    .param p0, "remoteViews"    # Landroid/widget/RemoteViews;

    .prologue
    const/4 v1, 0x0

    .line 254
    const v0, 0x7f0e0077

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 255
    const v0, 0x7f0e007a

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 256
    const v0, 0x7f0e0079

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 257
    const v0, 0x7f0e007c

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 259
    const v0, 0x7f0e007d

    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 260
    return-void
.end method

.method private static setStandbyViewVisibility(Landroid/widget/RemoteViews;)V
    .locals 3
    .param p0, "remoteViews"    # Landroid/widget/RemoteViews;

    .prologue
    const/16 v2, 0x8

    .line 272
    const v0, 0x7f0e007d

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 274
    const v0, 0x7f0e0077

    invoke-virtual {p0, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 275
    const v0, 0x7f0e007a

    invoke-virtual {p0, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 276
    const v0, 0x7f0e0079

    invoke-virtual {p0, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 277
    const v0, 0x7f0e007c

    invoke-virtual {p0, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 278
    return-void
.end method

.method private static updateContextualEventWidget(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 188
    if-nez p0, :cond_0

    .line 203
    :goto_0
    return-void

    .line 192
    :cond_0
    const-string v3, "keyguard"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 194
    .local v0, "km":Landroid/app/KeyguardManager;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->getMediaRecorderState()I

    move-result v1

    .line 195
    .local v1, "mediaRecorderState":I
    const/16 v3, 0x3eb

    if-eq v1, v3, :cond_1

    const/16 v3, 0x3ec

    if-ne v1, v3, :cond_2

    .line 197
    :cond_1
    new-instance v2, Landroid/widget/RemoteViews;

    sget-object v3, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f030032

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 198
    .local v2, "rv":Landroid/widget/RemoteViews;
    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->updateWidget(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    .line 199
    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->setContextualEvent(Landroid/app/KeyguardManager;Landroid/widget/RemoteViews;)V

    goto :goto_0

    .line 201
    .end local v2    # "rv":Landroid/widget/RemoteViews;
    :cond_2
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->removeContextualEvent(Landroid/app/KeyguardManager;)V

    goto :goto_0
.end method

.method private static updateWidget(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "remoteViews"    # Landroid/widget/RemoteViews;
    .param p2, "isContextualEvent"    # Z

    .prologue
    const/4 v1, 0x1

    .line 208
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->getMediaRecorderState()I

    move-result v0

    .line 209
    .local v0, "mediaRecorderState":I
    packed-switch v0, :pswitch_data_0

    .line 219
    invoke-static {p1}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->setStandbyViewVisibility(Landroid/widget/RemoteViews;)V

    .line 220
    const/4 v1, 0x0

    invoke-static {p1, v1, p2}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->setPendingIntent(Landroid/widget/RemoteViews;ZZ)V

    .line 223
    :goto_0
    invoke-static {p1, v0}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->setRecTime(Landroid/widget/RemoteViews;I)V

    .line 224
    const v1, 0x7f0e0075

    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->getRecTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->changeDurationToTimeText(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->stringForReadTime(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 226
    return-void

    .line 211
    :pswitch_0
    invoke-static {p1, v1, p2}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->setPendingIntent(Landroid/widget/RemoteViews;ZZ)V

    .line 212
    invoke-static {p1}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->setRecordingViewVisibility(Landroid/widget/RemoteViews;)V

    goto :goto_0

    .line 215
    :pswitch_1
    invoke-static {p1}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->setRecPauseViewVisibility(Landroid/widget/RemoteViews;)V

    .line 216
    invoke-static {p1, v1, p2}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->setPendingIntent(Landroid/widget/RemoteViews;ZZ)V

    goto :goto_0

    .line 209
    nop

    :pswitch_data_0
    .packed-switch 0x3eb
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 93
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 95
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "action":Ljava/lang/String;
    const-string v4, "VNWidgetProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onReceive() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v4, "com.sec.android.app.voicenote.ticker.request_state"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 98
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->getMediaRecorderState()I

    move-result v3

    .line 99
    .local v3, "recorder_state":I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->getRecTime()J

    move-result-wide v4

    long-to-int v2, v4

    .line 100
    .local v2, "rec_time":I
    const/4 v4, 0x0

    invoke-static {p1, v3, v2, v4}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendState(Landroid/content/Context;III)V

    .line 112
    .end local v2    # "rec_time":I
    .end local v3    # "recorder_state":I
    :goto_0
    return-void

    .line 102
    :cond_0
    const-string v4, "com.sec.android.app.voicenote.rec_new"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 103
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->checkService(Landroid/content/Context;Ljava/lang/String;)V

    .line 107
    :goto_1
    const-string v4, "keyguard"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    .line 109
    .local v1, "km":Landroid/app/KeyguardManager;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->init(Landroid/content/Context;)V

    .line 110
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->refreashWidget()V

    goto :goto_0

    .line 105
    .end local v1    # "km":Landroid/app/KeyguardManager;
    :cond_1
    const/4 v4, 0x0

    invoke-direct {p0, p1, v4}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->checkService(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 116
    const-string v0, "VNWidgetProvider"

    const-string v1, "onUpdate()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 118
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->mLastAction:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->checkService(Landroid/content/Context;Ljava/lang/String;)V

    .line 119
    invoke-static {p1}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->init(Landroid/content/Context;)V

    .line 120
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->refreashWidget()V

    .line 121
    return-void
.end method
