.class public Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;
.super Ljava/lang/Object;
.source "VNAudioFormat.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VNAudioFormat"


# instance fields
.field private mFileSize:J

.field private mMimeType:Ljava/lang/String;

.field private mQuality:I

.field private mRecordMode:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v1, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mRecordMode:I

    .line 27
    const-string v0, "audio/mp4"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    .line 28
    iput v1, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mQuality:I

    .line 29
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mFileSize:J

    .line 33
    const-string v0, "record_mode"

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mRecordMode:I

    .line 34
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mRecordMode:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->initAudioFormat(I)V

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 6
    .param p1, "mimetype"    # Ljava/lang/String;
    .param p2, "sizeMMS"    # J

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v1, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mRecordMode:I

    .line 27
    const-string v0, "audio/mp4"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    .line 28
    iput v1, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mQuality:I

    .line 29
    iput-wide v4, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mFileSize:J

    .line 38
    const-string v0, "record_mode"

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mRecordMode:I

    .line 40
    if-eqz p1, :cond_0

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    .line 42
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mRecordMode:I

    .line 44
    :cond_0
    cmp-long v0, p2, v4

    if-eqz v0, :cond_1

    .line 45
    iput-wide p2, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mFileSize:J

    .line 46
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mRecordMode:I

    .line 48
    :cond_1
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mRecordMode:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->initAudioFormat(I)V

    .line 49
    return-void
.end method

.method private initAudioFormat(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    const/4 v1, 0x2

    .line 52
    const-string v0, "recording_quality"

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mQuality:I

    .line 54
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mRecordMode:I

    packed-switch v0, :pswitch_data_0

    .line 80
    :cond_0
    :goto_0
    const-string v0, "VNAudioFormat"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initAudioFormat recordmode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mRecordMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v0, "VNAudioFormat"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initAudioFormat quality:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mQuality:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v0, "VNAudioFormat"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initAudioFormat mimetype:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    return-void

    .line 56
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mQuality:I

    if-ne v0, v1, :cond_0

    .line 57
    const-string v0, "audio/amr"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    goto :goto_0

    .line 62
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mQuality:I

    if-ne v0, v1, :cond_0

    .line 63
    const-string v0, "audio/amr"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    goto :goto_0

    .line 68
    :pswitch_2
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mQuality:I

    if-ne v0, v1, :cond_0

    .line 69
    const-string v0, "audio/amr"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    goto :goto_0

    .line 74
    :pswitch_3
    const-string v0, "audio/mp4"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    goto :goto_0

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getAudioEncoder()I
    .locals 2

    .prologue
    .line 106
    const-string v0, "audio/amr"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    const/4 v0, 0x1

    .line 109
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public getAudioEncodingBitrate()I
    .locals 2

    .prologue
    .line 94
    const-string v0, "audio/amr"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    const/16 v0, 0x2fa8

    .line 97
    :goto_0
    return v0

    :cond_0
    const v0, 0x1f400

    goto :goto_0
.end method

.method public getAudioSamplingRate()I
    .locals 4

    .prologue
    const v1, 0xac44

    const/16 v0, 0x1f40

    .line 114
    const-string v2, "audio/amr"

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 128
    :goto_0
    :pswitch_0
    return v0

    .line 117
    :cond_0
    iget v2, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mRecordMode:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_1

    iget v2, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mRecordMode:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    :cond_1
    move v0, v1

    .line 118
    goto :goto_0

    .line 120
    :cond_2
    iget v2, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mQuality:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 128
    goto :goto_0

    .line 122
    :pswitch_1
    const/16 v0, 0x3e80

    goto :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getExtension()Ljava/lang/String;
    .locals 2

    .prologue
    .line 135
    const-string v0, "audio/amr"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    const-string v0, ".amr"

    .line 138
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ".m4a"

    goto :goto_0
.end method

.method public getFilesize()J
    .locals 2

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mFileSize:J

    return-wide v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getMinRecordSize()I
    .locals 2

    .prologue
    .line 155
    const-string v0, "audio/amr"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const/16 v0, 0x400

    .line 158
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x2800

    goto :goto_0
.end method

.method public getOutputFormat()I
    .locals 2

    .prologue
    .line 86
    const-string v0, "audio/amr"

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const/4 v0, 0x3

    .line 89
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getRecordMode()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mRecordMode:I

    return v0
.end method

.method public isAttachMode()Z
    .locals 2

    .prologue
    .line 147
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->mRecordMode:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
