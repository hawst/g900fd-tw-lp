.class Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;
.super Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;
.source "VNLibraryExpandableListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionModeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;I)V
    .locals 3
    .param p2, "viewPosition"    # I

    .prologue
    const/4 v2, 0x1

    .line 1383
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 1384
    invoke-virtual {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;-><init>(Landroid/content/Context;I)V

    .line 1385
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v0

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getCheckBoxSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setCheckBoxSize(I)V

    .line 1386
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setSelectionMode(ZZ)V

    .line 1387
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setCheckBoxSize(I)V

    .line 1388
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;II)V
    .locals 3
    .param p2, "viewPosition"    # I
    .param p3, "mode"    # I

    .prologue
    const/4 v2, 0x1

    .line 1390
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    .line 1391
    invoke-virtual {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;-><init>(Landroid/content/Context;I)V

    .line 1392
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v0

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getCheckBoxSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setCheckBoxSize(I)V

    .line 1393
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setSelectionMode(ZZ)V

    .line 1394
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setCheckBoxSize(I)V

    .line 1396
    iput p3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->mMenuMode:I

    .line 1398
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1399
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 1401
    :cond_0
    return-void
.end method


# virtual methods
.method protected finishActionMode()V
    .locals 3

    .prologue
    .line 1733
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    .line 1734
    .local v0, "actionMode":Landroid/view/ActionMode;
    if-eqz v0, :cond_0

    .line 1735
    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 1737
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->updateState(Z)V

    .line 1738
    return-void
.end method

.method protected getItemCount()I
    .locals 1

    .prologue
    .line 1752
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->isCursorValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1753
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroupCount()I

    move-result v0

    .line 1756
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1761
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->isCursorValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1762
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getGroupId(I)J

    move-result-wide v0

    .line 1765
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getList()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 1747
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method protected getParentFragment()Landroid/app/Fragment;
    .locals 1

    .prologue
    .line 1742
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    return-object v0
.end method

.method public getTypeOfSelectedItems(Landroid/content/Context;)I
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1782
    const/4 v12, -0x1

    .line 1783
    .local v12, "typeOfSelectItem":I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMode()Z

    move-result v8

    .line 1784
    .local v8, "isPersonalPageMode":Z
    const/4 v9, 0x0

    .line 1785
    .local v9, "isPersonalPageMounted":Z
    const/4 v10, 0x0

    .line 1787
    .local v10, "personalPageRoot":Ljava/lang/String;
    if-eqz p1, :cond_7

    if-eqz v8, :cond_7

    .line 1788
    invoke-static {p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageMounted(Landroid/content/Context;)Z

    move-result v9

    .line 1789
    invoke-static {p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 1791
    if-eqz v9, :cond_2

    if-eqz v10, :cond_2

    .line 1792
    iget-object v13, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    iget-object v13, v13, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v13}, Landroid/widget/ExpandableListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    .line 1793
    .local v0, "arr":Landroid/util/SparseBooleanArray;
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v11

    .line 1794
    .local v11, "size":I
    const-wide/16 v6, 0x0

    .line 1795
    .local v6, "id":J
    const/4 v5, 0x0

    .line 1796
    .local v5, "isChecked":Z
    const/4 v1, 0x0

    .line 1797
    .local v1, "filePath":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1798
    .local v2, "hasNormal":Z
    const/4 v3, 0x0

    .line 1799
    .local v3, "hasPersonal":Z
    if-lez v11, :cond_2

    .line 1800
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v11, :cond_1

    .line 1801
    invoke-virtual {v0, v4}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v13

    invoke-virtual {p0, v13}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->getItemId(I)J

    move-result-wide v6

    .line 1802
    invoke-virtual {v0, v4}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v5

    .line 1803
    invoke-static {p1, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 1804
    if-eqz v1, :cond_0

    if-eqz v5, :cond_0

    .line 1805
    invoke-virtual {v1, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 1806
    const/4 v3, 0x1

    .line 1811
    :cond_0
    :goto_1
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    .line 1816
    :cond_1
    if-eqz v2, :cond_5

    if-eqz v3, :cond_5

    .line 1817
    const/4 v12, 0x2

    .line 1830
    .end local v0    # "arr":Landroid/util/SparseBooleanArray;
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "hasNormal":Z
    .end local v3    # "hasPersonal":Z
    .end local v4    # "i":I
    .end local v5    # "isChecked":Z
    .end local v6    # "id":J
    .end local v11    # "size":I
    :cond_2
    :goto_2
    return v12

    .line 1808
    .restart local v0    # "arr":Landroid/util/SparseBooleanArray;
    .restart local v1    # "filePath":Ljava/lang/String;
    .restart local v2    # "hasNormal":Z
    .restart local v3    # "hasPersonal":Z
    .restart local v4    # "i":I
    .restart local v5    # "isChecked":Z
    .restart local v6    # "id":J
    .restart local v11    # "size":I
    :cond_3
    const/4 v2, 0x1

    goto :goto_1

    .line 1800
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1819
    :cond_5
    if-eqz v3, :cond_6

    .line 1820
    const/4 v12, 0x1

    goto :goto_2

    .line 1822
    :cond_6
    const/4 v12, 0x0

    goto :goto_2

    .line 1827
    .end local v0    # "arr":Landroid/util/SparseBooleanArray;
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "hasNormal":Z
    .end local v3    # "hasPersonal":Z
    .end local v4    # "i":I
    .end local v5    # "isChecked":Z
    .end local v6    # "id":J
    .end local v11    # "size":I
    :cond_7
    invoke-static {p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isPersonalPageNormal(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 1828
    const/4 v12, 0x3

    goto :goto_2
.end method

.method protected isCursorValid()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1770
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1778
    :cond_0
    :goto_0
    return v1

    .line 1773
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 1774
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1778
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 22
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1625
    const-string v18, "VNLibraryActionModeListener"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "onActionItemClicked E : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1627
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->getParentFragment()Landroid/app/Fragment;

    move-result-object v9

    .line 1631
    .local v9, "fragment":Landroid/app/Fragment;
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Landroid/app/Fragment;->isResumed()Z

    move-result v18

    if-nez v18, :cond_1

    .line 1632
    :cond_0
    const-string v18, "VNLibraryActionModeListener"

    const-string v19, "onActionItemClicked() : activity is not resumed"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1633
    const/16 v18, 0x0

    .line 1710
    :goto_0
    return v18

    .line 1636
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v16

    .line 1637
    .local v16, "listView":Landroid/widget/ListView;
    if-nez v16, :cond_2

    .line 1638
    const-string v18, "VNLibraryActionModeListener"

    const-string v19, "onActionItemClicked() : list view is null"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1639
    const/16 v18, 0x0

    goto :goto_0

    .line 1642
    :cond_2
    invoke-virtual/range {v16 .. v16}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v4

    .line 1643
    .local v4, "arr":Landroid/util/SparseBooleanArray;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1645
    .local v5, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {v4}, Landroid/util/SparseBooleanArray;->size()I

    move-result v15

    .line 1648
    .local v15, "len":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    if-ge v11, v15, :cond_4

    .line 1649
    :try_start_0
    invoke-virtual {v4, v11}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 1650
    invoke-virtual {v4, v11}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->getItemId(I)J

    move-result-wide v12

    .line 1651
    .local v12, "id":J
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1648
    .end local v12    # "id":J
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 1655
    :cond_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_5

    .line 1656
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v19, v0

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Long;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->checkedItemId:J
    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$702(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;J)J
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1661
    :cond_5
    :goto_2
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v14

    .line 1662
    .local v14, "itemId":I
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/ActionMode;->setTag(Ljava/lang/Object;)V

    .line 1664
    packed-switch v14, :pswitch_data_0

    .line 1710
    :goto_3
    :pswitch_0
    invoke-super/range {p0 .. p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result v18

    goto :goto_0

    .line 1666
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->checkedItemId:J
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)J

    move-result-wide v18

    const-wide/16 v20, -0x1

    cmp-long v18, v18, v20

    if-nez v18, :cond_6

    .line 1667
    invoke-super/range {p0 .. p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result v18

    goto/16 :goto_0

    .line 1669
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 1670
    .local v7, "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->checkedItemId:J
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-static {v7, v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v19

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCurrntPath:Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$802(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1671
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1672
    .local v6, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->checkedItemId:J
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1673
    invoke-static {v7, v6}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasTagData(Landroid/content/Context;Ljava/util/ArrayList;)Z

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 1674
    .local v10, "hasNFC":Ljava/lang/Boolean;
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v18

    if-eqz v18, :cond_7

    .line 1675
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isPressEditBtn:Z
    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$902(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;Z)Z

    .line 1676
    const v18, 0x7f0b0105

    const v19, 0x7f0b013d

    const/16 v20, 0x1

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->setArguments(III)Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    move-result-object v8

    .local v8, "dialog":Landroid/app/DialogFragment;
    move-object/from16 v18, v8

    .line 1678
    check-cast v18, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/voicenote/common/fragment/VNAlertDialogFragment;->registerCallback(Landroid/app/Fragment;)V

    .line 1679
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v18

    const-string v19, "FRAGMENT_DIALOG"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1681
    .end local v8    # "dialog":Landroid/app/DialogFragment;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCurrntPath:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$800(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->checkedItemId:J
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)J

    move-result-wide v20

    invoke-virtual/range {v18 .. v21}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->showEditDialog(Ljava/lang/String;J)V

    goto/16 :goto_3

    .line 1687
    .end local v6    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v7    # "context":Landroid/content/Context;
    .end local v10    # "hasNFC":Ljava/lang/Boolean;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->checkedItemId:J
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)J

    move-result-wide v18

    const-wide/16 v20, -0x1

    cmp-long v18, v18, v20

    if-nez v18, :cond_8

    .line 1688
    invoke-super/range {p0 .. p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result v18

    goto/16 :goto_0

    .line 1690
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->checkedItemId:J
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->setArguments(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;

    move-result-object v8

    .line 1691
    .restart local v8    # "dialog":Landroid/app/DialogFragment;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v18

    const-string v19, "FRAGMENT_DIALOG_SETAS"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1695
    .end local v8    # "dialog":Landroid/app/DialogFragment;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->checkedItemId:J
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)J

    move-result-wide v18

    const-wide/16 v20, -0x1

    cmp-long v18, v18, v20

    if-nez v18, :cond_9

    .line 1696
    invoke-super/range {p0 .. p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result v18

    goto/16 :goto_0

    .line 1698
    :cond_9
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1699
    .local v17, "title":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->checkedItemId:J
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$700(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)J

    move-result-wide v18

    const v20, 0x7f0b006d

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;->setArguments(JI)Lcom/sec/android/app/voicenote/library/fragment/VNDetailDialogFragment;

    move-result-object v8

    .line 1700
    .restart local v8    # "dialog":Landroid/app/DialogFragment;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v18

    const-string v19, "FRAGMENT_DIALOG"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1704
    .end local v8    # "dialog":Landroid/app/DialogFragment;
    .end local v17    # "title":Ljava/lang/String;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v18

    const-string v19, "CHAN"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1658
    .end local v14    # "itemId":I
    :catch_0
    move-exception v18

    goto/16 :goto_2

    .line 1664
    :pswitch_data_0
    .packed-switch 0x7f0e00fa
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 9
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const v8, 0x7f0e00fd

    const v7, 0x7f0e00ff

    const v6, 0x7f0e0100

    .line 1427
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    const/4 v5, 0x1

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mNothingCheckedItem:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$602(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;Z)Z

    .line 1428
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 1429
    .local v2, "inflater":Landroid/view/MenuInflater;
    const v4, 0x7f0d0003

    invoke-virtual {v2, v4, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1431
    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->mMenuMode:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 1432
    const v4, 0x7f0e00f8

    invoke-interface {p2, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 1433
    const v4, 0x7f0e00f9

    invoke-interface {p2, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 1434
    const v4, 0x7f0e00fa

    invoke-interface {p2, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 1435
    const v4, 0x7f0e00fb

    invoke-interface {p2, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 1436
    const v4, 0x7f0e00fc

    invoke-interface {p2, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 1437
    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1438
    const v4, 0x7f0e00fe

    invoke-interface {p2, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 1441
    :cond_0
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    if-eqz v4, :cond_7

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isKNOXAvailable()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1442
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXVersion()Ljava/lang/String;

    move-result-object v3

    .line 1443
    .local v3, "version":Ljava/lang/String;
    const-string v4, "2.0"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1444
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXCallingUserId()I

    move-result v1

    .line 1445
    .local v1, "callingUserId":I
    if-gez v1, :cond_4

    .line 1446
    invoke-interface {p2, v6}, Landroid/view/Menu;->removeItem(I)V

    .line 1447
    invoke-interface {p2, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 1463
    .end local v1    # "callingUserId":I
    .end local v3    # "version":Ljava/lang/String;
    :goto_0
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCurrentPlayState()I

    move-result v4

    const/16 v5, 0x15

    if-eq v4, v5, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1465
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$300()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onDoStop()V

    .line 1468
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1469
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_2

    .line 1470
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 1472
    :cond_2
    sget-boolean v4, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_WRITETONFCTAG:Z

    if-nez v4, :cond_3

    .line 1474
    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1476
    :cond_3
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v4

    return v4

    .line 1448
    .end local v0    # "activity":Landroid/app/Activity;
    .restart local v1    # "callingUserId":I
    .restart local v3    # "version":Ljava/lang/String;
    :cond_4
    if-nez v1, :cond_5

    .line 1450
    invoke-interface {p2, v6}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0

    .line 1453
    :cond_5
    invoke-interface {p2, v7}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0

    .line 1456
    .end local v1    # "callingUserId":I
    :cond_6
    invoke-interface {p2, v6}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0

    .line 1459
    .end local v3    # "version":Ljava/lang/String;
    :cond_7
    invoke-interface {p2, v7}, Landroid/view/Menu;->removeItem(I)V

    .line 1460
    invoke-interface {p2, v6}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 3
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    const/4 v2, 0x0

    .line 1406
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->isCursorValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1407
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getCheckBoxSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setCheckBoxSize(I)V

    .line 1408
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setSelectionMode(ZZ)V

    .line 1409
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListAdapter:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$500(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->setCheckBoxSize(I)V

    .line 1410
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isRecordedCalls()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1411
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->getList()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 1414
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 1415
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getCallbacks()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$200(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1416
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$300()Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->setActionMode(Landroid/view/ActionMode;)V

    .line 1418
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mArray:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_2

    .line 1419
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mArray:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 1421
    :cond_2
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 10
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1481
    const/4 v6, -0x1

    .line 1482
    .local v6, "typeOfSelectItem":I
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    iget-object v8, v8, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v8}, Landroid/widget/ExpandableListView;->getCheckedItemCount()I

    move-result v2

    .line 1483
    .local v2, "checkedItemCount":I
    if-nez v2, :cond_7

    .line 1484
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    const/4 v9, 0x1

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mNothingCheckedItem:Z
    invoke-static {v8, v9}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$602(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;Z)Z

    .line 1488
    :goto_0
    invoke-interface {p2}, Landroid/view/Menu;->clear()V

    .line 1489
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v5

    .line 1490
    .local v5, "inflater":Landroid/view/MenuInflater;
    const v8, 0x7f0d0003

    invoke-virtual {v5, v8, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1491
    const v8, 0x7f0e00f9

    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 1493
    .local v4, "del":Landroid/view/MenuItem;
    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->mMenuMode:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_0

    .line 1494
    const v8, 0x7f0e00f8

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1495
    const v8, 0x7f0e00f9

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1496
    const v8, 0x7f0e00fa

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1497
    const v8, 0x7f0e00fb

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1498
    const v8, 0x7f0e00fc

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1499
    const v8, 0x7f0e00fd

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1500
    const v8, 0x7f0e00fe

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1501
    const v8, 0x7f0e0101

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1502
    const v8, 0x7f0e0102

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1505
    :cond_0
    iget v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->mMenuMode:I

    const/4 v9, 0x3

    if-ne v8, v9, :cond_1

    .line 1506
    const/4 v8, 0x0

    invoke-interface {v4, v8}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1507
    const v8, 0x7f0b0070

    invoke-interface {v4, v8}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 1508
    const v8, 0x7f0e00f8

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1509
    const v8, 0x7f0e00fa

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1510
    const v8, 0x7f0e00fb

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1511
    const v8, 0x7f0e00fc

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1512
    const v8, 0x7f0e00fd

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1513
    const v8, 0x7f0e00fe

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1514
    const v8, 0x7f0e0101

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1515
    const v8, 0x7f0e0102

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1518
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mNothingCheckedItem:Z
    invoke-static {v8}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$600(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1519
    const v8, 0x7f0e00f8

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1520
    const v8, 0x7f0e00f9

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1521
    const v8, 0x7f0e00fa

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1522
    const v8, 0x7f0e00fb

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1523
    const v8, 0x7f0e00fc

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1524
    const v8, 0x7f0e00fd

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1525
    const v8, 0x7f0e00fe

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1526
    const v8, 0x7f0e00ff

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1527
    const v8, 0x7f0e0100

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1528
    const v8, 0x7f0e0101

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1529
    const v8, 0x7f0e0102

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1580
    :cond_2
    :goto_1
    const/4 v8, 0x1

    if-le v2, v8, :cond_d

    .line 1581
    const v8, 0x7f0e00fb

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1582
    const v8, 0x7f0e00fc

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1583
    const v8, 0x7f0e00fd

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1584
    const v8, 0x7f0e00fe

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1603
    :cond_3
    :goto_2
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1604
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mNothingCheckedItem:Z
    invoke-static {v8}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$600(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1605
    const v8, 0x7f0e00f8

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1606
    const v8, 0x7f0e00f7

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1608
    :cond_4
    const v8, 0x7f0e00fa

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1609
    const v8, 0x7f0e00fb

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1610
    const v8, 0x7f0e00fc

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1611
    const v8, 0x7f0e00fd

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1612
    const v8, 0x7f0e00fe

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1613
    const v8, 0x7f0e00ff

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1614
    const v8, 0x7f0e0100

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1616
    :cond_5
    sget-boolean v8, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_WRITETONFCTAG:Z

    if-nez v8, :cond_6

    .line 1618
    const v8, 0x7f0e00fd

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1620
    :cond_6
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v8

    return v8

    .line 1486
    .end local v4    # "del":Landroid/view/MenuItem;
    .end local v5    # "inflater":Landroid/view/MenuInflater;
    :cond_7
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mNothingCheckedItem:Z
    invoke-static {v8, v9}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$602(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;Z)Z

    goto/16 :goto_0

    .line 1531
    .restart local v4    # "del":Landroid/view/MenuItem;
    .restart local v5    # "inflater":Landroid/view/MenuInflater;
    :cond_8
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v8

    if-eqz v8, :cond_c

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isKNOXAvailable()Z

    move-result v8

    if-eqz v8, :cond_c

    .line 1532
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXVersion()Ljava/lang/String;

    move-result-object v7

    .line 1533
    .local v7, "version":Ljava/lang/String;
    const-string v8, "2.0"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1534
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXCallingUserId()I

    move-result v1

    .line 1535
    .local v1, "callingUserId":I
    if-gez v1, :cond_9

    .line 1536
    const v8, 0x7f0e0100

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1537
    const v8, 0x7f0e00ff

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1554
    .end local v1    # "callingUserId":I
    .end local v7    # "version":Ljava/lang/String;
    :goto_3
    iget-object v8, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1555
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_2

    .line 1556
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 1557
    .local v3, "context":Landroid/content/Context;
    if-eqz v3, :cond_2

    .line 1558
    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->getTypeOfSelectedItems(Landroid/content/Context;)I

    move-result v6

    .line 1559
    packed-switch v6, :pswitch_data_0

    .line 1572
    const v8, 0x7f0e0101

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1573
    const v8, 0x7f0e0102

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    goto/16 :goto_1

    .line 1538
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v3    # "context":Landroid/content/Context;
    .restart local v1    # "callingUserId":I
    .restart local v7    # "version":Ljava/lang/String;
    :cond_9
    if-nez v1, :cond_a

    .line 1540
    const v8, 0x7f0e0100

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_3

    .line 1543
    :cond_a
    const v8, 0x7f0e00ff

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_3

    .line 1546
    .end local v1    # "callingUserId":I
    :cond_b
    const v8, 0x7f0e0100

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_3

    .line 1549
    .end local v7    # "version":Ljava/lang/String;
    :cond_c
    const v8, 0x7f0e00ff

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1550
    const v8, 0x7f0e0100

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_3

    .line 1562
    .restart local v0    # "activity":Landroid/app/Activity;
    .restart local v3    # "context":Landroid/content/Context;
    :pswitch_0
    const v8, 0x7f0e0102

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    goto/16 :goto_1

    .line 1565
    :pswitch_1
    const v8, 0x7f0e0101

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    goto/16 :goto_1

    .line 1569
    :pswitch_2
    const v8, 0x7f0e0102

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    goto/16 :goto_1

    .line 1585
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v3    # "context":Landroid/content/Context;
    :cond_d
    const/4 v8, 0x1

    if-ne v2, v8, :cond_3

    .line 1586
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v8

    if-eqz v8, :cond_f

    .line 1587
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXVersion()Ljava/lang/String;

    move-result-object v7

    .line 1588
    .restart local v7    # "version":Ljava/lang/String;
    const-string v8, "2.0"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 1589
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXCallingUserId()I

    move-result v1

    .line 1590
    .restart local v1    # "callingUserId":I
    if-lez v1, :cond_e

    .line 1591
    const v8, 0x7f0e00fc

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    .line 1599
    .end local v1    # "callingUserId":I
    .end local v7    # "version":Ljava/lang/String;
    :cond_e
    :goto_4
    const/4 v8, 0x1

    if-ne v6, v8, :cond_3

    .line 1600
    const v8, 0x7f0e00fc

    invoke-interface {p2, v8}, Landroid/view/Menu;->removeItem(I)V

    goto/16 :goto_2

    .line 1595
    :cond_f
    const-string v8, "VNLibraryActionModeListener"

    const-string v9, "mService is null"

    invoke-static {v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 1559
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method protected refreashActionMode()V
    .locals 5

    .prologue
    .line 1715
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    .line 1716
    .local v0, "actionMode":Landroid/view/ActionMode;
    if-eqz v0, :cond_0

    .line 1717
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v1

    .line 1718
    .local v1, "checkItemCount":I
    if-eqz v1, :cond_1

    .line 1719
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    const/4 v4, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mNothingCheckedItem:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$602(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;Z)Z

    .line 1724
    :goto_0
    :try_start_0
    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1729
    .end local v1    # "checkItemCount":I
    :cond_0
    :goto_1
    return-void

    .line 1721
    .restart local v1    # "checkItemCount":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$ActionModeListener;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    const/4 v4, 0x1

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->mNothingCheckedItem:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->access$602(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;Z)Z

    goto :goto_0

    .line 1725
    :catch_0
    move-exception v2

    .line 1726
    .local v2, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method
