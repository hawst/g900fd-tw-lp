.class public Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
.super Landroid/app/Service;
.source "VoiceNoteService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;,
        Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;
    }
.end annotation


# static fields
.field private static final PAUSE_OVER_TIME:J = 0x493e0L

.field private static final TAG:Ljava/lang/String; = "VoiceNoteService"

.field private static mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

.field private static mLongKeyCnt:I


# instance fields
.field private TempLongKeyCnt:I

.field private isMMSLimited:Z

.field private isPlayerReceiverRegisterd:Z

.field private final mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioFormat:Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;

.field private mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

.field private mBinder:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;

.field private mCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mCancelDialog:Landroid/app/AlertDialog;

.field private mDateAdded:Ljava/lang/String;

.field private mDurationUpdated:I

.field private mEasyModeReceiver:Landroid/content/BroadcastReceiver;

.field private final mEventHandler:Landroid/os/Handler;

.field private mFileName:Ljava/lang/String;

.field private mFileSaveHiddenPath:Ljava/lang/String;

.field private mFileSavePath:Ljava/lang/String;

.field private mFileSize:I

.field private mFragmentControllerState:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

.field private mFromVVM:Z

.field private mIsCallRinging:Z

.field private mIsEasymode:Z

.field private mIsOpenStatusBar:Z

.field private mIsPausedForaWhile:Z

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private mLastSavedFileUri:Landroid/net/Uri;

.field private mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

.field private mMediaButtonProcessReceiver:Landroid/content/BroadcastReceiver;

.field private mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

.field private mMediaRecorderState:I

.field private mMediaSession:Landroid/media/session/MediaSession;

.field private mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

.field private mMotionListener:Lcom/samsung/android/motion/MRListener;

.field private mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

.field private mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

.field mPauseEnoughHandler:Landroid/os/Handler;

.field private mPausedByCall:Z

.field private final mPlayerReceiver:Landroid/content/BroadcastReceiver;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mRecordedDurationMSec:I

.field private mRemoteViewDisappear:Ljava/lang/Runnable;

.field private mRepeatEndTime:I

.field private mRepeatStartTime:I

.field private mSelectedFileName:Ljava/lang/String;

.field private mStartRecordingUtcTimeForLoging:Ljava/lang/String;

.field private mStartVoicelabelAfterRecord:Z

.field private mStopDialog:Landroid/app/AlertDialog;

.field private mStorageFullDialog:Landroid/app/AlertDialog;

.field private mTrimEndTime:I

.field private mTrimStartTime:I

.field private mTurnonSTT:Z

.field private mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

.field private mVNMemoData:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

.field private mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

.field private strNewFileName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    .line 2074
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 118
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 121
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCallbacks:Landroid/os/RemoteCallbackList;

    .line 122
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mBinder:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;

    .line 123
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    .line 124
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPowerManager:Landroid/os/PowerManager;

    .line 125
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 126
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioFormat:Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;

    .line 127
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    .line 128
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .line 129
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    .line 130
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    .line 131
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNMemoData:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    .line 132
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    .line 133
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    .line 136
    iput v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    .line 137
    iput v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mDurationUpdated:I

    .line 138
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mSelectedFileName:Ljava/lang/String;

    .line 139
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    .line 140
    sget-object v0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;->HIDDEN:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFragmentControllerState:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    .line 141
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPausedByCall:Z

    .line 142
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTurnonSTT:Z

    .line 149
    iput v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSize:I

    .line 150
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isMMSLimited:Z

    .line 151
    iput v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatStartTime:I

    .line 152
    iput v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatEndTime:I

    .line 153
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsPausedForaWhile:Z

    .line 154
    iput v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimStartTime:I

    .line 155
    iput v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimEndTime:I

    .line 156
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStartVoicelabelAfterRecord:Z

    .line 157
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsCallRinging:Z

    .line 160
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFromVVM:Z

    .line 162
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStartRecordingUtcTimeForLoging:Ljava/lang/String;

    .line 164
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsEasymode:Z

    .line 165
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsOpenStatusBar:Z

    .line 941
    new-instance v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 1234
    new-instance v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mEventHandler:Landroid/os/Handler;

    .line 1517
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCancelDialog:Landroid/app/AlertDialog;

    .line 1518
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStopDialog:Landroid/app/AlertDialog;

    .line 1519
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStorageFullDialog:Landroid/app/AlertDialog;

    .line 1953
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlayerReceiverRegisterd:Z

    .line 1955
    new-instance v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$11;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPlayerReceiver:Landroid/content/BroadcastReceiver;

    .line 2014
    new-instance v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$12;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    .line 2037
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mEasyModeReceiver:Landroid/content/BroadcastReceiver;

    .line 2072
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaButtonProcessReceiver:Landroid/content/BroadcastReceiver;

    .line 2073
    iput v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I

    .line 2429
    new-instance v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$15;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRemoteViewDisappear:Ljava/lang/Runnable;

    .line 2438
    new-instance v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$16;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPauseEnoughHandler:Landroid/os/Handler;

    .line 2554
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPausedByCall:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .param p1, "x1"    # Z

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPausedByCall:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsPausedForaWhile:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Landroid/app/KeyguardManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mKeyguardManager:Landroid/app/KeyguardManager;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .param p1, "x1"    # Z

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsPausedForaWhile:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    iget v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatEndTime:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .param p1, "x1"    # I

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setState(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    iget v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNAudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .param p1, "x1"    # I

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendMessageCallback(I)V

    return-void
.end method

.method static synthetic access$1602(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .param p1, "x1"    # Z

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsOpenStatusBar:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStopDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$1802(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCancelDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$1902(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStorageFullDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNIntent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsEasymode:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .param p1, "x1"    # Z

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsEasymode:Z

    return p1
.end method

.method static synthetic access$2200()I
    .locals 1

    .prologue
    .line 118
    sget v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I

    return v0
.end method

.method static synthetic access$2202(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 118
    sput p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I

    return p0
.end method

.method static synthetic access$2208()I
    .locals 2

    .prologue
    .line 118
    sget v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLongKeyCnt:I

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    iget v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I

    return v0
.end method

.method static synthetic access$2302(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .param p1, "x1"    # I

    .prologue
    .line 118
    iput p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->TempLongKeyCnt:I

    return p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getFileSize()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getFilePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getDateAdded()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getLastSavedFileUriString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMaxAmplitude()I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .param p1, "x1"    # I

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRealFreeSize(I)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Landroid/media/session/MediaSession;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaSession:Landroid/media/session/MediaSession;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsCallRinging:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .param p1, "x1"    # Z

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsCallRinging:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecordingOnEvent()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->showStopDialog()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->showCancelDialog()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isAdvancedAndHeadsetMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    return-object v0
.end method

.method private checkAndMakeNewFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "src"    # Ljava/lang/String;

    .prologue
    .line 912
    const/4 v4, 0x1

    .line 914
    .local v4, "index":I
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 915
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 916
    .local v0, "curFileName":Ljava/lang/String;
    const-string v7, "."

    invoke-virtual {v0, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    .line 917
    .local v5, "intExtIndex":I
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 918
    .local v1, "curFileNameLen":I
    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    .line 920
    .local v2, "dirPath":Ljava/lang/String;
    const/4 v7, -0x1

    if-ne v5, v7, :cond_0

    .line 921
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getDestPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 934
    :goto_0
    return-object v7

    .line 923
    :cond_0
    :goto_1
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExistFileName(Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 924
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 925
    .local v6, "newfileName":Ljava/lang/StringBuffer;
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, v5}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 926
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 927
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->strNewFileName:Ljava/lang/String;

    .line 928
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    sub-int v8, v1, v5

    invoke-virtual {v6, v7, v5, v8}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 930
    new-instance v3, Ljava/io/File;

    .end local v3    # "file":Ljava/io/File;
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 931
    .restart local v3    # "file":Ljava/io/File;
    add-int/lit8 v4, v4, 0x1

    .line 932
    goto :goto_1

    .line 934
    .end local v6    # "newfileName":Ljava/lang/StringBuffer;
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method private disableSystemSound()V
    .locals 5

    .prologue
    .line 830
    :try_start_0
    const-string v2, "statusbar"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/StatusBarManager;

    .line 831
    .local v1, "statusBar":Landroid/app/StatusBarManager;
    if-eqz v1, :cond_0

    .line 832
    const/high16 v2, 0x40000

    invoke-virtual {v1, v2}, Landroid/app/StatusBarManager;->disable(I)V

    .line 834
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->setStreamMute(Z)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 840
    .end local v1    # "statusBar":Landroid/app/StatusBarManager;
    :goto_0
    return-void

    .line 835
    :catch_0
    move-exception v0

    .line 836
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v2, "VoiceNoteService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "disableSystemSound : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 837
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v0

    .line 838
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v2, "VoiceNoteService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "disableSystemSound : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private enableSystemSound()V
    .locals 5

    .prologue
    .line 844
    :try_start_0
    const-string v2, "statusbar"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/StatusBarManager;

    .line 845
    .local v1, "statusBar":Landroid/app/StatusBarManager;
    if-eqz v1, :cond_0

    .line 846
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/StatusBarManager;->disable(I)V

    .line 848
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->setStreamMute(Z)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 854
    .end local v1    # "statusBar":Landroid/app/StatusBarManager;
    :goto_0
    return-void

    .line 849
    :catch_0
    move-exception v0

    .line 850
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v2, "VoiceNoteService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableSystemSound : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 851
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v0

    .line 852
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v2, "VoiceNoteService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableSystemSound : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getDateAdded()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1708
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mDateAdded:Ljava/lang/String;

    return-object v0
.end method

.method private getDestPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 20
    .param p1, "src"    # Ljava/lang/String;

    .prologue
    .line 870
    move-object/from16 v10, p1

    .line 871
    .local v10, "rst":Ljava/lang/String;
    const-string v12, ""

    .line 873
    .local v12, "strTitle":Ljava/lang/String;
    const/4 v3, 0x0

    .line 874
    .local v3, "hasDir":Z
    const/4 v9, 0x0

    .line 875
    .local v9, "isNormalFile":Z
    const/4 v8, 0x1

    .line 877
    .local v8, "isFirst":Z
    const-string v14, "/"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 878
    .local v4, "intDirIndex":I
    const-string v14, "_"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    .line 879
    .local v7, "intPrefixIndex":I
    const-string v14, "."

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    .line 881
    .local v5, "intExtIndex":I
    const/4 v14, -0x1

    if-eq v4, v14, :cond_0

    .line 882
    const/4 v3, 0x1

    .line 884
    :cond_0
    const/4 v14, -0x1

    if-eq v7, v14, :cond_1

    .line 885
    add-int/lit8 v14, v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 886
    .local v13, "strTmpIndex":Ljava/lang/String;
    const-string v14, "\\d{3,4}"

    invoke-virtual {v13, v14}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_4

    const/4 v8, 0x1

    .line 888
    .end local v13    # "strTmpIndex":Ljava/lang/String;
    :cond_1
    :goto_0
    const/4 v14, -0x1

    if-eq v5, v14, :cond_2

    .line 889
    const/4 v9, 0x1

    .line 892
    :cond_2
    if-eqz v3, :cond_3

    if-eqz v9, :cond_3

    .line 893
    if-eqz v8, :cond_5

    .line 894
    add-int/lit8 v14, v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 900
    :cond_3
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->findFileIndex(Ljava/lang/String;Landroid/content/Context;)I

    move-result v6

    .line 902
    .local v6, "intFileIndex":I
    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v15, "%s%s(%d)%s"

    const/16 v16, 0x4

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    add-int/lit8 v19, v4, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    aput-object v12, v16, v17

    const/16 v17, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v14 .. v16}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 903
    .local v11, "strNewFilePath":Ljava/lang/String;
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->strNewFileName:Ljava/lang/String;

    .line 904
    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v15, "%s(%d)"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v12, v16, v17

    const/16 v17, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v14 .. v16}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->strNewFileName:Ljava/lang/String;

    .line 906
    move-object v10, v11

    .line 908
    return-object v10

    .line 886
    .end local v6    # "intFileIndex":I
    .end local v11    # "strNewFilePath":Ljava/lang/String;
    .restart local v13    # "strTmpIndex":Ljava/lang/String;
    :cond_4
    const/4 v8, 0x0

    goto :goto_0

    .line 896
    .end local v13    # "strTmpIndex":Ljava/lang/String;
    :cond_5
    add-int/lit8 v14, v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    goto :goto_1
.end method

.method private getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1696
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method private getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1704
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSavePath:Ljava/lang/String;

    return-object v0
.end method

.method private getFileSize()J
    .locals 2

    .prologue
    .line 1674
    iget v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSize:I

    int-to-long v0, v0

    return-wide v0
.end method

.method private getLastSavedFileUriString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1700
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLastSavedFileUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLastSavedFileUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getMaxAmplitude()I
    .locals 5

    .prologue
    .line 1678
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    const/16 v4, 0x3eb

    if-ne v3, v4, :cond_0

    .line 1679
    const/4 v1, -0x1

    .line 1682
    .local v1, "maxAmplitude":I
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->getMaxAmplitude()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 1691
    .end local v1    # "maxAmplitude":I
    :goto_0
    return v1

    .line 1683
    .restart local v1    # "maxAmplitude":I
    :catch_0
    move-exception v0

    .line 1684
    .local v0, "ise":Ljava/lang/IllegalStateException;
    const-string v3, "VoiceNoteService"

    const-string v4, "cannot get max amplitude"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1685
    .end local v0    # "ise":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v2

    .line 1686
    .local v2, "re":Ljava/lang/RuntimeException;
    const-string v3, "VoiceNoteService"

    const-string v4, "cannot get max amplitude"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1691
    .end local v1    # "maxAmplitude":I
    .end local v2    # "re":Ljava/lang/RuntimeException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private getRealFreeSize(I)J
    .locals 7
    .param p1, "warningSize"    # I

    .prologue
    .line 1923
    const-wide/16 v4, 0x0

    .line 1924
    .local v4, "realFreeSize":J
    const-wide/16 v2, 0x0

    .line 1925
    .local v2, "realFileSize":J
    const/4 v0, 0x0

    .line 1926
    .local v0, "fileSize":I
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSaveHiddenPath:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1927
    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSaveHiddenPath:Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 1928
    iget v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSize:I

    .line 1930
    :cond_0
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getInternalStorageSelected()Z

    move-result v1

    add-int/lit16 v6, p1, 0x1400

    invoke-static {v1, v6, v2, v3, v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkAlmostFull(ZIJI)J

    move-result-wide v4

    .line 1931
    return-wide v4
.end method

.method private isAdvancedAndHeadsetMode()Z
    .locals 3

    .prologue
    .line 1503
    const-string v2, "record_mode"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v1

    .line 1504
    .local v1, "record_mode":I
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1506
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->is4PinHeadsetConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isAttachMode:Z

    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    .line 1508
    const/4 v2, 0x1

    .line 1510
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isScreenOn()Z
    .locals 2

    .prologue
    .line 2509
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPowerManager:Landroid/os/PowerManager;

    if-nez v1, :cond_0

    .line 2510
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPowerManager:Landroid/os/PowerManager;

    .line 2512
    :cond_0
    const/4 v0, 0x0

    .line 2513
    .local v0, "isScreenOn":Z
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPowerManager:Landroid/os/PowerManager;

    if-eqz v1, :cond_1

    .line 2514
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    .line 2516
    :cond_1
    return v0
.end method

.method private prepareMediaSession()V
    .locals 4

    .prologue
    .line 2536
    new-instance v0, Landroid/media/session/MediaSession;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/media/session/MediaSession;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaSession:Landroid/media/session/MediaSession;

    .line 2538
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaSession:Landroid/media/session/MediaSession;

    new-instance v1, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$MediaSessionCallback;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$1;)V

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setCallback(Landroid/media/session/MediaSession$Callback;)V

    .line 2539
    const-string v0, "VoiceNoteService"

    const-string v1, " prepareMediaSession "

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2540
    new-instance v0, Landroid/media/session/PlaybackState$Builder;

    invoke-direct {v0}, Landroid/media/session/PlaybackState$Builder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    .line 2541
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const-wide/16 v2, 0x37f

    invoke-virtual {v0, v2, v3}, Landroid/media/session/PlaybackState$Builder;->setActions(J)Landroid/media/session/PlaybackState$Builder;

    .line 2548
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 2549
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaSession:Landroid/media/session/MediaSession;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setFlags(I)V

    .line 2551
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaSession:Landroid/media/session/MediaSession;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setActive(Z)V

    .line 2552
    return-void
.end method

.method private prepareRecording(Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;J)Z
    .locals 10
    .param p1, "audioformat"    # Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;
    .param p2, "sizeMMS"    # J

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1020
    const-string v5, "record_mode"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v2

    .line 1022
    .local v2, "record_mode":I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->is4PinHeadsetConnected()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1023
    invoke-virtual {p1}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->isAttachMode()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1024
    if-eq v2, v4, :cond_0

    const/4 v5, 0x2

    if-ne v2, v5, :cond_1

    .line 1026
    :cond_0
    const-string v4, "VoiceNoteService"

    const-string v5, "prepareRecording : advanced&headset"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0b000b

    invoke-static {v4, v5, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 1080
    :goto_0
    return v3

    .line 1030
    :cond_1
    const/4 v5, 0x3

    if-ne v2, v5, :cond_2

    .line 1031
    const-string v4, "VoiceNoteService"

    const-string v5, "prepareRecording : advanced&headset"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0b000f

    invoke-static {v4, v5, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto :goto_0

    .line 1038
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallIdle(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallActive(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1039
    :cond_3
    const-string v4, "VoiceNoteService"

    const-string v5, "prepareRecording : block during calling"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0b00d7

    invoke-static {v4, v5, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto :goto_0

    .line 1043
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->isRecordActive()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1044
    const-string v4, "VoiceNoteService"

    const-string v5, "prepareRecording : isRecordActive"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 1045
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0b00ff

    invoke-static {v4, v5, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto :goto_0

    .line 1048
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->requestAudioFocus()Z

    move-result v5

    if-nez v5, :cond_6

    .line 1049
    const-string v4, "VoiceNoteService"

    const-string v5, "prepareRecording : requestAudioFocus fail"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1052
    :cond_6
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getInternalStorageSelected()Z

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkAvailableStorage(Z)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1053
    const-string v4, "VoiceNoteService"

    const-string v5, "prepareRecording : checkAvailableStorage fail"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getInternalStorageSelected()Z

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getAvailableStorage(Z)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->showStorageDialog(J)V

    goto :goto_0

    .line 1057
    :cond_7
    invoke-virtual {p1}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->isAttachMode()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1058
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getInternalStorageSelected()Z

    move-result v5

    const/16 v6, 0x5280

    const-wide/16 v8, 0x0

    invoke-static {v5, v6, v8, v9, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkAlmostFull(ZIJI)J

    move-result-wide v0

    .line 1060
    .local v0, "freesize":J
    cmp-long v5, v0, p2

    if-lez v5, :cond_8

    .line 1061
    move-wide v0, p2

    .line 1062
    iput-boolean v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isMMSLimited:Z

    .line 1066
    :goto_1
    invoke-virtual {p1}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->getMinRecordSize()I

    move-result v5

    int-to-long v6, v5

    cmp-long v5, v0, v6

    if-gez v5, :cond_a

    .line 1067
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isMMSLimited:Z

    if-eqz v4, :cond_9

    .line 1068
    const-string v4, "VoiceNoteService"

    const-string v5, "prepareRecording : exceed_message_size_limitation"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 1069
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0b0080

    invoke-static {v4, v5, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 1064
    :cond_8
    iput-boolean v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isMMSLimited:Z

    goto :goto_1

    .line 1071
    :cond_9
    const-string v4, "VoiceNoteService"

    const-string v5, "prepareRecording : memory_full"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 1072
    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->showStorageDialog(J)V

    goto/16 :goto_0

    .line 1078
    .end local v0    # "freesize":J
    :cond_a
    iget v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    iget v6, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    invoke-static {p0, v5, v6, v3}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendState(Landroid/content/Context;III)V

    move v3, v4

    .line 1080
    goto/16 :goto_0
.end method

.method private prepareResumeRecording()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1084
    const-string v3, "record_mode"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v0

    .line 1086
    .local v0, "record_mode":I
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->is4PinHeadsetConnected()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1087
    if-eq v0, v2, :cond_0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    .line 1088
    :cond_0
    const-string v2, "VoiceNoteService"

    const-string v3, "prepareResumeRecording : advanced&headset"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 1089
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b000b

    invoke-static {v2, v3, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 1111
    :goto_0
    return v1

    .line 1091
    :cond_1
    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    .line 1092
    const-string v2, "VoiceNoteService"

    const-string v3, "prepareResumeRecording : advanced&headset"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 1093
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b000f

    invoke-static {v2, v3, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto :goto_0

    .line 1097
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallIdle(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallActive(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1098
    :cond_3
    const-string v2, "VoiceNoteService"

    const-string v3, "prepareResumeRecording : isCallIdle"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 1099
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b00d7

    invoke-static {v2, v3, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto :goto_0

    .line 1103
    :cond_4
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getInternalStorageSelected()Z

    move-result v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkAvailableStorage(Z)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1104
    const-string v2, "VoiceNoteService"

    const-string v3, "prepareResumeRecording : checkAvailableStorage fail"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 1105
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getInternalStorageSelected()Z

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getAvailableStorage(Z)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->showStorageDialog(J)V

    goto :goto_0

    .line 1109
    :cond_5
    iget v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    iget v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    invoke-static {p0, v3, v4, v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendState(Landroid/content/Context;III)V

    move v1, v2

    .line 1111
    goto :goto_0
.end method

.method private registerEasyModeReceiver()V
    .locals 3

    .prologue
    .line 2039
    const-string v1, "VoiceNoteService"

    const-string v2, "registerEasyModeReceiver"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2041
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2042
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2043
    const-string v1, "com.android.launcher.action.EASY_MODE_CHANGE_VOICENOTE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2045
    new-instance v1, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$13;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$13;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mEasyModeReceiver:Landroid/content/BroadcastReceiver;

    .line 2069
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mEasyModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2070
    return-void
.end method

.method private registerPlayerReceiver()V
    .locals 2

    .prologue
    .line 1967
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1968
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1969
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPlayerReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1971
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlayerReceiverRegisterd:Z

    .line 1972
    return-void
.end method

.method private release()V
    .locals 1

    .prologue
    .line 1728
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->release()V

    .line 1729
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    .line 1730
    return-void
.end method

.method private requestAudioFocus()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 343
    const-string v3, "VoiceNoteService"

    const-string v4, "requestAudioFocus()"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const/4 v0, 0x0

    .line 345
    .local v0, "focusResult":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v3, 0x5

    if-ge v1, v3, :cond_0

    .line 346
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)Z

    move-result v0

    .line 347
    if-ne v0, v2, :cond_2

    .line 350
    :cond_0
    if-nez v0, :cond_1

    .line 351
    const-string v2, "VoiceNoteService"

    const-string v3, "requestAudioFocus is failed"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const/4 v2, 0x0

    .line 354
    :cond_1
    return v2

    .line 345
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private saveFile()Z
    .locals 24

    .prologue
    .line 659
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getNewFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileName:Ljava/lang/String;

    .line 660
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getSoundsPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioFormat:Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->getExtension()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSavePath:Ljava/lang/String;

    .line 663
    :try_start_0
    new-instance v19, Landroid/media/MediaMetadataRetriever;

    invoke-direct/range {v19 .. v19}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 664
    .local v19, "retriever":Landroid/media/MediaMetadataRetriever;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSaveHiddenPath:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 666
    const/16 v23, 0x0

    .line 667
    .local v23, "value":Ljava/lang/String;
    const/16 v2, 0x9

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v23

    .line 668
    invoke-virtual/range {v19 .. v19}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 669
    const/16 v19, 0x0

    .line 671
    invoke-static/range {v23 .. v23}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    .line 672
    .local v16, "fileDuration":J
    const-wide/16 v2, 0x0

    cmp-long v2, v16, v2

    if-ltz v2, :cond_0

    const-wide/16 v2, 0x3e8

    cmp-long v2, v16, v2

    if-gez v2, :cond_0

    .line 673
    const-string v2, "VoiceNoteService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cancel recording while save recording by file duration is low than 1 sec, fileDuration: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->cancelRecording()Z

    .line 675
    const/4 v2, 0x0

    .line 795
    .end local v16    # "fileDuration":J
    .end local v19    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v23    # "value":Ljava/lang/String;
    :goto_0
    return v2

    .line 678
    .restart local v16    # "fileDuration":J
    .restart local v19    # "retriever":Landroid/media/MediaMetadataRetriever;
    .restart local v23    # "value":Ljava/lang/String;
    :cond_0
    new-instance v15, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSaveHiddenPath:Ljava/lang/String;

    invoke-direct {v15, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 679
    .local v15, "hiddenFile":Ljava/io/File;
    new-instance v20, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSavePath:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 681
    .local v20, "saveFile":Ljava/io/File;
    invoke-static {v15}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExistFile(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 682
    const-string v2, "VoiceNoteService"

    const-string v3, "cancel recording while save by Hidden file doesn\'t exist"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b00fd

    const/4 v5, 0x0

    invoke-static {v2, v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 684
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->cancelRecording()Z

    .line 685
    const/4 v2, 0x0

    goto :goto_0

    .line 688
    :cond_1
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExistFileName(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 691
    sget-boolean v2, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_CHECK_ATTVN_ENABLED:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFromVVM:Z

    if-nez v2, :cond_3

    .line 692
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0083

    const/4 v5, 0x0

    invoke-static {v2, v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 695
    :cond_3
    const-string v2, "VoiceNoteService"

    const-string v3, "rename saving file while save by saving file already exist"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v21

    .line 698
    .local v21, "saveString":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->checkAndMakeNewFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 699
    .local v13, "dest":Ljava/lang/String;
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSavePath:Ljava/lang/String;

    .line 700
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->strNewFileName:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileName:Ljava/lang/String;

    .line 701
    const/16 v20, 0x0

    .line 702
    new-instance v20, Ljava/io/File;

    .end local v20    # "saveFile":Ljava/io/File;
    move-object/from16 v0, v20

    invoke-direct {v0, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 705
    .end local v13    # "dest":Ljava/lang/String;
    .end local v21    # "saveString":Ljava/lang/String;
    .restart local v20    # "saveFile":Ljava/io/File;
    :cond_4
    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v18

    .line 706
    .local v18, "resMove":Z
    const-string v2, "VoiceNoteService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "move result : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    if-nez v18, :cond_5

    .line 708
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b00fd

    const/4 v5, 0x0

    invoke-static {v2, v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 709
    const-string v2, "VoiceNoteService"

    const-string v3, "cancel recording while save by can not rename file"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->cancelRecording()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 711
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 713
    .end local v15    # "hiddenFile":Ljava/io/File;
    .end local v16    # "fileDuration":J
    .end local v18    # "resMove":Z
    .end local v19    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v20    # "saveFile":Ljava/io/File;
    .end local v23    # "value":Ljava/lang/String;
    :catch_0
    move-exception v14

    .line 714
    .local v14, "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    .line 715
    const-string v2, "VoiceNoteService"

    const-string v3, "error occurred while save by moving temp file"

    invoke-static {v2, v3, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 716
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b00fd

    const/4 v5, 0x0

    invoke-static {v2, v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 717
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->cancelRecording()Z

    .line 718
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 720
    .end local v14    # "e":Ljava/lang/Exception;
    .restart local v15    # "hiddenFile":Ljava/io/File;
    .restart local v16    # "fileDuration":J
    .restart local v18    # "resMove":Z
    .restart local v19    # "retriever":Landroid/media/MediaMetadataRetriever;
    .restart local v20    # "saveFile":Ljava/io/File;
    .restart local v23    # "value":Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->getCacheLoader()Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;

    move-result-object v11

    .line 721
    .local v11, "cacheLoader":Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;
    if-eqz v11, :cond_6

    .line 722
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSavePath:Ljava/lang/String;

    invoke-virtual {v11, v2}, Lcom/sec/android/app/voicenote/library/common/VNCacheLoader;->removePathFromCache(Ljava/lang/String;)V

    .line 725
    :cond_6
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    const-wide/16 v6, 0x3e8

    div-long v8, v2, v6

    .line 726
    .local v8, "addTime":J
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mDateAdded:Ljava/lang/String;

    .line 729
    :try_start_1
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 730
    .local v12, "contentValues":Landroid/content/ContentValues;
    const-string v2, "title"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileName:Ljava/lang/String;

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    const-string v2, "mime_type"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioFormat:Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    const-string v2, "_data"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSavePath:Ljava/lang/String;

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    const-string v2, "duration"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 734
    const-string v2, "_size"

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 735
    const-string v2, "date_added"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 736
    const-string v2, "date_modified"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 737
    const-string v2, "recordingtype"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 738
    const-string v2, "latitude"

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->getLatitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 739
    const-string v2, "longitude"

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->getLongitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 740
    const-string v2, "addr"

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->getAddrTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    const-string v2, "track"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 743
    const-string v2, "is_ringtone"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 744
    const-string v2, "is_alarm"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 745
    const-string v2, "is_notification"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 746
    const-string v2, "label_id"

    const-string v3, "category_label_id"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 747
    const-string v2, "is_drm"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 748
    const-string v2, "album"

    const-string v3, "Sounds"

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    const-string v3, "is_memo"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNMemoData:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->hasSTTdata()Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v12, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 751
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    .line 752
    .local v22, "sb":Ljava/lang/StringBuilder;
    const-string v2, "_data"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSavePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 753
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    .line 756
    .local v4, "selection":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 758
    .local v10, "c":Landroid/database/Cursor;
    if-eqz v10, :cond_7

    .line 759
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_a

    .line 760
    const-string v2, "VoiceNoteService"

    const-string v3, "saved file is not inserted to DB yet"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v12}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLastSavedFileUri:Landroid/net/Uri;

    .line 768
    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 770
    :cond_7
    const/4 v10, 0x0

    .line 772
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLastSavedFileUri:Landroid/net/Uri;

    if-nez v2, :cond_c

    .line 773
    const-string v2, "VoiceNoteService"

    const-string v3, "cancel recording while save by Content Resolver insert failed"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b00fd

    const/4 v5, 0x0

    invoke-static {v2, v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 775
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->cancelRecording()Z

    .line 776
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSavePath:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 777
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z

    .line 779
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 749
    .end local v4    # "selection":[Ljava/lang/String;
    .end local v10    # "c":Landroid/database/Cursor;
    .end local v22    # "sb":Ljava/lang/StringBuilder;
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 763
    .restart local v4    # "selection":[Ljava/lang/String;
    .restart local v10    # "c":Landroid/database/Cursor;
    .restart local v22    # "sb":Ljava/lang/StringBuilder;
    :cond_a
    const-string v2, "VoiceNoteService"

    const-string v3, "saved file is inserted to DB already"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 765
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://media"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLastSavedFileUri:Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 781
    .end local v4    # "selection":[Ljava/lang/String;
    .end local v10    # "c":Landroid/database/Cursor;
    .end local v12    # "contentValues":Landroid/content/ContentValues;
    .end local v22    # "sb":Ljava/lang/StringBuilder;
    :catch_1
    move-exception v14

    .line 782
    .restart local v14    # "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    .line 783
    const-string v2, "VoiceNoteService"

    const-string v3, "cancel recording while save by error occurred while input data to MediaStore"

    invoke-static {v2, v3, v14}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 784
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b00fd

    const/4 v5, 0x0

    invoke-static {v2, v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 785
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->cancelRecording()Z

    .line 786
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSavePath:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 787
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z

    .line 789
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 792
    .end local v14    # "e":Ljava/lang/Exception;
    .restart local v4    # "selection":[Ljava/lang/String;
    .restart local v10    # "c":Landroid/database/Cursor;
    .restart local v12    # "contentValues":Landroid/content/ContentValues;
    .restart local v22    # "sb":Ljava/lang/StringBuilder;
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "SAVE"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 793
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLastSavedFileUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStartRecordingUtcTimeForLoging:Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentUtcTime()Ljava/lang/String;

    move-result-object v6

    move-wide/from16 v0, v16

    long-to-int v7, v0

    invoke-static {v2, v3, v5, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 795
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private saveMemoData()V
    .locals 3

    .prologue
    .line 799
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 801
    .local v0, "filepath":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 802
    :cond_0
    const-string v1, "VoiceNoteService"

    const-string v2, "saveMemoData: filepath is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    :goto_0
    return-void

    .line 805
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNMemoData:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->saveMemoData(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private saveRecordingOnEvent()V
    .locals 3

    .prologue
    .line 1009
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->isNotificationShow()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1010
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecordingOnBackground()Ljava/lang/String;

    .line 1017
    :cond_0
    :goto_0
    return-void

    .line 1012
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecording()Z

    .line 1013
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1014
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1015
    const/16 v1, 0xfdd

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendMessageCallback(I)V

    goto :goto_0
.end method

.method private sendMessageCallback(I)V
    .locals 6
    .param p1, "msg"    # I

    .prologue
    .line 1935
    const/16 v3, 0x838

    if-eq p1, v3, :cond_0

    const/16 v3, 0x835

    if-eq p1, v3, :cond_0

    .line 1936
    const-string v3, "VoiceNoteService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Callback : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1941
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCallbacks:Landroid/os/RemoteCallbackList;

    monitor-enter v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1942
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 1943
    .local v0, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 1944
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    invoke-interface {v3, p1}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;->messageCallback(I)V

    .line 1943
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1946
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1947
    monitor-exit v4

    .line 1951
    .end local v0    # "N":I
    .end local v2    # "i":I
    :goto_1
    return-void

    .line 1947
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1948
    :catch_0
    move-exception v1

    .line 1949
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method private setState(I)V
    .locals 5
    .param p1, "recorderStateOrCommand"    # I

    .prologue
    const/16 v4, 0x3e8

    const/4 v3, 0x0

    .line 1181
    const-string v0, "VoiceNoteService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "change state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1183
    sparse-switch p1, :sswitch_data_0

    .line 1226
    :goto_0
    iget v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setRecorderState(I)V

    .line 1228
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendMessageCallback(I)V

    .line 1229
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isLockScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1230
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sendUpdateAppWidgetOnBtnClick(Landroid/content/Context;)V

    .line 1232
    :cond_0
    return-void

    .line 1185
    :sswitch_0
    iput p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    .line 1186
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resetSaveCancelDialog()V

    goto :goto_0

    .line 1190
    :sswitch_1
    iput p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    goto :goto_0

    .line 1194
    :sswitch_2
    iput p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    goto :goto_0

    .line 1198
    :sswitch_3
    iput p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    .line 1199
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "voicenote_recording_enable"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1200
    const-string v0, "VoiceNoteService"

    const-string v1, "RECORDING voicenote_recording_enable, 1"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1204
    :sswitch_4
    iput p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    goto :goto_0

    .line 1210
    :sswitch_5
    iput v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    .line 1211
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resetSaveCancelDialog()V

    .line 1212
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "voicenote_recording_enable"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1213
    const-string v0, "VoiceNoteService"

    const-string v1, "SAVED voicenote_recording_enable, 0"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1217
    :sswitch_6
    iput v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    .line 1218
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resetSaveCancelDialog()V

    .line 1219
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "voicenote_recording_enable"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1220
    const-string v0, "VoiceNoteService"

    const-string v1, "CANCELED voicenote_recording_enable, 0"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1183
    nop

    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_0
        0x3e9 -> :sswitch_1
        0x3ea -> :sswitch_2
        0x3eb -> :sswitch_3
        0x3ec -> :sswitch_4
        0x7d1 -> :sswitch_5
        0x7d5 -> :sswitch_6
    .end sparse-switch
.end method

.method private showCancelDialog()V
    .locals 3

    .prologue
    .line 1593
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCancelDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    .line 1625
    :goto_0
    return-void

    .line 1597
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v2, 0x7f0c0004

    invoke-direct {v1, p0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1598
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0b0023

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b0022

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1599
    const v1, 0x7f0b00e0

    new-instance v2, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$7;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1608
    const v1, 0x7f0b0021

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1610
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCancelDialog:Landroid/app/AlertDialog;

    .line 1612
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCancelDialog:Landroid/app/AlertDialog;

    new-instance v2, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$8;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$8;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1619
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCancelDialog:Landroid/app/AlertDialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1620
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isLockScreen()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1621
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCancelDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d9

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    .line 1624
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCancelDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 1623
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCancelDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7de

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    goto :goto_1
.end method

.method private showStopDialog()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1533
    const-string v2, "statusbar"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/StatusBarManager;

    .line 1534
    .local v1, "statusBar":Landroid/app/StatusBarManager;
    if-eqz v1, :cond_0

    .line 1535
    invoke-virtual {v1}, Landroid/app/StatusBarManager;->collapsePanels()V

    .line 1538
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isLockScreen()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecDuration()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    .line 1539
    :cond_1
    const-string v2, "VoiceNoteService"

    const-string v3, "showStopDialog - Lock Screen"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1540
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecordingOnBackground()Ljava/lang/String;

    .line 1541
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->hideNotification()Z

    .line 1590
    :cond_2
    :goto_0
    return-void

    .line 1545
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStopDialog:Landroid/app/AlertDialog;

    if-nez v2, :cond_2

    .line 1549
    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v2, Landroid/view/ContextThemeWrapper;

    const v3, 0x7f0c0004

    invoke-direct {v2, p0, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1550
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0b012c

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b0015

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecordingOnBackground()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1551
    const v2, 0x7f0b012b

    new-instance v3, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$4;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1566
    const v2, 0x7f0b012a

    new-instance v3, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$5;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1574
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStopDialog:Landroid/app/AlertDialog;

    .line 1576
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStopDialog:Landroid/app/AlertDialog;

    new-instance v3, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$6;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1584
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStopDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1585
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isLockScreen()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1586
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStopDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7d9

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    .line 1589
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStopDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 1588
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStopDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7de

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    goto :goto_1
.end method

.method private showStorageDialog(J)V
    .locals 9
    .param p1, "enoughtFileSize"    # J

    .prologue
    const-wide/16 v6, 0x400

    const/4 v8, 0x1

    .line 1628
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStorageFullDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    .line 1663
    :goto_0
    return-void

    .line 1631
    :cond_0
    div-long v4, p1, v6

    div-long v2, v4, v6

    .line 1632
    .local v2, "freeSize":J
    const-string v1, "VoiceNoteService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "freeSize = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1633
    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v4, 0x7f0c0004

    invoke-direct {v1, p0, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1634
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0b015c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v4, 0x7f0b00d0

    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getNewFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Mb"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1639
    const v1, 0x7f0b008f

    new-instance v4, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$9;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$9;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1649
    const v1, 0x7f0b00e0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1651
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStorageFullDialog:Landroid/app/AlertDialog;

    .line 1653
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStorageFullDialog:Landroid/app/AlertDialog;

    new-instance v4, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$10;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$10;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1660
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStorageFullDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1661
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStorageFullDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v4, 0x7d9

    invoke-virtual {v1, v4}, Landroid/view/Window;->setType(I)V

    .line 1662
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStorageFullDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0
.end method

.method private unregisterPlayerReceiver()V
    .locals 2

    .prologue
    .line 1975
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlayerReceiverRegisterd:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1976
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPlayerReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1977
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlayerReceiverRegisterd:Z

    .line 1979
    :cond_0
    return-void
.end method


# virtual methods
.method public SetFromVVM(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1832
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFromVVM:Z

    .line 1833
    return-void
.end method

.method public addBookmark()I
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 813
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0b001a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%d"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNMemoData:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->getBookmarkSize()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 814
    .local v0, "bookmarktitle":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecDuration()J

    move-result-wide v2

    long-to-int v1, v2

    .line 816
    .local v1, "elapsed":I
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNMemoData:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    new-instance v3, Lcom/sec/android/app/voicenote/common/util/Bookmark;

    const-string v4, ""

    invoke-direct {v3, v1, v0, v4, v7}, Lcom/sec/android/app/voicenote/common/util/Bookmark;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->addBookmark(Lcom/sec/android/app/voicenote/common/util/Bookmark;)I

    move-result v2

    return v2
.end method

.method public declared-synchronized cancelRecording()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 506
    monitor-enter p0

    :try_start_0
    const-string v0, "VoiceNoteService"

    const-string v1, "cancelRecording"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->hideNotification()Z

    .line 511
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->unregisterMediaButtonEventReceiver(Ljava/lang/String;)V

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->setNoiseReduction(Z)V

    .line 513
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->releaseMediaRecorder()Z

    .line 514
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNMemoData:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->cancelMemoData()V

    .line 516
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mDurationUpdated:I

    .line 517
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    sput v0, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecordDuration:I

    .line 518
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isMMSLimited:Z

    .line 520
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->enableSystemSound()V

    .line 521
    const/16 v0, 0x3e8

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setState(I)V

    .line 522
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->turnOnSTT(Z)V

    .line 524
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->getSTTState()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 525
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->stopSTT()V

    .line 526
    const/16 v0, 0x83f

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendMessageCallback(I)V

    .line 529
    :cond_1
    iget v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    iget v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendState(Landroid/content/Context;III)V

    .line 530
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setStartVoiceLabelAfterRecord(Z)V

    .line 531
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPauseEnoughHandler:Landroid/os/Handler;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 533
    monitor-exit p0

    return v3

    .line 506
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public disconnectKnoxContainerManager()V
    .locals 2

    .prologue
    .line 286
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-eqz v0, :cond_0

    .line 287
    const-string v0, "VoiceNoteService"

    const-string v1, "disconnectKnoxContainerManager"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    invoke-virtual {v0}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->unbindContainerManager()V

    .line 289
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    .line 291
    :cond_0
    return-void
.end method

.method public getBookmarkcount()I
    .locals 1

    .prologue
    .line 825
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNMemoData:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->getBookmarkSize()I

    move-result v0

    return v0
.end method

.method public getContentURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1911
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->getContentURI()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentPlayingID()J
    .locals 2

    .prologue
    .line 1907
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->getCurrentPlayingID()J

    move-result-wide v0

    return-wide v0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 1855
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    if-nez v0, :cond_0

    .line 1856
    const/4 v0, -0x1

    .line 1858
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->getCurrentPosition()I

    move-result v0

    goto :goto_0
.end method

.method public getCurrentPositionForRepeat()I
    .locals 2

    .prologue
    .line 1862
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->getCurrentPosition()I

    move-result v0

    .line 1863
    .local v0, "position":I
    iget v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatEndTime:I

    if-lez v1, :cond_1

    .line 1864
    iget v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatEndTime:I

    if-lt v0, v1, :cond_0

    .line 1865
    iget v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatStartTime:I

    .line 1866
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    :cond_0
    :goto_0
    move v1, v0

    .line 1879
    :goto_1
    return v1

    .line 1868
    :cond_1
    iget v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimEndTime:I

    if-lez v1, :cond_0

    .line 1869
    iget v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimEndTime:I

    if-ge v1, v0, :cond_0

    .line 1870
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->isPaused()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1871
    iget v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimStartTime:I

    goto :goto_1

    .line 1873
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 1874
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->UpdateStop()V

    .line 1875
    iget v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimStartTime:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 1876
    iget v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimStartTime:I

    goto :goto_0
.end method

.method public getCurrentRemainTimeForReapeat()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2255
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->getCurrentPosition()I

    move-result v0

    .line 2256
    .local v0, "position":I
    iget v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatEndTime:I

    if-lez v2, :cond_1

    .line 2257
    iget v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatEndTime:I

    sub-int/2addr v2, v0

    if-lez v2, :cond_0

    iget v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatEndTime:I

    sub-int/2addr v1, v0

    .line 2263
    :cond_0
    :goto_0
    return v1

    .line 2258
    :cond_1
    iget v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimEndTime:I

    if-lez v2, :cond_2

    .line 2259
    iget v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimEndTime:I

    sub-int/2addr v2, v0

    if-lez v2, :cond_0

    iget v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimEndTime:I

    sub-int/2addr v1, v0

    goto :goto_0

    .line 2261
    :cond_2
    const/4 v0, -0x1

    move v1, v0

    .line 2263
    goto :goto_0
.end method

.method public getFragmentControllerState()Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;
    .locals 1

    .prologue
    .line 2524
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFragmentControllerState:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    return-object v0
.end method

.method public getKNOXCallingUserId()I
    .locals 6

    .prologue
    .line 2192
    const/4 v1, -0x1

    .line 2194
    .local v1, "callingUserId":I
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 2195
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "isSupportMoveTo"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2196
    .local v2, "isSupportMoveTo":Ljava/lang/String;
    const-string v3, "VoiceNoteService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isSupportMoveTo : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198
    const-string v3, "true"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2199
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v1

    .line 2201
    :cond_0
    const-string v3, "VoiceNoteService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Use : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2202
    return v1
.end method

.method public getKNOXContainer()Lcom/sec/knox/containeragent/ContainerInstallerManager;
    .locals 1

    .prologue
    .line 2251
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    return-object v0
.end method

.method public getKNOXVersion()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2206
    const-string v2, "1.0"

    .line 2209
    .local v2, "version":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 2210
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "version"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 2217
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    const-string v3, "VoiceNoteService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "KNX ver : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2218
    return-object v2

    .line 2211
    :catch_0
    move-exception v1

    .line 2212
    .local v1, "e":Ljava/lang/NoClassDefFoundError;
    const-string v3, "VoiceNoteService"

    const-string v4, "getKNOXVersion NoClassDefFoundError"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2213
    .end local v1    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v1

    .line 2214
    .local v1, "e":Ljava/lang/NoSuchMethodError;
    const-string v3, "VoiceNoteService"

    const-string v4, "getKNOXVersion NoSuchMethodError"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getMediaRecorderState()I
    .locals 1

    .prologue
    .line 1903
    iget v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    return v0
.end method

.method public getNewFileName(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 640
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->getLocationTag()Ljava/lang/String;

    move-result-object v1

    .line 641
    .local v1, "locationtag":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->getTimeTag()Ljava/lang/String;

    move-result-object v2

    .line 642
    .local v2, "timetag":Ljava/lang/String;
    const/4 v0, 0x0

    .line 643
    .local v0, "fileName":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->createNewFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 644
    if-eqz v1, :cond_0

    .line 645
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 647
    :cond_0
    if-eqz v2, :cond_1

    .line 648
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 650
    :cond_1
    const-string v3, "storage"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 651
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_sd"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 653
    :cond_2
    return-object v0
.end method

.method public getPlayerDuration()I
    .locals 1

    .prologue
    .line 1851
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->getDuration()I

    move-result v0

    return v0
.end method

.method public getRecDuration()J
    .locals 2

    .prologue
    .line 1666
    iget v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getRecDurationinLockscreen()J
    .locals 2

    .prologue
    .line 1670
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecordingTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    long-to-int v0, v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getRecordingTime()J
    .locals 10

    .prologue
    const/16 v7, 0x3e80

    .line 2474
    const-wide/16 v4, 0x0

    .line 2475
    .local v4, "time":J
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2476
    invoke-direct {p0, v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRealFreeSize(I)J

    move-result-wide v2

    .line 2477
    .local v2, "realFreeSize":J
    invoke-static {v2, v3, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getRemainingMmsSize(JI)J

    move-result-wide v0

    .line 2479
    .local v0, "limitedFileSize":J
    const/4 v6, 0x1

    invoke-static {v0, v1, v6}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getByte(JZ)J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecDuration()J

    move-result-wide v8

    sub-long v4, v6, v8

    .line 2486
    .end local v0    # "limitedFileSize":J
    .end local v2    # "realFreeSize":J
    :goto_0
    long-to-int v6, v4

    sput v6, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecordDuration:I

    .line 2487
    return-wide v4

    .line 2481
    :cond_0
    const-string v6, "record_mode"

    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_1

    .line 2482
    const-wide/32 v6, 0x494a8

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecDuration()J

    move-result-wide v8

    sub-long v4, v6, v8

    goto :goto_0

    .line 2484
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecDuration()J

    move-result-wide v4

    goto :goto_0
.end method

.method public getRepeatTime()[I
    .locals 3

    .prologue
    .line 1987
    const/4 v1, 0x2

    new-array v0, v1, [I

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatStartTime:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatEndTime:I

    aput v2, v0, v1

    .line 1988
    .local v0, "result":[I
    return-object v0
.end method

.method public getSTTState()Z
    .locals 1

    .prologue
    .line 866
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTurnonSTT:Z

    return v0
.end method

.method public getSavedID()J
    .locals 4

    .prologue
    .line 1764
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSavePath:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 1765
    const-string v2, "VoiceNoteService"

    const-string v3, "Save file name is null, can\'t play sound - file name is not saved because of mediarecorder state"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 1767
    const-wide/16 v0, -0x1

    .line 1770
    :goto_0
    return-wide v0

    .line 1769
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSavePath:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getIDFromFilePath(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    .line 1770
    .local v0, "currentSavedID":J
    goto :goto_0
.end method

.method public getSelectedFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1712
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mSelectedFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getStartVoiceLabelAfterRecord()Z
    .locals 1

    .prologue
    .line 2532
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStartVoicelabelAfterRecord:Z

    return v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 1720
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->getState()I

    move-result v0

    return v0
.end method

.method public getTrimTime()[I
    .locals 3

    .prologue
    .line 1992
    const/4 v1, 0x2

    new-array v0, v1, [I

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimStartTime:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimEndTime:I

    aput v2, v0, v1

    .line 1993
    .local v0, "result":[I
    return-object v0
.end method

.method public hideNotification()Z
    .locals 2

    .prologue
    .line 1887
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mEventHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1888
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mEventHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRemoteViewDisappear:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1889
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    if-nez v0, :cond_1

    .line 1890
    const/4 v0, 0x0

    .line 1892
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->hideNotification()Z

    move-result v0

    goto :goto_0
.end method

.method public declared-synchronized initRecording(Ljava/lang/String;J)Z
    .locals 10
    .param p1, "mimetype"    # Ljava/lang/String;
    .param p2, "sizeMMS"    # J

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 294
    monitor-enter p0

    :try_start_0
    const-string v4, "VoiceNoteService"

    const-string v5, "initRecording"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->stopPlay()V

    .line 297
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 298
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->unregisterPlayerReceiver()V

    .line 299
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->unregisterMotionListener()V

    .line 300
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->releaseMediaRecorder()Z

    .line 301
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNMemoData:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->initBookmark()V

    .line 302
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNMemoData:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->initMemoData()V

    .line 304
    new-instance v4, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;

    invoke-direct {v4, p1, p2, p3}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;-><init>(Ljava/lang/String;J)V

    iput-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioFormat:Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;

    .line 306
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioFormat:Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;

    invoke-direct {p0, v4, p2, p3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->prepareRecording(Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;J)Z

    move-result v4

    if-nez v4, :cond_0

    .line 307
    const-string v3, "VoiceNoteService"

    const-string v4, "initRecording:prepareRecording error"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    :goto_0
    monitor-exit p0

    return v2

    .line 311
    :cond_0
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getRootPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/.voice"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSaveHiddenPath:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 314
    :try_start_2
    const-string v4, "record_mode"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v1

    .line 315
    .local v1, "recordingMode":I
    new-instance v4, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    invoke-direct {v4}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    .line 316
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioFormat:Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->isAttachMode()Z

    move-result v4

    if-nez v4, :cond_1

    if-nez v1, :cond_2

    .line 317
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->setNoiseReduction(Z)V

    .line 319
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioFormat:Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->isAttachMode()Z

    move-result v4

    if-nez v4, :cond_4

    if-eq v1, v3, :cond_3

    const/4 v4, 0x2

    if-ne v1, v4, :cond_4

    .line 320
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    invoke-virtual {v4, v1}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->setBeamforming(I)V

    .line 322
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSaveHiddenPath:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioFormat:Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;

    const/16 v7, 0x3e80

    invoke-direct {p0, v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRealFreeSize(I)J

    move-result-wide v8

    invoke-virtual {v4, v5, v6, v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->prepare(Ljava/lang/String;Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;J)V

    .line 323
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    invoke-virtual {v4, p0}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setOnInfoListener(Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 336
    const/16 v2, 0x3e9

    :try_start_3
    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setState(I)V

    .line 338
    const-string v2, "VoiceNoteService"

    const-string v4, "initRecording X"

    invoke-static {v2, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 339
    goto :goto_0

    .line 324
    .end local v1    # "recordingMode":I
    :catch_0
    move-exception v0

    .line 325
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v3, "VoiceNoteService"

    const-string v4, "cancel recording while init recording by Illegal State Exception Occured"

    invoke-static {v3, v4, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 326
    const-string v3, "VoiceNoteService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNConsts$MediaRecorderState;->getStateString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", while trying to Prepared State"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->cancelRecording()Z

    .line 328
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 294
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 330
    :catch_1
    move-exception v0

    .line 331
    .local v0, "e":Ljava/io/IOException;
    :try_start_4
    const-string v3, "VoiceNoteService"

    const-string v4, "cancel recording while init recording by Error occured while MediaRecorder Prepare()"

    invoke-static {v3, v4, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->cancelRecording()Z

    .line 333
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method public isKNOXAvailable()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2222
    const-string v4, "VoiceNoteService"

    const-string v5, "isKNOXAvailable()"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2223
    const-string v4, "2.0"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXVersion()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2224
    const-string v4, "VoiceNoteService"

    const-string v5, "isKNOXAvailable() 2.0"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secV(Ljava/lang/String;Ljava/lang/String;)V

    .line 2227
    :try_start_0
    const-string v4, "persona"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PersonaManager;

    .line 2228
    .local v1, "mPersona":Landroid/os/PersonaManager;
    invoke-virtual {v1}, Landroid/os/PersonaManager;->getPersonas()Ljava/util/List;

    move-result-object v2

    .line 2229
    .local v2, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 2230
    const-string v4, "VoiceNoteService"

    const-string v5, "isKNOXAvailable() true"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2231
    const/4 v3, 0x1

    .line 2246
    .end local v1    # "mPersona":Landroid/os/PersonaManager;
    .end local v2    # "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    :cond_0
    :goto_0
    return v3

    .line 2233
    .restart local v1    # "mPersona":Landroid/os/PersonaManager;
    .restart local v2    # "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    :cond_1
    const-string v4, "VoiceNoteService"

    const-string v5, "isKNOXAvailable() false"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 2236
    .end local v1    # "mPersona":Landroid/os/PersonaManager;
    .end local v2    # "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    :catch_0
    move-exception v0

    .line 2237
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    const-string v4, "VoiceNoteService"

    const-string v5, "isKNOXAvailable NoClassDefFoundError"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2238
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 2239
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v4, "VoiceNoteService"

    const-string v5, "isKNOXAvailable NoSuchMethodError"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2243
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :cond_2
    const-string v4, "VoiceNoteService"

    const-string v5, "isKNOXAvailable() 1.0"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2245
    sget-object v4, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-eqz v4, :cond_0

    .line 2246
    sget-object v3, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    invoke-virtual {v3}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->isKNOXFileRelayAvailable()Z

    move-result v3

    goto :goto_0
.end method

.method public isLockScreen()Z
    .locals 3

    .prologue
    .line 2491
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPowerManager:Landroid/os/PowerManager;

    if-nez v2, :cond_0

    .line 2492
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPowerManager:Landroid/os/PowerManager;

    .line 2494
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-nez v2, :cond_1

    .line 2495
    const-string v2, "keyguard"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 2497
    :cond_1
    const/4 v1, 0x0

    .line 2498
    .local v1, "isScreenOn":Z
    const/4 v0, 0x0

    .line 2499
    .local v0, "isKeyguardLock":Z
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPowerManager:Landroid/os/PowerManager;

    if-eqz v2, :cond_2

    .line 2500
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    .line 2502
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-eqz v2, :cond_3

    .line 2503
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v2}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    .line 2505
    :cond_3
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isOpenStatusBar()Z
    .locals 1

    .prologue
    .line 1514
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsOpenStatusBar:Z

    return v0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 1837
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    if-nez v0, :cond_0

    .line 1838
    const/4 v0, 0x0

    .line 1840
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->isPaused()Z

    move-result v0

    goto :goto_0
.end method

.method public isPausedForaWhile()Z
    .locals 1

    .prologue
    .line 938
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsPausedForaWhile:Z

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 1824
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    if-nez v0, :cond_0

    .line 1825
    const/4 v0, 0x0

    .line 1827
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->isPlaying()Z

    move-result v0

    goto :goto_0
.end method

.method public isPrepared()Z
    .locals 1

    .prologue
    .line 1844
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    if-nez v0, :cond_0

    .line 1845
    const/4 v0, 0x0

    .line 1847
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->isPrepared()Z

    move-result v0

    goto :goto_0
.end method

.method public isSkipSilenceMode()Z
    .locals 1

    .prologue
    .line 1778
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->isSkipSilenceMode()Z

    move-result v0

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 275
    const-string v0, "VoiceNoteService"

    const-string v1, "onBind"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mBinder:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;

    return-object v0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "arg0"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v1, 0x0

    .line 622
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 623
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->seek(I)V

    .line 624
    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setSkipSilenceMode(Z)V

    .line 625
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    .line 626
    return-void
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 170
    const-string v1, "VoiceNoteService"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    new-instance v1, Landroid/os/RemoteCallbackList;

    invoke-direct {v1}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCallbacks:Landroid/os/RemoteCallbackList;

    .line 173
    new-instance v1, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$IVoiceNoteServiceStub;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Landroid/os/RemoteCallbackList;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mBinder:Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;

    .line 174
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    .line 176
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mEventHandler:Landroid/os/Handler;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNIntent;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiversForService(Z)V

    .line 178
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    .line 179
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNMemoData:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    .line 180
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    .line 181
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsCallRinging:Z

    .line 182
    new-instance v1, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    .line 183
    invoke-static {p0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 184
    invoke-static {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 185
    invoke-static {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 186
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 187
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 188
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 189
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 190
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 191
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 192
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 193
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 194
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 195
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 196
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 197
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 198
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 200
    invoke-static {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 201
    invoke-static {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 202
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->registerMediaButtonProcessListener()V

    .line 203
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->registerEasyModeReceiver()V

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsEasymode:Z

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->makeSTTfolder(Landroid/content/Context;)V

    .line 208
    :try_start_0
    new-instance v1, Lcom/sec/knox/containeragent/ContainerInstallerManager;

    new-instance v2, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$1;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    invoke-direct {v1, p0, v2}, Lcom/sec/knox/containeragent/ContainerInstallerManager;-><init>(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    sput-object v1, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isKNOXAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "2.0"

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXCallingUserId()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setExternalStorageEnable(I)V

    .line 227
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 228
    return-void

    .line 220
    :catch_0
    move-exception v0

    .line 221
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 232
    const-string v0, "VoiceNoteService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->release()V

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiversForService(Z)V

    .line 235
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 238
    invoke-static {}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->releaseService()V

    .line 240
    invoke-static {}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->releaseService()V

    .line 241
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;->releaseService()V

    .line 242
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNRecordingModeFragment;->releaseService()V

    .line 243
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->releaseService()V

    .line 244
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlButtonFragment;->releaseService()V

    .line 245
    invoke-static {}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->releaseService()V

    .line 246
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->releaseService()V

    .line 247
    invoke-static {}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->releaseService()V

    .line 248
    invoke-static {}, Lcom/sec/android/app/voicenote/common/activity/VNBackPlayDialogActivity;->releaseService()V

    .line 249
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->releaseService()V

    .line 250
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->releaseService()V

    .line 251
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->releaseLocationTag()V

    .line 252
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNSingleAttachFragment;->releaseService()V

    .line 253
    invoke-static {}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->releaseService()V

    .line 254
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimSaveDialogFragment;->releaseService()V

    .line 256
    invoke-static {}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->releaseService()V

    .line 257
    invoke-static {}, Lcom/sec/android/app/voicenote/library/subactivity/VNSttForThirdActivity;->releaseService()V

    .line 258
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->release()V

    .line 259
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    .line 260
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->unRegisterMediaButtonProcessListener()V

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mEasyModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 262
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->disconnectKnoxContainerManager()V

    .line 263
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->deleteSTTfolder()V

    .line 264
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 265
    return-void
.end method

.method public onInfo(Lcom/sec/android/secmediarecorder/SecMediaRecorder;II)V
    .locals 5
    .param p1, "mr"    # Lcom/sec/android/secmediarecorder/SecMediaRecorder;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v4, 0x0

    .line 1132
    sparse-switch p2, :sswitch_data_0

    .line 1178
    :cond_0
    :goto_0
    return-void

    .line 1134
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isScreenOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1137
    iput p3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    .line 1138
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getRecordingTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    long-to-int v1, v2

    sput v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecordDuration:I

    .line 1139
    div-int/lit16 v0, p3, 0x3e8

    .line 1140
    .local v0, "recordedDurationSec":I
    iget v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mDurationUpdated:I

    if-le v0, v1, :cond_0

    .line 1141
    iput v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mDurationUpdated:I

    .line 1142
    const/16 v1, 0x835

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendMessageCallback(I)V

    .line 1143
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isLockScreen()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1144
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sendUpdateAppWidgetOnBtnClick(Landroid/content/Context;)V

    goto :goto_0

    .line 1150
    .end local v0    # "recordedDurationSec":I
    :sswitch_1
    const-string v1, "VoiceNoteService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onInfo : MEDIA_RECORDER_INFO_MAX_DURATION_REACHED "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 1151
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->isNotificationShow()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1152
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecordingOnBackground()Ljava/lang/String;

    .line 1153
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_0

    .line 1155
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecording()Z

    goto :goto_0

    .line 1160
    :sswitch_2
    const-string v1, "VoiceNoteService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onInfo : MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 1162
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isMMSLimited:Z

    if-eqz v1, :cond_2

    .line 1163
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0080

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1167
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->isNotificationShow()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1168
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecordingOnBackground()Ljava/lang/String;

    .line 1169
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto/16 :goto_0

    .line 1165
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b00c6

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 1171
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecording()Z

    goto/16 :goto_0

    .line 1132
    :sswitch_data_0
    .sparse-switch
        0x320 -> :sswitch_1
        0x321 -> :sswitch_2
        0x385 -> :sswitch_0
    .end sparse-switch
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 269
    const-string v0, "VoiceNoteService"

    const-string v1, "onStartCommand"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const/4 v0, 0x2

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 281
    const-string v0, "VoiceNoteService"

    const-string v1, "onUnbind"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public pausePlay()V
    .locals 4

    .prologue
    .line 1786
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mEventHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRemoteViewDisappear:Ljava/lang/Runnable;

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1787
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->pausePlay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1788
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->refreshNotification()Z

    .line 1790
    :cond_0
    return-void
.end method

.method public declared-synchronized pauseRecording()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 448
    monitor-enter p0

    :try_start_0
    const-string v2, "VoiceNoteService"

    const-string v3, "pauseRecording"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    if-nez v2, :cond_0

    .line 451
    const-string v1, "VoiceNoteService"

    const-string v2, "pauseRecording - mMediaRecorder is null "

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469
    :goto_0
    monitor-exit p0

    return v0

    .line 455
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->pause()V

    .line 456
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->enableSystemSound()V

    .line 457
    const/16 v0, 0x3ec

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setState(I)V

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/4 v2, 0x2

    const-wide/16 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v4, v5, v3}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v2}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 461
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->getSTTState()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 462
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->pauseSTT()V

    .line 463
    const/16 v0, 0x83f

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendMessageCallback(I)V

    .line 466
    :cond_1
    iget v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    iget v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    const/4 v3, 0x0

    invoke-static {p0, v0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendState(Landroid/content/Context;III)V

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPauseEnoughHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    const-wide/32 v4, 0x493e0

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 469
    goto :goto_0

    .line 448
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public refreshFileName(J)Z
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 1758
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileName(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setSelectedFileName(Ljava/lang/String;)V

    .line 1759
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    .line 1760
    const/4 v0, 0x1

    return v0
.end method

.method public refreshNotification()Z
    .locals 1

    .prologue
    .line 1896
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isLockScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1897
    invoke-static {p0}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sendUpdateAppWidgetOnBtnClick(Landroid/content/Context;)V

    .line 1899
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->refreshNotification()Z

    move-result v0

    return v0
.end method

.method public registerMediaButtonEventReceiver(Ljava/lang/String;)V
    .locals 5
    .param p1, "packagename"    # Ljava/lang/String;

    .prologue
    .line 2652
    const-string v0, "VoiceNoteService"

    const-string v1, "RegisterMediaButtonProcessListener"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2653
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->prepareMediaSession()V

    .line 2654
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/4 v1, 0x3

    const-wide/16 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 2655
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 2656
    return-void
.end method

.method public registerMediaButtonProcessListener()V
    .locals 2

    .prologue
    .line 2077
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2078
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "Media_Button_Recieved"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2080
    new-instance v1, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$14;-><init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaButtonProcessReceiver:Landroid/content/BroadcastReceiver;

    .line 2181
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaButtonProcessReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2182
    return-void
.end method

.method registerMotionListener()V
    .locals 3

    .prologue
    .line 2002
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-nez v0, :cond_0

    .line 2003
    const-string v0, "motion_recognition"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/motion/MotionRecognitionManager;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    .line 2005
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    const/high16 v2, 0x20000

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/motion/MotionRecognitionManager;->registerListenerEvent(Lcom/samsung/android/motion/MRListener;I)V

    .line 2006
    return-void
.end method

.method public releaseMediaRecorder()Z
    .locals 3

    .prologue
    const/16 v2, 0x3e8

    .line 1115
    const-string v0, "VoiceNoteService"

    const-string v1, "releaseMediaRecorder"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    if-eqz v0, :cond_1

    .line 1119
    iget v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    if-eq v0, v2, :cond_0

    .line 1120
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->reset()V

    .line 1122
    :cond_0
    iput v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    .line 1123
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->release()V

    .line 1124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    .line 1127
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public resetPath()V
    .locals 1

    .prologue
    .line 1915
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->resetPath()V

    .line 1916
    return-void
.end method

.method public resetSaveCancelDialog()V
    .locals 1

    .prologue
    .line 1522
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCancelDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 1523
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mCancelDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 1526
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStopDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 1527
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStopDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 1529
    :cond_1
    return-void
.end method

.method public resumePlay()V
    .locals 3

    .prologue
    .line 1793
    const-string v0, "VoiceNoteService"

    const-string v1, "resumePlay in."

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1794
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallIdle(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1795
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00d6

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 1806
    :goto_0
    return-void

    .line 1797
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->requestAudioFocus()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1798
    const-string v0, "VoiceNoteService"

    const-string v1, "resumePlay out. fail by audio focus"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1802
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->registerMediaButtonEventReceiver(Ljava/lang/String;)V

    .line 1804
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->resumePlay()V

    .line 1805
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mEventHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRemoteViewDisappear:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public declared-synchronized resumeRecording()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 473
    monitor-enter p0

    :try_start_0
    const-string v1, "VoiceNoteService"

    const-string v2, "resumeRecording"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->prepareResumeRecording()Z

    move-result v1

    if-nez v1, :cond_0

    .line 476
    const-string v1, "VoiceNoteService"

    const-string v2, "resumeRecording:prepareResumeRecording error"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 502
    :goto_0
    monitor-exit p0

    return v0

    .line 479
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaSession:Landroid/media/session/MediaSession;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    if-eqz v1, :cond_1

    .line 480
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/4 v2, 0x3

    const-wide/16 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2, v4, v5, v3}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 481
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v2}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 483
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    if-nez v1, :cond_2

    .line 484
    const-string v1, "VoiceNoteService"

    const-string v2, "resumeRecording - mMediaRecorder is null "

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 473
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 487
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->resume()V

    .line 488
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->disableSystemSound()V

    .line 489
    const/16 v0, 0x3eb

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setState(I)V

    .line 491
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->getSTTState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 492
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->resumeSTT()V

    .line 493
    const/16 v0, 0x83e

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendMessageCallback(I)V

    .line 496
    :cond_3
    iget v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    iget v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendState(Landroid/content/Context;III)V

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPauseEnoughHandler:Landroid/os/Handler;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 499
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->is4PinHeadsetConnected()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 500
    const v0, 0x7f0b0102

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 502
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized saveRecording()Z
    .locals 1

    .prologue
    .line 537
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecording(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized saveRecording(Z)Z
    .locals 6
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 541
    monitor-enter p0

    :try_start_0
    const-string v3, "VoiceNoteService"

    const-string v4, "saveRecording"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    if-eqz v3, :cond_0

    .line 543
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->hideNotification()Z

    .line 544
    :cond_0
    iget v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    const/16 v4, 0x3eb

    if-eq v3, v4, :cond_2

    iget v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    const/16 v4, 0x3ea

    if-eq v3, v4, :cond_2

    iget v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    const/16 v4, 0x3ec

    if-eq v3, v4, :cond_2

    .line 547
    const-string v2, "VoiceNoteService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "saveRecording : Invalid MediaRecorder state : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNConsts$MediaRecorderState;->getStateString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617
    :cond_1
    :goto_0
    monitor-exit p0

    return v1

    .line 551
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->unregisterMediaButtonEventReceiver(Ljava/lang/String;)V

    .line 553
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    if-nez v3, :cond_3

    .line 554
    const-string v2, "VoiceNoteService"

    const-string v3, "mMediaRecorder is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 541
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 558
    :cond_3
    const/16 v3, 0x3e8

    :try_start_2
    iput v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    .line 560
    if-eqz p1, :cond_4

    .line 561
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 564
    :cond_4
    :try_start_3
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->stop()V
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 572
    :try_start_4
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->releaseMediaRecorder()Z

    .line 573
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->setNoiseReduction(Z)V

    .line 574
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->enableSystemSound()V

    .line 577
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveFile()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 582
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->isGateEnabled()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 583
    const-string v1, "GATE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<GATE-M> AUDIO_RECORDED: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSavePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " </GATE-M>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->noI(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    :cond_5
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->mFilename:Ljava/lang/String;

    .line 587
    const/4 v1, 0x0

    const-string v3, ""

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setFixedNewFileName(ZLjava/lang/String;)V

    .line 588
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveMemoData()V

    .line 589
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    sput v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecordDuration:I

    .line 590
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileSize:I

    .line 591
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileName:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mSelectedFileName:Ljava/lang/String;

    .line 592
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isMMSLimited:Z

    .line 594
    sget-boolean v1, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_CHECK_ATTVN_ENABLED:Z

    if-eqz v1, :cond_8

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFromVVM:Z

    if-eqz v1, :cond_8

    .line 595
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFromVVM:Z

    .line 600
    :goto_1
    const/16 v1, 0x7d1

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setState(I)V

    .line 601
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->turnOnSTT(Z)V

    .line 603
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    if-eqz v1, :cond_6

    .line 604
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->setLEDNotification(Z)V

    .line 607
    :cond_6
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->getSTTState()I

    move-result v1

    if-ne v1, v2, :cond_7

    .line 608
    invoke-static {}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->stopSTT()V

    .line 609
    const/16 v1, 0x83f

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendMessageCallback(I)V

    .line 612
    :cond_7
    iget v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    iget v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    const/4 v4, 0x0

    invoke-static {p0, v1, v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendState(Landroid/content/Context;III)V

    .line 613
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPauseEnoughHandler:Landroid/os/Handler;

    const/4 v3, -0x1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 614
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mEventHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRemoteViewDisappear:Ljava/lang/Runnable;

    const-wide/32 v4, 0x1d4c0

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 615
    const-string v1, "change_notification_resume"

    const/4 v3, 0x1

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 616
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;->isKeepListActivity:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v1, v2

    .line 617
    goto/16 :goto_0

    .line 565
    :catch_0
    move-exception v0

    .line 566
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_5
    const-string v2, "VoiceNoteService"

    const-string v3, "mediarecorder stop failed, IllegalStateException occurs"

    invoke-static {v2, v3, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 572
    :try_start_6
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->releaseMediaRecorder()Z

    .line 573
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->setNoiseReduction(Z)V

    .line 574
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->enableSystemSound()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 568
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 569
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_7
    const-string v2, "VoiceNoteService"

    const-string v3, "mediarecorder stop failed, RuntimeException occurs"

    invoke-static {v2, v3, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 572
    :try_start_8
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->releaseMediaRecorder()Z

    .line 573
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->setNoiseReduction(Z)V

    .line 574
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->enableSystemSound()V

    goto/16 :goto_0

    .line 572
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_1
    move-exception v1

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->releaseMediaRecorder()Z

    .line 573
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->setNoiseReduction(Z)V

    .line 574
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->enableSystemSound()V

    throw v1

    .line 598
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileName:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToastSaved(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1
.end method

.method public declared-synchronized saveRecordingOnBackground()Ljava/lang/String;
    .locals 2

    .prologue
    .line 629
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecording()Z

    move-result v0

    .line 631
    .local v0, "bRet":Z
    if-nez v0, :cond_0

    .line 632
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 633
    const/4 v1, 0x0

    .line 636
    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFileName:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 629
    .end local v0    # "bRet":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public seek(I)V
    .locals 1
    .param p1, "msec"    # I

    .prologue
    .line 1724
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->seek(I)V

    .line 1725
    return-void
.end method

.method public setFragmentControllerState(Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;)V
    .locals 0
    .param p1, "s"    # Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    .prologue
    .line 2520
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mFragmentControllerState:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController$state;

    .line 2521
    return-void
.end method

.method public setOnCompletionListener(Ljava/lang/Object;)V
    .locals 1
    .param p1, "callback"    # Ljava/lang/Object;

    .prologue
    .line 1919
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->setOnCompletionListener(Ljava/lang/Object;)V

    .line 1920
    return-void
.end method

.method public setPlaySpeed(I)V
    .locals 1
    .param p1, "speed"    # I

    .prologue
    .line 1820
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->setPlaySpeed(I)V

    .line 1821
    return-void
.end method

.method public setRepeatTime(II)V
    .locals 0
    .param p1, "startTime"    # I
    .param p2, "endTime"    # I

    .prologue
    .line 1982
    iput p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatStartTime:I

    .line 1983
    iput p2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatEndTime:I

    .line 1984
    return-void
.end method

.method public setSelectedFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "selectedFileName"    # Ljava/lang/String;

    .prologue
    .line 1716
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mSelectedFileName:Ljava/lang/String;

    .line 1717
    return-void
.end method

.method public setSkipSilenceMode(Z)V
    .locals 1
    .param p1, "onoff"    # Z

    .prologue
    .line 1782
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->setSkipSilenceMode(Z)V

    .line 1783
    return-void
.end method

.method public setStartVoiceLabelAfterRecord(Z)V
    .locals 0
    .param p1, "startVoiceLabelAfterRecord"    # Z

    .prologue
    .line 2528
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStartVoicelabelAfterRecord:Z

    .line 2529
    return-void
.end method

.method public setTrimTime(II)V
    .locals 0
    .param p1, "startTime"    # I
    .param p2, "endTime"    # I

    .prologue
    .line 1997
    iput p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimStartTime:I

    .line 1998
    iput p2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimEndTime:I

    .line 1999
    return-void
.end method

.method public showNotification()Z
    .locals 1

    .prologue
    .line 1883
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->showNotification()Z

    move-result v0

    return v0
.end method

.method public startPlay(JLjava/lang/Object;Z)Z
    .locals 9
    .param p1, "id"    # J
    .param p3, "callback"    # Ljava/lang/Object;
    .param p4, "directPlay"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v7, -0x1

    .line 1733
    if-eqz p4, :cond_0

    .line 1734
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->requestAudioFocus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1735
    const-string v1, "VoiceNoteService"

    const-string v2, "startPlay out. fail by audio focus"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1754
    :goto_0
    return v0

    .line 1740
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->registerMediaButtonEventReceiver(Ljava/lang/String;)V

    .line 1742
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p0

    move v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->startPlay(JLjava/lang/Object;Landroid/content/Context;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1743
    const-string v1, "VoiceNoteService"

    const-string v2, "startPlay out. failed"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1746
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->registerPlayerReceiver()V

    .line 1747
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->registerMotionListener()V

    .line 1748
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getFileName(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setSelectedFileName(Ljava/lang/String;)V

    .line 1749
    iput v7, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatStartTime:I

    .line 1750
    iput v7, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatEndTime:I

    .line 1751
    iput v7, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimStartTime:I

    .line 1752
    iput v7, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTrimEndTime:I

    .line 1753
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mEventHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRemoteViewDisappear:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1754
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized startPreRecording()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 418
    monitor-enter p0

    :try_start_0
    const-string v2, "VoiceNoteService"

    const-string v3, "startPreRecording"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    iget v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    const/16 v3, 0x3e9

    if-eq v2, v3, :cond_0

    .line 421
    const-string v2, "VoiceNoteService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startPreRecording : Invalid MediaRecorder state : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNConsts$MediaRecorderState;->getStateString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 444
    :goto_0
    monitor-exit p0

    return v1

    .line 425
    :cond_0
    :try_start_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.musicservicecommand"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 426
    .local v0, "stopMusicIntent":Landroid/content/Intent;
    const-string v1, "command"

    const-string v2, "pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 427
    const-string v1, "from"

    const-string v2, "com.sec.android.app.voicerecorder"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 428
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 430
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->getMaxAmplitude()I

    .line 431
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->start()V

    .line 433
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mDurationUpdated:I

    .line 434
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    sput v1, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecordDuration:I

    .line 436
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->disableSystemSound()V

    .line 437
    const/16 v1, 0x3ea

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setState(I)V

    .line 439
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    .line 440
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentUtcTime()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStartRecordingUtcTimeForLoging:Ljava/lang/String;

    .line 442
    iget v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    iget v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    const/4 v3, 0x0

    invoke-static {p0, v1, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendState(Landroid/content/Context;III)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444
    const/4 v1, 0x1

    goto :goto_0

    .line 418
    .end local v0    # "stopMusicIntent":Landroid/content/Intent;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized startRecording()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 358
    monitor-enter p0

    :try_start_0
    const-string v7, "VoiceNoteService"

    const-string v8, "startRecording"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    iget v7, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    const/16 v8, 0x3e9

    if-eq v7, v8, :cond_0

    .line 361
    const-string v6, "VoiceNoteService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "startRecording : Invalid MediaRecorder state : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    invoke-static {v8}, Lcom/sec/android/app/voicenote/common/util/VNConsts$MediaRecorderState;->getStateString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    :goto_0
    monitor-exit p0

    return v5

    .line 364
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->registerMediaButtonEventReceiver(Ljava/lang/String;)V

    .line 366
    new-instance v4, Landroid/content/Intent;

    const-string v7, "com.android.music.musicservicecommand"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 367
    .local v4, "stopMusicIntent":Landroid/content/Intent;
    const-string v7, "command"

    const-string v8, "pause"

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 368
    const-string v7, "from"

    const-string v8, "com.sec.android.app.voicerecorder"

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 369
    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 371
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->getMaxAmplitude()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 373
    :try_start_2
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorder:Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;

    invoke-virtual {v7}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->start()V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 389
    :try_start_3
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->isGateEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 390
    const-string v5, "GATE"

    const-string v7, "<GATE-M> AUDIO_RECORDING </GATE-M>"

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->noI(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string v7, "RECO"

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 396
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mDurationUpdated:I

    .line 397
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    sput v5, Lcom/sec/android/app/voicenote/common/util/VNSettings;->mRecordDuration:I

    .line 399
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->disableSystemSound()V

    .line 400
    const/16 v5, 0x3eb

    invoke-direct {p0, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setState(I)V

    .line 402
    const-string v5, "record_mode"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    const/4 v7, 0x3

    if-ne v5, v7, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioFormat:Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->getAudioEncoder()I

    move-result v5

    if-eq v5, v6, :cond_2

    .line 404
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->turnOnSTT(Z)V

    .line 407
    :cond_2
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentUtcTime()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mStartRecordingUtcTimeForLoging:Ljava/lang/String;

    .line 408
    iget v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I

    iget v7, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRecordedDurationMSec:I

    const/4 v8, 0x0

    invoke-static {p0, v5, v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendState(Landroid/content/Context;III)V

    .line 410
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->is4PinHeadsetConnected()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 411
    const v5, 0x7f0b0102

    const/4 v7, 0x0

    invoke-static {p0, v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 413
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mEventHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRemoteViewDisappear:Ljava/lang/Runnable;

    invoke-virtual {v5, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    move v5, v6

    .line 414
    goto/16 :goto_0

    .line 374
    :catch_0
    move-exception v0

    .line 375
    .local v0, "e":Ljava/lang/IllegalStateException;
    const v6, 0x7f0b00fd

    const/4 v7, 0x0

    invoke-static {p0, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 376
    const/16 v6, 0x3e8

    invoke-direct {p0, v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setState(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 358
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    .end local v4    # "stopMusicIntent":Landroid/content/Intent;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 378
    .restart local v4    # "stopMusicIntent":Landroid/content/Intent;
    :catch_1
    move-exception v2

    .line 379
    .local v2, "re":Ljava/lang/RuntimeException;
    :try_start_4
    const-string v6, "enterprise_policy"

    invoke-virtual {p0, v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 380
    .local v1, "mEDM":Landroid/app/enterprise/EnterpriseDeviceManager;
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v3

    .line 381
    .local v3, "rp":Landroid/app/enterprise/RestrictionPolicy;
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/app/enterprise/RestrictionPolicy;->isMicrophoneEnabled(Z)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 382
    const v6, 0x7f0b00fd

    const/4 v7, 0x0

    invoke-static {p0, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 384
    :cond_4
    const/16 v6, 0x3e8

    invoke-direct {p0, v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setState(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method public stopPlay()V
    .locals 4

    .prologue
    .line 1809
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->unregisterMediaButtonEventReceiver(Ljava/lang/String;)V

    .line 1811
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->pausePlay()Z

    .line 1812
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->stopPlay()V

    .line 1813
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->unregisterPlayerReceiver()V

    .line 1814
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->unregisterMotionListener()V

    .line 1815
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 1816
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mEventHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRemoteViewDisappear:Ljava/lang/Runnable;

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1817
    return-void
.end method

.method public switchSkipSilenceMode()V
    .locals 1

    .prologue
    .line 1774
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->switchSkipSilenceMode()V

    .line 1775
    return-void
.end method

.method public turnOnSTT(Z)V
    .locals 1
    .param p1, "on"    # Z

    .prologue
    .line 857
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTurnonSTT:Z

    .line 859
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mTurnonSTT:Z

    if-eqz v0, :cond_0

    .line 860
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->startSTT(Landroid/content/Context;Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 861
    const/16 v0, 0x83e

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendMessageCallback(I)V

    .line 863
    :cond_0
    return-void
.end method

.method public unRegisterMediaButtonProcessListener()V
    .locals 1

    .prologue
    .line 2185
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaButtonProcessReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 2186
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaButtonProcessReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2187
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaButtonProcessReceiver:Landroid/content/BroadcastReceiver;

    .line 2189
    :cond_0
    return-void
.end method

.method public unregisterMediaButtonEventReceiver(Ljava/lang/String;)V
    .locals 5
    .param p1, "packagename"    # Ljava/lang/String;

    .prologue
    .line 2659
    const-string v0, "VoiceNoteService"

    const-string v1, "unRegisterMediaButtonProcessListener"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2660
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaSession:Landroid/media/session/MediaSession;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    if-eqz v0, :cond_0

    .line 2661
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 2662
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 2663
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaSession:Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/session/MediaSession;->release()V

    .line 2665
    :cond_0
    return-void
.end method

.method unregisterMotionListener()V
    .locals 2

    .prologue
    .line 2009
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-eqz v0, :cond_0

    .line 2010
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/motion/MotionRecognitionManager;->unregisterListener(Lcom/samsung/android/motion/MRListener;)V

    .line 2012
    :cond_0
    return-void
.end method
