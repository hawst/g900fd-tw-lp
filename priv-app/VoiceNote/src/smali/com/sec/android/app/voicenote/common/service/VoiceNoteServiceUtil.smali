.class public Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;
.super Ljava/lang/Object;
.source "VoiceNoteServiceUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil$ServiceBinder;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VoiceNoteServiceUtil"

.field private static mConnectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil$ServiceBinder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;->mConnectionMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    return-void
.end method

.method public static bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "callback"    # Landroid/content/ServiceConnection;

    .prologue
    const/4 v1, 0x0

    .line 38
    const-string v2, "VoiceNoteServiceUtil"

    const-string v3, "bindToService"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 42
    new-instance v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil$ServiceBinder;

    invoke-direct {v0, p1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil$ServiceBinder;-><init>(Landroid/content/ServiceConnection;)V

    .line 44
    .local v0, "sb":Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil$ServiceBinder;
    sget-object v2, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;->mConnectionMap:Ljava/util/HashMap;

    invoke-virtual {v2, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-class v3, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2, v0, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    .line 47
    .end local v0    # "sb":Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil$ServiceBinder;
    :cond_0
    return v1
.end method

.method public static getnumBindingActivity()I
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;->mConnectionMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public static unbindFromService(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    const-string v1, "VoiceNoteServiceUtil"

    const-string v2, "unbindFromService"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    sget-object v1, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;->mConnectionMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil$ServiceBinder;

    .line 56
    .local v0, "sb":Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil$ServiceBinder;
    if-nez v0, :cond_0

    .line 60
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0
.end method
