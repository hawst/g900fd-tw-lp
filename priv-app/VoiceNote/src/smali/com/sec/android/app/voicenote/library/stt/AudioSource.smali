.class public Lcom/sec/android/app/voicenote/library/stt/AudioSource;
.super Lcom/nuance/dragon/toolkit/audio/sources/SingleSinkSource;
.source "AudioSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/nuance/dragon/toolkit/audio/sources/SingleSinkSource",
        "<",
        "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
        ">;"
    }
.end annotation


# static fields
.field static final RECENT_BUFFER_SIZE:I = 0x64

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAudioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

.field private final mBuffer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;"
        }
    .end annotation
.end field

.field private final mBufferHistory:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;"
        }
    .end annotation
.end field

.field private final mBufferToRead:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;"
        }
    .end annotation
.end field

.field private mChunkTimeStamp:J

.field private mClosed:Z

.field private mDone:Z

.field private mEnableDRC:Z

.field private mEnableNS:Z

.field private mListener:Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;

.field private final mMainThreadHandler:Landroid/os/Handler;

.field private mStarted:Z

.field private mStopped:Z

.field private mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

.field private mWorkerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

.field private mWorkerThreadHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/nuance/dragon/toolkit/audio/AudioType;JZ)V
    .locals 2
    .param p1, "audioType"    # Lcom/nuance/dragon/toolkit/audio/AudioType;
    .param p2, "recordingStartTime"    # J
    .param p4, "bEnableDRC"    # Z

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Lcom/nuance/dragon/toolkit/audio/sources/SingleSinkSource;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    .line 56
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBuffer:Ljava/util/List;

    .line 57
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBufferToRead:Ljava/util/List;

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mAudioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mClosed:Z

    .line 60
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mMainThreadHandler:Landroid/os/Handler;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mMainThreadHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    .line 62
    iput-wide p2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mChunkTimeStamp:J

    .line 63
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBufferHistory:Ljava/util/List;

    .line 64
    iput-boolean p4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mEnableDRC:Z

    .line 65
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mEnableNS:Z

    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->initializeVoiceEngine()Z

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mListener:Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/voicenote/library/stt/AudioSource;Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;)Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource;
    .param p1, "x1"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mListener:Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBuffer:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->notifyChunksAvailable()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->stopRecordingInternal()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mStarted:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/voicenote/library/stt/AudioSource;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mStarted:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mClosed:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/voicenote/library/stt/AudioSource;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mClosed:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->notifySourceClosed()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)Lcom/nuance/dragon/toolkit/util/WorkerThread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/voicenote/library/stt/AudioSource;Lcom/nuance/dragon/toolkit/util/WorkerThread;)Lcom/nuance/dragon/toolkit/util/WorkerThread;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource;
    .param p1, "x1"    # Lcom/nuance/dragon/toolkit/util/WorkerThread;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    return-object p1
.end method

.method private cleanup()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 237
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mDone:Z

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/util/WorkerThread;->stop()V

    .line 241
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThreadHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThreadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 246
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThreadHandler:Landroid/os/Handler;

    .line 248
    :cond_1
    return-void
.end method

.method private initializeVoiceEngine()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-static {}, Lcom/samsung/voiceshell/VoiceEngineWrapper;->getInstance()Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    .line 75
    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mEnableNS:Z

    if-eqz v2, :cond_0

    .line 77
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v2}, Lcom/samsung/voiceshell/VoiceEngine;->initializeNS()I

    move-result v0

    .line 78
    .local v0, "ret":I
    if-eqz v0, :cond_0

    .line 80
    sget-object v2, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SamsungAudioSource voiceEngine.initializeNS returned "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .end local v0    # "ret":I
    :goto_0
    return v1

    .line 84
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mEnableDRC:Z

    if-eqz v2, :cond_1

    .line 85
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v2}, Lcom/samsung/voiceshell/VoiceEngine;->initializeDRC()I

    move-result v0

    .line 86
    .restart local v0    # "ret":I
    if-eqz v0, :cond_1

    .line 87
    sget-object v2, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SamsungAudioSource voiceEngine.initializeDRC returned "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 92
    .end local v0    # "ret":I
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private processDRC([SI)V
    .locals 4
    .param p1, "pcmBuffer"    # [S
    .param p2, "bufferLen"    # I

    .prologue
    .line 104
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/voiceshell/VoiceEngine;->processDRC([SI)I

    move-result v0

    .line 105
    .local v0, "ret":I
    if-eqz v0, :cond_0

    .line 106
    sget-object v1, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processDRC returned "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_0
    return-void
.end method

.method private processNSFrame([SI)V
    .locals 2
    .param p1, "pcmBuffer"    # [S
    .param p2, "bufferLen"    # I

    .prologue
    .line 98
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/voiceshell/VoiceEngine;->processNSFrame([SI)I

    move-result v0

    .line 101
    .local v0, "ret":I
    return-void
.end method

.method private readBuffer()Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBufferToRead:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBufferToRead:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    .line 319
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private stopRecordingInternal()V
    .locals 2

    .prologue
    .line 206
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mDone:Z

    if-eqz v1, :cond_0

    .line 219
    :goto_0
    return-void

    .line 211
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->readBuffer()Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    move-result-object v0

    .line 212
    .local v0, "buf":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    if-eqz v0, :cond_1

    .line 213
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->handleNewAudio(Lcom/nuance/dragon/toolkit/audio/AudioChunk;)V

    .line 216
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->cleanup()V

    .line 218
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->handleSourceClosed()V

    goto :goto_0
.end method


# virtual methods
.method public addAudioBuffer(Lcom/nuance/dragon/toolkit/audio/AudioChunk;)V
    .locals 1
    .param p1, "chunk"    # Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    .prologue
    .line 286
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mStarted:Z

    if-nez v0, :cond_0

    .line 287
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mStarted:Z

    .line 288
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->handleStarted()V

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBuffer:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->notifyChunksAvailable()V

    .line 298
    return-void
.end method

.method public addAudioBuffer([S)V
    .locals 6
    .param p1, "buf"    # [S

    .prologue
    .line 301
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mEnableNS:Z

    if-eqz v1, :cond_0

    .line 302
    array-length v1, p1

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->processNSFrame([SI)V

    .line 305
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mEnableDRC:Z

    if-eqz v1, :cond_1

    .line 306
    array-length v1, p1

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->processDRC([SI)V

    .line 309
    :cond_1
    new-instance v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    sget-object v1, Lcom/nuance/dragon/toolkit/audio/AudioType;->PCM_16k:Lcom/nuance/dragon/toolkit/audio/AudioType;

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mChunkTimeStamp:J

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/nuance/dragon/toolkit/audio/AudioChunk;-><init>(Lcom/nuance/dragon/toolkit/audio/AudioType;[SJ)V

    .line 310
    .local v0, "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->addAudioBuffer(Lcom/nuance/dragon/toolkit/audio/AudioChunk;)V

    .line 311
    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mChunkTimeStamp:J

    iget v1, v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;->audioDuration:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mChunkTimeStamp:J

    .line 312
    return-void
.end method

.method public adjustBuffer(J)V
    .locals 7
    .param p1, "startTimeStamp"    # J

    .prologue
    .line 111
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBufferHistory:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 112
    .local v3, "nSize":I
    const/4 v2, -0x1

    .line 113
    .local v2, "nIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 114
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBufferHistory:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    .line 115
    .local v0, "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    iget-wide v4, v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;->audioTimestamp:J

    cmp-long v4, p1, v4

    if-nez v4, :cond_1

    .line 116
    add-int/lit8 v2, v1, -0x1

    .line 121
    .end local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    :cond_0
    sget-object v4, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nIndex in adjustBuffer: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    if-ltz v2, :cond_2

    .line 124
    add-int/lit8 v1, v3, -0x1

    :goto_1
    if-lt v1, v2, :cond_2

    .line 125
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBufferHistory:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    .line 126
    .restart local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBuffer:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 124
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 113
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129
    .end local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    :cond_2
    return-void
.end method

.method protected bridge synthetic getAudioChunk()Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->getAudioChunk()Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    move-result-object v0

    return-object v0
.end method

.method protected getAudioChunk()Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBuffer:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBuffer:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    .line 137
    .local v0, "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBufferHistory:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBufferHistory:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0x64

    if-le v1, v2, :cond_0

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBufferHistory:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 144
    .end local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAudioType()Lcom/nuance/dragon/toolkit/audio/AudioType;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mAudioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    return-object v0
.end method

.method public getChunksAvailable()I
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mBuffer:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method protected handleNewAudio(Lcom/nuance/dragon/toolkit/audio/AudioChunk;)V
    .locals 2
    .param p1, "audio"    # Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mMainThreadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/voicenote/library/stt/AudioSource$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/voicenote/library/stt/AudioSource$2;-><init>(Lcom/sec/android/app/voicenote/library/stt/AudioSource;Lcom/nuance/dragon/toolkit/audio/AudioChunk;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 184
    return-void
.end method

.method public handleSourceClosed()V
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->handleSourceClosed(Z)V

    .line 252
    return-void
.end method

.method protected handleSourceClosed(Z)V
    .locals 2
    .param p1, "success"    # Z

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mMainThreadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;-><init>(Lcom/sec/android/app/voicenote/library/stt/AudioSource;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 282
    return-void
.end method

.method protected handleStarted()V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mMainThreadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/voicenote/library/stt/AudioSource$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource$1;-><init>(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 171
    return-void
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mClosed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startRecording()V
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->startRecording(Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;)V

    .line 188
    return-void
.end method

.method public startRecording(Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;

    .prologue
    const/4 v2, 0x0

    .line 191
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mDone:Z

    .line 192
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mListener:Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;

    .line 193
    new-instance v0, Lcom/nuance/dragon/toolkit/util/WorkerThread;

    const-string v1, "com.nuance.dragon.toolkit.sample.SamsungAudioSource"

    invoke-direct {v0, v1}, Lcom/nuance/dragon/toolkit/util/WorkerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/util/WorkerThread;->start()V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/util/WorkerThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThreadHandler:Landroid/os/Handler;

    .line 197
    const-string v0, "Starting recording"

    invoke-static {p0, v0}, Lcom/nuance/dragon/toolkit/util/Logger;->debug(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mStarted:Z

    .line 201
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mStopped:Z

    .line 202
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mClosed:Z

    .line 203
    return-void
.end method

.method public stopRecording()J
    .locals 2

    .prologue
    .line 222
    const-string v0, "Stopping recording"

    invoke-static {p0, v0}, Lcom/nuance/dragon/toolkit/util/Logger;->debug(Ljava/lang/Object;Ljava/lang/String;)V

    .line 223
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mStarted:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mStopped:Z

    if-nez v0, :cond_0

    .line 224
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mStopped:Z

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThreadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/voicenote/library/stt/AudioSource$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource$3;-><init>(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 233
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mChunkTimeStamp:J

    return-wide v0
.end method
