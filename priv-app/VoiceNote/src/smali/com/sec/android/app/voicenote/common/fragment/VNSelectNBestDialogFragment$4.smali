.class Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;
.super Landroid/content/BroadcastReceiver;
.source "VNSelectNBestDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->registerBroadcastReceiverSip(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)V
    .locals 0

    .prologue
    .line 388
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v6, 0x50

    .line 391
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 392
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    const-string v3, "AxT9IME.isVisibleWindow"

    const/4 v4, 0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    # setter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->bShowKeyboard:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->access$202(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;Z)Z

    .line 393
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    # setter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mReceiveTime:J
    invoke-static {v2, v4, v5}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->access$302(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;J)J

    .line 395
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->bShowKeyboard:Z
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->access$200(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 396
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/Window;->setGravity(I)V

    .line 397
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 398
    .local v1, "params":Landroid/view/WindowManager$LayoutParams;
    const/4 v2, 0x0

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 399
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 418
    .end local v1    # "params":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    :goto_0
    return-void

    .line 402
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 403
    .local v0, "audiomanager":Landroid/media/AudioManager;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 405
    .restart local v1    # "params":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 406
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x30

    invoke-virtual {v2, v3}, Landroid/view/Window;->setGravity(I)V

    .line 407
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Landroid/widget/EditText;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->access$400(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Landroid/widget/ListView;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 408
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->yloc:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->access$500(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->access$100(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->access$400(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 415
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto :goto_0

    .line 411
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/Window;->setGravity(I)V

    .line 412
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$4;->this$0:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    # getter for: Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->yloc:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->access$500(Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)I

    move-result v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_1
.end method
