.class Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$11;
.super Landroid/content/BroadcastReceiver;
.source "VoiceNoteService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0

    .prologue
    .line 1955
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$11;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1958
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1959
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1960
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$11;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 1961
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$11;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 1963
    :cond_0
    return-void
.end method
