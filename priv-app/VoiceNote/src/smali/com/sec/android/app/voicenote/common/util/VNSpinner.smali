.class public Lcom/sec/android/app/voicenote/common/util/VNSpinner;
.super Landroid/widget/Spinner;
.source "VNSpinner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VNSpinner"


# instance fields
.field mCallback:Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mode"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;I)V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "mode"    # I

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;

    .line 19
    return-void
.end method


# virtual methods
.method public setCallback(Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;

    .line 39
    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;

    instance-of v0, v0, Lcom/sec/android/app/voicenote/common/util/VNActionModeListener;

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;

    invoke-interface {v0, p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;->onVNItemSelected(Landroid/widget/AdapterView;I)V

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->getSelectedItemPosition()I

    move-result p1

    .line 54
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 55
    return-void

    .line 48
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;

    invoke-interface {v0, p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNSpinner$onVNItemSelectedListener;->onVNItemSelected(Landroid/widget/AdapterView;I)V

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNSpinner;->getSelectedItemPosition()I

    move-result p1

    goto :goto_0
.end method
