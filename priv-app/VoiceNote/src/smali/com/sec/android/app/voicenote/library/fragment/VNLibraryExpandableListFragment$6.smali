.class Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$6;
.super Ljava/lang/Object;
.source "VNLibraryExpandableListFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;)V
    .locals 0

    .prologue
    .line 2072
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$6;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2076
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$6;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 2077
    .local v1, "activity":Landroid/app/Activity;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/app/Activity;->isResumed()Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v6, v1

    .line 2078
    check-cast v6, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getSearchItem()Landroid/view/MenuItem;

    move-result-object v5

    .local v5, "searchItem":Landroid/view/MenuItem;
    move-object v6, v1

    .line 2079
    check-cast v6, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getDeleteItem()Landroid/view/MenuItem;

    move-result-object v2

    .line 2080
    .local v2, "deleteItem":Landroid/view/MenuItem;
    check-cast v1, Lcom/sec/android/app/voicenote/main/VNMainActivity;

    .end local v1    # "activity":Landroid/app/Activity;
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getActionBarIsSearched()Z

    move-result v4

    .line 2081
    .local v4, "isSearched":Z
    if-eqz v5, :cond_1

    .line 2082
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$6;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$6;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;

    move-result-object v6

    invoke-interface {v6}, Landroid/widget/ExpandableListAdapter;->getGroupCount()I

    move-result v6

    if-nez v6, :cond_4

    :cond_0
    if-nez v4, :cond_4

    .line 2084
    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2085
    if-eqz v2, :cond_1

    .line 2086
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2095
    :cond_1
    :goto_0
    if-eqz v4, :cond_2

    .line 2096
    if-eqz v2, :cond_2

    .line 2097
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2102
    .end local v2    # "deleteItem":Landroid/view/MenuItem;
    .end local v4    # "isSearched":Z
    .end local v5    # "searchItem":Landroid/view/MenuItem;
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$6;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    .line 2103
    .local v0, "actionMode":Landroid/view/ActionMode;
    if-eqz v0, :cond_3

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment$6;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/library/fragment/VNLibraryExpandableListFragment;->isResumed()Z

    move-result v6

    if-nez v6, :cond_5

    .line 2111
    :cond_3
    :goto_1
    return-void

    .line 2089
    .end local v0    # "actionMode":Landroid/view/ActionMode;
    .restart local v2    # "deleteItem":Landroid/view/MenuItem;
    .restart local v4    # "isSearched":Z
    .restart local v5    # "searchItem":Landroid/view/MenuItem;
    :cond_4
    invoke-interface {v5, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2090
    if-eqz v2, :cond_1

    .line 2091
    invoke-interface {v2, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 2107
    .end local v2    # "deleteItem":Landroid/view/MenuItem;
    .end local v4    # "isSearched":Z
    .end local v5    # "searchItem":Landroid/view/MenuItem;
    .restart local v0    # "actionMode":Landroid/view/ActionMode;
    :cond_5
    :try_start_0
    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2108
    :catch_0
    move-exception v3

    .line 2109
    .local v3, "e":Ljava/lang/Exception;
    const-string v6, "VNLibraryExpandableListFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "refreshOptionsMenu()"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
