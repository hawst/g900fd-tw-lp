.class Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil$ServiceBinder;
.super Ljava/lang/Object;
.source "VoiceNoteServiceUtil.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ServiceBinder"
.end annotation


# instance fields
.field mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method constructor <init>(Landroid/content/ServiceConnection;)V
    .locals 0
    .param p1, "serviceConnection"    # Landroid/content/ServiceConnection;

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil$ServiceBinder;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 71
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "iBinder"    # Landroid/os/IBinder;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil$ServiceBinder;->mServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil$ServiceBinder;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-interface {v0, p1, p2}, Landroid/content/ServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 78
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil$ServiceBinder;->mServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteServiceUtil$ServiceBinder;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-interface {v0, p1}, Landroid/content/ServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V

    .line 85
    :cond_0
    return-void
.end method
