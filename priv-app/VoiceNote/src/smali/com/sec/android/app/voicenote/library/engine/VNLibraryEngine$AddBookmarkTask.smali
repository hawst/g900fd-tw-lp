.class Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;
.super Landroid/os/AsyncTask;
.source "VNLibraryEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddBookmarkTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field mPath:Ljava/lang/String;

.field mSleepTime:J

.field tempBookmark:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;Ljava/lang/String;)V
    .locals 2
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 544
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    .line 545
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 537
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->tempBookmark:Ljava/util/ArrayList;

    .line 538
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->mSleepTime:J

    .line 546
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->mPath:Ljava/lang/String;

    .line 547
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 535
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 9
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v8, 0x0

    .line 559
    const-string v4, "VNLibraryEngine"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AddBookmarkTask : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->mPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    sget-object v5, Lcom/sec/android/app/voicenote/common/util/M4aConsts;->FILE_LOCK:Ljava/lang/Object;

    monitor-enter v5

    .line 561
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->mPath:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->mPath:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isM4A(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 562
    new-instance v2, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->mPath:Ljava/lang/String;

    invoke-direct {v2, v4}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 563
    .local v2, "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v3

    .line 564
    .local v3, "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;

    invoke-direct {v0, v3}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V

    .line 566
    .local v0, "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->tempBookmark:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    .line 567
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->tempBookmark:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 568
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->tempBookmark:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->overwriteBookmarks(Ljava/util/List;)V

    .line 569
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    # getter for: Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mFragmentController:Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->access$100(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;)Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryFragmentController;->isBookmarkList()Z

    move-result v4

    if-nez v4, :cond_0

    .line 570
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->tempBookmark:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 572
    :cond_0
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->tempBookmark:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578
    .end local v0    # "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    .end local v2    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :cond_1
    :goto_0
    :try_start_1
    iget-wide v6, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->mSleepTime:J

    invoke-virtual {p0, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalMonitorStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587
    :goto_1
    :try_start_2
    monitor-exit v5

    return-object v8

    .line 574
    .restart local v0    # "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    .restart local v2    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .restart local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :cond_2
    const-string v4, "VNLibraryEngine"

    const-string v6, "saveBookmark : mBookmarks is null"

    invoke-static {v4, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 588
    .end local v0    # "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    .end local v2    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 579
    :catch_0
    move-exception v1

    .line 580
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 581
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v4

    goto :goto_1
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 551
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    # getter for: Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->access$000(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 552
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->tempBookmark:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->this$0:Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;

    # getter for: Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;->access$000(Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 554
    :cond_0
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 555
    return-void
.end method

.method public setClearTiming(J)V
    .locals 1
    .param p1, "sleepTime"    # J

    .prologue
    .line 541
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/library/engine/VNLibraryEngine$AddBookmarkTask;->mSleepTime:J

    .line 542
    return-void
.end method
