.class public Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;
.super Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;
.source "VNSingleSinkPipe.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<AudioChunkType:",
        "Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;",
        ">",
        "Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe",
        "<TAudioChunkType;TAudioChunkType;>;"
    }
.end annotation


# static fields
.field private static final BUFFER_SIZE:I = 0xa


# instance fields
.field private mAudioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

.field private final mBuffer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TAudioChunkType;>;"
        }
    .end annotation
.end field

.field private final mTempBuffer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TAudioChunkType;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    .local p0, "this":Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;, "Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe<TAudioChunkType;>;"
    invoke-direct {p0}, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;-><init>()V

    .line 40
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mBuffer:Ljava/util/List;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mTempBuffer:Ljava/util/ArrayList;

    .line 42
    sget-object v0, Lcom/nuance/dragon/toolkit/audio/AudioType;->UNKNOWN:Lcom/nuance/dragon/toolkit/audio/AudioType;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mAudioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    .line 43
    return-void
.end method


# virtual methods
.method protected chunksAvailable(Lcom/nuance/dragon/toolkit/audio/AudioSource;Lcom/nuance/dragon/toolkit/audio/AudioSink;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<TAudioChunkType;>;",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TAudioChunkType;>;)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, "this":Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;, "Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe<TAudioChunkType;>;"
    .local p1, "fromSource":Lcom/nuance/dragon/toolkit/audio/AudioSource;, "Lcom/nuance/dragon/toolkit/audio/AudioSource<TAudioChunkType;>;"
    .local p2, "internalSink":Lcom/nuance/dragon/toolkit/audio/AudioSink;, "Lcom/nuance/dragon/toolkit/audio/AudioSink<TAudioChunkType;>;"
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mTempBuffer:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Lcom/nuance/dragon/toolkit/audio/AudioSource;->getChunksAvailableForSink(Lcom/nuance/dragon/toolkit/audio/AudioSink;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 59
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mTempBuffer:Ljava/util/ArrayList;

    invoke-virtual {p1, p2, v2}, Lcom/nuance/dragon/toolkit/audio/AudioSource;->getAllAudioChunksForSink(Lcom/nuance/dragon/toolkit/audio/AudioSink;Ljava/util/List;)V

    .line 61
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mTempBuffer:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;

    .line 62
    .local v0, "chunk":Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;, "TAudioChunkType;"
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/16 v3, 0xa

    if-le v2, v3, :cond_0

    .line 65
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mBuffer:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 68
    .end local v0    # "chunk":Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;, "TAudioChunkType;"
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mTempBuffer:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->notifyChunksAvailable()V

    .line 71
    return-void
.end method

.method public connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<TAudioChunkType;>;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p0, "this":Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;, "Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe<TAudioChunkType;>;"
    .local p1, "source":Lcom/nuance/dragon/toolkit/audio/AudioSource;, "Lcom/nuance/dragon/toolkit/audio/AudioSource<TAudioChunkType;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 79
    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/audio/AudioSource;->getAudioType()Lcom/nuance/dragon/toolkit/audio/AudioType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mAudioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    .line 80
    invoke-super {p0, p1}, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    .line 81
    return-void
.end method

.method protected framesDropped(Lcom/nuance/dragon/toolkit/audio/AudioSource;Lcom/nuance/dragon/toolkit/audio/AudioSink;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<TAudioChunkType;>;",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TAudioChunkType;>;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p0, "this":Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;, "Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe<TAudioChunkType;>;"
    .local p1, "fromSource":Lcom/nuance/dragon/toolkit/audio/AudioSource;, "Lcom/nuance/dragon/toolkit/audio/AudioSource<TAudioChunkType;>;"
    .local p2, "internalSink":Lcom/nuance/dragon/toolkit/audio/AudioSink;, "Lcom/nuance/dragon/toolkit/audio/AudioSink<TAudioChunkType;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->notifyFramesDropped()V

    .line 86
    return-void
.end method

.method protected getAudioChunk()Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TAudioChunkType;"
        }
    .end annotation

    .prologue
    .line 47
    .local p0, "this":Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;, "Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe<TAudioChunkType;>;"
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mBuffer:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;

    .line 52
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAudioType()Lcom/nuance/dragon/toolkit/audio/AudioType;
    .locals 1

    .prologue
    .line 95
    .local p0, "this":Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;, "Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe<TAudioChunkType;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mAudioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    return-object v0
.end method

.method public getChunksAvailable()I
    .locals 1

    .prologue
    .line 105
    .local p0, "this":Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;, "Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe<TAudioChunkType;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->mBuffer:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 100
    .local p0, "this":Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;, "Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe<TAudioChunkType;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->isSourceActive()Z

    move-result v0

    return v0
.end method

.method protected sourceClosed(Lcom/nuance/dragon/toolkit/audio/AudioSource;Lcom/nuance/dragon/toolkit/audio/AudioSink;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<TAudioChunkType;>;",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TAudioChunkType;>;)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p0, "this":Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;, "Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe<TAudioChunkType;>;"
    .local p1, "fromSource":Lcom/nuance/dragon/toolkit/audio/AudioSource;, "Lcom/nuance/dragon/toolkit/audio/AudioSource<TAudioChunkType;>;"
    .local p2, "internalSink":Lcom/nuance/dragon/toolkit/audio/AudioSink;, "Lcom/nuance/dragon/toolkit/audio/AudioSink<TAudioChunkType;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSingleSinkPipe;->notifySourceClosed()V

    .line 91
    return-void
.end method
