.class Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;
.super Ljava/lang/Object;
.source "AudioConverter.java"

# interfaces
.implements Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEndOfSpeech()V
    .locals 4

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # operator++ for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mCountOfEndOfSpeech:I
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$108(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)I

    .line 121
    const-string v0, "AudioConverter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EPD onEndOfSpeech mCountOfEndOfSpeech:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mCountOfEndOfSpeech:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$100(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mAudioRecognizer:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$000(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->getRecentTimeStamp()J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mRecentTimeStamp:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$202(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;J)J

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mRecentTimeStamp:J
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$200(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mRecognizerSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$300(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mRecentTimeStamp:J
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$200(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->adjustBuffer(J)V

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mAudioRecognizer:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$000(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->processResult()V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mSpeechDetectListener:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$400(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mRecentTimeStamp:J
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$200(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)J

    move-result-wide v2

    # invokes: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->initializeRecognizer(Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;J)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$500(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;J)V

    .line 167
    return-void
.end method

.method public onStartOfSpeech()V
    .locals 2

    .prologue
    .line 113
    const-string v0, "AudioConverter"

    const-string v1, "Start of Recognition, onStartOfSpeech"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mAudioRecognizer:Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$000(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioRecognizer;->startRecognition()V

    .line 115
    return-void
.end method
