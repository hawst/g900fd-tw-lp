.class Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;
.super Ljava/lang/Object;
.source "VNEditSTTFragment.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->getDialogCallback()Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)V
    .locals 0

    .prologue
    .line 466
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized onDeleteWord()V
    .locals 6

    .prologue
    .line 518
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    if-nez v2, :cond_0

    .line 519
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "onDeleteWord: callback is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 538
    :goto_0
    monitor-exit p0

    return-void

    .line 522
    :cond_0
    :try_start_1
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDeleteWord select index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", text size : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 524
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "onDeleteWord: mSelectIndex is larger than mTextData size"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 518
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 527
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 528
    .local v1, "mTextDatasize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 529
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "before mTextData : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v2, v2, v5

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " - "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 531
    :cond_2
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "remove :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v2, v2, v5

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 534
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v1, :cond_3

    .line 535
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "after mTextData : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v2, v2, v5

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " - "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 537
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->updateAllWordOnScreen()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public onFinish()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1202(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onSTTEditPopup(Z)V

    .line 484
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mIsTouching:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$202(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Z)Z

    .line 485
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    const/4 v1, -0x1

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$902(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;I)I

    .line 486
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1300()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v0

    if-eqz v0, :cond_1

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1300()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->updateAllWordOnScreen()V

    .line 489
    :cond_1
    return-void
.end method

.method public declared-synchronized onResult(I)V
    .locals 3
    .param p1, "selectFromDialog"    # I

    .prologue
    .line 469
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    if-nez v0, :cond_0

    .line 470
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onResult1: callback is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 476
    :goto_0
    monitor-exit p0

    return-void

    .line 473
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/TextData;

    const/4 v1, 0x0

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/TextData;->swapText(II)V

    .line 474
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->updateAllWordOnScreen()V

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->saveData()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 469
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onResult(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "newLine"    # Z

    .prologue
    .line 494
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    if-nez v0, :cond_0

    .line 495
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onResult2: callback is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 514
    :goto_0
    monitor-exit p0

    return-void

    .line 498
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    .line 499
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onResult2: mTextData is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 494
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 502
    :cond_1
    :try_start_2
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 503
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$300()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResult2: data is empty selectid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I
    invoke-static {v2}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 508
    :goto_1
    if-eqz p2, :cond_2

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v1, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I
    invoke-static {v4}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v0, v0, v4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    .line 512
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->updateAllWordOnScreen()V

    .line 513
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mCallback:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$400(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v1

    invoke-interface {v0, v1, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onSTTStringChanged(ILjava/lang/String;Z)V

    goto/16 :goto_0

    .line 506
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mTextData:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$5;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mSelectIndex:I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$900(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v0, v0, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
