.class public Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;
.super Landroid/app/Activity;
.source "VNBackCancelDialogActivity.java"


# static fields
.field private static final CANCEL:I = 0x1

.field private static final TAG:Ljava/lang/String; = "VNBackCancelDialogActivity"

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# instance fields
.field private intentFilter:Landroid/content/IntentFilter;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mDialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mDialog:Landroid/app/Dialog;

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->intentFilter:Landroid/content/IntentFilter;

    return-void
.end method

.method static synthetic access$000()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-object v0
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 53
    sput-object p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 54
    return-void
.end method

.method public static releaseService()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 58
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 62
    const-string v1, "VNBackCancelDialogActivity"

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/Window;->requestFeature(I)Z

    .line 67
    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->showDialog(I)V

    .line 68
    const-string v1, "change_notification_resume"

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 69
    const-string v1, "change_notification_service"

    invoke-static {v1, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 70
    sget-object v1, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v1, :cond_0

    .line 71
    sget-object v1, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resetSaveCancelDialog()V

    .line 74
    :cond_0
    const-string v1, "search"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 75
    .local v0, "searchManager":Landroid/app/SearchManager;
    new-instance v1, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$1;-><init>(Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/SearchManager;->setOnDismissListener(Landroid/app/SearchManager$OnDismissListener;)V

    .line 81
    const-string v1, "VNBackCancelDialogActivity"

    const-string v2, "onCreate() END"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 86
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mDialog:Landroid/app/Dialog;

    .line 146
    :goto_0
    return-object v1

    .line 89
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 91
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    sget-object v1, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v1, :cond_1

    .line 92
    sget-object v1, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->hideNotification()Z

    .line 95
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 146
    const/4 v1, 0x0

    goto :goto_0

    .line 97
    :pswitch_0
    const v1, 0x7f0b0023

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b0022

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$2;-><init>(Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 108
    const v1, 0x7f0b00e0

    new-instance v2, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$3;-><init>(Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 124
    const v1, 0x7f0b0021

    new-instance v2, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$4;-><init>(Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 135
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mDialog:Landroid/app/Dialog;

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mDialog:Landroid/app/Dialog;

    new-instance v2, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$5;-><init>(Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d9

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 194
    const-string v0, "VNBackCancelDialogActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 201
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 204
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 205
    const-string v0, "VNBackCancelDialogActivity"

    const-string v1, "onDestroy() END"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 152
    const-string v0, "VNBackCancelDialogActivity"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 156
    sget-object v0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v0, :cond_0

    .line 157
    sget-object v0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->showNotification()Z

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 166
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 167
    const-string v0, "VNBackCancelDialogActivity"

    const-string v1, "onPause() END"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 172
    const-string v0, "VNBackCancelDialogActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->intentFilter:Landroid/content/IntentFilter;

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->intentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.app.voicenote.rec_save"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 177
    new-instance v0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$6;-><init>(Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->intentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 188
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 189
    const-string v0, "VNBackCancelDialogActivity"

    const-string v1, "onResume() END"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 210
    const-string v0, "VNBackCancelDialogActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTrimMemory() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    sparse-switch p1, :sswitch_data_0

    .line 224
    :sswitch_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 225
    return-void

    .line 211
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method
