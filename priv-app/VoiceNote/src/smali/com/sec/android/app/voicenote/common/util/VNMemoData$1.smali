.class Lcom/sec/android/app/voicenote/common/util/VNMemoData$1;
.super Ljava/lang/Object;
.source "VNMemoData.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/util/VNMemoData;->saveMemoData(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

.field final synthetic val$filepath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/util/VNMemoData;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData$1;->this$0:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    iput-object p2, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData$1;->val$filepath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 124
    sget-object v5, Lcom/sec/android/app/voicenote/common/util/M4aConsts;->FILE_LOCK:Ljava/lang/Object;

    monitor-enter v5

    .line 125
    :try_start_0
    new-instance v2, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData$1;->val$filepath:Ljava/lang/String;

    invoke-direct {v2, v4}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 126
    .local v2, "reader":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v1

    .line 128
    .local v1, "info":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData$1;->this$0:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->access$000(Lcom/sec/android/app/voicenote/common/util/VNMemoData;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData$1;->this$0:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->access$000(Lcom/sec/android/app/voicenote/common/util/VNMemoData;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 129
    if-eqz v1, :cond_0

    .line 130
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;

    invoke-direct {v0, v1}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V

    .line 131
    .local v0, "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData$1;->this$0:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->access$000(Lcom/sec/android/app/voicenote/common/util/VNMemoData;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->overwriteBookmarks(Ljava/util/List;)V

    .line 133
    .end local v0    # "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData$1;->this$0:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->access$000(Lcom/sec/android/app/voicenote/common/util/VNMemoData;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 137
    :cond_1
    new-instance v2, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    .end local v2    # "reader":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData$1;->val$filepath:Ljava/lang/String;

    invoke-direct {v2, v4}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 138
    .restart local v2    # "reader":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v1

    .line 150
    # getter for: Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->access$100()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData$1;->this$0:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->hasSTTdata()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 151
    new-instance v2, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    .end local v2    # "reader":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData$1;->val$filepath:Ljava/lang/String;

    invoke-direct {v2, v4}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 152
    .restart local v2    # "reader":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v1

    .line 153
    if-eqz v1, :cond_2

    .line 154
    new-instance v3, Lcom/sec/android/app/voicenote/common/util/SttdHelper;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNMemoData$1;->this$0:Lcom/sec/android/app/voicenote/common/util/VNMemoData;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->access$200(Lcom/sec/android/app/voicenote/common/util/VNMemoData;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;Landroid/content/Context;)V

    .line 155
    .local v3, "sttHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    # getter for: Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->access$100()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->overwriteSttd(Ljava/io/Serializable;)V

    .line 157
    .end local v3    # "sttHelper":Lcom/sec/android/app/voicenote/common/util/SttdHelper;
    :cond_2
    # getter for: Lcom/sec/android/app/voicenote/common/util/VNMemoData;->mSttData:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->access$100()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 159
    :cond_3
    monitor-exit v5

    .line 160
    return-void

    .line 159
    .end local v1    # "info":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .end local v2    # "reader":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method
