.class Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;
.super Ljava/lang/Object;
.source "VNEditSTTFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScrollDepender"
.end annotation


# instance fields
.field private mScrollNow:Z

.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)V
    .locals 1

    .prologue
    .line 874
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 876
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;->mScrollNow:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$1;

    .prologue
    .line 874
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)V

    return-void
.end method


# virtual methods
.method public isRunning()Z
    .locals 1

    .prologue
    .line 883
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;->mScrollNow:Z

    return v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 888
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$2000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/ScrollView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 889
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$2000(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Landroid/widget/ScrollView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->mScrollDepender:Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;->access$2100(Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 891
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;->mScrollNow:Z

    .line 892
    return-void
.end method

.method public setRunning()V
    .locals 1

    .prologue
    .line 879
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNEditSTTFragment$ScrollDepender;->mScrollNow:Z

    .line 880
    return-void
.end method
