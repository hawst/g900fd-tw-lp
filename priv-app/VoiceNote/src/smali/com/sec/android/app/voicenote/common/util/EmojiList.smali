.class public Lcom/sec/android/app/voicenote/common/util/EmojiList;
.super Ljava/lang/Object;
.source "EmojiList.java"


# static fields
.field private static unicodeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    .line 18
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x263a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 19
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f600"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f601"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 22
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f602"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 23
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f603"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f604"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f605"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f606"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f607"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f608"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f609"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f610"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f611"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f615"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f620"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f621"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f622"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f634"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f623"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f624"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f625"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f626"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f627"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f628"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f629"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f630"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f631"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f632"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f633"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f635"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f636"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f637"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f612"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f617"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f619"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f618"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f616"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f614"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f613"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f645"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f646"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f647"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f638"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f639"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f640"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f648"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f649"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f476"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f466"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f467"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f468"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f469"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f474"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f475"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f491"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f464"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f465"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f477"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f481"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f482"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f470"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f478"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f385"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f471"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f472"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f473"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f483"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f486"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f487"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f485"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f479"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f480"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f440"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f442"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f443"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f463"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f444"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f445"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2764"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f499"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f493"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f494"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f495"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f496"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f497"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f498"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x270a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x270c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x270b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x261d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f446"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f447"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f448"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f449"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f450"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f530"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f484"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f451"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f452"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f393"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f453"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x231a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f454"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f455"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f456"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 186
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f457"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f458"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f459"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f460"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f461"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f462"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4bc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f392"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 197
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 200
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 208
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 210
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f489"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ac"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f514"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f515"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 215
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 216
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 218
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f526"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 220
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 221
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4dc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4da"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 228
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4db"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f383"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f384"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f380"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f381"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f382"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f388"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f386"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f387"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 243
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f389"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 244
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f38a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f38d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f38b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 248
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4df"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 250
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x260e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4de"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 252
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2709"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 255
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 256
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ea"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4eb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ed"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 260
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ec"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 261
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ee"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ef"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 265
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 266
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ac"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ad"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2712"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x270f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 272
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4dd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4cf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 274
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4cd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4cc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ce"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 278
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2702"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ba"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4bb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 281
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4bd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 282
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4be"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4bf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 286
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 287
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4cb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 288
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 289
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 290
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 292
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ca"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 293
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 294
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 295
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26fa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 297
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 298
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 299
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 300
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 301
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ac"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 302
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 303
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 305
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ad"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 307
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ae"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f0cf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 313
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f004"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3af"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4fa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 316
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4fb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 317
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4fc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 319
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 322
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3bc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3bb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 325
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 326
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ba"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 329
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x303d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 332
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f415"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 333
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f436"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f429"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f408"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 336
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f431"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 337
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f400"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f401"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 339
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 340
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f439"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f422"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 342
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f407"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 343
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f430"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f413"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f414"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 346
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f423"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 347
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f424"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f425"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 349
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f426"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 350
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f411"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f410"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f403"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f402"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f404"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f434"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 359
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f417"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f416"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 361
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f437"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 362
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f438"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f427"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 367
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f418"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 368
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f428"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 369
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f412"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 370
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f435"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f406"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 372
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 373
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 374
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 375
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 376
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f433"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 378
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 379
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f420"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 381
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f421"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 382
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f419"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 383
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 385
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 386
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 387
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 388
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 389
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 390
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f432"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f409"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 392
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f378"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 394
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f37a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 395
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f37b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 396
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f377"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 397
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f379"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 398
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f376"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 399
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2615"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 400
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f375"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 401
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f37c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 402
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f374"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 403
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f368"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 404
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f367"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 405
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f366"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 406
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f369"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f370"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 409
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 410
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 411
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 412
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 413
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 414
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f373"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 415
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f354"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 416
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 417
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 418
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f355"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 419
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f356"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f357"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 421
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f364"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 422
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f363"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f371"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 424
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 425
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 426
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f359"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 427
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 428
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f372"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f365"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 431
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f362"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 432
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f361"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 433
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f358"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 434
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f360"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 435
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 436
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 437
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 438
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 439
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f344"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 441
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f345"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 442
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f346"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 443
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f347"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 444
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f348"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f349"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 446
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f350"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 447
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f351"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f352"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f353"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 450
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f330"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 452
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f331"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 453
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f332"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 454
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f333"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 455
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f334"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f335"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 457
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f337"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f338"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 459
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f339"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 460
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f340"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 461
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f341"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 462
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f342"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 463
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f343"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 464
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 465
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 466
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 468
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 469
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 470
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2600"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 471
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f308"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 472
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26c5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 473
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2601"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 474
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f301"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 475
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f302"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 476
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2614"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 477
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 478
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 479
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f300"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 480
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2744"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 481
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26c4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 482
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f319"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 483
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 484
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 485
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 486
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 487
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 488
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f311"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 489
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f312"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 490
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f313"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 491
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f314"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 492
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f315"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 493
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f316"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 494
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f317"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 495
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f318"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 496
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f391"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 497
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f304"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 498
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f305"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 499
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f307"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 500
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f306"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 501
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f303"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 502
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 503
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f309"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 504
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 505
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 506
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 507
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 508
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 509
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f310"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 512
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 513
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 514
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 515
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 516
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 517
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 518
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 519
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 520
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 521
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 522
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ea"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 523
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3eb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 524
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26ea"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 525
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26f2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 526
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ec"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 527
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3f0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 528
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ed"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 529
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f5fb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 530
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f5fc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 531
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f5fd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 532
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f5ff"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 533
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2693"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 534
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f488"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 535
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f527"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 536
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f528"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 537
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f529"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 538
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6bf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 539
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 540
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 541
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6bd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 542
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6be"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 543
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3bd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 544
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 545
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 546
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 547
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26be"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 548
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26f3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 549
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3be"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 550
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26bd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 551
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3bf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 552
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 553
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 554
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 555
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 556
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 557
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 558
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 560
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 561
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 562
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ca"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 563
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f682"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 564
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f683"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 565
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f684"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 566
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f685"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 567
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f686"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 568
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f687"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 569
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x24c2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f688"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 571
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 572
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 574
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 575
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 576
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 577
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f690"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 578
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f691"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 579
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f692"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 580
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f693"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 581
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f694"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 582
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f695"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 583
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f696"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 584
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f697"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 585
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f698"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 586
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f699"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 587
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 589
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 590
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 591
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 592
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 593
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 594
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 595
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 596
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 597
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f681"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 598
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2708"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 599
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 600
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 601
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 602
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 603
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26f5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 604
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 605
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 606
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 607
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 608
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 609
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 610
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f689"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 611
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f680"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 612
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 613
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 614
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26fd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 615
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f17f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 616
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 617
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 618
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 619
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 620
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2668"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 621
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 622
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 623
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 624
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f490"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 625
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f492"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 628
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 629
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f519"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 630
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 631
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 632
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 633
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23f3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 634
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x231b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 635
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23f0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 636
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2648"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 637
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2649"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 638
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 639
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 640
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 642
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 643
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 644
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2650"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 645
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2651"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 646
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2652"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 647
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2653"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 648
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26ce"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 649
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f531"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 650
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 651
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6bb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 652
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ae"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 653
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6af"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 654
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 655
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 656
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f170"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 657
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f171"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 658
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f18e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 659
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f17e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 660
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ae"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 661
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4af"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 662
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f520"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 663
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f521"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 664
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f522"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 665
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f523"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 666
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f524"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 667
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x27bf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 668
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 669
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 670
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 671
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 672
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 673
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ba"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 674
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6bc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 675
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x267f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 676
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x267b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 677
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ad"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 678
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 679
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26a0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 680
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f201"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 681
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 682
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26d4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 683
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f192"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 684
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f197"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 685
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f195"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 686
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f198"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 687
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f199"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 688
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f193"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 689
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f196"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 690
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f19a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 691
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f232"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 692
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f233"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 693
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f234"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 694
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f235"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 695
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f236"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 696
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f237"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 697
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f238"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 698
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f239"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 699
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f202"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 700
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f23a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 701
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f250"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 702
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f251"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 703
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x3299"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 707
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f21a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 708
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f22f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 709
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x3297"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 710
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b55"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 711
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x274c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 712
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x274e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 713
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2139"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 714
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 715
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2705"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 716
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2714"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 717
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f517"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 718
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2734"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 719
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2733"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 720
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2795"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 721
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2796"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 722
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2716"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 723
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2797"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 724
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 725
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 726
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 727
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 728
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f525"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 729
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 730
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 731
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 732
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 733
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 734
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f567"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 735
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f550"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 736
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 737
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f551"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 738
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 739
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f552"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 740
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 741
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f553"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 742
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 743
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f554"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 744
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f560"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 745
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f555"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 746
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f561"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 747
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f556"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 748
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f562"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 749
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f557"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 750
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f563"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 751
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f558"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 752
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f564"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 753
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f559"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 754
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f565"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 755
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 756
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f566"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 757
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2195"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 758
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b06"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 759
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2197"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 760
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x27a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 761
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2198"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 762
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b07"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 763
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2199"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 764
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b05"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 765
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2196"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 766
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2194"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 767
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2934"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 768
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2935"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 769
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23ea"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 770
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23eb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 771
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23ec"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 772
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23e9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 773
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25c0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 774
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25b6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 775
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f53d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 776
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f53c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 777
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2747"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 778
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2728"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 779
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f534"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 780
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f535"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 781
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 782
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 783
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f533"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 784
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f532"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 785
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b50"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 786
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 787
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f320"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 788
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 790
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25fd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 791
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25fe"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 792
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25fb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 793
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25fc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 794
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b1c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 795
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b1b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 796
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f538"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 797
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f539"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 798
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f536"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 799
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f537"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 800
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f53a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 801
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f53b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 802
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2754"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 803
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2753"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 804
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2755"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 805
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2757"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 807
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2049"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 808
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x3030"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 809
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x27b0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 810
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2660"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 811
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2665"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 812
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2663"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 813
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2666"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 814
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f194"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 815
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f511"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 816
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x21a9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 817
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f191"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 818
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 819
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f512"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 820
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f513"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 821
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x21aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 822
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f510"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 823
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2611"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 824
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f518"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 825
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 826
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f516"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 827
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 828
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f503"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 829
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f500"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 830
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f501"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 831
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f502"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 832
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f504"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 833
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 834
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f505"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 835
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f506"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 836
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f507"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 837
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f508"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 838
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f509"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 839
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 840
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getUnicodeList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    return-object v0
.end method
