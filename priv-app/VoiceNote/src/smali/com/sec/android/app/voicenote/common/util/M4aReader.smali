.class public Lcom/sec/android/app/voicenote/common/util/M4aReader;
.super Ljava/lang/Object;
.source "M4aReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;
    }
.end annotation


# instance fields
.field private final INVALID_NAME:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field buff:Ljava/nio/ByteBuffer;

.field channel:Ljava/nio/channels/FileChannel;

.field fstr:Ljava/io/FileInputStream;

.field private final path:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, "M4aReader"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->TAG:Ljava/lang/String;

    .line 34
    const-string v0, "Invalid_name"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->INVALID_NAME:Ljava/lang/String;

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->path:Ljava/lang/String;

    .line 42
    return-void
.end method

.method private arrToAscii([B)Ljava/lang/String;
    .locals 3
    .param p1, "data"    # [B

    .prologue
    .line 372
    if-nez p1, :cond_0

    .line 373
    const/4 v2, 0x0

    .line 382
    :goto_0
    return-object v2

    .line 374
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 375
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 376
    aget-byte v2, p1, v0

    if-gez v2, :cond_1

    .line 377
    const-string v2, "Invalid_name"

    goto :goto_0

    .line 380
    :cond_1
    aget-byte v2, p1, v0

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 375
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 382
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private findBOOK(Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V
    .locals 14
    .param p1, "inf"    # Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x8

    .line 147
    const-wide/16 v6, 0x0

    .line 148
    .local v6, "skip":J
    const-string v0, ""

    .line 151
    .local v0, "boxName":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->udtaPos:J

    add-long/2addr v8, v12

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 152
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 153
    :cond_0
    :goto_0
    const-string v3, "book"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 154
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 155
    .local v2, "readCount":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 158
    const/4 v3, 0x0

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasBookmarks:Z

    .line 159
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 160
    const/4 v3, 0x0

    iput v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileBookLength:I

    .line 217
    :cond_1
    :goto_1
    return-void

    .line 164
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 165
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    int-to-long v6, v3

    .line 166
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 168
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 169
    if-ltz v2, :cond_1

    .line 171
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 172
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->arrToAscii([B)Ljava/lang/String;

    move-result-object v0

    .line 175
    const-string v3, "book"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 176
    sub-long/2addr v6, v12

    .line 177
    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v6

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v10

    cmp-long v3, v8, v10

    if-gtz v3, :cond_3

    .line 178
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v6

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 212
    .end local v2    # "readCount":I
    :catch_0
    move-exception v1

    .line 213
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "M4aReader"

    const-string v8, "Error reading the file"

    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 215
    throw v1

    .line 180
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "readCount":I
    :cond_3
    :try_start_1
    const-string v3, "M4aReader"

    const-string v8, "Wrong skip value finding Bookmark! Returning from function"

    invoke-static {v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    const/4 v3, 0x0

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasBookmarks:Z

    goto :goto_1

    .line 187
    .end local v2    # "readCount":I
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v4

    .line 189
    .local v4, "savePosition":J
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    sub-long/2addr v8, v12

    iput-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->bookPos:J

    .line 191
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->bookPos:J

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 192
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 193
    .restart local v2    # "readCount":I
    if-ltz v2, :cond_1

    .line 195
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 196
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileBookLength:I

    .line 197
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 198
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 201
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    iput-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->bnumPos:J

    .line 202
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 203
    if-ltz v2, :cond_1

    .line 205
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 206
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileBnumLength:I

    .line 207
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 208
    const/4 v3, 0x1

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasBookmarks:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private findMEMO(Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V
    .locals 14
    .param p1, "inf"    # Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x8

    .line 220
    const-wide/16 v6, 0x0

    .line 223
    .local v6, "skip":J
    const-string v0, ""

    .line 226
    .local v0, "boxName":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->udtaPos:J

    add-long/2addr v8, v12

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 227
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 228
    :cond_0
    :goto_0
    const-string v3, "memo"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 229
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 230
    .local v2, "readCount":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 232
    const/4 v3, 0x0

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasMemo:Z

    .line 309
    :cond_1
    :goto_1
    return-void

    .line 236
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 237
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    int-to-long v6, v3

    .line 238
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 240
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 241
    if-ltz v2, :cond_1

    .line 243
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 244
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->arrToAscii([B)Ljava/lang/String;

    move-result-object v0

    .line 247
    const-string v3, "memo"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 248
    sub-long/2addr v6, v12

    .line 249
    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v6

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v10

    cmp-long v3, v8, v10

    if-gtz v3, :cond_3

    .line 250
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v6

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 304
    .end local v2    # "readCount":I
    :catch_0
    move-exception v1

    .line 305
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "M4aReader"

    const-string v8, "Error reading the file"

    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 307
    throw v1

    .line 252
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "readCount":I
    :cond_3
    :try_start_1
    const-string v3, "M4aReader"

    const-string v8, "Wrong skip value finding Memo! Possibly we are out of the file. Returning from function"

    invoke-static {v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    const/4 v3, 0x0

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasMemo:Z

    goto :goto_1

    .line 259
    .end local v2    # "readCount":I
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    sub-long/2addr v8, v12

    iput-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->memoPos:J

    .line 261
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v4

    .line 262
    .local v4, "save":J
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->memoPos:J

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 263
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 264
    .restart local v2    # "readCount":I
    if-ltz v2, :cond_1

    .line 266
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 267
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileMemoLength:I

    .line 268
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 271
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->memoPos:J

    add-long/2addr v8, v12

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 273
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    iput-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->filePos:J

    .line 274
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 275
    if-ltz v2, :cond_1

    .line 277
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 278
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileImagLength:I

    .line 279
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 281
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->filePos:J

    iget v10, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileImagLength:I

    int-to-long v10, v10

    add-long/2addr v8, v10

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 283
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    iput-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->thumPos:J

    .line 284
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 285
    if-ltz v2, :cond_1

    .line 287
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 288
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileThumLength:I

    .line 289
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 291
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->thumPos:J

    iget v10, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileThumLength:I

    int-to-long v10, v10

    add-long/2addr v8, v10

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 293
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    iput-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->textPos:J

    .line 294
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 295
    if-ltz v2, :cond_1

    .line 297
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 298
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileTextLength:I

    .line 299
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 301
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 303
    const/4 v3, 0x1

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasMemo:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private findOuterAtoms(Ljava/lang/String;)Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;
    .locals 12
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    const-wide/16 v4, 0x0

    .line 96
    .local v4, "skip":J
    const-string v0, ""

    .line 97
    .local v0, "boxName":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;

    invoke-direct {v3}, Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;-><init>()V

    .line 98
    .local v3, "pair":Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;
    const/4 v1, 0x0

    .line 101
    .local v1, "countRead":I
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 102
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v6, v7}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 103
    if-gez v1, :cond_1

    const/4 v3, 0x0

    .line 137
    .end local v3    # "pair":Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;
    :goto_1
    return-object v3

    .line 105
    .restart local v3    # "pair":Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 106
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    int-to-long v4, v6

    .line 107
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 109
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v6, v7}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 110
    if-gez v1, :cond_2

    const/4 v3, 0x0

    goto :goto_1

    .line 112
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 113
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->arrToAscii([B)Ljava/lang/String;

    move-result-object v0

    .line 115
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 116
    const-wide/16 v6, 0x8

    sub-long/2addr v4, v6

    .line 117
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-lez v6, :cond_3

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v6

    add-long/2addr v6, v4

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-gtz v6, :cond_3

    .line 118
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v7}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v4

    invoke-virtual {v6, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 139
    :catch_0
    move-exception v2

    .line 140
    .local v2, "e":Ljava/io/IOException;
    const-string v6, "M4aReader"

    const-string v7, "Error reading the file"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 142
    throw v2

    .line 119
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-nez v6, :cond_4

    .line 120
    :try_start_1
    const-string v6, "M4aReader"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "skip value is 0 : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 122
    :cond_4
    const-string v6, "M4aReader"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Wrong skip value finding OuterAtom: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ! Returning from function"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 127
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v6

    const-wide/16 v8, 0x8

    sub-long/2addr v6, v8

    iput-wide v6, v3, Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;->position:J

    .line 128
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 129
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-wide v8, v3, Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;->position:J

    invoke-virtual {v6, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 130
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v6, v7}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 131
    if-gez v1, :cond_6

    const/4 v3, 0x0

    goto/16 :goto_1

    .line 133
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 134
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    iput v6, v3, Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;->length:I

    .line 135
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 136
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-wide v8, v3, Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;->position:J

    const-wide/16 v10, 0x8

    add-long/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private findSTTD(Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V
    .locals 14
    .param p1, "inf"    # Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x8

    .line 312
    const-wide/16 v6, 0x0

    .line 315
    .local v6, "skip":J
    const-string v0, ""

    .line 318
    .local v0, "boxName":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->udtaPos:J

    add-long/2addr v8, v12

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 319
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 320
    :cond_0
    :goto_0
    const-string v3, "sttd"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 321
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 322
    .local v2, "readCount":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 324
    const/4 v3, 0x0

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z

    .line 369
    :cond_1
    :goto_1
    return-void

    .line 328
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 329
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    int-to-long v6, v3

    .line 330
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 332
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 333
    if-ltz v2, :cond_1

    .line 335
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 336
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->arrToAscii([B)Ljava/lang/String;

    move-result-object v0

    .line 339
    const-string v3, "sttd"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 340
    sub-long/2addr v6, v12

    .line 341
    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v6

    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v10

    cmp-long v3, v8, v10

    if-gtz v3, :cond_3

    .line 342
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v8}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    add-long/2addr v8, v6

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 364
    .end local v2    # "readCount":I
    :catch_0
    move-exception v1

    .line 365
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "M4aReader"

    const-string v8, "Error reading the file"

    invoke-static {v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 367
    throw v1

    .line 344
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "readCount":I
    :cond_3
    :try_start_1
    const-string v3, "M4aReader"

    const-string v8, "Wrong skip value finding STTD! Possibly we are out of the file. Returning from function"

    invoke-static {v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    const/4 v3, 0x0

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z

    goto :goto_1

    .line 351
    .end local v2    # "readCount":I
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v8

    sub-long/2addr v8, v12

    iput-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->sttdPos:J

    .line 353
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v4

    .line 354
    .local v4, "save":J
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-wide v8, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->sttdPos:J

    invoke-virtual {v3, v8, v9}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 355
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    iget-object v8, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v8}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 356
    .restart local v2    # "readCount":I
    if-ltz v2, :cond_1

    .line 358
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 359
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileSttdLength:I

    .line 360
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 361
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 363
    const/4 v3, 0x1

    iput-boolean v3, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private isM4A()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 386
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->path:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 394
    :cond_0
    :goto_0
    return v0

    .line 390
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->path:Ljava/lang/String;

    const-string v2, ".m4a"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->path:Ljava/lang/String;

    const-string v2, ".M4A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 391
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isM4A(Ljava/lang/String;)Z
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 404
    if-nez p0, :cond_1

    .line 412
    :cond_0
    :goto_0
    return v0

    .line 408
    :cond_1
    const-string v1, ".m4a"

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ".M4A"

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 409
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->path:Ljava/lang/String;

    return-object v0
.end method

.method public readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 46
    sget-object v6, Lcom/sec/android/app/voicenote/common/util/M4aConsts;->FILE_LOCK:Ljava/lang/Object;

    monitor-enter v6

    .line 47
    const/4 v2, 0x0

    .line 49
    .local v2, "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->isM4A()Z

    move-result v7

    if-nez v7, :cond_0

    .line 51
    monitor-exit v6

    .line 90
    :goto_0
    return-object v5

    .line 54
    :cond_0
    const/4 v7, 0x4

    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->buff:Ljava/nio/ByteBuffer;

    .line 56
    new-instance v1, Ljava/io/File;

    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->path:Ljava/lang/String;

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    .local v1, "f":Ljava/io/File;
    :try_start_1
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object v7, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->fstr:Ljava/io/FileInputStream;

    .line 59
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->fstr:Ljava/io/FileInputStream;

    invoke-virtual {v7}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->channel:Ljava/nio/channels/FileChannel;

    .line 61
    new-instance v3, Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    invoke-direct {v3}, Lcom/sec/android/app/voicenote/common/util/M4aInfo;-><init>()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62
    .end local v2    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .local v3, "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :try_start_2
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->path:Ljava/lang/String;

    iput-object v7, v3, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->path:Ljava/lang/String;

    .line 63
    const/4 v4, 0x0

    .line 64
    .local v4, "pair":Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;
    const-string v7, "moov"

    invoke-direct {p0, v7}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->findOuterAtoms(Ljava/lang/String;)Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v4

    .line 65
    if-nez v4, :cond_1

    .line 66
    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v2, v3

    .end local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .restart local v2    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    goto :goto_0

    .line 68
    .end local v2    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .restart local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :cond_1
    :try_start_4
    iget v7, v4, Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;->length:I

    iput v7, v3, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileMoovLength:I

    .line 69
    iget-wide v8, v4, Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;->position:J

    iput-wide v8, v3, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->moovPos:J

    .line 70
    const-string v7, "udta"

    invoke-direct {p0, v7}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->findOuterAtoms(Ljava/lang/String;)Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v4

    .line 71
    if-nez v4, :cond_2

    .line 72
    :try_start_5
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v2, v3

    .end local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .restart local v2    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    goto :goto_0

    .line 73
    .end local v2    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .restart local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :cond_2
    :try_start_6
    iget v5, v4, Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;->length:I

    iput v5, v3, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileUdtaLength:I

    .line 74
    iget-wide v8, v4, Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;->position:J

    iput-wide v8, v3, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->udtaPos:J

    .line 76
    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->findBOOK(Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V

    .line 77
    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->findMEMO(Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V

    .line 78
    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->findSTTD(Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V

    .line 80
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/util/M4aReader;->fstr:Ljava/io/FileInputStream;

    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-object v2, v3

    .line 90
    .end local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .end local v4    # "pair":Lcom/sec/android/app/voicenote/common/util/M4aReader$Pair;
    .restart local v2    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :goto_1
    :try_start_7
    monitor-exit v6

    move-object v5, v2

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_2
    const/4 v2, 0x0

    .line 84
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 91
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    .end local v1    # "f":Ljava/io/File;
    :catchall_0
    move-exception v5

    :goto_3
    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    throw v5

    .line 85
    .restart local v1    # "f":Ljava/io/File;
    :catch_1
    move-exception v0

    .line 86
    .local v0, "e":Ljava/io/IOException;
    :goto_4
    const/4 v2, 0x0

    .line 87
    :try_start_8
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    .line 91
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .restart local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .restart local v2    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    goto :goto_3

    .line 85
    .end local v2    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .restart local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :catch_2
    move-exception v0

    move-object v2, v3

    .end local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .restart local v2    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    goto :goto_4

    .line 82
    .end local v2    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .restart local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :catch_3
    move-exception v0

    move-object v2, v3

    .end local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .restart local v2    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    goto :goto_2
.end method
