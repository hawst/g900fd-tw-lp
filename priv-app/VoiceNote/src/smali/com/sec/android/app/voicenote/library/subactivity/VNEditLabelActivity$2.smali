.class Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity$2;
.super Ljava/lang/Object;
.source "VNEditLabelActivity.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onColorChanged(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelColor:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->access$100(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;

    # setter for: Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mRetColor:I
    invoke-static {v0, p1}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->access$202(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;I)I

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->COLORS:[I
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->access$300(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;)[I

    move-result-object v1

    array-length v1, v1

    # invokes: Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setSelectedBtn(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->access$400(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;I)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity$2;->this$0:Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;

    # getter for: Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->mLabelName:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->access$500(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;)Landroid/widget/EditText;

    move-result-object v1

    # invokes: Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->setEnableOKButton(Landroid/widget/EditText;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;->access$600(Lcom/sec/android/app/voicenote/library/subactivity/VNEditLabelActivity;Landroid/widget/EditText;)V

    .line 220
    return-void
.end method
