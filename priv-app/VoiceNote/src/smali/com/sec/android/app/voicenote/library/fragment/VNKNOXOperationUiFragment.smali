.class public Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
.super Landroid/app/DialogFragment;
.source "VNKNOXOperationUiFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;,
        Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;
    }
.end annotation


# static fields
.field private static final INTENT_FILE_RELAY_COMPLETE:Ljava/lang/String; = "com.sec.knox.container.FileRelayComplete"

.field public static final INTENT_FILE_RELAY_DONE:Ljava/lang/String; = "com.sec.knox.container.FileRelayDone"

.field public static final INTENT_FILE_RELAY_EXIST:Ljava/lang/String; = "com.sec.knox.container.FileRelayExist"

.field public static final INTENT_FILE_RELAY_FAIL:Ljava/lang/String; = "com.sec.knox.container.FileRelayFail"

.field public static final INTENT_FILE_RELAY_FAIL2:Ljava/lang/String; = "com.sec.knox.container.FileRelayFail2"

.field private static final INTENT_FILE_RELAY_PROGRESS:Ljava/lang/String; = "com.sec.knox.container.FileRelayProgress"

.field public static final INTENT_FILE_RELAY_START:Ljava/lang/String; = "com.sec.knox.container.FileRelayStart"

.field public static final KEY_SELECTED:Ljava/lang/String; = "key_selected_container_id"

.field public static final REQUEST_KNOXCONTAINERSELECT:I = 0x3e8

.field public static final RESULT_KNOXCONTAINERSELECT:I = 0x3e9

.field public static final SEC_TAG:Ljava/lang/String; = "selectFragmentDialogTag"

.field private static final TAG:Ljava/lang/String; = "VNKNOXOperationUiFragment"

.field static mIcons:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# instance fields
.field private mBroadcastReceiverSecretMode:Landroid/content/BroadcastReceiver;

.field private mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

.field private mKNOX2OperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;

.field private mKNOXTargetId:I

.field private mKNOXUserId:I

.field private mKNOXVersion:Ljava/lang/String;

.field private mListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mMovedCount:I

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mProgressPercent:I

.field private mProgressView:Landroid/view/View;

.field mTargetDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mTargetIDs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mbMoveToSecretBox:Z

.field private mfileCount:Landroid/widget/TextView;

.field private mfilePercent:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mIcons:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 115
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressView:Landroid/view/View;

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressBar:Landroid/widget/ProgressBar;

    .line 86
    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mbMoveToSecretBox:Z

    .line 90
    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mMovedCount:I

    .line 91
    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressPercent:I

    .line 93
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mfileCount:Landroid/widget/TextView;

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mfilePercent:Landroid/widget/TextView;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    .line 98
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXVersion:Ljava/lang/String;

    .line 100
    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXUserId:I

    .line 101
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXTargetId:I

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOX2OperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;

    .line 660
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$4;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 116
    return-void
.end method

.method private UpdateDialog()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 514
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mfileCount:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mfilePercent:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mTargetDatas:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 521
    :cond_0
    :goto_0
    return-void

    .line 515
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressBar:Landroid/widget/ProgressBar;

    iget v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressPercent:I

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 516
    const-string v2, "%d / %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mMovedCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 517
    .local v0, "countText":Ljava/lang/String;
    const-string v2, "%d%%"

    new-array v3, v6, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressPercent:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 519
    .local v1, "percentText":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mfileCount:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 520
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mfilePercent:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic access$008(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mMovedCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mMovedCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->UpdateDialog()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXUserId:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
    .param p1, "x1"    # I

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->errorDisplay(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->errorDisplay20(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$402(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
    .param p1, "x1"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressPercent:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXTargetId:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXVersion:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOX2OperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)Lcom/sec/knox/containeragent/ContainerInstallerManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    return-object v0
.end method

.method private checkKNOX2ContainerCount()Landroid/app/Dialog;
    .locals 12

    .prologue
    const/4 v8, 0x1

    .line 347
    const/4 v1, 0x0

    .line 349
    .local v1, "dialog":Landroid/app/Dialog;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-string v7, "persona"

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PersonaManager;

    .line 350
    .local v3, "mPersona":Landroid/os/PersonaManager;
    invoke-virtual {v3}, Landroid/os/PersonaManager;->getPersonas()Ljava/util/List;

    move-result-object v4

    .line 351
    .local v4, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    const/4 v5, 0x0

    .line 352
    .local v5, "size":I
    if-eqz v4, :cond_0

    .line 353
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 355
    :cond_0
    if-nez v5, :cond_2

    .line 372
    :cond_1
    :goto_0
    return-object v1

    .line 357
    :cond_2
    if-ne v5, v8, :cond_3

    .line 359
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->createProgressDialog()Landroid/app/Dialog;

    move-result-object v1

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v2

    .line 361
    .local v2, "fragment":Landroid/app/Fragment;
    if-eqz v2, :cond_1

    .line 362
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    const-string v8, "key_selected_container_id"

    const/4 v6, 0x0

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/PersonaInfo;

    invoke-virtual {v6}, Landroid/content/pm/PersonaInfo;->getId()I

    move-result v6

    int-to-long v10, v6

    invoke-virtual {v7, v8, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 363
    .local v0, "data":Landroid/content/Intent;
    const/16 v6, 0x3e8

    const/16 v7, 0x3e9

    invoke-virtual {v2, v6, v7, v0}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 365
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->dismissAllowingStateLoss()V

    goto :goto_0

    .line 367
    .end local v0    # "data":Landroid/content/Intent;
    .end local v2    # "fragment":Landroid/app/Fragment;
    :cond_3
    if-le v5, v8, :cond_1

    .line 369
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->createKNOXContainerSelectDialog()Landroid/app/Dialog;

    move-result-object v1

    goto :goto_0
.end method

.method private createKNOXContainerSelectDialog()Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 270
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->setCancelable(Z)V

    .line 271
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-string v7, "persona"

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PersonaManager;

    .line 272
    .local v3, "mPersona":Landroid/os/PersonaManager;
    invoke-virtual {v3}, Landroid/os/PersonaManager;->getPersonas()Ljava/util/List;

    move-result-object v4

    .line 274
    .local v4, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 275
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 276
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f03005b

    invoke-virtual {v2, v6, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout;

    .line 277
    .local v5, "view":Landroid/widget/FrameLayout;
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 278
    const v6, 0x7f0e00eb

    invoke-virtual {v5, v6}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    .line 279
    .local v1, "gridview":Landroid/widget/GridView;
    new-instance v6, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-direct {v6, p0, v7, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$ContainerListAdapater;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v1, v6}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 280
    const/high16 v6, 0x1040000

    invoke-virtual {v0, v6, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 282
    iget-boolean v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mbMoveToSecretBox:Z

    if-eqz v6, :cond_0

    .line 283
    const v6, 0x7f0b00c4

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 288
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v6}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 290
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    return-object v6

    .line 285
    :cond_0
    const v6, 0x7f0b00c3

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private createProgressDialog()Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 294
    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->setCancelable(Z)V

    .line 295
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 297
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 299
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 300
    .local v1, "factory":Landroid/view/LayoutInflater;
    const v2, 0x7f03003b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressView:Landroid/view/View;

    .line 302
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 303
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressView:Landroid/view/View;

    const v3, 0x7f0e0092

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mfileCount:Landroid/widget/TextView;

    .line 304
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressView:Landroid/view/View;

    const v3, 0x7f0e0093

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mfilePercent:Landroid/widget/TextView;

    .line 306
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressView:Landroid/view/View;

    const v3, 0x7f0e0091

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressBar:Landroid/widget/ProgressBar;

    .line 307
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 308
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->UpdateDialog()V

    .line 310
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 312
    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mbMoveToSecretBox:Z

    if-eqz v2, :cond_0

    .line 313
    const v2, 0x7f0b00c4

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 317
    :goto_0
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 326
    const v2, 0x7f0b0021

    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$3;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 343
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2

    .line 315
    :cond_0
    const v2, 0x7f0b00c3

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private errorDisplay(I)V
    .locals 3
    .param p1, "errorcode"    # I

    .prologue
    const v1, 0x7f0b015c

    const v0, 0x7f0b00dd

    .line 524
    packed-switch p1, :pswitch_data_0

    .line 538
    :goto_0
    :pswitch_0
    return-void

    .line 527
    :pswitch_1
    invoke-static {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 531
    :pswitch_2
    invoke-static {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->newInstance(II)Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "FRAGMENT_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/fragment/VNStorageFullDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 524
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private errorDisplay20(Ljava/lang/String;)V
    .locals 2
    .param p1, "errorMsg"    # Ljava/lang/String;

    .prologue
    .line 541
    const-string v0, "VNKNOXOperationUiFragment"

    invoke-static {v0, p1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 543
    return-void
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 119
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 120
    return-void
.end method

.method public static newInstance()Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
    .locals 2

    .prologue
    .line 108
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;-><init>()V

    .line 109
    .local v1, "fragment":Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 110
    .local v0, "argument":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->setArguments(Landroid/os/Bundle;)V

    .line 111
    return-object v1
.end method

.method private runKNOX2Task()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 376
    const-string v0, "VNKNOXOperationUiFragment"

    const-string v1, "runKNOX2Task()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOX2OperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOX2OperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 379
    const-string v0, "VNKNOXOperationUiFragment"

    const-string v1, "runKNOX2Task() : cancel runned task"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOX2OperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->cancel(Z)Z

    .line 383
    :cond_0
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOX2OperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOX2OperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$KNOX2OperationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 385
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v6, -0x1

    .line 547
    const-string v1, "VNKNOXOperationUiFragment"

    const-string v4, "onActivityResult()"

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    packed-switch p1, :pswitch_data_0

    .line 568
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->dismissAllowingStateLoss()V

    .line 571
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 572
    return-void

    .line 550
    :pswitch_0
    const/16 v1, 0x3e9

    if-ne p2, v1, :cond_2

    .line 551
    const-string v1, "key_selected_container_id"

    invoke-virtual {p3, v1, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 552
    .local v2, "selected":J
    cmp-long v1, v2, v6

    if-eqz v1, :cond_0

    .line 553
    const-string v1, "VNKNOXOperationUiFragment"

    const-string v4, "onActivityResult() : start to move to KNOX2.0"

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 556
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_1

    .line 557
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 560
    :cond_1
    long-to-int v1, v2

    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXTargetId:I

    .line 561
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->runKNOX2Task()V

    goto :goto_0

    .line 564
    .end local v0    # "dialog":Landroid/app/Dialog;
    .end local v2    # "selected":J
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->dismissAllowingStateLoss()V

    goto :goto_0

    .line 548
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
    .end packed-switch
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 576
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 577
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 578
    .local v0, "fragment":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    .line 579
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getTargetRequestCode()I

    move-result v1

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 581
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 124
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 126
    new-instance v0, Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 127
    .local v0, "argument":Landroid/os/Bundle;
    const-string v1, "movetosecret"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mbMoveToSecretBox:Z

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "target_datas"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mTargetDatas:Ljava/util/ArrayList;

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "target_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mTargetIDs:Ljava/util/ArrayList;

    .line 130
    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->registerBroadcastReceiverSecretMode(Z)V

    .line 132
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-nez v1, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->dismissAllowingStateLoss()V

    .line 147
    :goto_0
    return-void

    .line 137
    :cond_0
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXVersion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXVersion:Ljava/lang/String;

    .line 139
    const-string v1, "2.0"

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXVersion:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 140
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXCallingUserId()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXUserId:I

    .line 141
    iget v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXUserId:I

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mbMoveToSecretBox:Z

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 143
    :cond_2
    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getKNOXContainer()Lcom/sec/knox/containeragent/ContainerInstallerManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->KNOXFileRelayRequest(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v3, 0x3e8

    .line 171
    const/4 v0, 0x0

    .line 172
    .local v0, "dialog":Landroid/app/Dialog;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getTargetRequestCode()I

    move-result v1

    .line 174
    .local v1, "requestCode":I
    if-ne v1, v3, :cond_1

    .line 175
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->checkKNOX2ContainerCount()Landroid/app/Dialog;

    move-result-object v0

    .line 176
    if-nez v0, :cond_0

    .line 177
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->createProgressDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->dismissAllowingStateLoss()V

    .line 189
    :cond_0
    :goto_0
    return-object v0

    .line 181
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->createProgressDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 182
    if-eq v1, v3, :cond_0

    .line 183
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->newInstance()Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;

    move-result-object v2

    .line 184
    .local v2, "tdf":Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;
    invoke-virtual {v2, p0, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "selectFragmentDialogTag"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->registerBroadcastReceiverSecretMode(Z)V

    .line 166
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 167
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 151
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 152
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    .line 153
    .local v1, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 154
    .local v0, "dialog":Landroid/app/Dialog;
    if-nez v1, :cond_1

    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mKNOXTargetId:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 155
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 156
    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 159
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public registerBroadcastReceiverSecretMode(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 193
    if-eqz p1, :cond_2

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mBroadcastReceiverSecretMode:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mBroadcastReceiverSecretMode:Landroid/content/BroadcastReceiver;

    .line 254
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 255
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.knox.container.FileRelayExist"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 256
    const-string v1, "com.sec.knox.container.FileRelayDone"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 257
    const-string v1, "com.sec.knox.container.FileRelayFail"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 258
    const-string v1, "com.sec.knox.container.FileRelayComplete"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 259
    const-string v1, "com.sec.knox.container.FileRelayProgress"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 260
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mBroadcastReceiverSecretMode:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 262
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mBroadcastReceiverSecretMode:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 263
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mBroadcastReceiverSecretMode:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 264
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNKNOXOperationUiFragment;->mBroadcastReceiverSecretMode:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method
