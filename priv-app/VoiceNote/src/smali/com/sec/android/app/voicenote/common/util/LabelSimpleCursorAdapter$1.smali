.class Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$1;
.super Landroid/database/ContentObserver;
.source "LabelSimpleCursorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$1;->this$0:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 124
    if-nez p1, :cond_1

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$1;->this$0:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 126
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    .line 129
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$1;->this$0:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$1;->this$0:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    # getter for: Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mFilterbyCallback:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->access$000(Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;)Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$1;->this$0:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    # getter for: Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->mFilterbyCallback:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->access$000(Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;)Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter$FilterbyCallback;->refreshAllItemCount()V

    .line 134
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_1
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 135
    return-void
.end method
