.class public Lcom/sec/android/app/voicenote/common/util/VNKNOXReceiver;
.super Landroid/content/BroadcastReceiver;
.source "VNKNOXReceiver.java"


# static fields
.field private static final INTENT_FILE_RELAY_COMPLETE:Ljava/lang/String; = "com.sec.knox.container.FileRelayComplete"

.field public static final INTENT_FILE_RELAY_DONE:Ljava/lang/String; = "com.sec.knox.container.FileRelayDone"

.field public static final INTENT_FILE_RELAY_EXIST:Ljava/lang/String; = "com.sec.knox.container.FileRelayExist"

.field public static final INTENT_FILE_RELAY_FAIL:Ljava/lang/String; = "com.sec.knox.container.FileRelayFail"


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 29
    const-string v0, "VNKNOXReceiver"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNKNOXReceiver;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 39
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNKNOXReceiver;->TAG:Ljava/lang/String;

    const-string v2, "onReceive"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const-string v1, "PACKAGENAME"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "packageName":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    const-string v1, "com.sec.knox.container.FileRelayDone"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 46
    const/16 v1, 0x11f8

    invoke-static {p1, v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendKNOXMessageToDialog(Landroid/content/Context;I)V

    goto :goto_0

    .line 47
    :cond_2
    const-string v1, "com.sec.knox.container.FileRelayExist"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 48
    const/16 v1, 0x11f9

    invoke-static {p1, v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendKNOXMessageToDialog(Landroid/content/Context;I)V

    goto :goto_0

    .line 49
    :cond_3
    const-string v1, "com.sec.knox.container.FileRelayFail"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 50
    const/16 v1, 0x11fa

    invoke-static {p1, v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendKNOXMessageToDialog(Landroid/content/Context;I)V

    goto :goto_0

    .line 51
    :cond_4
    const-string v1, "com.sec.knox.container.FileRelayComplete"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    const/16 v1, 0x11fb

    invoke-static {p1, v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendKNOXMessageToDialog(Landroid/content/Context;I)V

    goto :goto_0
.end method
