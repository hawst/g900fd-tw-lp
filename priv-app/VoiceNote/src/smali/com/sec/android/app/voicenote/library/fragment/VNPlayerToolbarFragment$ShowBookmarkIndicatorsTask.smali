.class Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;
.super Landroid/os/AsyncTask;
.source "VNPlayerToolbarFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShowBookmarkIndicatorsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V
    .locals 0

    .prologue
    .line 786
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$1;

    .prologue
    .line 786
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 9
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v8, 0x0

    .line 800
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    if-nez v5, :cond_0

    .line 801
    const-string v5, "VNPlyayerToolbarFragment"

    const-string v6, " ShowBookmarkIndicatorsTask, doInBackground : mService returns null"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 802
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 826
    :goto_0
    return-object v5

    .line 805
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$100()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPlayingID()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    .line 807
    .local v4, "path":Ljava/lang/String;
    if-eqz v4, :cond_2

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isM4A(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 808
    sget-object v6, Lcom/sec/android/app/voicenote/common/util/M4aConsts;->FILE_LOCK:Ljava/lang/Object;

    monitor-enter v6

    .line 809
    :try_start_0
    new-instance v2, Lcom/sec/android/app/voicenote/common/util/M4aReader;

    invoke-direct {v2, v4}, Lcom/sec/android/app/voicenote/common/util/M4aReader;-><init>(Ljava/lang/String;)V

    .line 810
    .local v2, "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/M4aReader;->readFile()Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    move-result-object v3

    .line 811
    .local v3, "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;

    invoke-direct {v0, v3}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;-><init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;)V

    .line 812
    .local v0, "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;->getAllBookmarks()Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$902(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 813
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 815
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mSeekbar:Landroid/widget/SeekBar;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1100(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Landroid/widget/SeekBar;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getWidth()I

    move-result v5

    if-nez v5, :cond_1

    .line 817
    const-wide/16 v6, 0x32

    :try_start_1
    invoke-virtual {p0, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalMonitorStateException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 818
    :catch_0
    move-exception v1

    .line 819
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_0

    .line 813
    .end local v0    # "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v2    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :catchall_0
    move-exception v5

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 823
    .restart local v0    # "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    .restart local v2    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .restart local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->prepareParams()Z
    invoke-static {v5}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 824
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_0

    .line 826
    .end local v0    # "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    .end local v2    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .end local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :cond_2
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_0

    .line 820
    .restart local v0    # "bookHelper":Lcom/sec/android/app/voicenote/common/util/BookmarksHelper;
    .restart local v2    # "helper":Lcom/sec/android/app/voicenote/common/util/M4aReader;
    .restart local v3    # "inf":Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    :catch_1
    move-exception v5

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 786
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 831
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 832
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$800(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$900()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onBookmarksChanged(Ljava/util/ArrayList;)V

    .line 833
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # invokes: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->setIndicators()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1300(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;)V

    .line 835
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mShowBookmarkIndicatorsTask:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1402(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;)Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;

    .line 836
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 837
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 786
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 790
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$900()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 791
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$900()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 793
    :cond_0
    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mBookmarks:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$902(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 794
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment$ShowBookmarkIndicatorsTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;

    # setter for: Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->mParams:Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;->access$1002(Lcom/sec/android/app/voicenote/library/fragment/VNPlayerToolbarFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 795
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 796
    return-void
.end method
