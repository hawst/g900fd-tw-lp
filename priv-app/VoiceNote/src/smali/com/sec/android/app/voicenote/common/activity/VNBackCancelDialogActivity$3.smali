.class Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$3;
.super Ljava/lang/Object;
.source "VNBackCancelDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$3;->this$0:Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 113
    # getter for: Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->access$000()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 114
    const-string v0, "VNBackCancelDialogActivity"

    const-string v1, "cencel recording in quickpanel while recording or pause by cancel dialog"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    # getter for: Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->access$000()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->hideNotification()Z

    .line 116
    # getter for: Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->access$000()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->cancelRecording()Z

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity$3;->this$0:Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/activity/VNBackCancelDialogActivity;->finish()V

    .line 121
    return-void
.end method
