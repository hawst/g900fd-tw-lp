.class Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;
.super Landroid/os/Handler;
.source "VoiceNoteService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0

    .prologue
    .line 1234
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v11, 0x3e8

    const/16 v9, 0x3ec

    const/16 v8, 0x3eb

    const/4 v7, 0x1

    const/4 v10, 0x0

    .line 1237
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/content/Intent;

    .line 1239
    .local v2, "intent":Landroid/content/Intent;
    iget v5, p1, Landroid/os/Message;->what:I

    sparse-switch v5, :sswitch_data_0

    .line 1498
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 1241
    :sswitch_1
    const-string v5, "change_notification_resume"

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 1242
    const-string v5, "change_notification_service"

    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 1243
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x5

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setWakeLock(Landroid/content/Context;I)V

    .line 1245
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1246
    .local v0, "action":Ljava/lang/String;
    const-string v5, "com.sec.android.app.voicenote.rec_pause"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1247
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pauseRecording()Z

    .line 1248
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_0

    .line 1249
    :cond_1
    const-string v5, "com.sec.android.app.voicenote.rec_reume"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1250
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumeRecording()Z

    .line 1251
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_0

    .line 1252
    :cond_2
    const-string v5, "com.sec.android.app.voicenote.rec_save"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1253
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v4

    .line 1254
    .local v4, "state":I
    if-eq v4, v11, :cond_0

    .line 1255
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecordingOnBackground()Ljava/lang/String;

    .line 1256
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_0

    .line 1258
    .end local v4    # "state":I
    :cond_3
    const-string v5, "com.sec.android.app.voicenote.rec_save_keygard"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1259
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # invokes: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->showStopDialog()V
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$600(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    goto :goto_0

    .line 1260
    :cond_4
    const-string v5, "com.sec.android.app.voicenote.rec_cancel"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1261
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->cancelRecording()Z

    .line 1262
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto :goto_0

    .line 1263
    :cond_5
    const-string v5, "com.sec.android.app.voicenote.play"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1264
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumePlay()V

    .line 1265
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto/16 :goto_0

    .line 1266
    :cond_6
    const-string v5, "com.sec.android.app.voicenote.play_pause"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1267
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 1268
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto/16 :goto_0

    .line 1269
    :cond_7
    const-string v5, "com.sec.android.app.voicenote.play_stop"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1270
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 1271
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5, v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setSkipSilenceMode(Z)V

    .line 1272
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto/16 :goto_0

    .line 1273
    :cond_8
    const-string v5, "com.sec.android.app.voicenote.rec_new"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 1274
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallIdle(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isCallActive(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1275
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const v7, 0x7f0b00d7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showManagedToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1281
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const/4 v6, 0x0

    const-wide/16 v8, 0x0

    invoke-virtual {v5, v6, v8, v9}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->initRecording(Ljava/lang/String;J)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 1282
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->startRecording()Z

    move-result v5

    if-nez v5, :cond_b

    .line 1283
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0b00fe

    invoke-static {v5, v6, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 1284
    const-string v5, "VoiceNoteService"

    const-string v6, "BACKGROUND_VOICERECORDER_REC_NEW or BACKGROUND_VOICERECORDER_STANDBY recording failed a"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 1291
    :cond_b
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto/16 :goto_0

    .line 1288
    :cond_c
    const-string v5, "VoiceNoteService"

    const-string v6, "BACKGROUND_VOICERECORDER_REC_NEW or BACKGROUND_VOICERECORDER_STANDBY recording failed b"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1292
    :cond_d
    const-string v5, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1293
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopForeground(Z)V

    .line 1294
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto/16 :goto_0

    .line 1295
    :cond_e
    const-string v5, "com.sec.android.app.voicenote.rec_cancel.keygard"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 1296
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # invokes: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->showCancelDialog()V
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$700(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    goto/16 :goto_0

    .line 1297
    :cond_f
    const-string v5, "com.sec.android.app.voicenote.rec_accept_call"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1298
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v4

    .line 1299
    .restart local v4    # "state":I
    if-eq v4, v11, :cond_0

    .line 1300
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecordingOnBackground()Ljava/lang/String;

    .line 1301
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto/16 :goto_0

    .line 1307
    .end local v0    # "action":Ljava/lang/String;
    .end local v4    # "state":I
    :sswitch_2
    const-string v5, "VoiceNoteService"

    const-string v6, "BroadcastMessage.HEADSET_PLUGIN"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1308
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->isNotificationShow()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1309
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # invokes: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isAdvancedAndHeadsetMode()Z
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$800(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 1310
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pauseRecording()Z

    .line 1311
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    .line 1312
    const-string v5, "record_mode"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    if-eq v5, v7, :cond_10

    const-string v5, "record_mode"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_11

    .line 1314
    :cond_10
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0b000b

    invoke-static {v5, v6, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 1316
    :cond_11
    const-string v5, "record_mode"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    .line 1317
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0b000f

    invoke-static {v5, v6, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 1320
    :cond_12
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->is4PinHeadsetConnected()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1321
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v5

    if-ne v5, v8, :cond_0

    .line 1322
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0b0102

    invoke-static {v5, v6, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 1330
    :sswitch_3
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    goto/16 :goto_0

    .line 1334
    :sswitch_4
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resetSaveCancelDialog()V

    .line 1335
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string v6, "android.intent.action.SCREEN_ON"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 1336
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$900(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    move-result-object v5

    if-eqz v5, :cond_13

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$900(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    move-result-object v5

    invoke-virtual {v5, v10}, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->setLEDNotification(Z)V

    .line 1337
    :cond_13
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isLockScreen()Z

    move-result v5

    if-eqz v5, :cond_14

    .line 1338
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->isNotificationShow()Z

    move-result v5

    if-nez v5, :cond_15

    .line 1339
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->showNotification()Z

    .line 1345
    :cond_14
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isLockScreen()Z

    move-result v5

    if-eqz v5, :cond_16

    .line 1346
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->sendUpdateAppWidgetOnBtnClick(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1341
    :cond_15
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->UpdateStart()V

    goto :goto_2

    .line 1348
    :cond_16
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mKeyguardManager:Landroid/app/KeyguardManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1000(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Landroid/app/KeyguardManager;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->removeContextualEvent(Landroid/app/KeyguardManager;)V

    goto/16 :goto_0

    .line 1350
    :cond_17
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string v6, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 1352
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v4

    .line 1353
    .restart local v4    # "state":I
    if-eq v4, v8, :cond_18

    if-ne v4, v9, :cond_19

    .line 1355
    :cond_18
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$900(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    move-result-object v5

    if-eqz v5, :cond_19

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$900(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->setLEDNotification(Z)V

    .line 1357
    :cond_19
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatEndTime:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1100(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v5

    if-gez v5, :cond_1a

    .line 1358
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->UpdateStop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1363
    .end local v4    # "state":I
    :catch_0
    move-exception v1

    .line 1364
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1360
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v4    # "state":I
    :cond_1a
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->repeatPlaybackBackground()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 1366
    .end local v4    # "state":I
    :cond_1b
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string v6, "android.intent.action.USER_PRESENT"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1367
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isLockScreen()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1368
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mKeyguardManager:Landroid/app/KeyguardManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1000(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Landroid/app/KeyguardManager;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->removeContextualEvent(Landroid/app/KeyguardManager;)V

    .line 1369
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isBackgrounRunning(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1370
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->hideNotification()Z

    goto/16 :goto_0

    .line 1376
    :sswitch_5
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resetSaveCancelDialog()V

    .line 1377
    const-string v5, "coverOpen"

    invoke-virtual {v2, v5, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 1378
    .local v3, "isCoverOpen":Z
    if-eqz v3, :cond_1d

    .line 1379
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$900(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    move-result-object v5

    if-eqz v5, :cond_1c

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$900(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    move-result-object v5

    invoke-virtual {v5, v10}, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->setLEDNotification(Z)V

    .line 1380
    :cond_1c
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->UpdateStart()V

    .line 1381
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isLockScreen()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1382
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mKeyguardManager:Landroid/app/KeyguardManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1000(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Landroid/app/KeyguardManager;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VNWidgetProvider;->removeContextualEvent(Landroid/app/KeyguardManager;)V

    goto/16 :goto_0

    .line 1386
    :cond_1d
    :try_start_2
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v4

    .line 1387
    .restart local v4    # "state":I
    if-eq v4, v8, :cond_1e

    if-ne v4, v9, :cond_1f

    .line 1389
    :cond_1e
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$900(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    move-result-object v5

    if-eqz v5, :cond_1f

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mLedNotiManager:Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$900(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLEDNotiManager;->setLEDNotification(Z)V

    .line 1391
    :cond_1f
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mRepeatEndTime:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1100(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v5

    if-gez v5, :cond_20

    .line 1392
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->UpdateStop()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 1397
    .end local v4    # "state":I
    :catch_1
    move-exception v1

    .line 1398
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1394
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v4    # "state":I
    :cond_20
    :try_start_3
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->repeatPlaybackBackground()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 1405
    .end local v3    # "isCoverOpen":Z
    .end local v4    # "state":I
    :sswitch_6
    const-string v5, "storage"

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v7, :cond_0

    .line 1406
    const-string v5, "storage"

    invoke-static {v5, v10}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 1407
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v5

    const-string v6, "mounted"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1408
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v5

    if-eq v5, v8, :cond_21

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v5

    if-ne v5, v9, :cond_0

    .line 1409
    :cond_21
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0b00fd

    invoke-static {v5, v6, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 1410
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->cancelRecording()Z

    .line 1411
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const/16 v6, 0x7d5

    # invokes: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->setState(I)V
    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1200(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)V

    .line 1412
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1413
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->hideNotification()Z

    goto/16 :goto_0

    .line 1420
    :sswitch_7
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v5

    if-eq v5, v8, :cond_22

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v5

    if-ne v5, v9, :cond_23

    .line 1422
    :cond_22
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecording()Z

    .line 1423
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->hideNotification()Z

    .line 1425
    :cond_23
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1426
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    goto/16 :goto_0

    .line 1429
    :sswitch_8
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1400(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->isWiredHeadsetOn()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mAudioManager:Lcom/sec/android/app/voicenote/common/util/VNAudioManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1400(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNAudioManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/util/VNAudioManager;->isBluetoothA2dpOn()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1430
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 1431
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    .line 1432
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1440
    :sswitch_9
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0b0070

    invoke-static {v5, v6, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 1444
    :sswitch_a
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0b0021

    invoke-static {v5, v6, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 1448
    :sswitch_b
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0b0083

    invoke-static {v5, v6, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 1452
    :sswitch_c
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v5

    if-eq v5, v8, :cond_24

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mMediaRecorderState:I
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)I

    move-result v5

    if-ne v5, v9, :cond_25

    .line 1454
    :cond_24
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecording()Z

    .line 1455
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->hideNotification()Z

    .line 1457
    :cond_25
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1458
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->stopPlay()V

    .line 1459
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->hideNotification()Z

    goto/16 :goto_0

    .line 1464
    :sswitch_d
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecording(Z)Z

    goto/16 :goto_0

    .line 1468
    :sswitch_e
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsCallRinging:Z
    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$402(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z

    .line 1469
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    const/16 v6, 0xfdd

    # invokes: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendMessageCallback(I)V
    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1500(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;I)V

    goto/16 :goto_0

    .line 1473
    :sswitch_f
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # invokes: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecordingOnEvent()V
    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$500(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    .line 1474
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsCallRinging:Z
    invoke-static {v5, v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$402(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z

    goto/16 :goto_0

    .line 1478
    :sswitch_10
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsCallRinging:Z
    invoke-static {v5, v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$402(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z

    goto/16 :goto_0

    .line 1482
    :sswitch_11
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 1483
    .local v4, "state":Ljava/lang/String;
    const-string v5, "com.android.systemui.statusbar.ANIMATING"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_26

    .line 1484
    const-string v5, "VoiceNoteService"

    const-string v6, "status bar is open"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1485
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsOpenStatusBar:Z
    invoke-static {v5, v7}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1602(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z

    goto/16 :goto_0

    .line 1487
    :cond_26
    const-string v5, "VoiceNoteService"

    const-string v6, "status bar is close"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 1488
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsOpenStatusBar:Z
    invoke-static {v5, v10}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$1602(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z

    goto/16 :goto_0

    .line 1493
    .end local v4    # "state":Ljava/lang/String;
    :sswitch_12
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$3;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resetSaveCancelDialog()V

    goto/16 :goto_0

    .line 1239
    :sswitch_data_0
    .sparse-switch
        0xfb4 -> :sswitch_3
        0xfc8 -> :sswitch_1
        0xfd3 -> :sswitch_6
        0xfd7 -> :sswitch_6
        0xfdc -> :sswitch_10
        0xfdd -> :sswitch_e
        0xfde -> :sswitch_f
        0xff0 -> :sswitch_7
        0x1004 -> :sswitch_8
        0x10cc -> :sswitch_4
        0x1130 -> :sswitch_2
        0x11f8 -> :sswitch_0
        0x11f9 -> :sswitch_b
        0x11fa -> :sswitch_a
        0x11fb -> :sswitch_9
        0x11fc -> :sswitch_c
        0x12c0 -> :sswitch_d
        0x1324 -> :sswitch_5
        0x132e -> :sswitch_11
        0x1450 -> :sswitch_12
    .end sparse-switch
.end method
