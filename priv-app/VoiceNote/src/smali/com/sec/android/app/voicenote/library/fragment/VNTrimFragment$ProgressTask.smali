.class Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;
.super Landroid/os/AsyncTask;
.source "VNTrimFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProgressTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V
    .locals 0

    .prologue
    .line 707
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$1;

    .prologue
    .line 707
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 707
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 9
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v8, 0x0

    .line 710
    const/4 v2, 0x0

    .line 711
    .local v2, "currentPostion":I
    const/4 v0, 0x0

    .local v0, "count":I
    move v1, v0

    .line 714
    .end local v0    # "count":I
    .local v1, "count":I
    :goto_0
    :try_start_0
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$700()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v5

    if-nez v5, :cond_1

    .line 716
    const-wide/16 v6, 0x32

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 720
    :goto_1
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    const/16 v5, 0x14

    if-gt v1, v5, :cond_0

    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->isCancelled()Z
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_3

    move-result v5

    if-eqz v5, :cond_3

    .line 745
    :cond_0
    :goto_2
    return-object v8

    .line 717
    .end local v0    # "count":I
    .restart local v1    # "count":I
    :catch_0
    move-exception v3

    .line 718
    .local v3, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 736
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v4

    move v0, v1

    .line 737
    .end local v1    # "count":I
    .restart local v0    # "count":I
    .local v4, "npe":Ljava/lang/NullPointerException;
    :goto_3
    goto :goto_2

    .line 724
    .end local v0    # "count":I
    .end local v4    # "npe":Ljava/lang/NullPointerException;
    .restart local v1    # "count":I
    :cond_1
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$700()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 725
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$700()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getCurrentPosition()I

    move-result v2

    .line 726
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Integer;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1

    .line 728
    const-wide/16 v6, 0x32

    :try_start_4
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_1

    .line 732
    :goto_4
    :try_start_5
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v1

    .line 733
    .end local v1    # "count":I
    .restart local v0    # "count":I
    goto :goto_2

    .line 729
    .end local v0    # "count":I
    .restart local v1    # "count":I
    :catch_2
    move-exception v3

    .line 730
    .restart local v3    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_4

    .end local v3    # "e":Ljava/lang/InterruptedException;
    :cond_2
    move v0, v1

    .line 745
    .end local v1    # "count":I
    .restart local v0    # "count":I
    goto :goto_2

    .line 736
    :catch_3
    move-exception v4

    goto :goto_3

    :cond_3
    move v1, v0

    .end local v0    # "count":I
    .restart local v1    # "count":I
    goto :goto_0
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 3
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    const/4 v2, 0x0

    .line 749
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 771
    :goto_0
    return-void

    .line 753
    :cond_0
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$700()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/SeekBar;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1500(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    if-nez v0, :cond_2

    .line 754
    :cond_1
    const-string v0, "VNTrimFragment"

    const-string v1, "onProgressUpdate : activity is about to finishing"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 758
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getEndTime()I

    move-result v0

    aget-object v1, p1, v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v0, v1, :cond_3

    .line 759
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$700()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 760
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1500(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onUpdateState(Z)V

    .line 761
    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->cancel(Z)Z

    .line 762
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPauseButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1700(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 763
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1800(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 764
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mPlayButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$1800(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 765
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 766
    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$700()Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->getStartTime()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->seek(I)V

    .line 770
    :goto_1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 768
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->this$0:Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;

    # getter for: Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->mSeekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;->access$300(Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment;)Landroid/widget/SeekBar;

    move-result-object v0

    aget-object v1, p1, v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_1
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 707
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNTrimFragment$ProgressTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
