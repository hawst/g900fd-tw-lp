.class public Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "VNSpinnerSimpleCursorAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SpinnerSimpleCursorAdapter"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFrom:[Ljava/lang/String;

.field private mTo:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "from"    # [Ljava/lang/String;
    .param p5, "to"    # [I

    .prologue
    .line 42
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->mContext:Landroid/content/Context;

    .line 44
    iput-object p4, p0, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->mFrom:[Ljava/lang/String;

    .line 45
    iput-object p5, p0, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->mTo:[I

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "from"    # [Ljava/lang/String;
    .param p5, "to"    # [I
    .param p6, "flagRegisterContentObserver"    # I

    .prologue
    .line 49
    invoke-direct/range {p0 .. p6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 50
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->mContext:Landroid/content/Context;

    .line 51
    iput-object p4, p0, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->mFrom:[Ljava/lang/String;

    .line 52
    iput-object p5, p0, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->mTo:[I

    .line 53
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 114
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 115
    :cond_0
    const-string v5, "SpinnerSimpleCursorAdapter"

    const-string v6, "bindview : cursor is null or isClosed "

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :goto_0
    return-void

    .line 120
    :cond_1
    const/4 v0, -0x1

    .line 121
    .local v0, "color":I
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->mTo:[I

    aget v5, v5, v6

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 122
    .local v2, "label":Landroid/view/View;
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->mTo:[I

    aget v5, v5, v7

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 125
    .local v4, "titleText":Landroid/widget/TextView;
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->mFrom:[Ljava/lang/String;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 126
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    if-nez v5, :cond_2

    .line 127
    const v5, 0x7f0b0029

    invoke-virtual {p2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 141
    .local v3, "title":Ljava/lang/String;
    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 142
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 144
    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setHoverPopupType(I)V

    goto :goto_0

    .line 130
    .end local v3    # "title":Ljava/lang/String;
    :cond_2
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->mFrom:[Ljava/lang/String;

    const/4 v6, 0x1

    aget-object v5, v5, v6

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/StaleDataException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .restart local v3    # "title":Ljava/lang/String;
    goto :goto_1

    .line 133
    .end local v3    # "title":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 134
    .local v1, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v1}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    .line 136
    .end local v1    # "e":Landroid/database/CursorIndexOutOfBoundsException;
    :catch_1
    move-exception v1

    .line 137
    .local v1, "e":Landroid/database/StaleDataException;
    invoke-virtual {v1}, Landroid/database/StaleDataException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Landroid/widget/SimpleCursorAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v5, 0x7f03002b

    const/4 v4, 0x0

    .line 63
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 64
    .local v1, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 65
    :cond_0
    invoke-virtual {v1, v5, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    move-object v2, p2

    .line 78
    :goto_0
    return-object v2

    .line 68
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_2

    .line 69
    invoke-virtual {v1, v5, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    move-object v2, p2

    .line 70
    goto :goto_0

    .line 73
    :cond_2
    const v2, 0x7f03002d

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 74
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleCursorAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "ie":Ljava/lang/IllegalStateException;
    const-string v2, "SpinnerSimpleCursorAdapter"

    const-string v3, "IllegalStateException occured : attempt to re-open an already-closed object"

    invoke-static {v2, v3, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 77
    invoke-virtual {v1, v5, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    move-object v2, p2

    .line 78
    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 5
    .param p1, "position"    # I

    .prologue
    const-wide/16 v2, 0x0

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 157
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/SimpleCursorAdapter;->getItemId(I)J
    :try_end_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 164
    :cond_0
    :goto_0
    return-wide v2

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Landroid/database/StaleDataException;
    const-string v1, "SpinnerSimpleCursorAdapter"

    const-string v4, "getItemId : StableDataException"

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v5, 0x7f03002b

    const/4 v4, 0x0

    .line 87
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 88
    .local v1, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    const-string v2, "SpinnerSimpleCursorAdapter"

    const-string v3, "getView : cursor is closed! It shouldn\'t happen here"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_1

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v3

    invoke-virtual {p0, v2, v3, p3}, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 107
    :goto_0
    return-object v2

    .line 96
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/common/VNSpinnerSimpleCursorAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_2

    .line 98
    invoke-virtual {v1, v5, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    move-object v2, p2

    .line 99
    goto :goto_0

    .line 103
    :cond_2
    :try_start_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, "ex":Ljava/lang/IllegalStateException;
    const-string v2, "SpinnerSimpleCursorAdapter"

    const-string v3, "IllegalStateException occured: "

    invoke-static {v2, v3, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 106
    invoke-virtual {v1, v5, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    move-object v2, p2

    .line 107
    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 149
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 150
    .local v0, "view":Landroid/view/View;
    return-object v0
.end method
