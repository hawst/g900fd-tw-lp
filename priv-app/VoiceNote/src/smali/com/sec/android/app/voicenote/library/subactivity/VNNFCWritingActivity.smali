.class public Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;
.super Landroid/app/Activity;
.source "VNNFCWritingActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VoiceListNFCWritingActivity"


# instance fields
.field private mIntentFilter:[Landroid/content/IntentFilter;

.field private mLabelInfo:Ljava/lang/String;

.field private mNfcAdapter:Landroid/nfc/NfcAdapter;

.field private mPendingIntent:Landroid/app/PendingIntent;

.field private techListsArray:[[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mPendingIntent:Landroid/app/PendingIntent;

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mIntentFilter:[Landroid/content/IntentFilter;

    move-object v0, v1

    .line 61
    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->techListsArray:[[Ljava/lang/String;

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mLabelInfo:Ljava/lang/String;

    return-void
.end method

.method public static createTextRecord(Ljava/lang/String;Ljava/util/Locale;Z)Landroid/nfc/NdefRecord;
    .locals 9
    .param p0, "payload"    # Ljava/lang/String;
    .param p1, "locale"    # Ljava/util/Locale;
    .param p2, "encodeInUtf8"    # Z

    .prologue
    const/4 v6, 0x0

    .line 312
    if-nez p0, :cond_0

    .line 313
    const-string v6, "VoiceListNFCWritingActivity"

    const-string v7, "payload is null"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    const/4 v6, 0x0

    .line 328
    :goto_0
    return-object v6

    .line 317
    :cond_0
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    const-string v8, "US-ASCII"

    invoke-static {v8}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    .line 318
    .local v1, "langBytes":[B
    if-eqz p2, :cond_1

    const-string v7, "UTF-8"

    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v5

    .line 319
    .local v5, "utfEncoding":Ljava/nio/charset/Charset;
    :goto_1
    invoke-virtual {p0, v5}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    .line 320
    .local v3, "textBytes":[B
    if-eqz p2, :cond_2

    move v4, v6

    .line 321
    .local v4, "utfBit":I
    :goto_2
    array-length v7, v1

    add-int/2addr v7, v4

    int-to-char v2, v7

    .line 322
    .local v2, "status":C
    array-length v7, v1

    add-int/lit8 v7, v7, 0x1

    array-length v8, v3

    add-int/2addr v7, v8

    new-array v0, v7, [B

    .line 323
    .local v0, "data":[B
    int-to-byte v7, v2

    aput-byte v7, v0, v6

    .line 325
    const/4 v7, 0x1

    array-length v8, v1

    invoke-static {v1, v6, v0, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 326
    array-length v7, v1

    add-int/lit8 v7, v7, 0x1

    array-length v8, v3

    invoke-static {v3, v6, v0, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 328
    const-string v6, "voice/path"

    invoke-static {v6, v0}, Landroid/nfc/NdefRecord;->createMime(Ljava/lang/String;[B)Landroid/nfc/NdefRecord;

    move-result-object v6

    goto :goto_0

    .line 318
    .end local v0    # "data":[B
    .end local v2    # "status":C
    .end local v3    # "textBytes":[B
    .end local v4    # "utfBit":I
    .end local v5    # "utfEncoding":Ljava/nio/charset/Charset;
    :cond_1
    const-string v7, "UTF-16"

    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v5

    goto :goto_1

    .line 320
    .restart local v3    # "textBytes":[B
    .restart local v5    # "utfEncoding":Ljava/nio/charset/Charset;
    :cond_2
    const/16 v4, 0x80

    goto :goto_2
.end method

.method private resolveIntent(Landroid/content/Intent;)V
    .locals 17
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 161
    const-string v12, "VoiceListNFCWritingActivity"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "resolveIntent : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v12, "android.nfc.action.TAG_DISCOVERED"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    const-string v12, "android.nfc.action.TECH_DISCOVERED"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 166
    :cond_0
    const/4 v4, 0x1

    .line 167
    .local v4, "isSameDevice":Z
    const-string v12, "android.nfc.extra.NDEF_MESSAGES"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v10

    .line 168
    .local v10, "rawMsgs":[Landroid/os/Parcelable;
    if-eqz v10, :cond_4

    array-length v12, v10

    if-lez v12, :cond_4

    .line 169
    array-length v12, v10

    new-array v7, v12, [Landroid/nfc/NdefMessage;

    .line 170
    .local v7, "msgs":[Landroid/nfc/NdefMessage;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v12, v10

    if-ge v3, v12, :cond_4

    .line 172
    :try_start_0
    aget-object v12, v10, v3

    check-cast v12, Landroid/nfc/NdefMessage;

    aput-object v12, v7, v3

    .line 173
    aget-object v12, v7, v3

    invoke-virtual {v12}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v12

    const/4 v13, 0x0

    aget-object v12, v12, v13

    invoke-virtual {v12}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v9

    .line 175
    .local v9, "payload":[B
    if-eqz v9, :cond_1

    array-length v12, v9

    if-gtz v12, :cond_2

    .line 176
    :cond_1
    const-string v12, "VoiceListNFCWritingActivity"

    const-string v13, "resolveIntent : There is no payload."

    invoke-static {v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    .end local v9    # "payload":[B
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 180
    .restart local v9    # "payload":[B
    :cond_2
    const/4 v12, 0x0

    aget-byte v12, v9, v12

    and-int/lit16 v12, v12, 0x80

    if-nez v12, :cond_3

    const-string v2, "UTF-8"

    .line 181
    .local v2, "encoding":Ljava/lang/String;
    :goto_2
    const/4 v12, 0x0

    aget-byte v12, v9, v12

    and-int/lit8 v5, v12, 0x3f

    .line 183
    .local v5, "langCodeLen":I
    new-instance v8, Ljava/lang/String;

    add-int/lit8 v12, v5, 0x1

    array-length v13, v9

    sub-int/2addr v13, v5

    add-int/lit8 v13, v13, -0x1

    invoke-direct {v8, v9, v12, v13, v2}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 184
    .local v8, "path":Ljava/lang/String;
    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v12}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->updateTagedInfo(Ljava/lang/String;Z)Z

    move-result v4

    goto :goto_1

    .line 180
    .end local v2    # "encoding":Ljava/lang/String;
    .end local v5    # "langCodeLen":I
    .end local v8    # "path":Ljava/lang/String;
    :cond_3
    const-string v2, "UTF-16"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 185
    .end local v9    # "payload":[B
    :catch_0
    move-exception v1

    .line 186
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 187
    const-string v12, "VoiceListNFCWritingActivity"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "resolveIntent "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 192
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "i":I
    .end local v7    # "msgs":[Landroid/nfc/NdefMessage;
    :cond_4
    if-nez v4, :cond_6

    .line 193
    const-string v12, "VoiceListNFCWritingActivity"

    const-string v13, "writeTag fail : different device"

    invoke-static {v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const v13, 0x7f0b0163

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const v13, 0x7f0b0164

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 195
    .local v6, "msg":Ljava/lang/String;
    const v12, 0x7f0b015d

    invoke-static {v12, v6}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->newInstance(ILjava/lang/String;)Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v13

    const-string v14, "FRAGMENT_DIALOG"

    invoke-virtual {v12, v13, v14}, Lcom/sec/android/app/voicenote/common/fragment/VNInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 215
    .end local v4    # "isSameDevice":Z
    .end local v6    # "msg":Ljava/lang/String;
    .end local v10    # "rawMsgs":[Landroid/os/Parcelable;
    :cond_5
    :goto_3
    return-void

    .line 199
    .restart local v4    # "isSameDevice":Z
    .restart local v10    # "rawMsgs":[Landroid/os/Parcelable;
    :cond_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mLabelInfo:Ljava/lang/String;

    if-nez v12, :cond_7

    .line 200
    const-string v12, "VoiceListNFCWritingActivity"

    const-string v13, "mFilepath is null. Gets filename from settings."

    invoke-static {v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getNFCfilename()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mLabelInfo:Ljava/lang/String;

    .line 204
    :cond_7
    const-string v12, "android.nfc.extra.TAG"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v11

    check-cast v11, Landroid/nfc/Tag;

    .line 205
    .local v11, "tag":Landroid/nfc/Tag;
    new-instance v6, Landroid/nfc/NdefMessage;

    const/4 v12, 0x1

    new-array v12, v12, [Landroid/nfc/NdefRecord;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mLabelInfo:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v15

    const/16 v16, 0x1

    invoke-static/range {v14 .. v16}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->createTextRecord(Ljava/lang/String;Ljava/util/Locale;Z)Landroid/nfc/NdefRecord;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-direct {v6, v12}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    .line 206
    .local v6, "msg":Landroid/nfc/NdefMessage;
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v6}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->writeTag(Landroid/nfc/Tag;Landroid/nfc/NdefMessage;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 207
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mLabelInfo:Ljava/lang/String;

    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->updateTagedInfo(Ljava/lang/String;Z)Z

    .line 208
    const v12, 0x7f0b0001

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 209
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->finish()V

    goto :goto_3

    .line 211
    :cond_8
    const/high16 v12, 0x7f0b0000

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    .line 212
    const-string v12, "VoiceListNFCWritingActivity"

    const-string v13, "writeTag fail"

    invoke-static {v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private updateTagedInfo(Ljava/lang/String;Z)Z
    .locals 23
    .param p1, "labelInfo"    # Ljava/lang/String;
    .param p2, "add"    # Z

    .prologue
    .line 218
    const-string v2, "VoiceListNFCWritingActivity"

    const-string v3, "updateTagedInfo"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const/16 v2, 0x3a

    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v17

    .line 222
    .local v17, "idIndex":I
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v12

    .line 223
    .local v12, "dateIndex":I
    const/4 v14, 0x0

    .line 224
    .local v14, "fileID":Ljava/lang/String;
    const/4 v15, 0x0

    .line 225
    .local v15, "filedate":Ljava/lang/String;
    const/16 v16, 0x0

    .line 227
    .local v16, "filepath":Ljava/lang/String;
    if-lez v17, :cond_0

    .line 228
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 230
    :cond_0
    if-lez v12, :cond_1

    .line 231
    add-int/lit8 v2, v17, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 232
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    .line 235
    :cond_1
    if-nez v16, :cond_2

    if-nez p2, :cond_2

    .line 236
    const-string v2, "VoiceListNFCWritingActivity"

    const-string v3, "File does not exist."

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const/4 v2, 0x1

    .line 308
    .end local v12    # "dateIndex":I
    .end local v14    # "fileID":Ljava/lang/String;
    .end local v15    # "filedate":Ljava/lang/String;
    .end local v16    # "filepath":Ljava/lang/String;
    .end local v17    # "idIndex":I
    :goto_0
    return v2

    .line 239
    .restart local v12    # "dateIndex":I
    .restart local v14    # "fileID":Ljava/lang/String;
    .restart local v15    # "filedate":Ljava/lang/String;
    .restart local v16    # "filepath":Ljava/lang/String;
    .restart local v17    # "idIndex":I
    :cond_2
    if-eqz v14, :cond_3

    if-nez v15, :cond_8

    .line 240
    :cond_3
    const-string v2, "VoiceListNFCWritingActivity"

    const-string v3, "OLD VERSION : fileID and filedate is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :cond_4
    const/4 v5, 0x0

    .line 249
    .local v5, "selection":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 250
    .local v19, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 251
    const-string v2, "_data"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 254
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 255
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_7

    .line 256
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 257
    const-string v2, "date_added"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 258
    .local v11, "dataTime":I
    const-string v2, "year_name"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 260
    .local v10, "dataNFC":I
    invoke-interface {v9, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 261
    .local v20, "time":Ljava/lang/String;
    invoke-interface {v9, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 262
    .local v18, "nfc":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 263
    .local v22, "where":Ljava/lang/String;
    const/4 v8, 0x0

    .line 265
    .local v8, "count":I
    if-eqz v18, :cond_5

    const-string v2, "NFC"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 266
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x3

    if-le v2, v3, :cond_5

    .line 267
    const-string v2, "VoiceListNFCWritingActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "count : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x3

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const/4 v2, 0x3

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 271
    :cond_5
    if-eqz p2, :cond_9

    .line 272
    new-instance v21, Landroid/content/ContentValues;

    invoke-direct/range {v21 .. v21}, Landroid/content/ContentValues;-><init>()V

    .line 273
    .local v21, "v":Landroid/content/ContentValues;
    const-string v2, "year_name"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NFC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v8, v8, 0x1

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 294
    .end local v8    # "count":I
    .end local v10    # "dataNFC":I
    .end local v11    # "dataTime":I
    .end local v18    # "nfc":Ljava/lang/String;
    .end local v20    # "time":Ljava/lang/String;
    .end local v21    # "v":Landroid/content/ContentValues;
    .end local v22    # "where":Ljava/lang/String;
    :cond_6
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 308
    :cond_7
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 242
    .end local v5    # "selection":Ljava/lang/String;
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v19    # "strBuilder":Ljava/lang/StringBuilder;
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getIDCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 243
    const-string v2, "VoiceListNFCWritingActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DIFFERENT DEVICE : fileID is different, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 277
    .restart local v5    # "selection":Ljava/lang/String;
    .restart local v8    # "count":I
    .restart local v9    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "dataNFC":I
    .restart local v11    # "dataTime":I
    .restart local v18    # "nfc":Ljava/lang/String;
    .restart local v19    # "strBuilder":Ljava/lang/StringBuilder;
    .restart local v20    # "time":Ljava/lang/String;
    .restart local v22    # "where":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 278
    add-int/lit8 v8, v8, -0x1

    .line 279
    if-gez v8, :cond_a

    .line 280
    const/4 v8, 0x0

    .line 283
    :cond_a
    new-instance v21, Landroid/content/ContentValues;

    invoke-direct/range {v21 .. v21}, Landroid/content/ContentValues;-><init>()V

    .line 284
    .restart local v21    # "v":Landroid/content/ContentValues;
    if-lez v8, :cond_b

    .line 285
    const-string v2, "year_name"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NFC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 296
    .end local v5    # "selection":Ljava/lang/String;
    .end local v8    # "count":I
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v10    # "dataNFC":I
    .end local v11    # "dataTime":I
    .end local v12    # "dateIndex":I
    .end local v14    # "fileID":Ljava/lang/String;
    .end local v15    # "filedate":Ljava/lang/String;
    .end local v16    # "filepath":Ljava/lang/String;
    .end local v17    # "idIndex":I
    .end local v18    # "nfc":Ljava/lang/String;
    .end local v19    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v20    # "time":Ljava/lang/String;
    .end local v21    # "v":Landroid/content/ContentValues;
    .end local v22    # "where":Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 297
    .local v13, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v13}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 298
    const-string v2, "VoiceListNFCWritingActivity"

    const-string v3, "error occurred while extractMetadata"

    invoke-static {v2, v3, v13}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 299
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 287
    .end local v13    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v5    # "selection":Ljava/lang/String;
    .restart local v8    # "count":I
    .restart local v9    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "dataNFC":I
    .restart local v11    # "dataTime":I
    .restart local v12    # "dateIndex":I
    .restart local v14    # "fileID":Ljava/lang/String;
    .restart local v15    # "filedate":Ljava/lang/String;
    .restart local v16    # "filepath":Ljava/lang/String;
    .restart local v17    # "idIndex":I
    .restart local v18    # "nfc":Ljava/lang/String;
    .restart local v19    # "strBuilder":Ljava/lang/StringBuilder;
    .restart local v20    # "time":Ljava/lang/String;
    .restart local v21    # "v":Landroid/content/ContentValues;
    .restart local v22    # "where":Ljava/lang/String;
    :cond_b
    :try_start_1
    const-string v2, "year_name"

    const-string v3, ""

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_2

    .line 300
    .end local v5    # "selection":Ljava/lang/String;
    .end local v8    # "count":I
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v10    # "dataNFC":I
    .end local v11    # "dataTime":I
    .end local v12    # "dateIndex":I
    .end local v14    # "fileID":Ljava/lang/String;
    .end local v15    # "filedate":Ljava/lang/String;
    .end local v16    # "filepath":Ljava/lang/String;
    .end local v17    # "idIndex":I
    .end local v18    # "nfc":Ljava/lang/String;
    .end local v19    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v20    # "time":Ljava/lang/String;
    .end local v21    # "v":Landroid/content/ContentValues;
    .end local v22    # "where":Ljava/lang/String;
    :catch_1
    move-exception v13

    .line 301
    .local v13, "e":Landroid/database/sqlite/SQLiteConstraintException;
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    .line 302
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 303
    .end local v13    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    :catch_2
    move-exception v13

    .line 304
    .local v13, "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    .line 305
    const-string v2, "VoiceListNFCWritingActivity"

    const-string v3, "error occurred while input data to MediaStore"

    invoke-static {v2, v3, v13}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 306
    const/4 v2, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 66
    const-string v5, "VoiceListNFCWritingActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCreate "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    const v5, 0x7f030003

    invoke-virtual {p0, v5}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->setContentView(I)V

    .line 70
    new-instance v2, Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;-><init>(Landroid/app/Activity;)V

    .line 71
    .local v2, "mVNActionBar":Lcom/sec/android/app/voicenote/common/util/VNActionBar;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 74
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 75
    const-string v5, "VoiceListNFCWritingActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCreate "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v5, "labelinfo"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mLabelInfo:Ljava/lang/String;

    .line 77
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->resolveIntent(Landroid/content/Intent;)V

    .line 80
    :cond_0
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 81
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-nez v5, :cond_1

    .line 82
    const-string v5, "VoiceListNFCWritingActivity"

    const-string v6, "mNfcAdapter is null"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_1
    new-instance v5, Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v6, 0x20000000

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v5

    invoke-static {p0, v8, v5, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mPendingIntent:Landroid/app/PendingIntent;

    .line 86
    const/4 v3, 0x0

    .line 88
    .local v3, "ndef":Landroid/content/IntentFilter;
    :try_start_0
    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.nfc.action.TAG_DISCOVERED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    .end local v3    # "ndef":Landroid/content/IntentFilter;
    .local v4, "ndef":Landroid/content/IntentFilter;
    :try_start_1
    const-string v5, "android.intent.category.DEFAULT"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v3, v4

    .line 93
    .end local v4    # "ndef":Landroid/content/IntentFilter;
    .restart local v3    # "ndef":Landroid/content/IntentFilter;
    :goto_0
    new-array v5, v9, [Landroid/content/IntentFilter;

    aput-object v3, v5, v8

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mIntentFilter:[Landroid/content/IntentFilter;

    .line 94
    new-array v5, v9, [[Ljava/lang/String;

    new-array v6, v9, [Ljava/lang/String;

    const-class v7, Landroid/nfc/tech/NfcF;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    aput-object v6, v5, v8

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->techListsArray:[[Ljava/lang/String;

    .line 95
    return-void

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->finish()V

    goto :goto_0

    .line 90
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v3    # "ndef":Landroid/content/IntentFilter;
    .restart local v4    # "ndef":Landroid/content/IntentFilter;
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4    # "ndef":Landroid/content/IntentFilter;
    .restart local v3    # "ndef":Landroid/content/IntentFilter;
    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 131
    const-string v0, "VoiceListNFCWritingActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 133
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 156
    const-string v0, "VoiceListNFCWritingActivity"

    const-string v1, "onNewIntent"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->resolveIntent(Landroid/content/Intent;)V

    .line 158
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 393
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 398
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 395
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->finish()V

    goto :goto_0

    .line 393
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 110
    const-string v1, "VoiceListNFCWritingActivity"

    const-string v2, "onPause"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v1, :cond_0

    .line 115
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v1, p0}, Landroid/nfc/NfcAdapter;->disableForegroundDispatch(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "VoiceListNFCWritingActivity"

    const-string v2, "catch the exception while disableForegroundDispatch the NfcAdapter"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 99
    const-string v0, "VoiceListNFCWritingActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_0

    .line 103
    const-string v0, "VoiceListNFCWritingActivity"

    const-string v1, "NFC enableForegroundDispatch"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mPendingIntent:Landroid/app/PendingIntent;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->mIntentFilter:[Landroid/content/IntentFilter;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->techListsArray:[[Ljava/lang/String;

    invoke-virtual {v0, p0, v1, v2, v3}, Landroid/nfc/NfcAdapter;->enableForegroundDispatch(Landroid/app/Activity;Landroid/app/PendingIntent;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V

    .line 106
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 124
    const-string v0, "VoiceListNFCWritingActivity"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/subactivity/VNNFCWritingActivity;->finish()V

    .line 126
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 127
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 137
    const-string v0, "VoiceListNFCWritingActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTrimMemory() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    sparse-switch p1, :sswitch_data_0

    .line 151
    :sswitch_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 152
    return-void

    .line 138
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method writeTag(Landroid/nfc/Tag;Landroid/nfc/NdefMessage;)Z
    .locals 7
    .param p1, "tag"    # Landroid/nfc/Tag;
    .param p2, "mMessage"    # Landroid/nfc/NdefMessage;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 333
    :try_start_0
    invoke-static {p1}, Landroid/nfc/tech/Ndef;->get(Landroid/nfc/Tag;)Landroid/nfc/tech/Ndef;

    move-result-object v2

    .line 334
    .local v2, "ndef":Landroid/nfc/tech/Ndef;
    if-eqz v2, :cond_4

    .line 335
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->connect()V

    .line 337
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->isWritable()Z

    move-result v5

    if-nez v5, :cond_1

    .line 338
    const-string v4, "VoiceListNFCWritingActivity"

    const-string v5, "IDS_NFC_BODY_TAG_IS_READ_ONLY"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 340
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->close()V

    .line 388
    .end local v2    # "ndef":Landroid/nfc/tech/Ndef;
    :cond_0
    :goto_0
    return v3

    .line 344
    .restart local v2    # "ndef":Landroid/nfc/tech/Ndef;
    :cond_1
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->getMaxSize()I

    move-result v5

    if-gez v5, :cond_2

    .line 345
    const-string v4, "VoiceListNFCWritingActivity"

    const-string v5, "IDS_NFC_BODY_TAG_CAPACITY_IS_PD_BYTES_CONTENT_IS_PD_BYTES"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 347
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 384
    .end local v2    # "ndef":Landroid/nfc/tech/Ndef;
    :catch_0
    move-exception v0

    .line 385
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "VoiceListNFCWritingActivity"

    const-string v5, "Failed to write tag"

    invoke-static {v4, v5, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 387
    const-string v4, "VoiceListNFCWritingActivity"

    const-string v5, "IDS_NFC_BODY_FAILED_TO_WRITE_TO_TAG"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 352
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "ndef":Landroid/nfc/tech/Ndef;
    :cond_2
    :try_start_1
    invoke-virtual {v2, p2}, Landroid/nfc/tech/Ndef;->writeNdefMessage(Landroid/nfc/NdefMessage;)V

    .line 353
    const-string v5, "VoiceListNFCWritingActivity"

    const-string v6, "IDS_NFC_BODY_MESSAGE_SAVED_TO_PRE_FORMATTED_TAG"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 356
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->close()V

    :cond_3
    move v3, v4

    .line 358
    goto :goto_0

    .line 360
    :cond_4
    invoke-static {p1}, Landroid/nfc/tech/NdefFormatable;->get(Landroid/nfc/Tag;)Landroid/nfc/tech/NdefFormatable;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    .line 361
    .local v1, "format":Landroid/nfc/tech/NdefFormatable;
    if-eqz v1, :cond_6

    .line 363
    :try_start_2
    invoke-virtual {v1}, Landroid/nfc/tech/NdefFormatable;->connect()V

    .line 364
    invoke-virtual {v1, p2}, Landroid/nfc/tech/NdefFormatable;->format(Landroid/nfc/NdefMessage;)V

    .line 365
    const-string v5, "VoiceListNFCWritingActivity"

    const-string v6, "IDS_NFC_BODY_TAG_FORMATTED_AND_MESSAGE_SAVED"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    invoke-virtual {v1}, Landroid/nfc/tech/NdefFormatable;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 368
    invoke-virtual {v1}, Landroid/nfc/tech/NdefFormatable;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_5
    move v3, v4

    .line 370
    goto :goto_0

    .line 371
    :catch_1
    move-exception v0

    .line 372
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    const-string v4, "VoiceListNFCWritingActivity"

    const-string v5, "IDS_NFC_BODY_FAILED_TO_FORMAT_TAG"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    invoke-virtual {v1}, Landroid/nfc/tech/NdefFormatable;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 375
    invoke-virtual {v1}, Landroid/nfc/tech/NdefFormatable;->close()V

    goto :goto_0

    .line 380
    .end local v0    # "e":Ljava/io/IOException;
    :cond_6
    const-string v4, "VoiceListNFCWritingActivity"

    const-string v5, "IDS_NFC_BODY_NDEF_NOT_SUPPORTED_BY_TAG"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0
.end method
