.class Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNRecIconSwitcher;
.super Landroid/widget/ImageSwitcher;
.source "VNPanelTextFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VNRecIconSwitcher"
.end annotation


# static fields
.field private static mOnAni:Landroid/view/animation/AlphaAnimation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNRecIconSwitcher;->mOnAni:Landroid/view/animation/AlphaAnimation;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 314
    invoke-direct {p0, p1}, Landroid/widget/ImageSwitcher;-><init>(Landroid/content/Context;)V

    .line 315
    return-void
.end method

.method public static declared-synchronized getOnAnimation()Landroid/view/animation/AlphaAnimation;
    .locals 4

    .prologue
    .line 318
    const-class v1, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNRecIconSwitcher;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNRecIconSwitcher;->mOnAni:Landroid/view/animation/AlphaAnimation;

    if-nez v0, :cond_0

    .line 319
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNRecIconSwitcher;->mOnAni:Landroid/view/animation/AlphaAnimation;

    .line 320
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNRecIconSwitcher;->mOnAni:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 321
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNRecIconSwitcher;->mOnAni:Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/animation/AlphaAnimation;->setRepeatCount(I)V

    .line 322
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNRecIconSwitcher;->mOnAni:Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/view/animation/AlphaAnimation;->setRepeatMode(I)V

    .line 323
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNRecIconSwitcher;->mOnAni:Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 325
    :cond_0
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNPanelTextFragment$VNRecIconSwitcher;->mOnAni:Landroid/view/animation/AlphaAnimation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 318
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
