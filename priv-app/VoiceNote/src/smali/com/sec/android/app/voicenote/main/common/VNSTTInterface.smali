.class public interface abstract Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;
.super Ljava/lang/Object;
.source "VNSTTInterface.java"


# virtual methods
.method public abstract onClearScreen()V
.end method

.method public abstract onEndOfSpeech()V
.end method

.method public abstract onError(Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;)V
.end method

.method public abstract onResultSentence(Lcom/sec/android/app/voicenote/main/common/VNSTTData;)V
.end method

.method public abstract onResultWord(Lcom/sec/android/app/voicenote/main/common/VNSTTData;)V
.end method

.method public abstract onStartOfSpeech()V
.end method
