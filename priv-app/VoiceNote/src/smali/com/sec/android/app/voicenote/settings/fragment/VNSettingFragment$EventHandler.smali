.class Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment$EventHandler;
.super Landroid/os/Handler;
.source "VNSettingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EventHandler"
.end annotation


# instance fields
.field mHandler:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;)V
    .locals 1
    .param p1, "fragment"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;

    .prologue
    .line 404
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 402
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment$EventHandler;->mHandler:Ljava/lang/ref/WeakReference;

    .line 405
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment$EventHandler;->mHandler:Ljava/lang/ref/WeakReference;

    .line 406
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 409
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment$EventHandler;->mHandler:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;

    .line 410
    .local v0, "frgment":Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;
    if-eqz v0, :cond_0

    .line 411
    # invokes: Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->handleMessage(Landroid/os/Message;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;->access$000(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingFragment;Landroid/os/Message;)V

    .line 413
    :cond_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 414
    return-void
.end method
