.class public abstract Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;
.super Landroid/os/Binder;
.source "IVoiceNoteService.java"

# interfaces
.implements Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

.field static final TRANSACTION_addBookmark:I = 0x15

.field static final TRANSACTION_cancelRecording:I = 0x7

.field static final TRANSACTION_getDateAdded:I = 0xe

.field static final TRANSACTION_getFileName:I = 0xd

.field static final TRANSACTION_getFilePath:I = 0xc

.field static final TRANSACTION_getFileSize:I = 0xb

.field static final TRANSACTION_getLastSavedFileUriString:I = 0xf

.field static final TRANSACTION_getMaxAmplitude:I = 0x9

.field static final TRANSACTION_getMediaRecorderState:I = 0x8

.field static final TRANSACTION_getRealFreeSize:I = 0x10

.field static final TRANSACTION_getRecDuration:I = 0xa

.field static final TRANSACTION_getSTTState:I = 0x14

.field static final TRANSACTION_hideNotification:I = 0x12

.field static final TRANSACTION_initRecording:I = 0x1

.field static final TRANSACTION_isPaused:I = 0x19

.field static final TRANSACTION_isPlaying:I = 0x18

.field static final TRANSACTION_pauseRecording:I = 0x5

.field static final TRANSACTION_registerCallback:I = 0x16

.field static final TRANSACTION_resumeRecording:I = 0x6

.field static final TRANSACTION_saveRecording:I = 0x4

.field static final TRANSACTION_setFromVVM:I = 0x1b

.field static final TRANSACTION_showNotification:I = 0x11

.field static final TRANSACTION_startPreRecording:I = 0x3

.field static final TRANSACTION_startRecording:I = 0x2

.field static final TRANSACTION_stopPlay:I = 0x1a

.field static final TRANSACTION_turnOnSTT:I = 0x13

.field static final TRANSACTION_unregisterCallback:I = 0x17


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 271
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 42
    :sswitch_0
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v7, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 52
    .local v2, "_arg1":J
    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->initRecording(Ljava/lang/String;J)Z

    move-result v4

    .line 53
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 54
    if-eqz v4, :cond_0

    move v1, v6

    :cond_0
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 59
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":J
    .end local v4    # "_result":Z
    :sswitch_2
    const-string v7, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->startRecording()Z

    move-result v4

    .line 61
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 62
    if-eqz v4, :cond_1

    move v1, v6

    :cond_1
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 67
    .end local v4    # "_result":Z
    :sswitch_3
    const-string v7, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->startPreRecording()Z

    move-result v4

    .line 69
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 70
    if-eqz v4, :cond_2

    move v1, v6

    :cond_2
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 75
    .end local v4    # "_result":Z
    :sswitch_4
    const-string v7, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->saveRecording()Z

    move-result v4

    .line 77
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 78
    if-eqz v4, :cond_3

    move v1, v6

    :cond_3
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 83
    .end local v4    # "_result":Z
    :sswitch_5
    const-string v7, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->pauseRecording()Z

    move-result v4

    .line 85
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 86
    if-eqz v4, :cond_4

    move v1, v6

    :cond_4
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 91
    .end local v4    # "_result":Z
    :sswitch_6
    const-string v7, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->resumeRecording()Z

    move-result v4

    .line 93
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 94
    if-eqz v4, :cond_5

    move v1, v6

    :cond_5
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 99
    .end local v4    # "_result":Z
    :sswitch_7
    const-string v7, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->cancelRecording()Z

    move-result v4

    .line 101
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 102
    if-eqz v4, :cond_6

    move v1, v6

    :cond_6
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 107
    .end local v4    # "_result":Z
    :sswitch_8
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->getMediaRecorderState()I

    move-result v4

    .line 109
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 110
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 115
    .end local v4    # "_result":I
    :sswitch_9
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->getMaxAmplitude()I

    move-result v4

    .line 117
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 118
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 123
    .end local v4    # "_result":I
    :sswitch_a
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->getRecDuration()J

    move-result-wide v4

    .line 125
    .local v4, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 126
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 131
    .end local v4    # "_result":J
    :sswitch_b
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->getFileSize()J

    move-result-wide v4

    .line 133
    .restart local v4    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 134
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 139
    .end local v4    # "_result":J
    :sswitch_c
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->getFilePath()Ljava/lang/String;

    move-result-object v4

    .line 141
    .local v4, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 142
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 147
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_d
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->getFileName()Ljava/lang/String;

    move-result-object v4

    .line 149
    .restart local v4    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 150
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 155
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_e
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->getDateAdded()Ljava/lang/String;

    move-result-object v4

    .line 157
    .restart local v4    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 158
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 163
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_f
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->getLastSavedFileUriString()Ljava/lang/String;

    move-result-object v4

    .line 165
    .restart local v4    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 166
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 171
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_10
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 173
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 174
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->getRealFreeSize(I)J

    move-result-wide v4

    .line 175
    .local v4, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 176
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 181
    .end local v0    # "_arg0":I
    .end local v4    # "_result":J
    :sswitch_11
    const-string v7, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->showNotification()Z

    move-result v4

    .line 183
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 184
    if-eqz v4, :cond_7

    move v1, v6

    :cond_7
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 189
    .end local v4    # "_result":Z
    :sswitch_12
    const-string v7, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->hideNotification()Z

    move-result v4

    .line 191
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 192
    if-eqz v4, :cond_8

    move v1, v6

    :cond_8
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 197
    .end local v4    # "_result":Z
    :sswitch_13
    const-string v7, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 199
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_9

    move v0, v6

    .line 200
    .local v0, "_arg0":Z
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->turnOnSTT(Z)V

    .line 201
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    :cond_9
    move v0, v1

    .line 199
    goto :goto_1

    .line 206
    :sswitch_14
    const-string v7, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->getSTTState()Z

    move-result v4

    .line 208
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 209
    if-eqz v4, :cond_a

    move v1, v6

    :cond_a
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 214
    .end local v4    # "_result":Z
    :sswitch_15
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->addBookmark()I

    move-result v4

    .line 216
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 217
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 222
    .end local v4    # "_result":I
    :sswitch_16
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    move-result-object v0

    .line 225
    .local v0, "_arg0":Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->registerCallback(Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;)V

    .line 226
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 231
    .end local v0    # "_arg0":Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;
    :sswitch_17
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 233
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;

    move-result-object v0

    .line 234
    .restart local v0    # "_arg0":Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->unregisterCallback(Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;)V

    .line 235
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 240
    .end local v0    # "_arg0":Lcom/sec/android/app/voicenote/common/service/IVoiceNoteServiceCallback;
    :sswitch_18
    const-string v7, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->isPlaying()Z

    move-result v4

    .line 242
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 243
    if-eqz v4, :cond_b

    move v1, v6

    :cond_b
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 248
    .end local v4    # "_result":Z
    :sswitch_19
    const-string v7, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->isPaused()Z

    move-result v4

    .line 250
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 251
    if-eqz v4, :cond_c

    move v1, v6

    :cond_c
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 256
    .end local v4    # "_result":Z
    :sswitch_1a
    const-string v1, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 257
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->stopPlay()V

    .line 258
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 263
    :sswitch_1b
    const-string v7, "com.sec.android.app.voicenote.common.service.IVoiceNoteService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 265
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_d

    move v0, v6

    .line 266
    .local v0, "_arg0":Z
    :goto_2
    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/service/IVoiceNoteService$Stub;->setFromVVM(Z)V

    .line 267
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    :cond_d
    move v0, v1

    .line 265
    goto :goto_2

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
