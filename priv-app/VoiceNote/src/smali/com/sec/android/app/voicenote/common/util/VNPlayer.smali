.class public Lcom/sec/android/app/voicenote/common/util/VNPlayer;
.super Ljava/lang/Object;
.source "VNPlayer.java"


# static fields
.field private static final FADE_IN:I = 0x1f

.field private static final FADE_INITIAL:I = 0x20

.field public static final PLAYER_FF_STATE:I = 0x1a

.field public static final PLAYER_IDLE_STATE:I = 0x15

.field public static final PLAYER_PAUSE_STATE:I = 0x18

.field public static final PLAYER_PLAY_STATE:I = 0x17

.field public static final PLAYER_PREPARE_STATE:I = 0x16

.field public static final PLAYER_REW_STATE:I = 0x19

.field private static final TAG:Ljava/lang/String; = "VNPlayer"

.field private static mPlaySpeedIndex:I


# instance fields
.field private mCompleteListner:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mContentUri:Landroid/net/Uri;

.field private mFadeInHandler:Landroid/os/Handler;

.field private mID:J

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMediaPlayerState:I

.field private mSkipSlienceMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mPlaySpeedIndex:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mContentUri:Landroid/net/Uri;

    .line 37
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 38
    const/16 v0, 0x15

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayerState:I

    .line 47
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mID:J

    .line 49
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mCompleteListner:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mSkipSlienceMode:Z

    .line 110
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer$1;-><init>(Lcom/sec/android/app/voicenote/common/util/VNPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mFadeInHandler:Landroid/os/Handler;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/util/VNPlayer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mFadeInHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public getContentURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mContentUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getCurrentPlayingID()J
    .locals 2

    .prologue
    .line 278
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mID:J

    return-wide v0
.end method

.method public getCurrentPosition()I
    .locals 4

    .prologue
    .line 87
    const/4 v1, -0x1

    .line 88
    .local v1, "pos":I
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    .line 90
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getCurrentPosition()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 96
    :cond_0
    :goto_0
    return v1

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v2, "VNPlayer"

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getDuration()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 250
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    .line 252
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getDuration()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 258
    :cond_0
    :goto_0
    return v1

    .line 253
    :catch_0
    move-exception v0

    .line 254
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v2, "VNPlayer"

    const-string v3, "getDuration failed - MediaPlayer is in a wrong state"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayerState:I

    return v0
.end method

.method public isPaused()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayerState:I

    const/16 v2, 0x18

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 59
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v2, :cond_0

    .line 69
    :goto_0
    return v1

    .line 63
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    goto :goto_0

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 67
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 68
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public isPrepared()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayerState:I

    const/16 v2, 0x16

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSkipSilenceMode()Z
    .locals 3

    .prologue
    .line 139
    const-string v0, "VNPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isSkipSilenceMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mSkipSlienceMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mSkipSlienceMode:Z

    return v0
.end method

.method public pausePlay()Z
    .locals 2

    .prologue
    .line 214
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayerState:I

    const/16 v1, 0x17

    if-eq v0, v1, :cond_0

    .line 215
    const/4 v0, 0x0

    .line 221
    :goto_0
    return v0

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 220
    :cond_1
    const/16 v0, 0x18

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayerState:I

    .line 221
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 242
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 243
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mID:J

    .line 245
    :cond_0
    return-void
.end method

.method public resetPath()V
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mContentUri:Landroid/net/Uri;

    .line 248
    return-void
.end method

.method public resumePlay()V
    .locals 2

    .prologue
    .line 224
    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayerState:I

    const/16 v1, 0x18

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayerState:I

    const/16 v1, 0x16

    if-eq v0, v1, :cond_0

    .line 235
    :goto_0
    return-void

    .line 227
    :cond_0
    sget v0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mPlaySpeedIndex:I

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getPlaySpeedIndex()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 228
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getPlaySpeedIndex()I

    move-result v0

    sput v0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mPlaySpeedIndex:I

    .line 229
    sget v0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mPlaySpeedIndex:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->setPlaySpeed(I)V

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 234
    :cond_2
    const/16 v0, 0x17

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayerState:I

    goto :goto_0
.end method

.method public seek(I)V
    .locals 3
    .param p1, "msec"    # I

    .prologue
    .line 268
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_0

    .line 276
    :goto_0
    return-void

    .line 271
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->seekTo(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 273
    :catch_0
    move-exception v0

    .line 274
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "VNPlayer"

    const-string v2, "Seek failed - MediaPlayer is in a wrong state"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setOnCompletionListener(Ljava/lang/Object;)V
    .locals 2
    .param p1, "callback"    # Ljava/lang/Object;

    .prologue
    .line 203
    if-eqz p1, :cond_0

    .line 204
    check-cast p1, Landroid/media/MediaPlayer$OnCompletionListener;

    .end local p1    # "callback":Ljava/lang/Object;
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mCompleteListner:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mCompleteListner:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 207
    :cond_0
    return-void
.end method

.method public setPlaySpeed(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 281
    const-string v4, "VNPlayer"

    const-string v5, "setPlaySpeed()"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const/high16 v3, 0x3f800000    # 1.0f

    .line 283
    .local v3, "speed":F
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v4, :cond_0

    .line 314
    :goto_0
    return-void

    .line 285
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 299
    :pswitch_0
    const/high16 v3, 0x3f800000    # 1.0f

    .line 302
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->newRequest()Landroid/os/Parcel;

    move-result-object v2

    .line 303
    .local v2, "request":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 305
    .local v1, "reply":Landroid/os/Parcel;
    const/16 v4, 0x400

    :try_start_0
    invoke-virtual {v2, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 306
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeFloat(F)V

    .line 307
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4, v2, v1}, Landroid/media/MediaPlayer;->setSoundAlive(Landroid/os/Parcel;Landroid/os/Parcel;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 312
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    .line 288
    .end local v1    # "reply":Landroid/os/Parcel;
    .end local v2    # "request":Landroid/os/Parcel;
    :pswitch_1
    const/high16 v3, 0x3fc00000    # 1.5f

    .line 289
    goto :goto_1

    .line 292
    :pswitch_2
    const/high16 v3, 0x40000000    # 2.0f

    .line 293
    goto :goto_1

    .line 296
    :pswitch_3
    const/high16 v3, 0x3f000000    # 0.5f

    .line 297
    goto :goto_1

    .line 308
    .restart local v1    # "reply":Landroid/os/Parcel;
    .restart local v2    # "request":Landroid/os/Parcel;
    :catch_0
    move-exception v0

    .line 309
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v4, "VNPlayer"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 311
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 312
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    .line 311
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 312
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v4

    .line 285
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setSkipSilenceMode(Z)V
    .locals 3
    .param p1, "onoff"    # Z

    .prologue
    .line 144
    const-string v0, "VNPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSkipSilenceMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mSkipSlienceMode:Z

    .line 146
    return-void
.end method

.method public startFadeIn()V
    .locals 5

    .prologue
    const/16 v1, 0x1f

    const/16 v4, 0x20

    .line 99
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->volumeCtrl(F)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mFadeInHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mFadeInHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mFadeInHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mFadeInHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 106
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->volumeCtrl(F)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mFadeInHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mFadeInHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 109
    return-void
.end method

.method public startPlay(JLjava/lang/Object;Landroid/content/Context;Z)Z
    .locals 7
    .param p1, "id"    # J
    .param p3, "callback"    # Ljava/lang/Object;
    .param p4, "context"    # Landroid/content/Context;
    .param p5, "directPlay"    # Z

    .prologue
    const/4 v3, 0x0

    .line 149
    const-string v4, "VNPlayer"

    const-string v5, "startPlay"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayerState:I

    const/16 v5, 0x15

    if-eq v4, v5, :cond_0

    .line 200
    .end local p3    # "callback":Ljava/lang/Object;
    :goto_0
    return v3

    .line 153
    .restart local p3    # "callback":Ljava/lang/Object;
    :cond_0
    invoke-static {p4, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 154
    .local v2, "path":Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 155
    :cond_1
    const-string v4, "VNPlayer"

    const-string v5, "startPlay : path is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :cond_2
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 159
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v4

    if-nez v4, :cond_3

    .line 160
    const-string v4, "VNPlayer"

    const-string v5, "startPlay : file doesn\'t exsit"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 163
    :cond_3
    const-string v4, "VNPlayer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startPlay : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    new-instance v4, Landroid/media/MediaPlayer;

    invoke-direct {v4}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 166
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mID:J

    .line 167
    iget-wide v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mID:J

    invoke-static {p4, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getContentURI(Landroid/content/Context;J)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mContentUri:Landroid/net/Uri;

    .line 168
    if-eqz p3, :cond_4

    .line 169
    check-cast p3, Landroid/media/MediaPlayer$OnCompletionListener;

    .end local p3    # "callback":Ljava/lang/Object;
    iput-object p3, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mCompleteListner:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 171
    :cond_4
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setPlaySpeedIndex(I)V

    .line 173
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 174
    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mSkipSlienceMode:Z

    if-eqz v4, :cond_6

    .line 175
    const-string v4, "VNPlayer"

    const-string v5, "STREAM_VOICENOTE"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v5, 0xd

    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 180
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mCompleteListner:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 181
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->prepare()V

    .line 182
    const/16 v4, 0x16

    iput v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayerState:I

    .line 183
    if-eqz p5, :cond_7

    .line 184
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->start()V

    .line 185
    const/16 v4, 0x17

    iput v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayerState:I

    .line 190
    :goto_2
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->isGateEnabled()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 191
    const-string v4, "GATE"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<GATE-M> AUDIO_PLAYING : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " </GATE-M>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->noI(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_5
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 178
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 193
    :catch_0
    move-exception v0

    .line 194
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "VNPlayer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 187
    .end local v0    # "e":Ljava/io/IOException;
    :cond_7
    const/16 v4, 0x18

    :try_start_1
    iput v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayerState:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 196
    :catch_1
    move-exception v0

    .line 197
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v4, "VNPlayer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IllegalStateException error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public stopPlay()V
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->release()V

    .line 210
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setPlaySpeedIndex(I)V

    .line 211
    const/16 v0, 0x15

    iput v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayerState:I

    .line 212
    return-void
.end method

.method public switchSkipSilenceMode()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 134
    const-string v3, "VNPlayer"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "switchSkipSilenceMode : "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v4, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mSkipSlienceMode:Z

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " -> "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mSkipSlienceMode:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mSkipSlienceMode:Z

    if-nez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mSkipSlienceMode:Z

    .line 136
    return-void

    :cond_0
    move v0, v2

    .line 134
    goto :goto_0

    :cond_1
    move v1, v2

    .line 135
    goto :goto_1
.end method

.method public volumeCtrl(F)V
    .locals 1
    .param p1, "volume"    # F

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 57
    :cond_0
    return-void
.end method
