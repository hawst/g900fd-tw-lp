.class public interface abstract Lcom/sec/android/app/voicenote/common/util/IVNOperation;
.super Ljava/lang/Object;
.source "IVNOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;
    }
.end annotation


# virtual methods
.method public abstract askOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/DialogFragment;",
            ">;"
        }
    .end annotation
.end method

.method public abstract startOperation(Landroid/app/Fragment;Ljava/util/ArrayList;Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Fragment;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/sec/android/app/voicenote/common/util/IVNOperation$OperationCallback;",
            ")V"
        }
    .end annotation
.end method
