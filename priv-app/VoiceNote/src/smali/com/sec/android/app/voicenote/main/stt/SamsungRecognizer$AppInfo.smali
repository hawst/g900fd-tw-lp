.class Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$AppInfo;
.super Ljava/lang/Object;
.source "SamsungRecognizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AppInfo"
.end annotation


# static fields
.field public static final AppId:Ljava/lang/String; = "Samsung_Android_VoiceNoteV2_20130911"

.field public static final AppId_SSL:Ljava/lang/String; = "Samsung_Android_VoiceNoteSSL_20140227"

.field public static final AppKey:[B

.field public static final AppKey_SSL:[B

.field public static final Port:I = 0x1bb

.field public static final Port_SSL:I = 0x1bb


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x40

    .line 316
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$AppInfo;->AppKey:[B

    .line 328
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$AppInfo;->AppKey_SSL:[B

    return-void

    .line 316
    nop

    :array_0
    .array-data 1
        -0x3bt
        -0x33t
        0x28t
        -0x23t
        -0x33t
        -0x58t
        0x10t
        -0x30t
        0x34t
        -0x49t
        -0x10t
        0x4dt
        -0x71t
        0x5ft
        0x31t
        0x35t
        -0x11t
        -0x4ct
        -0x14t
        0x67t
        -0x65t
        0x1bt
        -0x3ft
        -0x7t
        -0x1t
        0x31t
        -0x19t
        -0x3ct
        -0x1bt
        -0x15t
        -0x7ct
        0x25t
        -0x1et
        -0x2t
        0x3ft
        0x2t
        0x12t
        -0x7ft
        0x12t
        0xbt
        0x7t
        -0x31t
        0x64t
        0x42t
        -0x2t
        -0x4et
        0x56t
        -0x44t
        0x53t
        -0x43t
        0x5dt
        -0x58t
        0x36t
        -0x8t
        0x10t
        -0x70t
        -0x61t
        -0x3et
        0x42t
        -0x21t
        0x7et
        -0xdt
        0x53t
        -0x5et
    .end array-data

    .line 328
    :array_1
    .array-data 1
        0x64t
        0x5et
        -0x27t
        0x49t
        0x1bt
        -0x7ft
        0x46t
        -0x13t
        -0x53t
        0x52t
        0x48t
        0x38t
        -0x6t
        -0x2bt
        -0x32t
        0xct
        0x5t
        -0x29t
        -0x57t
        0x66t
        0x18t
        0x54t
        0x4ft
        -0x65t
        -0x70t
        -0x6dt
        -0x4ft
        0x2ct
        -0x15t
        0x7t
        -0x6at
        0x34t
        -0x69t
        -0x4bt
        0x70t
        0x43t
        -0x7et
        -0x4ct
        0x41t
        -0x49t
        -0x7ct
        0x1dt
        -0x26t
        -0x16t
        0x20t
        -0x40t
        -0x5dt
        0x4ct
        -0x52t
        -0x3at
        0x52t
        -0x60t
        0x4bt
        -0x48t
        0x4ct
        -0x38t
        -0x7at
        0x5bt
        -0x73t
        0x3at
        0x23t
        -0x5bt
        0x1t
        0x10t
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDNS(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "code"    # Ljava/lang/String;

    .prologue
    .line 339
    const v0, 0x7f0b0131

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    const-string v0, "eo.nvc.deudeu.nuancemobility.net"

    .line 364
    :goto_0
    return-object v0

    .line 341
    :cond_0
    const v0, 0x7f0b013a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 342
    const-string v0, "eo.nvc.enggbr.nuancemobility.net"

    goto :goto_0

    .line 343
    :cond_1
    const v0, 0x7f0b013b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 344
    const-string v0, "eo.nvc.engusa.nuancemobility.net"

    goto :goto_0

    .line 345
    :cond_2
    const v0, 0x7f0b0133

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 346
    const-string v0, "eo.nvc.spaesp.nuancemobility.net"

    goto :goto_0

    .line 347
    :cond_3
    const v0, 0x7f0b0132

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 348
    const-string v0, "eo.nvc.spaxla.nuancemobility.net"

    goto :goto_0

    .line 349
    :cond_4
    const v0, 0x7f0b0134

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 350
    const-string v0, "eo.nvc.frafra.nuancemobility.net"

    goto :goto_0

    .line 351
    :cond_5
    const v0, 0x7f0b0135

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 352
    const-string v0, "eo.nvc.itaita.nuancemobility.net"

    goto :goto_0

    .line 353
    :cond_6
    const v0, 0x7f0b0139

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 354
    const-string v0, "eo.nvc.rusrus.nuancemobility.net"

    goto :goto_0

    .line 355
    :cond_7
    const v0, 0x7f0b0138

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 356
    const-string v0, "eo.nvc.porbra.nuancemobility.net"

    goto :goto_0

    .line 357
    :cond_8
    const v0, 0x7f0b0130

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 358
    const-string v0, "eo.nvc.cmnchn.nuancemobility.net"

    goto/16 :goto_0

    .line 359
    :cond_9
    const v0, 0x7f0b0136

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 360
    const-string v0, "eo.nvc.jpnjpn.nuancemobility.net"

    goto/16 :goto_0

    .line 361
    :cond_a
    const v0, 0x7f0b0137

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 362
    const-string v0, "eo.nvc.krkr.nuancemobility.net"

    goto/16 :goto_0

    .line 364
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public static getDNS(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "code"    # Ljava/lang/String;
    .param p2, "isSSL"    # Z

    .prologue
    .line 368
    if-nez p2, :cond_0

    .line 369
    invoke-static {p0, p1}, Lcom/sec/android/app/voicenote/main/stt/SamsungRecognizer$AppInfo;->getDNS(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 397
    :goto_0
    return-object v0

    .line 372
    :cond_0
    const v0, 0x7f0b0131

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 373
    const-string v0, "gg.nvc.deudeu.nuancemobility.net"

    goto :goto_0

    .line 374
    :cond_1
    const v0, 0x7f0b013a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 375
    const-string v0, "gg.nvc.enggbr.nuancemobility.net"

    goto :goto_0

    .line 376
    :cond_2
    const v0, 0x7f0b013b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 377
    const-string v0, "gg.nvc.engusa.nuancemobility.net"

    goto :goto_0

    .line 378
    :cond_3
    const v0, 0x7f0b0133

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 379
    const-string v0, "gg.nvc.spaesp.nuancemobility.net"

    goto :goto_0

    .line 380
    :cond_4
    const v0, 0x7f0b0132

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 381
    const-string v0, "gg.nvc.spaxla.nuancemobility.net"

    goto :goto_0

    .line 382
    :cond_5
    const v0, 0x7f0b0134

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 383
    const-string v0, "gg.nvc.frafra.nuancemobility.net"

    goto :goto_0

    .line 384
    :cond_6
    const v0, 0x7f0b0135

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 385
    const-string v0, "gg.nvc.itaita.nuancemobility.net"

    goto :goto_0

    .line 386
    :cond_7
    const v0, 0x7f0b0139

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 387
    const-string v0, "gg.nvc.rusrus.nuancemobility.net"

    goto :goto_0

    .line 388
    :cond_8
    const v0, 0x7f0b0138

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 389
    const-string v0, "gg.nvc.porbra.nuancemobility.net"

    goto/16 :goto_0

    .line 390
    :cond_9
    const v0, 0x7f0b0130

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 391
    const-string v0, "gg.nvc.cmnchn.nuancemobility.net"

    goto/16 :goto_0

    .line 392
    :cond_a
    const v0, 0x7f0b0136

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 393
    const-string v0, "gg.nvc.jpnjpn.nuancemobility.net"

    goto/16 :goto_0

    .line 394
    :cond_b
    const v0, 0x7f0b0137

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 395
    const-string v0, "gg.nvc.korkor.nuancemobility.net"

    goto/16 :goto_0

    .line 397
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
