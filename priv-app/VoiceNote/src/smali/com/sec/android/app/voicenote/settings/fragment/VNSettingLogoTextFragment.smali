.class public Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;
.super Landroid/app/Fragment;
.source "VNSettingLogoTextFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;
    }
.end annotation


# static fields
.field public static final BGCOLOR:Ljava/lang/String; = "bgcolor"

.field public static final FONT:Ljava/lang/String; = "font"

.field public static final LOGO_TEXT_MAX_LINE:I = 0x2

.field public static final OKSTATE:Ljava/lang/String; = "ok"

.field public static final TAG:Ljava/lang/String; = "VNSettingLogoTextFragment"

.field public static final TEXTCOLOR:Ljava/lang/String; = "textcolor"

.field public static misInputMethodShown:Z


# instance fields
.field private final COLORS:[I

.field private final colorChangedListener:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;

.field labelTextWatcher:Landroid/text/TextWatcher;

.field private mCancelButton:Landroid/widget/Button;

.field private mColor:[Landroid/widget/ImageView;

.field private mColorPalette:I

.field private mDoneButton:Landroid/widget/Button;

.field private mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

.field private mGradientView:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;

.field mLogoBG:Landroid/widget/ImageView;

.field private mLogoText:Landroid/widget/EditText;

.field private mLogoTextChanged:Z

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSaveBGColor:I

.field private mSaveColor:I

.field private mSaveFont:I

.field private mTextBGColorbtn:Landroid/widget/ImageButton;

.field private mTextColorbtn:Landroid/widget/ImageButton;

.field private mTextFont:Landroid/widget/Spinner;

.field private mToast:Landroid/widget/Toast;

.field private mbOKEnable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->misInputMethodShown:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/16 v7, 0x30

    const/16 v6, 0x13

    const/4 v5, 0x0

    const/16 v4, 0xff

    const/4 v0, 0x0

    .line 85
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 59
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    .line 61
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    .line 62
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoBG:Landroid/widget/ImageView;

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextColorbtn:Landroid/widget/ImageButton;

    .line 65
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextBGColorbtn:Landroid/widget/ImageButton;

    .line 66
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    .line 67
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mCancelButton:Landroid/widget/Button;

    .line 68
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextFont:Landroid/widget/Spinner;

    .line 69
    iput v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveFont:I

    .line 70
    iput v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveColor:I

    .line 71
    iput v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveBGColor:I

    .line 72
    iput-boolean v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z

    .line 73
    iput-boolean v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoTextChanged:Z

    .line 74
    iput v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColorPalette:I

    .line 76
    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 78
    const/16 v0, 0x10

    new-array v0, v0, [I

    invoke-static {v4, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v5

    const/4 v1, 0x1

    const/16 v2, 0xfd

    const/16 v3, 0x2d

    invoke-static {v2, v4, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x83

    const/16 v3, 0x5d

    invoke-static {v4, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x3b

    const/16 v3, 0x5b

    invoke-static {v4, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x49

    const/16 v3, 0xc9

    invoke-static {v4, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0xca

    const/16 v3, 0x85

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x38

    const/16 v3, 0xa8

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x33

    const/16 v3, 0x67

    const/16 v4, 0xfd

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x9f

    const/16 v3, 0xc7

    invoke-static {v2, v3, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const/4 v2, 0x1

    const/16 v3, 0x94

    const/16 v4, 0x2e

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa

    const/4 v2, 0x4

    const/16 v3, 0x67

    const/16 v4, 0x2e

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0xa6

    const/16 v3, 0xa5

    const/16 v4, 0xa5

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x73

    const/16 v3, 0x72

    const/16 v4, 0x72

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xd

    invoke-static {v7, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xe

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf

    const v2, 0x106000d

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    .line 307
    new-instance v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$3;-><init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->colorChangedListener:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;

    .line 319
    new-instance v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$4;-><init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->labelTextWatcher:Landroid/text/TextWatcher;

    .line 86
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->enableDoneButton()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Landroid/widget/Button;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;
    .param p1, "x1"    # Landroid/widget/Button;
    .param p2, "x2"    # Z

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setEnableButton(Landroid/widget/Button;Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;)Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;
    .param p1, "x1"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mCancelButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextColorbtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;
    .param p1, "x1"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColorPalette:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)[Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;
    .param p1, "x1"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;
    .param p1, "x1"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z

    return p1
.end method

.method private enableDoneButton()Z
    .locals 9

    .prologue
    const/16 v8, 0x19

    const/16 v7, 0x15

    const/4 v5, 0x3

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 509
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 510
    .local v0, "trimmedStr":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 538
    :cond_0
    :goto_0
    return v1

    .line 513
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 514
    goto :goto_0

    .line 515
    :cond_2
    const-string v3, "logo_text_temp"

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 516
    const-string v3, "logo_text_font_temp"

    invoke-static {v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveFont:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 517
    goto :goto_0

    .line 519
    :cond_3
    const-string v3, "logo_text_color_temp"

    const/16 v4, 0xac

    const/16 v5, 0xea

    const/16 v6, 0xff

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveColor:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 520
    goto :goto_0

    .line 522
    :cond_4
    const-string v3, "logo_bg_color_temp"

    const/16 v4, 0x1c

    invoke-static {v7, v8, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveBGColor:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 523
    goto :goto_0

    .line 526
    :cond_5
    const-string v3, "logo_text_font"

    invoke-static {v3, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveFont:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 527
    goto :goto_0

    .line 529
    :cond_6
    const-string v3, "logo_text_color"

    const/16 v4, 0xac

    const/16 v5, 0xea

    const/16 v6, 0xff

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveColor:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 530
    goto :goto_0

    .line 532
    :cond_7
    const-string v3, "logo_bg_color"

    const/16 v4, 0x1c

    invoke-static {v7, v8, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveBGColor:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 533
    goto/16 :goto_0

    .line 535
    :cond_8
    const-string v3, "logo_type_temp"

    invoke-static {v3, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 536
    goto/16 :goto_0
.end method

.method private maxLineOverCheck()V
    .locals 4

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$2;-><init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)V

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 305
    return-void
.end method

.method private setEnableButton(Landroid/widget/Button;Z)V
    .locals 2
    .param p1, "v"    # Landroid/widget/Button;
    .param p2, "enable"    # Z

    .prologue
    .line 542
    if-eqz p2, :cond_0

    .line 543
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080041

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 544
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 549
    :goto_0
    return-void

    .line 546
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 547
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method private setSelectedBtn(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 361
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 362
    if-ne v0, p1, :cond_0

    .line 363
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 361
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 365
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_1

    .line 368
    :cond_1
    return-void
.end method

.method private updateColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 486
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextColorbtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 488
    iput p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveColor:I

    .line 502
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->enableDoneButton()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setEnableButton(Landroid/widget/Button;Z)V

    .line 506
    :cond_0
    return-void

    .line 490
    :cond_1
    const v0, 0x106000d

    if-ne p1, v0, :cond_3

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoBG:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoBG:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 500
    :cond_2
    :goto_1
    iput p1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveBGColor:I

    goto :goto_0

    .line 495
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoBG:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_4

    .line 496
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoBG:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 498
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoBG:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_1
.end method


# virtual methods
.method protected finishCreateView()V
    .locals 3

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextFont:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextFont:Landroid/widget/Spinner;

    iget v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveFont:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    iget v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveFont:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->getFont(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 232
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v7, 0xff

    const/4 v6, 0x2

    const/16 v5, 0xf

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 372
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 373
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 483
    :goto_0
    return-void

    .line 375
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextColorbtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 376
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextBGColorbtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 377
    iget v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColorPalette:I

    if-nez v1, :cond_0

    .line 378
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    aget-object v1, v1, v5

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 380
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    aget-object v1, v1, v5

    iget v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColorPalette:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 385
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextColorbtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 386
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextBGColorbtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 387
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    aget-object v1, v1, v5

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200d8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 391
    :sswitch_2
    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 392
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    aget v1, v1, v3

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto :goto_0

    .line 395
    :sswitch_3
    invoke-direct {p0, v4}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 396
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    aget v1, v1, v4

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto :goto_0

    .line 399
    :sswitch_4
    invoke-direct {p0, v6}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 400
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    aget v1, v1, v6

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto :goto_0

    .line 403
    :sswitch_5
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 404
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    const/4 v2, 0x3

    aget v1, v1, v2

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto :goto_0

    .line 407
    :sswitch_6
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 408
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    const/4 v2, 0x4

    aget v1, v1, v2

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto/16 :goto_0

    .line 411
    :sswitch_7
    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 412
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    const/4 v2, 0x5

    aget v1, v1, v2

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto/16 :goto_0

    .line 415
    :sswitch_8
    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 416
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    const/4 v2, 0x6

    aget v1, v1, v2

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto/16 :goto_0

    .line 419
    :sswitch_9
    const/4 v1, 0x7

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 420
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    const/4 v2, 0x7

    aget v1, v1, v2

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto/16 :goto_0

    .line 423
    :sswitch_a
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 424
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    const/16 v2, 0x8

    aget v1, v1, v2

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto/16 :goto_0

    .line 427
    :sswitch_b
    const/16 v1, 0x9

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 428
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    const/16 v2, 0x9

    aget v1, v1, v2

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto/16 :goto_0

    .line 431
    :sswitch_c
    const/16 v1, 0xa

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 432
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    const/16 v2, 0xa

    aget v1, v1, v2

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto/16 :goto_0

    .line 435
    :sswitch_d
    const/16 v1, 0xb

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 436
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    const/16 v2, 0xb

    aget v1, v1, v2

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto/16 :goto_0

    .line 439
    :sswitch_e
    const/16 v1, 0xc

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 440
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    const/16 v2, 0xc

    aget v1, v1, v2

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto/16 :goto_0

    .line 443
    :sswitch_f
    const/16 v1, 0xd

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 444
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    const/16 v2, 0xd

    aget v1, v1, v2

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto/16 :goto_0

    .line 447
    :sswitch_10
    const/16 v1, 0xe

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 448
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    const/16 v2, 0xe

    aget v1, v1, v2

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto/16 :goto_0

    .line 451
    :sswitch_11
    invoke-direct {p0, v5}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setSelectedBtn(I)V

    .line 452
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextColorbtn:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 453
    iget v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColorPalette:I

    if-nez v1, :cond_1

    invoke-static {v7, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    :goto_1
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto/16 :goto_0

    :cond_1
    iget v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColorPalette:I

    goto :goto_1

    .line 455
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    aget v1, v1, v5

    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->updateColor(I)V

    goto/16 :goto_0

    .line 460
    :sswitch_12
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 461
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->popBackStack()V

    goto/16 :goto_0

    .line 465
    :sswitch_13
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 466
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    if-nez v1, :cond_3

    .line 467
    new-instance v1, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;-><init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$1;)V

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 470
    :cond_3
    const-string v1, "logo_text_temp"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    const-string v1, "logo_text_font_temp"

    iget v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveFont:I

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 472
    const-string v1, "logo_text_color_temp"

    iget v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveColor:I

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 473
    const-string v1, "logo_bg_color_temp"

    iget v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveBGColor:I

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 474
    const-string v1, "logo_text_font_typeface_temp"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    iget v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveFont:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->getFontandPackageName(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    const-string v1, "logo_type_temp"

    invoke-static {v1, v6}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 477
    const-string v1, "change_notification_resume"

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 478
    iput-boolean v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoTextChanged:Z

    .line 480
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->popBackStack()V

    goto/16 :goto_0

    .line 373
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0e0001 -> :sswitch_12
        0x7f0e0002 -> :sswitch_13
        0x7f0e0046 -> :sswitch_0
        0x7f0e0047 -> :sswitch_1
        0x7f0e007e -> :sswitch_2
        0x7f0e007f -> :sswitch_3
        0x7f0e0080 -> :sswitch_4
        0x7f0e0081 -> :sswitch_5
        0x7f0e0082 -> :sswitch_6
        0x7f0e0083 -> :sswitch_7
        0x7f0e0084 -> :sswitch_8
        0x7f0e0085 -> :sswitch_9
        0x7f0e0086 -> :sswitch_a
        0x7f0e0087 -> :sswitch_b
        0x7f0e0088 -> :sswitch_c
        0x7f0e0089 -> :sswitch_d
        0x7f0e008a -> :sswitch_e
        0x7f0e008b -> :sswitch_f
        0x7f0e008c -> :sswitch_10
        0x7f0e008d -> :sswitch_11
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 90
    const-string v0, "VNSettingLogoTextFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    if-eqz p1, :cond_0

    .line 93
    const-string v0, "ok"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z

    .line 96
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setHasOptionsMenu(Z)V

    .line 98
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 99
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v10, 0x61

    const/16 v9, 0x16

    const/16 v8, 0x8

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 113
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/VNActionBar;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;-><init>(Landroid/app/Activity;)V

    .line 114
    .local v0, "actionBar":Lcom/sec/android/app/voicenote/common/util/VNActionBar;
    const/high16 v4, 0x7f030000

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 115
    .local v1, "actionbar_v":Landroid/view/View;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNActionBar;->show_selectMode(Landroid/view/View;)V

    .line 116
    const v4, 0x7f0e0001

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mCancelButton:Landroid/widget/Button;

    .line 117
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setFocusable(Z)V

    .line 119
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 120
    const v4, 0x7f0e0002

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    .line 121
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setFocusable(Z)V

    .line 123
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 124
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    iget-boolean v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setEnableButton(Landroid/widget/Button;Z)V

    .line 126
    const v4, 0x7f03001b

    invoke-virtual {p1, v4, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 128
    .local v3, "v":Landroid/view/View;
    const v4, 0x7f0e000c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mGradientView:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;

    .line 129
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mGradientView:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;

    const/high16 v5, -0x1000000

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->setBackgroundColor(I)V

    .line 130
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mGradientView:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->colorChangedListener:Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/common/util/GradientColorPicker;->setColorchangedListener(Lcom/sec/android/app/voicenote/common/util/GradientColorPicker$ColorChangedListener;)V

    .line 132
    const v4, 0x7f0e0041

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    .line 133
    const v4, 0x7f0e0040

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoBG:Landroid/widget/ImageView;

    .line 135
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->labelTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 136
    const v4, 0x7f0e0046

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextColorbtn:Landroid/widget/ImageButton;

    .line 137
    const v4, 0x7f0e0047

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextBGColorbtn:Landroid/widget/ImageButton;

    .line 138
    const v4, 0x7f0e0048

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextFont:Landroid/widget/Spinner;

    .line 140
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextColorbtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 141
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextColorbtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextBGColorbtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 143
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextBGColorbtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    if-nez v4, :cond_0

    .line 146
    new-instance v4, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;-><init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$1;)V

    new-array v5, v7, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$LoadListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 149
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mTextFont:Landroid/widget/Spinner;

    invoke-virtual {v4, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 151
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    array-length v4, v4

    new-array v4, v4, [Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    .line 152
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const v4, 0x7f0e007e

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v7

    .line 153
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const v4, 0x7f0e007f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v6

    .line 154
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const/4 v6, 0x2

    const v4, 0x7f0e0080

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v6

    .line 155
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const/4 v6, 0x3

    const v4, 0x7f0e0081

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v6

    .line 156
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const/4 v6, 0x4

    const v4, 0x7f0e0082

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v6

    .line 157
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const/4 v6, 0x5

    const v4, 0x7f0e0083

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v6

    .line 158
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const/4 v6, 0x6

    const v4, 0x7f0e0084

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v6

    .line 159
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const/4 v6, 0x7

    const v4, 0x7f0e0085

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v6

    .line 160
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const v4, 0x7f0e0086

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v8

    .line 161
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const/16 v6, 0x9

    const v4, 0x7f0e0087

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v6

    .line 162
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const/16 v6, 0xa

    const v4, 0x7f0e0088

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v6

    .line 163
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const/16 v6, 0xb

    const v4, 0x7f0e0089

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v6

    .line 164
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const/16 v6, 0xc

    const v4, 0x7f0e008a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v6

    .line 165
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const/16 v6, 0xd

    const v4, 0x7f0e008b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v6

    .line 166
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const/16 v6, 0xe

    const v4, 0x7f0e008c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v6

    .line 167
    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    const/16 v6, 0xf

    const v4, 0x7f0e008d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v6

    .line 168
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->COLORS:[I

    array-length v4, v4

    if-ge v2, v4, :cond_1

    .line 169
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mColor:[Landroid/widget/ImageView;

    aget-object v4, v4, v2

    invoke-virtual {v4, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 172
    :cond_1
    if-nez p3, :cond_5

    .line 173
    const-string v4, "logo_text_temp"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 174
    const-string v4, "logo_text_font_temp"

    invoke-static {v4, v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveFont:I

    .line 175
    const-string v4, "logo_text_color_temp"

    const/16 v5, 0xaa

    const/16 v6, 0xc4

    invoke-static {v7, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveColor:I

    .line 176
    const-string v4, "logo_bg_color_temp"

    const/16 v5, 0x6e

    invoke-static {v9, v10, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveBGColor:I

    .line 187
    :goto_1
    const-string v4, "logo_text_temp"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 188
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    const-string v5, "logo_text_temp"

    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    const-string v5, "logo_text_temp"

    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 195
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    iget v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveColor:I

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setTextColor(I)V

    .line 196
    iget v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveBGColor:I

    const v5, 0x106000d

    if-ne v4, v5, :cond_7

    .line 197
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoBG:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_2

    .line 198
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoBG:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 206
    :cond_2
    :goto_3
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->requestFocus()Z

    .line 207
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setSelection(I)V

    .line 209
    iput-boolean v7, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z

    .line 211
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    if-eqz v4, :cond_9

    .line 212
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->enableDoneButton()Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z

    .line 213
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    if-eqz v4, :cond_3

    .line 214
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    iget-boolean v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setEnableButton(Landroid/widget/Button;Z)V

    .line 225
    :cond_3
    :goto_4
    return-object v3

    .line 178
    :cond_4
    const-string v4, "logo_text_font"

    invoke-static {v4, v7}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveFont:I

    .line 179
    const-string v4, "logo_text_color"

    const/16 v5, 0xaa

    const/16 v6, 0xc4

    invoke-static {v7, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveColor:I

    .line 180
    const-string v4, "logo_bg_color"

    const/16 v5, 0x6e

    invoke-static {v9, v10, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveBGColor:I

    goto/16 :goto_1

    .line 183
    :cond_5
    const-string v4, "font"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveFont:I

    .line 184
    const-string v4, "textcolor"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveColor:I

    .line 185
    const-string v4, "bgcolor"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveBGColor:I

    goto/16 :goto_1

    .line 191
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    const-string v5, "logo_text"

    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 192
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    const-string v5, "logo_text"

    iget-object v6, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getStringSettings(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 201
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoBG:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_8

    .line 202
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoBG:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 204
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoBG:Landroid/widget/ImageView;

    iget v5, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveBGColor:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto/16 :goto_3

    .line 217
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    if-eqz v4, :cond_a

    .line 218
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    invoke-direct {p0, v4, v7}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setEnableButton(Landroid/widget/Button;Z)V

    .line 220
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mCancelButton:Landroid/widget/Button;

    if-eqz v4, :cond_3

    .line 221
    iget-object v4, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mCancelButton:Landroid/widget/Button;

    invoke-direct {p0, v4, v7}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setEnableButton(Landroid/widget/Button;Z)V

    goto/16 :goto_4
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 587
    const-string v1, "VNSettingLogoTextFragment"

    const-string v2, "onDestroyView"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 589
    .local v0, "actionbar":Landroid/app/ActionBar;
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 591
    if-eqz v0, :cond_0

    .line 592
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 594
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    .line 595
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 596
    iput-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 598
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mCancelButton:Landroid/widget/Button;

    if-eqz v1, :cond_2

    .line 599
    iput-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mCancelButton:Landroid/widget/Button;

    .line 601
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    if-eqz v1, :cond_3

    .line 602
    iput-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    .line 604
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mToast:Landroid/widget/Toast;

    if-eqz v1, :cond_4

    .line 605
    iput-object v3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mToast:Landroid/widget/Toast;

    .line 606
    :cond_4
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 607
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 274
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mFontListAdapter:Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;

    iput p3, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveFont:I

    invoke-virtual {v2, p3}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoFontListAdapter;->getFont(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 275
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->maxLineOverCheck()V

    .line 276
    if-eqz p2, :cond_0

    .line 277
    const v1, 0x7f0e0061

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 278
    .local v0, "title":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 279
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 282
    .end local v0    # "title":Landroid/view/View;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->enableDoneButton()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z

    .line 283
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    if-eqz v1, :cond_1

    .line 284
    iget-object v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mDoneButton:Landroid/widget/Button;

    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->setEnableButton(Landroid/widget/Button;Z)V

    .line 286
    :cond_1
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 290
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 236
    const-string v0, "VNSettingLogoTextFragment"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->misInputMethodShown:Z

    .line 239
    const-string v0, "logo_text_changed"

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoTextChanged:Z

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoTextChanged:Z

    .line 242
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 243
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 247
    const-string v0, "VNSettingLogoTextFragment"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoTextChanged:Z

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mLogoText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment$1;-><init>(Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 269
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 270
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 103
    const-string v0, "ok"

    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mbOKEnable:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 104
    const-string v0, "textcolor"

    iget v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveColor:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 105
    const-string v0, "bgcolor"

    iget v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveBGColor:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 106
    const-string v0, "font"

    iget v1, p0, Lcom/sec/android/app/voicenote/settings/fragment/VNSettingLogoTextFragment;->mSaveFont:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 108
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 109
    return-void
.end method
