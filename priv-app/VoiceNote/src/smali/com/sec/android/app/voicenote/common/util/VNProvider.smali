.class public Lcom/sec/android/app/voicenote/common/util/VNProvider;
.super Landroid/content/ContentProvider;
.source "VNProvider.java"


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.voicenote.common.util.VNProvider"

.field private static final ColumnCount:I = 0x8

.field private static final GLOBAL_SEARCH:I = 0x1

.field private static final REGEX_SEARCH:I = 0x3

.field private static final SUGGEST_COLUMNS:[Ljava/lang/String;

.field static final SearchTarget:[Ljava/lang/String;

.field private static final TAG_COLUMNS:[Ljava/lang/String;

.field private static final TAG_SEARCH:I = 0x2

.field private static final URI_MATCHER:Landroid/content/UriMatcher;

.field private static final datetimefomat:Ljava/lang/String; = "(\\d)([a-zA-Z])"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

.field private mEndTime:Ljava/lang/String;

.field private mLocationStr:Ljava/lang/String;

.field private mStartTime:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 48
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    .line 58
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.voicenote.common.util.VNProvider"

    const-string v2, "search_suggest_query"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 61
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.voicenote.common.util.VNProvider"

    const-string v2, "search_suggest_query/*"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 64
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.voicenote.common.util.VNProvider"

    const-string v2, "search_suggest_tag_query"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 65
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.voicenote.common.util.VNProvider"

    const-string v2, "search_suggest_tag_query/#"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 66
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.voicenote.common.util.VNProvider"

    const-string v2, "search_suggest_regex_query"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 69
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "suggest_text_1"

    aput-object v1, v0, v3

    const-string v1, "suggest_text_2"

    aput-object v1, v0, v4

    const-string v1, "suggest_text_3"

    aput-object v1, v0, v6

    const-string v1, "suggest_uri"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "suggest_mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_intent_extra_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->SUGGEST_COLUMNS:[Ljava/lang/String;

    .line 80
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "suggest_tag_type"

    aput-object v1, v0, v5

    const-string v1, "suggest_tag_value"

    aput-object v1, v0, v3

    const-string v1, "suggest_tag_content_uri"

    aput-object v1, v0, v4

    const-string v1, "suggest_tag_create_time"

    aput-object v1, v0, v6

    const-string v1, "suggest_tag_encode"

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->TAG_COLUMNS:[Ljava/lang/String;

    .line 88
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v0, v5

    const-string v1, "addr"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->SearchTarget:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 40
    const-string v0, "VNProvider"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->TAG:Ljava/lang/String;

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mStartTime:Ljava/lang/String;

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mEndTime:Ljava/lang/String;

    .line 54
    iput-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mLocationStr:Ljava/lang/String;

    return-void
.end method

.method private getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "date"    # J

    .prologue
    const/16 v4, 0x20

    .line 371
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    .line 372
    .local v2, "shortDateFormat":Ljava/text/DateFormat;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 373
    .local v0, "dateString":Ljava/lang/StringBuffer;
    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 374
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    const/16 v4, 0x81

    invoke-static {p1, p2, p3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 377
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 383
    :goto_0
    return-object v3

    .line 379
    :cond_0
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    const/16 v4, 0x141

    invoke-static {p1, p2, p3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 382
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 383
    .local v1, "datetime":Ljava/lang/String;
    const-string v3, "(\\d)([a-zA-Z])"

    const-string v4, "$1 $2"

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private getGlobalSearchCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13
    .param p1, "arg"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 166
    const-string v0, "VNProvider"

    const-string v1, "getGlobalSearchCursor"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const/4 v6, 0x0

    .line 169
    .local v6, "c":Landroid/database/Cursor;
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v0, "( "

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 171
    .local v8, "selection":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->SearchTarget:[Ljava/lang/String;

    array-length v0, v0

    if-ge v7, v0, :cond_1

    .line 172
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->SearchTarget:[Ljava/lang/String;

    aget-object v0, v0, v7

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    const-string v0, " Like ?"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    sget-object v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->SearchTarget:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ge v7, v0, :cond_0

    .line 176
    const-string v0, " ) OR ( "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 179
    :cond_1
    const-string v0, " )"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v10

    const-string v3, "title"

    aput-object v3, v2, v11

    const-string v3, "date_modified"

    aput-object v3, v2, v12

    const-string v3, "duration"

    aput-object v3, v2, v5

    const/4 v3, 0x4

    const-string v4, "mime_type"

    aput-object v4, v2, v3

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "%"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "%"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "%"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "%"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v11

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "%"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "%"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v12

    const-string v5, "title_key"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 195
    if-nez v6, :cond_2

    .line 196
    const/4 v0, 0x0

    .line 200
    :goto_1
    return-object v0

    .line 198
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 200
    invoke-direct {p0, v6}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getResultCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1
.end method

.method public static getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 537
    if-nez p0, :cond_0

    .line 538
    const-string p0, "_"

    .line 544
    :goto_0
    return-object p0

    .line 540
    :cond_0
    const-string v0, "!"

    const-string v1, "!!"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 541
    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 542
    const-string v0, "_"

    const-string v1, "!_"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 543
    const-string v0, "%"

    const-string v1, "!%"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 544
    goto :goto_0
.end method

.method private getResultCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 14
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v10, 0x0

    .line 117
    const-string v11, "VNProvider"

    const-string v12, "getResultCursor"

    invoke-static {v11, v12}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    new-instance v11, Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;-><init>(Landroid/content/Context;)V

    iput-object v11, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 120
    iget-object v11, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v11}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->LoadLabeltoMap()V

    .line 122
    new-instance v9, Landroid/database/MatrixCursor;

    sget-object v11, Lcom/sec/android/app/voicenote/common/util/VNProvider;->SUGGEST_COLUMNS:[Ljava/lang/String;

    invoke-direct {v9, v11}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 124
    .local v9, "result":Landroid/database/MatrixCursor;
    if-nez p1, :cond_0

    .line 125
    invoke-virtual {v9}, Landroid/database/MatrixCursor;->close()V

    move-object v9, v10

    .line 162
    .end local v9    # "result":Landroid/database/MatrixCursor;
    :goto_0
    return-object v9

    .line 129
    .restart local v9    # "result":Landroid/database/MatrixCursor;
    :cond_0
    const-string v11, "_id"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 130
    .local v2, "columnIdxID":I
    const-string v11, "title"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 131
    .local v6, "columnIdxTitle":I
    const-string v11, "date_modified"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 132
    .local v1, "columnIdxDate":I
    const-string v11, "duration"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 133
    .local v5, "columnIdxTime":I
    const-string v11, "mime_type"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 134
    .local v4, "columnIdxMime":I
    const-string v11, "label_id"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 136
    .local v3, "columnIdxLabel":I
    if-ltz v2, :cond_1

    if-ltz v6, :cond_1

    if-ltz v1, :cond_1

    if-gez v5, :cond_2

    .line 137
    :cond_1
    const-string v11, "VNProvider"

    const-string v12, "getResultCursor - return null"

    invoke-static {v11, v12}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-virtual {v9}, Landroid/database/MatrixCursor;->close()V

    .line 139
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    move-object v9, v10

    .line 140
    goto :goto_0

    .line 143
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    const/16 v10, 0x8

    invoke-direct {v0, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 144
    .local v0, "column":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 145
    .local v7, "count":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v7, :cond_3

    .line 146
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 148
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->stringForTime(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    sget-object v10, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-static {v10, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    iget-object v10, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getLabelColor(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    invoke-virtual {v9, v0}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    .line 158
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 145
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 160
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 161
    invoke-virtual {v9}, Landroid/database/MatrixCursor;->moveToFirst()Z

    goto/16 :goto_0
.end method

.method private getRexCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 19
    .param p1, "arg"    # Ljava/lang/String;

    .prologue
    .line 204
    const-string v2, "VNProvider"

    const-string v3, "getRexCursor"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const/4 v13, 0x0

    .line 207
    .local v13, "c":Landroid/database/Cursor;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 208
    .local v11, "RegExSelection":Ljava/lang/StringBuilder;
    const/4 v6, 0x0

    .line 209
    .local v6, "RegExSelectionArg":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 210
    .local v10, "RegExSeg":[Ljava/lang/String;
    const/16 v16, 0x0

    .line 213
    .local v16, "searchTargetTemp":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mLocationStr:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 214
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNProvider;->SearchTarget:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v16, v0

    .line 215
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNProvider;->SearchTarget:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v16

    array-length v5, v0

    move-object/from16 v0, v16

    invoke-static {v2, v3, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 220
    :goto_0
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 221
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getListQuery(Landroid/content/Context;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 272
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mStartTime:Ljava/lang/String;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mEndTime:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 273
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mStartTime:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mStartTime:Ljava/lang/String;

    .line 274
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mEndTime:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mEndTime:Ljava/lang/String;

    .line 275
    const-string v2, " AND ( ( "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "date_modified"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " >= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mStartTime:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND ( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "date_modified"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " <= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mEndTime:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ) )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "_id"

    aput-object v7, v4, v5

    const/4 v5, 0x1

    const-string v7, "title"

    aput-object v7, v4, v5

    const/4 v5, 0x2

    const-string v7, "date_modified"

    aput-object v7, v4, v5

    const/4 v5, 0x3

    const-string v7, "duration"

    aput-object v7, v4, v5

    const/4 v5, 0x4

    const-string v7, "mime_type"

    aput-object v7, v4, v5

    const/4 v5, 0x5

    const-string v7, "label_id"

    aput-object v7, v4, v5

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v7, "title_key"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 293
    if-nez v13, :cond_e

    .line 294
    const/4 v2, 0x0

    .line 298
    :goto_2
    return-object v2

    .line 217
    :cond_2
    sget-object v2, Lcom/sec/android/app/voicenote/common/util/VNProvider;->SearchTarget:[Ljava/lang/String;

    invoke-virtual {v2}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "searchTargetTemp":[Ljava/lang/String;
    check-cast v16, [Ljava/lang/String;

    .restart local v16    # "searchTargetTemp":[Ljava/lang/String;
    goto/16 :goto_0

    .line 223
    :cond_3
    const-string v2, "( "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getListQuery(Landroid/content/Context;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") AND ( ( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    const-string v2, "["

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 225
    new-instance v15, Lcom/sec/android/app/voicenote/common/util/VNQueryParser;

    invoke-direct {v15}, Lcom/sec/android/app/voicenote/common/util/VNQueryParser;-><init>()V

    .line 226
    .local v15, "qParser":Lcom/sec/android/app/voicenote/common/util/VNQueryParser;
    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/sec/android/app/voicenote/common/util/VNQueryParser;->regexParser(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 244
    .end local v15    # "qParser":Lcom/sec/android/app/voicenote/common/util/VNQueryParser;
    :cond_4
    array-length v2, v10

    const/4 v3, 0x1

    if-le v2, v3, :cond_9

    .line 245
    move-object/from16 v0, v16

    array-length v2, v0

    array-length v3, v10

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v2, v3

    new-array v6, v2, [Ljava/lang/String;

    .line 249
    :goto_3
    const/4 v8, 0x0

    .line 251
    .local v8, "ArgIndex":I
    const/4 v12, 0x0

    .local v12, "RequestIdx":I
    :goto_4
    move-object/from16 v0, v16

    array-length v2, v0

    if-ge v12, v2, :cond_d

    .line 252
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_5
    array-length v2, v10

    if-ge v14, v2, :cond_b

    .line 253
    rem-int/lit8 v2, v14, 0x2

    if-nez v2, :cond_a

    .line 254
    aget-object v2, v16, v12

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    const-string v2, " Like ?"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "ArgIndex":I
    .local v9, "ArgIndex":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v10, v14

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v8

    move v8, v9

    .line 252
    .end local v9    # "ArgIndex":I
    .restart local v8    # "ArgIndex":I
    :cond_5
    :goto_6
    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    .line 228
    .end local v8    # "ArgIndex":I
    .end local v12    # "RequestIdx":I
    .end local v14    # "i":I
    :cond_6
    const-string v2, " "

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 229
    .local v18, "temp":[Ljava/lang/String;
    move-object/from16 v0, v18

    array-length v2, v0

    mul-int/lit8 v2, v2, 0x2

    add-int/lit8 v17, v2, -0x1

    .line 230
    .local v17, "size":I
    move/from16 v0, v17

    new-array v10, v0, [Ljava/lang/String;

    .line 231
    const/4 v14, 0x0

    .restart local v14    # "i":I
    :goto_7
    move/from16 v0, v17

    if-ge v14, v0, :cond_4

    .line 232
    rem-int/lit8 v2, v14, 0x2

    if-nez v2, :cond_8

    .line 233
    if-nez v14, :cond_7

    .line 234
    const/4 v2, 0x0

    const/4 v3, 0x0

    aget-object v3, v18, v3

    aput-object v3, v10, v2

    .line 231
    :goto_8
    add-int/lit8 v14, v14, 0x1

    goto :goto_7

    .line 236
    :cond_7
    div-int/lit8 v2, v14, 0x2

    aget-object v2, v18, v2

    aput-object v2, v10, v14

    goto :goto_8

    .line 239
    :cond_8
    const-string v2, "AND"

    aput-object v2, v10, v14

    goto :goto_8

    .line 247
    .end local v14    # "i":I
    .end local v17    # "size":I
    .end local v18    # "temp":[Ljava/lang/String;
    :cond_9
    move-object/from16 v0, v16

    array-length v2, v0

    new-array v6, v2, [Ljava/lang/String;

    goto :goto_3

    .line 258
    .restart local v8    # "ArgIndex":I
    .restart local v12    # "RequestIdx":I
    .restart local v14    # "i":I
    :cond_a
    const/16 v2, 0x20

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 259
    aget-object v2, v10, v14

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    array-length v2, v10

    add-int/lit8 v2, v2, -0x1

    if-eq v14, v2, :cond_5

    .line 261
    const/16 v2, 0x20

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 265
    :cond_b
    move-object/from16 v0, v16

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    if-ge v12, v2, :cond_c

    .line 266
    const-string v2, " ) OR ( "

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    :cond_c
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_4

    .line 269
    .end local v14    # "i":I
    :cond_d
    const-string v2, " ) )"

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 296
    .end local v8    # "ArgIndex":I
    .end local v12    # "RequestIdx":I
    :cond_e
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 298
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getResultCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v2

    goto/16 :goto_2
.end method

.method private getTagSearchCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 9
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 413
    new-instance v4, Landroid/database/MatrixCursor;

    sget-object v6, Lcom/sec/android/app/voicenote/common/util/VNProvider;->TAG_COLUMNS:[Ljava/lang/String;

    invoke-direct {v4, v6}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 415
    .local v4, "tagSearchCursor":Landroid/database/MatrixCursor;
    if-nez p1, :cond_0

    .line 441
    :goto_0
    return-object v4

    .line 420
    :cond_0
    const-string v6, "_id"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 421
    .local v1, "audioId":Ljava/lang/String;
    const-string v6, "addr"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 422
    .local v0, "addr":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 423
    .local v5, "uri":Ljava/lang/String;
    const-string v6, "date_modified"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 430
    .local v3, "strDate":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->makeDate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 432
    .local v2, "date":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 433
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "0"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v0, v6, v7

    const/4 v7, 0x2

    aput-object v5, v6, v7

    const/4 v7, 0x3

    aput-object v2, v6, v7

    const/4 v7, 0x4

    const-string v8, "0"

    aput-object v8, v6, v7

    invoke-virtual {v4, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 436
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 438
    const-string v6, "VNProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getTagSearchCursor() tagSearchCursor count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Landroid/database/MatrixCursor;->getCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->moveToFirst()Z

    goto :goto_0
.end method

.method private makeDate(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "strDate"    # Ljava/lang/String;

    .prologue
    .line 400
    const/4 v0, 0x0

    .line 401
    .local v0, "date":Ljava/util/Date;
    if-eqz p1, :cond_0

    .line 402
    new-instance v0, Ljava/util/Date;

    .end local v0    # "date":Ljava/util/Date;
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 403
    .restart local v0    # "date":Ljava/util/Date;
    if-eqz v0, :cond_0

    .line 404
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 405
    .local v1, "dateFormat":Ljava/text/DateFormat;
    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    .line 408
    .end local v1    # "dateFormat":Ljava/text/DateFormat;
    :cond_0
    return-object p1
.end method

.method private stringForTime(J)Ljava/lang/String;
    .locals 13
    .param p1, "timeMs"    # J

    .prologue
    .line 388
    const-wide/16 v8, 0x3e8

    div-long v6, p1, v8

    .line 389
    .local v6, "totalSeconds":J
    const-wide/16 v8, 0x3c

    rem-long v4, v6, v8

    .line 390
    .local v4, "seconds":J
    const-wide/16 v8, 0x3c

    div-long v8, v6, v8

    const-wide/16 v10, 0x3c

    rem-long v2, v8, v10

    .line 391
    .local v2, "minutes":J
    const-wide/16 v8, 0xe10

    div-long v0, v6, v8

    .line 392
    .local v0, "hours":J
    const-wide/16 v8, 0x0

    cmp-long v8, v0, v8

    if-lez v8, :cond_0

    .line 393
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "%d:%02d:%02d"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 395
    :goto_0
    return-object v8

    :cond_0
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "%02d:%02d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # [Ljava/lang/String;

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public getFinDoCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 445
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getFinDoCursor([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFinDoCursor(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "startId"    # Ljava/lang/String;
    .param p2, "endId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 449
    invoke-virtual {p0, v0, v0, p1, p2}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getFinDoCursor([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFinDoCursor([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13
    .param p1, "strFilter"    # [Ljava/lang/String;
    .param p2, "location"    # Ljava/lang/String;
    .param p3, "startId"    # Ljava/lang/String;
    .param p4, "endId"    # Ljava/lang/String;

    .prologue
    .line 453
    const/4 v2, 0x0

    .line 454
    .local v2, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 455
    .local v6, "orderByClause":Ljava/lang/String;
    const/4 v3, 0x0

    .line 456
    .local v3, "cols":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 458
    .local v7, "cursorVideo":Landroid/database/Cursor;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 460
    .local v12, "stringBuilder":Ljava/lang/StringBuilder;
    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 461
    const-string v6, "title COLLATE LOCALIZED ASC"

    .line 462
    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    .end local v3    # "cols":[Ljava/lang/String;
    const/4 v1, 0x0

    const-string v4, "_id"

    aput-object v4, v3, v1

    const/4 v1, 0x1

    const-string v4, "date_modified"

    aput-object v4, v3, v1

    const/4 v1, 0x2

    const-string v4, "addr"

    aput-object v4, v3, v1

    .line 468
    .restart local v3    # "cols":[Ljava/lang/String;
    if-eqz p1, :cond_5

    .line 469
    const-string v1, " AND"

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 471
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/voicenote/common/util/VNProvider;->SearchTarget:[Ljava/lang/String;

    array-length v1, v1

    if-ge v11, v1, :cond_4

    .line 472
    if-eqz v11, :cond_1

    .line 473
    const-string v1, " OR ("

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 478
    :goto_1
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_2
    array-length v1, p1

    if-ge v10, v1, :cond_3

    .line 479
    aget-object v1, p1, v10

    if-eqz v1, :cond_0

    aget-object v1, p1, v10

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 480
    rem-int/lit8 v1, v10, 0x2

    const/4 v4, 0x1

    if-ne v1, v4, :cond_2

    .line 481
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v4, p1, v10

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 478
    :cond_0
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 475
    .end local v10    # "i":I
    :cond_1
    const-string v1, " (("

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 483
    .restart local v10    # "i":I
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/app/voicenote/common/util/VNProvider;->SearchTarget:[Ljava/lang/String;

    aget-object v4, v4, v11

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " like \'%"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v4, p1, v10

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "%\'"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 488
    :cond_3
    const-string v1, ")"

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 471
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 490
    .end local v10    # "i":I
    :cond_4
    const-string v1, ")"

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    .end local v11    # "j":I
    :cond_5
    if-eqz p4, :cond_a

    .line 494
    if-eqz p1, :cond_6

    .line 495
    const-string v1, " AND "

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 497
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " ( _id >= \'"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\' AND _id <= \'"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\')"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 505
    :cond_7
    :goto_4
    if-eqz p2, :cond_9

    .line 506
    if-eqz p3, :cond_8

    .line 507
    const-string v1, " AND "

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " ( addr like \'%"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "%\')"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 512
    :cond_9
    const-string v1, "VNProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getFindoCursor query string : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    .line 524
    if-nez v7, :cond_c

    .line 525
    const/4 v1, 0x0

    .line 533
    :goto_5
    return-object v1

    .line 498
    :cond_a
    if-eqz p3, :cond_7

    .line 499
    if-eqz p1, :cond_b

    .line 500
    const-string v1, " AND "

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 502
    :cond_b
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " ( _id = \'"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\')"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 516
    :catch_0
    move-exception v8

    .line 517
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "VNProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getFindoCursor - SQLiteException :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    const/4 v1, 0x0

    goto :goto_5

    .line 519
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v9

    .line 520
    .local v9, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v1, "VNProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getFindoCursor - UnsupportedOperationException :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    const/4 v1, 0x0

    goto :goto_5

    .line 526
    .end local v9    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_c
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_d

    .line 527
    const-string v1, "VNProvider"

    const-string v4, "cursorVideo.getCount() <= 0"

    invoke-static {v1, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v7

    .line 528
    goto :goto_5

    .line 531
    :cond_d
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-object v1, v7

    .line 533
    goto :goto_5
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;

    .prologue
    .line 101
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Landroid/content/ContentValues;

    .prologue
    .line 107
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 303
    const-string v5, "VNProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "query uri : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    const-string v5, "location"

    invoke-virtual {p1, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mLocationStr:Ljava/lang/String;

    .line 305
    const-string v5, "startid"

    invoke-virtual {p1, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 306
    .local v3, "startid":Ljava/lang/String;
    const-string v5, "endid"

    invoke-virtual {p1, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 307
    .local v0, "endid":Ljava/lang/String;
    const-string v5, "stime"

    invoke-virtual {p1, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mStartTime:Ljava/lang/String;

    .line 308
    const-string v5, "etime"

    invoke-virtual {p1, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mEndTime:Ljava/lang/String;

    .line 310
    sget-object v5, Lcom/sec/android/app/voicenote/common/util/VNProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 311
    .local v2, "match":I
    packed-switch v2, :pswitch_data_0

    .line 362
    :cond_0
    :goto_0
    return-object v4

    .line 313
    :pswitch_0
    aget-object v4, p4, v8

    invoke-direct {p0, v4}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getGlobalSearchCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    goto :goto_0

    .line 315
    :pswitch_1
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 316
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 317
    const-string v4, "VNProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "tag query1 start id : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " end id : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getFinDoCursor(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 320
    .local v1, "finDoCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getTagSearchCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 322
    if-eqz v1, :cond_0

    .line 323
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 322
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_1

    .line 323
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v4

    .line 326
    .end local v1    # "finDoCursor":Landroid/database/Cursor;
    :cond_2
    const-string v5, "VNProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "tag query1 start id : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getFinDoCursor(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 329
    .restart local v1    # "finDoCursor":Landroid/database/Cursor;
    :try_start_1
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getTagSearchCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 331
    if-eqz v1, :cond_0

    .line 332
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 331
    :catchall_1
    move-exception v4

    if-eqz v1, :cond_3

    .line 332
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v4

    .line 336
    .end local v1    # "finDoCursor":Landroid/database/Cursor;
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getFinDoCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 338
    .restart local v1    # "finDoCursor":Landroid/database/Cursor;
    :try_start_2
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getTagSearchCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v4

    .line 340
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catchall_2
    move-exception v4

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v4

    .line 345
    .end local v1    # "finDoCursor":Landroid/database/Cursor;
    :pswitch_2
    if-nez p4, :cond_7

    .line 346
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mLocationStr:Ljava/lang/String;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mLocationStr:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    .line 347
    const-string v4, "VNProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "tag query Location : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mLocationStr:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mLocationStr:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getRexCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    goto/16 :goto_0

    .line 349
    :cond_5
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_6

    .line 350
    const-string v4, "VNProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "tag query id2 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getRexCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    goto/16 :goto_0

    .line 352
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mStartTime:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mEndTime:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mStartTime:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mEndTime:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 353
    const-string v5, "VNProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "startTime : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mStartTime:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " endTime : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/VNProvider;->mEndTime:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    invoke-direct {p0, v4}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getRexCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    goto/16 :goto_0

    .line 357
    :cond_7
    aget-object v4, p4, v8

    invoke-direct {p0, v4}, Lcom/sec/android/app/voicenote/common/util/VNProvider;->getRexCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    goto/16 :goto_0

    .line 311
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Landroid/content/ContentValues;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # [Ljava/lang/String;

    .prologue
    .line 367
    const/4 v0, 0x0

    return v0
.end method
