.class Lcom/sec/android/app/voicenote/main/VNMainActivity$14;
.super Landroid/content/BroadcastReceiver;
.source "VNMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/main/VNMainActivity;->registerEasyModeReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/main/VNMainActivity;)V
    .locals 0

    .prologue
    .line 4218
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$14;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 4221
    const-string v1, "VoiceNoteMainActivity"

    const-string v2, "registerEasyModeReceiver com.android.launcher.action.EASY_MODE_CHANGE_VOICENOTE"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 4222
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$14;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    .line 4223
    .local v0, "currentEasymode":Z
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$14;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # getter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsEasymode:Z
    invoke-static {v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$4500(Lcom/sec/android/app/voicenote/main/VNMainActivity;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 4224
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$14;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    # setter for: Lcom/sec/android/app/voicenote/main/VNMainActivity;->mIsEasymode:Z
    invoke-static {v1, v0}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->access$4502(Lcom/sec/android/app/voicenote/main/VNMainActivity;Z)Z

    .line 4230
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/VNMainActivity$14;->this$0:Lcom/sec/android/app/voicenote/main/VNMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/main/VNMainActivity;->finishCustomActionMode()V

    .line 4231
    :cond_0
    return-void
.end method
