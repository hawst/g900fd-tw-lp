.class Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;
.super Ljava/lang/Object;
.source "AudioConverter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->runFileInputStream()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 252
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mReadBufferSize:I
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$700(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)I

    move-result v0

    .line 253
    .local v0, "buffersize":I
    const/4 v4, 0x0

    .line 255
    .local v4, "readCount":I
    mul-int/lit8 v6, v0, 0x2

    new-array v1, v6, [B

    .line 256
    .local v1, "byteArray":[B
    new-array v5, v0, [S

    .line 259
    .local v5, "shortArray":[S
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mFileInputStream:Ljava/io/FileInputStream;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$800(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Ljava/io/FileInputStream;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/io/FileInputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 271
    if-gtz v4, :cond_2

    .line 272
    const-string v6, "AudioConverter"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "FileStream : readCount = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/4 v6, 0x5

    if-ge v3, v6, :cond_1

    .line 274
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$900(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v6

    array-length v7, v5

    invoke-virtual {v6, v5, v7}, Lcom/samsung/voiceshell/VoiceEngine;->processNSFrame([SI)I

    .line 275
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mEPDSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$1000(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->addAudioBuffer([S)V

    .line 276
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mRecognizerSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$300(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->addAudioBuffer([S)V

    .line 277
    const-string v6, "AudioConverter"

    const-string v7, "Add the tailed silence"

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/library/stt/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 260
    .end local v3    # "i":I
    :catch_0
    move-exception v2

    .line 262
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 301
    .end local v2    # "e":Ljava/io/IOException;
    :cond_0
    :goto_1
    return-void

    .line 280
    .restart local v3    # "i":I
    :cond_1
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mFileInputStream:Ljava/io/FileInputStream;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$800(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Ljava/io/FileInputStream;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 285
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mWriteHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$1100(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mWriteHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$1100(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v7

    const-wide/16 v8, 0x1f4

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    .line 281
    :catch_1
    move-exception v2

    .line 282
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 290
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "i":I
    :cond_2
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    div-int/lit8 v6, v4, 0x2

    if-ge v3, v6, :cond_3

    .line 291
    mul-int/lit8 v6, v3, 0x2

    aget-byte v6, v1, v6

    and-int/lit16 v6, v6, 0xff

    mul-int/lit8 v7, v3, 0x2

    add-int/lit8 v7, v7, 0x1

    aget-byte v7, v1, v7

    shl-int/lit8 v7, v7, 0x8

    const v8, 0xff00

    and-int/2addr v7, v8

    add-int/2addr v6, v7

    int-to-short v6, v6

    aput-short v6, v5, v3

    .line 290
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 294
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$900(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v6

    array-length v7, v5

    invoke-virtual {v6, v5, v7}, Lcom/samsung/voiceshell/VoiceEngine;->processNSFrame([SI)I

    .line 295
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mEPDSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$1000(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->addAudioBuffer([S)V

    .line 296
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mRecognizerSource:Lcom/sec/android/app/voicenote/library/stt/AudioSource;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$300(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->addAudioBuffer([S)V

    .line 298
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mEnded:Z
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$1200(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 299
    iget-object v6, p0, Lcom/sec/android/app/voicenote/library/stt/AudioConverter$3;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioConverter;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->mWriteHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/voicenote/library/stt/AudioConverter;->access$1100(Lcom/sec/android/app/voicenote/library/stt/AudioConverter;)Landroid/os/Handler;

    move-result-object v6

    const-wide/16 v8, 0x64

    invoke-virtual {v6, p0, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method
