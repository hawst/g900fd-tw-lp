.class public Lcom/sec/android/app/voicenote/common/util/ATTCommandReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ATTCommandReceiver.java"


# static fields
.field private static final ACTION_AT_RESPONSE:Ljava/lang/String; = "android.intent.action.BCS_RESPONSE"

.field private static final AT_COMMAND:Ljava/lang/String; = "AT+CVRCD=NR"

.field private static final TAG:Ljava/lang/String; = "ATTCommandReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private getVoiceFileCounts(Landroid/content/Context;)I
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 55
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "count(*)"

    aput-object v0, v2, v1

    .line 59
    .local v2, "cols":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 60
    .local v3, "selection":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getListQuery(Landroid/content/Context;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 62
    .local v6, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 66
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 67
    .local v7, "count":I
    if-eqz v8, :cond_1

    .line 69
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 75
    :cond_0
    if-eqz v8, :cond_1

    .line 76
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 77
    const/4 v8, 0x0

    .line 81
    :cond_1
    :goto_0
    const-string v0, "ATTCommandReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onReceive() NR : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    return v7

    .line 72
    :catch_0
    move-exception v9

    .line 73
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "ATTCommandReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVoiceFileCounts - Exception :"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    if-eqz v8, :cond_1

    .line 76
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 77
    const/4 v8, 0x0

    goto :goto_0

    .line 75
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_2

    .line 76
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 77
    const/4 v8, 0x0

    :cond_2
    throw v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 42
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 43
    .local v1, "data":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 44
    const-string v2, "command"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "command":Ljava/lang/String;
    const-string v2, "AT+CVRCD=NR"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 46
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.BCS_RESPONSE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "response"

    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/ATTCommandReceiver;->getVoiceFileCounts(Landroid/content/Context;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 52
    .end local v0    # "command":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    const-string v2, "ATTCommandReceiver"

    const-string v3, "data is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
