.class Lcom/sec/android/app/voicenote/library/stt/MemoData$1;
.super Ljava/lang/Object;
.source "MemoData.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/stt/MemoData;->saveMemoData(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/stt/MemoData;

.field final synthetic val$filepath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/stt/MemoData;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/MemoData$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/MemoData;

    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/stt/MemoData$1;->val$filepath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 49
    new-instance v1, Lcom/sec/android/app/voicenote/library/stt/M4aReader;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/MemoData$1;->val$filepath:Ljava/lang/String;

    invoke-direct {v1, v3}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;-><init>(Ljava/lang/String;)V

    .line 50
    .local v1, "reader":Lcom/sec/android/app/voicenote/library/stt/M4aReader;
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->readFile()Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    move-result-object v0

    .line 53
    .local v0, "info":Lcom/sec/android/app/voicenote/library/stt/M4aInfo;
    new-instance v1, Lcom/sec/android/app/voicenote/library/stt/M4aReader;

    .end local v1    # "reader":Lcom/sec/android/app/voicenote/library/stt/M4aReader;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/MemoData$1;->val$filepath:Ljava/lang/String;

    invoke-direct {v1, v3}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;-><init>(Ljava/lang/String;)V

    .line 54
    .restart local v1    # "reader":Lcom/sec/android/app/voicenote/library/stt/M4aReader;
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->readFile()Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    move-result-object v0

    .line 56
    # getter for: Lcom/sec/android/app/voicenote/library/stt/MemoData;->mTextDataArrayList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/MemoData;->access$000()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_1

    # getter for: Lcom/sec/android/app/voicenote/library/stt/MemoData;->mTextDataArrayList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/MemoData;->access$000()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 57
    new-instance v1, Lcom/sec/android/app/voicenote/library/stt/M4aReader;

    .end local v1    # "reader":Lcom/sec/android/app/voicenote/library/stt/M4aReader;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/MemoData$1;->val$filepath:Ljava/lang/String;

    invoke-direct {v1, v3}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;-><init>(Ljava/lang/String;)V

    .line 58
    .restart local v1    # "reader":Lcom/sec/android/app/voicenote/library/stt/M4aReader;
    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/library/stt/M4aReader;->readFile()Lcom/sec/android/app/voicenote/library/stt/M4aInfo;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_0

    .line 60
    new-instance v2, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/stt/MemoData$1;->this$0:Lcom/sec/android/app/voicenote/library/stt/MemoData;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/MemoData;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/stt/MemoData;->access$100(Lcom/sec/android/app/voicenote/library/stt/MemoData;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;-><init>(Lcom/sec/android/app/voicenote/library/stt/M4aInfo;Landroid/content/Context;)V

    .line 61
    .local v2, "sttHelper":Lcom/sec/android/app/voicenote/library/stt/SttdHelper;
    # getter for: Lcom/sec/android/app/voicenote/library/stt/MemoData;->mTextDataArrayList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/MemoData;->access$000()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/library/stt/SttdHelper;->overwriteSttd(Ljava/io/Serializable;)V

    .line 63
    .end local v2    # "sttHelper":Lcom/sec/android/app/voicenote/library/stt/SttdHelper;
    :cond_0
    # getter for: Lcom/sec/android/app/voicenote/library/stt/MemoData;->mTextDataArrayList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/stt/MemoData;->access$000()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 65
    :cond_1
    return-void
.end method
