.class public Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;
.super Ljava/lang/Object;
.source "VNSTTData.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/main/common/VNSTTData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "STTWord"
.end annotation


# instance fields
.field public dConfidenceScore:D

.field public elapsedTime:J

.field public lEndTime:J

.field public lInitialSilence:J

.field public lStartTime:J

.field public lUtteranceLength:J

.field public lUtteranceOffset:J

.field public timeStamp:J

.field public word:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->word:Ljava/util/ArrayList;

    .line 44
    return-void
.end method


# virtual methods
.method protected clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public swap(Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;)V
    .locals 24
    .param p1, "sw"    # Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;

    .prologue
    .line 52
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->timeStamp:J

    move-wide/from16 v18, v0

    .line 53
    .local v18, "temptimeStamp":J
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->elapsedTime:J

    .line 54
    .local v6, "tempelapsedTime":J
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lInitialSilence:J

    .line 55
    .local v10, "templInitialSilence":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lUtteranceOffset:J

    move-wide/from16 v16, v0

    .line 56
    .local v16, "templUtteranceOffset":J
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lUtteranceLength:J

    .line 57
    .local v14, "templUtteranceLength":J
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lStartTime:J

    .line 58
    .local v12, "templStartTime":J
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lEndTime:J

    .line 59
    .local v8, "templEndTime":J
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->dConfidenceScore:D

    .line 60
    .local v4, "tempdConfidenceScore":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->word:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    .line 62
    .local v20, "tempword":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->timeStamp:J

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->timeStamp:J

    .line 63
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->elapsedTime:J

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->elapsedTime:J

    .line 64
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lInitialSilence:J

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lInitialSilence:J

    .line 65
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lUtteranceOffset:J

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lUtteranceOffset:J

    .line 66
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lUtteranceLength:J

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lUtteranceLength:J

    .line 67
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lStartTime:J

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lStartTime:J

    .line 68
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lEndTime:J

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lEndTime:J

    .line 69
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->dConfidenceScore:D

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->dConfidenceScore:D

    .line 70
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->word:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->word:Ljava/util/ArrayList;

    .line 72
    move-wide/from16 v0, v18

    move-object/from16 v2, p1

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->timeStamp:J

    .line 73
    move-object/from16 v0, p1

    iput-wide v6, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->elapsedTime:J

    .line 74
    move-object/from16 v0, p1

    iput-wide v10, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lInitialSilence:J

    .line 75
    move-wide/from16 v0, v16

    move-object/from16 v2, p1

    iput-wide v0, v2, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lUtteranceOffset:J

    .line 76
    move-object/from16 v0, p1

    iput-wide v14, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lUtteranceLength:J

    .line 77
    move-object/from16 v0, p1

    iput-wide v12, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lStartTime:J

    .line 78
    move-object/from16 v0, p1

    iput-wide v8, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->lEndTime:J

    .line 79
    move-object/from16 v0, p1

    iput-wide v4, v0, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->dConfidenceScore:D

    .line 80
    move-object/from16 v0, v20

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/sec/android/app/voicenote/main/common/VNSTTData$STTWord;->word:Ljava/util/ArrayList;

    .line 81
    return-void
.end method
