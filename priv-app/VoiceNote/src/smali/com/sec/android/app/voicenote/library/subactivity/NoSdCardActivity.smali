.class public Lcom/sec/android/app/voicenote/library/subactivity/NoSdCardActivity;
.super Landroid/app/Activity;
.source "NoSdCardActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NoSdCardActivity"


# instance fields
.field private mEventHandler:Landroid/os/Handler;

.field private mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 58
    new-instance v0, Lcom/sec/android/app/voicenote/library/subactivity/NoSdCardActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/subactivity/NoSdCardActivity$1;-><init>(Lcom/sec/android/app/voicenote/library/subactivity/NoSdCardActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/NoSdCardActivity;->mEventHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    const-string v1, "NoSdCardActivity"

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    const v1, 0x7f030002

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/NoSdCardActivity;->setContentView(I)V

    .line 44
    const v1, 0x7f0e0007

    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/subactivity/NoSdCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 46
    .local v0, "text":Landroid/widget/TextView;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    const-string v2, "shared"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    const v1, 0x7f0b0112

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 51
    :goto_0
    new-instance v1, Lcom/sec/android/app/voicenote/common/util/VNIntent;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/subactivity/NoSdCardActivity;->mEventHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNIntent;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/NoSdCardActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/subactivity/NoSdCardActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiversForActivity(Z)V

    .line 55
    const-string v1, "NoSdCardActivity"

    const-string v2, "onCreate() END"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void

    .line 49
    :cond_0
    const v1, 0x7f0b0113

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 82
    const-string v0, "NoSdCardActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/subactivity/NoSdCardActivity;->mVNIntent:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiversForActivity(Z)V

    .line 84
    const-string v0, "NoSdCardActivity"

    const-string v1, "onDestroy() END"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 86
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 90
    const-string v0, "NoSdCardActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTrimMemory() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    sparse-switch p1, :sswitch_data_0

    .line 104
    :sswitch_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 105
    return-void

    .line 91
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method
