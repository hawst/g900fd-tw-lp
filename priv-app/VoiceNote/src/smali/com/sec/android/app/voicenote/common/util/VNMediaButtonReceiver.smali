.class public Lcom/sec/android/app/voicenote/common/util/VNMediaButtonReceiver;
.super Landroid/content/BroadcastReceiver;
.source "VNMediaButtonReceiver.java"


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 32
    const-string v0, "VNMediaButtonReceiver"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/VNMediaButtonReceiver;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v8, 0x5a

    const/16 v5, 0x59

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 37
    const-class v4, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isServiceRunning(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 38
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/VNMediaButtonReceiver;->TAG:Ljava/lang/String;

    const-string v5, "received media button event but service is not running : unregister MediaButtonEventReceiver at the VoiceRecorder"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v4, "audio"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/AudioManager;

    .line 42
    .local v3, "mAudioManager":Landroid/media/AudioManager;
    new-instance v4, Landroid/content/ComponentName;

    const-string v5, "com.sec.android.app.voicerecorder"

    const-class v6, Lcom/sec/android/app/voicenote/common/util/VNMediaButtonReceiver;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 115
    .end local v3    # "mAudioManager":Landroid/media/AudioManager;
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    const-string v4, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/view/KeyEvent;

    .line 48
    .local v1, "event":Landroid/view/KeyEvent;
    if-eqz v1, :cond_0

    .line 51
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    .line 52
    .local v2, "keycode":I
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 54
    .local v0, "action":I
    if-nez v0, :cond_2

    .line 55
    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 57
    :sswitch_0
    const/16 v4, 0x18

    invoke-virtual {v1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v5

    invoke-static {p1, v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendMediaButtonReceived(Landroid/content/Context;IIZ)V

    goto :goto_0

    .line 62
    :sswitch_1
    const/16 v4, 0x19

    invoke-virtual {v1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v5

    invoke-static {p1, v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendMediaButtonReceived(Landroid/content/Context;IIZ)V

    goto :goto_0

    .line 70
    :sswitch_2
    const/16 v4, 0x4f

    invoke-virtual {v1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v5

    invoke-static {p1, v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendMediaButtonReceived(Landroid/content/Context;IIZ)V

    goto :goto_0

    .line 76
    :sswitch_3
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    invoke-static {p1, v8, v4, v6}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendMediaButtonReceived(Landroid/content/Context;IIZ)V

    goto :goto_0

    .line 82
    :sswitch_4
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    invoke-static {p1, v5, v4, v6}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendMediaButtonReceived(Landroid/content/Context;IIZ)V

    goto :goto_0

    .line 88
    :sswitch_5
    const/16 v4, 0x58

    invoke-virtual {v1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v5

    invoke-static {p1, v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendMediaButtonReceived(Landroid/content/Context;IIZ)V

    goto :goto_0

    .line 92
    :sswitch_6
    const/16 v4, 0x57

    invoke-virtual {v1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v5

    invoke-static {p1, v4, v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendMediaButtonReceived(Landroid/content/Context;IIZ)V

    goto :goto_0

    .line 96
    :cond_2
    if-ne v0, v6, :cond_0

    .line 97
    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 99
    :pswitch_1
    const/16 v4, 0x56

    invoke-virtual {v1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v5

    invoke-static {p1, v4, v5, v7}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendMediaButtonReceived(Landroid/content/Context;IIZ)V

    goto :goto_0

    .line 104
    :pswitch_2
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    invoke-static {p1, v8, v4, v7}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendMediaButtonReceived(Landroid/content/Context;IIZ)V

    goto :goto_0

    .line 108
    :pswitch_3
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    invoke-static {p1, v5, v4, v7}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->sendMediaButtonReceived(Landroid/content/Context;IIZ)V

    goto :goto_0

    .line 55
    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_1
        0x4f -> :sswitch_2
        0x55 -> :sswitch_2
        0x57 -> :sswitch_6
        0x58 -> :sswitch_5
        0x59 -> :sswitch_4
        0x5a -> :sswitch_3
        0x7e -> :sswitch_2
        0x7f -> :sswitch_2
    .end sparse-switch

    .line 97
    :pswitch_data_0
    .packed-switch 0x56
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
