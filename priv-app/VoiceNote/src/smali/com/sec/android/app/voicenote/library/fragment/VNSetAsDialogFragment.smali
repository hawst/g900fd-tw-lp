.class public Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNSetAsDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$SetAsDialogAdapter;,
        Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$Callbacks;
    }
.end annotation


# static fields
.field private static IDX_ALARM_TONE:I = 0x0

.field private static IDX_CALL_RINGTONE:I = 0x0

.field private static IDX_INDIVIDUAL_RINGTONE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "VNSetAsDialogFragment"


# instance fields
.field private mAdapter:Landroid/widget/BaseAdapter;

.field private mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$Callbacks;

.field private mItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mList:Landroid/widget/ListView;

.field private mSelectedUri:Ljava/lang/String;

.field private mViewChild:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_CALL_RINGTONE:I

    .line 71
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_INDIVIDUAL_RINGTONE:I

    .line 72
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_ALARM_TONE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 80
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mList:Landroid/widget/ListView;

    .line 65
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mAdapter:Landroid/widget/BaseAdapter;

    .line 68
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mViewChild:Landroid/view/View;

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mItem:Ljava/util/ArrayList;

    return-object v0
.end method

.method private checkAlarmPackage()Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mSelectedUri:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getContentUriFromFilePath(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 200
    .local v6, "ringUri":Landroid/net/Uri;
    const/4 v4, 0x0

    .line 202
    .local v4, "packageName":Ljava/lang/String;
    if-nez v6, :cond_1

    .line 203
    const-string v8, "VNSetAsDialogFragment"

    const-string v9, "ringUri is NULL !!"

    invoke-static {v8, v9}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->dismiss()V

    .line 228
    :cond_0
    :goto_0
    return v7

    .line 208
    :cond_1
    invoke-virtual {v6}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 210
    .local v0, "filePath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 211
    .local v1, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    const-string v8, "android.intent.action.VIEW"

    const-string v9, "content://com.sec.android.app.clockpackage/alarmlist/"

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-direct {v1, v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 213
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v8, "set_alarm"

    invoke-virtual {v1, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    const-string v8, "alarm_uri"

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    if-eqz v1, :cond_2

    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 217
    .local v3, "mPm":Landroid/content/pm/PackageManager;
    invoke-virtual {v3, v1, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 219
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_2

    .line 220
    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 221
    .local v5, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v8, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 225
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v3    # "mPm":Landroid/content/pm/PackageManager;
    .end local v5    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_2
    if-eqz v4, :cond_0

    .line 226
    const/4 v7, 0x1

    goto :goto_0
.end method

.method public static getAudioFilePath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v9, 0x1

    .line 383
    const/4 v7, 0x0

    .line 385
    .local v7, "str":Ljava/lang/String;
    if-nez p1, :cond_0

    move-object v8, v7

    .line 411
    .end local v7    # "str":Ljava/lang/String;
    .local v8, "str":Ljava/lang/String;
    :goto_0
    return-object v8

    .line 387
    .end local v8    # "str":Ljava/lang/String;
    .restart local v7    # "str":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 389
    const/4 v0, 0x7

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 390
    invoke-static {v7}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    .line 392
    .end local v7    # "str":Ljava/lang/String;
    .restart local v8    # "str":Ljava/lang/String;
    goto :goto_0

    .line 393
    .end local v8    # "str":Ljava/lang/String;
    .restart local v7    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://media/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v8, v7

    .line 394
    .end local v7    # "str":Ljava/lang/String;
    .restart local v8    # "str":Ljava/lang/String;
    goto :goto_0

    .line 397
    .end local v8    # "str":Ljava/lang/String;
    .restart local v7    # "str":Ljava/lang/String;
    :cond_2
    const/4 v6, 0x0

    .line 399
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_data"

    aput-object v3, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 402
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v9, :cond_3

    .line 403
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 404
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 407
    :cond_3
    if-eqz v6, :cond_4

    .line 408
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    move-object v8, v7

    .line 411
    .end local v7    # "str":Ljava/lang/String;
    .restart local v8    # "str":Ljava/lang/String;
    goto :goto_0

    .line 407
    .end local v8    # "str":Ljava/lang/String;
    .restart local v7    # "str":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    .line 408
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method private static getSystemProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "property"    # Ljava/lang/String;
    .param p1, "slot"    # I
    .param p2, "defaultVal"    # Ljava/lang/String;

    .prologue
    .line 461
    const/4 v2, 0x0

    .line 462
    .local v2, "propVal":Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/telephony/MultiSimManager;->getSubId(I)[J

    move-result-object v3

    .line 463
    .local v3, "subId":[J
    if-nez v3, :cond_1

    .line 474
    .end local p2    # "defaultVal":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p2

    .line 466
    .restart local p2    # "defaultVal":Ljava/lang/String;
    :cond_1
    const/4 v5, 0x0

    aget-wide v6, v3, v5

    invoke-static {v6, v7}, Lcom/samsung/android/telephony/MultiSimManager;->getPhoneId(J)I

    move-result v0

    .line 467
    .local v0, "phoneId":I
    invoke-static {p0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 468
    .local v1, "prop":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 469
    const-string v5, ","

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 470
    .local v4, "values":[Ljava/lang/String;
    if-ltz v0, :cond_2

    array-length v5, v4

    if-ge v0, v5, :cond_2

    aget-object v5, v4, v0

    if-eqz v5, :cond_2

    .line 471
    aget-object v2, v4, v0

    .line 474
    .end local v4    # "values":[Ljava/lang/String;
    :cond_2
    if-eqz v2, :cond_0

    move-object p2, v2

    goto :goto_0
.end method

.method private initStaticVariables()V
    .locals 1

    .prologue
    .line 415
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_CALL_RINGTONE:I

    .line 416
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_INDIVIDUAL_RINGTONE:I

    .line 417
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_ALARM_TONE:I

    .line 418
    return-void
.end method

.method public static setArguments(Ljava/lang/String;)Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;
    .locals 3
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 84
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;-><init>()V

    .line 85
    .local v1, "dialogFragment":Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 86
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "uri"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 88
    return-object v1
.end method

.method private setItems()V
    .locals 7

    .prologue
    const v6, 0x7f0b00b0

    const/4 v5, -0x1

    .line 155
    const/4 v2, 0x0

    .line 156
    .local v2, "intent":Landroid/content/Intent;
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    const-string v3, "android.intent.action.INSERT_OR_EDIT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 157
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v3, "vnd.android.cursor.item/contact"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->initStaticVariables()V

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->hasAvailableApp(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    .line 162
    .local v0, "hasContact":Z
    const/4 v1, 0x0

    .line 164
    .local v1, "index":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Landroid/util/GeneralUtil;->isVoiceCapable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 166
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mItem:Ljava/util/ArrayList;

    const v4, 0x7f0b00b2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    sput v1, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_CALL_RINGTONE:I

    .line 168
    add-int/lit8 v1, v1, 0x1

    .line 170
    if-eqz v0, :cond_0

    .line 171
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mItem:Ljava/util/ArrayList;

    const v4, 0x7f0b00b1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    sput v1, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_INDIVIDUAL_RINGTONE:I

    .line 173
    add-int/lit8 v1, v1, 0x1

    .line 178
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->checkAlarmPackage()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 179
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mItem:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    sput v1, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_ALARM_TONE:I

    .line 181
    add-int/lit8 v1, v1, 0x1

    .line 196
    :goto_1
    return-void

    .line 175
    :cond_0
    sput v5, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_INDIVIDUAL_RINGTONE:I

    goto :goto_0

    .line 183
    :cond_1
    sput v5, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_ALARM_TONE:I

    goto :goto_1

    .line 186
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->checkAlarmPackage()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 187
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mItem:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    sput v1, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_ALARM_TONE:I

    .line 189
    add-int/lit8 v1, v1, 0x1

    .line 193
    :goto_2
    sput v5, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_CALL_RINGTONE:I

    .line 194
    sput v5, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_INDIVIDUAL_RINGTONE:I

    goto :goto_1

    .line 191
    :cond_3
    sput v5, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_ALARM_TONE:I

    goto :goto_2
.end method

.method private setRingtone(I)V
    .locals 3
    .param p1, "menuId"    # I

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mSelectedUri:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getContentUriFromFilePath(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 320
    .local v0, "ringUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mSelectedUri:Ljava/lang/String;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 325
    :cond_1
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->updateContentValue(ILandroid/net/Uri;)Z

    .line 328
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->showSetAsResult(ILandroid/net/Uri;)V

    goto :goto_0
.end method

.method private showSetAsResult(ILandroid/net/Uri;)V
    .locals 10
    .param p1, "menuId"    # I
    .param p2, "ringUri"    # Landroid/net/Uri;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 332
    sget v6, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_CALL_RINGTONE:I

    if-ne p1, v6, :cond_4

    .line 334
    const-string v6, "gsm.sim.state"

    const-string v7, "ABSENT"

    invoke-static {v6, v8, v7}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getSystemProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 335
    .local v3, "sim1State":Ljava/lang/String;
    const-string v6, "gsm.sim.state"

    const-string v7, "ABSENT"

    invoke-static {v6, v9, v7}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getSystemProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 336
    .local v4, "sim2State":Ljava/lang/String;
    const-string v6, "READY"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "READY"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 337
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6, p2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showChooseSimCardDialog(Landroid/content/Context;Landroid/net/Uri;)V

    .line 380
    .end local v3    # "sim1State":Ljava/lang/String;
    .end local v4    # "sim2State":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 340
    .restart local v3    # "sim1State":Ljava/lang/String;
    .restart local v4    # "sim2State":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isSIMInsertedOnlyInSlot2()Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "READY"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 341
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const/16 v7, 0x8

    invoke-static {v6, v7, p2}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    .line 347
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f0b010a

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto :goto_0

    .line 344
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6, v9, p2}, Landroid/media/RingtoneManager;->setActualDefaultRingtoneUri(Landroid/content/Context;ILandroid/net/Uri;)V

    goto :goto_1

    .line 348
    .end local v3    # "sim1State":Ljava/lang/String;
    .end local v4    # "sim2State":Ljava/lang/String;
    :cond_4
    sget v6, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_INDIVIDUAL_RINGTONE:I

    if-ne p1, v6, :cond_7

    .line 349
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 350
    .local v1, "filePath":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.intent.action.INSERT_OR_EDIT"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 351
    .local v2, "intent":Landroid/content/Intent;
    const-string v6, "ringtone_filepath"

    invoke-virtual {v2, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    const-string v6, "ringtone_uri"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 353
    const-string v6, "vnd.android.cursor.item/contact"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 355
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string v7, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 357
    .local v5, "strContactPackageName":Ljava/lang/String;
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 358
    :cond_5
    const-string v5, "com.android.contacts"

    .line 361
    :cond_6
    invoke-virtual {v2, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 364
    :try_start_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 365
    :catch_0
    move-exception v0

    .line 366
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v6, "VNSetAsDialogFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "can not set as individual ringtone : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 368
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v5    # "strContactPackageName":Ljava/lang/String;
    :cond_7
    sget v6, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_ALARM_TONE:I

    if-ne p1, v6, :cond_0

    .line 369
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 370
    .restart local v1    # "filePath":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    const-string v7, "content://com.sec.android.app.clockpackage/alarmlist/"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v2, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 372
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v6, "set_alarm"

    invoke-virtual {v2, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 373
    const-string v6, "alarm_uri"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 375
    :try_start_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 376
    :catch_1
    move-exception v0

    .line 377
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    const-string v6, "VNSetAsDialogFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "can not set as alarm tone : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private updateContentValue(ILandroid/net/Uri;)Z
    .locals 6
    .param p1, "position"    # I
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 297
    const/4 v1, 0x0

    .line 300
    .local v1, "result":Z
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 301
    .local v2, "values":Landroid/content/ContentValues;
    sget v3, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_INDIVIDUAL_RINGTONE:I

    if-eq p1, v3, :cond_0

    sget v3, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_CALL_RINGTONE:I

    if-ne p1, v3, :cond_1

    .line 302
    :cond_0
    const-string v3, "is_ringtone"

    const-string v4, "1"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, p2, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 307
    const/4 v1, 0x1

    .line 315
    .end local v2    # "values":Landroid/content/ContentValues;
    :goto_1
    return v1

    .line 304
    .restart local v2    # "values":Landroid/content/ContentValues;
    :cond_1
    const-string v3, "is_alarm"

    const-string v4, "1"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 308
    .end local v2    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 309
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v3, "VNSetAsDialogFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IllegalArgumentException occured :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    const/4 v1, 0x0

    .line 314
    goto :goto_1

    .line 311
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 312
    .local v0, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v3, "VNSetAsDialogFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UnsupportedOperationException occured :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/UnsupportedOperationException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 126
    instance-of v0, p1, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$Callbacks;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 127
    check-cast v0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$Callbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$Callbacks;

    .line 129
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 130
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "uri"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mSelectedUri:Ljava/lang/String;

    .line 97
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mItem:Ljava/util/ArrayList;

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 100
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030015

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mViewChild:Landroid/view/View;

    .line 102
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mViewChild:Landroid/view/View;

    const v4, 0x7f0e0035

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mList:Landroid/widget/ListView;

    .line 103
    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$SetAsDialogAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$SetAsDialogAdapter;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mAdapter:Landroid/widget/BaseAdapter;

    .line 104
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mList:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 105
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mList:Landroid/widget/ListView;

    invoke-virtual {v3, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 106
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mList:Landroid/widget/ListView;

    invoke-virtual {v3, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 108
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 109
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0b011c

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/high16 v4, 0x1040000

    new-instance v5, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 118
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mViewChild:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 120
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 121
    .local v1, "dialog":Landroid/app/AlertDialog;
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 145
    const-string v0, "VNSetAsDialogFragment"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mAdapter:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_0

    .line 148
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mAdapter:Landroid/widget/BaseAdapter;

    .line 150
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 151
    const-string v0, "VNSetAsDialogFragment"

    const-string v1, "onDestroy() END"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 287
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->setDoFinishWhenClickItem(Z)V

    .line 288
    invoke-direct {p0, p3}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->setRingtone(I)V

    .line 289
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getDoFinishWhenClickItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->dismissAllowingStateLoss()V

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$Callbacks;

    if-eqz v0, :cond_1

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$Callbacks;

    invoke-interface {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment$Callbacks;->onSetAsNameDialogItemClick()V

    .line 293
    :cond_1
    return-void
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 12
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 234
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mSelectedUri:Ljava/lang/String;

    invoke-static {v9, v10}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getContentUriFromFilePath(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 235
    .local v7, "ringUri":Landroid/net/Uri;
    const/4 v2, 0x0

    .line 236
    .local v2, "intent":Landroid/content/Intent;
    const/4 v5, 0x0

    .line 238
    .local v5, "packageName":Ljava/lang/String;
    if-eqz v7, :cond_2

    .line 239
    sget v9, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_CALL_RINGTONE:I

    if-ne p3, v9, :cond_3

    .line 240
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    const-string v9, "android.intent.action.CALL"

    const-string v10, "tel:"

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-direct {v2, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 263
    .restart local v2    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 264
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 265
    .local v4, "mPm":Landroid/content/pm/PackageManager;
    const/4 v9, 0x0

    invoke-virtual {v4, v2, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 267
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_1

    .line 268
    const/4 v9, 0x0

    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    .line 269
    .local v6, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v9, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 274
    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v4    # "mPm":Landroid/content/pm/PackageManager;
    .end local v6    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_1
    if-eqz v5, :cond_2

    .line 275
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    const-string v10, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    const-string v10, "package"

    const/4 v11, 0x0

    invoke-static {v10, v5, v11}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 277
    .local v1, "in":Landroid/content/Intent;
    const/high16 v9, 0x10000000

    invoke-virtual {v1, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 278
    invoke-virtual {p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->startActivity(Landroid/content/Intent;)V

    .line 281
    .end local v1    # "in":Landroid/content/Intent;
    :cond_2
    const/4 v9, 0x1

    return v9

    .line 241
    :cond_3
    sget v9, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_INDIVIDUAL_RINGTONE:I

    if-ne p3, v9, :cond_6

    .line 242
    invoke-virtual {v7}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 243
    .local v0, "filePath":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    const-string v9, "android.intent.action.INSERT_OR_EDIT"

    invoke-direct {v2, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 244
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v9, "ringtone_filepath"

    invoke-virtual {v2, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    const-string v9, "ringtone_uri"

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 246
    const-string v9, "vnd.android.cursor.item/contact"

    invoke-virtual {v2, v9}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 248
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v9

    const-string v10, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v9, v10}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 250
    .local v8, "strContactPackageName":Ljava/lang/String;
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 251
    :cond_4
    const-string v8, "com.android.contacts"

    .line 254
    :cond_5
    invoke-virtual {v2, v8}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 256
    .end local v0    # "filePath":Ljava/lang/String;
    .end local v8    # "strContactPackageName":Ljava/lang/String;
    :cond_6
    sget v9, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->IDX_ALARM_TONE:I

    if-ne p3, v9, :cond_0

    .line 257
    invoke-virtual {v7}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 258
    .restart local v0    # "filePath":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    const-string v9, "android.intent.action.VIEW"

    const-string v10, "content://com.sec.android.app.clockpackage/alarmlist/"

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-direct {v2, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 260
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v9, "set_alarm"

    invoke-virtual {v2, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 261
    const-string v9, "alarm_uri"

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 134
    const-string v0, "VNSetAsDialogFragment"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 137
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->setItems()V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSetAsDialogFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 139
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 140
    const-string v0, "VNSetAsDialogFragment"

    const-string v1, "onResume() END"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    return-void
.end method
