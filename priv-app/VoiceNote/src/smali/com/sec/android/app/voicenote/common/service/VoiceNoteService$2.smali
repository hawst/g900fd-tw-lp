.class Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;
.super Ljava/lang/Object;
.source "VoiceNoteService.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0

    .prologue
    .line 941
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 9
    .param p1, "focusChange"    # I

    .prologue
    const/16 v8, 0x3ec

    const/16 v7, 0x3eb

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 944
    const-string v4, "VoiceNoteService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onAudioFocusChange - focusChange : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    packed-switch p1, :pswitch_data_0

    .line 1002
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->isNotificationShow()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1003
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->refreshNotification()Z

    .line 1005
    :cond_1
    return-void

    .line 947
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPausedByCall:Z
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$000(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Z

    move-result v4

    if-eq v4, v3, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsPausedForaWhile:Z
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$100(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Z

    move-result v4

    if-ne v4, v3, :cond_3

    .line 948
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPausedByCall:Z
    invoke-static {v3, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$002(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z

    .line 949
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mVNPlayer:Lcom/sec/android/app/voicenote/common/util/VNPlayer;
    invoke-static {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$200(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/util/VNPlayer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/util/VNPlayer;->startFadeIn()V

    .line 950
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->resumePlay()V

    .line 951
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 953
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsPausedForaWhile:Z
    invoke-static {v3, v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$102(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z

    goto :goto_0

    .line 957
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v2

    if-eq v2, v7, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v2

    if-ne v2, v8, :cond_6

    .line 958
    :cond_4
    const-string v2, "change_notification_resume"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;Z)V

    .line 959
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mNotiManager:Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$300(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VNNotificationManager;->isNotificationShow()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 960
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecordingOnBackground()Ljava/lang/String;

    goto :goto_0

    .line 962
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecording()Z

    goto :goto_0

    .line 965
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 966
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 967
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 972
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v4

    if-eq v4, v7, :cond_7

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v4

    if-ne v4, v8, :cond_c

    .line 973
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "phone"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 974
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v4

    if-eq v4, v3, :cond_8

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsCallRinging:Z
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$400(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Z

    move-result v4

    if-eqz v4, :cond_9

    :cond_8
    move v0, v3

    .line 975
    .local v0, "isRinging":Z
    :goto_1
    const-string v4, "VoiceNoteService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AUDIOFOCUS_LOSS_TRANSIENT : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # getter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsCallRinging:Z
    invoke-static {v6}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$400(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->secD(Ljava/lang/String;Ljava/lang/String;)V

    .line 976
    if-eqz v0, :cond_b

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v4

    if-nez v4, :cond_b

    .line 977
    const-string v4, "VoiceNoteService"

    const-string v5, "AUDIOFOCUS_LOSS_TRANSIENT : keep recording"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v5

    if-ne v5, v3, :cond_a

    :goto_2
    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsCallRinging:Z
    invoke-static {v4, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$402(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z

    goto/16 :goto_0

    .end local v0    # "isRinging":Z
    :cond_9
    move v0, v2

    .line 974
    goto :goto_1

    .restart local v0    # "isRinging":Z
    :cond_a
    move v3, v2

    .line 978
    goto :goto_2

    .line 980
    :cond_b
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # invokes: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->saveRecordingOnEvent()V
    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$500(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V

    goto/16 :goto_0

    .line 982
    .end local v0    # "isRinging":Z
    .end local v1    # "tm":Landroid/telephony/TelephonyManager;
    :cond_c
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 983
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 984
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 985
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPausedByCall:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$002(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z

    .line 986
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsPausedForaWhile:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$102(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z

    goto/16 :goto_0

    .line 991
    :pswitch_4
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 992
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->pausePlay()V

    .line 993
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.android.voicenote.VOICE_INTENT_REFRESH"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->sendBroadcast(Landroid/content/Intent;)V

    .line 994
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mPausedByCall:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$002(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z

    .line 995
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService$2;->this$0:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    # setter for: Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->mIsPausedForaWhile:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->access$102(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;Z)Z

    goto/16 :goto_0

    .line 945
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
