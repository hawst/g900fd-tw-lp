.class public final Lcom/sec/android/app/voicenote/Manifest$permission;
.super Ljava/lang/Object;
.source "Manifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "permission"
.end annotation


# static fields
.field public static final Controller:Ljava/lang/String; = "com.sec.android.app.voicenote.Controller"

.field public static final RECORDER_CALLBACK_PERMISSION:Ljava/lang/String; = "com.sec.android.app.voicenote.service.RECORDER_CALLBACK_PERMISSION"

.field public static final RECORDER_PERMISSION:Ljava/lang/String; = "com.sec.android.app.voicenote.service.RECORDER_PERMISSION"

.field public static final permission:Ljava/lang/String; = "com.sec.android.app.voicenote.appwidget.permission"

.field public static final read:Ljava/lang/String; = "com.sec.android.app.voicenote.common.util.VNProvider.permission.read"

.field public static final write:Ljava/lang/String; = "com.sec.android.app.voicenote.common.util.VNProvider.permission.write"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
