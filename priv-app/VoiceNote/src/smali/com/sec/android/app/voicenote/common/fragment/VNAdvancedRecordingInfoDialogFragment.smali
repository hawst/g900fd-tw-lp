.class public Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNAdvancedRecordingInfoDialogFragment.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VNAdvancedRecordingInfoDialogFragment"

.field private static final VOICENOTE_SHOW_STT_REC_INFO2:I = 0x4


# instance fields
.field private mCheckbox:Landroid/widget/CheckBox;

.field private mRecord_mode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 49
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->mRecord_mode:I

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->mRecord_mode:I

    return v0
.end method

.method public static newInstance(I)Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;
    .locals 3
    .param p0, "recording_mode"    # I

    .prologue
    .line 56
    new-instance v1, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;-><init>()V

    .line 57
    .local v1, "f":Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 58
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "recording_mode"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 59
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 60
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 72
    .local v1, "args":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 73
    const-string v7, "recording_mode"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->mRecord_mode:I

    .line 76
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-direct {v0, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 77
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f03005f

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 78
    .local v3, "layout":Landroid/view/View;
    const v7, 0x7f0e00ec

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 79
    .local v2, "image":Landroid/widget/ImageView;
    const v7, 0x7f0e00ed

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 80
    .local v5, "text":Landroid/widget/TextView;
    const v7, 0x7f0e00c3

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 81
    .local v6, "warningText":Landroid/widget/TextView;
    const v7, 0x7f0e00ef

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 82
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 84
    iget v7, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->mRecord_mode:I

    packed-switch v7, :pswitch_data_0

    .line 120
    :goto_0
    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 121
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 123
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    new-instance v8, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;)V

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    const v7, 0x104000a

    new-instance v8, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$2;

    invoke-direct {v8, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 166
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    return-object v7

    .line 86
    :pswitch_0
    const v7, 0x7f0b0096

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020092

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 88
    const v7, 0x7f0b0097

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 89
    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v7

    const/4 v8, 0x0

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v9

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0

    .line 93
    :pswitch_1
    const v7, 0x7f0b013f

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020091

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 95
    const v7, 0x7f0b004a

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 96
    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v7

    const/4 v8, 0x0

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v9

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0

    .line 100
    :pswitch_2
    const v7, 0x7f0b0165

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020093

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 102
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isUsaEnglish(Landroid/content/res/Resources;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 104
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "%d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/16 v11, 0x8

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f0b0093

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/16 v11, 0x14

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f0b0032

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const v8, 0x7f0b0169

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/16 v11, 0x8

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const/4 v11, 0x5

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    :goto_1
    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v7

    const/4 v8, 0x0

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v9

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_0

    .line 107
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "%d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/16 v11, 0x14

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f0b0032

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/16 v11, 0x8

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f0b0093

    invoke-virtual {p0, v8}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const v8, 0x7f0b0166

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/16 v11, 0x14

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const/4 v11, 0x5

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 113
    :pswitch_3
    const v7, 0x7f0b016b

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 114
    sget-boolean v7, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_CHINA_WLAN:Z

    if-eqz v7, :cond_2

    const v4, 0x7f0b0168

    .line 116
    .local v4, "messageId":I
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 114
    .end local v4    # "messageId":I
    :cond_2
    const v4, 0x7f0b0167

    goto :goto_2

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v1, 0x100

    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 173
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 7

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 179
    .local v0, "context":Landroid/content/Context;
    sget-boolean v4, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_DATA_CHECK_POPUP:Z

    if-nez v4, :cond_0

    const-string v4, "show_stt_recc_info2"

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_0

    .line 181
    if-eqz v0, :cond_0

    iget v4, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->mRecord_mode:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 182
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isWiFiConnected(Landroid/content/Context;)Z

    move-result v3

    .line 183
    .local v3, "isWifi":Z
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isMobileDataConnected(Landroid/content/Context;)Z

    move-result v2

    .line 184
    .local v2, "isMobileData":Z
    if-nez v3, :cond_0

    if-eqz v2, :cond_0

    .line 186
    const/4 v4, 0x4

    :try_start_0
    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->newInstance(I)Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "FRAGMENT_DIALOG"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    .end local v2    # "isMobileData":Z
    .end local v3    # "isWifi":Z
    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 195
    return-void

    .line 188
    .restart local v2    # "isMobileData":Z
    .restart local v3    # "isWifi":Z
    :catch_0
    move-exception v1

    .line 189
    .local v1, "e":Ljava/lang/IllegalStateException;
    const-string v4, "VNAdvancedRecordingInfoDialogFragment"

    const-string v5, "catch the exception while onDestroyView the VNAdvancedRecordingInfoDialogFragment"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNAdvancedRecordingInfoDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    const v1, 0x7f0b006f

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 66
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 67
    return-void
.end method
