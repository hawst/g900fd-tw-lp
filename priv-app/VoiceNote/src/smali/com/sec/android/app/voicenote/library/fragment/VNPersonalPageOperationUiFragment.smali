.class public Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
.super Landroid/app/DialogFragment;
.source "VNPersonalPageOperationUiFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;
    }
.end annotation


# static fields
.field public static final COPY_BUFFER_SIZE:I = 0x2000

.field public static final DIR_SLASH:Ljava/lang/String; = "/"

.field public static EXTERNAL_STOP:Z = false

.field public static final FILE_INSERT:I = 0x0

.field public static final FILE_OPERATION_CANCEL:I = 0x2

.field public static final FILE_OPERATION_COMPLETE:I = 0x5

.field public static final FILE_OPERATION_DONE:I = 0x1

.field public static final FILE_OPERATION_FAIL:I = 0x3

.field public static final FILE_OPERATION_FAIL_NOT_ENOUGH_SPACE:I = 0x6

.field public static final FILE_OPERATION_PROGRESS:I = 0x4

.field public static final FILE_OPERATION_START:I = 0x0

.field public static final FILE_UPDATE:I = 0x1

.field public static final IDS:Ljava/lang/String; = "ids"

.field public static final MODE:Ljava/lang/String; = "mode"

.field public static final SOUNDS_DIR:Ljava/lang/String; = "/Sounds"

.field private static final TAG:Ljava/lang/String; = "VNPersonalPageOperationUiFragment"

.field private static mPrivateModeBinder:Landroid/os/IBinder;

.field private static mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;


# instance fields
.field private callback:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$ICallback;

.field dstFile:Ljava/io/File;

.field fileNameString:Ljava/lang/String;

.field private mIDs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mMode:I

.field private mMovedCount:I

.field private mPersonalPageOperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mProgressPercent:I

.field private mProgressView:Landroid/view/View;

.field private mRemoveCount:I

.field private mfileCount:Landroid/widget/TextView;

.field private mfilePercent:Landroid/widget/TextView;

.field private state:I

.field private vnPersonalPageOperationSelectDialog:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;

.field private waitingDialogInput:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 90
    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    .line 91
    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    .line 92
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->EXTERNAL_STOP:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 120
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 82
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressView:Landroid/view/View;

    .line 83
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressBar:Landroid/widget/ProgressBar;

    .line 84
    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I

    .line 85
    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressPercent:I

    .line 86
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mIDs:Ljava/util/ArrayList;

    .line 87
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMode:I

    .line 88
    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mRemoveCount:I

    .line 93
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mfileCount:Landroid/widget/TextView;

    .line 94
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mfilePercent:Landroid/widget/TextView;

    .line 95
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z

    .line 96
    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->state:I

    .line 97
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dstFile:Ljava/io/File;

    .line 98
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->fileNameString:Ljava/lang/String;

    .line 100
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPersonalPageOperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;

    .line 669
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$3;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->callback:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$ICallback;

    .line 121
    return-void
.end method

.method private UpdateDialog()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 639
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mfileCount:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mfilePercent:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mIDs:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 650
    :cond_0
    :goto_0
    return-void

    .line 640
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mIDs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 641
    .local v2, "size":I
    if-eqz v2, :cond_0

    .line 644
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressBar:Landroid/widget/ProgressBar;

    iget v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I

    mul-int/lit8 v4, v4, 0x64

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressPercent:I

    add-int/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 645
    const-string v3, "%d / %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 646
    .local v0, "countText":Ljava/lang/String;
    const-string v3, "%d%%"

    new-array v4, v6, [Ljava/lang/Object;

    iget v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I

    mul-int/lit8 v5, v5, 0x64

    iget v6, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressPercent:I

    add-int/2addr v5, v6

    div-int/2addr v5, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 648
    .local v1, "percentText":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mfileCount:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 649
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mfilePercent:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPersonalPageOperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressPercent:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->showPrivateDialog()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->vnPersonalPageOperationSelectDialog:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->state:I

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->state:I

    return p1
.end method

.method static synthetic access$1500()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method static synthetic access$1600()Lcom/samsung/android/privatemode/PrivateModeManager;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;Ljava/io/File;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getNewFileName(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMode:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mRemoveCount:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mRemoveCount:I

    return p1
.end method

.method static synthetic access$308(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mRemoveCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mRemoveCount:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mIDs:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dismissPrivateDialog()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->UpdateDialog()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getDestFolder(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I

    return p1
.end method

.method static synthetic access$908(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMovedCount:I

    return v0
.end method

.method private createProgressDialog()Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 160
    invoke-virtual {p0, v4}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->setCancelable(Z)V

    .line 161
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 163
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 166
    .local v1, "factory":Landroid/view/LayoutInflater;
    const v2, 0x7f03003b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressView:Landroid/view/View;

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 169
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressView:Landroid/view/View;

    const v3, 0x7f0e0092

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mfileCount:Landroid/widget/TextView;

    .line 170
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressView:Landroid/view/View;

    const v3, 0x7f0e0093

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mfilePercent:Landroid/widget/TextView;

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressView:Landroid/view/View;

    const v3, 0x7f0e0091

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressBar:Landroid/widget/ProgressBar;

    .line 173
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 175
    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMode:I

    if-nez v2, :cond_0

    .line 176
    const v2, 0x7f0b00ee

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 181
    :goto_0
    new-instance v2, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 190
    const v2, 0x7f0b0021

    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 202
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2

    .line 178
    :cond_0
    const v2, 0x7f0b00f0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private dismissPrivateDialog()V
    .locals 2

    .prologue
    .line 712
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "FRAGMENT_PRIVATE_SELECT"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->dismissDialogFragment(Landroid/app/FragmentManager;Ljava/lang/String;)Z

    .line 713
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->vnPersonalPageOperationSelectDialog:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;

    .line 715
    return-void
.end method

.method private getDestFolder(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 624
    iget v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMode:I

    if-nez v2, :cond_1

    .line 625
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 635
    .local v0, "destFolderPath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 627
    .end local v0    # "destFolderPath":Ljava/lang/String;
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Sounds"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 628
    .restart local v0    # "destFolderPath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 629
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 630
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 631
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    goto :goto_0
.end method

.method private getNewFileName(Ljava/io/File;)Ljava/io/File;
    .locals 12
    .param p1, "dest"    # Ljava/io/File;

    .prologue
    const/4 v11, 0x0

    .line 694
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 695
    .local v4, "fname":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    .line 696
    .local v2, "dirName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 697
    .local v3, "extension":Ljava/lang/String;
    const/4 v5, 0x0

    .line 698
    .local v5, "fnameOnly":Ljava/lang/String;
    const-string v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    .line 699
    .local v6, "pos":I
    if-lez v6, :cond_0

    .line 700
    invoke-virtual {v4, v11, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 701
    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 703
    :cond_0
    const/4 v0, 0x0

    .line 704
    .local v0, "count":I
    :goto_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 706
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " (%d)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 707
    .local v7, "tempFileName":Ljava/lang/String;
    new-instance p1, Ljava/io/File;

    .end local p1    # "dest":Ljava/io/File;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local p1    # "dest":Ljava/io/File;
    move v0, v1

    .line 708
    .end local v1    # "count":I
    .restart local v0    # "count":I
    goto :goto_0

    .line 709
    .end local v7    # "tempFileName":Ljava/lang/String;
    :cond_1
    return-object p1
.end method

.method public static newInstance()Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    .locals 2

    .prologue
    .line 104
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;-><init>()V

    .line 105
    .local v1, "fragment":Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 106
    .local v0, "argument":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->setArguments(Landroid/os/Bundle;)V

    .line 107
    return-object v1
.end method

.method public static newInstance(Lcom/samsung/android/privatemode/PrivateModeManager;Landroid/os/IBinder;)Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    .locals 2
    .param p0, "manager"    # Lcom/samsung/android/privatemode/PrivateModeManager;
    .param p1, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 111
    sput-object p1, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    .line 112
    sput-object p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    .line 113
    new-instance v1, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;-><init>()V

    .line 114
    .local v1, "fragment":Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 115
    .local v0, "argument":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->setArguments(Landroid/os/Bundle;)V

    .line 116
    return-object v1
.end method

.method private runPersonalPageTask()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 206
    const-string v0, "VNPersonalPageOperationUiFragment"

    const-string v1, "runPersonalPageTask()"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPersonalPageOperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPersonalPageOperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 209
    const-string v0, "VNPersonalPageOperationUiFragment"

    const-string v1, "runPersonalPageTask() : cancel runned task"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPersonalPageOperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->cancel(Z)Z

    .line 214
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z

    .line 215
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$1;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPersonalPageOperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPersonalPageOperationTask:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment$PersonalPageOperationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 217
    return-void
.end method

.method private showPrivateDialog()V
    .locals 4

    .prologue
    .line 610
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dismissPrivateDialog()V

    .line 611
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->vnPersonalPageOperationSelectDialog:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;

    if-nez v1, :cond_0

    .line 612
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->callback:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$ICallback;

    invoke-static {v1}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->newInstance(Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog$ICallback;)Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->vnPersonalPageOperationSelectDialog:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;

    .line 614
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->vnPersonalPageOperationSelectDialog:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dstFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->setName(Ljava/lang/String;)V

    .line 615
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 616
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 618
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->vnPersonalPageOperationSelectDialog:Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "FRAGMENT_PRIVATE_SELECT"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationSelectDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 620
    :cond_1
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 665
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 666
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->runPersonalPageTask()V

    .line 667
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 654
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 655
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 656
    .local v0, "fragment":Landroid/app/Fragment;
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z

    .line 657
    if-eqz v0, :cond_0

    .line 658
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getTargetRequestCode()I

    move-result v1

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 660
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 125
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 127
    new-instance v0, Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 128
    .local v0, "argument":Landroid/os/Bundle;
    const-string v2, "ids"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 129
    .local v1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mIDs:Ljava/util/ArrayList;

    .line 130
    const-string v2, "mode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mMode:I

    .line 131
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->createProgressDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 151
    .local v0, "dialog":Landroid/app/Dialog;
    iget-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z

    if-eqz v1, :cond_0

    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->dismissPrivateDialog()V

    .line 153
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->waitingDialogInput:Z

    .line 155
    :cond_0
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->EXTERNAL_STOP:Z

    .line 156
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 140
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    .line 141
    sget-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    sget-object v1, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    invoke-virtual {v0, v1}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;)Z

    .line 142
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/library/fragment/VNPersonalPageOperationUiFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    .line 144
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 145
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 135
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 136
    return-void
.end method
