.class public Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;
.super Landroid/app/Fragment;
.source "VNPlayerControlIdleFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "VNPlayerControlIdleFragment"


# instance fields
.field private mBackButton:Landroid/widget/ImageButton;

.field private mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field private mEasyModeBackButton:Landroid/view/View;

.field private mModeButton:Landroid/widget/ImageButton;

.field private mRecordButton:Landroid/view/View;

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->view:Landroid/view/View;

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mRecordButton:Landroid/view/View;

    .line 48
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mEasyModeBackButton:Landroid/view/View;

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mBackButton:Landroid/widget/ImageButton;

    .line 50
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    .line 55
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 168
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 169
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 170
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 180
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onControlButtonSelected(IJ)Z

    move-result v0

    .line 182
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mBackButton:Landroid/widget/ImageButton;

    if-ne p1, v1, :cond_0

    if-eqz v0, :cond_0

    .line 183
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mBackButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 184
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mRecordButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 185
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 189
    .end local v0    # "result":Z
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const v10, 0x7f0e0099

    const/4 v9, 0x4

    const/4 v8, 0x0

    const/16 v7, 0x3031

    .line 59
    const-string v5, "VNPlayerControlIdleFragment"

    const-string v6, "onCreateView"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const v3, 0x7f03003d

    .line 62
    .local v3, "playControlIdleLayoutId":I
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 63
    const v3, 0x7f03003e

    .line 66
    :cond_0
    invoke-virtual {p1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->view:Landroid/view/View;

    .line 67
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->view:Landroid/view/View;

    if-nez v5, :cond_1

    .line 140
    :goto_0
    return-object v4

    .line 71
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->view:Landroid/view/View;

    const v5, 0x7f0e009b

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mRecordButton:Landroid/view/View;

    .line 72
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mRecordButton:Landroid/view/View;

    if-eqz v4, :cond_2

    .line 73
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mRecordButton:Landroid/view/View;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 77
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->view:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mEasyModeBackButton:Landroid/view/View;

    .line 78
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mEasyModeBackButton:Landroid/view/View;

    if-eqz v4, :cond_3

    .line 79
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mEasyModeBackButton:Landroid/view/View;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 90
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->view:Landroid/view/View;

    const v5, 0x7f0e009c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    .line 91
    sget-boolean v4, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_BEAMFORMING:Z

    if-nez v4, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITH_SVOICE(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNFeature;->FLAG_SUPPORT_STT_WITHOUT_SVOICE()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 94
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 97
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    if-eqz v4, :cond_6

    .line 98
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 99
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 100
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    const v5, 0x3ecccccd    # 0.4f

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 105
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 110
    .local v1, "activity":Landroid/app/Activity;
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_8

    const-string v4, "android.provider.MediaStore.RECORD_SOUND"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 113
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mBackButton:Landroid/widget/ImageButton;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mBackButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 114
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 117
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isHoveringUI(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 118
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mRecordButton:Landroid/view/View;

    if-eqz v4, :cond_9

    .line 119
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mRecordButton:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    .line 120
    .local v2, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v2, :cond_9

    .line 121
    invoke-virtual {v2, v7}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 125
    .end local v2    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mBackButton:Landroid/widget/ImageButton;

    if-eqz v4, :cond_a

    .line 126
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mBackButton:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    .line 127
    .restart local v2    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v2, :cond_a

    .line 128
    invoke-virtual {v2, v7}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 132
    .end local v2    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    if-eqz v4, :cond_b

    .line 133
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    .line 134
    .restart local v2    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v2, :cond_b

    .line 135
    invoke-virtual {v2, v7}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 140
    .end local v2    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->view:Landroid/view/View;

    goto/16 :goto_0

    .line 82
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "activity":Landroid/app/Activity;
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->view:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mBackButton:Landroid/widget/ImageButton;

    .line 84
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mBackButton:Landroid/widget/ImageButton;

    if-eqz v4, :cond_3

    .line 85
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mBackButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 102
    :cond_d
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 103
    iget-object v4, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setAlpha(F)V

    goto/16 :goto_2
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 158
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mRecordButton:Landroid/view/View;

    .line 159
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mBackButton:Landroid/widget/ImageButton;

    .line 160
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mModeButton:Landroid/widget/ImageButton;

    .line 161
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mEasyModeBackButton:Landroid/view/View;

    .line 163
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 164
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 175
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 176
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 147
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getActionMode()Landroid/view/ActionMode;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->getActionBarIsSearched()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 148
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNPlayerControlIdleFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 149
    .local v0, "fragmentTransaction":Landroid/app/FragmentTransaction;
    invoke-virtual {v0, p0}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 150
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 153
    .end local v0    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 154
    return-void
.end method
