.class Lcom/sec/android/app/voicenote/common/util/VNIntent$10;
.super Landroid/content/BroadcastReceiver;
.source "VNIntent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/common/util/VNIntent;->registerBroadcastReceiverMusicCommand(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/common/util/VNIntent;)V
    .locals 0

    .prologue
    .line 453
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$10;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v3, 0xfbe

    .line 456
    const-string v1, "VNIntent"

    const-string v2, "MusicService Intent Received"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    const-string v1, "com.android.music.musicservicecommand"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 459
    const-string v1, "from"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 460
    .local v0, "from":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "com.sec.android.app.voicenote"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 467
    .end local v0    # "from":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 462
    .restart local v0    # "from":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$10;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 463
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$10;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 464
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/VNIntent$10;->this$0:Lcom/sec/android/app/voicenote/common/util/VNIntent;

    # getter for: Lcom/sec/android/app/voicenote/common/util/VNIntent;->mEventHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/VNIntent;->access$100(Lcom/sec/android/app/voicenote/common/util/VNIntent;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
