.class public Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNSelectLabelDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final ADD_REQUEST:I = 0x0

.field private static final RESULT_OK:I = -0x1

.field private static final TAG:Ljava/lang/String; = "VNSelectLabelDialogFragment"

.field private static mIsShown:Z


# instance fields
.field public clickListener:Landroid/view/View$OnClickListener;

.field private currentFileId:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field private mCursor:Landroid/database/Cursor;

.field private mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

.field private mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mViewChild:Landroid/view/View;

.field private mtitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mIsShown:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mViewChild:Landroid/view/View;

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->currentFileId:Ljava/util/ArrayList;

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mtitle:Ljava/lang/String;

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 317
    new-instance v0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$3;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->clickListener:Landroid/view/View$OnClickListener;

    .line 72
    return-void
.end method

.method private initView()V
    .locals 3

    .prologue
    .line 155
    const-string v0, "VNSelectLabelDialogFragment"

    const-string v1, "initView"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mViewChild:Landroid/view/View;

    const v1, 0x7f0e0033

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mViewChild:Landroid/view/View;

    const v2, 0x7f0e0034

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 159
    return-void
.end method

.method public static isShown()Z
    .locals 1

    .prologue
    .line 389
    sget-boolean v0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mIsShown:Z

    return v0
.end method

.method private listBinding()Z
    .locals 15

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v14, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    .line 162
    const-string v0, "VNSelectLabelDialogFragment"

    const-string v1, "listBinding E"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 166
    iput-object v14, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 169
    :cond_0
    new-array v4, v3, [Ljava/lang/String;

    const-string v0, "COLOR"

    aput-object v0, v4, v10

    const-string v0, "TITLE"

    aput-object v0, v4, v11

    const-string v0, "_id"

    aput-object v0, v4, v2

    .line 174
    .local v4, "cols":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 176
    .local v5, "to":[I
    const/4 v0, 0x4

    new-array v5, v0, [I

    .line 177
    const v0, 0x7f0e0059

    aput v0, v5, v10

    .line 178
    const v0, 0x7f0e005a

    aput v0, v5, v11

    .line 179
    const v0, 0x7f0e005b

    aput v0, v5, v2

    .line 180
    const v0, 0x7f0e005c

    aput v0, v5, v3

    .line 183
    const/4 v9, 0x0

    .line 184
    .local v9, "selection":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getListQuery()Ljava/lang/StringBuilder;

    move-result-object v6

    .line 185
    .local v6, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 188
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getDB()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v9, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 203
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_2

    .line 204
    const-string v0, "VNSelectLabelDialogFragment"

    const-string v1, "listBinding() : cursor null"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v10

    .line 222
    :goto_1
    return v0

    .line 189
    :catch_0
    move-exception v7

    .line 190
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "VNSelectLabelDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "listBinding - SQLiteException :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 193
    iput-object v14, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    goto :goto_0

    .line 195
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 196
    .local v8, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v0, "VNSelectLabelDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "listBinding - UnsupportedOperationException :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 199
    iput-object v14, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    goto :goto_0

    .line 208
    .end local v8    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_2
    new-instance v0, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f030025

    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, v11}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->selectMode(Z)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, v11}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->setClearAll(Z)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, v10}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->setCategorySelected(Z)V

    .line 213
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->currentFileId:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-static {v2, v12, v13}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->findLabelID(Landroid/content/Context;J)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->selectRadionButton(I)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_2

    .line 218
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->getCount()I

    move-result v0

    if-gt v0, v11, :cond_3

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v14}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :goto_3
    move v0, v11

    .line 222
    goto/16 :goto_1

    .line 214
    :catch_2
    move-exception v7

    .line 215
    .local v7, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v7}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_2

    .line 221
    .end local v7    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_3
.end method

.method private selectRadioButton(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x1

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->setClearAll(Z)V

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->setCategorySelected(Z)V

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->selectRadionButton(I)V

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 386
    return-void
.end method


# virtual methods
.method public RefreshList()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 365
    const-string v0, "VNSelectLabelDialogFragment"

    const-string v1, "RefreshList"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    if-nez v0, :cond_0

    .line 367
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->listBinding()Z

    .line 379
    :goto_0
    return-void

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 371
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getDB()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->getListQuery()Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    goto :goto_0
.end method

.method public SetParameter(JLjava/lang/String;)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->currentFileId:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    iput-object p3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mtitle:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public SetParameter(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 1
    .param p2, "title"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 80
    .local p1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->currentFileId:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 81
    iput-object p2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mtitle:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 131
    const-string v1, "VNSelectLabelDialogFragment"

    const-string v2, "onActivityCreated"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    if-eqz v1, :cond_0

    .line 134
    const-string v1, "listview"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 136
    .local v0, "state":Landroid/os/Parcelable;
    if-eqz v0, :cond_0

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 141
    .end local v0    # "state":Landroid/os/Parcelable;
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 142
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 343
    const-string v2, "VNSelectLabelDialogFragment"

    const-string v3, "onActivityResult"

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 345
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    if-nez v2, :cond_0

    .line 362
    :goto_0
    return-void

    .line 348
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 349
    packed-switch p1, :pswitch_data_0

    .line 361
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    goto :goto_0

    .line 351
    :pswitch_0
    const/4 v2, -0x1

    if-ne p2, v2, :cond_1

    .line 352
    const-string v2, "color"

    invoke-virtual {p3, v2, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 353
    .local v0, "color":I
    const-string v2, "title"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 354
    .local v1, "title":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->insertColumn(Ljava/lang/String;Ljava/lang/String;)J

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->RefreshList()V

    goto :goto_1

    .line 349
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 86
    const-string v0, "VNSelectLabelDialogFragment"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 91
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 92
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 96
    const-string v3, "VNSelectLabelDialogFragment"

    const-string v4, "onCreateDialog"

    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    new-instance v3, Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 99
    if-eqz p1, :cond_0

    .line 100
    const-string v3, "title"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mtitle:Ljava/lang/String;

    .line 101
    const-string v3, "id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->currentFileId:Ljava/util/ArrayList;

    .line 103
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 104
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030014

    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mViewChild:Landroid/view/View;

    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->initView()V

    .line 106
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 107
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0b0117

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 108
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mViewChild:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 109
    const v3, 0x7f0b0008

    invoke-virtual {v0, v3, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 110
    const v3, 0x7f0b0021

    invoke-virtual {v0, v3, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 112
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 113
    .local v1, "dialog":Landroid/app/AlertDialog;
    new-instance v3, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 125
    const/4 v3, 0x1

    sput-boolean v3, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mIsShown:Z

    .line 126
    return-object v1
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 227
    const-string v0, "VNSelectLabelDialogFragment"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 232
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    if-eqz v0, :cond_1

    .line 235
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 239
    iput-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCursor:Landroid/database/Cursor;

    .line 241
    :cond_2
    sput-boolean v3, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mIsShown:Z

    .line 242
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 243
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "clickedView"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v9, 0x0

    .line 264
    const-string v5, "VNSelectLabelDialogFragment"

    const-string v6, "onItemClick"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->isResumed()Z

    move-result v5

    if-nez v5, :cond_0

    .line 266
    const-string v5, "VNSelectLabelDialogFragment"

    const-string v6, "already dialog is dismissed"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :goto_0
    return-void

    .line 269
    :cond_0
    invoke-direct {p0, p3}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->selectRadioButton(I)V

    .line 271
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 272
    .local v4, "sb":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->currentFileId:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 273
    .local v3, "length":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_2

    .line 274
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->currentFileId:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 275
    add-int/lit8 v5, v3, -0x1

    if-ge v2, v5, :cond_1

    .line 276
    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 279
    :cond_2
    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 283
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v5, "label_id"

    long-to-int v6, p4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 284
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id IN "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v0, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 286
    const-string v5, "VNSelectLabelDialogFragment"

    const-string v6, "Success to update label"

    invoke-static {v5, v6}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    if-eqz v5, :cond_3

    .line 296
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    invoke-interface {v5, v9}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onUpdateState(Z)V

    .line 298
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    if-eqz v5, :cond_4

    .line 299
    iget-object v5, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, v9}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 301
    :cond_4
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getHandler()Landroid/os/Handler;

    move-result-object v5

    new-instance v6, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;)V

    const-wide/16 v8, 0x64

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 287
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 288
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 289
    const-string v5, "VNSelectLabelDialogFragment"

    const-string v6, "Fail to update label"

    invoke-static {v5, v6, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 290
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const v6, 0x7f0b00fd

    invoke-static {v5, v6, v9}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->showToast(Landroid/content/Context;II)V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 247
    const-string v0, "VNSelectLabelDialogFragment"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->dismissAllowingStateLoss()V

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->open()Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    .line 252
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->listBinding()Z

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mDbOpenHelper:Lcom/sec/android/app/voicenote/common/util/LabelHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelHelper;->close()V

    .line 255
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    if-eqz v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListAdapter:Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/util/LabelSimpleCursorAdapter;->notifyDataSetChanged()V

    .line 260
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 146
    const-string v1, "VNSelectLabelDialogFragment"

    const-string v2, "onSaveInstanceState"

    invoke-static {v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v1, "id"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->currentFileId:Ljava/util/ArrayList;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 148
    const-string v1, "title"

    iget-object v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mtitle:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNSelectLabelDialogFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 150
    .local v0, "state":Landroid/os/Parcelable;
    const-string v1, "listview"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 151
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 152
    return-void
.end method
