.class Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$5;
.super Ljava/lang/Object;
.source "VNExpandableCursorAdapter.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->startCheckBoxAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)V
    .locals 0

    .prologue
    .line 1137
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$5;->this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 1161
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$5;->this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mIsCheckBoxAnimating:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->access$302(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;Z)Z

    .line 1162
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 1149
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$5;->this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    const/4 v4, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mIsCheckBoxAnimating:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->access$302(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;Z)Z

    .line 1150
    # getter for: Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->access$400()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1151
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 1152
    # getter for: Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mCheckBoxes:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->access$400()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1153
    .local v0, "checkbox":Landroid/widget/CheckBox;
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$5;->this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    # getter for: Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mIsSelectionMode:Z
    invoke-static {v3}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->access$500(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1154
    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 1151
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1157
    .end local v0    # "checkbox":Landroid/widget/CheckBox;
    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 1145
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 1140
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter$5;->this$0:Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->mIsCheckBoxAnimating:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;->access$302(Lcom/sec/android/app/voicenote/library/common/VNExpandableCursorAdapter;Z)Z

    .line 1141
    return-void
.end method
