.class Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;
.super Ljava/lang/Object;
.source "AudioSource.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/voicenote/library/stt/AudioSource;->handleSourceClosed(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

.field final synthetic val$success:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/voicenote/library/stt/AudioSource;Z)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    iput-boolean p2, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->val$success:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mStarted:Z
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->access$400(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mStarted:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->access$402(Lcom/sec/android/app/voicenote/library/stt/AudioSource;Z)Z

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mClosed:Z
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->access$500(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mClosed:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->access$502(Lcom/sec/android/app/voicenote/library/stt/AudioSource;Z)Z

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    # invokes: Lcom/sec/android/app/voicenote/library/stt/AudioSource;->notifySourceClosed()V
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->access$600(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->access$700(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)Lcom/nuance/dragon/toolkit/util/WorkerThread;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->access$700(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)Lcom/nuance/dragon/toolkit/util/WorkerThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/util/WorkerThread;->stop()V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    # setter for: Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mWorkerThread:Lcom/nuance/dragon/toolkit/util/WorkerThread;
    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->access$702(Lcom/sec/android/app/voicenote/library/stt/AudioSource;Lcom/nuance/dragon/toolkit/util/WorkerThread;)Lcom/nuance/dragon/toolkit/util/WorkerThread;

    .line 272
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mListener:Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->access$000(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 273
    iget-boolean v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->val$success:Z

    if-eqz v0, :cond_3

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mListener:Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->access$000(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;->onStopped(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)V

    .line 278
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    # setter for: Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mListener:Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;
    invoke-static {v0, v2}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->access$002(Lcom/sec/android/app/voicenote/library/stt/AudioSource;Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;)Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;

    .line 280
    :cond_2
    return-void

    .line 276
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    # getter for: Lcom/sec/android/app/voicenote/library/stt/AudioSource;->mListener:Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/library/stt/AudioSource;->access$000(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/voicenote/library/stt/AudioSource$4;->this$0:Lcom/sec/android/app/voicenote/library/stt/AudioSource;

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/library/stt/AudioSource$Listener;->onError(Lcom/sec/android/app/voicenote/library/stt/AudioSource;)V

    goto :goto_0
.end method
