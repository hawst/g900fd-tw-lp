.class Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;
.super Ljava/lang/Object;
.source "AnchorSizePopupMenu.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectionMenuAdapter"
.end annotation


# instance fields
.field private mMenuItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mObserver:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/database/DataSetObserver;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;)V
    .locals 1

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->this$0:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->mObserver:Ljava/util/ArrayList;

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->mMenuItems:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;
    .param p2, "x1"    # Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$1;

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;-><init>(Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;)V

    return-void
.end method


# virtual methods
.method addMenu(Ljava/lang/String;)V
    .locals 1
    .param p1, "menu"    # Ljava/lang/String;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->mMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->notifyDataSetChanged()V

    .line 121
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x1

    return v0
.end method

.method clearMenu()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->mMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->notifyDataSetChanged()V

    .line 131
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->mMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->mMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->this$0:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;

    # getter for: Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mMenuKeys:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->access$200(Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 168
    const/4 v0, 0x0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 155
    if-nez p2, :cond_0

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->this$0:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;

    # getter for: Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->access$300(Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030043

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 158
    :cond_0
    const v1, 0x7f0e00c4

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 159
    .local v0, "tv":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->mMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    if-nez p1, :cond_1

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->this$0:Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;

    # getter for: Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;->access$300(Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 163
    :cond_1
    return-object p2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 188
    const/4 v0, 0x1

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 3

    .prologue
    .line 202
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->mObserver:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/DataSetObserver;

    .line 203
    .local v1, "ob":Landroid/database/DataSetObserver;
    invoke-virtual {v1}, Landroid/database/DataSetObserver;->onChanged()V

    goto :goto_0

    .line 205
    .end local v1    # "ob":Landroid/database/DataSetObserver;
    :cond_0
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->mObserver:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    return-void
.end method

.method removeMenu(Ljava/lang/String;)V
    .locals 1
    .param p1, "menu"    # Ljava/lang/String;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->mMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->notifyDataSetChanged()V

    .line 126
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/util/AnchorSizePopupMenu$SelectionMenuAdapter;->mObserver:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 199
    return-void
.end method
