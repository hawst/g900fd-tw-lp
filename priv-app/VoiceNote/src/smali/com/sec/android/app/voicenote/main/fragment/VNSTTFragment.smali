.class public Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
.super Landroid/app/Fragment;
.source "VNSTTFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$6;,
        Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VNSTTFragment"

.field private static mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

.field private static mTextData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

.field private mConvertedText:Ljava/lang/CharSequence;

.field public mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

.field mEmptyView:Landroid/view/View;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mIsLongPressed:Z

.field private mIsTouching:I

.field private mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

.field mProcessingView:Landroid/widget/ImageView;

.field mProgressView:Landroid/view/ViewGroup;

.field private mRecordState:I

.field private mScrollView:Landroid/widget/ScrollView;

.field private mSelectIndex:I

.field private mSttEdit:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 66
    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;

    .line 74
    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 84
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mProgressView:Landroid/view/ViewGroup;

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mProcessingView:Landroid/widget/ImageView;

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mEmptyView:Landroid/view/View;

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mScrollView:Landroid/widget/ScrollView;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mGestureDetector:Landroid/view/GestureDetector;

    .line 72
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mIsLongPressed:Z

    .line 73
    iput v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mIsTouching:I

    .line 76
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mRecordState:I

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mConvertedText:Ljava/lang/CharSequence;

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mIsTouching:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
    .param p1, "x1"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mIsTouching:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;Landroid/widget/TextView;Landroid/view/MotionEvent;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
    .param p1, "x1"    # Landroid/widget/TextView;
    .param p2, "x2"    # Landroid/view/MotionEvent;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getOffset(Landroid/widget/TextView;Landroid/view/MotionEvent;)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
    .param p1, "x1"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I

    return p1
.end method

.method static synthetic access$300()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;Ljava/util/ArrayList;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getSelectStringIndex(Ljava/util/ArrayList;I)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;)Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
    .param p1, "x1"    # Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
    .param p1, "x1"    # Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->setVisibleState(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V

    return-void
.end method

.method private changeViewVisible(Landroid/view/View;I)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 434
    if-nez p1, :cond_1

    .line 443
    :cond_0
    :goto_0
    return-void

    .line 438
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, p2, :cond_0

    .line 442
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private doProcessingAnim(Z)V
    .locals 3
    .param p1, "anim"    # Z

    .prologue
    .line 446
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mProcessingView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 447
    .local v0, "ad":Landroid/graphics/drawable/AnimationDrawable;
    if-eqz p1, :cond_0

    .line 448
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mProcessingView:Landroid/widget/ImageView;

    new-instance v2, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$3;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$3;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;Landroid/graphics/drawable/AnimationDrawable;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 466
    :goto_0
    return-void

    .line 457
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mProcessingView:Landroid/widget/ImageView;

    new-instance v2, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$4;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$4;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;Landroid/graphics/drawable/AnimationDrawable;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private getMediaRecorderState()I
    .locals 2

    .prologue
    .line 277
    const/16 v0, 0x3e8

    .line 279
    .local v0, "state":I
    sget-object v1, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    if-eqz v1, :cond_0

    .line 280
    sget-object v1, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    invoke-virtual {v1}, Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;->getMediaRecorderState()I

    move-result v0

    .line 285
    :goto_0
    return v0

    .line 282
    :cond_0
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getRecorderState()I

    move-result v0

    goto :goto_0
.end method

.method private getOffset(Landroid/widget/TextView;Landroid/view/MotionEvent;)I
    .locals 6
    .param p1, "text"    # Landroid/widget/TextView;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 371
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v3, v5

    .line 372
    .local v3, "positionX":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v4, v5

    .line 374
    .local v4, "positionY":I
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v5

    sub-int/2addr v3, v5

    .line 375
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v5

    sub-int/2addr v4, v5

    .line 377
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 378
    .local v0, "layout":Landroid/text/Layout;
    invoke-virtual {v0, v4}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v1

    .line 379
    .local v1, "line":I
    int-to-float v5, v3

    invoke-virtual {v0, v1, v5}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v2

    .line 381
    .local v2, "off":I
    return v2
.end method

.method private getSelectStringIndex(Ljava/util/ArrayList;I)I
    .locals 7
    .param p2, "offsetToFind"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 385
    .local p1, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    if-nez p1, :cond_0

    .line 386
    const/4 v2, -0x1

    .line 402
    :goto_0
    return v2

    .line 388
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 389
    .local v0, "count":I
    const/4 v1, 0x0

    .line 390
    .local v1, "currentOffset":I
    const/4 v3, -0x1

    .line 392
    .local v3, "lastSttText":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_2

    .line 393
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget v4, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    if-nez v4, :cond_1

    .line 394
    move v3, v2

    .line 395
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v4, v4, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v1, v4

    .line 396
    if-le v1, p2, :cond_1

    .line 397
    const-string v4, "VNSTTFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getSelectStringIndex return "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 392
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v2, v3

    .line 402
    goto :goto_0
.end method

.method public static initService(Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;)V
    .locals 0
    .param p0, "service"    # Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .prologue
    .line 269
    sput-object p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 270
    return-void
.end method

.method public static newInstance(ILjava/lang/CharSequence;)Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
    .locals 3
    .param p0, "recordState"    # I
    .param p1, "convertedText"    # Ljava/lang/CharSequence;

    .prologue
    .line 88
    new-instance v1, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;

    invoke-direct {v1}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;-><init>()V

    .line 89
    .local v1, "f":Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 90
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "recordstate"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 91
    const-string v2, "prevText"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 92
    invoke-virtual {v1, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->setArguments(Landroid/os/Bundle;)V

    .line 93
    return-object v1
.end method

.method public static releaseService()V
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mService:Lcom/sec/android/app/voicenote/common/service/VoiceNoteService;

    .line 274
    return-void
.end method

.method private setVisibleState(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V
    .locals 4
    .param p1, "viewState"    # Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 406
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$6;->$SwitchMap$com$sec$android$app$voicenote$main$fragment$VNSTTFragment$ViewState:[I

    invoke-virtual {p1}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 431
    :goto_0
    return-void

    .line 409
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mEmptyView:Landroid/view/View;

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->changeViewVisible(Landroid/view/View;I)V

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mProgressView:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->changeViewVisible(Landroid/view/View;I)V

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->changeViewVisible(Landroid/view/View;I)V

    .line 412
    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->doProcessingAnim(Z)V

    goto :goto_0

    .line 416
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mEmptyView:Landroid/view/View;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->changeViewVisible(Landroid/view/View;I)V

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mProgressView:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->changeViewVisible(Landroid/view/View;I)V

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->changeViewVisible(Landroid/view/View;I)V

    .line 419
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->doProcessingAnim(Z)V

    goto :goto_0

    .line 423
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mEmptyView:Landroid/view/View;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->changeViewVisible(Landroid/view/View;I)V

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mProgressView:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->changeViewVisible(Landroid/view/View;I)V

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    invoke-direct {p0, v0, v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->changeViewVisible(Landroid/view/View;I)V

    .line 426
    invoke-direct {p0, v3}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->doProcessingAnim(Z)V

    goto :goto_0

    .line 406
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getConvertedText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 472
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDialogCallback()Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;
    .locals 1

    .prologue
    .line 483
    new-instance v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$5;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)V

    .line 550
    .local v0, "temDalogCallback":Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;
    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x3e8

    .line 98
    const-string v2, "VNSTTFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreateView : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iput v5, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mRecordState:I

    .line 101
    iput-object v6, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mConvertedText:Ljava/lang/CharSequence;

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 104
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 105
    const-string v2, "recordstate"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mRecordState:I

    .line 106
    const-string v2, "prevText"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mConvertedText:Ljava/lang/CharSequence;

    .line 109
    :cond_0
    const v2, 0x7f03001e

    invoke-virtual {p1, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 110
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f0e0049

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mScrollView:Landroid/widget/ScrollView;

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mScrollView:Landroid/widget/ScrollView;

    const v3, 0x7f0e00c8

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->setNextFocusUpId(I)V

    .line 113
    const v2, 0x7f0e004d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mProgressView:Landroid/view/ViewGroup;

    .line 114
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mProgressView:Landroid/view/ViewGroup;

    const v3, 0x7f0e004e

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mProcessingView:Landroid/widget/ImageView;

    .line 115
    const v2, 0x7f0e004f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mEmptyView:Landroid/view/View;

    .line 116
    const v2, 0x7f0e004a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    .line 117
    if-eqz p3, :cond_1

    .line 118
    const-string v2, "selectindex"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I

    .line 121
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setWritingBuddyEnabled(Z)V

    .line 123
    iget v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mRecordState:I

    const/16 v3, 0x3eb

    if-ne v2, v3, :cond_3

    .line 124
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    sget-object v2, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Converting:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->setVisibleState(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V

    .line 137
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$1;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 177
    :cond_2
    new-instance v2, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$2;-><init>(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;)V

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    .line 224
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getDialogCallback()Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    .line 225
    return-object v1

    .line 126
    :cond_3
    iget v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mRecordState:I

    if-ne v2, v5, :cond_5

    .line 127
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mConvertedText:Ljava/lang/CharSequence;

    if-eqz v2, :cond_4

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mConvertedText:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    sget-object v2, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Converted:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->setVisibleState(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V

    goto :goto_0

    .line 131
    :cond_4
    sget-object v2, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Empty:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->setVisibleState(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V

    goto :goto_0

    .line 134
    :cond_5
    sget-object v2, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Converted:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    invoke-direct {p0, v2}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->setVisibleState(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 230
    const-string v0, "VNSTTFragment"

    const-string v1, "onDestroyView"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mDialogCallback:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment$DialogCallback;

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mNbestDialog:Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/voicenote/common/fragment/VNSelectNBestDialogFragment;->dismiss()V

    .line 236
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    .line 237
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    .line 238
    iput-object v2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mScrollView:Landroid/widget/ScrollView;

    .line 240
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 241
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 262
    const-string v0, "VNSTTFragment"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->doProcessingAnim(Z)V

    .line 264
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->setCallback(Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;)V

    .line 265
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 266
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 245
    const-string v0, "VNSTTFragment"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->updateSTTScreen()V

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->updateAllWordOnScreen()V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mCallback:Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;

    invoke-static {v0}, Lcom/sec/android/app/voicenote/main/stt/SamsungSTT;->setCallback(Lcom/sec/android/app/voicenote/main/common/VNSTTInterface;)V

    .line 251
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 252
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 256
    const-string v0, "selectindex"

    iget v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 257
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 258
    return-void
.end method

.method public resetConvertedText()V
    .locals 2

    .prologue
    .line 476
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 478
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Empty:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->setVisibleState(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V

    .line 480
    :cond_0
    return-void
.end method

.method public setRecordStateNText(ILjava/lang/CharSequence;)V
    .locals 0
    .param p1, "recState"    # I
    .param p2, "convertedText"    # Ljava/lang/CharSequence;

    .prologue
    .line 309
    iput-object p2, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mConvertedText:Ljava/lang/CharSequence;

    .line 310
    iput p1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mRecordState:I

    .line 311
    return-void
.end method

.method public updateAllWordOnScreen()V
    .locals 11

    .prologue
    const/16 v10, 0x21

    const/4 v9, -0x1

    .line 314
    const-string v7, "VNSTTFragment"

    const-string v8, "updateAllWordOnScreen"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    if-nez v7, :cond_1

    .line 317
    const-string v7, "VNSTTFragment"

    const-string v8, "mSttEdit is NULL"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    :cond_0
    :goto_0
    return-void

    .line 321
    :cond_1
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNMemoData;->getSttData()Ljava/util/ArrayList;

    move-result-object v7

    sput-object v7, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;

    .line 322
    sget-object v7, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;

    if-nez v7, :cond_2

    .line 323
    const-string v7, "VNSTTFragment"

    const-string v8, "mTextData is NULL"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 326
    :cond_2
    sget-object v7, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_3

    .line 327
    const-string v7, "VNSTTFragment"

    const-string v8, "mTextData size is 0"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 331
    :cond_3
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 332
    .local v4, "ssb":Landroid/text/SpannableStringBuilder;
    sget-object v7, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 333
    .local v6, "textDataSize":I
    const/4 v3, 0x0

    .line 334
    .local v3, "selectWordOffset":I
    const/4 v2, 0x0

    .line 337
    .local v2, "selectWordLength":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v6, :cond_6

    .line 338
    :try_start_0
    sget-object v7, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mTextData:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/voicenote/common/util/TextData;

    .line 339
    .local v5, "tempData":Lcom/sec/android/app/voicenote/common/util/TextData;
    if-eqz v5, :cond_4

    iget v7, v5, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    if-nez v7, :cond_4

    .line 340
    iget-object v7, v5, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v4, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 341
    iget v7, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I

    if-eq v9, v7, :cond_4

    .line 342
    iget v7, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I

    if-ge v1, v7, :cond_5

    .line 343
    iget-object v7, v5, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v3, v7

    .line 337
    :cond_4
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 344
    :cond_5
    iget v7, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I

    if-ne v1, v7, :cond_4

    .line 345
    iget-object v7, v5, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v7}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_2

    .line 350
    .end local v5    # "tempData":Lcom/sec/android/app/voicenote/common/util/TextData;
    :catch_0
    move-exception v0

    .line 351
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v7, "VNSTTFragment"

    const-string v8, "updateAllWordOnScreen : mTextData is out of bound"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_6
    iget v7, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSelectIndex:I

    if-eq v9, v7, :cond_7

    if-eqz v2, :cond_7

    .line 355
    const-string v7, "VNSTTFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "updateAllWordOnScreen : selectWordOffset - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " lengh - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    const/high16 v8, -0x10000

    invoke-direct {v7, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int v8, v3, v2

    invoke-virtual {v4, v7, v3, v8, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 359
    new-instance v7, Landroid/text/style/StyleSpan;

    const/4 v8, 0x1

    invoke-direct {v7, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int v8, v3, v2

    invoke-virtual {v4, v7, v3, v8, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 363
    :cond_7
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 364
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    const/high16 v8, -0x1000000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 365
    iget-object v7, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    sget-object v7, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Converted:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    invoke-direct {p0, v7}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->setVisibleState(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V

    goto/16 :goto_0
.end method

.method public updateSTTScreen()V
    .locals 2

    .prologue
    .line 289
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->getMediaRecorderState()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mRecordState:I

    .line 290
    iget v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mRecordState:I

    const/16 v1, 0x3eb

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mRecordState:I

    const/16 v1, 0x3ec

    if-ne v0, v1, :cond_2

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mConvertedText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mConvertedText:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mConvertedText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    :goto_0
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Converting:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->setVisibleState(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V

    .line 306
    :goto_1
    return-void

    .line 294
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 296
    :cond_2
    iget v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mRecordState:I

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_4

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mConvertedText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mSttEdit:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->mConvertedText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 299
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Converted:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->setVisibleState(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V

    goto :goto_1

    .line 301
    :cond_3
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Empty:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->setVisibleState(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V

    goto :goto_1

    .line 304
    :cond_4
    sget-object v0, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;->Converted:Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;

    invoke-direct {p0, v0}, Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment;->setVisibleState(Lcom/sec/android/app/voicenote/main/fragment/VNSTTFragment$ViewState;)V

    goto :goto_1
.end method
