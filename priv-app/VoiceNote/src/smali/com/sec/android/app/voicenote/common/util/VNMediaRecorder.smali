.class public Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;
.super Lcom/sec/android/secmediarecorder/SecMediaRecorder;
.source "VNMediaRecorder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;-><init>()V

    return-void
.end method


# virtual methods
.method public prepare(Ljava/lang/String;Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;J)V
    .locals 7
    .param p1, "file"    # Ljava/lang/String;
    .param p2, "audio"    # Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;
    .param p3, "freesize"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    const-wide/16 v0, 0x0

    .line 30
    .local v0, "maxFileSize":J
    invoke-virtual {p2}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->getRecordMode()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    .line 31
    invoke-virtual {p2}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->getFilesize()J

    move-result-wide v0

    .line 32
    const-string v2, "audio/amr"

    invoke-virtual {p2}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 33
    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getByte(JZ)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setMaxDuration(I)V

    .line 49
    :goto_0
    invoke-virtual {p2}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->getRecordMode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 74
    :goto_1
    invoke-virtual {p2}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->getOutputFormat()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setOutputFormat(I)V

    .line 75
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setMaxFileSize(J)V

    .line 77
    invoke-virtual {p0, p1}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p2}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->getAudioEncodingBitrate()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setAudioEncodingBitRate(I)V

    .line 79
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setAudioChannels(I)V

    .line 80
    invoke-virtual {p2}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->getAudioSamplingRate()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setAudioSamplingRate(I)V

    .line 81
    invoke-virtual {p2}, Lcom/sec/android/app/voicenote/common/util/VNAudioFormat;->getAudioEncoder()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setAudioEncoder(I)V

    .line 82
    const/16 v2, 0x64

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setDurationInterval(I)V

    .line 83
    const-wide/16 v2, 0x300

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setFileSizeInterval(J)V

    .line 84
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setAuthor(I)V

    .line 85
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->getLatitude()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->getLongitude()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_1

    .line 86
    :cond_0
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->getLatitude()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNLocationManager;->getLongitude()D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setLocation(FF)V

    .line 89
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->prepare()V

    .line 90
    return-void

    .line 35
    :cond_2
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getByte(JZ)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setMaxDuration(I)V

    goto :goto_0

    .line 37
    :cond_3
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->isMmsMode()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 38
    const/16 v2, 0x3e80

    invoke-static {p3, p4, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getRemainingMmsSize(JI)J

    move-result-wide v0

    goto :goto_0

    .line 39
    :cond_4
    const-string v2, "record_mode"

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_5

    .line 40
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getInternalStorageSelected()Z

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkAvailableStorage(Z)Z

    .line 41
    sget-wide v2, Lcom/sec/android/app/voicenote/common/util/VNUtil;->avaliableSize:J

    const-wide/32 v4, 0x1457800

    sub-long v0, v2, v4

    .line 42
    const v2, 0x494a8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setMaxDuration(I)V

    goto/16 :goto_0

    .line 44
    :cond_5
    invoke-static {}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getInternalStorageSelected()Z

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->checkAvailableStorage(Z)Z

    .line 45
    sget-wide v2, Lcom/sec/android/app/voicenote/common/util/VNUtil;->avaliableSize:J

    const-wide/32 v4, 0x1457800

    sub-long v0, v2, v4

    .line 46
    const v2, 0x22554e8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setMaxDuration(I)V

    goto/16 :goto_0

    .line 51
    :pswitch_0
    const-string v2, "recording_volume"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->getSettings(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_6

    .line 52
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setAudioSource(I)V

    goto/16 :goto_1

    .line 54
    :cond_6
    const/16 v2, 0xe

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setAudioSource(I)V

    goto/16 :goto_1

    .line 59
    :pswitch_1
    const/16 v2, 0x15

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setAudioSource(I)V

    goto/16 :goto_1

    .line 63
    :pswitch_2
    const/16 v2, 0x15

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setAudioSource(I)V

    goto/16 :goto_1

    .line 67
    :pswitch_3
    const/4 v2, 0x6

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setAudioSource(I)V

    goto/16 :goto_1

    .line 71
    :pswitch_4
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/voicenote/common/util/VNMediaRecorder;->setAudioSource(I)V

    goto/16 :goto_1

    .line 49
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
