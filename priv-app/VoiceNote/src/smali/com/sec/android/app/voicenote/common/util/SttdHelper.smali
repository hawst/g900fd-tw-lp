.class public Lcom/sec/android/app/voicenote/common/util/SttdHelper;
.super Ljava/lang/Object;
.source "SttdHelper.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final TEMP_NAME:Ljava/lang/String;

.field private inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

.field private invalidInit:Z

.field private mContext:Landroid/content/Context;

.field private final newSTTD:[B

.field private newSttdLength:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/common/util/M4aInfo;Landroid/content/Context;)V
    .locals 3
    .param p1, "inf"    # Lcom/sec/android/app/voicenote/common/util/M4aInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->newSTTD:[B

    .line 47
    const-string v0, "SttdHelper"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->TAG:Ljava/lang/String;

    .line 48
    iput-boolean v1, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->invalidInit:Z

    .line 49
    const-string v0, "temp3223293.m4a"

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->TEMP_NAME:Ljava/lang/String;

    .line 50
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    .line 51
    iput v1, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->newSttdLength:I

    .line 53
    iput-object v2, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->mContext:Landroid/content/Context;

    .line 57
    if-eqz p1, :cond_0

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    .line 59
    iget-boolean v0, p1, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->usedToWrite:Z

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->invalidInit:Z

    .line 63
    :goto_0
    iput-object p2, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->mContext:Landroid/content/Context;

    .line 64
    return-void

    .line 61
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->invalidInit:Z

    goto :goto_0

    .line 43
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x73t
        0x74t
        0x74t
        0x64t
    .end array-data
.end method

.method private exportToFile(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/voicenote/common/util/TextData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    const/4 v8, 0x0

    .line 314
    if-nez p1, :cond_0

    .line 315
    const-string v7, "SttdHelper"

    const-string v8, "exportToFile() path is null"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :goto_0
    return-void

    .line 319
    :cond_0
    const/16 v7, 0x2e

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    invoke-virtual {p1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 320
    .local v2, "export_filepath":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_memo.txt"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 322
    const/4 v5, 0x0

    .line 323
    .local v5, "os":Ljava/io/FileOutputStream;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 325
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 326
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 329
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 330
    :cond_2
    const-string v7, "SttdHelper"

    const-string v8, "exportToFile() list is null or empty"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 333
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 336
    .local v0, "builder":Ljava/lang/StringBuilder;
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 337
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 338
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .local v6, "os":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    :try_start_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v4, v7, :cond_5

    .line 339
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget v7, v7, Lcom/sec/android/app/voicenote/common/util/TextData;->dataType:I

    if-nez v7, :cond_4

    .line 340
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/voicenote/common/util/TextData;

    iget-object v7, v7, Lcom/sec/android/app/voicenote/common/util/TextData;->mText:[Ljava/lang/String;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 343
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/FileOutputStream;->write([B)V

    .line 345
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->mContext:Landroid/content/Context;

    if-eqz v7, :cond_6

    .line 346
    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->mContext:Landroid/content/Context;

    invoke-static {v7, v2}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->sendScan(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 357
    :cond_6
    :try_start_2
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-object v5, v6

    .line 361
    .end local v4    # "i":I
    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    :goto_2
    const-string v7, "SttdHelper"

    const-string v8, "exportToFile() x"

    invoke-static {v7, v8}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 358
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v4    # "i":I
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v7

    move-object v5, v6

    .line 360
    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 349
    .end local v4    # "i":I
    :catch_1
    move-exception v1

    .line 351
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 357
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 358
    :catch_2
    move-exception v7

    goto :goto_2

    .line 352
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v1

    .line 354
    .local v1, "e":Ljava/io/IOException;
    :goto_4
    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 357
    :try_start_6
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_2

    .line 358
    :catch_4
    move-exception v7

    goto :goto_2

    .line 356
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 357
    :goto_5
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 359
    :goto_6
    throw v7

    .line 358
    :catch_5
    move-exception v8

    goto :goto_6

    .line 356
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v4    # "i":I
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 352
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v1

    move-object v5, v6

    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 349
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v1

    move-object v5, v6

    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_3
.end method

.method private updateOuterAtomsLengths(Ljava/nio/channels/FileChannel;)V
    .locals 8
    .param p1, "channel"    # Ljava/nio/channels/FileChannel;

    .prologue
    .line 286
    const-wide/16 v4, 0x0

    .line 287
    .local v4, "savePos":J
    const/4 v6, 0x4

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 288
    .local v0, "buff":Ljava/nio/ByteBuffer;
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget v6, v6, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileUdtaLength:I

    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget v7, v7, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileSttdLength:I

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->newSttdLength:I

    add-int v3, v6, v7

    .line 289
    .local v3, "newUdtaLength":I
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget v6, v6, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileMoovLength:I

    iget-object v7, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget v7, v7, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileUdtaLength:I

    sub-int/2addr v6, v7

    add-int v2, v6, v3

    .line 290
    .local v2, "newMoovLength":I
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iput v3, v6, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileUdtaLength:I

    .line 291
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iput v2, v6, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileMoovLength:I

    .line 293
    :try_start_0
    invoke-virtual {p1}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v4

    .line 295
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 296
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 297
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v6, v6, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->udtaPos:J

    invoke-virtual {p1, v6, v7}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 298
    invoke-virtual {p1, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 299
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 301
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 302
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 303
    iget-object v6, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v6, v6, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->moovPos:J

    invoke-virtual {p1, v6, v7}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 304
    invoke-virtual {p1, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 305
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 307
    invoke-virtual {p1, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 311
    :goto_0
    return-void

    .line 308
    :catch_0
    move-exception v1

    .line 309
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public overwriteSttd(Ljava/io/Serializable;)V
    .locals 21
    .param p1, "serializable"    # Ljava/io/Serializable;

    .prologue
    .line 67
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->invalidInit:Z

    if-eqz v4, :cond_0

    .line 68
    const-string v4, "SttdHelper"

    const-string v5, "overwriteSttd() invalid init false"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    .end local p1    # "serializable":Ljava/io/Serializable;
    :goto_0
    return-void

    .line 72
    .restart local p1    # "serializable":Ljava/io/Serializable;
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->usedToWrite:Z

    .line 74
    new-instance v9, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v9}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 75
    .local v9, "bos":Ljava/io/ByteArrayOutputStream;
    const/4 v12, 0x0

    .line 76
    .local v12, "out":Ljava/io/ObjectOutput;
    const/4 v10, 0x0

    .line 77
    .local v10, "dataBytes":[B
    const/16 v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->newSttdLength:I

    .line 79
    :try_start_0
    new-instance v13, Ljava/io/ObjectOutputStream;

    invoke-direct {v13, v9}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    .end local v12    # "out":Ljava/io/ObjectOutput;
    .local v13, "out":Ljava/io/ObjectOutput;
    :try_start_1
    move-object/from16 v0, p1

    invoke-interface {v13, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    .line 81
    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    .line 82
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->newSttdLength:I

    array-length v5, v10

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->newSttdLength:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 87
    :try_start_2
    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 88
    invoke-interface {v13}, Ljava/io/ObjectOutput;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v12, v13

    .line 93
    .end local v13    # "out":Ljava/io/ObjectOutput;
    .restart local v12    # "out":Ljava/io/ObjectOutput;
    :goto_1
    if-nez v10, :cond_1

    .line 94
    const-string v4, "SttdHelper"

    const-string v5, "overwriteSttd() data bytes is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/voicenote/common/util/VNLog;->V(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 89
    .end local v12    # "out":Ljava/io/ObjectOutput;
    .restart local v13    # "out":Ljava/io/ObjectOutput;
    :catch_0
    move-exception v11

    .line 90
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    move-object v12, v13

    .line 92
    .end local v13    # "out":Ljava/io/ObjectOutput;
    .restart local v12    # "out":Ljava/io/ObjectOutput;
    goto :goto_1

    .line 83
    .end local v11    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v11

    .line 84
    .restart local v11    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 87
    :try_start_4
    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 88
    invoke-interface {v12}, Ljava/io/ObjectOutput;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 89
    :catch_2
    move-exception v11

    .line 90
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 86
    .end local v11    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 87
    :goto_3
    :try_start_5
    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 88
    invoke-interface {v12}, Ljava/io/ObjectOutput;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 91
    :goto_4
    throw v4

    .line 89
    :catch_3
    move-exception v11

    .line 90
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 98
    .end local v11    # "e":Ljava/io/IOException;
    :cond_1
    invoke-static {v10}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v16

    .line 100
    .local v16, "sttData":Ljava/nio/ByteBuffer;
    const/4 v14, 0x0

    .line 101
    .local v14, "raf":Ljava/io/RandomAccessFile;
    const/16 v18, 0x0

    .line 102
    .local v18, "temp":Ljava/io/RandomAccessFile;
    const/4 v3, 0x0

    .line 103
    .local v3, "srcWrite":Ljava/nio/channels/FileChannel;
    const/4 v2, 0x0

    .line 105
    .local v2, "dst":Ljava/nio/channels/FileChannel;
    :try_start_6
    new-instance v15, Ljava/io/RandomAccessFile;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-object v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->path:Ljava/lang/String;

    const-string v5, "rw"

    invoke-direct {v15, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 106
    .end local v14    # "raf":Ljava/io/RandomAccessFile;
    .local v15, "raf":Ljava/io/RandomAccessFile;
    :try_start_7
    new-instance v19, Ljava/io/RandomAccessFile;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "temp3223293.m4a"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "rw"

    move-object/from16 v0, v19

    invoke-direct {v0, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 108
    .end local v18    # "temp":Ljava/io/RandomAccessFile;
    .local v19, "temp":Ljava/io/RandomAccessFile;
    :try_start_8
    invoke-virtual {v15}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    .line 109
    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 111
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-boolean v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z

    if-eqz v4, :cond_2

    .line 112
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->sttdPos:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget v6, v6, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileSttdLength:I

    int-to-long v6, v6

    add-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 113
    const-wide/16 v4, 0x0

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 114
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->sttdPos:J

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 122
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v6

    iput-wide v6, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->sttdPos:J

    .line 123
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->newSTTD:[B

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v17

    .line 124
    .local v17, "sttdBuff":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->newSttdLength:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 125
    invoke-virtual/range {v17 .. v17}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 126
    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 129
    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 131
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v5

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v7

    move-object v4, v2

    invoke-virtual/range {v3 .. v8}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 135
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->updateOuterAtomsLengths(Ljava/nio/channels/FileChannel;)V

    .line 136
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z

    .line 137
    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->close()V

    .line 138
    invoke-virtual {v15}, Ljava/io/RandomAccessFile;->close()V

    .line 140
    new-instance v20, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "temp3223293.m4a"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 142
    .local v20, "toDelete":Ljava/io/File;
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 149
    :try_start_9
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 150
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 151
    invoke-virtual {v15}, Ljava/io/RandomAccessFile;->close()V

    .line 152
    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    move-object/from16 v18, v19

    .end local v19    # "temp":Ljava/io/RandomAccessFile;
    .restart local v18    # "temp":Ljava/io/RandomAccessFile;
    move-object v14, v15

    .line 157
    .end local v15    # "raf":Ljava/io/RandomAccessFile;
    .end local v17    # "sttdBuff":Ljava/nio/ByteBuffer;
    .end local v20    # "toDelete":Ljava/io/File;
    .restart local v14    # "raf":Ljava/io/RandomAccessFile;
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-object v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->path:Ljava/lang/String;

    check-cast p1, Ljava/util/ArrayList;

    .end local p1    # "serializable":Ljava/io/Serializable;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->exportToFile(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 159
    const-string v4, "SttdHelper"

    const-string v5, "overwriteSttd has ended"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 116
    .end local v14    # "raf":Ljava/io/RandomAccessFile;
    .end local v18    # "temp":Ljava/io/RandomAccessFile;
    .restart local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v19    # "temp":Ljava/io/RandomAccessFile;
    .restart local p1    # "serializable":Ljava/io/Serializable;
    :cond_2
    :try_start_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->udtaPos:J

    const-wide/16 v6, 0x8

    add-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 118
    const-wide/16 v4, 0x0

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 119
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->udtaPos:J

    const-wide/16 v6, 0x8

    add-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    goto/16 :goto_5

    .line 144
    :catch_4
    move-exception v11

    move-object/from16 v18, v19

    .end local v19    # "temp":Ljava/io/RandomAccessFile;
    .restart local v18    # "temp":Ljava/io/RandomAccessFile;
    move-object v14, v15

    .line 145
    .end local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v11    # "e":Ljava/io/IOException;
    .restart local v14    # "raf":Ljava/io/RandomAccessFile;
    :goto_7
    :try_start_b
    const-string v4, "SttdHelper"

    const-string v5, "Error writing STTD to file"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 149
    :try_start_c
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 150
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 151
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V

    .line 152
    invoke-virtual/range {v18 .. v18}, Ljava/io/RandomAccessFile;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    goto :goto_6

    .line 153
    :catch_5
    move-exception v4

    goto :goto_6

    .end local v11    # "e":Ljava/io/IOException;
    .end local v14    # "raf":Ljava/io/RandomAccessFile;
    .end local v18    # "temp":Ljava/io/RandomAccessFile;
    .restart local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v17    # "sttdBuff":Ljava/nio/ByteBuffer;
    .restart local v19    # "temp":Ljava/io/RandomAccessFile;
    .restart local v20    # "toDelete":Ljava/io/File;
    :catch_6
    move-exception v4

    move-object/from16 v18, v19

    .end local v19    # "temp":Ljava/io/RandomAccessFile;
    .restart local v18    # "temp":Ljava/io/RandomAccessFile;
    move-object v14, v15

    .line 155
    .end local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v14    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_6

    .line 148
    .end local v17    # "sttdBuff":Ljava/nio/ByteBuffer;
    .end local v20    # "toDelete":Ljava/io/File;
    :catchall_1
    move-exception v4

    .line 149
    :goto_8
    :try_start_d
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 150
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 151
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V

    .line 152
    invoke-virtual/range {v18 .. v18}, Ljava/io/RandomAccessFile;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_7

    .line 154
    :goto_9
    throw v4

    .line 153
    :catch_7
    move-exception v5

    goto :goto_9

    .line 148
    .end local v14    # "raf":Ljava/io/RandomAccessFile;
    .restart local v15    # "raf":Ljava/io/RandomAccessFile;
    :catchall_2
    move-exception v4

    move-object v14, v15

    .end local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v14    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_8

    .end local v14    # "raf":Ljava/io/RandomAccessFile;
    .end local v18    # "temp":Ljava/io/RandomAccessFile;
    .restart local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v19    # "temp":Ljava/io/RandomAccessFile;
    :catchall_3
    move-exception v4

    move-object/from16 v18, v19

    .end local v19    # "temp":Ljava/io/RandomAccessFile;
    .restart local v18    # "temp":Ljava/io/RandomAccessFile;
    move-object v14, v15

    .end local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v14    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_8

    .line 144
    :catch_8
    move-exception v11

    goto :goto_7

    .end local v14    # "raf":Ljava/io/RandomAccessFile;
    .restart local v15    # "raf":Ljava/io/RandomAccessFile;
    :catch_9
    move-exception v11

    move-object v14, v15

    .end local v15    # "raf":Ljava/io/RandomAccessFile;
    .restart local v14    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_7

    .line 86
    .end local v2    # "dst":Ljava/nio/channels/FileChannel;
    .end local v3    # "srcWrite":Ljava/nio/channels/FileChannel;
    .end local v12    # "out":Ljava/io/ObjectOutput;
    .end local v14    # "raf":Ljava/io/RandomAccessFile;
    .end local v16    # "sttData":Ljava/nio/ByteBuffer;
    .end local v18    # "temp":Ljava/io/RandomAccessFile;
    .restart local v13    # "out":Ljava/io/ObjectOutput;
    :catchall_4
    move-exception v4

    move-object v12, v13

    .end local v13    # "out":Ljava/io/ObjectOutput;
    .restart local v12    # "out":Ljava/io/ObjectOutput;
    goto/16 :goto_3

    .line 83
    .end local v12    # "out":Ljava/io/ObjectOutput;
    .restart local v13    # "out":Ljava/io/ObjectOutput;
    :catch_a
    move-exception v11

    move-object v12, v13

    .end local v13    # "out":Ljava/io/ObjectOutput;
    .restart local v12    # "out":Ljava/io/ObjectOutput;
    goto/16 :goto_2
.end method

.method public readSttd()Ljava/io/Serializable;
    .locals 18

    .prologue
    .line 163
    const/4 v11, 0x0

    .line 165
    .local v11, "result":Ljava/io/Serializable;
    const/4 v4, 0x0

    .line 166
    .local v4, "countRead":I
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->invalidInit:Z

    if-eqz v13, :cond_1

    move-object v13, v11

    .line 212
    :cond_0
    :goto_0
    return-object v13

    .line 169
    :cond_1
    const/4 v9, 0x0

    .line 170
    .local v9, "raf":Ljava/io/RandomAccessFile;
    const/4 v12, 0x0

    .line 172
    .local v12, "srcRead":Ljava/nio/channels/FileChannel;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-boolean v13, v13, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z

    if-eqz v13, :cond_6

    .line 173
    new-instance v10, Ljava/io/RandomAccessFile;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-object v13, v13, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->path:Ljava/lang/String;

    const-string v14, "r"

    invoke-direct {v10, v13, v14}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 174
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .local v10, "raf":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v12

    .line 175
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget v13, v13, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileSttdLength:I

    add-int/lit8 v13, v13, -0x8

    invoke-static {v13}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 176
    .local v3, "buff":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v14, v13, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->sttdPos:J

    const-wide/16 v16, 0x8

    add-long v14, v14, v16

    invoke-virtual {v12, v14, v15}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 177
    invoke-virtual {v12, v3}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    .line 178
    if-gez v4, :cond_3

    const/4 v13, 0x0

    .line 206
    if-eqz v12, :cond_2

    :try_start_2
    invoke-virtual {v12}, Ljava/nio/channels/FileChannel;->close()V

    .line 207
    :cond_2
    if-eqz v10, :cond_0

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 208
    :catch_0
    move-exception v14

    goto :goto_0

    .line 180
    :cond_3
    :try_start_3
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 181
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    .line 182
    .local v5, "dataBytes":[B
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 183
    .local v2, "bis":Ljava/io/ByteArrayInputStream;
    const/4 v7, 0x0

    .line 185
    .local v7, "in":Ljava/io/ObjectInput;
    :try_start_4
    new-instance v8, Ljava/io/ObjectInputStream;

    invoke-direct {v8, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 186
    .end local v7    # "in":Ljava/io/ObjectInput;
    .local v8, "in":Ljava/io/ObjectInput;
    :try_start_5
    invoke-interface {v8}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v13

    move-object v0, v13

    check-cast v0, Ljava/io/Serializable;

    move-object v11, v0
    :try_end_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 190
    if-eqz v2, :cond_4

    .line 191
    :try_start_6
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->close()V

    .line 193
    :cond_4
    if-eqz v8, :cond_f

    .line 194
    const-string v13, "SttdHelper"

    const-string v14, "Error reading object data from file"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    invoke-interface {v8}, Ljava/io/ObjectInput;->close()V

    move-object v7, v8

    .line 198
    .end local v8    # "in":Ljava/io/ObjectInput;
    .restart local v7    # "in":Ljava/io/ObjectInput;
    :cond_5
    :goto_1
    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-object v9, v10

    .line 206
    .end local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .end local v3    # "buff":Ljava/nio/ByteBuffer;
    .end local v5    # "dataBytes":[B
    .end local v7    # "in":Ljava/io/ObjectInput;
    .end local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    :cond_6
    if-eqz v12, :cond_7

    :try_start_7
    invoke-virtual {v12}, Ljava/nio/channels/FileChannel;->close()V

    .line 207
    :cond_7
    if-eqz v9, :cond_8

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    :cond_8
    :goto_2
    move-object v13, v11

    .line 212
    goto/16 :goto_0

    .line 187
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "buff":Ljava/nio/ByteBuffer;
    .restart local v5    # "dataBytes":[B
    .restart local v7    # "in":Ljava/io/ObjectInput;
    .restart local v10    # "raf":Ljava/io/RandomAccessFile;
    :catch_1
    move-exception v6

    .line 188
    .local v6, "e":Ljava/lang/ClassNotFoundException;
    :goto_3
    :try_start_8
    invoke-virtual {v6}, Ljava/lang/ClassNotFoundException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 190
    if-eqz v2, :cond_9

    .line 191
    :try_start_9
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->close()V

    .line 193
    :cond_9
    if-eqz v7, :cond_5

    .line 194
    const-string v13, "SttdHelper"

    const-string v14, "Error reading object data from file"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    invoke-interface {v7}, Ljava/io/ObjectInput;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_1

    .line 200
    .end local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .end local v3    # "buff":Ljava/nio/ByteBuffer;
    .end local v5    # "dataBytes":[B
    .end local v6    # "e":Ljava/lang/ClassNotFoundException;
    .end local v7    # "in":Ljava/io/ObjectInput;
    :catch_2
    move-exception v6

    move-object v9, v10

    .line 201
    .end local v10    # "raf":Ljava/io/RandomAccessFile;
    .local v6, "e":Ljava/io/IOException;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    :goto_4
    :try_start_a
    const-string v13, "SttdHelper"

    const-string v14, "Error reading data from file"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    const/4 v11, 0x0

    .line 203
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 206
    if-eqz v12, :cond_a

    :try_start_b
    invoke-virtual {v12}, Ljava/nio/channels/FileChannel;->close()V

    .line 207
    :cond_a
    if-eqz v9, :cond_8

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3

    goto :goto_2

    .line 208
    :catch_3
    move-exception v13

    goto :goto_2

    .line 190
    .end local v6    # "e":Ljava/io/IOException;
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "buff":Ljava/nio/ByteBuffer;
    .restart local v5    # "dataBytes":[B
    .restart local v7    # "in":Ljava/io/ObjectInput;
    .restart local v10    # "raf":Ljava/io/RandomAccessFile;
    :catchall_0
    move-exception v13

    :goto_5
    if-eqz v2, :cond_b

    .line 191
    :try_start_c
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->close()V

    .line 193
    :cond_b
    if-eqz v7, :cond_c

    .line 194
    const-string v14, "SttdHelper"

    const-string v15, "Error reading object data from file"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    invoke-interface {v7}, Ljava/io/ObjectInput;->close()V

    :cond_c
    throw v13
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 205
    .end local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .end local v3    # "buff":Ljava/nio/ByteBuffer;
    .end local v5    # "dataBytes":[B
    .end local v7    # "in":Ljava/io/ObjectInput;
    :catchall_1
    move-exception v13

    move-object v9, v10

    .line 206
    .end local v10    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    :goto_6
    if-eqz v12, :cond_d

    :try_start_d
    invoke-virtual {v12}, Ljava/nio/channels/FileChannel;->close()V

    .line 207
    :cond_d
    if-eqz v9, :cond_e

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4

    .line 209
    :cond_e
    :goto_7
    throw v13

    .line 208
    :catch_4
    move-exception v14

    goto :goto_7

    .line 205
    :catchall_2
    move-exception v13

    goto :goto_6

    .line 200
    :catch_5
    move-exception v6

    goto :goto_4

    .line 208
    :catch_6
    move-exception v13

    goto :goto_2

    .line 190
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v2    # "bis":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "buff":Ljava/nio/ByteBuffer;
    .restart local v5    # "dataBytes":[B
    .restart local v8    # "in":Ljava/io/ObjectInput;
    .restart local v10    # "raf":Ljava/io/RandomAccessFile;
    :catchall_3
    move-exception v13

    move-object v7, v8

    .end local v8    # "in":Ljava/io/ObjectInput;
    .restart local v7    # "in":Ljava/io/ObjectInput;
    goto :goto_5

    .line 187
    .end local v7    # "in":Ljava/io/ObjectInput;
    .restart local v8    # "in":Ljava/io/ObjectInput;
    :catch_7
    move-exception v6

    move-object v7, v8

    .end local v8    # "in":Ljava/io/ObjectInput;
    .restart local v7    # "in":Ljava/io/ObjectInput;
    goto :goto_3

    .end local v7    # "in":Ljava/io/ObjectInput;
    .restart local v8    # "in":Ljava/io/ObjectInput;
    :cond_f
    move-object v7, v8

    .end local v8    # "in":Ljava/io/ObjectInput;
    .restart local v7    # "in":Ljava/io/ObjectInput;
    goto :goto_1
.end method

.method public removeSttd()V
    .locals 13

    .prologue
    .line 216
    iget-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->invalidInit:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-boolean v2, v2, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z

    if-nez v2, :cond_1

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    const/4 v8, 0x0

    .line 219
    .local v8, "raf":Ljava/io/RandomAccessFile;
    const/4 v10, 0x0

    .line 220
    .local v10, "temp":Ljava/io/RandomAccessFile;
    const/4 v1, 0x0

    .line 221
    .local v1, "srcWrite":Ljava/nio/channels/FileChannel;
    const/4 v0, 0x0

    .line 223
    .local v0, "dst":Ljava/nio/channels/FileChannel;
    :try_start_0
    new-instance v9, Ljava/io/RandomAccessFile;

    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-object v2, v2, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->path:Ljava/lang/String;

    const-string v3, "rw"

    invoke-direct {v9, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    .end local v8    # "raf":Ljava/io/RandomAccessFile;
    .local v9, "raf":Ljava/io/RandomAccessFile;
    :try_start_1
    new-instance v11, Ljava/io/RandomAccessFile;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "temp3223293.m4a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "rw"

    invoke-direct {v11, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 226
    .end local v10    # "temp":Ljava/io/RandomAccessFile;
    .local v11, "temp":Ljava/io/RandomAccessFile;
    :try_start_2
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 227
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 229
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v2, v2, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->sttdPos:J

    iget-object v4, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget v4, v4, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileSttdLength:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 230
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 232
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    iget-wide v2, v2, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->sttdPos:J

    invoke-virtual {v1, v2, v3}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 233
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v3

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v5

    move-object v2, v0

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J

    .line 234
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/nio/channels/FileChannel;->truncate(J)Ljava/nio/channels/FileChannel;

    .line 236
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->newSttdLength:I

    .line 237
    invoke-direct {p0, v1}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->updateOuterAtomsLengths(Ljava/nio/channels/FileChannel;)V

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z

    .line 239
    iget-object v2, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->inf:Lcom/sec/android/app/voicenote/common/util/M4aInfo;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->usedToWrite:Z

    .line 240
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->invalidInit:Z

    .line 242
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->close()V

    .line 243
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V

    .line 244
    new-instance v12, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "temp3223293.m4a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v12, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 247
    .local v12, "toDelete":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 253
    :try_start_3
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 254
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V

    .line 255
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V

    .line 256
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-object v10, v11

    .end local v11    # "temp":Ljava/io/RandomAccessFile;
    .restart local v10    # "temp":Ljava/io/RandomAccessFile;
    move-object v8, v9

    .line 258
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "raf":Ljava/io/RandomAccessFile;
    goto/16 :goto_0

    .line 257
    .end local v8    # "raf":Ljava/io/RandomAccessFile;
    .end local v10    # "temp":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v11    # "temp":Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v2

    move-object v10, v11

    .end local v11    # "temp":Ljava/io/RandomAccessFile;
    .restart local v10    # "temp":Ljava/io/RandomAccessFile;
    move-object v8, v9

    .line 259
    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "raf":Ljava/io/RandomAccessFile;
    goto/16 :goto_0

    .line 248
    .end local v12    # "toDelete":Ljava/io/File;
    :catch_1
    move-exception v7

    .line 249
    .local v7, "e":Ljava/io/IOException;
    :goto_1
    :try_start_4
    const-string v2, "SttdHelper"

    const-string v3, "Error removing Sttd from file"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 253
    :try_start_5
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 254
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V

    .line 255
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V

    .line 256
    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 257
    :catch_2
    move-exception v2

    goto/16 :goto_0

    .line 252
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 253
    :goto_2
    :try_start_6
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 254
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V

    .line 255
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V

    .line 256
    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 258
    :goto_3
    throw v2

    .line 257
    :catch_3
    move-exception v3

    goto :goto_3

    .line 252
    .end local v8    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v2

    move-object v8, v9

    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_2

    .end local v8    # "raf":Ljava/io/RandomAccessFile;
    .end local v10    # "temp":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v11    # "temp":Ljava/io/RandomAccessFile;
    :catchall_2
    move-exception v2

    move-object v10, v11

    .end local v11    # "temp":Ljava/io/RandomAccessFile;
    .restart local v10    # "temp":Ljava/io/RandomAccessFile;
    move-object v8, v9

    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_2

    .line 248
    .end local v8    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    :catch_4
    move-exception v7

    move-object v8, v9

    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_1

    .end local v8    # "raf":Ljava/io/RandomAccessFile;
    .end local v10    # "temp":Ljava/io/RandomAccessFile;
    .restart local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v11    # "temp":Ljava/io/RandomAccessFile;
    :catch_5
    move-exception v7

    move-object v10, v11

    .end local v11    # "temp":Ljava/io/RandomAccessFile;
    .restart local v10    # "temp":Ljava/io/RandomAccessFile;
    move-object v8, v9

    .end local v9    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_1
.end method

.method public trimSttData(II)V
    .locals 8
    .param p1, "startTime"    # I
    .param p2, "endTime"    # I

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->readSttd()Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    .line 264
    .local v4, "sttData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 265
    .local v3, "newSttData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    if-eqz v4, :cond_2

    .line 266
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 269
    .local v5, "sttIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 270
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/voicenote/common/util/TextData;

    .line 271
    .local v2, "checkedSttText":Lcom/sec/android/app/voicenote/common/util/TextData;
    iget-wide v0, v2, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    .line 272
    .local v0, "checkedElapsed":J
    int-to-long v6, p1

    cmp-long v6, v0, v6

    if-lez v6, :cond_0

    int-to-long v6, p2

    cmp-long v6, v0, v6

    if-gez v6, :cond_0

    .line 273
    int-to-long v6, p1

    sub-long v6, v0, v6

    iput-wide v6, v2, Lcom/sec/android/app/voicenote/common/util/TextData;->elapsedTime:J

    .line 274
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 277
    .end local v0    # "checkedElapsed":J
    .end local v2    # "checkedSttText":Lcom/sec/android/app/voicenote/common/util/TextData;
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_3

    .line 278
    invoke-virtual {p0, v3}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->overwriteSttd(Ljava/io/Serializable;)V

    .line 283
    .end local v5    # "sttIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    :cond_2
    :goto_1
    return-void

    .line 280
    .restart local v5    # "sttIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/voicenote/common/util/TextData;>;"
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/util/SttdHelper;->removeSttd()V

    goto :goto_1
.end method
