.class Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;
.super Lcom/nuance/dragon/toolkit/audio/AudioChunk;
.source "BufferingPipe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomAudioChunk"
.end annotation


# instance fields
.field public mEndTimeStamp:J

.field public mTimeStamp:J

.field final synthetic this$0:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;Lcom/nuance/dragon/toolkit/audio/AudioType;[BI)V
    .locals 0
    .param p2, "type"    # Lcom/nuance/dragon/toolkit/audio/AudioType;
    .param p3, "data"    # [B
    .param p4, "duration"    # I

    .prologue
    .line 145
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>.CustomAudioChunk;"
    iput-object p1, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;->this$0:Lcom/sec/android/app/voicenote/library/stt/BufferingPipe;

    .line 146
    invoke-direct {p0, p2, p3, p4}, Lcom/nuance/dragon/toolkit/audio/AudioChunk;-><init>(Lcom/nuance/dragon/toolkit/audio/AudioType;[BI)V

    .line 147
    return-void
.end method


# virtual methods
.method public setTimeStamp(JI)V
    .locals 5
    .param p1, "timeStamp"    # J
    .param p3, "duration"    # I

    .prologue
    .line 150
    .local p0, "this":Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;, "Lcom/sec/android/app/voicenote/library/stt/BufferingPipe<TAudioChunkType;>.CustomAudioChunk;"
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;->mTimeStamp:J

    .line 151
    iget-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;->mTimeStamp:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/stt/BufferingPipe$CustomAudioChunk;->mEndTimeStamp:J

    .line 152
    return-void
.end method
