.class public Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNLocationTagInfoDialogFragment.java"


# instance fields
.field private mCheckbox:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 47
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 48
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f03005f

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 49
    .local v1, "layout":Landroid/view/View;
    const v3, 0x7f0e00c3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 50
    .local v2, "warningText":Landroid/widget/TextView;
    const v3, 0x7f0e00ef

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 52
    const v3, 0x7f0b0016

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 53
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 54
    const v3, 0x7f0b00a4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 56
    iget-object v3, p0, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    new-instance v4, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    const v3, 0x104000a

    new-instance v4, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 83
    const/high16 v3, 0x1040000

    invoke-virtual {v0, v3, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 84
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/voicenote/common/fragment/VNLocationTagInfoDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    const v1, 0x7f0b006f

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 90
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 91
    return-void
.end method
