.class public Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;
.super Landroid/app/DialogFragment;
.source "VNWarningAlertDialogFragment.java"


# instance fields
.field private currentPlayID:J

.field private mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

.field private mCheckbox:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 42
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->currentPlayID:J

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->doPositiveAction()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->doNegativeAction()V

    return-void
.end method

.method private doNegativeAction()V
    .locals 0

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->dismiss()V

    .line 100
    return-void
.end method

.method private doPositiveAction()V
    .locals 4

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    const-string v0, "show_trim_warning"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    .line 93
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->currentPlayID:J

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->getCurrentPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/voicenote/common/util/VNUtil;->isExistFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;->onWarningDialogResult(I)V

    .line 95
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->dismiss()V

    .line 96
    return-void

    .line 90
    :cond_1
    const-string v0, "show_trim_warning"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/voicenote/common/util/VNSettings;->setSettings(Ljava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 104
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    iput-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->mCallbacks:Lcom/sec/android/app/voicenote/common/util/VNLibraryCallbacks;

    .line 105
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 106
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 54
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 56
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f03005f

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 57
    .local v1, "layout":Landroid/view/View;
    const v3, 0x7f0e00c3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 58
    .local v2, "warningText":Landroid/widget/TextView;
    const v3, 0x7f0e00ef

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 60
    const v3, 0x7f0b017b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 61
    const v3, 0x7f0b0157

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 62
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 64
    iget-object v3, p0, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment$1;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    const v3, 0x104000a

    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment$2;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 76
    const/high16 v3, 0x1040000

    new-instance v4, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment$3;-><init>(Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 83
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    const v1, 0x7f0b006f

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 111
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 112
    return-void
.end method

.method public setCurrentPlayingID(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 49
    iput-wide p1, p0, Lcom/sec/android/app/voicenote/library/fragment/VNWarningAlertDialogFragment;->currentPlayID:J

    .line 50
    return-void
.end method
