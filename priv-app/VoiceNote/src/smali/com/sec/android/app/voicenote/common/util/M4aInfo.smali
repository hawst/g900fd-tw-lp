.class public Lcom/sec/android/app/voicenote/common/util/M4aInfo;
.super Ljava/lang/Object;
.source "M4aInfo.java"


# instance fields
.field public bnumPos:J

.field public bookPos:J

.field public fileBnumLength:I

.field public fileBookLength:I

.field public fileImagLength:I

.field public fileMemoLength:I

.field public fileMoovLength:I

.field public filePos:J

.field public fileSttdLength:I

.field public fileTextLength:I

.field public fileThumLength:I

.field public fileUdtaLength:I

.field public hasBookmarks:Z

.field public hasMemo:Z

.field public hasSttd:Z

.field public memoPos:J

.field public moovPos:J

.field public path:Ljava/lang/String;

.field public sttdPos:J

.field public textPos:J

.field public thumPos:J

.field public udtaPos:J

.field public usedToWrite:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const-wide/16 v0, -0x1

    const/4 v2, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileMoovLength:I

    .line 24
    iput v3, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileUdtaLength:I

    .line 26
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileBookLength:I

    .line 27
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileBnumLength:I

    .line 29
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileMemoLength:I

    .line 30
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileImagLength:I

    .line 31
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileThumLength:I

    .line 32
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileTextLength:I

    .line 34
    iput v2, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->fileSttdLength:I

    .line 36
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->moovPos:J

    .line 37
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->udtaPos:J

    .line 38
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->bookPos:J

    .line 39
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->bnumPos:J

    .line 41
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->memoPos:J

    .line 42
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->filePos:J

    .line 43
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->thumPos:J

    .line 44
    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->textPos:J

    .line 46
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->sttdPos:J

    .line 48
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasBookmarks:Z

    .line 49
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasMemo:Z

    .line 50
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->hasSttd:Z

    .line 51
    iput-boolean v2, p0, Lcom/sec/android/app/voicenote/common/util/M4aInfo;->usedToWrite:Z

    return-void
.end method
