.class public abstract Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub;
.super Landroid/os/Binder;
.source "IContainerInstallerManagerService.java"

# interfaces
.implements Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.knox.containeragent.service.containerinstallermanager.IContainerInstallerManagerService"

.field static final TRANSACTION_KNOXFileRelayCancel:I = 0x9

.field static final TRANSACTION_KNOXFileRelayRequest:I = 0x8

.field static final TRANSACTION_getLaunchIntentForPackage:I = 0x4

.field static final TRANSACTION_installPackage:I = 0x1

.field static final TRANSACTION_isContainerAvailable:I = 0x3

.field static final TRANSACTION_isKNOXFileRelayAvailable:I = 0x7

.field static final TRANSACTION_resetLockTimer:I = 0x5

.field static final TRANSACTION_uninstallPackage:I = 0x2

.field static final TRANSACTION_upgradeCompleted:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.knox.containeragent.service.containerinstallermanager.IContainerInstallerManagerService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.knox.containeragent.service.containerinstallermanager.IContainerInstallerManagerService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 7
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 140
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v5

    :goto_0
    return v5

    .line 42
    :sswitch_0
    const-string v4, "com.sec.knox.containeragent.service.containerinstallermanager.IContainerInstallerManagerService"

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v6, "com.sec.knox.containeragent.service.containerinstallermanager.IContainerInstallerManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback;

    move-result-object v2

    .line 52
    .local v2, "_arg1":Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback;
    invoke-virtual {p0, v0, v2}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub;->installPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback;)Z

    move-result v3

    .line 53
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 54
    if-eqz v3, :cond_0

    move v4, v5

    :cond_0
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 59
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback;
    .end local v3    # "_result":Z
    :sswitch_2
    const-string v6, "com.sec.knox.containeragent.service.containerinstallermanager.IContainerInstallerManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 61
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 63
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback;

    move-result-object v2

    .line 64
    .restart local v2    # "_arg1":Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback;
    invoke-virtual {p0, v0, v2}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub;->uninstallPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback;)Z

    move-result v3

    .line 65
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 66
    if-eqz v3, :cond_1

    move v4, v5

    :cond_1
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 71
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback;
    .end local v3    # "_result":Z
    :sswitch_3
    const-string v6, "com.sec.knox.containeragent.service.containerinstallermanager.IContainerInstallerManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub;->isContainerAvailable()Z

    move-result v3

    .line 73
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 74
    if-eqz v3, :cond_2

    move v4, v5

    :cond_2
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 79
    .end local v3    # "_result":Z
    :sswitch_4
    const-string v6, "com.sec.knox.containeragent.service.containerinstallermanager.IContainerInstallerManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 82
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 83
    .local v3, "_result":Landroid/content/Intent;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 84
    if-eqz v3, :cond_3

    .line 85
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    invoke-virtual {v3, p3, v5}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 89
    :cond_3
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 95
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v3    # "_result":Landroid/content/Intent;
    :sswitch_5
    const-string v4, "com.sec.knox.containeragent.service.containerinstallermanager.IContainerInstallerManagerService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 96
    invoke-virtual {p0}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub;->resetLockTimer()V

    .line 97
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 102
    :sswitch_6
    const-string v6, "com.sec.knox.containeragent.service.containerinstallermanager.IContainerInstallerManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 104
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_4

    move v0, v5

    .line 105
    .local v0, "_arg0":Z
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub;->upgradeCompleted(Z)V

    .line 106
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    :cond_4
    move v0, v4

    .line 104
    goto :goto_1

    .line 111
    :sswitch_7
    const-string v6, "com.sec.knox.containeragent.service.containerinstallermanager.IContainerInstallerManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub;->isKNOXFileRelayAvailable()Z

    move-result v3

    .line 113
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 114
    if-eqz v3, :cond_5

    move v4, v5

    :cond_5
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 119
    .end local v3    # "_result":Z
    :sswitch_8
    const-string v4, "com.sec.knox.containeragent.service.containerinstallermanager.IContainerInstallerManagerService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 121
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 123
    .local v1, "_arg0":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 124
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub;->KNOXFileRelayRequest(Ljava/util/List;Ljava/lang/String;)V

    .line 125
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 130
    .end local v1    # "_arg0":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "_arg1":Ljava/lang/String;
    :sswitch_9
    const-string v4, "com.sec.knox.containeragent.service.containerinstallermanager.IContainerInstallerManagerService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 132
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 134
    .restart local v1    # "_arg0":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 135
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService$Stub;->KNOXFileRelayCancel(Ljava/util/List;Ljava/lang/String;)V

    .line 136
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
