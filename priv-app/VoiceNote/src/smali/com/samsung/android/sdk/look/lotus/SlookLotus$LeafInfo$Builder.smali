.class public Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
.super Ljava/lang/Object;
.source "SlookLotus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mBundle:Landroid/os/Bundle;

.field private mButtonIntent_1:Landroid/content/Intent;

.field private mButtonIntent_2:Landroid/content/Intent;

.field private mButtonIntent_3:Landroid/content/Intent;

.field private mButtonIntent_4:Landroid/content/Intent;

.field private mButtonIntent_5:Landroid/content/Intent;

.field private mButtonString_1:Ljava/lang/String;

.field private mButtonString_2:Ljava/lang/String;

.field private mButtonString_3:Ljava/lang/String;

.field private mButtonString_4:Ljava/lang/String;

.field private mButtonString_5:Ljava/lang/String;

.field private mButtonURI_1:Ljava/lang/String;

.field private mButtonURI_2:Ljava/lang/String;

.field private mButtonURI_3:Ljava/lang/String;

.field private mButtonURI_4:Ljava/lang/String;

.field private mButtonURI_5:Ljava/lang/String;

.field private mButtonVolatile_1:I

.field private mButtonVolatile_2:I

.field private mButtonVolatile_3:I

.field private mButtonVolatile_4:I

.field private mButtonVolatile_5:I

.field private mCardGroupId:I

.field private mCardId:I

.field private mCardPriority:I

.field private mCardViewType:I

.field private mCheck:I

.field private mCheckIntent:Landroid/content/Intent;

.field private mCheckVolatile:I

.field private mContentIntent:Landroid/content/Intent;

.field private mFileNameDescription:Ljava/lang/String;

.field private mGroupPriority:I

.field private mImageURI_1:Ljava/lang/String;

.field private mImageURI_2:Ljava/lang/String;

.field private mImageURI_3:Ljava/lang/String;

.field private mImageURI_4:Ljava/lang/String;

.field private mRelayAction:I

.field private mRemoteView:Landroid/widget/RemoteViews;

.field private mSubTitle:Ljava/lang/String;

.field private mSubTitle_2:Ljava/lang/String;

.field private mSwitch:I

.field private mSwitchIntent:Landroid/content/Intent;

.field private mSwitchVolatile:I

.field private mTextDescription:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mVolatile:I


# direct methods
.method public constructor <init>(III)V
    .locals 2
    .param p1, "leafViewType"    # I
    .param p2, "leafGroupId"    # I
    .param p3, "leafId"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 516
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardViewType:I

    .line 518
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardGroupId:I

    .line 520
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardId:I

    .line 522
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mVolatile:I

    .line 525
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mGroupPriority:I

    .line 527
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardPriority:I

    .line 530
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mTitle:Ljava/lang/String;

    .line 532
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSubTitle:Ljava/lang/String;

    .line 534
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSubTitle_2:Ljava/lang/String;

    .line 536
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mTextDescription:Ljava/lang/String;

    .line 538
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mFileNameDescription:Ljava/lang/String;

    .line 541
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_1:Ljava/lang/String;

    .line 543
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_2:Ljava/lang/String;

    .line 545
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_3:Ljava/lang/String;

    .line 547
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_4:Ljava/lang/String;

    .line 549
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mContentIntent:Landroid/content/Intent;

    .line 552
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_1:Ljava/lang/String;

    .line 554
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_1:Ljava/lang/String;

    .line 556
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_1:Landroid/content/Intent;

    .line 558
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_1:I

    .line 561
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_2:Ljava/lang/String;

    .line 563
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_2:Ljava/lang/String;

    .line 565
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_2:Landroid/content/Intent;

    .line 567
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_2:I

    .line 570
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_3:Ljava/lang/String;

    .line 572
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_3:Ljava/lang/String;

    .line 574
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_3:Landroid/content/Intent;

    .line 576
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_3:I

    .line 579
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_4:Ljava/lang/String;

    .line 581
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_4:Ljava/lang/String;

    .line 583
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_4:Landroid/content/Intent;

    .line 585
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_4:I

    .line 588
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_5:Ljava/lang/String;

    .line 590
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_5:Ljava/lang/String;

    .line 592
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_5:Landroid/content/Intent;

    .line 594
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_5:I

    .line 597
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSwitch:I

    .line 599
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSwitchIntent:Landroid/content/Intent;

    .line 601
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSwitchVolatile:I

    .line 604
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCheck:I

    .line 606
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCheckIntent:Landroid/content/Intent;

    .line 608
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCheckVolatile:I

    .line 611
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mRemoteView:Landroid/widget/RemoteViews;

    .line 614
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mBundle:Landroid/os/Bundle;

    .line 617
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mRelayAction:I

    .line 620
    iput p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardViewType:I

    .line 621
    iput p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardGroupId:I

    .line 622
    iput p3, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardId:I

    .line 623
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mVolatile:I

    .line 624
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 2
    .param p1, "leafViewType"    # I
    .param p2, "leafGroupId"    # I
    .param p3, "leafId"    # I
    .param p4, "leafLife"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 516
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardViewType:I

    .line 518
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardGroupId:I

    .line 520
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardId:I

    .line 522
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mVolatile:I

    .line 525
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mGroupPriority:I

    .line 527
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardPriority:I

    .line 530
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mTitle:Ljava/lang/String;

    .line 532
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSubTitle:Ljava/lang/String;

    .line 534
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSubTitle_2:Ljava/lang/String;

    .line 536
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mTextDescription:Ljava/lang/String;

    .line 538
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mFileNameDescription:Ljava/lang/String;

    .line 541
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_1:Ljava/lang/String;

    .line 543
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_2:Ljava/lang/String;

    .line 545
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_3:Ljava/lang/String;

    .line 547
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_4:Ljava/lang/String;

    .line 549
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mContentIntent:Landroid/content/Intent;

    .line 552
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_1:Ljava/lang/String;

    .line 554
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_1:Ljava/lang/String;

    .line 556
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_1:Landroid/content/Intent;

    .line 558
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_1:I

    .line 561
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_2:Ljava/lang/String;

    .line 563
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_2:Ljava/lang/String;

    .line 565
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_2:Landroid/content/Intent;

    .line 567
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_2:I

    .line 570
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_3:Ljava/lang/String;

    .line 572
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_3:Ljava/lang/String;

    .line 574
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_3:Landroid/content/Intent;

    .line 576
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_3:I

    .line 579
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_4:Ljava/lang/String;

    .line 581
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_4:Ljava/lang/String;

    .line 583
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_4:Landroid/content/Intent;

    .line 585
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_4:I

    .line 588
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_5:Ljava/lang/String;

    .line 590
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_5:Ljava/lang/String;

    .line 592
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_5:Landroid/content/Intent;

    .line 594
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_5:I

    .line 597
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSwitch:I

    .line 599
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSwitchIntent:Landroid/content/Intent;

    .line 601
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSwitchVolatile:I

    .line 604
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCheck:I

    .line 606
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCheckIntent:Landroid/content/Intent;

    .line 608
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCheckVolatile:I

    .line 611
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mRemoteView:Landroid/widget/RemoteViews;

    .line 614
    iput-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mBundle:Landroid/os/Bundle;

    .line 617
    iput v1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mRelayAction:I

    .line 627
    iput p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardViewType:I

    .line 628
    iput p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardGroupId:I

    .line 629
    iput p3, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardId:I

    .line 630
    iput p4, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mVolatile:I

    .line 631
    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSubTitle_2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mTextDescription:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mFileNameDescription:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_1:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_3:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_4:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mContentIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_1:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_1:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardViewType:I

    return v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_1:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_1:I

    return v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_2:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_2:I

    return v0
.end method

.method static synthetic access$2600(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_3:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_3:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_3:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_3:I

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardGroupId:I

    return v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_4:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_4:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_4:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_4:I

    return v0
.end method

.method static synthetic access$3400(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_5:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_5:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_5:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_5:I

    return v0
.end method

.method static synthetic access$3800(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSwitch:I

    return v0
.end method

.method static synthetic access$3900(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSwitchIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardId:I

    return v0
.end method

.method static synthetic access$4000(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSwitchVolatile:I

    return v0
.end method

.method static synthetic access$4100(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCheck:I

    return v0
.end method

.method static synthetic access$4200(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCheckIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCheckVolatile:I

    return v0
.end method

.method static synthetic access$4400(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Landroid/widget/RemoteViews;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mRemoteView:Landroid/widget/RemoteViews;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mRelayAction:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mVolatile:I

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mGroupPriority:I

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardPriority:I

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSubTitle:Ljava/lang/String;

    return-object v0
.end method

.method private printBuildInfo()V
    .locals 3

    .prologue
    .line 822
    const-string v0, "SlookLotus"

    const-string v1, "<< building info >>"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 823
    const-string v1, "SlookLotus"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardViewType:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "GroupID : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardGroupId:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "LeafID : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardId:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Volatile : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mVolatile:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Priority : Group - "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mGroupPriority:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Card : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardPriority:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Title : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "SubTitle : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSubTitle:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Description : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mTextDescription:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Description(file) : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mFileNameDescription:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ImageURI_1 : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_1:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ImageURI_2 : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_2:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ImageURI_3 : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_3:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ImageURI_4 : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_4:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ContentsIntent : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mContentIntent:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ButtonURI_1 : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_1:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / String : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_1:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / Intent : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_1:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ButtonURI_2 : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_2:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / String : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_2:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / Intent : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_2:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ButtonURI_3 : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_3:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / String : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_3:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / Intent : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_3:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ButtonURI_4 : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_4:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / String : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_4:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / Intent : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_4:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ButtonURI_5 : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_5:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / String : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_5:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / Intent : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_5:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Switch : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSwitch:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / Intent : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSwitchIntent:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Check : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCheck:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / Intent : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCheckIntent:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Bundle : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mBundle:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "RelayAction : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mRelayAction:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "RemoteView : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mRemoteView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 864
    return-void

    .line 823
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mBundle:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo;
    .locals 2

    .prologue
    .line 817
    invoke-direct {p0}, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->printBuildInfo()V

    .line 818
    new-instance v0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo;-><init>(Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;Lcom/samsung/android/sdk/look/lotus/SlookLotus$1;)V

    return-object v0
.end method

.method public setButtonVolatile_1(I)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "buttonVolatile"    # I

    .prologue
    .line 755
    iput p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_1:I

    .line 756
    return-object p0
.end method

.method public setButtonVolatile_2(I)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "buttonVolatile"    # I

    .prologue
    .line 760
    iput p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_2:I

    .line 761
    return-object p0
.end method

.method public setButtonVolatile_3(I)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "buttonVolatile"    # I

    .prologue
    .line 765
    iput p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_3:I

    .line 766
    return-object p0
.end method

.method public setButtonVolatile_4(I)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "buttonVolatile"    # I

    .prologue
    .line 770
    iput p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_4:I

    .line 771
    return-object p0
.end method

.method public setButtonVolatile_5(I)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "buttonVolatile"    # I

    .prologue
    .line 775
    iput p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonVolatile_5:I

    .line 776
    return-object p0
.end method

.method public setButton_1(Ljava/lang/String;Landroid/content/Intent;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "btnImage_1"    # Ljava/lang/String;
    .param p2, "buttonIntent_1"    # Landroid/content/Intent;

    .prologue
    .line 690
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_1:Ljava/lang/String;

    .line 691
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_1:Landroid/content/Intent;

    .line 692
    return-object p0
.end method

.method public setButton_1(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "btnImage_1"    # Ljava/lang/String;
    .param p2, "btnString_1"    # Ljava/lang/String;
    .param p3, "buttonIntent_1"    # Landroid/content/Intent;

    .prologue
    .line 720
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_1:Ljava/lang/String;

    .line 721
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_1:Ljava/lang/String;

    .line 722
    iput-object p3, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_1:Landroid/content/Intent;

    .line 723
    return-object p0
.end method

.method public setButton_2(Ljava/lang/String;Landroid/content/Intent;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "btnImage_2"    # Ljava/lang/String;
    .param p2, "buttonIntent_2"    # Landroid/content/Intent;

    .prologue
    .line 696
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_2:Ljava/lang/String;

    .line 697
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_2:Landroid/content/Intent;

    .line 698
    return-object p0
.end method

.method public setButton_2(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "btnImage_2"    # Ljava/lang/String;
    .param p2, "btnString_2"    # Ljava/lang/String;
    .param p3, "buttonIntent_2"    # Landroid/content/Intent;

    .prologue
    .line 727
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_2:Ljava/lang/String;

    .line 728
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_2:Ljava/lang/String;

    .line 729
    iput-object p3, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_2:Landroid/content/Intent;

    .line 730
    return-object p0
.end method

.method public setButton_3(Ljava/lang/String;Landroid/content/Intent;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "btnImage_3"    # Ljava/lang/String;
    .param p2, "buttonIntent_3"    # Landroid/content/Intent;

    .prologue
    .line 702
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_3:Ljava/lang/String;

    .line 703
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_3:Landroid/content/Intent;

    .line 704
    return-object p0
.end method

.method public setButton_3(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "btnImage_3"    # Ljava/lang/String;
    .param p2, "btnString_3"    # Ljava/lang/String;
    .param p3, "buttonIntent_3"    # Landroid/content/Intent;

    .prologue
    .line 734
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_3:Ljava/lang/String;

    .line 735
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_3:Ljava/lang/String;

    .line 736
    iput-object p3, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_3:Landroid/content/Intent;

    .line 737
    return-object p0
.end method

.method public setButton_4(Ljava/lang/String;Landroid/content/Intent;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "btnImage_4"    # Ljava/lang/String;
    .param p2, "buttonIntent_4"    # Landroid/content/Intent;

    .prologue
    .line 708
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_4:Ljava/lang/String;

    .line 709
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_4:Landroid/content/Intent;

    .line 710
    return-object p0
.end method

.method public setButton_4(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "btnImage_4"    # Ljava/lang/String;
    .param p2, "btnString_4"    # Ljava/lang/String;
    .param p3, "buttonIntent_4"    # Landroid/content/Intent;

    .prologue
    .line 741
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_4:Ljava/lang/String;

    .line 742
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_4:Ljava/lang/String;

    .line 743
    iput-object p3, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_4:Landroid/content/Intent;

    .line 744
    return-object p0
.end method

.method public setButton_5(Ljava/lang/String;Landroid/content/Intent;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "btnImage_5"    # Ljava/lang/String;
    .param p2, "buttonIntent_5"    # Landroid/content/Intent;

    .prologue
    .line 714
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_5:Ljava/lang/String;

    .line 715
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_5:Landroid/content/Intent;

    .line 716
    return-object p0
.end method

.method public setButton_5(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "btnImage_5"    # Ljava/lang/String;
    .param p2, "btnString_5"    # Ljava/lang/String;
    .param p3, "buttonIntent_5"    # Landroid/content/Intent;

    .prologue
    .line 748
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonURI_5:Ljava/lang/String;

    .line 749
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonString_5:Ljava/lang/String;

    .line 750
    iput-object p3, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mButtonIntent_5:Landroid/content/Intent;

    .line 751
    return-object p0
.end method

.method public setCardPriority(I)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "priority"    # I

    .prologue
    .line 639
    iput p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCardPriority:I

    .line 640
    return-object p0
.end method

.method public setCheck(ILandroid/content/Intent;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "checkState"    # I
    .param p2, "checkIntent"    # Landroid/content/Intent;

    .prologue
    .line 791
    iput p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCheck:I

    .line 792
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCheckIntent:Landroid/content/Intent;

    .line 793
    return-object p0
.end method

.method public setCheckVolatile(I)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "checkVolatile"    # I

    .prologue
    .line 797
    iput p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mCheckVolatile:I

    .line 798
    return-object p0
.end method

.method public setContentIntent(Landroid/content/Intent;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "contentIntent"    # Landroid/content/Intent;

    .prologue
    .line 685
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mContentIntent:Landroid/content/Intent;

    .line 686
    return-object p0
.end method

.method public setGroupPriority(I)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "priority"    # I

    .prologue
    .line 634
    iput p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mGroupPriority:I

    .line 635
    return-object p0
.end method

.method public setImage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "imageUri_1"    # Ljava/lang/String;
    .param p2, "imageUri_2"    # Ljava/lang/String;
    .param p3, "imageUri_3"    # Ljava/lang/String;
    .param p4, "imageUri_4"    # Ljava/lang/String;

    .prologue
    .line 677
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_1:Ljava/lang/String;

    .line 678
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_2:Ljava/lang/String;

    .line 679
    iput-object p3, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_3:Ljava/lang/String;

    .line 680
    iput-object p4, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mImageURI_4:Ljava/lang/String;

    .line 681
    return-object p0
.end method

.method public setRelayAction(I)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "relayAction"    # I

    .prologue
    .line 812
    iput p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mRelayAction:I

    .line 813
    return-object p0
.end method

.method public setRemoteView(Landroid/widget/RemoteViews;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "remoteView"    # Landroid/widget/RemoteViews;

    .prologue
    .line 802
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mRemoteView:Landroid/widget/RemoteViews;

    .line 803
    return-object p0
.end method

.method public setSpecificExtraData(Landroid/os/Bundle;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 807
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mBundle:Landroid/os/Bundle;

    .line 808
    return-object p0
.end method

.method public setSwitch(ILandroid/content/Intent;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "switchState"    # I
    .param p2, "switchIntent"    # Landroid/content/Intent;

    .prologue
    .line 780
    iput p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSwitch:I

    .line 781
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSwitchIntent:Landroid/content/Intent;

    .line 782
    return-object p0
.end method

.method public setSwitchVolatile(I)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "switchVolatile"    # I

    .prologue
    .line 786
    iput p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSwitchVolatile:I

    .line 787
    return-object p0
.end method

.method public setText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "subTitle"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 644
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mTitle:Ljava/lang/String;

    .line 645
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSubTitle:Ljava/lang/String;

    .line 646
    iput-object p3, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mTextDescription:Ljava/lang/String;

    .line 647
    return-object p0
.end method

.method public setText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "subTitle"    # Ljava/lang/String;
    .param p3, "subTitle_2"    # Ljava/lang/String;
    .param p4, "description"    # Ljava/lang/String;

    .prologue
    .line 652
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mTitle:Ljava/lang/String;

    .line 653
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSubTitle:Ljava/lang/String;

    .line 654
    iput-object p3, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSubTitle_2:Ljava/lang/String;

    .line 655
    iput-object p4, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mTextDescription:Ljava/lang/String;

    .line 656
    return-object p0
.end method

.method public setTextFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "subTitle"    # Ljava/lang/String;
    .param p3, "fileNameDescription"    # Ljava/lang/String;

    .prologue
    .line 660
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mTitle:Ljava/lang/String;

    .line 661
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSubTitle:Ljava/lang/String;

    .line 662
    iput-object p3, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mFileNameDescription:Ljava/lang/String;

    .line 663
    return-object p0
.end method

.method public setTextFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "subTitle"    # Ljava/lang/String;
    .param p3, "subTitle_2"    # Ljava/lang/String;
    .param p4, "fileNameDescription"    # Ljava/lang/String;

    .prologue
    .line 668
    iput-object p1, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mTitle:Ljava/lang/String;

    .line 669
    iput-object p2, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSubTitle:Ljava/lang/String;

    .line 670
    iput-object p3, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mSubTitle_2:Ljava/lang/String;

    .line 671
    iput-object p4, p0, Lcom/samsung/android/sdk/look/lotus/SlookLotus$LeafInfo$Builder;->mFileNameDescription:Ljava/lang/String;

    .line 672
    return-object p0
.end method
