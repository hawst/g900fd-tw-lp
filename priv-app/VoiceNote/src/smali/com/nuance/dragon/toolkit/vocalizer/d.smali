.class Lcom/nuance/dragon/toolkit/vocalizer/d;
.super Ljava/lang/Object;


# static fields
.field public static final ALICE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final ALLISON:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final ALL_VOICES:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final ALVA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final AMELIE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final ANNA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final AUDREY:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final AVA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final CARLOS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final CARMELA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final CARMIT:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final CATARINA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final CEM:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final CLAIRE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final DAMAYANTI:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final DANIEL:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final DIEGO:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final ELLEN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final EWA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final FEDERICA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final FELIPE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final FIONA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final HENRIK:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final IOANA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final IVETA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final JOANA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final JORDI:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final JORGE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final JUAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final KANYA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final KAREN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final KYOKO:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final LAURA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final LEE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final LUCA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final LUCIANA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final MAGNUS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final MARISKA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final MEI_JIA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final MELINA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final MILENA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final MIREN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final MOIRA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final MONICA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final MONTSERRAT:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final NICOLAS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final NIKOS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final NORA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final OSKAR:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final OTOYA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final PAOLA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final PAULINA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final PETRA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final SAMANTHA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final SARA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final SATU:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final SERENA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final SIN_JI:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final SORA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final TARIK:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final TESSA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final THOMAS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final TIAN_TIAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final TOM:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final XANDER:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final YANNICK:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final YELDA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final YURI:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

.field public static final ZUZANA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->DANISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->MAGNUS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->DANISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->SARA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->NORWEGIAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->HENRIK:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->NORWEGIAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->NORA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->JAPANESE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->KYOKO:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->JAPANESE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->OTOYA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->ARABIC:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->TARIK:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->MANDARIN_TRADITIONAL:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->MEI_JIA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->CANTONESE_TRADITIONAL:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->SIN_JI:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->HUNGARIAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->MARISKA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->SOUTH_AFRICAN_ENGLISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->FIONA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->POLISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->EWA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->BRAZILIAN_PORTUGUESE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->FELIPE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->BRAZILIAN_PORTUGUESE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->LUCIANA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->COLOMBIAN_SPANISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->CARLOS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->LATIN_AMERICAN_SPANISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->JUAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->LATIN_AMERICAN_SPANISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->PAULINA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->GERMAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->ANNA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->GERMAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->YANNICK:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->GERMAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v4

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->PETRA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->RUSSIAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->MILENA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->RUSSIAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->YURI:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->CZECH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->IVETA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->CZECH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->ZUZANA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->ROMANIAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->IOANA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->MANDARIN_SIMPLIFIED:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->TIAN_TIAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->EUROPEAN_SPANISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->JORGE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->EUROPEAN_SPANISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->MONICA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->EUROPEAN_FRENCH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->AUDREY:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->EUROPEAN_FRENCH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->THOMAS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->CANADIAN_FRENCH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->AMELIE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->CANADIAN_FRENCH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->NICOLAS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->AUSTRALIAN_ENGLISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->KAREN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->AUSTRALIAN_ENGLISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->LEE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->KOREAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->SORA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->SLOVAK:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->LAURA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->IRISH_ENGLISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->MOIRA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->DUTCH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->CLAIRE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->DUTCH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->XANDER:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->BRITISH_ENGLISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->DANIEL:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->BRITISH_ENGLISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->SERENA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->BELGIAN_DUTCH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->ELLEN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->ITALIAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->ALICE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->ITALIAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->FEDERICA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->ITALIAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v4

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->LUCA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->ITALIAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v5

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->PAOLA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->GAELIC:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->CARMELA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->SWEDISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->ALVA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->SWEDISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->OSKAR:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->GREEK:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->MELINA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->GREEK:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->NIKOS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->TURKISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->CEM:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->TURKISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->YELDA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->BASQUE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->MIREN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->FINNISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->SATU:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->UNITED_STATES_ENGLISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->ALLISON:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->UNITED_STATES_ENGLISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->AVA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->UNITED_STATES_ENGLISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v4

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->SAMANTHA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->UNITED_STATES_ENGLISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v5

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->TOM:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->HINDI:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->DAMAYANTI:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->NEW_ZEALAND_ENGLISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->TESSA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->EUROPEAN_PORTUGUESE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->CATARINA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->EUROPEAN_PORTUGUESE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->JOANA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->ARGENTINIAN_SPANISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->DIEGO:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->HEBREW:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->CARMIT:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->CANADIAN_ENGLISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->JORDI:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->CANADIAN_ENGLISH:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v3

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->MONTSERRAT:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocalizer/a;->THAI:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocalizer/VocalizerLanguage;->voices:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aget-object v0, v0, v2

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->KANYA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    const/16 v0, 0x44

    new-array v0, v0, [Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    sget-object v1, Lcom/nuance/dragon/toolkit/vocalizer/d;->MAGNUS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v1, v0, v2

    sget-object v1, Lcom/nuance/dragon/toolkit/vocalizer/d;->SARA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v1, v0, v3

    sget-object v1, Lcom/nuance/dragon/toolkit/vocalizer/d;->HENRIK:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v1, v0, v4

    sget-object v1, Lcom/nuance/dragon/toolkit/vocalizer/d;->NORA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v1, v0, v5

    const/4 v1, 0x4

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->KYOKO:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->OTOYA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->TARIK:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->MEI_JIA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->SIN_JI:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->MARISKA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->FIONA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->EWA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->FELIPE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->LUCIANA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->CARLOS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->JUAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->PAULINA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->ANNA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->YANNICK:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->PETRA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->MILENA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->YURI:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->IVETA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->ZUZANA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->IOANA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->TIAN_TIAN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->JORGE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->MONICA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->AUDREY:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->THOMAS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->AMELIE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->NICOLAS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->KAREN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->LEE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->SORA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->LAURA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->MOIRA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->CLAIRE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->XANDER:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->DANIEL:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->SERENA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->ELLEN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->ALICE:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->FEDERICA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->LUCA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->PAOLA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->CARMELA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->ALVA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->OSKAR:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->MELINA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->NIKOS:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->CEM:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->YELDA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->MIREN:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->SATU:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->ALLISON:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->AVA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->SAMANTHA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->TOM:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->DAMAYANTI:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->TESSA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->CATARINA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->JOANA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->DIEGO:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->CARMIT:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->JORDI:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->MONTSERRAT:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/nuance/dragon/toolkit/vocalizer/d;->KANYA:Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    aput-object v2, v0, v1

    sput-object v0, Lcom/nuance/dragon/toolkit/vocalizer/d;->ALL_VOICES:[Lcom/nuance/dragon/toolkit/vocalizer/VocalizerVoice;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
