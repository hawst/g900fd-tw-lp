.class public Lcom/nuance/dragon/toolkit/cloudservices/TransactionError$ConnectionErrorCodes;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/dragon/toolkit/cloudservices/TransactionError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConnectionErrorCodes"
.end annotation


# static fields
.field public static final COMMAND_ENDED_UNEXPECTEDLY:I = 0x4

.field public static final COMMAND_IDLE_FOR_TOO_LONG:I = 0x5

.field public static final NETWORK_UNAVAILABLE:I = 0xfff0000

.field public static final REMOTE_DISCONNECTION:I = 0x3

.field public static final TIMED_OUT_WAITING_FOR_RESULT:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
