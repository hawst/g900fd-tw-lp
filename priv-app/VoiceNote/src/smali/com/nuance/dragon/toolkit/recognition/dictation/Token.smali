.class public interface abstract Lcom/nuance/dragon/toolkit/recognition/dictation/Token;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/nuance/dragon/toolkit/recognition/dictation/EditorItem;


# virtual methods
.method public abstract getConfidenceScore()D
.end method

.method public abstract getEndTime()J
.end method

.method public abstract getStartTime()J
.end method

.method public abstract hasNoSpaceAfterDirective()Z
.end method

.method public abstract hasNoSpaceBeforeDirective()Z
.end method

.method public abstract toString()Ljava/lang/String;
.end method
