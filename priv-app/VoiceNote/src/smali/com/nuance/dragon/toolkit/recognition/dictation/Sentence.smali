.class public interface abstract Lcom/nuance/dragon/toolkit/recognition/dictation/Sentence;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getConfidenceScore()D
.end method

.method public abstract getEndTime()J
.end method

.method public abstract getStartTime()J
.end method

.method public abstract size()I
.end method

.method public abstract toString()Ljava/lang/String;
.end method

.method public abstract tokenAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/Token;
.end method
