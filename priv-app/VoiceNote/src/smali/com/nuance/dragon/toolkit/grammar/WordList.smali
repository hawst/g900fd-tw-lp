.class public abstract Lcom/nuance/dragon/toolkit/grammar/WordList;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/dragon/toolkit/grammar/WordList$WordIterator;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract fullUpdateRequired()Z
.end method

.method public abstract getAcceptedChecksum()I
.end method

.method public abstract getFullIterator()Lcom/nuance/dragon/toolkit/grammar/WordList$WordIterator;
.end method

.method public abstract getModifiedIterator()Lcom/nuance/dragon/toolkit/grammar/WordList$WordIterator;
.end method
