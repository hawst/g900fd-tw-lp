.class public interface abstract Lcom/nuance/dragon/toolkit/audio/nat/NativeAudio$Recorder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/dragon/toolkit/audio/nat/NativeAudio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Recorder"
.end annotation


# virtual methods
.method public abstract getBufferSizeMs()I
.end method

.method public abstract read()Lcom/nuance/dragon/toolkit/audio/AudioChunk;
.end method

.method public abstract release()V
.end method

.method public abstract start()Z
.end method

.method public abstract stop()Z
.end method
