.class final Lcom/nuance/dragon/toolkit/hybrid/a$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/dragon/toolkit/hybrid/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private a:Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

.field private b:Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;

.field private c:Z

.field private d:Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;

.field private e:Lcom/nuance/dragon/toolkit/elvis/ElvisError;

.field private f:Lcom/nuance/dragon/toolkit/util/internal/a$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/nuance/dragon/toolkit/util/internal/a$b",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/nuance/dragon/toolkit/hybrid/HybridRecognitionResult;",
            "Lcom/nuance/dragon/toolkit/hybrid/HybridRecognitionError;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/nuance/dragon/toolkit/hybrid/a$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/hybrid/a$a;Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;)Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;
    .locals 0

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->a:Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

    return-object p1
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/hybrid/a$a;Lcom/nuance/dragon/toolkit/elvis/ElvisError;)Lcom/nuance/dragon/toolkit/elvis/ElvisError;
    .locals 0

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->e:Lcom/nuance/dragon/toolkit/elvis/ElvisError;

    return-object p1
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/hybrid/a$a;Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;)Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;
    .locals 0

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->b:Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;

    return-object p1
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/hybrid/a$a;Lcom/nuance/dragon/toolkit/util/internal/a$b;)Lcom/nuance/dragon/toolkit/util/internal/a$b;
    .locals 0

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->f:Lcom/nuance/dragon/toolkit/util/internal/a$b;

    return-object p1
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/hybrid/a$a;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->g:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/hybrid/a$a;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->g:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/hybrid/a$a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->h:Z

    return p1
.end method

.method static synthetic b(Lcom/nuance/dragon/toolkit/hybrid/a$a;Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;)Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;
    .locals 0

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->d:Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;

    return-object p1
.end method

.method static synthetic b(Lcom/nuance/dragon/toolkit/hybrid/a$a;)Lcom/nuance/dragon/toolkit/util/internal/a$b;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->f:Lcom/nuance/dragon/toolkit/util/internal/a$b;

    return-object v0
.end method

.method static synthetic b(Lcom/nuance/dragon/toolkit/hybrid/a$a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->c:Z

    return p1
.end method

.method static synthetic c(Lcom/nuance/dragon/toolkit/hybrid/a$a;)Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->a:Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognizer;

    return-object v0
.end method

.method static synthetic d(Lcom/nuance/dragon/toolkit/hybrid/a$a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->c:Z

    return v0
.end method

.method static synthetic e(Lcom/nuance/dragon/toolkit/hybrid/a$a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->h:Z

    return v0
.end method

.method static synthetic f(Lcom/nuance/dragon/toolkit/hybrid/a$a;)Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->d:Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;

    return-object v0
.end method

.method static synthetic g(Lcom/nuance/dragon/toolkit/hybrid/a$a;)Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->b:Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;

    return-object v0
.end method

.method static synthetic h(Lcom/nuance/dragon/toolkit/hybrid/a$a;)Lcom/nuance/dragon/toolkit/elvis/ElvisError;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/hybrid/a$a;->e:Lcom/nuance/dragon/toolkit/elvis/ElvisError;

    return-object v0
.end method
