.class final Lcom/nuance/dragon/toolkit/vocon/d$6$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/dragon/toolkit/vocon/d$6;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/nuance/dragon/toolkit/vocon/d$6;


# direct methods
.method constructor <init>(Lcom/nuance/dragon/toolkit/vocon/d$6;)V
    .locals 0

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/vocon/d$6$1;->a:Lcom/nuance/dragon/toolkit/vocon/d$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$6$1;->a:Lcom/nuance/dragon/toolkit/vocon/d$6;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocon/d$6;->a:Lcom/nuance/dragon/toolkit/vocon/d$a;

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$6$1;->a:Lcom/nuance/dragon/toolkit/vocon/d$6;

    iget-object v1, v1, Lcom/nuance/dragon/toolkit/vocon/d$6;->d:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v1}, Lcom/nuance/dragon/toolkit/vocon/d;->i(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/d$a;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$6$1;->a:Lcom/nuance/dragon/toolkit/vocon/d$6;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocon/d$6;->d:Lcom/nuance/dragon/toolkit/vocon/d;

    const-string v1, "Cannot load VoCon State or Grammar.  VoCon is not initialized. "

    invoke-static {v0, v1}, Lcom/nuance/dragon/toolkit/util/Logger;->error(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$6$1;->a:Lcom/nuance/dragon/toolkit/vocon/d$6;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocon/d$6;->b:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$RebuildListener;

    new-instance v1, Lcom/nuance/dragon/toolkit/vocon/VoconError;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Lcom/nuance/dragon/toolkit/vocon/VoconError;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$RebuildListener;->onError(Lcom/nuance/dragon/toolkit/vocon/VoconError;)V

    :cond_0
    return-void
.end method
