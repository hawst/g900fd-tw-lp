.class public interface abstract Lcom/nuance/dragon/toolkit/recognition/dictation/AlterChoices;
.super Ljava/lang/Object;


# virtual methods
.method public abstract choose(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract getChoiceAt(I)Lcom/nuance/dragon/toolkit/recognition/dictation/AlterChoice;
.end method

.method public abstract size()I
.end method

.method public abstract toStringArray()[Ljava/lang/String;
.end method
