.class public interface abstract Lcom/nuance/dragon/toolkit/speechkit/SKCloudRecognizer$Listener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/dragon/toolkit/speechkit/SKCloudRecognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onError(Lcom/nuance/dragon/toolkit/speechkit/SKCloudRecognizer;Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionError;)V
.end method

.method public abstract onListeningStart(Lcom/nuance/dragon/toolkit/speechkit/SKCloudRecognizer;)V
.end method

.method public abstract onListeningStop(Lcom/nuance/dragon/toolkit/speechkit/SKCloudRecognizer;)V
.end method

.method public abstract onResult(Lcom/nuance/dragon/toolkit/speechkit/SKCloudRecognizer;Lcom/nuance/dragon/toolkit/cloudservices/recognizer/CloudRecognitionResult;)V
.end method
