.class public interface abstract Lcom/nuance/dragon/toolkit/grammar/WordList$WordIterator;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/dragon/toolkit/grammar/WordList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WordIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/nuance/dragon/toolkit/grammar/WordAction;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract acceptChanges()V
.end method

.method public abstract discardChanges()V
.end method

.method public abstract getChecksum()I
.end method

.method public abstract getCount()I
.end method
