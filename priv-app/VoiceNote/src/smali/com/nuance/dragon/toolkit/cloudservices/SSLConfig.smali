.class public Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;
.super Ljava/lang/Object;


# instance fields
.field private final a:Z

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;->a:Z

    iput-object v1, p0, Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;->a:Z

    const-string v0, "certSummary"

    invoke-static {v0, p2}, Lcom/nuance/dragon/toolkit/util/internal/d;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "certData"

    invoke-static {v0, p3}, Lcom/nuance/dragon/toolkit/util/internal/d;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    iput-object p2, p0, Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public certData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;->c:Ljava/lang/String;

    return-object v0
.end method

.method public certSummary()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;->b:Ljava/lang/String;

    return-object v0
.end method

.method public isSelfSigned()Z
    .locals 1

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;->a:Z

    return v0
.end method
