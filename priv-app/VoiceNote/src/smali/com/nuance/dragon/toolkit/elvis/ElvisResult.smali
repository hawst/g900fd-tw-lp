.class public final Lcom/nuance/dragon/toolkit/elvis/ElvisResult;
.super Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;

# interfaces
.implements Lcom/nuance/dragon/toolkit/util/JSONCompliant;


# static fields
.field public static final GATE_CONFIDENCE_UNAVAILABLE:I = -0x1

.field public static final MAX_CONFIDENCE:I = 0x64

.field public static final MIN_CONFIDENCE:I


# instance fields
.field private final a:Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;

.field private final b:I

.field private final c:I


# direct methods
.method constructor <init>(Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;I)V
    .locals 4

    const/4 v2, 0x0

    const/16 v1, 0x64

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;->toJSON()Lorg/json/JSONObject;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;-><init>(Lorg/json/JSONObject;)V

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->a:Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;

    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;->size()I

    move-result v0

    const/4 v3, 0x2

    if-ge v0, v3, :cond_2

    move v0, v1

    :cond_0
    :goto_1
    iput v0, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->b:I

    iput p2, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->c:I

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry;->getScore()I

    move-result v3

    invoke-virtual {p1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry;->getScore()I

    move-result v0

    sub-int v0, v3, v0

    if-gez v0, :cond_3

    move v0, v2

    :cond_3
    if-le v0, v1, :cond_0

    move v0, v1

    goto :goto_1
.end method

.method public static createFromJSON(Lorg/json/JSONObject;)Lcom/nuance/dragon/toolkit/elvis/ElvisResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    :try_start_0
    const-string v0, "gate_confidence"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    new-instance v1, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;

    const-string v2, "nbest"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;->createFromJSON(Lorg/json/JSONObject;)Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;-><init>(Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;I)V

    return-object v1

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;

    iget v2, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->b:I

    iget v3, p1, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->c:I

    iget v3, p1, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->a:Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;

    iget-object v3, p1, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->a:Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;

    invoke-virtual {v2, v3}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final getChoiceCount()I
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->a:Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;->size()I

    move-result v0

    return v0
.end method

.method public final getChoiceList()Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->a:Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;

    return-object v0
.end method

.method public final getConfidence()I
    .locals 1

    iget v0, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->b:I

    return v0
.end method

.method public final getGateConfidence()I
    .locals 1

    iget v0, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->c:I

    return v0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->b:I

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->a:Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;

    invoke-virtual {v1}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->c:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final toJSON()Lorg/json/JSONObject;
    .locals 3

    new-instance v0, Lcom/nuance/dragon/toolkit/util/a/b;

    invoke-direct {v0}, Lcom/nuance/dragon/toolkit/util/a/b;-><init>()V

    const-string v1, "nbest"

    iget-object v2, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->a:Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;

    invoke-virtual {v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;->toJSON()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/nuance/dragon/toolkit/util/a/b;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    const-string v1, "gate_confidence"

    iget v2, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/nuance/dragon/toolkit/util/a/b;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->a:Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
