.class public Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing$Listener;
    }
.end annotation


# instance fields
.field private final a:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

.field private b:Lcom/nuance/dragon/toolkit/cloudservices/Transaction;


# direct methods
.method public constructor <init>(Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "cloudServices"

    invoke-static {v0, p1}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;->a:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    return-void
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;)Lcom/nuance/dragon/toolkit/cloudservices/Transaction;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;->b:Lcom/nuance/dragon/toolkit/cloudservices/Transaction;

    return-object v0
.end method

.method static synthetic b(Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;)Lcom/nuance/dragon/toolkit/cloudservices/Transaction;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;->b:Lcom/nuance/dragon/toolkit/cloudservices/Transaction;

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 2

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;->b:Lcom/nuance/dragon/toolkit/cloudservices/Transaction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;->b:Lcom/nuance/dragon/toolkit/cloudservices/Transaction;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;->b:Lcom/nuance/dragon/toolkit/cloudservices/Transaction;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/cloudservices/Transaction;->cancel()V

    :cond_0
    return-void
.end method

.method public ping(Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing$Listener;)V
    .locals 5

    const-string v0, "resultListener"

    invoke-static {v0, p1}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;->cancel()V

    new-instance v0, Lcom/nuance/dragon/toolkit/cloudservices/Transaction;

    const-string v1, "PING"

    const/4 v2, 0x0

    new-instance v3, Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing$1;

    invoke-direct {v3, p0, p1}, Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing$1;-><init>(Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing$Listener;)V

    const/16 v4, 0x2710

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/nuance/dragon/toolkit/cloudservices/Transaction;-><init>(Ljava/lang/String;Lcom/nuance/dragon/toolkit/data/Data$Dictionary;Lcom/nuance/dragon/toolkit/cloudservices/Transaction$Listener;I)V

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;->b:Lcom/nuance/dragon/toolkit/cloudservices/Transaction;

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;->a:Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;->b:Lcom/nuance/dragon/toolkit/cloudservices/Transaction;

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/nuance/dragon/toolkit/cloudservices/CloudServices;->addTransaction(Lcom/nuance/dragon/toolkit/cloudservices/Transaction;I)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/ping/CloudPing;->b:Lcom/nuance/dragon/toolkit/cloudservices/Transaction;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/cloudservices/Transaction;->finish()V

    return-void
.end method
