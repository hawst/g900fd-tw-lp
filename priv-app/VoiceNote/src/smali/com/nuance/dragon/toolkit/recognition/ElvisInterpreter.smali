.class public Lcom/nuance/dragon/toolkit/recognition/ElvisInterpreter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/nuance/dragon/toolkit/recognition/RecognitionInterpreter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/nuance/dragon/toolkit/recognition/RecognitionInterpreter",
        "<",
        "Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpretedResult(Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;)Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/dragon/toolkit/recognition/InterpretException;
        }
    .end annotation

    const-string v2, "elvisResult"

    const-string v3, "instanceof ElvisResult"

    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;

    invoke-static {v2, v3, v4}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    check-cast p1, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;

    new-instance v18, Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;

    invoke-virtual/range {p1 .. p1}, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->getGateConfidence()I

    move-result v2

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;-><init>(I)V

    invoke-virtual/range {p1 .. p1}, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->getChoiceList()Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_0
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry;

    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v10}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry;->getWords()Ljava/util/List;

    move-result-object v21

    new-instance v22, Ljava/util/ArrayList;

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lcom/nuance/dragon/toolkit/recognition/InterpretException;

    const-string v3, "Elvis Nbest list can\'t be empty"

    invoke-direct {v2, v3}, Lcom/nuance/dragon/toolkit/recognition/InterpretException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v15, 0x0

    const/4 v14, 0x0

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v23

    const/4 v2, -0x1

    move/from16 v16, v2

    move-object/from16 v17, v15

    :goto_1
    move/from16 v0, v16

    move/from16 v1, v23

    if-ge v0, v1, :cond_a

    add-int/lit8 v2, v16, 0x1

    move/from16 v0, v23

    if-ge v2, v0, :cond_4

    add-int/lit8 v2, v16, 0x1

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry$Word;

    move-object v15, v2

    :goto_2
    if-eqz v15, :cond_e

    iget-object v2, v15, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry$Word;->wordSlot:Ljava/lang/String;

    if-nez v2, :cond_5

    const/4 v2, -0x1

    :goto_3
    if-ltz v2, :cond_6

    const/4 v5, 0x1

    :goto_4
    if-eqz v5, :cond_7

    iget-object v7, v15, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry$Word;->wordSlot:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v7, v8, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :goto_5
    move v12, v5

    move-object v13, v2

    :goto_6
    if-eqz v17, :cond_d

    if-eqz v15, :cond_1

    move-object/from16 v0, v17

    iget-boolean v2, v0, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry$Word;->isGenericSpeech:Z

    if-nez v2, :cond_1

    iget-boolean v2, v15, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry$Word;->isGenericSpeech:Z

    if-nez v2, :cond_1

    invoke-static {v14, v13}, Lcom/nuance/dragon/toolkit/util/internal/f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_1
    const/4 v2, 0x1

    move v11, v2

    :goto_7
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry$Word;->fullPhrase:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/nuance/dragon/toolkit/util/internal/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry$Word;->spokenForm:Ljava/lang/String;

    invoke-static {v4, v2}, Lcom/nuance/dragon/toolkit/util/internal/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v6, :cond_2

    if-eqz v11, :cond_c

    :cond_2
    new-instance v2, Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition$Word;

    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition$Word;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v8, 0x0

    move-object v9, v8

    :goto_8
    if-eqz v11, :cond_3

    new-instance v2, Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition$Phrase;

    const-string v3, " "

    move-object/from16 v0, v22

    invoke-static {v0, v3}, Lcom/nuance/dragon/toolkit/util/internal/h;->a(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    move-object/from16 v0, v17

    iget-boolean v3, v0, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry$Word;->isGenericSpeech:Z

    if-nez v3, :cond_9

    const/4 v7, 0x1

    :goto_9
    move-object v3, v14

    move-object/from16 v5, v22

    invoke-direct/range {v2 .. v7}, Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition$Phrase;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/nuance/dragon/toolkit/data/Data$Dictionary;Z)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->clear()V

    :cond_3
    move-object v2, v8

    move-object v3, v9

    :goto_a
    add-int/lit8 v4, v16, 0x1

    move/from16 v16, v4

    move v6, v12

    move-object v14, v13

    move-object/from16 v17, v15

    move-object v4, v2

    goto/16 :goto_1

    :cond_4
    const/4 v2, 0x0

    move-object v15, v2

    goto/16 :goto_2

    :cond_5
    iget-object v2, v15, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry$Word;->wordSlot:Ljava/lang/String;

    const/16 v5, 0x7e

    invoke-virtual {v2, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    goto/16 :goto_3

    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_4

    :cond_7
    iget-object v2, v15, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry$Word;->wordSlot:Ljava/lang/String;

    goto :goto_5

    :cond_8
    const/4 v2, 0x0

    move v11, v2

    goto :goto_7

    :cond_9
    const/4 v7, 0x0

    goto :goto_9

    :cond_a
    invoke-virtual {v10}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry;->getConstraint()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry;->getScore()I

    move-result v5

    const/4 v7, 0x0

    move-object/from16 v2, v18

    move-object/from16 v6, v20

    invoke-virtual/range {v2 .. v7}, Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;->addChoice(Ljava/lang/String;Ljava/lang/String;ILjava/util/List;Lcom/nuance/dragon/toolkit/data/Data$Dictionary;)V

    goto/16 :goto_0

    :cond_b
    return-object v18

    :cond_c
    move-object v8, v5

    move-object v9, v3

    goto :goto_8

    :cond_d
    move-object v2, v4

    goto :goto_a

    :cond_e
    move v12, v6

    move-object v13, v14

    goto/16 :goto_6
.end method

.method public bridge synthetic getInterpretedResult(Ljava/lang/Object;)Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/dragon/toolkit/recognition/InterpretException;
        }
    .end annotation

    check-cast p1, Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;

    invoke-virtual {p0, p1}, Lcom/nuance/dragon/toolkit/recognition/ElvisInterpreter;->getInterpretedResult(Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;)Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;

    move-result-object v0

    return-object v0
.end method

.method public getUpdateRequiredList(Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0
.end method

.method public bridge synthetic getUpdateRequiredList(Ljava/lang/Object;)Ljava/util/List;
    .locals 1

    check-cast p1, Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;

    invoke-virtual {p0, p1}, Lcom/nuance/dragon/toolkit/recognition/ElvisInterpreter;->getUpdateRequiredList(Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public processForCloud(Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/cloudservices/DataParam;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "elvisResult"

    const-string v1, "instanceof ElvisResult"

    instance-of v2, p1, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;

    invoke-static {v0, v1, v2}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    check-cast p1, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;

    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/elvis/ElvisResult;->getChoiceList()Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/elvis/ElvisNbestList$Entry;->hasGenericSpeech()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic processForCloud(Ljava/lang/Object;Ljava/util/List;)Z
    .locals 1

    check-cast p1, Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;

    invoke-virtual {p0, p1, p2}, Lcom/nuance/dragon/toolkit/recognition/ElvisInterpreter;->processForCloud(Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;Ljava/util/List;)Z

    move-result v0

    return v0
.end method
