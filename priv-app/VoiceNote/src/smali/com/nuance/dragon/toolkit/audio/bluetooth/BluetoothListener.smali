.class public interface abstract Lcom/nuance/dragon/toolkit/audio/bluetooth/BluetoothListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onAudioConnected(I)V
.end method

.method public abstract onAudioDisconnected(I)V
.end method

.method public abstract onBluetoothInitialized(I)V
.end method

.method public abstract onBluetoothReleased(I)V
.end method

.method public abstract onDeviceConnected(I)V
.end method

.method public abstract onDeviceDisconnected(I)V
.end method
