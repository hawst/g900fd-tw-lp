.class public interface abstract Lcom/nuance/dragon/toolkit/hybrid/HybridRecognizer$Listener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/dragon/toolkit/hybrid/HybridRecognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onError(Lcom/nuance/dragon/toolkit/hybrid/HybridRecognitionError;)V
.end method

.method public abstract onResult(Lcom/nuance/dragon/toolkit/hybrid/HybridRecognitionResult;)V
.end method
