.class public interface abstract Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/Bluetooth$HeadsetStateListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/Bluetooth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "HeadsetStateListener"
.end annotation


# virtual methods
.method public abstract onAudioStateChanged(I)V
.end method

.method public abstract onConnectionStateChanged(I)V
.end method

.method public abstract onServiceConnected(Z)V
.end method

.method public abstract onServiceDisconnected()V
.end method
