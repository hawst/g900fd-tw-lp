.class public interface abstract Lcom/nuance/dragon/toolkit/audio/nat/NativeAudio;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/dragon/toolkit/audio/nat/NativeAudio$Player;,
        Lcom/nuance/dragon/toolkit/audio/nat/NativeAudio$Recorder;
    }
.end annotation


# virtual methods
.method public abstract createPlayer(I)Lcom/nuance/dragon/toolkit/audio/nat/NativeAudio$Player;
.end method

.method public abstract createRecorder(I)Lcom/nuance/dragon/toolkit/audio/nat/NativeAudio$Recorder;
.end method

.method public abstract initialize()Z
.end method

.method public abstract release()V
.end method
