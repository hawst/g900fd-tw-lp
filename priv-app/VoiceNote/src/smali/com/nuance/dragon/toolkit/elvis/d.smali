.class Lcom/nuance/dragon/toolkit/elvis/d;
.super Ljava/lang/Object;


# static fields
.field public static final ALL_LANGUAGES:[Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final AUSTRALIAN_ENGLISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final BRAZILIAN_PORTUGUESE:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final BRITISH_ENGLISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final CANADIAN_FRENCH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final CANTONESE_ENGLISH_SIMPLIFIED:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final CANTONESE_ENGLISH_TRADITIONAL:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final CANTONESE_SIMPLIFIED:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final CANTONESE_TRADITIONAL:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final DANISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final DUTCH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final EUROPEAN_FRENCH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final EUROPEAN_PORTUGUESE:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final EUROPEAN_SPANISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final FINNISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final GERMAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final GREEK:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final ITALIAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final JAPANESE:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final KOREAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final LATIN_AMERICAN_SPANISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final MANDARIN_ENGLISH_SIMPLIFIED:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final MANDARIN_ENGLISH_TRADITIONAL:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final MANDARIN_TRADITIONAL:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final NORWEGIAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final POLISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final RUSSIAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final SWEDISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final TURKISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final UKRAINIAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final UNITED_STATES_ENGLISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

.field public static final US_SPANISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "zhhk"

    const-string v2, "Cantonese Traditional"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->CANTONESE_TRADITIONAL:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "frfr"

    const-string v2, "European French"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->EUROPEAN_FRENCH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "fifi"

    const-string v2, "Finnish"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->FINNISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "engb"

    const-string v2, "British English"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->BRITISH_ENGLISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "enau"

    const-string v2, "Australian English"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->AUSTRALIAN_ENGLISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "trtr"

    const-string v2, "Turkish"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->TURKISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "jajp"

    const-string v2, "Japanese"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->JAPANESE:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "esmx"

    const-string v2, "Latin American Spanish"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->LATIN_AMERICAN_SPANISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "svse"

    const-string v2, "Swedish"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->SWEDISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "ruru"

    const-string v2, "Russian"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->RUSSIAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "ptpt"

    const-string v2, "European Portuguese"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->EUROPEAN_PORTUGUESE:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "plpl"

    const-string v2, "Polish"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->POLISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "dede"

    const-string v2, "German"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->GERMAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "zehk"

    const-string v2, "Cantonese/English Traditional"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->CANTONESE_ENGLISH_TRADITIONAL:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "nlnl"

    const-string v2, "Dutch"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->DUTCH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "zhtw"

    const-string v2, "Mandarin Traditional"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->MANDARIN_TRADITIONAL:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "zetw"

    const-string v2, "Mandarin/English Traditional"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->MANDARIN_ENGLISH_TRADITIONAL:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "frca"

    const-string v2, "Canadian French"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->CANADIAN_FRENCH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "esus"

    const-string v2, "US Spanish"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->US_SPANISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "enus"

    const-string v2, "United States English"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->UNITED_STATES_ENGLISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "eses"

    const-string v2, "European Spanish"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->EUROPEAN_SPANISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "ptbr"

    const-string v2, "Brazilian Portuguese"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->BRAZILIAN_PORTUGUESE:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "ukua"

    const-string v2, "Ukrainian"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->UKRAINIAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "zhct"

    const-string v2, "Cantonese Simplified"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->CANTONESE_SIMPLIFIED:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "dadk"

    const-string v2, "Danish"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->DANISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "nono"

    const-string v2, "Norwegian"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->NORWEGIAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "itit"

    const-string v2, "Italian"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->ITALIAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "zecn"

    const-string v2, "Mandarin/English Simplified"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->MANDARIN_ENGLISH_SIMPLIFIED:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "zect"

    const-string v2, "Cantonese/English Simplified"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->CANTONESE_ENGLISH_SIMPLIFIED:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "elgr"

    const-string v2, "Greek"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->GREEK:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    new-instance v0, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const-string v1, "kokr"

    const-string v2, "Korean"

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->KOREAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const/16 v0, 0x1f

    new-array v0, v0, [Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    const/4 v1, 0x0

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->CANTONESE_TRADITIONAL:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->EUROPEAN_FRENCH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->FINNISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->BRITISH_ENGLISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->AUSTRALIAN_ENGLISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->TURKISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->JAPANESE:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->LATIN_AMERICAN_SPANISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->SWEDISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->RUSSIAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->EUROPEAN_PORTUGUESE:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->POLISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->GERMAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->CANTONESE_ENGLISH_TRADITIONAL:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->DUTCH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->MANDARIN_TRADITIONAL:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->MANDARIN_ENGLISH_TRADITIONAL:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->CANADIAN_FRENCH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->US_SPANISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->UNITED_STATES_ENGLISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->EUROPEAN_SPANISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->BRAZILIAN_PORTUGUESE:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->UKRAINIAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->CANTONESE_SIMPLIFIED:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->DANISH:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->NORWEGIAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->ITALIAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->MANDARIN_ENGLISH_SIMPLIFIED:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->CANTONESE_ENGLISH_SIMPLIFIED:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->GREEK:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/nuance/dragon/toolkit/elvis/d;->KOREAN:Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    aput-object v2, v0, v1

    sput-object v0, Lcom/nuance/dragon/toolkit/elvis/d;->ALL_LANGUAGES:[Lcom/nuance/dragon/toolkit/elvis/ElvisLanguage;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    const-string v0, "~type~_~language~_~quality~_~rate_khz~k.bin"

    const-string v1, "~type~"

    const-string v2, "lvr"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "~language~"

    invoke-virtual {v0, v1, p0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "~quality~"

    invoke-virtual {v0, v1, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "~rate_khz~"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/regex/Matcher;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static b(Ljava/util/regex/Matcher;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static c(Ljava/util/regex/Matcher;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
