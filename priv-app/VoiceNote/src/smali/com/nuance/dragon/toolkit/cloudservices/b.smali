.class final Lcom/nuance/dragon/toolkit/cloudservices/b;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:[B

.field private final c:Ljava/lang/String;

.field private final d:S

.field private final e:Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;

.field private final f:Landroid/content/Context;

.field private final g:Ljava/lang/String;

.field private final h:Lcom/nuance/dragon/toolkit/audio/AudioType;

.field private final i:Lcom/nuance/dragon/toolkit/audio/AudioType;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/nuance/dragon/toolkit/cloudservices/SSLConfig;Ljava/lang/String;[BLjava/lang/String;Lcom/nuance/dragon/toolkit/audio/AudioType;Lcom/nuance/dragon/toolkit/audio/AudioType;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->f:Landroid/content/Context;

    iput-object p2, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->c:Ljava/lang/String;

    int-to-short v0, p3

    iput-short v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->d:S

    iput-object p4, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->e:Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;

    iput-object p5, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->a:Ljava/lang/String;

    iput-object p6, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->b:[B

    if-eqz p7, :cond_0

    invoke-virtual {p7}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    :goto_0
    iput-object p7, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->g:Ljava/lang/String;

    iput-object p8, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->h:Lcom/nuance/dragon/toolkit/audio/AudioType;

    iput-object p9, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->i:Lcom/nuance/dragon/toolkit/audio/AudioType;

    return-void

    :cond_0
    new-instance v0, Lcom/nuance/a/a/a/c/c;

    invoke-direct {v0, p1}, Lcom/nuance/a/a/a/c/c;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/nuance/a/a/a/c/c;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    move-object p7, v0

    goto :goto_0
.end method

.method private static a(Lcom/nuance/dragon/toolkit/audio/AudioType;)Lcom/nuance/a/a/a/a/a/a$a;
    .locals 4

    const/16 v3, 0x3e80

    const/16 v2, 0x1f40

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/AudioType;->encoding:Lcom/nuance/dragon/toolkit/audio/AudioType$Encoding;

    sget-object v1, Lcom/nuance/dragon/toolkit/audio/AudioType$Encoding;->PCM_16:Lcom/nuance/dragon/toolkit/audio/AudioType$Encoding;

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/nuance/dragon/toolkit/audio/AudioType;->frequency:I

    if-ne v0, v2, :cond_0

    sget-object v0, Lcom/nuance/a/a/a/a/a/a$a;->B:Lcom/nuance/a/a/a/a/a/a$a;

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/nuance/dragon/toolkit/audio/AudioType;->frequency:I

    if-ne v0, v3, :cond_3

    sget-object v0, Lcom/nuance/a/a/a/a/a/a$a;->D:Lcom/nuance/a/a/a/a/a/a$a;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/AudioType;->encoding:Lcom/nuance/dragon/toolkit/audio/AudioType$Encoding;

    sget-object v1, Lcom/nuance/dragon/toolkit/audio/AudioType$Encoding;->SPEEX:Lcom/nuance/dragon/toolkit/audio/AudioType$Encoding;

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/nuance/dragon/toolkit/audio/AudioType;->frequency:I

    if-ne v0, v2, :cond_2

    sget-object v0, Lcom/nuance/a/a/a/a/a/a$a;->y:Lcom/nuance/a/a/a/a/a/a$a;

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/nuance/dragon/toolkit/audio/AudioType;->frequency:I

    if-ne v0, v3, :cond_3

    sget-object v0, Lcom/nuance/a/a/a/a/a/a$a;->z:Lcom/nuance/a/a/a/a/a/a$a;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()[B
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->b:[B

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()S
    .locals 1

    iget-short v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->d:S

    return v0
.end method

.method public final e()Z
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->e:Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->e:Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->f:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const-string v0, "carrier"

    :cond_1
    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->f:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "unknown"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public final j()Lcom/nuance/a/a/a/a/a/a$a;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->h:Lcom/nuance/dragon/toolkit/audio/AudioType;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/cloudservices/b;->a(Lcom/nuance/dragon/toolkit/audio/AudioType;)Lcom/nuance/a/a/a/a/a/a$a;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lcom/nuance/a/a/a/a/a/a$a;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->i:Lcom/nuance/dragon/toolkit/audio/AudioType;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/cloudservices/b;->a(Lcom/nuance/dragon/toolkit/audio/AudioType;)Lcom/nuance/a/a/a/a/a/a$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/nuance/dragon/toolkit/audio/AudioType;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/b;->i:Lcom/nuance/dragon/toolkit/audio/AudioType;

    return-object v0
.end method
