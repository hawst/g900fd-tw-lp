.class public abstract Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;
.super Lcom/nuance/dragon/toolkit/audio/AudioPipe;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<InputType:",
        "Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;",
        "OutputType:",
        "Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;",
        ">",
        "Lcom/nuance/dragon/toolkit/audio/AudioPipe",
        "<TInputType;TOutputType;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/nuance/dragon/toolkit/audio/AudioSink;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TOutputType;>;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/nuance/dragon/toolkit/audio/AudioPipe;-><init>()V

    return-void
.end method


# virtual methods
.method protected final audioSinkConnected(Lcom/nuance/dragon/toolkit/audio/AudioSink;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TOutputType;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    invoke-virtual {p0, p1}, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->onSinkConnected(Lcom/nuance/dragon/toolkit/audio/AudioSink;)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->c:Z

    invoke-virtual {p1, p0}, Lcom/nuance/dragon/toolkit/audio/AudioSink;->framesDropped(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    :cond_2
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1, p0}, Lcom/nuance/dragon/toolkit/audio/AudioSink;->chunksAvailable(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    :cond_3
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1, p0}, Lcom/nuance/dragon/toolkit/audio/AudioSink;->sourceClosed(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    goto :goto_0
.end method

.method protected final audioSinkDisconnected(Lcom/nuance/dragon/toolkit/audio/AudioSink;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TOutputType;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    invoke-virtual {p0, p1}, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->onSinkDisconnected(Lcom/nuance/dragon/toolkit/audio/AudioSink;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Wrong sink disconnected"

    invoke-static {p0, v0}, Lcom/nuance/dragon/toolkit/util/Logger;->warn(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<TInputType;>;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/audio/AudioSource;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->b:Z

    :cond_0
    invoke-super {p0, p1}, Lcom/nuance/dragon/toolkit/audio/AudioPipe;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    return-void
.end method

.method protected getAllAudioChunks(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TOutputType;>;)V"
        }
    .end annotation

    :goto_0
    invoke-virtual {p0}, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->getAudioChunk()Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final getAllAudioChunksForSink(Lcom/nuance/dragon/toolkit/audio/AudioSink;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TOutputType;>;",
            "Ljava/util/List",
            "<TOutputType;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->getAllAudioChunks(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method protected abstract getAudioChunk()Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TOutputType;"
        }
    .end annotation
.end method

.method public final getAudioChunkForSink(Lcom/nuance/dragon/toolkit/audio/AudioSink;)Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TOutputType;>;)TOutputType;"
        }
    .end annotation

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->getAudioChunk()Lcom/nuance/dragon/toolkit/audio/AbstractAudioChunk;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getChunksAvailableForSink(Lcom/nuance/dragon/toolkit/audio/AudioSink;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TOutputType;>;)I"
        }
    .end annotation

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->getChunksAvailable()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmptyForSink(Lcom/nuance/dragon/toolkit/audio/AudioSink;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TOutputType;>;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->isEmpty()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected notifyChunksAvailable()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    invoke-virtual {v0, p0}, Lcom/nuance/dragon/toolkit/audio/AudioSink;->chunksAvailable(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    :cond_0
    return-void
.end method

.method protected notifyFramesDropped()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    invoke-virtual {v0, p0}, Lcom/nuance/dragon/toolkit/audio/AudioSink;->framesDropped(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->c:Z

    goto :goto_0
.end method

.method protected notifySourceClosed()V
    .locals 1

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->a:Lcom/nuance/dragon/toolkit/audio/AudioSink;

    invoke-virtual {v0, p0}, Lcom/nuance/dragon/toolkit/audio/AudioSink;->sourceClosed(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/pipes/SingleSinkPipe;->b:Z

    goto :goto_0
.end method

.method protected onSinkConnected(Lcom/nuance/dragon/toolkit/audio/AudioSink;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TOutputType;>;)V"
        }
    .end annotation

    return-void
.end method

.method protected onSinkDisconnected(Lcom/nuance/dragon/toolkit/audio/AudioSink;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSink",
            "<TOutputType;>;)V"
        }
    .end annotation

    return-void
.end method
