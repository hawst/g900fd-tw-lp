.class interface abstract Lcom/nuance/dragon/toolkit/vocon/NativeVocon;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/dragon/toolkit/vocon/NativeVocon$RecognitionListener;,
        Lcom/nuance/dragon/toolkit/vocon/NativeVocon$WakeUpMode;
    }
.end annotation


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a(Ljava/lang/String;)Z
.end method

.method public abstract a(Ljava/lang/String;I)Z
.end method

.method public abstract a(Ljava/lang/String;ILjava/lang/String;)Z
.end method

.method public abstract a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract a(Ljava/lang/String;Lcom/nuance/dragon/toolkit/vocon/Grammar;)Z
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract a(Ljava/lang/String;Ljava/util/List;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;Ljava/util/Map;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Lcom/nuance/dragon/toolkit/vocon/VoconParam;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/List;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/vocon/c;",
            ">;)Z"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/List;IIZLcom/nuance/dragon/toolkit/vocon/NativeVocon$RecognitionListener;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IIZ",
            "Lcom/nuance/dragon/toolkit/vocon/NativeVocon$RecognitionListener;",
            ")Z"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/Map;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/nuance/dragon/toolkit/vocon/VoconParam;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation
.end method

.method public abstract b()I
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract b(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract b(Ljava/util/List;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/vocon/b;",
            ">;)Z"
        }
    .end annotation
.end method

.method public abstract c()V
.end method

.method public abstract c(Ljava/lang/String;)Z
.end method

.method public abstract c(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract d(Ljava/lang/String;)Z
.end method

.method public abstract d(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract e(Ljava/lang/String;)V
.end method

.method public abstract e(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract f(Ljava/lang/String;)Z
.end method

.method public abstract g(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract h(Ljava/lang/String;)Lcom/nuance/dragon/toolkit/vocon/Grammar;
.end method

.method public abstract i(Ljava/lang/String;)Z
.end method

.method public abstract j(Ljava/lang/String;)V
.end method
