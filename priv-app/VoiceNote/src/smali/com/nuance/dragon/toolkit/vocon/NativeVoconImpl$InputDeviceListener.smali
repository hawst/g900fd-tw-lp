.class public interface abstract Lcom/nuance/dragon/toolkit/vocon/NativeVoconImpl$InputDeviceListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/dragon/toolkit/vocon/NativeVoconImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InputDeviceListener"
.end annotation


# virtual methods
.method public abstract onAbnormalAudio(I)V
.end method

.method public abstract onCanceled()V
.end method

.method public abstract onEndOfSpeech()V
.end method

.method public abstract onGetAudio([S)I
.end method

.method public abstract onHasAudio()Z
.end method

.method public abstract onProcessedAudio(I)V
.end method

.method public abstract onRecognitionResult(Ljava/lang/String;)V
.end method

.method public abstract onStartAudio()V
.end method

.method public abstract onStartOfSpeech()V
.end method

.method public abstract onStopAudio()V
.end method
