.class public interface abstract Lcom/nuance/dragon/toolkit/elvis/ElvisRecognizer$RebuildListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/dragon/toolkit/elvis/ElvisRecognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RebuildListener"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/dragon/toolkit/elvis/ElvisRecognizer$RebuildListener$SkippedWord;
    }
.end annotation


# virtual methods
.method public abstract onComplete(Lcom/nuance/dragon/toolkit/elvis/Grammar;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/elvis/Grammar;",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/elvis/ElvisRecognizer$RebuildListener$SkippedWord;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onError(Lcom/nuance/dragon/toolkit/elvis/ElvisError;)V
.end method
