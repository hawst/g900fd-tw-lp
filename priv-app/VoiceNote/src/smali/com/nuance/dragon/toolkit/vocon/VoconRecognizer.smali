.class public abstract Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer;
.super Lcom/nuance/dragon/toolkit/language/LanguageEvent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$Languages;,
        Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResourceReleaseListener;,
        Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$SaveListener;,
        Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResourceLoadListener;,
        Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ReleaseListener;,
        Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$InitializeListener;,
        Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$RebuildListener;,
        Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$WakeupCheckListener;,
        Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$SignalListener;,
        Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;
    }
.end annotation


# static fields
.field public static LOAD_NATIVE_LIBRARY_SUCCESS:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/nuance/dragon/toolkit/vocon/NativeVoconImpl;->a:Z

    sput-boolean v0, Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer;->LOAD_NATIVE_LIBRARY_SUCCESS:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/nuance/dragon/toolkit/language/LanguageEvent;-><init>()V

    return-void
.end method

.method public static createVoconRecognizer(Lcom/nuance/dragon/toolkit/file/FileManager;)Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer;
    .locals 1

    new-instance v0, Lcom/nuance/dragon/toolkit/vocon/e;

    invoke-direct {v0, p0}, Lcom/nuance/dragon/toolkit/vocon/e;-><init>(Lcom/nuance/dragon/toolkit/file/FileManager;)V

    return-object v0
.end method

.method public static createVoconRecognizer(Lcom/nuance/dragon/toolkit/file/FileManager;Landroid/os/Handler;)Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer;
    .locals 1

    new-instance v0, Lcom/nuance/dragon/toolkit/vocon/e;

    invoke-direct {v0, p0, p1}, Lcom/nuance/dragon/toolkit/vocon/e;-><init>(Lcom/nuance/dragon/toolkit/file/FileManager;Landroid/os/Handler;)V

    return-object v0
.end method


# virtual methods
.method public abstract cancelRebuild()V
.end method

.method public abstract cancelRecognition()V
.end method

.method public abstract checkWakeupPhrases(Ljava/util/List;Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$WakeupCheckListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$WakeupCheckListener;",
            ")V"
        }
    .end annotation
.end method

.method public abstract clearGrammar(Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$RebuildListener;)V
.end method

.method public abstract enableVerboseAndroidLogging(Z)V
.end method

.method public abstract getAvailableModels()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/vocon/VoconModelInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAvailableModels(Lcom/nuance/dragon/toolkit/vocon/VoconLanguage;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/vocon/VoconLanguage;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/vocon/VoconModelInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract initialize(Lcom/nuance/dragon/toolkit/vocon/VoconConfig;Ljava/lang/String;)V
.end method

.method public abstract initialize(Lcom/nuance/dragon/toolkit/vocon/VoconConfig;Ljava/lang/String;Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$InitializeListener;)V
.end method

.method public abstract loadGrammar(Lcom/nuance/dragon/toolkit/vocon/Grammar;Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$RebuildListener;)V
.end method

.method public abstract loadResource(Ljava/lang/String;Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResourceLoadListener;)V
.end method

.method public abstract loadState(Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$RebuildListener;)V
.end method

.method public abstract loadStateAndGrammar(Lcom/nuance/dragon/toolkit/vocon/Grammar;Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$RebuildListener;)V
.end method

.method public abstract release()V
.end method

.method public abstract release(Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ReleaseListener;)V
.end method

.method public abstract releaseResource(Ljava/lang/String;Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResourceReleaseListener;)V
.end method

.method public abstract saveState()V
.end method

.method public abstract saveState(Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$SaveListener;)V
.end method

.method public abstract startFuzzyMatchRecognition(Lcom/nuance/dragon/toolkit/audio/AudioSource;Lcom/nuance/dragon/toolkit/vocon/VoconContext;Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$SignalListener;Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;",
            "Lcom/nuance/dragon/toolkit/vocon/VoconContext;",
            "Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;",
            "Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$SignalListener;",
            "Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;",
            ")V"
        }
    .end annotation
.end method

.method public abstract startRecognition(Lcom/nuance/dragon/toolkit/audio/AudioSource;Ljava/util/Map;Ljava/util/List;Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$SignalListener;Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/nuance/dragon/toolkit/vocon/VoconParam;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/vocon/VoconContext;",
            ">;",
            "Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;",
            "Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$SignalListener;",
            "Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;",
            ")V"
        }
    .end annotation
.end method

.method public abstract startSpeechDetection(Lcom/nuance/dragon/toolkit/audio/AudioSource;Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;",
            "Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;",
            ")V"
        }
    .end annotation
.end method

.method public abstract startSpeechDetection(Lcom/nuance/dragon/toolkit/audio/AudioSource;ZLcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;Z",
            "Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;",
            ")V"
        }
    .end annotation
.end method

.method public abstract startTimedRecognitionMode(Lcom/nuance/dragon/toolkit/audio/AudioSource;Lcom/nuance/dragon/toolkit/vocon/VoconContext;Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$SignalListener;Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;",
            "Lcom/nuance/dragon/toolkit/vocon/VoconContext;",
            "Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;",
            "Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$SignalListener;",
            "Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;",
            ")V"
        }
    .end annotation
.end method

.method public abstract startWakeupMode(Lcom/nuance/dragon/toolkit/audio/AudioSource;Ljava/util/List;ILcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;",
            ")V"
        }
    .end annotation
.end method

.method public abstract stopListening()V
.end method
