.class final Lcom/nuance/dragon/toolkit/vocon/d$b$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/audio/AudioSource;Ljava/util/Map;Ljava/util/List;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:I

.field final synthetic d:I

.field final synthetic e:Lcom/nuance/dragon/toolkit/audio/AudioSource;

.field final synthetic f:Ljava/util/Map;

.field final synthetic g:Ljava/util/List;

.field final synthetic h:Ljava/util/Map;

.field final synthetic i:Lcom/nuance/dragon/toolkit/vocon/d$b;


# direct methods
.method constructor <init>(Lcom/nuance/dragon/toolkit/vocon/d$b;Ljava/util/List;Ljava/util/List;IILcom/nuance/dragon/toolkit/audio/AudioSource;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->i:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iput-object p2, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->b:Ljava/util/List;

    iput p4, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->c:I

    iput p5, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->d:I

    iput-object p6, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->e:Lcom/nuance/dragon/toolkit/audio/AudioSource;

    iput-object p7, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->f:Ljava/util/Map;

    iput-object p8, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->g:Ljava/util/List;

    iput-object p9, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->h:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(Lcom/nuance/dragon/toolkit/vocon/VoconError;)V
    .locals 7

    const/4 v2, 0x2

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->i:Lcom/nuance/dragon/toolkit/vocon/d$b;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d$b;->i(Lcom/nuance/dragon/toolkit/vocon/d$b;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->i:Lcom/nuance/dragon/toolkit/vocon/d$b;

    new-instance v1, Lcom/nuance/dragon/toolkit/vocon/VoconError;

    invoke-direct {v1, v2}, Lcom/nuance/dragon/toolkit/vocon/VoconError;-><init>(I)V

    invoke-static {v0, v1}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/vocon/d$b;Lcom/nuance/dragon/toolkit/vocon/VoconError;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->i:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->c(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/d$b;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/nuance/dragon/toolkit/vocon/VoconError;->getReasonCode()I

    move-result v0

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->c:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->i:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->e:Lcom/nuance/dragon/toolkit/audio/AudioSource;

    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->f:Ljava/util/Map;

    iget-object v3, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->g:Ljava/util/List;

    iget-object v4, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->h:Ljava/util/Map;

    iget-object v5, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->i:Lcom/nuance/dragon/toolkit/vocon/d$b;

    invoke-static {v5}, Lcom/nuance/dragon/toolkit/vocon/d$b;->j(Lcom/nuance/dragon/toolkit/vocon/d$b;)Z

    move-result v5

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/vocon/d$b;Lcom/nuance/dragon/toolkit/audio/AudioSource;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;ZLcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->i:Lcom/nuance/dragon/toolkit/vocon/d$b;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/vocon/d$b;->disconnectAudioSource()Lcom/nuance/dragon/toolkit/audio/AudioSource;

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->i:Lcom/nuance/dragon/toolkit/vocon/d$b;

    invoke-static {v0, p1}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/vocon/d$b;Lcom/nuance/dragon/toolkit/vocon/VoconError;)V

    goto :goto_0
.end method

.method public final onResult(Lcom/nuance/dragon/toolkit/vocon/VoconResult;)V
    .locals 7

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->i:Lcom/nuance/dragon/toolkit/vocon/d$b;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d$b;->i(Lcom/nuance/dragon/toolkit/vocon/d$b;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->i:Lcom/nuance/dragon/toolkit/vocon/d$b;

    new-instance v1, Lcom/nuance/dragon/toolkit/vocon/VoconError;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/nuance/dragon/toolkit/vocon/VoconError;-><init>(I)V

    invoke-static {v0, v1}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/vocon/d$b;Lcom/nuance/dragon/toolkit/vocon/VoconError;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->c:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;

    invoke-virtual {v1}, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->getConfidence()I

    move-result v3

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->getConfidence()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Current confidence: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ,last best confidence: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    if-lt v4, v3, :cond_4

    :goto_2
    move-object v1, v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {v1}, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->getConfidence()I

    move-result v0

    iget v2, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->d:I

    add-int/2addr v0, v2

    const/16 v2, 0x1388

    if-gt v0, v2, :cond_3

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->i:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->e:Lcom/nuance/dragon/toolkit/audio/AudioSource;

    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->f:Ljava/util/Map;

    iget-object v3, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->g:Ljava/util/List;

    iget-object v4, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->h:Ljava/util/Map;

    iget-object v5, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->i:Lcom/nuance/dragon/toolkit/vocon/d$b;

    invoke-static {v5}, Lcom/nuance/dragon/toolkit/vocon/d$b;->j(Lcom/nuance/dragon/toolkit/vocon/d$b;)Z

    move-result v5

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/vocon/d$b;Lcom/nuance/dragon/toolkit/audio/AudioSource;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;ZLcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->i:Lcom/nuance/dragon/toolkit/vocon/d$b;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/vocon/d$b;->disconnectAudioSource()Lcom/nuance/dragon/toolkit/audio/AudioSource;

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->g:Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;->i:Lcom/nuance/dragon/toolkit/vocon/d$b;

    invoke-static {v0, v1}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/vocon/d$b;Lcom/nuance/dragon/toolkit/vocon/VoconResult;)V

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method
