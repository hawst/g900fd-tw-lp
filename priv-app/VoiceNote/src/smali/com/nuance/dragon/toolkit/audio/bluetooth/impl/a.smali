.class final Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;
.super Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/Bluetooth;


# instance fields
.field private a:Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset;

.field private b:Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/Bluetooth$HeadsetStateListener;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Landroid/media/AudioManager;

.field private final h:Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset$ServiceListener;

.field private final i:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/Bluetooth;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a$1;

    invoke-direct {v0, p0}, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a$1;-><init>(Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;)V

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->h:Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset$ServiceListener;

    new-instance v0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a$2;

    invoke-direct {v0, p0}, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a$2;-><init>(Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;)V

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->i:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;)Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->a:Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset;

    return-object v0
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->c:Z

    return p1
.end method

.method static synthetic b(Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;)Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/Bluetooth$HeadsetStateListener;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->b:Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/Bluetooth$HeadsetStateListener;

    return-object v0
.end method

.method static synthetic b(Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->e:Z

    return p1
.end method

.method static synthetic c(Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->e:Z

    return v0
.end method

.method static synthetic c(Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->f:Z

    return p1
.end method

.method static synthetic d(Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->f:Z

    return v0
.end method


# virtual methods
.method public final close()V
    .locals 2

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->d:Z

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->a:Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->a:Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->a:Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset;

    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final getAudioState()I
    .locals 1

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getBondedDevices()[Ljava/lang/String;
    .locals 6

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v2, v1, 0x1

    if-eqz v3, :cond_1

    move-object v0, v3

    :goto_2
    aput-object v0, v4, v1

    move v1, v2

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    move-object v0, v4

    goto :goto_0
.end method

.method public final getHeadsetState()I
    .locals 1

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getPlaybackStream()I
    .locals 1

    sget v0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/Audio;->VOICE_CALL_STREAM:I

    return v0
.end method

.method public final getRecordingSource()I
    .locals 1

    sget v0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/Audio;->DEFAULT_AUDIO_SOURCE:I

    return v0
.end method

.method public final isEnabled()Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    :cond_0
    return v0
.end method

.method public final setHeadsetStateListener(Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/Bluetooth$HeadsetStateListener;)V
    .locals 3

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->b:Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/Bluetooth$HeadsetStateListener;

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->a:Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset;

    if-nez v0, :cond_0

    new-instance v0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset;

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->h:Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset$ServiceListener;

    invoke-direct {v0, v1, v2}, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset;-><init>(Landroid/content/Context;Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset$ServiceListener;)V

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->a:Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset;

    new-instance v0, Landroid/content/IntentFilter;

    sget-object v1, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/BluetoothHeadset;->ACTION_STATE_CHANGED:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "android.media.SCO_AUDIO_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->d:Z

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->g:Landroid/media/AudioManager;

    :cond_0
    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->b:Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/Bluetooth$HeadsetStateListener;

    invoke-virtual {p0}, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->getHeadsetState()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/Bluetooth$HeadsetStateListener;->onConnectionStateChanged(I)V

    :cond_1
    return-void
.end method

.method public final startBluetoothSco()Z
    .locals 2

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->startBluetoothSco()V

    const-string v0, "called void mAudioManager.startBluetoothSco"

    const-string v1, "DMT-Bluetooth"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public final stopBluetoothSco()V
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/bluetooth/impl/a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    return-void
.end method
