.class public final Lcom/nuance/dragon/toolkit/vocon/VoconResult;
.super Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;

# interfaces
.implements Lcom/nuance/dragon/toolkit/util/JSONCompliant;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;
    }
.end annotation


# instance fields
.field private final a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

.field private final b:Z

.field private final c:Z

.field private final d:Lorg/json/JSONArray;

.field private final e:Lorg/json/JSONArray;

.field private final f:I

.field private final g:I

.field private h:Ljava/lang/String;

.field private i:Z


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const/4 v3, -0x1

    const/4 v8, 0x0

    const/4 v1, 0x0

    new-instance v0, Lcom/nuance/dragon/toolkit/util/a/b;

    invoke-direct {v0, p1}, Lcom/nuance/dragon/toolkit/util/a/b;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/nuance/dragon/toolkit/recognition/LocalRecognitionResult;-><init>(Lorg/json/JSONObject;)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->_rawResult:Lorg/json/JSONObject;

    const-string v2, "_resultType"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "NBest"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v0, Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;->NBEST:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->_rawResult:Lorg/json/JSONObject;

    const-string v2, "_isSpeech"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "yes"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->b:Z

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->_rawResult:Lorg/json/JSONObject;

    const-string v2, "_isInGrammar"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "no"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->c:Z

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->_rawResult:Lorg/json/JSONObject;

    const-string v2, "_hypotheses"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    iput-object v8, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->e:Lorg/json/JSONArray;

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "_conf"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->f:I

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "_items"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    move v0, v1

    move v2, v1

    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_3

    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v5, "_type"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "tag"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "_items"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    move v3, v2

    move v2, v1

    :goto_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_1

    invoke-virtual {v5, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "_userID.lo32"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v3, "_userID.lo32"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    if-gtz v3, :cond_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v2, v3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iput v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->g:I

    :goto_2
    iput-object v8, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->h:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->i:Z

    return-void

    :cond_4
    iput v3, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->f:I

    iput v1, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->g:I

    goto :goto_2

    :cond_5
    const-string v2, "Table"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;->TABLE:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->b:Z

    iput-boolean v1, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->c:Z

    iput-object v8, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->_rawResult:Lorg/json/JSONObject;

    const-string v2, "_interpretations"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->e:Lorg/json/JSONArray;

    iput v3, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->f:I

    iput v1, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->g:I

    goto :goto_2

    :cond_6
    const-string v0, "Vocon result type is invalid."

    invoke-static {p0, v0}, Lcom/nuance/dragon/toolkit/util/Logger;->error(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Vocon result type is invalid."

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method final a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "_startRule"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    const-string v0, "#"

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v4, v5, :cond_0

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->h:Ljava/lang/String;

    const-string v0, "seamless"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->i:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error setting recognized wake-up phrase"

    invoke-static {p0, v1, v0}, Lcom/nuance/dragon/toolkit/util/Logger;->error(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/nuance/dragon/toolkit/vocon/VoconResult;

    iget v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->f:I

    iget v3, p1, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->f:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->e:Lorg/json/JSONArray;

    if-nez v2, :cond_5

    iget-object v2, p1, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->e:Lorg/json/JSONArray;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->e:Lorg/json/JSONArray;

    iget-object v3, p1, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->e:Lorg/json/JSONArray;

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-boolean v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->c:Z

    iget-boolean v3, p1, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->c:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-boolean v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->i:Z

    iget-boolean v3, p1, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->i:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-boolean v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->b:Z

    iget-boolean v3, p1, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->b:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    if-nez v2, :cond_a

    iget-object v2, p1, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    if-eqz v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    iget-object v3, p1, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    iget-object v3, p1, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    if-eq v2, v3, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->g:I

    iget v3, p1, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->g:I

    if-eq v2, v3, :cond_d

    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->h:Ljava/lang/String;

    if-nez v2, :cond_e

    iget-object v2, p1, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_e
    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final getChoiceCount()I
    .locals 3

    const-string v1, "ResultType"

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    sget-object v2, Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;->NBEST:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/Object;Z)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getChoiceList()Lorg/json/JSONArray;
    .locals 3

    const-string v1, "ResultType"

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    sget-object v2, Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;->NBEST:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/Object;Z)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getConfidence()I
    .locals 3

    const-string v1, "ResultType"

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    sget-object v2, Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;->NBEST:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/Object;Z)V

    iget v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->f:I

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getInterpretationCount()I
    .locals 3

    const-string v1, "ResultType"

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    sget-object v2, Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;->TABLE:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/Object;Z)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->e:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getInterpretationList()Lorg/json/JSONArray;
    .locals 3

    const-string v1, "ResultType"

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    sget-object v2, Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;->TABLE:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/Object;Z)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->e:Lorg/json/JSONArray;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getRecognizedWakeupPhrase()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final getResultType()Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    return-object v0
.end method

.method public final getUserID()I
    .locals 3

    const-string v1, "ResultType"

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    sget-object v2, Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;->NBEST:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/Object;Z)V

    iget v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->g:I

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasGenericSpeech(I)Z
    .locals 11

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "ResultType"

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    sget-object v4, Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;->NBEST:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/Object;Z)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge p1, v0, :cond_5

    :try_start_0
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    invoke-virtual {v0, p1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v3, "_items"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    move v4, v2

    move v0, v2

    :goto_1
    :try_start_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v4, v3, :cond_4

    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v6, "_type"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "terminal"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "_orthography"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "<...>"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "_conf"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_2

    :goto_2
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    const-string v6, "_items"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    move v3, v2

    :goto_3
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v3, v7, :cond_2

    invoke-virtual {v6, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    const-string v8, "_type"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "terminal"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "_orthography"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "<...>"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "_conf"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v7

    if-lez v7, :cond_3

    move v0, v1

    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    move v1, v0

    goto :goto_2

    :catch_0
    move-exception v0

    move v1, v2

    :goto_4
    const-string v2, "Unable to detect generic speech data"

    invoke-static {p0, v2, v0}, Lcom/nuance/dragon/toolkit/util/Logger;->error(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :catch_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method public final hashCode()I
    .locals 5

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    iget v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->f:I

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->e:Lorg/json/JSONArray;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->c:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->i:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->b:Z

    if-eqz v4, :cond_3

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->g:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->h:Ljava/lang/String;

    if-nez v2, :cond_6

    :goto_6
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->e:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_2

    :cond_3
    move v2, v3

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_6
    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6
.end method

.method public final isOutOfGrammar()Z
    .locals 3

    const-string v1, "ResultType"

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    sget-object v2, Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;->NBEST:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/Object;Z)V

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->c:Z

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSeamlessWakeup()Z
    .locals 1

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->i:Z

    return v0
.end method

.method public final isSpeech()Z
    .locals 3

    const-string v1, "ResultType"

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    sget-object v2, Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;->NBEST:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/Object;Z)V

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->b:Z

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toJSON()Lorg/json/JSONObject;
    .locals 3

    new-instance v0, Lcom/nuance/dragon/toolkit/util/a/b;

    invoke-direct {v0}, Lcom/nuance/dragon/toolkit/util/a/b;-><init>()V

    const-string v1, "rawres"

    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->_rawResult:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2}, Lcom/nuance/dragon/toolkit/util/a/b;->a(Ljava/lang/String;Ljava/lang/Object;)Z

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VoconResult [_resultType="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->a:Lcom/nuance/dragon/toolkit/vocon/VoconResult$ResultType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", _isSpeech="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", _isOutOfGrammar="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", _nbest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->d:Lorg/json/JSONArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", _interpretations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->e:Lorg/json/JSONArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", _confidence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", _userID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", _wakeupPhrase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", _isSeamlessWakeup="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;->i:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
