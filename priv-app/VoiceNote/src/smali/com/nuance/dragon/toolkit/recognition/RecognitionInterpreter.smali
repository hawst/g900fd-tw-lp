.class public interface abstract Lcom/nuance/dragon/toolkit/recognition/RecognitionInterpreter;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RecogType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract getInterpretedResult(Ljava/lang/Object;)Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRecogType;)",
            "Lcom/nuance/dragon/toolkit/recognition/InterpretedRecognition;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/dragon/toolkit/recognition/InterpretException;
        }
    .end annotation
.end method

.method public abstract getUpdateRequiredList(Ljava/lang/Object;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRecogType;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract processForCloud(Ljava/lang/Object;Ljava/util/List;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRecogType;",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/cloudservices/DataParam;",
            ">;)Z"
        }
    .end annotation
.end method
