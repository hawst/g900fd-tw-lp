.class final Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;->startRecordingInternal(Lcom/nuance/dragon/toolkit/audio/AudioType;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;

.field private b:Z


# direct methods
.method constructor <init>(Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;)V
    .locals 1

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource$1;->a:Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource$1;->b:Z

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource$1;->a:Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;->a(Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource$1;->a:Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;->b(Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;)Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-boolean v1, p0, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource$1;->b:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource$1;->b:Z

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource$1;->a:Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;

    invoke-virtual {v1}, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;->handleStarted()V

    :cond_2
    iget-object v1, p0, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource$1;->a:Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;

    invoke-virtual {v1, v0}, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;->handleNewAudio(Lcom/nuance/dragon/toolkit/audio/AudioChunk;)V

    :cond_3
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource$1;->a:Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;->a(Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource$1;->a:Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;->d(Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource$1;->a:Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;

    invoke-static {v1}, Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;->c(Lcom/nuance/dragon/toolkit/audio/sources/MicrophoneRecorderSource;)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
