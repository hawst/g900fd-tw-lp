.class final Lcom/nuance/dragon/toolkit/vocon/d$b;
.super Lcom/nuance/dragon/toolkit/audio/AudioSink;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/dragon/toolkit/vocon/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/nuance/dragon/toolkit/audio/AudioSink",
        "<",
        "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/nuance/dragon/toolkit/vocon/d;

.field private b:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;

.field private c:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

.field private d:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

.field private final e:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$SignalListener;

.field private final f:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

.field private h:I

.field private i:Z

.field private j:I


# direct methods
.method constructor <init>(Lcom/nuance/dragon/toolkit/vocon/d;Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$SignalListener;)V
    .locals 1

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-direct {p0}, Lcom/nuance/dragon/toolkit/audio/AudioSink;-><init>()V

    iput-object p2, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->b:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;

    iput-object p3, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->c:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    iput-object p3, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->d:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    iput-object p4, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->e:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$SignalListener;

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->f:Ljava/util/concurrent/LinkedBlockingQueue;

    return-void
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/vocon/d$b;)Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$SignalListener;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->e:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$SignalListener;

    return-object v0
.end method

.method private a(Lcom/nuance/dragon/toolkit/audio/AudioSource;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;ZLcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/nuance/dragon/toolkit/vocon/VoconParam;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/nuance/dragon/toolkit/vocon/VoconParam;",
            "Ljava/lang/Integer;",
            ">;Z",
            "Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;",
            ")V"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->e(Lcom/nuance/dragon/toolkit/vocon/d;)Landroid/os/Handler;

    move-result-object v9

    new-instance v0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;

    move-object v1, p0

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p1

    move v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/nuance/dragon/toolkit/vocon/d$b$3;-><init>(Lcom/nuance/dragon/toolkit/vocon/d$b;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Lcom/nuance/dragon/toolkit/audio/AudioSource;ZLcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;)V

    invoke-virtual {v9, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private a(Lcom/nuance/dragon/toolkit/vocon/VoconError;)V
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->c(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/d$b;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->d(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->b:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->b:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;

    invoke-interface {v0, p1}, Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;->onError(Lcom/nuance/dragon/toolkit/vocon/VoconError;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->b:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/vocon/d$b;Lcom/nuance/dragon/toolkit/audio/AudioSource;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;ZLcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/audio/AudioSource;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;ZLcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;)V

    return-void
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/vocon/d$b;Lcom/nuance/dragon/toolkit/vocon/VoconError;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/vocon/VoconError;)V

    return-void
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/vocon/d$b;Lcom/nuance/dragon/toolkit/vocon/VoconResult;)V
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->c(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/d$b;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->d(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->b:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->b:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;

    invoke-interface {v0, p1}, Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;->onResult(Lcom/nuance/dragon/toolkit/vocon/VoconResult;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->b:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/vocon/d$b;Ljava/util/List;)V
    .locals 6

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/vocon/VoconContext;

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/vocon/VoconContext;->getSlotIds()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v4, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v4}, Lcom/nuance/dragon/toolkit/vocon/d;->a(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/NativeVocon;

    move-result-object v4

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/vocon/VoconContext;->getFileName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Lcom/nuance/dragon/toolkit/vocon/NativeVocon;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v1}, Lcom/nuance/dragon/toolkit/vocon/d;->a(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/NativeVocon;

    move-result-object v1

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/vocon/VoconContext;->getFileName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/nuance/dragon/toolkit/vocon/NativeVocon;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->b(Lcom/nuance/dragon/toolkit/vocon/d;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/nuance/dragon/toolkit/vocon/d$b$4;

    invoke-direct {v1, p0}, Lcom/nuance/dragon/toolkit/vocon/d$b$4;-><init>(Lcom/nuance/dragon/toolkit/vocon/d$b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic a(Lcom/nuance/dragon/toolkit/vocon/d$b;I)Z
    .locals 2

    :goto_0
    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->f:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/nuance/dragon/toolkit/util/internal/i;->a(J)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->i:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->f:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->size()I

    move-result v0

    if-lez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic b(Lcom/nuance/dragon/toolkit/vocon/d$b;)Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    return-object v0
.end method

.method static synthetic b(Lcom/nuance/dragon/toolkit/vocon/d$b;I)Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x0

    :goto_0
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->h:I

    add-int/2addr v0, p1

    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    iget-object v2, v2, Lcom/nuance/dragon/toolkit/audio/AudioChunk;->audioShorts:[S

    array-length v2, v2

    if-ge v0, v2, :cond_0

    new-array v1, p1, [S

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;->audioShorts:[S

    iget v2, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->h:I

    array-length v3, v1

    invoke-static {v0, v2, v1, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->h:I

    array-length v2, v1

    add-int/2addr v0, v2

    iput v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->h:I

    new-instance v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    iget-object v2, v2, Lcom/nuance/dragon/toolkit/audio/AudioChunk;->audioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    invoke-direct {v0, v2, v1}, Lcom/nuance/dragon/toolkit/audio/AudioChunk;-><init>(Lcom/nuance/dragon/toolkit/audio/AudioType;[S)V

    :goto_1
    return-object v0

    :cond_0
    iget v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->h:I

    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    iget-object v2, v2, Lcom/nuance/dragon/toolkit/audio/AudioChunk;->audioShorts:[S

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;->audioShorts:[S

    array-length v0, v0

    iget v2, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->h:I

    sub-int/2addr v0, v2

    new-array v2, v0, [S

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;->audioShorts:[S

    iget v3, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->h:I

    array-length v4, v2

    invoke-static {v0, v3, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    iget-object v3, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    iget-object v3, v3, Lcom/nuance/dragon/toolkit/audio/AudioChunk;->audioType:Lcom/nuance/dragon/toolkit/audio/AudioType;

    invoke-direct {v0, v3, v2}, Lcom/nuance/dragon/toolkit/audio/AudioChunk;-><init>(Lcom/nuance/dragon/toolkit/audio/AudioType;[S)V

    iput-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    iput v5, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->h:I

    goto :goto_1

    :cond_1
    iput v5, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->h:I

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->f:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->g:Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->f:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->remove()Ljava/lang/Object;

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic c(Lcom/nuance/dragon/toolkit/vocon/d$b;)Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->c:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    return-object v0
.end method

.method static synthetic d(Lcom/nuance/dragon/toolkit/vocon/d$b;)Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->c:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    return-object v0
.end method

.method static synthetic e(Lcom/nuance/dragon/toolkit/vocon/d$b;)Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->d:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    return-object v0
.end method

.method static synthetic f(Lcom/nuance/dragon/toolkit/vocon/d$b;)Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->d:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    return-object v0
.end method

.method static synthetic g(Lcom/nuance/dragon/toolkit/vocon/d$b;)Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->b:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;

    return-object v0
.end method

.method static synthetic h(Lcom/nuance/dragon/toolkit/vocon/d$b;)Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->b:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;

    return-object v0
.end method

.method static synthetic i(Lcom/nuance/dragon/toolkit/vocon/d$b;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->i:Z

    return v0
.end method

.method static synthetic j(Lcom/nuance/dragon/toolkit/vocon/d$b;)Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->j:I

    iget v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->j:I

    mul-int/lit16 v1, v1, 0xbb8

    const v2, 0x1d4c0

    if-lt v1, v2, :cond_0

    iput v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->j:I

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method final a()V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Z)V

    new-instance v0, Lcom/nuance/dragon/toolkit/vocon/VoconError;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/nuance/dragon/toolkit/vocon/VoconError;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/vocon/VoconError;)V

    return-void
.end method

.method final a(Lcom/nuance/dragon/toolkit/audio/AudioSource;Ljava/util/Map;Ljava/util/List;I)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/nuance/dragon/toolkit/vocon/VoconParam;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/nuance/dragon/toolkit/vocon/ParamSpecs$Ctx;->TSILENCE:Lcom/nuance/dragon/toolkit/vocon/VoconParam;

    const/16 v1, 0x258

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocon/ParamSpecs$Ctx;->TSILENCE_FX:Lcom/nuance/dragon/toolkit/vocon/VoconParam;

    const/16 v1, 0x12c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocon/ParamSpecs$Ctx;->MAXNBEST:Lcom/nuance/dragon/toolkit/vocon/VoconParam;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/nuance/dragon/toolkit/vocon/ParamSpecs$Ctx;->MAXNBEST_SECONDPASS:Lcom/nuance/dragon/toolkit/vocon/VoconParam;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v4, v0, 0x2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->i:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->j:I

    invoke-virtual {p0, p1}, Lcom/nuance/dragon/toolkit/vocon/d$b;->connectAudioSource(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V

    const/4 v10, 0x1

    new-instance v0, Lcom/nuance/dragon/toolkit/vocon/d$b$2;

    move-object v1, p0

    move/from16 v5, p4

    move-object v6, p1

    move-object v7, p2

    move-object v8, p3

    invoke-direct/range {v0 .. v9}, Lcom/nuance/dragon/toolkit/vocon/d$b$2;-><init>(Lcom/nuance/dragon/toolkit/vocon/d$b;Ljava/util/List;Ljava/util/List;IILcom/nuance/dragon/toolkit/audio/AudioSource;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;)V

    move-object v5, p0

    move-object v6, p1

    move-object v7, p2

    move-object v8, p3

    move-object v11, v0

    invoke-direct/range {v5 .. v11}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/audio/AudioSource;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;ZLcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;)V

    return-void
.end method

.method final a(Lcom/nuance/dragon/toolkit/audio/AudioSource;Ljava/util/Map;Ljava/util/List;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/nuance/dragon/toolkit/vocon/VoconParam;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/nuance/dragon/toolkit/vocon/VoconContext;",
            ">;Z)V"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->i:Z

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->e(Lcom/nuance/dragon/toolkit/vocon/d;)Landroid/os/Handler;

    move-result-object v6

    new-instance v0, Lcom/nuance/dragon/toolkit/vocon/d$b$1;

    move-object v1, p0

    move-object v2, p2

    move-object v4, p1

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/nuance/dragon/toolkit/vocon/d$b$1;-><init>(Lcom/nuance/dragon/toolkit/vocon/d$b;Ljava/util/Map;Ljava/util/List;Lcom/nuance/dragon/toolkit/audio/AudioSource;Z)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method final a(Z)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->c:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->d:Lcom/nuance/dragon/toolkit/audio/SpeechDetectionListener;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->a(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/NativeVocon;

    move-result-object v0

    invoke-interface {v0}, Lcom/nuance/dragon/toolkit/vocon/NativeVocon;->c()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->i:Z

    return-void
.end method

.method public final chunksAvailable(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->c(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/d$b;

    move-result-object v0

    if-eq v0, p0, :cond_0

    const-string v0, "Got audio for old recognition"

    invoke-static {p0, v0}, Lcom/nuance/dragon/toolkit/util/Logger;->warn(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->f:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {p1, p0}, Lcom/nuance/dragon/toolkit/audio/AudioSource;->getAllAudioChunksForSink(Lcom/nuance/dragon/toolkit/audio/AudioSink;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final framesDropped(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->c(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/d$b;

    move-result-object v0

    if-eq p0, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/nuance/dragon/toolkit/vocon/VoconError;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/nuance/dragon/toolkit/vocon/VoconError;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/vocon/VoconError;)V

    goto :goto_0
.end method

.method public final sourceClosed(Lcom/nuance/dragon/toolkit/audio/AudioSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nuance/dragon/toolkit/audio/AudioSource",
            "<",
            "Lcom/nuance/dragon/toolkit/audio/AudioChunk;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->c(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/d$b;

    move-result-object v0

    if-eq p0, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Z)V

    goto :goto_0
.end method
