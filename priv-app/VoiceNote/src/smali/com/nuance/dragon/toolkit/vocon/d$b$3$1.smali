.class final Lcom/nuance/dragon/toolkit/vocon/d$b$3$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/nuance/dragon/toolkit/vocon/NativeVocon$RecognitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/dragon/toolkit/vocon/d$b$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/nuance/dragon/toolkit/vocon/d$b$3;


# direct methods
.method constructor <init>(Lcom/nuance/dragon/toolkit/vocon/d$b$3;I)V
    .locals 0

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3$1;->b:Lcom/nuance/dragon/toolkit/vocon/d$b$3;

    iput p2, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3$1;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCanceled()V
    .locals 2

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3$1;->b:Lcom/nuance/dragon/toolkit/vocon/d$b$3;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->b(Lcom/nuance/dragon/toolkit/vocon/d;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/nuance/dragon/toolkit/vocon/d$b$3$1$1;

    invoke-direct {v1, p0}, Lcom/nuance/dragon/toolkit/vocon/d$b$3$1$1;-><init>(Lcom/nuance/dragon/toolkit/vocon/d$b$3$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final onEndOfSpeech()V
    .locals 0

    return-void
.end method

.method public final onGetAudioChunk(I)Lcom/nuance/dragon/toolkit/audio/AudioChunk;
    .locals 2

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3$1;->b:Lcom/nuance/dragon/toolkit/vocon/d$b$3;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3$1;->a:I

    invoke-static {v0, v1}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/vocon/d$b;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3$1;->b:Lcom/nuance/dragon/toolkit/vocon/d$b$3;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    invoke-static {v0, p1}, Lcom/nuance/dragon/toolkit/vocon/d$b;->b(Lcom/nuance/dragon/toolkit/vocon/d$b;I)Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onHasAudioChunk()Z
    .locals 1

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3$1;->b:Lcom/nuance/dragon/toolkit/vocon/d$b$3;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d$b;->b(Lcom/nuance/dragon/toolkit/vocon/d$b;)Lcom/nuance/dragon/toolkit/audio/AudioChunk;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onRecognitionResult(Ljava/lang/String;)V
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Lcom/nuance/dragon/toolkit/vocon/VoconResult;

    invoke-direct {v0, p1}, Lcom/nuance/dragon/toolkit/vocon/VoconResult;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3$1;->b:Lcom/nuance/dragon/toolkit/vocon/d$b$3;

    iget-object v1, v1, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v1, v1, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v1}, Lcom/nuance/dragon/toolkit/vocon/d;->b(Lcom/nuance/dragon/toolkit/vocon/d;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/nuance/dragon/toolkit/vocon/d$b$3$1$2;

    invoke-direct {v2, p0, v0}, Lcom/nuance/dragon/toolkit/vocon/d$b$3$1$2;-><init>(Lcom/nuance/dragon/toolkit/vocon/d$b$3$1;Lcom/nuance/dragon/toolkit/vocon/VoconResult;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :catch_0
    move-exception v0

    const-string v2, "Error parsing Vocon raw JSON result."

    invoke-static {p0, v2, v0}, Lcom/nuance/dragon/toolkit/util/Logger;->error(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final onSignalUpdate(I)V
    .locals 0

    return-void
.end method

.method public final onSignalWarning(I)V
    .locals 0

    return-void
.end method

.method public final onStartAudioPull()V
    .locals 0

    return-void
.end method

.method public final onStartOfSpeech()V
    .locals 0

    return-void
.end method

.method public final onStopAudioPull()V
    .locals 0

    return-void
.end method
