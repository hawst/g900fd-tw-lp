.class final Lcom/nuance/dragon/toolkit/vocon/d$b$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/audio/AudioSource;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;ZLcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Map;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Ljava/util/List;

.field final synthetic d:Ljava/util/Map;

.field final synthetic e:Lcom/nuance/dragon/toolkit/audio/AudioSource;

.field final synthetic f:Z

.field final synthetic g:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;

.field final synthetic h:Lcom/nuance/dragon/toolkit/vocon/d$b;


# direct methods
.method constructor <init>(Lcom/nuance/dragon/toolkit/vocon/d$b;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Lcom/nuance/dragon/toolkit/audio/AudioSource;ZLcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;)V
    .locals 0

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iput-object p2, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->a:Ljava/util/Map;

    iput-object p3, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->b:Ljava/util/List;

    iput-object p4, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->c:Ljava/util/List;

    iput-object p5, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->d:Ljava/util/Map;

    iput-object p6, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->e:Lcom/nuance/dragon/toolkit/audio/AudioSource;

    iput-boolean p7, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->f:Z

    iput-object p8, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->g:Lcom/nuance/dragon/toolkit/vocon/VoconRecognizer$ResultListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->a(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/NativeVocon;

    move-result-object v0

    invoke-interface {v0}, Lcom/nuance/dragon/toolkit/vocon/NativeVocon;->b()I

    move-result v6

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->a(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/NativeVocon;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Lcom/nuance/dragon/toolkit/vocon/NativeVocon;->a(Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Unable to set recognizer parameters."

    invoke-static {p0, v0}, Lcom/nuance/dragon/toolkit/util/Logger;->error(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/vocon/d$b;Ljava/util/List;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move v5, v3

    :goto_1
    const/4 v2, 0x2

    if-ge v5, v2, :cond_2

    const-string v2, "default"

    if-lez v5, :cond_7

    const-string v4, "seamless"

    const/4 v2, 0x1

    :goto_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "#"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v8, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v8, v8, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v8}, Lcom/nuance/dragon/toolkit/vocon/d;->a(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/NativeVocon;

    move-result-object v8

    invoke-interface {v8, v4, v2}, Lcom/nuance/dragon/toolkit/vocon/NativeVocon;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v2, Lcom/nuance/dragon/toolkit/vocon/VoconContext;

    invoke-direct {v2, v4}, Lcom/nuance/dragon/toolkit/vocon/VoconContext;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->d:Ljava/util/Map;

    invoke-virtual {v2, v8}, Lcom/nuance/dragon/toolkit/vocon/VoconContext;->setParams(Ljava/util/Map;)V

    iget-object v8, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->b:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v2, v2, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v2}, Lcom/nuance/dragon/toolkit/vocon/d;->a(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/NativeVocon;

    move-result-object v2

    invoke-interface {v2, v4, v0}, Lcom/nuance/dragon/toolkit/vocon/NativeVocon;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v0, "Unable to add WuW phrase in context."

    invoke-static {p0, v0}, Lcom/nuance/dragon/toolkit/util/Logger;->error(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/vocon/d$b;Ljava/util/List;)V

    goto :goto_0

    :cond_3
    const-string v0, "Unable to create WuW context."

    invoke-static {p0, v0}, Lcom/nuance/dragon/toolkit/util/Logger;->error(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/vocon/d$b;Ljava/util/List;)V

    goto/16 :goto_0

    :cond_4
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v0, v0, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v0}, Lcom/nuance/dragon/toolkit/vocon/d;->a(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/NativeVocon;

    move-result-object v0

    mul-int/lit8 v2, v6, 0x2

    iget-object v3, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->e:Lcom/nuance/dragon/toolkit/audio/AudioSource;

    invoke-virtual {v3}, Lcom/nuance/dragon/toolkit/audio/AudioSource;->getAudioType()Lcom/nuance/dragon/toolkit/audio/AudioType;

    move-result-object v3

    iget v3, v3, Lcom/nuance/dragon/toolkit/audio/AudioType;->frequency:I

    iget-boolean v4, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->f:Z

    new-instance v5, Lcom/nuance/dragon/toolkit/vocon/d$b$3$1;

    invoke-direct {v5, p0, v6}, Lcom/nuance/dragon/toolkit/vocon/d$b$3$1;-><init>(Lcom/nuance/dragon/toolkit/vocon/d$b$3;I)V

    invoke-interface/range {v0 .. v5}, Lcom/nuance/dragon/toolkit/vocon/NativeVocon;->a(Ljava/util/List;IIZLcom/nuance/dragon/toolkit/vocon/NativeVocon$RecognitionListener;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "Unable to start recognizer."

    invoke-static {p0, v0}, Lcom/nuance/dragon/toolkit/util/Logger;->error(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v1, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/nuance/dragon/toolkit/vocon/d$b;->a(Lcom/nuance/dragon/toolkit/vocon/d$b;Ljava/util/List;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/dragon/toolkit/vocon/VoconContext;

    iget-object v2, p0, Lcom/nuance/dragon/toolkit/vocon/d$b$3;->h:Lcom/nuance/dragon/toolkit/vocon/d$b;

    iget-object v2, v2, Lcom/nuance/dragon/toolkit/vocon/d$b;->a:Lcom/nuance/dragon/toolkit/vocon/d;

    invoke-static {v2}, Lcom/nuance/dragon/toolkit/vocon/d;->a(Lcom/nuance/dragon/toolkit/vocon/d;)Lcom/nuance/dragon/toolkit/vocon/NativeVocon;

    move-result-object v2

    invoke-virtual {v0}, Lcom/nuance/dragon/toolkit/vocon/VoconContext;->getFileName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/nuance/dragon/toolkit/vocon/NativeVocon;->b(Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    move-object v4, v2

    move v2, v3

    goto/16 :goto_2
.end method
