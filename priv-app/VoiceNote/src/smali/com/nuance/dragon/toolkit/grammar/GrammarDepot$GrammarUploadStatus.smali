.class public final Lcom/nuance/dragon/toolkit/grammar/GrammarDepot$GrammarUploadStatus;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/dragon/toolkit/grammar/GrammarDepot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GrammarUploadStatus"
.end annotation


# static fields
.field public static final INVALID_WORD_LIST_ID:I = 0x2

.field public static final NOTHING_TO_UPLOAD:I = 0x1

.field public static final SUCCESS:I = 0x0

.field public static final UNKNOWN_ERROR:I = 0x5

.field public static final UPLOAD_ERROR:I = 0x3

.field public static final UPLOAD_IN_PROGRESS:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
