.class public Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;
.super Ljava/lang/Object;


# instance fields
.field public final appId:Ljava/lang/String;

.field public final appKey:[B

.field public final deviceId:Ljava/lang/String;

.field public final host:Ljava/lang/String;

.field public final playerCodec:Lcom/nuance/dragon/toolkit/audio/AudioType;

.field public final port:I

.field public final recorderCodec:Lcom/nuance/dragon/toolkit/audio/AudioType;

.field public final sslConfig:Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILcom/nuance/dragon/toolkit/cloudservices/SSLConfig;Ljava/lang/String;[BLjava/lang/String;Lcom/nuance/dragon/toolkit/audio/AudioType;Lcom/nuance/dragon/toolkit/audio/AudioType;)V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "appId"

    invoke-static {v0, p4}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "host"

    invoke-static {v0, p1}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "port"

    const-string v3, "greater than 0"

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v2, v3, v0}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    const-string v0, "appKey"

    invoke-static {v0, p5}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "recorderCodec"

    invoke-static {v0, p7}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "playerCodec"

    invoke-static {v0, p8}, Lcom/nuance/dragon/toolkit/util/internal/d;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;->host:Ljava/lang/String;

    iput p2, p0, Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;->port:I

    iput-object p3, p0, Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;->sslConfig:Lcom/nuance/dragon/toolkit/cloudservices/SSLConfig;

    iput-object p4, p0, Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;->appId:Ljava/lang/String;

    iput-object p6, p0, Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;->deviceId:Ljava/lang/String;

    iput-object p7, p0, Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;->recorderCodec:Lcom/nuance/dragon/toolkit/audio/AudioType;

    iput-object p8, p0, Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;->playerCodec:Lcom/nuance/dragon/toolkit/audio/AudioType;

    array-length v0, p5

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;->appKey:[B

    iget-object v0, p0, Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;->appKey:[B

    array-length v2, p5

    invoke-static {p5, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;[BLcom/nuance/dragon/toolkit/audio/AudioType;Lcom/nuance/dragon/toolkit/audio/AudioType;)V
    .locals 9

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, v3

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/nuance/dragon/toolkit/cloudservices/CloudConfig;-><init>(Ljava/lang/String;ILcom/nuance/dragon/toolkit/cloudservices/SSLConfig;Ljava/lang/String;[BLjava/lang/String;Lcom/nuance/dragon/toolkit/audio/AudioType;Lcom/nuance/dragon/toolkit/audio/AudioType;)V

    return-void
.end method
