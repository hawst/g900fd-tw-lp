.class public Lcom/nuance/a/a/a/b/c/b/b/z;
.super Ljava/lang/Object;
.source "PDXTransactionImpl.java"

# interfaces
.implements Lcom/nuance/a/a/a/a/b/a/b$b;
.implements Lcom/nuance/a/a/a/b/c/c/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/a/a/a/b/c/b/b/z$a;
    }
.end annotation


# static fields
.field private static final g:Lcom/nuance/a/a/a/a/b/a/a$a;


# instance fields
.field protected a:B

.field private h:Lcom/nuance/a/a/a/b/c/b/b/a;

.field private i:Lcom/nuance/a/a/a/b/c/c/f;

.field private j:Lcom/nuance/a/a/a/b/c/c/g;

.field private k:Lcom/nuance/a/a/a/b/c/b/a/b;

.field private l:S

.field private m:Lcom/nuance/a/a/a/a/b/a/b;

.field private n:Lcom/nuance/a/a/a/a/b/a/e$a;

.field private o:J

.field private p:Z

.field private q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

.field private r:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/nuance/a/a/a/b/c/b/b/z;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method protected constructor <init>(Lcom/nuance/a/a/a/a/b/a/b;Lcom/nuance/a/a/a/b/c/c/g;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/a/a/a/b/c/b/a/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/a/a/a/a/a/a$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/nuance/a/a/a/b/c/c/c;Lcom/nuance/a/a/a/b/c/b/b/a;Lcom/nuance/a/a/a/b/c/c/f;B)V
    .locals 6

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;

    .line 70
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->k:Lcom/nuance/a/a/a/b/c/b/a/b;

    .line 78
    const/4 v2, -0x1

    iput-short v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    .line 90
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->p:Z

    .line 132
    sget-object v2, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    sget-object v2, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v3, "PDXTransactionImpl()"

    invoke-virtual {v2, v3}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 136
    :cond_0
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->r:Ljava/lang/Object;

    .line 137
    iput-object p1, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    .line 138
    iput-object p5, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->k:Lcom/nuance/a/a/a/b/c/b/a/b;

    .line 139
    iput-object p2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;

    .line 140
    move-wide/from16 v0, p18

    iput-wide v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->o:J

    .line 141
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->h:Lcom/nuance/a/a/a/b/c/b/b/a;

    .line 142
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->i:Lcom/nuance/a/a/a/b/c/c/f;

    .line 143
    move/from16 v0, p23

    iput-byte v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    .line 145
    invoke-virtual/range {p21 .. p21}, Lcom/nuance/a/a/a/b/c/b/b/a;->g()Lcom/nuance/a/a/a/b/c/a/a;

    move-result-object v2

    check-cast v2, Lcom/nuance/a/a/a/b/c/b/a/a;

    invoke-virtual {v2}, Lcom/nuance/a/a/a/b/c/b/a/a;->h()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 146
    new-instance v2, Lcom/nuance/a/a/a/b/c/b/b/z$a;

    iget-byte v3, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    invoke-direct {v2, v3, p0}, Lcom/nuance/a/a/a/b/c/b/b/z$a;-><init>(ILcom/nuance/a/a/a/b/c/b/b/z;)V

    iput-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    .line 147
    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    const-string v3, "INTERNAL_ERROR"

    invoke-static {v2, v3}, Lcom/nuance/a/a/a/b/c/b/b/z;->b(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V

    .line 150
    :cond_1
    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->r:Ljava/lang/Object;

    monitor-enter v3

    .line 151
    const/4 v2, 0x0

    :try_start_0
    iput-short v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    .line 152
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    const/16 v2, 0x12

    new-array v2, v2, [Ljava/lang/Object;

    .line 165
    const/4 v3, 0x0

    aput-object p6, v2, v3

    .line 167
    const/4 v3, 0x1

    aput-object p7, v2, v3

    .line 168
    const/4 v3, 0x2

    aput-object p8, v2, v3

    .line 169
    const/4 v3, 0x3

    aput-object p9, v2, v3

    .line 170
    const/4 v3, 0x4

    aput-object p10, v2, v3

    .line 171
    const/4 v3, 0x5

    aput-object p11, v2, v3

    .line 172
    const/4 v3, 0x6

    aput-object p12, v2, v3

    .line 173
    const/4 v3, 0x7

    aput-object p13, v2, v3

    .line 174
    const/16 v3, 0x8

    new-instance v4, Ljava/lang/Short;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/lang/Short;-><init>(S)V

    aput-object v4, v2, v3

    .line 175
    const/16 v3, 0x9

    new-instance v4, Ljava/lang/Short;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/lang/Short;-><init>(S)V

    aput-object v4, v2, v3

    .line 176
    const/16 v3, 0xa

    aput-object p14, v2, v3

    .line 177
    const/16 v3, 0xb

    aput-object p15, v2, v3

    .line 178
    const/16 v3, 0xc

    aput-object p16, v2, v3

    .line 179
    const/16 v3, 0xd

    aput-object p17, v2, v3

    .line 181
    const/16 v3, 0xe

    aput-object p4, v2, v3

    .line 182
    const/16 v3, 0xf

    aput-object p3, v2, v3

    .line 183
    const/16 v3, 0x10

    aput-object p20, v2, v3

    .line 185
    new-instance v3, Lcom/nuance/a/a/a/a/b/a/b$a;

    const/4 v4, 0x1

    invoke-direct {v3, v4, v2}, Lcom/nuance/a/a/a/a/b/a/b$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-interface {p1}, Lcom/nuance/a/a/a/a/b/a/b;->a()[Ljava/lang/Object;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-interface {p1, v3, p0, v2, v4}, Lcom/nuance/a/a/a/a/b/a/b;->a(Ljava/lang/Object;Lcom/nuance/a/a/a/a/b/a/b$b;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 186
    return-void

    .line 152
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method static synthetic a(Lcom/nuance/a/a/a/b/c/b/b/z;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->r:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 62
    invoke-static {p0, p1}, Lcom/nuance/a/a/a/b/c/b/b/z;->b(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/nuance/a/a/a/b/c/b/b/z;Lcom/nuance/a/a/a/b/c/b/b/z$a;)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->h:Lcom/nuance/a/a/a/b/c/b/b/a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/a/d;->g()Lcom/nuance/a/a/a/b/c/a/a;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/a/a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/a/a;->h()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    if-nez v0, :cond_1

    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "appendLogToResLogs: NMSPDefines.DEVICE_CMD_LOG_TO_SERVER_ENABLED is disabled."

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->c(Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static b([B)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1023
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1024
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_3

    .line 1029
    aget-byte v2, p0, v0

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    .line 1030
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_2

    .line 1031
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1036
    :goto_1
    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    const/4 v2, 0x5

    if-eq v0, v2, :cond_0

    const/4 v2, 0x7

    if-eq v0, v2, :cond_0

    const/16 v2, 0x9

    if-ne v0, v2, :cond_1

    .line 1038
    :cond_0
    const/16 v2, 0x2d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1024
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1033
    :cond_2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1041
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/nuance/a/a/a/b/c/b/b/z;)S
    .locals 1

    .prologue
    .line 62
    const/4 v0, -0x1

    iput-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    return v0
.end method

.method private static b(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1121
    if-eqz p0, :cond_0

    .line 1122
    invoke-static {p0, p1}, Lcom/nuance/a/a/a/b/c/b/b/z$a;->a(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V

    .line 1123
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/nuance/a/a/a/b/c/b/b/z;)Lcom/nuance/a/a/a/b/c/c/g;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;

    return-object v0
.end method

.method static synthetic d(Lcom/nuance/a/a/a/b/c/b/b/z;)Lcom/nuance/a/a/a/b/c/b/b/z$a;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    return-object v0
.end method

.method static synthetic e(Lcom/nuance/a/a/a/b/c/b/b/z;)Lcom/nuance/a/a/a/b/c/b/a/b;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->k:Lcom/nuance/a/a/a/b/c/b/a/b;

    return-object v0
.end method

.method static synthetic g()Lcom/nuance/a/a/a/a/b/a/a$a;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/a/a/a/b/b/d;,
            Lcom/nuance/a/a/a/b/b/e;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 259
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "PDXTransactionImpl.end()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 262
    :cond_0
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 263
    :try_start_0
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 265
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "PDXTransactionImpl.end() transaction already finished!"

    invoke-virtual {v0, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 267
    new-instance v0, Lcom/nuance/a/a/a/b/b/d;

    const-string v2, "transaction already finished!"

    invoke-direct {v0, v2}, Lcom/nuance/a/a/a/b/b/d;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 268
    :cond_1
    :try_start_1
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-nez v0, :cond_3

    .line 269
    const/4 v0, 0x1

    iput-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    .line 270
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    new-instance v2, Lcom/nuance/a/a/a/a/b/a/b$a;

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/nuance/a/a/a/a/b/a/b$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    invoke-interface {v4}, Lcom/nuance/a/a/a/a/b/a/b;->a()[Ljava/lang/Object;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-interface {v0, v2, p0, v3, v4}, Lcom/nuance/a/a/a/a/b/a/b;->a(Ljava/lang/Object;Lcom/nuance/a/a/a/a/b/a/b$b;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 283
    :cond_2
    monitor-exit v1

    return-void

    .line 272
    :cond_3
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-ne v0, v3, :cond_4

    .line 274
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "PDXTransactionImpl.end() transaction already finished!"

    invoke-virtual {v0, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 276
    new-instance v0, Lcom/nuance/a/a/a/b/b/d;

    const-string v2, "transaction already finished!"

    invoke-direct {v0, v2}, Lcom/nuance/a/a/a/b/b/d;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :cond_4
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 279
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "PDXTransactionImpl.end() transaction already expired!"

    invoke-virtual {v0, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 281
    new-instance v0, Lcom/nuance/a/a/a/b/b/e;

    const-string v2, "transaction already expired!"

    invoke-direct {v0, v2}, Lcom/nuance/a/a/a/b/b/e;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
.end method

.method public final a(Lcom/nuance/a/a/a/b/c/b/b/r;B)V
    .locals 5

    .prologue
    const/4 v2, -0x1

    .line 350
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "PDXTransactionImpl.onQueryError()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 352
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    if-eq p2, v0, :cond_0

    .line 386
    :goto_0
    return-void

    .line 355
    :cond_0
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 356
    :try_start_0
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-eq v0, v2, :cond_1

    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 357
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 386
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 358
    :cond_2
    const/4 v0, -0x1

    :try_start_1
    iput-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    .line 363
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    if-eqz v0, :cond_3

    .line 364
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v2}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    .line 365
    :cond_3
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_5

    .line 367
    :try_start_2
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    const-string v2, "QUERY_ERROR"

    invoke-static {v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/z;->b(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V

    .line 369
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 370
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "[LATCHK] calling PDXQueryErrorReturned()"

    invoke-virtual {v0, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 372
    :cond_4
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;

    invoke-interface {v0, p1}, Lcom/nuance/a/a/a/b/c/c/g;->a(Lcom/nuance/a/a/a/b/c/c/i;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 386
    :cond_5
    :goto_1
    :try_start_3
    monitor-exit v1

    goto :goto_0

    .line 373
    :catch_0
    move-exception v0

    .line 375
    sget-object v2, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "got exp in PDXCommandListener.PDXQueryErrorReturned() ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] msg ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final a(Lcom/nuance/a/a/a/b/c/b/b/t;B)V
    .locals 5

    .prologue
    .line 296
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "[LATCHK] PDXTransactionImpl.onQueryResults()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 299
    :cond_0
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    if-eq p2, v0, :cond_1

    .line 345
    :goto_0
    return-void

    .line 302
    :cond_1
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 303
    :try_start_0
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 304
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 345
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 311
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/b/t;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 312
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v2}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    .line 313
    :cond_4
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_7

    .line 315
    :try_start_2
    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/b/t;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 316
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    const-string v2, "FINAL_RESULT"

    invoke-static {v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/z;->b(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V

    .line 319
    :cond_5
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 320
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "[LATCHK] calling PDXQueryResultReturned()"

    invoke-virtual {v0, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 322
    :cond_6
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;

    invoke-interface {v0, p1}, Lcom/nuance/a/a/a/b/c/c/g;->a(Lcom/nuance/a/a/a/b/c/c/j;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 345
    :cond_7
    :goto_1
    :try_start_3
    monitor-exit v1

    goto :goto_0

    .line 323
    :catch_0
    move-exception v0

    .line 325
    sget-object v2, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "got exp in PDXCommandListener.PDXQueryResultReturned() ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] msg ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final a(Lcom/nuance/a/a/a/b/c/b/b/u;B)V
    .locals 5

    .prologue
    const/4 v2, -0x1

    .line 391
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "PDXTransactionImpl.onQueryRetry()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 393
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    if-eq p2, v0, :cond_0

    .line 427
    :goto_0
    return-void

    .line 396
    :cond_0
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 397
    :try_start_0
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-eq v0, v2, :cond_1

    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 398
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 427
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 399
    :cond_2
    const/4 v0, -0x1

    :try_start_1
    iput-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    .line 404
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    if-eqz v0, :cond_3

    .line 405
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v2}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    .line 406
    :cond_3
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_5

    .line 408
    :try_start_2
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    const-string v2, "QUERY_RETRY"

    invoke-static {v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/z;->b(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V

    .line 410
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 411
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "[LATCHK] calling PDXQueryRetryReturned()"

    invoke-virtual {v0, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 413
    :cond_4
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;

    invoke-interface {v0, p1}, Lcom/nuance/a/a/a/b/c/c/g;->a(Lcom/nuance/a/a/a/b/c/c/k;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 427
    :cond_5
    :goto_1
    :try_start_3
    monitor-exit v1

    goto :goto_0

    .line 414
    :catch_0
    move-exception v0

    .line 416
    sget-object v2, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "got exp in PDXCommandListener.PDXQueryRetryReturned() ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] msg ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final a(Lcom/nuance/a/a/a/b/c/c/h;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/a/a/a/b/b/d;,
            Lcom/nuance/a/a/a/b/b/e;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 193
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "PDXTransactionImpl.sendParam()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 196
    :cond_0
    if-nez p1, :cond_1

    .line 197
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Parameter cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :cond_1
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 200
    :try_start_0
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 202
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "PDXTransactionImpl.sendParam() transaction already finished!"

    invoke-virtual {v0, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 204
    new-instance v0, Lcom/nuance/a/a/a/b/b/d;

    const-string v2, "transaction already finished!"

    invoke-direct {v0, v2}, Lcom/nuance/a/a/a/b/b/d;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 205
    :cond_2
    :try_start_1
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-nez v0, :cond_4

    .line 206
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    new-instance v2, Lcom/nuance/a/a/a/a/b/a/b$a;

    const/4 v3, 0x2

    invoke-direct {v2, v3, p1}, Lcom/nuance/a/a/a/a/b/a/b$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    invoke-interface {v4}, Lcom/nuance/a/a/a/a/b/a/b;->a()[Ljava/lang/Object;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-interface {v0, v2, p0, v3, v4}, Lcom/nuance/a/a/a/a/b/a/b;->a(Ljava/lang/Object;Lcom/nuance/a/a/a/a/b/a/b$b;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 219
    :cond_3
    monitor-exit v1

    return-void

    .line 208
    :cond_4
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    const/4 v2, 0x1

    if-ne v0, v2, :cond_5

    .line 210
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "PDXTransactionImpl.sendParam() transaction already finished!"

    invoke-virtual {v0, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 212
    new-instance v0, Lcom/nuance/a/a/a/b/b/d;

    const-string v2, "transaction already finished!"

    invoke-direct {v0, v2}, Lcom/nuance/a/a/a/b/b/d;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_5
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-ne v0, v3, :cond_3

    .line 215
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "PDXTransactionImpl.sendParam() transaction already expired!"

    invoke-virtual {v0, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 217
    new-instance v0, Lcom/nuance/a/a/a/b/b/e;

    const-string v2, "transaction already expired!"

    invoke-direct {v0, v2}, Lcom/nuance/a/a/a/b/b/e;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 22

    .prologue
    .line 592
    check-cast p1, Lcom/nuance/a/a/a/a/b/a/b$a;

    .line 593
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    .line 597
    move-object/from16 v0, p1

    iget-byte v3, v0, Lcom/nuance/a/a/a/a/b/a/b$a;->a:B

    packed-switch v3, :pswitch_data_0

    .line 617
    :cond_0
    :goto_0
    return-void

    .line 599
    :pswitch_0
    check-cast v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    check-cast v3, Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v4, v2, v4

    check-cast v4, Ljava/lang/String;

    const/4 v5, 0x2

    aget-object v5, v2, v5

    check-cast v5, Ljava/lang/String;

    const/4 v6, 0x3

    aget-object v6, v2, v6

    check-cast v6, Ljava/lang/String;

    const/4 v7, 0x4

    aget-object v7, v2, v7

    check-cast v7, Ljava/lang/String;

    const/4 v8, 0x5

    aget-object v8, v2, v8

    check-cast v8, Ljava/lang/String;

    const/4 v9, 0x6

    aget-object v9, v2, v9

    check-cast v9, Lcom/nuance/a/a/a/a/a/a$a;

    const/4 v10, 0x7

    aget-object v10, v2, v10

    check-cast v10, Ljava/lang/String;

    const/16 v11, 0x8

    aget-object v11, v2, v11

    check-cast v11, Ljava/lang/Short;

    invoke-virtual {v11}, Ljava/lang/Short;->shortValue()S

    move-result v11

    const/16 v12, 0x9

    aget-object v12, v2, v12

    check-cast v12, Ljava/lang/Short;

    invoke-virtual {v12}, Ljava/lang/Short;->shortValue()S

    move-result v12

    const/16 v13, 0xa

    aget-object v13, v2, v13

    check-cast v13, Ljava/lang/String;

    const/16 v14, 0xb

    aget-object v14, v2, v14

    check-cast v14, Ljava/lang/String;

    const/16 v15, 0xc

    aget-object v15, v2, v15

    check-cast v15, Ljava/lang/String;

    const/16 v16, 0xd

    aget-object v16, v2, v16

    check-cast v16, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->k:Lcom/nuance/a/a/a/b/c/b/a/b;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/nuance/a/a/a/b/c/b/a/b;->d()[B

    move-result-object v17

    const/16 v18, 0xe

    aget-object v18, v2, v18

    check-cast v18, Ljava/lang/String;

    const/16 v19, 0xf

    aget-object v19, v2, v19

    check-cast v19, Ljava/lang/String;

    const/16 v20, 0x10

    aget-object v20, v2, v20

    check-cast v20, Lcom/nuance/a/a/a/b/c/c/c;

    new-instance v2, Lcom/nuance/a/a/a/c/b;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->h:Lcom/nuance/a/a/a/b/c/b/b/a;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->d:Ljava/util/Vector;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-direct {v2, v0}, Lcom/nuance/a/a/a/c/b;-><init>(Ljava/util/Vector;)V

    invoke-virtual {v2}, Lcom/nuance/a/a/a/c/b;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v9}, Lcom/nuance/a/a/a/a/d/d;->b(Lcom/nuance/a/a/a/a/a/a$a;)Lcom/nuance/a/a/a/a/a/a$a;

    move-result-object v9

    :cond_1
    invoke-static {}, Lcom/nuance/a/a/a/b/c/b/a/d;->i()J

    new-instance v2, Lcom/nuance/a/a/a/b/c/b/b/p;

    invoke-direct/range {v2 .. v20}, Lcom/nuance/a/a/a/b/c/b/b/p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/a/a/a/a/a/a$a;Ljava/lang/String;SSLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Lcom/nuance/a/a/a/b/c/c/c;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->h:Lcom/nuance/a/a/a/b/c/b/b/a;

    invoke-virtual {v3}, Lcom/nuance/a/a/a/b/c/b/a/d;->g()Lcom/nuance/a/a/a/b/c/a/a;

    move-result-object v3

    check-cast v3, Lcom/nuance/a/a/a/b/c/b/a/a;

    invoke-virtual {v3}, Lcom/nuance/a/a/a/b/c/b/a/a;->h()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Vector;

    if-nez v3, :cond_2

    sget-object v3, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v4, "appendLogToQueryBegin: NMSPDefines.DEVICE_CMD_LOG_TO_SERVER_ENABLED is disabled"

    invoke-virtual {v3, v4}, Lcom/nuance/a/a/a/a/b/a/a$a;->c(Ljava/lang/Object;)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->k:Lcom/nuance/a/a/a/b/c/b/a/b;

    const/16 v3, 0xa25

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SEND_BCP_BEGIN"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-byte v5, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->f()[B

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-byte v7, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->h:Lcom/nuance/a/a/a/b/c/b/b/a;

    const/4 v11, 0x0

    move-object v2, v12

    invoke-virtual/range {v2 .. v11}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(SLjava/lang/String;[B[BBJLcom/nuance/a/a/a/b/c/b/a/c;Z)V

    if-eqz v17, :cond_0

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->p:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->p:Z

    sget-object v2, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PDXCommandCreated() called from handleInit()"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v17 .. v17}, Lcom/nuance/a/a/a/b/c/b/b/z;->b([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-byte v4, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->i:Lcom/nuance/a/a/a/b/c/c/f;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->i:Lcom/nuance/a/a/a/b/c/c/f;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v17 .. v17}, Lcom/nuance/a/a/a/b/c/b/b/z;->b([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-byte v4, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/nuance/a/a/a/b/c/c/f;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v2

    sget-object v3, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "got exp in PDXCommandListener.PDXCommandCreated() ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] msg ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v4

    if-nez v4, :cond_3

    sget-object v3, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v4, "appendLogToQueryBegin: nmasResLogsToServer is empty, nothing to log to server"

    invoke-virtual {v3, v4}, Lcom/nuance/a/a/a/a/b/a/a$a;->c(Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_3
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v6

    new-instance v7, Lcom/nuance/a/a/a/b/c/b/b/i;

    invoke-direct {v7}, Lcom/nuance/a/a/a/b/c/b/b/i;-><init>()V

    new-instance v8, Lcom/nuance/a/a/a/b/c/b/b/w;

    invoke-direct {v8}, Lcom/nuance/a/a/a/b/c/b/b/w;-><init>()V

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    if-ge v5, v6, :cond_5

    invoke-virtual {v3, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/nuance/a/a/a/b/c/b/b/z$a;

    invoke-virtual {v4}, Lcom/nuance/a/a/a/b/c/b/b/z$a;->a()I

    move-result v9

    move-object/from16 v0, p0

    iget-byte v10, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    if-eq v9, v10, :cond_4

    new-instance v9, Lcom/nuance/a/a/a/b/c/b/b/i;

    invoke-direct {v9}, Lcom/nuance/a/a/a/b/c/b/b/i;-><init>()V

    const-string v10, "id"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Lcom/nuance/a/a/a/b/c/b/b/z$a;->b()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v4}, Lcom/nuance/a/a/a/b/c/b/b/z$a;->a()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0xc1

    invoke-virtual {v9, v10, v11, v12}, Lcom/nuance/a/a/a/b/c/b/b/i;->a(Ljava/lang/String;Ljava/lang/String;S)V

    const-string v10, "status"

    invoke-virtual {v4}, Lcom/nuance/a/a/a/b/c/b/b/z$a;->c()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0xc1

    invoke-virtual {v9, v10, v11, v12}, Lcom/nuance/a/a/a/b/c/b/b/i;->a(Ljava/lang/String;Ljava/lang/String;S)V

    invoke-virtual {v8, v9}, Lcom/nuance/a/a/a/b/c/b/b/w;->a(Lcom/nuance/a/a/a/b/c/c/c;)V

    invoke-virtual {v4}, Lcom/nuance/a/a/a/b/c/b/b/z$a;->d()V

    :cond_4
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    :cond_5
    const-string v3, "device_log"

    invoke-virtual {v7, v3, v8}, Lcom/nuance/a/a/a/b/c/b/b/i;->a(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/c/l;)V

    const-string v3, "app_info"

    invoke-virtual {v2, v3, v7}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/c/c;)V

    goto/16 :goto_1

    .line 602
    :pswitch_1
    check-cast v2, Lcom/nuance/a/a/a/b/c/c/h;

    move-object v3, v2

    check-cast v3, Lcom/nuance/a/a/a/b/c/b/b/o;

    invoke-virtual {v3}, Lcom/nuance/a/a/a/b/c/b/b/o;->c()B

    move-result v3

    const/16 v4, 0x7f

    if-ne v3, v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->k:Lcom/nuance/a/a/a/b/c/b/a/b;

    move-object v3, v2

    check-cast v3, Lcom/nuance/a/a/a/b/c/b/b/x;

    invoke-virtual {v3}, Lcom/nuance/a/a/a/b/c/b/b/x;->a()I

    move-result v5

    move-object v3, v2

    check-cast v3, Lcom/nuance/a/a/a/b/c/b/b/x;

    invoke-virtual {v3}, Lcom/nuance/a/a/a/b/c/b/b/x;->e()Lcom/nuance/a/a/a/b/b/a;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->h:Lcom/nuance/a/a/a/b/c/b/b/a;

    invoke-virtual {v4, v5, v3, v6}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(ILcom/nuance/a/a/a/b/b/a;Lcom/nuance/a/a/a/b/c/b/a/c;)V

    :cond_6
    invoke-static {}, Lcom/nuance/a/a/a/b/c/b/a/d;->i()J

    new-instance v5, Lcom/nuance/a/a/a/b/c/b/b/s;

    check-cast v2, Lcom/nuance/a/a/a/b/c/b/b/o;

    invoke-direct {v5, v2}, Lcom/nuance/a/a/a/b/c/b/b/s;-><init>(Lcom/nuance/a/a/a/b/c/b/b/o;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->k:Lcom/nuance/a/a/a/b/c/b/a/b;

    const/16 v3, 0xa19

    const-string v4, "SEND_BCP_DATA"

    invoke-virtual {v5}, Lcom/nuance/a/a/a/b/c/b/b/s;->d()[B

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-byte v7, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->h:Lcom/nuance/a/a/a/b/c/b/b/a;

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(SLjava/lang/String;[B[BBJLcom/nuance/a/a/a/b/c/b/a/c;Z)V

    goto/16 :goto_0

    .line 605
    :pswitch_2
    check-cast v2, [B

    invoke-static {}, Lcom/nuance/a/a/a/b/c/b/a/d;->i()J

    new-instance v5, Lcom/nuance/a/a/a/b/c/b/b/j;

    invoke-direct {v5, v2}, Lcom/nuance/a/a/a/b/c/b/b/j;-><init>([B)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->k:Lcom/nuance/a/a/a/b/c/b/a/b;

    const/16 v3, 0xa19

    const-string v4, "SEND_BCP_DATA"

    invoke-virtual {v5}, Lcom/nuance/a/a/a/b/c/b/b/j;->d()[B

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-byte v7, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->h:Lcom/nuance/a/a/a/b/c/b/b/a;

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(SLjava/lang/String;[B[BBJLcom/nuance/a/a/a/b/c/b/a/c;Z)V

    goto/16 :goto_0

    .line 608
    :pswitch_3
    invoke-static {}, Lcom/nuance/a/a/a/b/c/b/a/d;->i()J

    new-instance v5, Lcom/nuance/a/a/a/b/c/b/b/q;

    invoke-direct {v5}, Lcom/nuance/a/a/a/b/c/b/b/q;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->k:Lcom/nuance/a/a/a/b/c/b/a/b;

    const/16 v3, 0xa19

    const-string v4, "SEND_BCP_DATA"

    invoke-virtual {v5}, Lcom/nuance/a/a/a/b/c/b/b/q;->d()[B

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-byte v7, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->h:Lcom/nuance/a/a/a/b/c/b/b/a;

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(SLjava/lang/String;[B[BBJLcom/nuance/a/a/a/b/c/b/a/c;Z)V

    new-instance v2, Lcom/nuance/a/a/a/b/c/b/b/z$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/nuance/a/a/a/b/c/b/b/z$1;-><init>(Lcom/nuance/a/a/a/b/c/b/b/z;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->o:J

    invoke-interface {v2, v3, v4, v5}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;J)V

    goto/16 :goto_0

    .line 611
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;

    const/4 v3, 0x4

    invoke-interface {v2, v3}, Lcom/nuance/a/a/a/b/c/c/g;->a(S)V

    goto/16 :goto_0

    .line 614
    :pswitch_5
    check-cast v2, Lcom/nuance/a/a/a/b/c/c/b$a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    if-eqz v3, :cond_0

    invoke-virtual {v3, v2}, Lcom/nuance/a/a/a/b/c/b/b/z$a;->a(Lcom/nuance/a/a/a/b/c/c/b$a;)V

    goto/16 :goto_0

    .line 597
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(S)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x3

    const/4 v3, -0x1

    .line 447
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 448
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXTransactionImpl.onSessionDisconnected() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 450
    :cond_0
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 451
    :try_start_0
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-ne v0, v5, :cond_1

    .line 452
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    if-eqz v0, :cond_1

    .line 453
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v2}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    .line 455
    :cond_1
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-ne v0, v6, :cond_2

    .line 456
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 525
    :goto_0
    return-void

    .line 460
    :cond_2
    if-nez p1, :cond_4

    .line 461
    :try_start_1
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-eq v0, v3, :cond_3

    .line 462
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    const-string v2, "REMOTE_DISC"

    invoke-static {v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/z;->b(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V

    .line 463
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Lcom/nuance/a/a/a/b/c/c/g;->a(S)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521
    :cond_3
    :goto_1
    const/4 v0, 0x2

    :try_start_2
    iput-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    .line 525
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 466
    :cond_4
    if-ne p1, v5, :cond_5

    .line 467
    :try_start_3
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-eq v0, v3, :cond_3

    .line 468
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;

    const/4 v2, 0x4

    invoke-interface {v0, v2}, Lcom/nuance/a/a/a/b/c/c/g;->a(S)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 507
    :catch_0
    move-exception v0

    .line 509
    :try_start_4
    sget-object v2, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "got exp in PDXCommandListener.PDXCommandEvent() or PDXManagerListener.PDXManagerError() or PDXManagerListener.PDXManagerDisconnected() reasonCode ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] msg ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 471
    :cond_5
    if-ne p1, v4, :cond_6

    .line 472
    :try_start_5
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-eq v0, v3, :cond_3

    .line 473
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    const-string v2, "TIMEOUT_IDLE"

    invoke-static {v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/z;->b(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V

    .line 474
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;

    const/4 v2, 0x5

    invoke-interface {v0, v2}, Lcom/nuance/a/a/a/b/c/c/g;->a(S)V

    goto :goto_1

    .line 477
    :cond_6
    if-ne p1, v7, :cond_7

    .line 478
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-eq v0, v3, :cond_3

    .line 479
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    const-string v2, "CONN_FAILED"

    invoke-static {v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/z;->b(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V

    .line 480
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->i:Lcom/nuance/a/a/a/b/c/c/f;

    invoke-interface {v0}, Lcom/nuance/a/a/a/b/c/c/f;->a()V

    goto :goto_1

    .line 483
    :cond_7
    const/4 v0, 0x5

    if-ne p1, v0, :cond_8

    .line 484
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-eq v0, v3, :cond_3

    .line 485
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    const-string v2, "CONN_FAILED"

    invoke-static {v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/z;->b(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V

    .line 486
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->i:Lcom/nuance/a/a/a/b/c/c/f;

    invoke-interface {v0}, Lcom/nuance/a/a/a/b/c/c/f;->a()V

    goto/16 :goto_1

    .line 489
    :cond_8
    const/4 v0, 0x6

    if-ne p1, v0, :cond_9

    .line 490
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-eq v0, v3, :cond_3

    .line 491
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    const-string v2, "REMOTE_DISC"

    invoke-static {v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/z;->b(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V

    .line 492
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Lcom/nuance/a/a/a/b/c/c/g;->a(S)V

    goto/16 :goto_1

    .line 495
    :cond_9
    const/4 v0, 0x7

    if-ne p1, v0, :cond_a

    .line 496
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-eq v0, v3, :cond_3

    .line 497
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    const-string v2, "CONN_FAILED"

    invoke-static {v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/z;->b(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V

    .line 498
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->i:Lcom/nuance/a/a/a/b/c/c/f;

    invoke-interface {v0}, Lcom/nuance/a/a/a/b/c/c/f;->a()V

    goto/16 :goto_1

    .line 501
    :cond_a
    const/16 v0, 0x8

    if-ne p1, v0, :cond_3

    .line 502
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-eq v0, v3, :cond_3

    .line 503
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    const-string v2, "REMOTE_DISC"

    invoke-static {v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/z;->b(Lcom/nuance/a/a/a/b/c/b/b/z$a;Ljava/lang/String;)V

    .line 504
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->j:Lcom/nuance/a/a/a/b/c/c/g;

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Lcom/nuance/a/a/a/b/c/c/g;->a(S)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1
.end method

.method public final a([B)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 530
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "PDXTransactionImpl.onSessionConnected()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 533
    :cond_0
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 534
    :try_start_0
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-eqz v0, :cond_1

    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v2, :cond_3

    .line 539
    :cond_1
    :try_start_1
    invoke-static {p1}, Lcom/nuance/a/a/a/b/c/b/b/z;->b([B)Ljava/lang/String;

    move-result-object v0

    .line 540
    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->q:Lcom/nuance/a/a/a/b/c/b/b/z$a;

    if-eqz v2, :cond_2

    invoke-virtual {v2, v0}, Lcom/nuance/a/a/a/b/c/b/b/z$a;->a(Ljava/lang/String;)V

    .line 541
    :cond_2
    iget-boolean v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->p:Z

    if-nez v2, :cond_3

    .line 542
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->p:Z

    .line 544
    sget-object v2, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PDXCommandCreated() called from onSessionConnected()"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-byte v4, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->i:Lcom/nuance/a/a/a/b/c/c/f;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 546
    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->i:Lcom/nuance/a/a/a/b/c/c/f;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v3, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/nuance/a/a/a/b/c/c/f;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 555
    :cond_3
    :goto_0
    :try_start_2
    monitor-exit v1

    return-void

    .line 548
    :catch_0
    move-exception v0

    .line 550
    sget-object v2, Lcom/nuance/a/a/a/b/c/b/b/z;->g:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "got exp in PDXCommandListener.PDXCommandCreated() ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] msg ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 555
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 432
    invoke-virtual {p0}, Lcom/nuance/a/a/a/b/c/b/b/z;->f()V

    .line 433
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 437
    invoke-virtual {p0}, Lcom/nuance/a/a/a/b/c/b/b/z;->f()V

    .line 438
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 442
    invoke-virtual {p0}, Lcom/nuance/a/a/a/b/c/b/b/z;->f()V

    .line 443
    return-void
.end method

.method public final e()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    .line 559
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 560
    :try_start_0
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 561
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    if-eqz v0, :cond_0

    .line 562
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v2}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    .line 564
    :cond_0
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    if-ne v0, v3, :cond_1

    .line 565
    monitor-exit v1

    .line 579
    :goto_0
    return-void

    .line 567
    :cond_1
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    .line 569
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    new-instance v2, Lcom/nuance/a/a/a/a/b/a/b$a;

    const/4 v3, 0x5

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/nuance/a/a/a/a/b/a/b$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    iget-object v4, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    invoke-interface {v4}, Lcom/nuance/a/a/a/a/b/a/b;->a()[Ljava/lang/Object;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-interface {v0, v2, p0, v3, v4}, Lcom/nuance/a/a/a/a/b/a/b;->a(Ljava/lang/Object;Lcom/nuance/a/a/a/a/b/a/b$b;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 578
    :cond_2
    const/4 v0, 0x2

    iput-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->l:S

    .line 579
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final f()V
    .locals 4

    .prologue
    .line 790
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    .line 792
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->m:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    iget-wide v2, p0, Lcom/nuance/a/a/a/b/c/b/b/z;->o:J

    invoke-interface {v0, v1, v2, v3}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;J)V

    .line 794
    :cond_0
    return-void
.end method
