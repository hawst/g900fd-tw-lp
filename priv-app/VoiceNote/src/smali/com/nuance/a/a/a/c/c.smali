.class public Lcom/nuance/a/a/a/c/c;
.super Ljava/lang/Object;
.source "DeviceInfoOEM.java"


# static fields
.field private static final a:Lcom/nuance/a/a/a/a/b/a/a$a;


# instance fields
.field private b:Landroid/telephony/TelephonyManager;

.field private c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/nuance/a/a/a/c/c;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/c/c;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/a/a/a/c/c;->c:Landroid/content/Context;

    .line 49
    iput-object p1, p0, Lcom/nuance/a/a/a/c/c;->c:Landroid/content/Context;

    .line 50
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/nuance/a/a/a/c/c;->b:Landroid/telephony/TelephonyManager;

    .line 51
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 76
    iget-object v0, p0, Lcom/nuance/a/a/a/c/c;->c:Landroid/content/Context;

    const-string v1, "nuance_sdk_pref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 77
    const-string v0, "NUANCE_NMSP_UID"

    const-string v2, ""

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 80
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_0

    .line 83
    const-string v2, "-"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 86
    :cond_0
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 87
    const-string v2, "NUANCE_NMSP_UID"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 89
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    if-nez v1, :cond_1

    .line 91
    sget-object v1, Lcom/nuance/a/a/a/c/c;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "Storing nuance sdk uid has failed"

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 96
    :cond_1
    if-nez v0, :cond_2

    const-string v0, ""

    .line 98
    :cond_2
    return-object v0
.end method
