.class public Lcom/nuance/a/a/a/b/c/b/a/b;
.super Ljava/lang/Object;
.source "NMSPSession.java"

# interfaces
.implements Lcom/nuance/a/a/a/a/b/a/b$b;
.implements Lcom/nuance/a/a/a/b/a/a$a;


# static fields
.field private static final a:Lcom/nuance/a/a/a/a/b/a/a$a;

.field private static m:[B

.field private static p:I

.field private static v:[Ljava/lang/String;


# instance fields
.field private b:Ljava/util/Hashtable;

.field private c:Ljava/util/Hashtable;

.field private d:Ljava/util/Hashtable;

.field private e:Lcom/nuance/a/a/a/b/a/a;

.field private f:Ljava/lang/String;

.field private g:S

.field private h:Ljava/util/Vector;

.field private i:Lcom/nuance/a/a/a/a/b/a/b;

.field private j:Lcom/nuance/a/a/a/b/c/a/c;

.field private k:Ljava/util/Vector;

.field private l:[B

.field private n:Lcom/nuance/a/a/a/a/a/a$a;

.field private o:Lcom/nuance/a/a/a/a/a/a$a;

.field private q:J

.field private r:Z

.field private s:Lcom/nuance/a/a/a/b/c/a/d;

.field private t:J

.field private u:B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v1, 0x10

    const/4 v3, 0x1

    .line 55
    const-class v0, Lcom/nuance/a/a/a/b/c/b/a/b;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    .line 72
    new-array v0, v1, [B

    sput-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->m:[B

    .line 77
    sput v3, Lcom/nuance/a/a/a/b/c/b/a/b;->p:I

    .line 1161
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "0"

    aput-object v2, v0, v1

    const-string v1, "1"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "2"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "3"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "4"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "5"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "6"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "7"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "8"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "9"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "A"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "B"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "C"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "D"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "E"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "F"

    aput-object v2, v0, v1

    sput-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->v:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;SLjava/lang/String;[BLjava/lang/String;Ljava/util/Vector;Lcom/nuance/a/a/a/a/b/a/b;Lcom/nuance/a/a/a/b/c/a/c;)V
    .locals 11

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    .line 81
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->r:Z

    .line 84
    const-wide/16 v2, 0x1

    iput-wide v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->t:J

    .line 85
    const/4 v2, 0x1

    iput-byte v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->u:B

    .line 106
    iput-object p1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->f:Ljava/lang/String;

    .line 107
    iput-short p2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->g:S

    .line 108
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->i:Lcom/nuance/a/a/a/a/b/a/b;

    .line 109
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->j:Lcom/nuance/a/a/a/b/c/a/c;

    .line 110
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->s:Lcom/nuance/a/a/a/b/c/a/d;

    .line 112
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    .line 114
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->b:Ljava/util/Hashtable;

    .line 115
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->c:Ljava/util/Hashtable;

    .line 116
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    iput-object v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->d:Ljava/util/Hashtable;

    .line 117
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    .line 119
    new-instance v2, Lcom/nuance/a/a/a/b/a/a;

    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->f:Ljava/lang/String;

    iget-short v4, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->g:S

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move-object v8, p0

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    invoke-direct/range {v2 .. v10}, Lcom/nuance/a/a/a/b/a/a;-><init>(Ljava/lang/String;SLjava/lang/String;[BLjava/lang/String;Lcom/nuance/a/a/a/b/a/a$a;Ljava/util/Vector;Lcom/nuance/a/a/a/a/b/a/b;)V

    iput-object v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->e:Lcom/nuance/a/a/a/b/a/a;

    .line 124
    return-void
.end method

.method public static a([B)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1146
    if-nez p0, :cond_0

    .line 1147
    const-string v0, ""

    .line 1158
    :goto_0
    return-object v0

    .line 1149
    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1151
    const/4 v0, 0x0

    :goto_1
    const/16 v2, 0x10

    if-ge v0, v2, :cond_3

    .line 1153
    aget-byte v2, p0, v0

    and-int/lit16 v3, v2, 0xf0

    int-to-byte v3, v3

    ushr-int/lit8 v3, v3, 0x4

    int-to-byte v3, v3

    and-int/lit8 v3, v3, 0xf

    int-to-byte v3, v3

    and-int/lit8 v2, v2, 0xf

    int-to-byte v2, v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/nuance/a/a/a/b/c/b/a/b;->v:[Ljava/lang/String;

    aget-object v3, v5, v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/nuance/a/a/a/b/c/b/a/b;->v:[Ljava/lang/String;

    aget-object v2, v4, v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1154
    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    const/4 v2, 0x5

    if-eq v0, v2, :cond_1

    const/4 v2, 0x7

    if-eq v0, v2, :cond_1

    const/16 v2, 0x9

    if-ne v0, v2, :cond_2

    .line 1155
    :cond_1
    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1151
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1158
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(BLjava/lang/Object;)V
    .locals 5

    .prologue
    .line 984
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->i:Lcom/nuance/a/a/a/a/b/a/b;

    new-instance v1, Lcom/nuance/a/a/a/a/b/a/b$a;

    invoke-direct {v1, p1, p2}, Lcom/nuance/a/a/a/a/b/a/b$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->i:Lcom/nuance/a/a/a/a/b/a/b;

    invoke-interface {v3}, Lcom/nuance/a/a/a/a/b/a/b;->a()[Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-interface {v0, v1, p0, v2, v3}, Lcom/nuance/a/a/a/a/b/a/b;->a(Ljava/lang/Object;Lcom/nuance/a/a/a/a/b/a/b$b;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 985
    return-void
.end method

.method private b([B)V
    .locals 4

    .prologue
    .line 633
    const/16 v0, 0x10

    aget-byte v2, p1, v0

    .line 638
    const/16 v0, 0x11

    invoke-static {p1, v0}, Lcom/nuance/a/a/a/a/d/b;->a([BI)S

    move-result v3

    .line 641
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 647
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/a/c;

    .line 648
    invoke-interface {v0, v2, v3}, Lcom/nuance/a/a/a/b/c/b/a/c;->a(BS)V

    .line 646
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 651
    :cond_0
    return-void
.end method

.method private c([B)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 680
    .line 683
    invoke-static {p1, v0}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v3

    .line 687
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->c:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 748
    :cond_0
    :goto_0
    return-void

    .line 690
    :cond_1
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->c:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/b/a;

    .line 691
    if-nez v0, :cond_2

    .line 696
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find the audio sink associated with AID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_0

    .line 702
    :cond_2
    const/4 v1, 0x4

    invoke-static {p1, v1}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v2

    .line 703
    const/16 v1, 0x8

    .line 706
    iget-object v4, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->o:Lcom/nuance/a/a/a/a/a/a$a;

    invoke-static {v4}, Lcom/nuance/a/a/a/a/d/d;->a(Lcom/nuance/a/a/a/a/a/a$a;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 707
    :goto_1
    aget-byte v4, p1, v1

    and-int/lit16 v4, v4, 0x80

    if-lez v4, :cond_3

    .line 708
    add-int/lit8 v1, v1, 0x1

    .line 709
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 711
    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 712
    add-int/lit8 v2, v2, -0x1

    .line 718
    :cond_4
    if-lez v2, :cond_5

    array-length v4, p1

    sub-int/2addr v4, v1

    if-gt v2, v4, :cond_5

    .line 723
    const/4 v4, 0x0

    :try_start_0
    invoke-interface {v0, p1, v1, v2, v4}, Lcom/nuance/a/a/a/b/b/a;->a([BIIZ)V
    :try_end_0
    .catch Lcom/nuance/a/a/a/b/b/f; {:try_start_0 .. :try_end_0} :catch_0

    .line 733
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->d:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 736
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->d:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/a/c;

    .line 737
    if-nez v0, :cond_6

    .line 741
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "parseXModeMsgVapPlay:: Could not find the session listener associated with AID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_0

    .line 725
    :catch_0
    move-exception v0

    .line 727
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/f;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_2

    .line 746
    :cond_6
    invoke-interface {v0}, Lcom/nuance/a/a/a/b/c/b/a/c;->e()V

    goto/16 :goto_0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 990
    :goto_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 991
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->firstElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/a/b/a/b$a;

    .line 992
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/Vector;->removeElementAt(I)V

    .line 993
    iget-byte v1, v0, Lcom/nuance/a/a/a/a/b/a/b$a;->a:B

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 995
    :pswitch_1
    const/4 v1, 0x1

    iget-object v0, v0, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    invoke-direct {p0, v1, v0}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(BLjava/lang/Object;)V

    goto :goto_0

    .line 998
    :pswitch_2
    const/4 v1, 0x2

    iget-object v0, v0, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    invoke-direct {p0, v1, v0}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(BLjava/lang/Object;)V

    goto :goto_0

    .line 1001
    :pswitch_3
    const/4 v1, 0x4

    iget-object v0, v0, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    invoke-direct {p0, v1, v0}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(BLjava/lang/Object;)V

    goto :goto_0

    .line 1004
    :pswitch_4
    const/4 v1, 0x5

    iget-object v0, v0, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    invoke-direct {p0, v1, v0}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(BLjava/lang/Object;)V

    goto :goto_0

    .line 1007
    :pswitch_5
    const/4 v1, 0x6

    iget-object v0, v0, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    invoke-direct {p0, v1, v0}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(BLjava/lang/Object;)V

    goto :goto_0

    .line 1010
    :pswitch_6
    const/4 v1, 0x7

    iget-object v0, v0, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    invoke-direct {p0, v1, v0}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(BLjava/lang/Object;)V

    goto :goto_0

    .line 1013
    :pswitch_7
    const/16 v1, 0x8

    iget-object v0, v0, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    invoke-direct {p0, v1, v0}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(BLjava/lang/Object;)V

    goto :goto_0

    .line 1017
    :cond_0
    return-void

    .line 993
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public final declared-synchronized a()I
    .locals 3

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    sget v0, Lcom/nuance/a/a/a/b/c/b/a/b;->p:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/nuance/a/a/a/b/c/b/a/b;->p:I

    .line 133
    sget v1, Lcom/nuance/a/a/a/b/c/b/a/b;->p:I

    const/high16 v2, -0x80000000

    if-ne v1, v2, :cond_0

    .line 134
    const/4 v1, 0x1

    sput v1, Lcom/nuance/a/a/a/b/c/b/a/b;->p:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    :cond_0
    monitor-exit p0

    return v0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(BILcom/nuance/a/a/a/b/c/b/a/c;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 243
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "freeResource, TID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", disconnect timeout: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 245
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0, p3}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 246
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 247
    const/4 v1, 0x0

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, p1}, Ljava/lang/Byte;-><init>(B)V

    aput-object v2, v0, v1

    .line 248
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p2}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v0, v1

    .line 249
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 250
    invoke-direct {p0, v3, v0}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(BLjava/lang/Object;)V

    .line 254
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    new-instance v2, Lcom/nuance/a/a/a/a/b/a/b$a;

    invoke-direct {v2, v3, v0}, Lcom/nuance/a/a/a/a/b/a/b$a;-><init>(BLjava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 277
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "vapRecordBegin, AID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 280
    :cond_0
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(I)V

    .line 281
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 282
    invoke-direct {p0, v3, v0}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(BLjava/lang/Object;)V

    .line 286
    :goto_0
    return-void

    .line 284
    :cond_1
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    new-instance v2, Lcom/nuance/a/a/a/a/b/a/b$a;

    invoke-direct {v2, v3, v0}, Lcom/nuance/a/a/a/a/b/a/b$a;-><init>(BLjava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(ILcom/nuance/a/a/a/b/b/a;Lcom/nuance/a/a/a/b/c/b/a/c;)V
    .locals 2

    .prologue
    .line 1029
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->c:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1030
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->d:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, p3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1031
    return-void
.end method

.method public final a(Lcom/nuance/a/a/a/a/a/a$a;Lcom/nuance/a/a/a/a/a/a$a;)V
    .locals 2

    .prologue
    .line 159
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "connect()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 162
    :cond_0
    iput-object p1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->n:Lcom/nuance/a/a/a/a/a/a$a;

    .line 163
    iput-object p2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->o:Lcom/nuance/a/a/a/a/a/a$a;

    .line 164
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(BLjava/lang/Object;)V

    .line 165
    return-void
.end method

.method public final a(Lcom/nuance/a/a/a/a/c/b;[B)V
    .locals 10

    .prologue
    const/16 v7, 0x10

    const/16 v8, 0x17

    const/16 v6, 0x11

    const/16 v4, 0x15

    const/4 v0, 0x0

    .line 834
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 835
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "xmodeMsgCallback, protocol: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-byte v3, p1, Lcom/nuance/a/a/a/a/c/b;->a:B

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", command: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v3, p1, Lcom/nuance/a/a/a/a/c/b;->c:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 837
    :cond_0
    iget-byte v1, p1, Lcom/nuance/a/a/a/a/c/b;->a:B

    packed-switch v1, :pswitch_data_0

    .line 915
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "Unknown Xmode protocol"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 919
    :cond_1
    :goto_0
    :sswitch_0
    return-void

    .line 839
    :pswitch_0
    iget-short v1, p1, Lcom/nuance/a/a/a/a/c/b;->c:S

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 841
    :sswitch_1
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->e:Lcom/nuance/a/a/a/b/a/a;

    iget-object v1, v1, Lcom/nuance/a/a/a/b/a/a;->e:[B

    iput-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    .line 844
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 845
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "connected("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    invoke-static {v3}, Lcom/nuance/a/a/a/b/c/b/a/b;->a([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") called on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->j:Lcom/nuance/a/a/a/b/c/a/c;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 847
    :cond_2
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->j:Lcom/nuance/a/a/a/b/c/a/c;

    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    invoke-static {v2}, Lcom/nuance/a/a/a/b/c/b/a/b;->a([B)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->s:Lcom/nuance/a/a/a/b/c/a/d;

    invoke-interface {v1, v2}, Lcom/nuance/a/a/a/b/c/a/c;->a(Ljava/lang/String;)V

    move v1, v0

    .line 849
    :goto_1
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 850
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/a/c;

    .line 851
    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    invoke-interface {v0, v2}, Lcom/nuance/a/a/a/b/c/b/a/c;->a([B)V

    .line 849
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 854
    :cond_3
    invoke-direct {p0}, Lcom/nuance/a/a/a/b/c/b/a/b;->h()V

    goto :goto_0

    .line 866
    :pswitch_1
    iget-short v1, p1, Lcom/nuance/a/a/a/a/c/b;->c:S

    sparse-switch v1, :sswitch_data_1

    .line 895
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "Unknown BCP command"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_0

    .line 868
    :sswitch_2
    invoke-static {p2, v6}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v1

    int-to-long v2, v1

    invoke-static {p2, v4}, Lcom/nuance/a/a/a/a/d/b;->a([BI)S

    invoke-static {p2, v8}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v1

    if-lez v1, :cond_4

    array-length v4, p2

    add-int/lit8 v4, v4, -0x1b

    if-gt v1, v4, :cond_4

    new-array v4, v1, [B

    const/16 v5, 0x1b

    invoke-static {p2, v5, v4, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->b:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 871
    :sswitch_3
    invoke-static {p2, v6}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v1

    int-to-long v2, v1

    invoke-static {p2, v4}, Lcom/nuance/a/a/a/a/d/b;->a([BI)S

    invoke-static {p2, v8}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v1

    if-lez v1, :cond_5

    array-length v4, p2

    add-int/lit8 v4, v4, -0x1b

    if-gt v1, v4, :cond_5

    new-array v4, v1, [B

    const/16 v5, 0x1b

    invoke-static {p2, v5, v4, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->b:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 874
    :sswitch_4
    const/4 v5, 0x0

    aget-byte v1, p2, v7

    invoke-static {p2, v6}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v2

    invoke-static {p2, v4}, Lcom/nuance/a/a/a/a/d/b;->a([BI)S

    move-result v4

    invoke-static {p2, v8}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v3

    if-lez v3, :cond_6

    array-length v6, p2

    add-int/lit8 v6, v6, -0x1b

    if-gt v3, v6, :cond_6

    new-array v5, v3, [B

    const/16 v6, 0x1b

    invoke-static {p2, v6, v5, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->b:Ljava/util/Hashtable;

    new-instance v3, Ljava/lang/Long;

    int-to-long v6, v2

    invoke-direct {v3, v6, v7}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v3}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/a/c;

    if-eqz v0, :cond_1

    int-to-long v2, v2

    invoke-interface/range {v0 .. v5}, Lcom/nuance/a/a/a/b/c/b/a/c;->b(BJS[B)V

    goto/16 :goto_0

    .line 877
    :sswitch_5
    const/4 v5, 0x0

    aget-byte v1, p2, v7

    invoke-static {p2, v6}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v2

    invoke-static {p2, v4}, Lcom/nuance/a/a/a/a/d/b;->a([BI)S

    move-result v4

    invoke-static {p2, v8}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v3

    if-lez v3, :cond_7

    array-length v6, p2

    add-int/lit8 v6, v6, -0x1b

    if-gt v3, v6, :cond_7

    new-array v5, v3, [B

    const/16 v6, 0x1b

    invoke-static {p2, v6, v5, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->b:Ljava/util/Hashtable;

    new-instance v3, Ljava/lang/Long;

    int-to-long v6, v2

    invoke-direct {v3, v6, v7}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v3}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/a/c;

    if-eqz v0, :cond_1

    int-to-long v2, v2

    invoke-interface/range {v0 .. v5}, Lcom/nuance/a/a/a/b/c/b/a/c;->a(BJS[B)V

    goto/16 :goto_0

    .line 880
    :sswitch_6
    invoke-static {p2, v6}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v0

    int-to-long v0, v0

    invoke-static {p2, v4}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    const/16 v2, 0x19

    invoke-static {p2, v2}, Lcom/nuance/a/a/a/a/d/b;->a([BI)S

    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->b:Ljava/util/Hashtable;

    new-instance v3, Ljava/lang/Long;

    invoke-direct {v3, v0, v1}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 883
    :sswitch_7
    invoke-direct {p0, p2}, Lcom/nuance/a/a/a/b/c/b/a/b;->b([B)V

    goto/16 :goto_0

    .line 886
    :sswitch_8
    aget-byte v1, p2, v7

    invoke-static {p2, v6}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v7

    invoke-static {p2, v4}, Lcom/nuance/a/a/a/a/d/b;->a([BI)S

    move-result v4

    invoke-static {p2, v8}, Lcom/nuance/a/a/a/a/d/b;->a([BI)S

    move-result v5

    const/16 v0, 0x19

    invoke-static {p2, v0}, Lcom/nuance/a/a/a/a/d/b;->a([BI)S

    move-result v6

    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->b:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Long;

    int-to-long v8, v7

    invoke-direct {v2, v8, v9}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/a/c;

    if-eqz v0, :cond_1

    int-to-long v2, v7

    invoke-interface/range {v0 .. v6}, Lcom/nuance/a/a/a/b/c/b/a/c;->a(BJSSS)V

    const/16 v0, 0xc8

    if-eq v4, v0, :cond_1

    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->b:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Long;

    int-to-long v2, v7

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 891
    :sswitch_9
    aget-byte v1, p2, v7

    invoke-static {p2, v4}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v2

    if-lez v2, :cond_1

    array-length v3, p2

    add-int/lit8 v3, v3, -0x19

    if-gt v2, v3, :cond_1

    new-array v3, v2, [B

    const/16 v4, 0x19

    invoke-static {p2, v4, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->b:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Long;

    iget-wide v4, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->q:J

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/a/c;

    if-eqz v0, :cond_1

    iget-wide v4, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->q:J

    invoke-interface {v0, v1, v3}, Lcom/nuance/a/a/a/b/c/b/a/c;->a(B[B)V

    goto/16 :goto_0

    .line 901
    :pswitch_2
    iget-short v1, p1, Lcom/nuance/a/a/a/a/c/b;->c:S

    sparse-switch v1, :sswitch_data_2

    goto/16 :goto_0

    .line 909
    :sswitch_a
    invoke-direct {p0, p2}, Lcom/nuance/a/a/a/b/c/b/a/b;->c([B)V

    goto/16 :goto_0

    .line 903
    :sswitch_b
    invoke-static {p2, v0}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v1

    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->d:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->d:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/a/c;

    if-nez v0, :cond_8

    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "parseVapPlayBegin:: Could not find the session listener associated with AID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_8
    invoke-interface {v0}, Lcom/nuance/a/a/a/b/c/b/a/c;->d()V

    goto/16 :goto_0

    .line 906
    :sswitch_c
    invoke-static {p2, v0}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v1

    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->c:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->c:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/b/a;

    if-nez v0, :cond_9

    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find the audio sink associated with AID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_9
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    :try_start_0
    invoke-interface {v0, v2, v3, v4, v5}, Lcom/nuance/a/a/a/b/b/a;->a([BIIZ)V
    :try_end_0
    .catch Lcom/nuance/a/a/a/b/b/f; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->d:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/a/c;

    if-nez v0, :cond_a

    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "parseXModeMsgVapPlayEnd:: Could not find the session listener associated with AID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/f;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_2

    :cond_a
    invoke-interface {v0}, Lcom/nuance/a/a/a/b/c/b/a/c;->f()V

    goto/16 :goto_0

    .line 837
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 839
    :sswitch_data_0
    .sparse-switch
        0x101 -> :sswitch_1
        0x200 -> :sswitch_0
        0x300 -> :sswitch_0
    .end sparse-switch

    .line 866
    :sswitch_data_1
    .sparse-switch
        0xa10 -> :sswitch_8
        0xa11 -> :sswitch_6
        0xa12 -> :sswitch_0
        0xa13 -> :sswitch_2
        0xa14 -> :sswitch_3
        0xa16 -> :sswitch_5
        0xa18 -> :sswitch_4
        0xa19 -> :sswitch_9
        0xa28 -> :sswitch_7
    .end sparse-switch

    .line 901
    :sswitch_data_2
    .sparse-switch
        0x200 -> :sswitch_a
        0x210 -> :sswitch_b
        0x400 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Lcom/nuance/a/a/a/b/c/a/d;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->s:Lcom/nuance/a/a/a/b/c/a/d;

    .line 129
    return-void
.end method

.method public final a(Lcom/nuance/a/a/a/b/c/b/a/c;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 146
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 922
    check-cast p1, Lcom/nuance/a/a/a/a/b/a/b$a;

    .line 925
    iget-byte v0, p1, Lcom/nuance/a/a/a/a/b/a/b$a;->a:B

    packed-switch v0, :pswitch_data_0

    .line 977
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "Unknown command"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 981
    :cond_0
    :goto_0
    return-void

    .line 927
    :pswitch_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->e:Lcom/nuance/a/a/a/b/a/a;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->n:Lcom/nuance/a/a/a/a/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/a/a$a;->a()S

    move-result v1

    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->o:Lcom/nuance/a/a/a/a/a/a$a;

    invoke-virtual {v2}, Lcom/nuance/a/a/a/a/a/a$a;->a()S

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/nuance/a/a/a/b/a/a;->a(SS)V

    goto :goto_0

    .line 930
    :pswitch_1
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->e:Lcom/nuance/a/a/a/b/a/a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/a/a;->b()V

    goto :goto_0

    .line 933
    :pswitch_2
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->e:Lcom/nuance/a/a/a/b/a/a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/a/a;->b()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->r:Z

    goto :goto_0

    .line 936
    :pswitch_3
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    if-eqz v0, :cond_0

    .line 937
    iget-object v0, p1, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 940
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Ljava/lang/Byte;

    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    const/4 v2, 0x1

    aget-object v0, v0, v2

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x5

    new-array v2, v2, [B

    const/4 v3, 0x0

    aput-byte v1, v2, v3

    const/4 v1, 0x1

    invoke-static {v0, v2, v1}, Lcom/nuance/a/a/a/a/d/b;->a(I[BI)V

    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    invoke-static {v2, v0}, Lcom/nuance/a/a/a/a/c/a;->a([B[B)[B

    move-result-object v0

    const/4 v1, 0x2

    const/16 v2, 0x22

    const/16 v3, 0xa29

    invoke-static {v1, v2, v3, v0}, Lcom/nuance/a/a/a/a/c/a;->a(BBS[B)[B

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->e:Lcom/nuance/a/a/a/b/a/a;

    const-string v2, "SEND_BCP_FREE_RESOURCE"

    invoke-virtual {v1, v0, v2}, Lcom/nuance/a/a/a/b/a/a;->a([BLjava/lang/Object;)V

    goto :goto_0

    .line 943
    :pswitch_4
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    if-eqz v0, :cond_0

    .line 944
    iget-object v0, p1, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->e:Lcom/nuance/a/a/a/b/a/a;

    invoke-virtual {v1, v0}, Lcom/nuance/a/a/a/b/a/a;->a(I)V

    goto :goto_0

    .line 949
    :pswitch_5
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    if-eqz v0, :cond_0

    .line 950
    iget-object v0, p1, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 953
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, [B

    const/4 v2, 0x1

    aget-object v0, v0, v2

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/a/c;

    invoke-interface {v0}, Lcom/nuance/a/a/a/b/c/b/a/c;->c()V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "[LATCHK] calling xmode.sendVapRecordMsg()"

    invoke-virtual {v0, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    :cond_2
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->e:Lcom/nuance/a/a/a/b/a/a;

    invoke-virtual {v0, v1, v3}, Lcom/nuance/a/a/a/b/a/a;->a([BI)V

    goto/16 :goto_0

    .line 956
    :pswitch_6
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    if-eqz v0, :cond_0

    .line 957
    iget-object v0, p1, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->e:Lcom/nuance/a/a/a/b/a/a;

    invoke-virtual {v1, v0}, Lcom/nuance/a/a/a/b/a/a;->b(I)V

    goto/16 :goto_0

    .line 962
    :pswitch_7
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    if-eqz v0, :cond_0

    .line 963
    iget-object v0, p1, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    move-object v4, v0

    check-cast v4, [Ljava/lang/Object;

    .line 966
    const/4 v0, 0x0

    aget-object v0, v4, v0

    check-cast v0, Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v8

    const/4 v0, 0x1

    aget-object v0, v4, v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    const/4 v0, 0x2

    aget-object v0, v4, v0

    move-object v6, v0

    check-cast v6, [B

    const/4 v0, 0x3

    aget-object v0, v4, v0

    move-object v7, v0

    check-cast v7, [B

    const/4 v0, 0x4

    aget-object v0, v4, v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    const/4 v0, 0x5

    aget-object v0, v4, v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v0, 0x6

    aget-object v0, v4, v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/a/c;

    const/4 v9, 0x7

    aget-object v4, v4, v9

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    iget-object v4, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->b:Ljava/util/Hashtable;

    new-instance v10, Ljava/lang/Long;

    invoke-direct {v10, v2, v3}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v4, v10, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    array-length v4, v6

    add-int/lit8 v4, v4, 0x5

    const/16 v10, 0xa19

    if-ne v8, v10, :cond_3

    add-int/lit8 v4, v4, 0x4

    :cond_3
    new-array v10, v4, [B

    const/4 v4, 0x0

    aput-byte v1, v10, v4

    const/4 v4, 0x1

    const/16 v11, 0xa19

    if-ne v8, v11, :cond_4

    long-to-int v4, v2

    const/4 v11, 0x1

    invoke-static {v4, v10, v11}, Lcom/nuance/a/a/a/a/d/b;->a(I[BI)V

    const/4 v4, 0x5

    :cond_4
    array-length v11, v6

    invoke-static {v11, v10, v4}, Lcom/nuance/a/a/a/a/d/b;->a(I[BI)V

    add-int/lit8 v4, v4, 0x4

    const/4 v11, 0x0

    array-length v12, v6

    invoke-static {v6, v11, v10, v4, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v4, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    :goto_2
    invoke-static {v10, v4}, Lcom/nuance/a/a/a/a/c/a;->a([B[B)[B

    move-result-object v4

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/4 v10, 0x0

    array-length v11, v4

    invoke-virtual {v6, v4, v10, v11}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    if-eqz v7, :cond_5

    const/4 v4, 0x0

    array-length v10, v7

    invoke-virtual {v6, v7, v4, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    :cond_5
    const/4 v4, 0x2

    const/16 v7, 0x22

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-static {v4, v7, v8, v6}, Lcom/nuance/a/a/a/a/c/a;->a(BBS[B)[B

    move-result-object v4

    iget-object v6, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->e:Lcom/nuance/a/a/a/b/a/a;

    invoke-virtual {v6, v4, v5}, Lcom/nuance/a/a/a/b/a/a;->a([BLjava/lang/Object;)V

    if-eqz v9, :cond_0

    const/16 v4, 0xa15

    if-ne v8, v4, :cond_7

    if-eqz v0, :cond_0

    const/16 v4, 0xc8

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/nuance/a/a/a/b/c/b/a/c;->a(BJS[B)V

    goto/16 :goto_0

    :cond_6
    sget-object v4, Lcom/nuance/a/a/a/b/c/b/a/b;->m:[B

    goto :goto_2

    :cond_7
    const/16 v1, 0xa30

    if-ne v8, v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    invoke-interface {v0}, Lcom/nuance/a/a/a/b/c/b/a/c;->b()V

    goto/16 :goto_0

    .line 925
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a(SLjava/lang/String;[B[BBJLcom/nuance/a/a/a/b/c/b/a/c;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 341
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "postBcpMessage, BCP: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", TID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", RID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6, p7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 344
    :cond_0
    new-array v0, v3, [Ljava/lang/Object;

    .line 345
    const/4 v1, 0x0

    new-instance v2, Ljava/lang/Short;

    invoke-direct {v2, p1}, Ljava/lang/Short;-><init>(S)V

    aput-object v2, v0, v1

    .line 346
    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 347
    const/4 v1, 0x2

    aput-object p3, v0, v1

    .line 348
    const/4 v1, 0x3

    aput-object p4, v0, v1

    .line 349
    const/4 v1, 0x4

    new-instance v2, Ljava/lang/Byte;

    invoke-direct {v2, p5}, Ljava/lang/Byte;-><init>(B)V

    aput-object v2, v0, v1

    .line 350
    const/4 v1, 0x5

    new-instance v2, Ljava/lang/Long;

    invoke-direct {v2, p6, p7}, Ljava/lang/Long;-><init>(J)V

    aput-object v2, v0, v1

    .line 351
    const/4 v1, 0x6

    aput-object p8, v0, v1

    .line 352
    const/4 v1, 0x7

    new-instance v2, Ljava/lang/Boolean;

    invoke-direct {v2, p9}, Ljava/lang/Boolean;-><init>(Z)V

    aput-object v2, v0, v1

    .line 353
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 354
    invoke-direct {p0, v3, v0}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(BLjava/lang/Object;)V

    .line 358
    :goto_0
    return-void

    .line 356
    :cond_1
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    new-instance v2, Lcom/nuance/a/a/a/a/b/a/b$a;

    invoke-direct {v2, v3, v0}, Lcom/nuance/a/a/a/a/b/a/b$a;-><init>(BLjava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(SS)V
    .locals 3

    .prologue
    .line 203
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "socketClosed, reason: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 206
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->r:Z

    if-eqz v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->i:Lcom/nuance/a/a/a/a/b/a/b;

    invoke-interface {v0}, Lcom/nuance/a/a/a/a/b/a/b;->c()V

    .line 211
    :cond_1
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 212
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "socketClosed() sessionListeners.size():"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 214
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 215
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/a/c;

    .line 216
    invoke-interface {v0, p1}, Lcom/nuance/a/a/a/b/c/b/a/c;->a(S)V

    .line 214
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 218
    :cond_3
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    .line 219
    :cond_4
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    .line 221
    const/4 v0, 0x4

    if-eq p1, v0, :cond_5

    const/4 v0, 0x5

    if-eq p1, v0, :cond_5

    const/4 v0, 0x7

    if-ne p1, v0, :cond_7

    .line 224
    :cond_5
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->j:Lcom/nuance/a/a/a/b/c/a/c;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->s:Lcom/nuance/a/a/a/b/c/a/d;

    invoke-interface {v0}, Lcom/nuance/a/a/a/b/c/a/c;->a()V

    .line 237
    :cond_6
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    .line 238
    return-void

    .line 227
    :cond_7
    const/16 v0, 0x8

    if-ne p1, v0, :cond_8

    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    if-nez v0, :cond_8

    .line 228
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->j:Lcom/nuance/a/a/a/b/c/a/c;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->s:Lcom/nuance/a/a/a/b/c/a/d;

    invoke-interface {v0}, Lcom/nuance/a/a/a/b/c/a/c;->a()V

    goto :goto_1

    .line 232
    :cond_8
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    if-eqz v0, :cond_6

    .line 233
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->j:Lcom/nuance/a/a/a/b/c/a/c;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->s:Lcom/nuance/a/a/a/b/c/a/d;

    invoke-interface {v0, p2}, Lcom/nuance/a/a/a/b/c/a/c;->a(S)V

    goto :goto_1
.end method

.method public final a([BI)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 295
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "vapRecord, AID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 298
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 299
    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 300
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p2}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v0, v1

    .line 301
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 302
    invoke-direct {p0, v3, v0}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(BLjava/lang/Object;)V

    .line 306
    :goto_0
    return-void

    .line 304
    :cond_1
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    new-instance v2, Lcom/nuance/a/a/a/a/b/a/b$a;

    invoke-direct {v2, v3, v0}, Lcom/nuance/a/a/a/a/b/a/b$a;-><init>(BLjava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 153
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->q:J

    .line 154
    return-void
.end method

.method public final b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x7

    .line 323
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "vapRecordEnd, AID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 326
    :cond_0
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(I)V

    .line 327
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 328
    invoke-direct {p0, v3, v0}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(BLjava/lang/Object;)V

    .line 332
    :goto_0
    return-void

    .line 330
    :cond_1
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->k:Ljava/util/Vector;

    new-instance v2, Lcom/nuance/a/a/a/a/b/a/b$a;

    invoke-direct {v2, v3, v0}, Lcom/nuance/a/a/a/a/b/a/b$a;-><init>(BLjava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Lcom/nuance/a/a/a/b/c/b/a/c;)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->h:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 150
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 185
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "disconnectAndShutdown"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 189
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(BLjava/lang/Object;)V

    .line 190
    return-void
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 1034
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->l:[B

    return-object v0
.end method

.method public final e()J
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    .line 1039
    iget-wide v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->t:J

    add-long v2, v0, v6

    iput-wide v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->t:J

    .line 1040
    iget-wide v2, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->t:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 1041
    iput-wide v6, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->t:J

    .line 1043
    :cond_0
    return-wide v0
.end method

.method public final f()B
    .locals 3

    .prologue
    .line 1055
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->u:B

    add-int/lit8 v1, v0, 0x1

    int-to-byte v1, v1

    iput-byte v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->u:B

    .line 1061
    iget-byte v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->u:B

    const/16 v2, -0x80

    if-ne v1, v2, :cond_0

    .line 1062
    const/4 v1, 0x1

    iput-byte v1, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->u:B

    .line 1068
    :cond_0
    return v0
.end method

.method public final g()Lcom/nuance/a/a/a/a/a/a$a;
    .locals 1

    .prologue
    .line 1072
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/b;->n:Lcom/nuance/a/a/a/a/a/a$a;

    return-object v0
.end method
