.class public interface abstract Lcom/nuance/a/a/a/b/c/c/b;
.super Ljava/lang/Object;
.source "Command.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/a/a/a/b/c/c/b$a;
    }
.end annotation


# static fields
.field public static final b:Lcom/nuance/a/a/a/b/c/c/b$a;

.field public static final c:Lcom/nuance/a/a/a/b/c/c/b$a;

.field public static final d:Lcom/nuance/a/a/a/b/c/c/b$a;

.field public static final e:Lcom/nuance/a/a/a/b/c/c/b$a;

.field public static final f:Lcom/nuance/a/a/a/b/c/c/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 125
    new-instance v0, Lcom/nuance/a/a/a/b/c/c/b$a;

    const-string v1, "ABORT_END"

    invoke-direct {v0, v1, v2}, Lcom/nuance/a/a/a/b/c/c/b$a;-><init>(Ljava/lang/String;B)V

    sput-object v0, Lcom/nuance/a/a/a/b/c/c/b;->b:Lcom/nuance/a/a/a/b/c/c/b$a;

    .line 131
    new-instance v0, Lcom/nuance/a/a/a/b/c/c/b$a;

    const-string v1, "ABORT_BACK"

    invoke-direct {v0, v1, v2}, Lcom/nuance/a/a/a/b/c/c/b$a;-><init>(Ljava/lang/String;B)V

    sput-object v0, Lcom/nuance/a/a/a/b/c/c/b;->c:Lcom/nuance/a/a/a/b/c/c/b$a;

    .line 136
    new-instance v0, Lcom/nuance/a/a/a/b/c/c/b$a;

    const-string v1, "ABORT_NEW"

    invoke-direct {v0, v1, v2}, Lcom/nuance/a/a/a/b/c/c/b$a;-><init>(Ljava/lang/String;B)V

    sput-object v0, Lcom/nuance/a/a/a/b/c/c/b;->d:Lcom/nuance/a/a/a/b/c/c/b$a;

    .line 141
    new-instance v0, Lcom/nuance/a/a/a/b/c/c/b$a;

    const-string v1, "STOPPED_TOO_SOON"

    invoke-direct {v0, v1, v2}, Lcom/nuance/a/a/a/b/c/c/b$a;-><init>(Ljava/lang/String;B)V

    sput-object v0, Lcom/nuance/a/a/a/b/c/c/b;->e:Lcom/nuance/a/a/a/b/c/c/b$a;

    .line 147
    new-instance v0, Lcom/nuance/a/a/a/b/c/c/b$a;

    const-string v1, "PREEMPTED"

    invoke-direct {v0, v1, v2}, Lcom/nuance/a/a/a/b/c/c/b$a;-><init>(Ljava/lang/String;B)V

    sput-object v0, Lcom/nuance/a/a/a/b/c/c/b;->f:Lcom/nuance/a/a/a/b/c/c/b$a;

    return-void
.end method


# virtual methods
.method public abstract a()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/a/a/a/b/b/d;,
            Lcom/nuance/a/a/a/b/b/e;
        }
    .end annotation
.end method

.method public abstract a(Lcom/nuance/a/a/a/b/c/c/h;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/a/a/a/b/b/d;,
            Lcom/nuance/a/a/a/b/b/e;
        }
    .end annotation
.end method
