.class public Lcom/nuance/a/a/a/c/g;
.super Ljava/lang/Object;
.source "NetworkSystemOEM.java"

# interfaces
.implements Lcom/nuance/a/a/a/a/b/a/b$b;
.implements Lcom/nuance/a/a/a/a/b/a/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/a/a/a/c/g$c;,
        Lcom/nuance/a/a/a/c/g$b;,
        Lcom/nuance/a/a/a/c/g$a;
    }
.end annotation


# static fields
.field private static final a:Lcom/nuance/a/a/a/a/b/a/a$a;

.field private static final c:Ljava/lang/Integer;

.field private static final d:Ljava/lang/Integer;

.field private static final e:Ljava/lang/Integer;

.field private static final f:Ljava/lang/Integer;


# instance fields
.field private b:Lcom/nuance/a/a/a/a/b/a/b;

.field private g:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 54
    const-class v0, Lcom/nuance/a/a/a/c/g;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    .line 59
    new-instance v0, Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    sput-object v0, Lcom/nuance/a/a/a/c/g;->c:Ljava/lang/Integer;

    .line 60
    new-instance v0, Ljava/lang/Integer;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    sput-object v0, Lcom/nuance/a/a/a/c/g;->d:Ljava/lang/Integer;

    .line 61
    new-instance v0, Ljava/lang/Integer;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    sput-object v0, Lcom/nuance/a/a/a/c/g;->e:Ljava/lang/Integer;

    .line 62
    new-instance v0, Ljava/lang/Integer;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    sput-object v0, Lcom/nuance/a/a/a/c/g;->f:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/nuance/a/a/a/a/b/a/b;)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/a/a/a/c/g;->b:Lcom/nuance/a/a/a/a/b/a/b;

    .line 64
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/nuance/a/a/a/c/g;->g:Ljava/lang/Object;

    .line 84
    iput-object p1, p0, Lcom/nuance/a/a/a/c/g;->b:Lcom/nuance/a/a/a/a/b/a/b;

    .line 85
    return-void
.end method

.method static synthetic a()Lcom/nuance/a/a/a/a/b/a/a$a;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-object v0
.end method

.method static synthetic a(Lcom/nuance/a/a/a/c/g;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/nuance/a/a/a/c/g;->g:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lcom/nuance/a/a/a/c/g;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/nuance/a/a/a/c/g;->a([Ljava/lang/Object;)V

    return-void
.end method

.method private a([Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 88
    iget-object v0, p0, Lcom/nuance/a/a/a/c/g;->b:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/c/g;->b:Lcom/nuance/a/a/a/a/b/a/b;

    invoke-interface {v1}, Lcom/nuance/a/a/a/a/b/a/b;->b()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/a/a/a/c/g;->b:Lcom/nuance/a/a/a/a/b/a/b;

    invoke-interface {v2}, Lcom/nuance/a/a/a/a/b/a/b;->a()[Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-interface {v0, p1, p0, v1, v2}, Lcom/nuance/a/a/a/a/b/a/b;->a(Ljava/lang/Object;Lcom/nuance/a/a/a/a/b/a/b$b;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 89
    return-void
.end method

.method static synthetic b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/nuance/a/a/a/c/g;->c:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/nuance/a/a/a/c/g;->e:Ljava/lang/Integer;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/nuance/a/a/a/a/b/a/d$b;[BILcom/nuance/a/a/a/a/b/a/d$e;Ljava/lang/Object;)Lcom/nuance/a/a/a/a/b/a/d$c;
    .locals 7

    .prologue
    .line 240
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$b;->b:Lcom/nuance/a/a/a/a/b/a/d$b;

    if-eq p2, v0, :cond_1

    .line 242
    sget-object v0, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    sget-object v0, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "Blackberry NetworkSystem only supports NETWORK_READ_FULL"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 245
    :cond_0
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->b:Lcom/nuance/a/a/a/a/b/a/d$c;

    .line 266
    :goto_0
    return-object v0

    .line 247
    :cond_1
    check-cast p1, Lcom/nuance/a/a/a/c/g$a;

    .line 248
    iget-object v0, p1, Lcom/nuance/a/a/a/c/g$a;->f:Lcom/nuance/a/a/a/c/g$c;

    if-eqz v0, :cond_4

    .line 249
    iget-object v0, p1, Lcom/nuance/a/a/a/c/g$a;->f:Lcom/nuance/a/a/a/c/g$c;

    invoke-static {v0}, Lcom/nuance/a/a/a/c/g$c;->a(Lcom/nuance/a/a/a/c/g$c;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 250
    new-instance v0, Lcom/nuance/a/a/a/c/g$b;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/nuance/a/a/a/c/g$b;-><init>(Lcom/nuance/a/a/a/c/g;Lcom/nuance/a/a/a/a/b/a/d$b;[BILcom/nuance/a/a/a/a/b/a/d$e;Ljava/lang/Object;)V

    .line 251
    iget-object v1, p1, Lcom/nuance/a/a/a/c/g$a;->f:Lcom/nuance/a/a/a/c/g$c;

    invoke-virtual {v1, v0}, Lcom/nuance/a/a/a/c/g$c;->a(Lcom/nuance/a/a/a/c/g$b;)Lcom/nuance/a/a/a/a/b/a/d$c;

    move-result-object v0

    goto :goto_0

    .line 255
    :cond_2
    sget-object v0, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 256
    sget-object v0, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "socket read thread is stopping"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 258
    :cond_3
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->b:Lcom/nuance/a/a/a/a/b/a/d$c;

    goto :goto_0

    .line 263
    :cond_4
    sget-object v0, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 264
    sget-object v0, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "SOCKET READ ERROR: socket read thread is null"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 266
    :cond_5
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->b:Lcom/nuance/a/a/a/a/b/a/d$c;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;[BILcom/nuance/a/a/a/a/b/a/d$f;Ljava/lang/Object;)Lcom/nuance/a/a/a/a/b/a/d$c;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 275
    sget-object v0, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276
    sget-object v0, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "socketWrite(bufferLen:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") start"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    :cond_0
    move-object v0, p1

    .line 279
    check-cast v0, Lcom/nuance/a/a/a/c/g$a;

    .line 280
    iget-object v1, v0, Lcom/nuance/a/a/a/c/g$a;->a:Ljava/net/Socket;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/nuance/a/a/a/c/g$a;->d:Ljava/io/OutputStream;

    if-nez v1, :cond_2

    .line 281
    :cond_1
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->b:Lcom/nuance/a/a/a/a/b/a/d$c;

    .line 303
    :goto_0
    return-object v0

    .line 284
    :cond_2
    iget-object v0, v0, Lcom/nuance/a/a/a/c/g$a;->d:Ljava/io/OutputStream;

    .line 286
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, p2, v1, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 287
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    sget-object v1, Lcom/nuance/a/a/a/c/g;->f:Ljava/lang/Integer;

    aput-object v1, v0, v4

    aput-object p4, v0, v5

    sget-object v1, Lcom/nuance/a/a/a/a/b/a/d$c;->a:Lcom/nuance/a/a/a/a/b/a/d$c;

    aput-object v1, v0, v6

    aput-object p1, v0, v7

    aput-object p2, v0, v8

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p3}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p3}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    aput-object p5, v0, v1

    invoke-direct {p0, v0}, Lcom/nuance/a/a/a/c/g;->a([Ljava/lang/Object;)V

    .line 300
    sget-object v0, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 301
    sget-object v0, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "socketWrite(bufferLen:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") end"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 303
    :cond_3
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->a:Lcom/nuance/a/a/a/a/b/a/d$c;

    goto :goto_0

    .line 288
    :catch_0
    move-exception v0

    .line 290
    sget-object v1, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 291
    sget-object v1, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Socket Write Exception - ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] Message - ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 293
    :cond_4
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    sget-object v1, Lcom/nuance/a/a/a/c/g;->f:Ljava/lang/Integer;

    aput-object v1, v0, v4

    aput-object p4, v0, v5

    sget-object v1, Lcom/nuance/a/a/a/a/b/a/d$c;->b:Lcom/nuance/a/a/a/a/b/a/d$c;

    aput-object v1, v0, v6

    aput-object p1, v0, v7

    aput-object p2, v0, v8

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p3}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v4}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    aput-object p5, v0, v1

    invoke-direct {p0, v0}, Lcom/nuance/a/a/a/c/g;->a([Ljava/lang/Object;)V

    .line 294
    invoke-virtual {p0, p1}, Lcom/nuance/a/a/a/c/g;->a(Ljava/lang/Object;)V

    .line 295
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->b:Lcom/nuance/a/a/a/a/b/a/d$c;

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 197
    move-object v0, p1

    check-cast v0, Lcom/nuance/a/a/a/c/g$a;

    .line 200
    if-nez v0, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 203
    :cond_1
    iget-object v4, p0, Lcom/nuance/a/a/a/c/g;->g:Ljava/lang/Object;

    monitor-enter v4

    .line 204
    :try_start_0
    iget-object v1, v0, Lcom/nuance/a/a/a/c/g$a;->f:Lcom/nuance/a/a/a/c/g$c;

    if-nez v1, :cond_2

    .line 205
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 212
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 207
    :cond_2
    :try_start_1
    iget-object v1, v0, Lcom/nuance/a/a/a/c/g$a;->f:Lcom/nuance/a/a/a/c/g$c;

    invoke-static {v1}, Lcom/nuance/a/a/a/c/g$c;->a(Lcom/nuance/a/a/a/c/g$c;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 209
    iget-object v1, v0, Lcom/nuance/a/a/a/c/g$a;->f:Lcom/nuance/a/a/a/c/g$c;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/c/g$c;->a()V

    .line 210
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/nuance/a/a/a/c/g$a;->f:Lcom/nuance/a/a/a/c/g$c;

    move v1, v2

    .line 212
    :goto_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214
    iget-object v4, v0, Lcom/nuance/a/a/a/c/g$a;->a:Ljava/net/Socket;

    if-eqz v4, :cond_0

    .line 217
    if-eqz v1, :cond_0

    .line 219
    :try_start_2
    iget-object v1, v0, Lcom/nuance/a/a/a/c/g$a;->c:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 220
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/nuance/a/a/a/c/g$a;->c:Ljava/io/InputStream;

    .line 221
    iget-object v1, v0, Lcom/nuance/a/a/a/c/g$a;->a:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    .line 222
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/nuance/a/a/a/c/g$a;->a:Ljava/net/Socket;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 231
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v4, Lcom/nuance/a/a/a/c/g;->d:Ljava/lang/Integer;

    aput-object v4, v1, v3

    iget-object v3, v0, Lcom/nuance/a/a/a/c/g$a;->b:Lcom/nuance/a/a/a/a/b/a/d$a;

    aput-object v3, v1, v2

    sget-object v2, Lcom/nuance/a/a/a/a/b/a/d$c;->a:Lcom/nuance/a/a/a/a/b/a/d$c;

    aput-object v2, v1, v7

    aput-object p1, v1, v8

    const/4 v2, 0x4

    iget-object v0, v0, Lcom/nuance/a/a/a/c/g$a;->e:Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-direct {p0, v1}, Lcom/nuance/a/a/a/c/g;->a([Ljava/lang/Object;)V

    goto :goto_0

    .line 223
    :catch_0
    move-exception v1

    .line 225
    sget-object v4, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v4}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 226
    sget-object v4, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Socket Close Expception - ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] Message - ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "]"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 228
    :cond_3
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v4, Lcom/nuance/a/a/a/c/g;->d:Ljava/lang/Integer;

    aput-object v4, v1, v3

    iget-object v3, v0, Lcom/nuance/a/a/a/c/g$a;->b:Lcom/nuance/a/a/a/a/b/a/d$a;

    aput-object v3, v1, v2

    sget-object v2, Lcom/nuance/a/a/a/a/b/a/d$c;->b:Lcom/nuance/a/a/a/a/b/a/d$c;

    aput-object v2, v1, v7

    aput-object p1, v1, v8

    const/4 v2, 0x4

    iget-object v0, v0, Lcom/nuance/a/a/a/c/g$a;->e:Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-direct {p0, v1}, Lcom/nuance/a/a/a/c/g;->a([Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_4
    move v1, v3

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 92
    check-cast p1, [Ljava/lang/Object;

    .line 93
    aget-object v0, p1, v2

    sget-object v1, Lcom/nuance/a/a/a/c/g;->c:Ljava/lang/Integer;

    if-ne v0, v1, :cond_1

    .line 94
    aget-object v0, p1, v3

    check-cast v0, Lcom/nuance/a/a/a/a/b/a/d$d;

    aget-object v1, p1, v4

    check-cast v1, Lcom/nuance/a/a/a/a/b/a/d$c;

    aget-object v2, p1, v5

    invoke-interface {v0, v1, v2}, Lcom/nuance/a/a/a/a/b/a/d$d;->a(Lcom/nuance/a/a/a/a/b/a/d$c;Ljava/lang/Object;)V

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    aget-object v0, p1, v2

    sget-object v1, Lcom/nuance/a/a/a/c/g;->d:Ljava/lang/Integer;

    if-ne v0, v1, :cond_2

    .line 97
    aget-object v0, p1, v3

    check-cast v0, Lcom/nuance/a/a/a/a/b/a/d$a;

    invoke-interface {v0}, Lcom/nuance/a/a/a/a/b/a/d$a;->a()V

    goto :goto_0

    .line 99
    :cond_2
    aget-object v0, p1, v2

    sget-object v1, Lcom/nuance/a/a/a/c/g;->e:Ljava/lang/Integer;

    if-ne v0, v1, :cond_3

    .line 100
    aget-object v0, p1, v3

    check-cast v0, Lcom/nuance/a/a/a/a/b/a/d$e;

    aget-object v1, p1, v4

    check-cast v1, Lcom/nuance/a/a/a/a/b/a/d$c;

    aget-object v2, p1, v5

    const/4 v3, 0x4

    aget-object v3, p1, v3

    check-cast v3, [B

    aget-object v4, p1, v6

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x7

    aget-object v5, p1, v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v6, 0x8

    aget-object v6, p1, v6

    invoke-interface/range {v0 .. v6}, Lcom/nuance/a/a/a/a/b/a/d$e;->a(Lcom/nuance/a/a/a/a/b/a/d$c;Ljava/lang/Object;[BIILjava/lang/Object;)V

    goto :goto_0

    .line 104
    :cond_3
    aget-object v0, p1, v2

    sget-object v1, Lcom/nuance/a/a/a/c/g;->f:Ljava/lang/Integer;

    if-ne v0, v1, :cond_0

    .line 105
    aget-object v0, p1, v3

    check-cast v0, Lcom/nuance/a/a/a/a/b/a/d$f;

    aget-object v1, p1, v4

    check-cast v1, Lcom/nuance/a/a/a/a/b/a/d$c;

    aget-object v2, p1, v5

    aget-object v3, p1, v6

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x7

    aget-object v4, p1, v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x8

    aget-object v5, p1, v5

    invoke-interface/range {v0 .. v5}, Lcom/nuance/a/a/a/a/b/a/d$f;->a(Lcom/nuance/a/a/a/a/b/a/d$c;Ljava/lang/Object;IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ILcom/nuance/a/a/a/a/b/a/d$d;Lcom/nuance/a/a/a/a/b/a/d$a;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 117
    new-instance v0, Lcom/nuance/a/a/a/c/g$c;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/nuance/a/a/a/c/g$c;-><init>(Lcom/nuance/a/a/a/c/g;Ljava/lang/String;ILcom/nuance/a/a/a/a/b/a/d$d;Lcom/nuance/a/a/a/a/b/a/d$a;)V

    .line 118
    :try_start_0
    invoke-virtual {v0}, Lcom/nuance/a/a/a/c/g$c;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :goto_0
    return-void

    .line 131
    :catch_0
    move-exception v0

    .line 133
    sget-object v1, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 134
    sget-object v1, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Open Socket Exception - ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] Message - ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 136
    :cond_0
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    sget-object v2, Lcom/nuance/a/a/a/c/g;->c:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p3, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/nuance/a/a/a/a/b/a/d$c;->b:Lcom/nuance/a/a/a/a/b/a/d$c;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object v6, v0, v1

    const/4 v1, 0x4

    aput-object v6, v0, v1

    invoke-direct {p0, v0}, Lcom/nuance/a/a/a/c/g;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ILjava/util/Vector;Lcom/nuance/a/a/a/a/b/a/d$d;Lcom/nuance/a/a/a/a/b/a/d$a;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 146
    new-instance v4, Lcom/nuance/a/a/a/c/b/a/c;

    invoke-direct {v4}, Lcom/nuance/a/a/a/c/b/a/c;-><init>()V

    .line 147
    invoke-virtual {p3}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    .line 148
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 149
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/b/c;

    .line 150
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SSL_SelfSigned_Cert"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 152
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->b()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    const-string v3, "TRUE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->b()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 154
    :cond_1
    iput-boolean v7, v4, Lcom/nuance/a/a/a/c/b/a/c;->a:Z

    .line 157
    :cond_2
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SSL_Cert_Summary"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 159
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->b()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    iput-object v2, v4, Lcom/nuance/a/a/a/c/b/a/c;->b:Ljava/lang/String;

    .line 162
    :cond_3
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SSL_Cert_Data"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 164
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->b()[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    iput-object v2, v4, Lcom/nuance/a/a/a/c/b/a/c;->c:Ljava/lang/String;

    goto :goto_0

    .line 168
    :cond_4
    new-instance v0, Lcom/nuance/a/a/a/c/g$c;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/nuance/a/a/a/c/g$c;-><init>(Lcom/nuance/a/a/a/c/g;Ljava/lang/String;ILcom/nuance/a/a/a/c/b/a/c;Lcom/nuance/a/a/a/a/b/a/d$d;Lcom/nuance/a/a/a/a/b/a/d$a;)V

    .line 169
    :try_start_0
    invoke-virtual {v0}, Lcom/nuance/a/a/a/c/g$c;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :goto_1
    return-void

    .line 181
    :catch_0
    move-exception v0

    .line 183
    sget-object v1, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 184
    sget-object v1, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Open Socket Exception - ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] Message - ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 186
    :cond_5
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    sget-object v2, Lcom/nuance/a/a/a/c/g;->c:Ljava/lang/Integer;

    aput-object v2, v0, v1

    aput-object p4, v0, v7

    const/4 v1, 0x2

    sget-object v2, Lcom/nuance/a/a/a/a/b/a/d$c;->b:Lcom/nuance/a/a/a/a/b/a/d$c;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object v8, v0, v1

    const/4 v1, 0x4

    aput-object v8, v0, v1

    invoke-direct {p0, v0}, Lcom/nuance/a/a/a/c/g;->a([Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 311
    move-object v0, p1

    check-cast v0, Lcom/nuance/a/a/a/c/g$a;

    .line 312
    iget-object v1, v0, Lcom/nuance/a/a/a/c/g$a;->f:Lcom/nuance/a/a/a/c/g$c;

    if-eqz v1, :cond_0

    .line 313
    iget-object v0, v0, Lcom/nuance/a/a/a/c/g$a;->f:Lcom/nuance/a/a/a/c/g$c;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/c/g$c;->b()V

    .line 321
    :goto_0
    return-void

    .line 316
    :cond_0
    sget-object v0, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 317
    sget-object v0, Lcom/nuance/a/a/a/c/g;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "SOCKET WRITE ERROR: socket read thread is null"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 319
    :cond_1
    invoke-virtual {p0, p1}, Lcom/nuance/a/a/a/c/g;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
