.class public Lcom/nuance/a/a/a/b/c/b/b/a;
.super Lcom/nuance/a/a/a/b/c/b/a/d;
.source "NMASResourceImpl.java"

# interfaces
.implements Lcom/nuance/a/a/a/b/c/c/d;


# static fields
.field private static final k:Lcom/nuance/a/a/a/a/b/a/a$a;

.field private static o:Ljava/lang/String;


# instance fields
.field private l:Lcom/nuance/a/a/a/a/b/a/b;

.field private m:Lcom/nuance/a/a/a/b/c/c/f;

.field private n:Lcom/nuance/a/a/a/b/c/b/b/z;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/nuance/a/a/a/b/c/b/b/a;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/b/c/b/b/a;->k:Lcom/nuance/a/a/a/a/b/a/a$a;

    .line 468
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sput-object v0, Lcom/nuance/a/a/a/b/c/b/b/a;->o:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/nuance/a/a/a/b/c/b/a/a;Lcom/nuance/a/a/a/b/c/c/f;Ljava/util/Vector;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 71
    invoke-direct {p0, p1, p2, p3}, Lcom/nuance/a/a/a/b/c/b/a/d;-><init>(Lcom/nuance/a/a/a/b/c/b/a/a;Lcom/nuance/a/a/a/b/c/a/f;Ljava/util/Vector;)V

    .line 73
    iput-object p2, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->m:Lcom/nuance/a/a/a/b/c/c/f;

    .line 74
    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/a/a;->a()Lcom/nuance/a/a/a/a/b/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->l:Lcom/nuance/a/a/a/a/b/a/b;

    .line 78
    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/a/a;->g()Lcom/nuance/a/a/a/b/c/b/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->a:Lcom/nuance/a/a/a/b/c/b/a/b;

    .line 80
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->a:Lcom/nuance/a/a/a/b/c/b/a/b;

    invoke-virtual {v0, p0}, Lcom/nuance/a/a/a/b/c/b/a/b;->a(Lcom/nuance/a/a/a/b/c/a/d;)V

    .line 81
    iput-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    .line 83
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->l:Lcom/nuance/a/a/a/a/b/a/b;

    new-instance v1, Lcom/nuance/a/a/a/a/b/a/b$a;

    invoke-direct {v1, v4, v2}, Lcom/nuance/a/a/a/a/b/a/b$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->l:Lcom/nuance/a/a/a/a/b/a/b;

    invoke-interface {v3}, Lcom/nuance/a/a/a/a/b/a/b;->a()[Ljava/lang/Object;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-interface {v0, v1, p0, v2, v3}, Lcom/nuance/a/a/a/a/b/a/b;->a(Ljava/lang/Object;Lcom/nuance/a/a/a/a/b/a/b$b;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 85
    return-void
.end method

.method private a(B)V
    .locals 4

    .prologue
    .line 360
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    iget-byte v0, v0, Lcom/nuance/a/a/a/b/c/b/b/z;->a:B

    if-ne p1, v0, :cond_2

    .line 361
    invoke-virtual {p0}, Lcom/nuance/a/a/a/b/c/b/b/a;->g()Lcom/nuance/a/a/a/b/c/a/a;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/a/a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/a/a;->h()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    .line 362
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 364
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/b/a;->k:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "clearResLogsToServer() before clean the log vector tranId["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] log list size ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 366
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 367
    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/a/a/a/b/c/b/b/z$a;

    .line 368
    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/c/b/b/z$a;->e()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 369
    invoke-virtual {v0, v1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    goto :goto_0

    .line 371
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    .line 372
    goto :goto_0

    .line 374
    :cond_1
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/b/a;->k:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "clearResLogsToServer() after clean the log vector tranId["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] log list size ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 378
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/nuance/a/a/a/b/c/c/a;
    .locals 3

    .prologue
    .line 160
    if-nez p1, :cond_0

    .line 161
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_0
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/c;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->a:Lcom/nuance/a/a/a/b/c/b/a/b;

    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->l:Lcom/nuance/a/a/a/a/b/a/b;

    invoke-direct {v0, p1, v1, v2}, Lcom/nuance/a/a/a/b/c/b/b/c;-><init>(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/b/a/b;Lcom/nuance/a/a/a/a/b/a/b;)V

    return-object v0
.end method

.method public final a(Lcom/nuance/a/a/a/b/c/c/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/a/a/a/b/c/c/c;J)Lcom/nuance/a/a/a/b/c/c/b;
    .locals 28
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/a/a/a/b/c/a/e;
        }
    .end annotation

    .prologue
    .line 93
    const-string v2, ""

    .line 94
    if-nez p1, :cond_0

    .line 95
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "commandListener is invalid; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 96
    :cond_0
    if-eqz p2, :cond_1

    const-string v3, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 97
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "cmd should be non-null; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 98
    :cond_2
    if-eqz p3, :cond_3

    const-string v3, ""

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 99
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "scriptVersion should be non-null; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 100
    :cond_4
    if-eqz p4, :cond_5

    const-string v3, ""

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 101
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "dictationLanguage should be non-null; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 105
    :cond_6
    if-eqz p6, :cond_7

    const-string v3, ""

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 106
    :cond_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "phoneModel should be non-null; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 107
    :cond_8
    const-wide/16 v4, 0x0

    cmp-long v3, p8, v4

    if-gtz v3, :cond_9

    .line 108
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "commandTimeout is invalid; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 110
    :cond_9
    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 112
    sget-object v3, Lcom/nuance/a/a/a/b/c/b/b/a;->k:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NMASResourceImpl.createCommand() "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 114
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 117
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->g:Ljava/lang/Object;

    move-object/from16 v26, v0

    monitor-enter v26

    .line 118
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    if-eqz v2, :cond_b

    .line 119
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    invoke-virtual {v2}, Lcom/nuance/a/a/a/b/c/b/b/z;->e()V

    .line 122
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->a:Lcom/nuance/a/a/a/b/c/b/a/b;

    invoke-virtual {v2}, Lcom/nuance/a/a/a/b/c/b/a/b;->b()V

    .line 124
    move-object/from16 v0, p0

    iget v2, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->f:I

    if-nez v2, :cond_c

    .line 125
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->l:Lcom/nuance/a/a/a/a/b/a/b;

    new-instance v3, Lcom/nuance/a/a/a/a/b/a/b$a;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/nuance/a/a/a/a/b/a/b$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->l:Lcom/nuance/a/a/a/a/b/a/b;

    invoke-interface {v5}, Lcom/nuance/a/a/a/a/b/a/b;->a()[Ljava/lang/Object;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v5, v5, v6

    move-object/from16 v0, p0

    invoke-interface {v2, v3, v0, v4, v5}, Lcom/nuance/a/a/a/a/b/a/b;->a(Ljava/lang/Object;Lcom/nuance/a/a/a/a/b/a/b$b;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 129
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->a:Lcom/nuance/a/a/a/b/c/b/a/b;

    invoke-virtual {v2}, Lcom/nuance/a/a/a/b/c/b/a/b;->f()B

    move-result v2

    move-object/from16 v0, p0

    iput-byte v2, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->i:B

    .line 131
    new-instance v2, Lcom/nuance/a/a/a/b/c/b/b/z;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->l:Lcom/nuance/a/a/a/a/b/a/b;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->e:Lcom/nuance/a/a/a/b/c/a/a;

    check-cast v4, Lcom/nuance/a/a/a/b/c/b/a/a;

    iget-object v6, v4, Lcom/nuance/a/a/a/b/c/b/a/a;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->a:Lcom/nuance/a/a/a/b/c/b/a/b;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->e:Lcom/nuance/a/a/a/b/c/a/a;

    check-cast v4, Lcom/nuance/a/a/a/b/c/b/a/a;

    invoke-virtual {v4}, Lcom/nuance/a/a/a/b/c/b/a/a;->d()Ljava/lang/String;

    move-result-object v8

    const-string v9, "1"

    sget-object v10, Lcom/nuance/a/a/a/b/c/b/b/a;->o:Ljava/lang/String;

    const-string v12, "enus"

    const-string v13, "ne"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->e:Lcom/nuance/a/a/a/b/c/a/a;

    check-cast v4, Lcom/nuance/a/a/a/b/c/b/a/a;

    invoke-virtual {v4}, Lcom/nuance/a/a/a/b/c/b/a/a;->e()Lcom/nuance/a/a/a/a/a/a$a;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->e:Lcom/nuance/a/a/a/b/c/a/a;

    check-cast v4, Lcom/nuance/a/a/a/b/c/b/a/a;

    invoke-virtual {v4}, Lcom/nuance/a/a/a/b/c/b/a/a;->d()Ljava/lang/String;

    move-result-object v18

    const-string v19, ""

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->m:Lcom/nuance/a/a/a/b/c/c/f;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->i:B

    move/from16 v25, v0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v11, p3

    move-object/from16 v15, p4

    move-object/from16 v16, p5

    move-object/from16 v17, p6

    move-wide/from16 v20, p8

    move-object/from16 v22, p7

    move-object/from16 v23, p0

    invoke-direct/range {v2 .. v25}, Lcom/nuance/a/a/a/b/c/b/b/z;-><init>(Lcom/nuance/a/a/a/a/b/a/b;Lcom/nuance/a/a/a/b/c/c/g;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/a/a/a/b/c/b/a/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/a/a/a/a/a/a$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/nuance/a/a/a/b/c/c/c;Lcom/nuance/a/a/a/b/c/b/b/a;Lcom/nuance/a/a/a/b/c/c/f;B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    .line 138
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    monitor-exit v26
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v2

    .line 139
    :catchall_0
    move-exception v2

    monitor-exit v26

    throw v2
.end method

.method public final a(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/c/c;)Lcom/nuance/a/a/a/b/c/c/h;
    .locals 2

    .prologue
    .line 190
    if-nez p1, :cond_0

    .line 191
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_0
    if-nez p2, :cond_1

    .line 194
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "dict can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 196
    :cond_1
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/h;

    check-cast p2, Lcom/nuance/a/a/a/b/c/b/b/i;

    invoke-direct {v0, p1, p2}, Lcom/nuance/a/a/a/b/c/b/b/h;-><init>(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/b/b/i;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/c/c;Lcom/nuance/a/a/a/b/b/a;)Lcom/nuance/a/a/a/b/c/c/h;
    .locals 2

    .prologue
    .line 244
    if-nez p1, :cond_0

    .line 245
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247
    :cond_0
    if-nez p2, :cond_1

    .line 248
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "tts_dict can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 250
    :cond_1
    if-nez p3, :cond_2

    .line 251
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "audioSink can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_2
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/x;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->a:Lcom/nuance/a/a/a/b/c/b/a/b;

    check-cast p2, Lcom/nuance/a/a/a/b/c/b/b/i;

    invoke-direct {v0, p1, v1, p2, p3}, Lcom/nuance/a/a/a/b/c/b/b/x;-><init>(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/b/a/b;Lcom/nuance/a/a/a/b/c/b/b/i;Lcom/nuance/a/a/a/b/b/a;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/nuance/a/a/a/b/c/c/h;
    .locals 2

    .prologue
    .line 168
    if-nez p1, :cond_0

    .line 169
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :cond_0
    if-nez p2, :cond_1

    .line 172
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "choicename can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_1
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/e;

    invoke-direct {v0, p1, p2}, Lcom/nuance/a/a/a/b/c/b/b/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;[B)Lcom/nuance/a/a/a/b/c/c/h;
    .locals 2

    .prologue
    .line 179
    if-nez p1, :cond_0

    .line 180
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182
    :cond_0
    if-nez p2, :cond_1

    .line 183
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "data can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_1
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/g;

    invoke-direct {v0, p1, p2}, Lcom/nuance/a/a/a/b/c/b/b/g;-><init>(Ljava/lang/String;[B)V

    return-object v0
.end method

.method public final a()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nuance/a/a/a/b/c/a/e;
        }
    .end annotation

    .prologue
    .line 270
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/a;->k:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "freeResource() disconnectTimeout:0"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 272
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/z;->e()V

    .line 275
    :cond_0
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 277
    :try_start_0
    iget v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->f:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 278
    const/4 v0, 0x0

    iput v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->f:I

    .line 279
    new-instance v0, Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/lang/Integer;-><init>(I)V

    .line 280
    iget-object v2, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->l:Lcom/nuance/a/a/a/a/b/a/b;

    new-instance v3, Lcom/nuance/a/a/a/a/b/a/b$a;

    const/4 v4, 0x3

    invoke-direct {v3, v4, v0}, Lcom/nuance/a/a/a/a/b/a/b$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v4, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->l:Lcom/nuance/a/a/a/a/b/a/b;

    invoke-interface {v4}, Lcom/nuance/a/a/a/a/b/a/b;->a()[Ljava/lang/Object;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-interface {v2, v3, p0, v0, v4}, Lcom/nuance/a/a/a/a/b/a/b;->a(Ljava/lang/Object;Lcom/nuance/a/a/a/a/b/a/b$b;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 285
    monitor-exit v1

    return-void

    .line 283
    :cond_1
    new-instance v0, Lcom/nuance/a/a/a/b/c/a/e;

    const-string v2, "the resource was unloaded. "

    invoke-direct {v0, v2}, Lcom/nuance/a/a/a/b/c/a/e;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(BS)V
    .locals 1

    .prologue
    .line 382
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->i:B

    if-eq p1, v0, :cond_0

    .line 389
    :goto_0
    return-void

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    if-eqz v0, :cond_1

    .line 386
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/z;->e()V

    .line 388
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/nuance/a/a/a/b/c/b/a/d;->a(BS)V

    goto :goto_0
.end method

.method public final a(B[B)V
    .locals 4

    .prologue
    .line 304
    invoke-static {p2}, Lcom/nuance/a/a/a/b/c/b/b/m;->a([B)Lcom/nuance/a/a/a/b/c/b/b/l;

    move-result-object v0

    .line 305
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/l;->e()S

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 353
    :pswitch_0
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/b/a;->k:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Session.parseXModeMsgBcpData() Unknown command: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/l;->e()S

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 357
    :cond_0
    :goto_0
    return-void

    .line 319
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/nuance/a/a/a/b/c/b/b/a;->a(B)V

    .line 320
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    if-eqz v1, :cond_0

    .line 321
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/t;

    invoke-virtual {v1, v0, p1}, Lcom/nuance/a/a/a/b/c/b/b/z;->a(Lcom/nuance/a/a/a/b/c/b/b/t;B)V

    goto :goto_0

    .line 324
    :pswitch_2
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    if-eqz v1, :cond_0

    .line 325
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/r;

    invoke-virtual {v1, v0, p1}, Lcom/nuance/a/a/a/b/c/b/b/z;->a(Lcom/nuance/a/a/a/b/c/b/b/r;B)V

    goto :goto_0

    .line 328
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/nuance/a/a/a/b/c/b/b/a;->a(B)V

    .line 329
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    if-eqz v1, :cond_0

    .line 330
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/u;

    invoke-virtual {v1, v0, p1}, Lcom/nuance/a/a/a/b/c/b/b/z;->a(Lcom/nuance/a/a/a/b/c/b/b/u;B)V

    goto :goto_0

    .line 305
    :pswitch_data_0
    .packed-switch 0x7201
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 425
    move-object v0, p1

    check-cast v0, Lcom/nuance/a/a/a/a/b/a/b$a;

    .line 427
    iget-byte v0, v0, Lcom/nuance/a/a/a/a/b/a/b$a;->a:B

    packed-switch v0, :pswitch_data_0

    .line 449
    :pswitch_0
    invoke-super {p0, p1, p2}, Lcom/nuance/a/a/a/b/c/b/a/d;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 452
    :goto_0
    return-void

    .line 429
    :pswitch_1
    invoke-super {p0, p0}, Lcom/nuance/a/a/a/b/c/b/a/d;->a(Lcom/nuance/a/a/a/b/c/b/a/c;)V

    goto :goto_0

    .line 439
    :pswitch_2
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->a:Lcom/nuance/a/a/a/b/c/b/a/b;

    invoke-virtual {v0, p0}, Lcom/nuance/a/a/a/b/c/b/a/b;->b(Lcom/nuance/a/a/a/b/c/b/a/c;)V

    .line 440
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->m:Lcom/nuance/a/a/a/b/c/c/f;

    goto :goto_0

    .line 427
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(S)V
    .locals 3

    .prologue
    .line 296
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/a;->k:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSessionDisconnected() reasonCode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 298
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    invoke-virtual {v0, p1}, Lcom/nuance/a/a/a/b/c/b/b/z;->a(S)V

    .line 300
    :cond_0
    invoke-super {p0, p1}, Lcom/nuance/a/a/a/b/c/b/a/d;->a(S)V

    .line 301
    return-void
.end method

.method public final a([B)V
    .locals 1

    .prologue
    .line 289
    invoke-super {p0, p1}, Lcom/nuance/a/a/a/b/c/b/a/d;->a([B)V

    .line 290
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    invoke-virtual {v0, p1}, Lcom/nuance/a/a/a/b/c/b/b/z;->a([B)V

    .line 292
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/c/c;)Lcom/nuance/a/a/a/b/c/c/h;
    .locals 2

    .prologue
    .line 206
    if-nez p1, :cond_0

    .line 207
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_0
    if-nez p2, :cond_1

    .line 210
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "dict can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :cond_1
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/v;

    check-cast p2, Lcom/nuance/a/a/a/b/c/b/b/i;

    const/4 v1, 0x2

    invoke-direct {v0, p1, p2, v1}, Lcom/nuance/a/a/a/b/c/b/b/v;-><init>(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/b/b/i;B)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/nuance/a/a/a/b/c/c/h;
    .locals 2

    .prologue
    .line 258
    if-nez p1, :cond_0

    .line 259
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 261
    :cond_0
    if-nez p2, :cond_1

    .line 262
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "text can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 264
    :cond_1
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/y;

    invoke-direct {v0, p1, p2}, Lcom/nuance/a/a/a/b/c/b/b/y;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/c/c;)Lcom/nuance/a/a/a/b/c/c/h;
    .locals 2

    .prologue
    .line 217
    if-nez p1, :cond_0

    .line 218
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    :cond_0
    if-nez p2, :cond_1

    .line 221
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "dict can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 223
    :cond_1
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/v;

    check-cast p2, Lcom/nuance/a/a/a/b/c/b/b/i;

    const/4 v1, 0x3

    invoke-direct {v0, p1, p2, v1}, Lcom/nuance/a/a/a/b/c/b/b/v;-><init>(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/b/b/i;B)V

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/z;->f()V

    .line 404
    return-void
.end method

.method public final d(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/c/c;)Lcom/nuance/a/a/a/b/c/c/h;
    .locals 2

    .prologue
    .line 228
    if-nez p1, :cond_0

    .line 229
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :cond_0
    if-nez p2, :cond_1

    .line 232
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "dict can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 234
    :cond_1
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/v;

    check-cast p2, Lcom/nuance/a/a/a/b/c/b/b/i;

    const/4 v1, 0x1

    invoke-direct {v0, p1, p2, v1}, Lcom/nuance/a/a/a/b/c/b/b/v;-><init>(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/b/b/i;B)V

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/z;->b()V

    .line 410
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/z;->c()V

    .line 416
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/a;->n:Lcom/nuance/a/a/a/b/c/b/b/z;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/z;->d()V

    .line 422
    return-void
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 155
    invoke-super {p0}, Lcom/nuance/a/a/a/b/c/b/a/d;->h()J

    move-result-wide v0

    return-wide v0
.end method

.method public final j()Lcom/nuance/a/a/a/b/c/c/c;
    .locals 1

    .prologue
    .line 201
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/i;

    invoke-direct {v0}, Lcom/nuance/a/a/a/b/c/b/b/i;-><init>()V

    return-object v0
.end method

.method public final k()Lcom/nuance/a/a/a/b/c/c/l;
    .locals 1

    .prologue
    .line 239
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/w;

    invoke-direct {v0}, Lcom/nuance/a/a/a/b/c/b/b/w;-><init>()V

    return-object v0
.end method
