.class public abstract Lcom/nuance/a/a/a/c/a/b;
.super Ljava/lang/Object;
.source "Bluetooth.java"


# static fields
.field private static final c:Lcom/nuance/a/a/a/a/b/a/a$a;


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Lcom/nuance/a/a/a/c/a/d;

.field private d:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/nuance/a/a/a/c/a/b;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/c/a/b;->c:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/nuance/a/a/a/c/a/b;->d:Ljava/lang/Object;

    .line 38
    iput-object p1, p0, Lcom/nuance/a/a/a/c/a/b;->a:Landroid/content/Context;

    .line 39
    new-instance v0, Lcom/nuance/a/a/a/c/a/d;

    iget-object v1, p0, Lcom/nuance/a/a/a/c/a/b;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/nuance/a/a/a/c/a/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/nuance/a/a/a/c/a/b;->b:Lcom/nuance/a/a/a/c/a/d;

    .line 40
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/nuance/a/a/a/c/a/b;
    .locals 2

    .prologue
    .line 27
    sget v0, Lcom/nuance/a/a/a/c/a/a;->a:I

    .line 28
    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    sget-boolean v1, Lcom/nuance/a/a/a/c/a/a;->d:Z

    if-eqz v1, :cond_1

    .line 29
    :cond_0
    new-instance v0, Lcom/nuance/a/a/a/c/a/f;

    invoke-direct {v0, p0}, Lcom/nuance/a/a/a/c/a/f;-><init>(Landroid/content/Context;)V

    .line 33
    :goto_0
    return-object v0

    .line 30
    :cond_1
    const/16 v1, 0x9

    if-ge v0, v1, :cond_2

    .line 31
    new-instance v0, Lcom/nuance/a/a/a/c/a/g;

    invoke-direct {v0, p0}, Lcom/nuance/a/a/a/c/a/g;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 33
    :cond_2
    new-instance v0, Lcom/nuance/a/a/a/c/a/e;

    invoke-direct {v0, p0}, Lcom/nuance/a/a/a/c/a/e;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/nuance/a/a/a/c/a/b;->b:Lcom/nuance/a/a/a/c/a/d;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/c/a/d;->a()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 93
    :goto_0
    return v0

    .line 92
    :cond_0
    const/4 v0, 0x0

    .line 93
    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/nuance/a/a/a/c/a/b;->b:Lcom/nuance/a/a/a/c/a/d;

    if-eqz v0, :cond_0

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/nuance/a/a/a/c/a/b;->b:Lcom/nuance/a/a/a/c/a/d;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/c/a/d;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/a/a/a/c/a/b;->b:Lcom/nuance/a/a/a/c/a/d;

    .line 104
    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
