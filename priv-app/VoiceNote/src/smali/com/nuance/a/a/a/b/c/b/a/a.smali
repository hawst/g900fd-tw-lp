.class public Lcom/nuance/a/a/a/b/c/b/a/a;
.super Lcom/nuance/a/a/a/b/b/b;
.source "ManagerImpl.java"

# interfaces
.implements Lcom/nuance/a/a/a/b/c/a/a;


# static fields
.field private static final h:Lcom/nuance/a/a/a/a/b/a/a$a;


# instance fields
.field public g:Ljava/lang/String;

.field private i:Lcom/nuance/a/a/a/b/c/b/a/b;

.field private j:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/nuance/a/a/a/b/c/b/a/a;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/b/c/b/a/a;->h:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;SLjava/lang/String;[BLjava/lang/String;Lcom/nuance/a/a/a/a/a/a$a;Lcom/nuance/a/a/a/a/a/a$a;Ljava/util/Vector;Lcom/nuance/a/a/a/b/c/a/c;)V
    .locals 10

    .prologue
    .line 69
    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    invoke-direct/range {v1 .. v6}, Lcom/nuance/a/a/a/b/b/b;-><init>(Ljava/lang/String;SLjava/lang/String;Lcom/nuance/a/a/a/a/a/a$a;Lcom/nuance/a/a/a/a/a/a$a;)V

    .line 71
    if-nez p3, :cond_0

    .line 72
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, " application id is null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 74
    :cond_0
    iput-object p3, p0, Lcom/nuance/a/a/a/b/c/b/a/a;->g:Ljava/lang/String;

    .line 76
    if-nez p4, :cond_1

    .line 77
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, " application key is null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 81
    :cond_1
    if-eqz p8, :cond_2

    invoke-virtual/range {p8 .. p8}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_4

    .line 82
    :cond_2
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    .line 108
    :cond_3
    new-instance v1, Lcom/nuance/a/a/a/b/c/b/a/b;

    invoke-virtual {p0}, Lcom/nuance/a/a/a/b/c/b/a/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/nuance/a/a/a/b/c/b/a/a;->c()S

    move-result v3

    invoke-virtual {p0}, Lcom/nuance/a/a/a/b/c/b/a/a;->a()Lcom/nuance/a/a/a/a/b/a/b;

    move-result-object v8

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/nuance/a/a/a/b/c/b/a/b;-><init>(Ljava/lang/String;SLjava/lang/String;[BLjava/lang/String;Ljava/util/Vector;Lcom/nuance/a/a/a/a/b/a/b;Lcom/nuance/a/a/a/b/c/a/c;)V

    iput-object v1, p0, Lcom/nuance/a/a/a/b/c/b/a/a;->i:Lcom/nuance/a/a/a/b/c/b/a/b;

    .line 116
    return-void

    .line 84
    :cond_4
    new-instance v7, Ljava/util/Vector;

    invoke-virtual/range {p8 .. p8}, Ljava/util/Vector;->size()I

    move-result v1

    invoke-direct {v7, v1}, Ljava/util/Vector;-><init>(I)V

    .line 85
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual/range {p8 .. p8}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 86
    move-object/from16 v0, p8

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/a/a/a/b/b/c;

    .line 87
    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/b/c;->d()Lcom/nuance/a/a/a/b/b/c$a;

    move-result-object v3

    sget-object v4, Lcom/nuance/a/a/a/b/b/c$a;->a:Lcom/nuance/a/a/a/b/b/c$a;

    if-ne v3, v4, :cond_5

    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "DEVICE_CMD_LOG_TO_SERVER_ENABLED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 97
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    iput-object v3, p0, Lcom/nuance/a/a/a/b/c/b/a/a;->j:Ljava/lang/Object;

    .line 104
    :cond_5
    invoke-virtual {v7, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 85
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method


# virtual methods
.method public final c_()V
    .locals 2

    .prologue
    .line 132
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/a/a;->h:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "shutdown"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 134
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/a;->i:Lcom/nuance/a/a/a/b/c/b/a/b;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/a/b;->c()V

    .line 135
    return-void
.end method

.method public final g()Lcom/nuance/a/a/a/b/c/b/a/b;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/a;->i:Lcom/nuance/a/a/a/b/c/b/a/b;

    return-object v0
.end method

.method public final h()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/a/a;->j:Ljava/lang/Object;

    return-object v0
.end method
