.class public final Lcom/nuance/a/a/a/b/b/c;
.super Ljava/lang/Object;
.source "Parameter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/a/a/a/b/b/c$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Object;

.field private c:Lcom/nuance/a/a/a/b/b/c$a;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/nuance/a/a/a/b/b/c$a;)V
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object p1, p0, Lcom/nuance/a/a/a/b/b/c;->a:Ljava/lang/String;

    .line 108
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/nuance/a/a/a/b/b/c;->b:Ljava/lang/Object;

    .line 109
    iput-object p2, p0, Lcom/nuance/a/a/a/b/b/c;->c:Lcom/nuance/a/a/a/b/b/c$a;

    .line 110
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;Lcom/nuance/a/a/a/b/b/c$a;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/nuance/a/a/a/b/b/c;->a:Ljava/lang/String;

    .line 96
    iput-object p2, p0, Lcom/nuance/a/a/a/b/b/c;->b:Ljava/lang/Object;

    .line 97
    iput-object p3, p0, Lcom/nuance/a/a/a/b/b/c;->c:Lcom/nuance/a/a/a/b/b/c$a;

    .line 98
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/nuance/a/a/a/b/b/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()[B
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/nuance/a/a/a/b/b/c;->b:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/a/a/a/b/b/c;->b:Ljava/lang/Object;

    check-cast v0, [B

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "THIS IS NOT A STRING PARAMETER!!!"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/nuance/a/a/a/b/b/c;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/nuance/a/a/a/b/b/c;->e()Lcom/nuance/a/a/a/b/b/c;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/nuance/a/a/a/b/b/c$a;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/nuance/a/a/a/b/b/c;->c:Lcom/nuance/a/a/a/b/b/c$a;

    return-object v0
.end method

.method public final e()Lcom/nuance/a/a/a/b/b/c;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 154
    iget-object v0, p0, Lcom/nuance/a/a/a/b/b/c;->b:Ljava/lang/Object;

    .line 155
    iget-object v1, p0, Lcom/nuance/a/a/a/b/b/c;->b:Ljava/lang/Object;

    instance-of v1, v1, [B

    if-eqz v1, :cond_0

    .line 156
    iget-object v0, p0, Lcom/nuance/a/a/a/b/b/c;->b:Ljava/lang/Object;

    check-cast v0, [B

    .line 157
    array-length v1, v0

    new-array v1, v1, [B

    .line 158
    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v1

    .line 160
    :cond_0
    new-instance v1, Lcom/nuance/a/a/a/b/b/c;

    iget-object v2, p0, Lcom/nuance/a/a/a/b/b/c;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/nuance/a/a/a/b/b/c;->c:Lcom/nuance/a/a/a/b/b/c$a;

    invoke-direct {v1, v2, v0, v3}, Lcom/nuance/a/a/a/b/b/c;-><init>(Ljava/lang/String;Ljava/lang/Object;Lcom/nuance/a/a/a/b/b/c$a;)V

    return-object v1
.end method
