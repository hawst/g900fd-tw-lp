.class public final Lcom/nuance/a/a/a/c/e$b;
.super Ljava/lang/Object;
.source "MessageSystemOEM.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/a/a/a/c/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/nuance/a/a/a/c/e;

.field private b:Lcom/nuance/a/a/a/a/b/a/e$a;


# direct methods
.method public constructor <init>(Lcom/nuance/a/a/a/c/e;Lcom/nuance/a/a/a/a/b/a/e$a;)V
    .locals 1

    .prologue
    .line 56
    iput-object p1, p0, Lcom/nuance/a/a/a/c/e$b;->a:Lcom/nuance/a/a/a/c/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p2, p0, Lcom/nuance/a/a/a/c/e$b;->b:Lcom/nuance/a/a/a/a/b/a/e$a;

    .line 58
    invoke-static {p1}, Lcom/nuance/a/a/a/c/e;->a(Lcom/nuance/a/a/a/c/e;)Ljava/util/Hashtable;

    move-result-object v0

    invoke-virtual {v0, p2, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 62
    iget-object v0, p0, Lcom/nuance/a/a/a/c/e$b;->a:Lcom/nuance/a/a/a/c/e;

    invoke-static {v0}, Lcom/nuance/a/a/a/c/e;->a(Lcom/nuance/a/a/a/c/e;)Ljava/util/Hashtable;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/a/a/a/c/e$b;->b:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/c/e$b;

    .line 64
    invoke-static {}, Lcom/nuance/a/a/a/c/e;->d()Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    invoke-static {}, Lcom/nuance/a/a/a/c/e;->d()Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TIMER run() _pendingTimerTasks.size():"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/nuance/a/a/a/c/e$b;->a:Lcom/nuance/a/a/a/c/e;

    invoke-static {v3}, Lcom/nuance/a/a/a/c/e;->a(Lcom/nuance/a/a/a/c/e;)Ljava/util/Hashtable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Hashtable;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", this:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", r:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 67
    :cond_0
    if-eqz v0, :cond_1

    .line 68
    iget-object v0, v0, Lcom/nuance/a/a/a/c/e$b;->b:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0}, Lcom/nuance/a/a/a/a/b/a/e$a;->run()V

    .line 69
    :cond_1
    return-void
.end method
