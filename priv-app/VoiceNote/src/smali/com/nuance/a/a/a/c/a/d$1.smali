.class final Lcom/nuance/a/a/a/c/a/d$1;
.super Ljava/lang/Object;
.source "BluetoothHeadsetOEM.java"

# interfaces
.implements Lcom/nuance/a/a/a/c/a/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/a/a/a/c/a/d;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/nuance/a/a/a/c/a/d;


# direct methods
.method constructor <init>(Lcom/nuance/a/a/a/c/a/d;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/nuance/a/a/a/c/a/d$1;->a:Lcom/nuance/a/a/a/c/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 53
    invoke-static {}, Lcom/nuance/a/a/a/c/a/d;->c()Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    invoke-static {}, Lcom/nuance/a/a/a/c/a/d;->c()Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    const-string v1, "BluetoothHeadsetOEM reflected onServiceConnected()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->c(Ljava/lang/Object;)V

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/nuance/a/a/a/c/a/d$1;->a:Lcom/nuance/a/a/a/c/a/d;

    invoke-static {v0}, Lcom/nuance/a/a/a/c/a/d;->a(Lcom/nuance/a/a/a/c/a/d;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 57
    :try_start_0
    iget-object v0, p0, Lcom/nuance/a/a/a/c/a/d$1;->a:Lcom/nuance/a/a/a/c/a/d;

    invoke-static {v0}, Lcom/nuance/a/a/a/c/a/d;->b(Lcom/nuance/a/a/a/c/a/d;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 58
    iget-object v0, p0, Lcom/nuance/a/a/a/c/a/d$1;->a:Lcom/nuance/a/a/a/c/a/d;

    invoke-static {v0}, Lcom/nuance/a/a/a/c/a/d;->c(Lcom/nuance/a/a/a/c/a/d;)Z

    .line 59
    iget-object v0, p0, Lcom/nuance/a/a/a/c/a/d$1;->a:Lcom/nuance/a/a/a/c/a/d;

    invoke-static {v0}, Lcom/nuance/a/a/a/c/a/d;->a(Lcom/nuance/a/a/a/c/a/d;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 63
    :goto_0
    monitor-exit v1

    return-void

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/nuance/a/a/a/c/a/d$1;->a:Lcom/nuance/a/a/a/c/a/d;

    invoke-static {v0}, Lcom/nuance/a/a/a/c/a/d;->d(Lcom/nuance/a/a/a/c/a/d;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 67
    invoke-static {}, Lcom/nuance/a/a/a/c/a/d;->c()Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    invoke-static {}, Lcom/nuance/a/a/a/c/a/d;->c()Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    const-string v1, "BluetoothHeadsetOEM reflected onServiceDisconnected()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->c(Ljava/lang/Object;)V

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/nuance/a/a/a/c/a/d$1;->a:Lcom/nuance/a/a/a/c/a/d;

    invoke-static {v0}, Lcom/nuance/a/a/a/c/a/d;->a(Lcom/nuance/a/a/a/c/a/d;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 71
    :try_start_0
    iget-object v0, p0, Lcom/nuance/a/a/a/c/a/d$1;->a:Lcom/nuance/a/a/a/c/a/d;

    invoke-static {v0}, Lcom/nuance/a/a/a/c/a/d;->d(Lcom/nuance/a/a/a/c/a/d;)V

    .line 72
    iget-object v0, p0, Lcom/nuance/a/a/a/c/a/d$1;->a:Lcom/nuance/a/a/a/c/a/d;

    invoke-static {v0}, Lcom/nuance/a/a/a/c/a/d;->b(Lcom/nuance/a/a/a/c/a/d;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/nuance/a/a/a/c/a/d$1;->a:Lcom/nuance/a/a/a/c/a/d;

    invoke-static {v0}, Lcom/nuance/a/a/a/c/a/d;->c(Lcom/nuance/a/a/a/c/a/d;)Z

    .line 74
    iget-object v0, p0, Lcom/nuance/a/a/a/c/a/d$1;->a:Lcom/nuance/a/a/a/c/a/d;

    invoke-static {v0}, Lcom/nuance/a/a/a/c/a/d;->a(Lcom/nuance/a/a/a/c/a/d;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 76
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
