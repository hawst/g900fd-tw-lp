.class public Lcom/nuance/a/a/a/a/d/d;
.super Ljava/lang/Object;
.source "Util.java"


# static fields
.field private static final a:Lcom/nuance/a/a/a/a/b/a/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/nuance/a/a/a/a/d/d;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/a/d/d;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 120
    sget-object v0, Lcom/nuance/a/a/a/a/d/d;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    sget-object v0, Lcom/nuance/a/a/a/a/d/d;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Escaping XML reserved tokens (&, <, >, \" and \') of: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->a(Ljava/lang/Object;)V

    .line 123
    :cond_0
    const/4 v0, 0x0

    .line 124
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 125
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-ge v0, v2, :cond_6

    .line 127
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    .line 128
    const/16 v3, 0x26

    if-ne v2, v3, :cond_1

    .line 130
    add-int/lit8 v0, v0, 0x1

    .line 131
    const-string v2, "amp;"

    invoke-virtual {v1, v0, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 132
    add-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 134
    :cond_1
    const/16 v3, 0x3c

    if-ne v2, v3, :cond_2

    .line 136
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 137
    const-string v2, "&lt;"

    invoke-virtual {v1, v0, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 138
    add-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 140
    :cond_2
    const/16 v3, 0x3e

    if-ne v2, v3, :cond_3

    .line 142
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 143
    const-string v2, "&gt;"

    invoke-virtual {v1, v0, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 144
    add-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 146
    :cond_3
    const/16 v3, 0x22

    if-ne v2, v3, :cond_4

    .line 148
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 149
    const-string v2, "&quot;"

    invoke-virtual {v1, v0, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 150
    add-int/lit8 v0, v0, 0x6

    goto :goto_0

    .line 152
    :cond_4
    const/16 v3, 0x27

    if-ne v2, v3, :cond_5

    .line 154
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 155
    const-string v2, "&apos;"

    invoke-virtual {v1, v0, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 156
    add-int/lit8 v0, v0, 0x6

    goto :goto_0

    .line 159
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 160
    goto :goto_0

    .line 162
    :cond_6
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 164
    sget-object v1, Lcom/nuance/a/a/a/a/d/d;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 165
    sget-object v1, Lcom/nuance/a/a/a/a/d/d;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Final output: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->a(Ljava/lang/Object;)V

    .line 167
    :cond_7
    return-object v0
.end method

.method public static a(Lcom/nuance/a/a/a/a/a/a$a;)Z
    .locals 2

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/nuance/a/a/a/a/a/a$a;->a()S

    move-result v0

    .line 108
    sget-object v1, Lcom/nuance/a/a/a/a/a/a$a;->y:Lcom/nuance/a/a/a/a/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/a/a$a;->a()S

    move-result v1

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/nuance/a/a/a/a/a/a$a;->A:Lcom/nuance/a/a/a/a/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/a/a$a;->a()S

    move-result v1

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/nuance/a/a/a/a/a/a$a;->z:Lcom/nuance/a/a/a/a/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/a/a$a;->a()S

    move-result v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/nuance/a/a/a/a/a/a$a;)Lcom/nuance/a/a/a/a/a/a$a;
    .locals 4

    .prologue
    .line 178
    sget-object v0, Lcom/nuance/a/a/a/a/a/a$a;->z:Lcom/nuance/a/a/a/a/a/a$a;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/nuance/a/a/a/a/a/a$a;->A:Lcom/nuance/a/a/a/a/a/a$a;

    if-ne p0, v0, :cond_2

    .line 180
    :cond_0
    sget-object v0, Lcom/nuance/a/a/a/a/a/a$a;->y:Lcom/nuance/a/a/a/a/a/a$a;

    .line 192
    :goto_0
    sget-object v1, Lcom/nuance/a/a/a/a/d/d;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 193
    sget-object v1, Lcom/nuance/a/a/a/a/d/d;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "adjustCodecForBluetooth() "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/nuance/a/a/a/a/a/a$a;->a()S

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/a/a$a;->a()S

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->c(Ljava/lang/Object;)V

    .line 195
    :cond_1
    return-object v0

    .line 182
    :cond_2
    sget-object v0, Lcom/nuance/a/a/a/a/a/a$a;->C:Lcom/nuance/a/a/a/a/a/a$a;

    if-eq p0, v0, :cond_3

    sget-object v0, Lcom/nuance/a/a/a/a/a/a$a;->D:Lcom/nuance/a/a/a/a/a/a$a;

    if-eq p0, v0, :cond_3

    sget-object v0, Lcom/nuance/a/a/a/a/a/a$a;->F:Lcom/nuance/a/a/a/a/a/a$a;

    if-eq p0, v0, :cond_3

    sget-object v0, Lcom/nuance/a/a/a/a/a/a$a;->E:Lcom/nuance/a/a/a/a/a/a$a;

    if-ne p0, v0, :cond_4

    .line 186
    :cond_3
    sget-object v0, Lcom/nuance/a/a/a/a/a/a$a;->B:Lcom/nuance/a/a/a/a/a/a$a;

    goto :goto_0

    :cond_4
    move-object v0, p0

    .line 189
    goto :goto_0
.end method
