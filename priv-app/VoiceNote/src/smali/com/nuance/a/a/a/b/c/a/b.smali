.class public Lcom/nuance/a/a/a/b/c/a/b;
.super Ljava/lang/Object;
.source "ManagerFactory.java"


# static fields
.field private static final a:Lcom/nuance/a/a/a/a/b/a/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/nuance/a/a/a/b/c/a/b;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/b/c/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method public static a(Ljava/lang/String;SLjava/lang/String;[BLjava/lang/String;Lcom/nuance/a/a/a/a/a/a$a;Lcom/nuance/a/a/a/a/a/a$a;Ljava/util/Vector;Lcom/nuance/a/a/a/b/c/a/c;)Lcom/nuance/a/a/a/b/c/a/a;
    .locals 11

    .prologue
    .line 76
    sget-object v1, Lcom/nuance/a/a/a/b/c/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    sget-object v1, Lcom/nuance/a/a/a/b/c/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "createManager"

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 79
    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 81
    :cond_1
    sget-object v1, Lcom/nuance/a/a/a/b/c/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "NullPointerException gatewayIP is NULL. "

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 83
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "gatewayIP must be provided!"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 85
    :cond_2
    if-gtz p1, :cond_3

    .line 87
    sget-object v1, Lcom/nuance/a/a/a/b/c/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "IllegalArgumentException gatewayPort is invalid. "

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 89
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "gatewayPort invalid value!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 91
    :cond_3
    if-nez p2, :cond_4

    .line 93
    sget-object v1, Lcom/nuance/a/a/a/b/c/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "NullPointerException applicationId is NULL. "

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 95
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Application id can not be null!"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 97
    :cond_4
    if-nez p3, :cond_5

    .line 99
    sget-object v1, Lcom/nuance/a/a/a/b/c/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "NullPointerException appKey is NULL. "

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 101
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Application key can not be null!"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 103
    :cond_5
    if-nez p4, :cond_6

    .line 105
    sget-object v1, Lcom/nuance/a/a/a/b/c/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "NullPointerException uid is NULL. "

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 107
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "uid can not be null!"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 109
    :cond_6
    if-nez p5, :cond_7

    .line 111
    sget-object v1, Lcom/nuance/a/a/a/b/c/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "NullPointerException inputCodec is NULL. "

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 113
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "inputCodec can not be null!"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 115
    :cond_7
    if-nez p6, :cond_8

    .line 117
    sget-object v1, Lcom/nuance/a/a/a/b/c/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "NullPointerException outputCodec is NULL. "

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 119
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "outputCodec can not be null!"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 121
    :cond_8
    if-nez p8, :cond_9

    .line 123
    sget-object v1, Lcom/nuance/a/a/a/b/c/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "NullPointerException managerListener is NULL. "

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 125
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "managerListener can not be null!"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 127
    :cond_9
    if-eqz p7, :cond_c

    .line 128
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual/range {p7 .. p7}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v2, v1, :cond_c

    .line 129
    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/a/a/a/b/b/c;

    .line 130
    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/b/c;->d()Lcom/nuance/a/a/a/b/b/c$a;

    move-result-object v3

    sget-object v4, Lcom/nuance/a/a/a/b/b/c$a;->e:Lcom/nuance/a/a/a/b/b/c$a;

    if-eq v3, v4, :cond_a

    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/b/c;->d()Lcom/nuance/a/a/a/b/b/c$a;

    move-result-object v3

    sget-object v4, Lcom/nuance/a/a/a/b/b/c$a;->f:Lcom/nuance/a/a/a/b/b/c$a;

    if-eq v3, v4, :cond_a

    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/b/c;->d()Lcom/nuance/a/a/a/b/b/c$a;

    move-result-object v3

    sget-object v4, Lcom/nuance/a/a/a/b/b/c$a;->g:Lcom/nuance/a/a/a/b/b/c$a;

    if-eq v3, v4, :cond_a

    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/b/c;->d()Lcom/nuance/a/a/a/b/b/c$a;

    move-result-object v3

    sget-object v4, Lcom/nuance/a/a/a/b/b/c$a;->h:Lcom/nuance/a/a/a/b/b/c$a;

    if-eq v3, v4, :cond_a

    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/b/c;->d()Lcom/nuance/a/a/a/b/b/c$a;

    move-result-object v3

    sget-object v4, Lcom/nuance/a/a/a/b/b/c$a;->d:Lcom/nuance/a/a/a/b/b/c$a;

    if-eq v3, v4, :cond_a

    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/b/c;->d()Lcom/nuance/a/a/a/b/b/c$a;

    move-result-object v3

    sget-object v4, Lcom/nuance/a/a/a/b/b/c$a;->i:Lcom/nuance/a/a/a/b/b/c$a;

    if-eq v3, v4, :cond_a

    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/b/c;->d()Lcom/nuance/a/a/a/b/b/c$a;

    move-result-object v3

    sget-object v4, Lcom/nuance/a/a/a/b/b/c$a;->j:Lcom/nuance/a/a/a/b/b/c$a;

    if-ne v3, v4, :cond_b

    .line 138
    :cond_a
    sget-object v2, Lcom/nuance/a/a/a/b/c/a/b;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "IllegalArgumentException Parameter type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/b/c;->d()Lcom/nuance/a/a/a/b/b/c$a;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not allowed. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 140
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Parameter type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/b/c;->d()Lcom/nuance/a/a/a/b/b/c$a;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " not allowed. "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 128
    :cond_b
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_0

    .line 144
    :cond_c
    new-instance v1, Lcom/nuance/a/a/a/b/c/b/a/a;

    move-object v2, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v10}, Lcom/nuance/a/a/a/b/c/b/a/a;-><init>(Ljava/lang/String;SLjava/lang/String;[BLjava/lang/String;Lcom/nuance/a/a/a/a/a/a$a;Lcom/nuance/a/a/a/a/a/a$a;Ljava/util/Vector;Lcom/nuance/a/a/a/b/c/a/c;)V

    return-object v1
.end method
