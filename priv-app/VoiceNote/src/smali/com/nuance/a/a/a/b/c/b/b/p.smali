.class public Lcom/nuance/a/a/a/b/c/b/b/p;
.super Lcom/nuance/a/a/a/b/c/b/b/l;
.source "PDXQueryBegin.java"


# static fields
.field private static final a:Lcom/nuance/a/a/a/a/b/a/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/nuance/a/a/a/b/c/b/b/p;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/b/c/b/b/p;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/nuance/a/a/a/a/a/a$a;Ljava/lang/String;SSLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Lcom/nuance/a/a/a/b/c/c/c;)V
    .locals 5

    .prologue
    .line 65
    const/16 v1, 0x202

    invoke-direct {p0, v1}, Lcom/nuance/a/a/a/b/c/b/b/l;-><init>(S)V

    .line 67
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/b/p;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/b/p;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "PDXQueryBegin()"

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 70
    :cond_0
    const-string v1, "uid"

    const/16 v2, 0xc1

    invoke-virtual {p0, v1, p1, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    .line 71
    const-string v1, "pdx_version"

    const/16 v2, 0xc1

    invoke-virtual {p0, v1, p2, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    .line 72
    const-string v1, "client_version"

    const/16 v2, 0xc1

    invoke-virtual {p0, v1, p3, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    .line 73
    const-string v1, "script_version"

    const/16 v2, 0xc1

    invoke-virtual {p0, v1, p4, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    .line 74
    const-string v1, "language"

    const/16 v2, 0xc1

    invoke-virtual {p0, v1, p5, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    .line 75
    const-string v1, "region"

    const/16 v2, 0xc1

    invoke-virtual {p0, v1, p6, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    .line 76
    const-string v1, "device_codec"

    invoke-virtual {p7}, Lcom/nuance/a/a/a/a/a/a$a;->a()S

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;I)V

    .line 77
    const-string v1, "dictation_language"

    const/16 v2, 0xc1

    invoke-virtual {p0, v1, p8, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    .line 78
    const-string v1, "lcd_width"

    invoke-virtual {p0, v1, p9}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;I)V

    .line 79
    const-string v1, "lcd_height"

    invoke-virtual {p0, v1, p10}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;I)V

    .line 80
    if-nez p11, :cond_2

    .line 81
    const-string v1, "carrier"

    const/4 v2, 0x0

    new-array v2, v2, [B

    const/4 v3, 0x5

    invoke-virtual {p0, v1, v2, v3}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;[BS)V

    .line 84
    :goto_0
    const-string v1, "phone_model"

    const/16 v2, 0xc1

    move-object/from16 v0, p12

    invoke-virtual {p0, v1, v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    .line 85
    const-string v1, "phone_number"

    const/16 v2, 0xc1

    move-object/from16 v0, p13

    invoke-virtual {p0, v1, v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    .line 86
    const-string v1, "original_session_id"

    const/16 v2, 0x16

    move-object/from16 v0, p14

    invoke-virtual {p0, v1, v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    .line 87
    if-eqz p15, :cond_1

    .line 88
    const-string v1, "new_session_id"

    const/16 v2, 0x16

    move-object/from16 v0, p15

    invoke-virtual {p0, v1, v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;[BS)V

    .line 90
    :cond_1
    const-string v1, "application"

    const/16 v2, 0x16

    move-object/from16 v0, p16

    invoke-virtual {p0, v1, v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    .line 91
    const-string v1, "nmaid"

    const/16 v2, 0x16

    move-object/from16 v0, p16

    invoke-virtual {p0, v1, v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    .line 92
    const-string v1, "command"

    const/16 v2, 0x16

    move-object/from16 v0, p17

    invoke-virtual {p0, v1, v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    .line 97
    if-eqz p18, :cond_3

    .line 98
    invoke-interface/range {p18 .. p18}, Lcom/nuance/a/a/a/b/c/c/c;->a()Ljava/util/Enumeration;

    move-result-object v3

    .line 99
    :goto_1
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 100
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v2, p18

    .line 101
    check-cast v2, Lcom/nuance/a/a/a/b/c/b/b/i;

    invoke-virtual {v2, v1}, Lcom/nuance/a/a/a/b/c/b/b/i;->b(Ljava/lang/String;)Lcom/nuance/a/a/a/b/c/b/b/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nuance/a/a/a/b/c/b/b/f;->c()S

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto :goto_1

    .line 106
    :sswitch_0
    move-object/from16 v0, p18

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/b/c/c/c;->e(Ljava/lang/String;)[B

    move-result-object v2

    const/4 v4, 0x4

    invoke-virtual {p0, v1, v2, v4}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;[BS)V

    goto :goto_1

    .line 83
    :cond_2
    const-string v1, "carrier"

    const/16 v2, 0xc1

    move-object/from16 v0, p11

    invoke-virtual {p0, v1, v0, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    goto :goto_0

    .line 103
    :sswitch_1
    move-object/from16 v0, p18

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/b/c/c/c;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0x16

    invoke-virtual {p0, v1, v2, v4}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    goto :goto_1

    .line 109
    :sswitch_2
    move-object/from16 v0, p18

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/b/c/c/c;->d(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;I)V

    goto :goto_1

    .line 112
    :sswitch_3
    move-object/from16 v0, p18

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/b/c/c/c;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0xc1

    invoke-virtual {p0, v1, v2, v4}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;Ljava/lang/String;S)V

    goto :goto_1

    .line 115
    :sswitch_4
    const/4 v2, 0x0

    const/4 v4, 0x5

    invoke-virtual {p0, v1, v2, v4}, Lcom/nuance/a/a/a/b/c/b/b/p;->a(Ljava/lang/String;[BS)V

    goto :goto_1

    .line 119
    :sswitch_5
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/b/p;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "PDXQueryBegin() Dictionary not accepted in optionalKeys"

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_1

    .line 124
    :sswitch_6
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/b/p;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v2, "PDXQueryBegin() Sequence not accepted in optionalKeys"

    invoke-virtual {v1, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_1

    .line 130
    :cond_3
    return-void

    .line 101
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x5 -> :sswitch_4
        0x10 -> :sswitch_6
        0x16 -> :sswitch_1
        0xc0 -> :sswitch_2
        0xc1 -> :sswitch_3
        0xe0 -> :sswitch_5
    .end sparse-switch
.end method
