.class public Lcom/nuance/a/a/a/b/c/b/b/r;
.super Lcom/nuance/a/a/a/b/c/b/b/l;
.source "PDXQueryError.java"

# interfaces
.implements Lcom/nuance/a/a/a/b/c/c/i;


# static fields
.field private static final a:Lcom/nuance/a/a/a/a/b/a/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/nuance/a/a/a/b/c/b/b/r;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/b/c/b/b/r;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 52
    const/16 v0, 0x7202

    invoke-direct {p0, v0, p1}, Lcom/nuance/a/a/a/b/c/b/b/l;-><init>(S[B)V

    .line 60
    return-void
.end method


# virtual methods
.method public final g()I
    .locals 2

    .prologue
    .line 64
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/r;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "PDXQueryError.getError()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 66
    const-string v0, "error"

    invoke-virtual {p0, v0}, Lcom/nuance/a/a/a/b/c/b/b/r;->d(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/r;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "PDXQueryError.getDescription()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 73
    const-string v0, "description"

    invoke-virtual {p0, v0}, Lcom/nuance/a/a/a/b/c/b/b/r;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_0

    .line 77
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
