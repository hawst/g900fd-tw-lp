.class public Lcom/nuance/a/a/a/b/c/b/b/s;
.super Lcom/nuance/a/a/a/b/c/b/b/l;
.source "PDXQueryParameter.java"


# static fields
.field private static final a:Lcom/nuance/a/a/a/a/b/a/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/nuance/a/a/a/b/c/b/b/s;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/b/c/b/b/s;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method public constructor <init>(Lcom/nuance/a/a/a/b/c/b/b/o;)V
    .locals 4

    .prologue
    const/16 v3, 0xe0

    const/16 v2, 0xc1

    .line 45
    const/16 v0, 0x203

    invoke-direct {p0, v0}, Lcom/nuance/a/a/a/b/c/b/b/l;-><init>(S)V

    .line 47
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/s;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/s;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "PDXQueryParameter()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 50
    :cond_0
    const-string v0, "name"

    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/b/o;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/nuance/a/a/a/b/c/b/b/s;->a(Ljava/lang/String;Ljava/lang/String;S)V

    .line 51
    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/b/o;->c()B

    move-result v0

    const/16 v1, 0x7f

    if-ne v0, v1, :cond_1

    .line 52
    const-string v0, "type"

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/nuance/a/a/a/b/c/b/b/s;->a(Ljava/lang/String;I)V

    .line 57
    :goto_0
    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/b/o;->c()B

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 83
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/s;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXQueryParameter() Unknown parameter type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/b/o;->c()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 87
    :goto_1
    return-void

    .line 54
    :cond_1
    const-string v0, "type"

    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/b/o;->c()B

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/nuance/a/a/a/b/c/b/b/s;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 59
    :sswitch_0
    const-string v0, "buffer_id"

    check-cast p1, Lcom/nuance/a/a/a/b/c/b/b/c;

    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/b/c;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/nuance/a/a/a/b/c/b/b/s;->a(Ljava/lang/String;I)V

    goto :goto_1

    .line 62
    :sswitch_1
    const-string v0, "text"

    check-cast p1, Lcom/nuance/a/a/a/b/c/b/b/e;

    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/b/e;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/nuance/a/a/a/b/c/b/b/s;->a(Ljava/lang/String;Ljava/lang/String;S)V

    goto :goto_1

    .line 65
    :sswitch_2
    const-string v0, "data"

    check-cast p1, Lcom/nuance/a/a/a/b/c/b/b/g;

    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/b/g;->a()[B

    move-result-object v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/nuance/a/a/a/b/c/b/b/s;->a(Ljava/lang/String;[BS)V

    goto :goto_1

    .line 68
    :sswitch_3
    const-string v0, "dict"

    check-cast p1, Lcom/nuance/a/a/a/b/c/b/b/h;

    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/b/h;->a()[B

    move-result-object v1

    invoke-virtual {p0, v0, v1, v3}, Lcom/nuance/a/a/a/b/c/b/b/s;->a(Ljava/lang/String;[BS)V

    goto :goto_1

    .line 73
    :sswitch_4
    const-string v0, "dict"

    check-cast p1, Lcom/nuance/a/a/a/b/c/b/b/v;

    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/b/v;->a()[B

    move-result-object v1

    invoke-virtual {p0, v0, v1, v3}, Lcom/nuance/a/a/a/b/c/b/b/s;->a(Ljava/lang/String;[BS)V

    goto :goto_1

    .line 76
    :sswitch_5
    const-string v0, "text"

    check-cast p1, Lcom/nuance/a/a/a/b/c/b/b/y;

    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/b/y;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/nuance/a/a/a/b/c/b/b/s;->a(Ljava/lang/String;Ljava/lang/String;S)V

    goto :goto_1

    .line 79
    :sswitch_6
    const-string v0, "dict"

    check-cast p1, Lcom/nuance/a/a/a/b/c/b/b/x;

    invoke-virtual {p1}, Lcom/nuance/a/a/a/b/c/b/b/x;->d()[B

    move-result-object v1

    invoke-virtual {p0, v0, v1, v3}, Lcom/nuance/a/a/a/b/c/b/b/s;->a(Ljava/lang/String;[BS)V

    goto :goto_1

    .line 57
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_5
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x5 -> :sswitch_3
        0x6 -> :sswitch_4
        0x7 -> :sswitch_4
        0x8 -> :sswitch_4
        0x7f -> :sswitch_6
    .end sparse-switch
.end method
