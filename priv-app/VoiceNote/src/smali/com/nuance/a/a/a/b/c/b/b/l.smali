.class public Lcom/nuance/a/a/a/b/c/b/b/l;
.super Lcom/nuance/a/a/a/b/c/b/b/i;
.source "PDXMessage.java"


# static fields
.field private static final a:Lcom/nuance/a/a/a/a/b/a/a$a;


# instance fields
.field private b:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/nuance/a/a/a/b/c/b/b/l;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/b/c/b/b/l;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method public constructor <init>(S)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/nuance/a/a/a/b/c/b/b/i;-><init>()V

    .line 64
    iput-short p1, p0, Lcom/nuance/a/a/a/b/c/b/b/l;->b:S

    .line 65
    return-void
.end method

.method public constructor <init>(S[B)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p2}, Lcom/nuance/a/a/a/b/c/b/b/i;-><init>([B)V

    .line 76
    iput-short p1, p0, Lcom/nuance/a/a/a/b/c/b/b/l;->b:S

    .line 77
    return-void
.end method


# virtual methods
.method public final d()[B
    .locals 5

    .prologue
    .line 97
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 99
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/l;->b:S

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 100
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/l;->b:S

    const v2, 0xff00

    and-int/2addr v0, v2

    shr-int/lit8 v0, v0, 0x8

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 103
    :try_start_0
    invoke-super {p0}, Lcom/nuance/a/a/a/b/c/b/b/i;->d()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    .line 104
    :catch_0
    move-exception v0

    .line 106
    sget-object v2, Lcom/nuance/a/a/a/b/c/b/b/l;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PDXMessage.toByteArray() "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ". "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final e()S
    .locals 1

    .prologue
    .line 83
    iget-short v0, p0, Lcom/nuance/a/a/a/b/c/b/b/l;->b:S

    return v0
.end method

.method public final f()[B
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Lcom/nuance/a/a/a/b/c/b/b/i;->d()[B

    move-result-object v0

    return-object v0
.end method
