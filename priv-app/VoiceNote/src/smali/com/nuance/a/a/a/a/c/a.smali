.class public Lcom/nuance/a/a/a/a/c/a;
.super Ljava/lang/Object;
.source "ProtocolBuilder.java"


# static fields
.field private static final a:Lcom/nuance/a/a/a/a/b/a/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/nuance/a/a/a/a/c/a;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/a/c/a;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(BBS[B)[B
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 39
    sget-object v0, Lcom/nuance/a/a/a/a/c/a;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    sget-object v0, Lcom/nuance/a/a/a/a/c/a;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Building XMode buffer: protocol="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " version="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cmd="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " payload len:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->a(Ljava/lang/Object;)V

    .line 45
    sget-object v0, Lcom/nuance/a/a/a/a/c/a;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0, p3}, Lcom/nuance/a/a/a/a/b/a/a$a;->a([B)V

    .line 49
    :cond_0
    array-length v0, p3

    .line 50
    add-int/lit8 v1, v0, 0x8

    .line 51
    new-array v1, v1, [B

    .line 52
    new-instance v2, Lcom/nuance/a/a/a/a/c/b;

    invoke-direct {v2, p0, p1, p2, v0}, Lcom/nuance/a/a/a/a/c/b;-><init>(BBSI)V

    .line 55
    invoke-virtual {v2}, Lcom/nuance/a/a/a/a/c/b;->a()[B

    move-result-object v2

    invoke-static {v2, v3, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    invoke-static {p3, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 59
    return-object v1
.end method

.method public static a([B[B)[B
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/4 v2, 0x0

    .line 65
    sget-object v0, Lcom/nuance/a/a/a/a/c/a;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    sget-object v0, Lcom/nuance/a/a/a/a/c/a;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "Appending session ID"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->a(Ljava/lang/Object;)V

    .line 68
    sget-object v0, Lcom/nuance/a/a/a/a/c/a;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0, p1}, Lcom/nuance/a/a/a/a/b/a/a$a;->a([B)V

    .line 69
    sget-object v0, Lcom/nuance/a/a/a/a/c/a;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0, p0}, Lcom/nuance/a/a/a/a/b/a/a$a;->a([B)V

    .line 72
    :cond_0
    array-length v0, p0

    add-int/lit8 v0, v0, 0x10

    new-array v0, v0, [B

    .line 73
    invoke-static {p1, v2, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 74
    array-length v1, p0

    invoke-static {p0, v2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    return-object v0
.end method
