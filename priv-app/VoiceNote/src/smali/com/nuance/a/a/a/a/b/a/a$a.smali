.class public abstract Lcom/nuance/a/a/a/a/b/a/a$a;
.super Ljava/lang/Object;
.source "LogFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/a/a/a/a/b/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation


# static fields
.field private static a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 126
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "0"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "2"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "3"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "4"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "5"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "6"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "7"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "8"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "9"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "A"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "B"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "C"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "D"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "E"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "F"

    aput-object v2, v0, v1

    sput-object v0, Lcom/nuance/a/a/a/a/b/a/a$a;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(B)Ljava/lang/String;
    .locals 4

    .prologue
    .line 131
    and-int/lit16 v0, p0, 0xf0

    int-to-byte v0, v0

    .line 132
    ushr-int/lit8 v0, v0, 0x4

    int-to-byte v0, v0

    .line 133
    and-int/lit8 v0, v0, 0xf

    int-to-byte v0, v0

    .line 135
    and-int/lit8 v1, p0, 0xf

    int-to-byte v1, v1

    .line 137
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/nuance/a/a/a/a/b/a/a$a;->a:[Ljava/lang/String;

    aget-object v0, v3, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/nuance/a/a/a/a/b/a/a$a;->a:[Ljava/lang/String;

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b([B)Ljava/lang/String;
    .locals 3

    .prologue
    .line 111
    if-nez p0, :cond_0

    .line 112
    const-string v0, ""

    .line 123
    :goto_0
    return-object v0

    .line 114
    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 116
    const/4 v0, 0x0

    :goto_1
    const/16 v2, 0x10

    if-ge v0, v2, :cond_3

    .line 118
    aget-byte v2, p0, v0

    invoke-static {v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->a(B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 119
    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    const/4 v2, 0x5

    if-eq v0, v2, :cond_1

    const/4 v2, 0x7

    if-eq v0, v2, :cond_1

    const/16 v2, 0x9

    if-ne v0, v2, :cond_2

    .line 120
    :cond_1
    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 116
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 123
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static c([B)[Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 74
    array-length v0, p0

    div-int/lit8 v0, v0, 0x8

    .line 75
    array-length v1, p0

    rem-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_0

    .line 76
    add-int/lit8 v0, v0, 0x1

    .line 77
    :cond_0
    new-array v6, v0, [Ljava/lang/String;

    .line 82
    :try_start_0
    new-instance v1, Ljava/lang/String;

    const-string v2, "ISO-8859-1"

    invoke-direct {v1, p0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move v5, v3

    move v4, v3

    .line 89
    :goto_1
    if-ge v5, v0, :cond_3

    .line 91
    const/16 v2, 0x29

    new-array v7, v2, [C

    move v2, v3

    .line 92
    :goto_2
    array-length v8, v7

    if-ge v2, v8, :cond_1

    .line 93
    const/16 v8, 0x20

    aput-char v8, v7, v2

    .line 92
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 85
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>([B)V

    goto :goto_0

    :cond_1
    move v2, v3

    .line 95
    :goto_3
    const/16 v8, 0x8

    if-ge v2, v8, :cond_2

    array-length v8, p0

    if-ge v4, v8, :cond_2

    .line 97
    aget-byte v8, p0, v4

    invoke-static {v8}, Lcom/nuance/a/a/a/a/b/a/a$a;->a(B)Ljava/lang/String;

    move-result-object v8

    .line 98
    mul-int/lit8 v9, v2, 0x3

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v10

    aput-char v10, v7, v9

    .line 99
    mul-int/lit8 v9, v2, 0x3

    add-int/lit8 v9, v9, 0x1

    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Ljava/lang/String;->charAt(I)C

    move-result v8

    aput-char v8, v7, v9

    .line 100
    mul-int/lit8 v8, v2, 0x2

    add-int/lit8 v8, v8, 0x1a

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v9

    aput-char v9, v7, v8

    .line 95
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 103
    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/lang/String;-><init>([C)V

    aput-object v2, v6, v5

    .line 89
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    .line 106
    :cond_3
    return-object v6
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;)V
.end method

.method public final a([B)V
    .locals 3

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/nuance/a/a/a/a/b/a/a$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "Buffer dump:"

    invoke-virtual {p0, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->a(Ljava/lang/Object;)V

    .line 66
    invoke-static {p1}, Lcom/nuance/a/a/a/a/b/a/a$a;->c([B)[Ljava/lang/String;

    move-result-object v1

    .line 67
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 68
    aget-object v2, v1, v0

    invoke-virtual {p0, v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->a(Ljava/lang/Object;)V

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_0
    return-void
.end method

.method public abstract a()Z
.end method

.method public abstract b(Ljava/lang/Object;)V
.end method

.method public abstract b()Z
.end method

.method public abstract c(Ljava/lang/Object;)V
.end method

.method public abstract c()Z
.end method

.method public abstract d(Ljava/lang/Object;)V
.end method

.method public abstract d()Z
.end method

.method public abstract e(Ljava/lang/Object;)V
.end method

.method public abstract e()Z
.end method

.method public abstract f()V
.end method

.method public abstract g()V
.end method
