.class public Lcom/nuance/a/a/a/b/c/b/b/i;
.super Lcom/nuance/a/a/a/b/c/b/b/f;
.source "PDXDictionary.java"

# interfaces
.implements Lcom/nuance/a/a/a/b/c/c/c;


# static fields
.field private static final a:Lcom/nuance/a/a/a/a/b/a/a$a;


# instance fields
.field private b:Ljava/util/Hashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/nuance/a/a/a/b/c/b/b/i;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    const/16 v0, 0xe0

    invoke-direct {p0, v0}, Lcom/nuance/a/a/a/b/c/b/b/f;-><init>(S)V

    .line 51
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    .line 52
    return-void
.end method

.method public constructor <init>([B)V
    .locals 4

    .prologue
    const/16 v1, 0xe0

    const/4 v3, 0x0

    .line 55
    invoke-direct {p0, v1}, Lcom/nuance/a/a/a/b/c/b/b/f;-><init>(S)V

    .line 63
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    .line 64
    array-length v0, p1

    if-lez v0, :cond_0

    .line 65
    aget-byte v0, p1, v3

    and-int/lit16 v0, v0, 0xff

    .line 66
    if-eq v0, v1, :cond_1

    .line 68
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "PDXDictionary() Expected a dictionary. "

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/nuance/a/a/a/b/c/b/b/i;->a([BI)I

    move-result v0

    .line 75
    new-array v1, v0, [B

    .line 76
    array-length v2, p1

    sub-int v0, v2, v0

    array-length v2, v1

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 77
    invoke-direct {p0, v1}, Lcom/nuance/a/a/a/b/c/b/b/i;->b([B)V

    goto :goto_0
.end method

.method public constructor <init>([BB)V
    .locals 1

    .prologue
    .line 82
    const/16 v0, 0xe0

    invoke-direct {p0, v0}, Lcom/nuance/a/a/a/b/c/b/b/f;-><init>(S)V

    .line 90
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    .line 91
    invoke-direct {p0, p1}, Lcom/nuance/a/a/a/b/c/b/b/i;->b([B)V

    .line 92
    return-void
.end method

.method private b([B)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 504
    move v0, v1

    .line 509
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 512
    add-int/lit8 v2, v0, 0x1

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    .line 513
    const/16 v3, 0x16

    if-eq v0, v3, :cond_1

    .line 515
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PDXDictionary.setContent() Expected an ASCII string but got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 568
    :cond_0
    return-void

    .line 520
    :cond_1
    invoke-static {p1, v2}, Lcom/nuance/a/a/a/b/c/b/b/i;->a([BI)I

    move-result v0

    .line 521
    invoke-static {v0}, Lcom/nuance/a/a/a/b/c/b/b/i;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 523
    new-array v3, v0, [B

    .line 524
    array-length v4, v3

    invoke-static {p1, v2, v3, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 525
    add-int/2addr v0, v2

    .line 527
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    .line 530
    add-int/lit8 v3, v0, 0x1

    aget-byte v0, p1, v0

    and-int/lit16 v4, v0, 0xff

    .line 531
    invoke-static {p1, v3}, Lcom/nuance/a/a/a/b/c/b/b/i;->a([BI)I

    move-result v0

    .line 532
    invoke-static {v0}, Lcom/nuance/a/a/a/b/c/b/b/i;->a(I)I

    move-result v5

    add-int/2addr v3, v5

    .line 534
    new-array v5, v0, [B

    .line 535
    array-length v6, v5

    invoke-static {p1, v3, v5, v1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 536
    add-int/2addr v0, v3

    .line 539
    sparse-switch v4, :sswitch_data_0

    .line 563
    sget-object v2, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "PDXDictionary.setContent() Unknown PDXClass type: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_0

    .line 541
    :sswitch_0
    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v4, Lcom/nuance/a/a/a/b/c/b/b/k;

    invoke-direct {v4, v5}, Lcom/nuance/a/a/a/b/c/b/b/k;-><init>([B)V

    invoke-virtual {v3, v2, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 544
    :sswitch_1
    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v4, Lcom/nuance/a/a/a/b/c/b/b/d;

    invoke-direct {v4, v5}, Lcom/nuance/a/a/a/b/c/b/b/d;-><init>([B)V

    invoke-virtual {v3, v2, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 547
    :sswitch_2
    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v4, Lcom/nuance/a/a/a/b/c/b/b/aa;

    invoke-direct {v4, v5}, Lcom/nuance/a/a/a/b/c/b/b/aa;-><init>([B)V

    invoke-virtual {v3, v2, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 550
    :sswitch_3
    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v4, Lcom/nuance/a/a/a/b/c/b/b/b;

    invoke-direct {v4, v5}, Lcom/nuance/a/a/a/b/c/b/b/b;-><init>([B)V

    invoke-virtual {v3, v2, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 553
    :sswitch_4
    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v4, Lcom/nuance/a/a/a/b/c/b/b/n;

    invoke-direct {v4}, Lcom/nuance/a/a/a/b/c/b/b/n;-><init>()V

    invoke-virtual {v3, v2, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 556
    :sswitch_5
    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v4, Lcom/nuance/a/a/a/b/c/b/b/i;

    invoke-direct {v4, v5, v1}, Lcom/nuance/a/a/a/b/c/b/b/i;-><init>([BB)V

    invoke-virtual {v3, v2, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 559
    :sswitch_6
    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v4, Lcom/nuance/a/a/a/b/c/b/b/w;

    invoke-direct {v4, v5}, Lcom/nuance/a/a/a/b/c/b/b/w;-><init>([B)V

    invoke-virtual {v3, v2, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 539
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x5 -> :sswitch_4
        0x10 -> :sswitch_6
        0x16 -> :sswitch_3
        0xc0 -> :sswitch_0
        0xc1 -> :sswitch_2
        0xe0 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public final a()Ljava/util/Enumeration;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v1, Lcom/nuance/a/a/a/b/c/b/b/k;

    invoke-direct {v1, p2}, Lcom/nuance/a/a/a/b/c/b/b/k;-><init>(I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/c/c;)V
    .locals 2

    .prologue
    .line 454
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key or value is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v0, p2

    .line 455
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/f;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/f;->c()S

    move-result v0

    const/16 v1, 0xe0

    if-eq v0, v1, :cond_2

    .line 457
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "PDXDictionary.addDictionary() value is not a valid dictionary."

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 459
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "value is not a valid dictionary. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 461
    :cond_2
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 462
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/nuance/a/a/a/b/c/c/l;)V
    .locals 2

    .prologue
    .line 477
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key or value is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v0, p2

    .line 478
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/f;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/f;->c()S

    move-result v0

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    .line 480
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "PDXDictionary.addSequence() value is not a valid sequence."

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 482
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "value is not a valid sequence. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 484
    :cond_2
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 436
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key or value is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 437
    :cond_1
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/b;

    invoke-direct {v0, p2}, Lcom/nuance/a/a/a/b/c/b/b/b;-><init>(Ljava/lang/String;)V

    .line 438
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v1, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;S)V
    .locals 2

    .prologue
    .line 114
    sparse-switch p3, :sswitch_data_0

    .line 120
    :goto_0
    return-void

    .line 116
    :sswitch_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v1, Lcom/nuance/a/a/a/b/c/b/b/aa;

    invoke-direct {v1, p2}, Lcom/nuance/a/a/a/b/c/b/b/aa;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 119
    :sswitch_1
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v1, Lcom/nuance/a/a/a/b/c/b/b/b;

    invoke-direct {v1, p2}, Lcom/nuance/a/a/a/b/c/b/b/b;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 114
    :sswitch_data_0
    .sparse-switch
        0x16 -> :sswitch_1
        0xc1 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 2

    .prologue
    .line 445
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key or value is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 446
    :cond_1
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/d;

    invoke-direct {v0, p2}, Lcom/nuance/a/a/a/b/c/b/b/d;-><init>([B)V

    .line 447
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v1, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    return-void
.end method

.method protected final a(Ljava/lang/String;[BS)V
    .locals 3

    .prologue
    .line 147
    sparse-switch p3, :sswitch_data_0

    .line 171
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXDictionary.put() Unknown PDXClass type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 175
    :goto_0
    return-void

    .line 149
    :sswitch_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v1, Lcom/nuance/a/a/a/b/c/b/b/k;

    invoke-direct {v1, p2}, Lcom/nuance/a/a/a/b/c/b/b/k;-><init>([B)V

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 152
    :sswitch_1
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v1, Lcom/nuance/a/a/a/b/c/b/b/d;

    invoke-direct {v1, p2}, Lcom/nuance/a/a/a/b/c/b/b/d;-><init>([B)V

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 155
    :sswitch_2
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v1, Lcom/nuance/a/a/a/b/c/b/b/aa;

    invoke-direct {v1, p2}, Lcom/nuance/a/a/a/b/c/b/b/aa;-><init>([B)V

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 158
    :sswitch_3
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v1, Lcom/nuance/a/a/a/b/c/b/b/b;

    invoke-direct {v1, p2}, Lcom/nuance/a/a/a/b/c/b/b/b;-><init>([B)V

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 161
    :sswitch_4
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v1, Lcom/nuance/a/a/a/b/c/b/b/n;

    invoke-direct {v1}, Lcom/nuance/a/a/a/b/c/b/b/n;-><init>()V

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 164
    :sswitch_5
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v1, Lcom/nuance/a/a/a/b/c/b/b/i;

    const/4 v2, 0x0

    invoke-direct {v1, p2, v2}, Lcom/nuance/a/a/a/b/c/b/b/i;-><init>([BB)V

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 167
    :sswitch_6
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    new-instance v1, Lcom/nuance/a/a/a/b/c/b/b/w;

    invoke-direct {v1, p2}, Lcom/nuance/a/a/a/b/c/b/b/w;-><init>([B)V

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 147
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x5 -> :sswitch_4
        0x10 -> :sswitch_6
        0x16 -> :sswitch_3
        0xc0 -> :sswitch_0
        0xc1 -> :sswitch_2
        0xe0 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 217
    if-nez p1, :cond_0

    .line 218
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "PDXDictionary.containsKey key is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Ljava/lang/String;)Lcom/nuance/a/a/a/b/c/b/b/f;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/f;

    return-object v0
.end method

.method protected final b(I)Ljava/lang/String;
    .locals 7

    .prologue
    .line 628
    const-string v1, ""

    .line 629
    const-string v3, ""

    .line 630
    const-string v0, ""

    .line 631
    const/4 v2, 0x0

    move-object v4, v3

    :goto_0
    add-int/lit8 v3, p1, -0x1

    if-ge v2, v3, :cond_0

    .line 632
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 631
    add-int/lit8 v2, v2, 0x1

    move-object v4, v3

    goto :goto_0

    .line 634
    :cond_0
    if-lez p1, :cond_4

    .line 635
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 638
    :goto_1
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v5

    .line 639
    if-eqz p1, :cond_3

    .line 640
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{ \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 642
    :goto_2
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 643
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 644
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nuance/a/a/a/b/c/b/b/f;

    .line 645
    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/c/b/b/f;->c()S

    move-result v6

    sparse-switch v6, :sswitch_data_0

    move-object v0, v3

    :goto_3
    move-object v3, v0

    .line 668
    goto :goto_2

    .line 647
    :sswitch_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ": <INT> "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast v1, Lcom/nuance/a/a/a/b/c/b/b/k;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/c/b/b/k;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 648
    goto :goto_2

    .line 650
    :sswitch_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ": <BYTES> \""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast v1, Lcom/nuance/a/a/a/b/c/b/b/d;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/c/b/b/d;->a()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\"\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 651
    goto :goto_2

    .line 653
    :sswitch_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ": <UTF8> \""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast v1, Lcom/nuance/a/a/a/b/c/b/b/aa;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/c/b/b/aa;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\"\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 654
    goto/16 :goto_2

    .line 656
    :sswitch_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ": <ASCII> \""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast v1, Lcom/nuance/a/a/a/b/c/b/b/b;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/b/c/b/b/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\"\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 657
    goto/16 :goto_2

    .line 659
    :sswitch_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": <NULL> \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 660
    goto/16 :goto_2

    .line 662
    :sswitch_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ": "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast v1, Lcom/nuance/a/a/a/b/c/b/b/i;

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {v1, v3}, Lcom/nuance/a/a/a/b/c/b/b/i;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 663
    goto/16 :goto_2

    .line 665
    :sswitch_6
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ": "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast v1, Lcom/nuance/a/a/a/b/c/b/b/w;

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {v1, v3}, Lcom/nuance/a/a/a/b/c/b/b/w;->j(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 669
    :cond_1
    if-eqz p1, :cond_2

    .line 670
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 675
    :cond_2
    return-object v3

    :cond_3
    move-object v3, v1

    goto/16 :goto_2

    :cond_4
    move-object v2, v0

    goto/16 :goto_1

    .line 645
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x5 -> :sswitch_4
        0x10 -> :sswitch_6
        0x16 -> :sswitch_3
        0xc0 -> :sswitch_0
        0xc1 -> :sswitch_2
        0xe0 -> :sswitch_5
    .end sparse-switch
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 468
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key or value is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 469
    :cond_0
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/k;

    invoke-direct {v0, p2}, Lcom/nuance/a/a/a/b/c/b/b/k;-><init>(I)V

    .line 470
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v1, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 491
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key or value is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 492
    :cond_1
    new-instance v0, Lcom/nuance/a/a/a/b/c/b/b/aa;

    invoke-direct {v0, p2}, Lcom/nuance/a/a/a/b/c/b/b/aa;-><init>(Ljava/lang/String;)V

    .line 493
    iget-object v1, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v1, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    return-void
.end method

.method public final b()[B
    .locals 6

    .prologue
    .line 574
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 575
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    .line 576
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 577
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 578
    new-instance v3, Lcom/nuance/a/a/a/b/c/b/b/b;

    invoke-direct {v3, v0}, Lcom/nuance/a/a/a/b/c/b/b/b;-><init>(Ljava/lang/String;)V

    .line 580
    :try_start_0
    invoke-virtual {v3}, Lcom/nuance/a/a/a/b/c/b/b/b;->b()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 581
    iget-object v3, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v3, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/f;

    .line 582
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/f;->c()S

    move-result v3

    sparse-switch v3, :sswitch_data_0

    goto :goto_0

    .line 587
    :sswitch_0
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/d;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/d;->b()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 605
    :catch_0
    move-exception v0

    .line 607
    sget-object v3, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PDXDictionary.getContent() "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ". "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_0

    .line 584
    :sswitch_1
    :try_start_1
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/k;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/k;->b()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto :goto_0

    .line 590
    :sswitch_2
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/aa;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/aa;->b()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto :goto_0

    .line 593
    :sswitch_3
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/b;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/b;->b()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto :goto_0

    .line 596
    :sswitch_4
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/n;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/n;->a()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto :goto_0

    .line 599
    :sswitch_5
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/i;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/i;->d()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto/16 :goto_0

    .line 602
    :sswitch_6
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/w;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/w;->b()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 611
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    .line 582
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x5 -> :sswitch_4
        0x10 -> :sswitch_6
        0x16 -> :sswitch_3
        0xc0 -> :sswitch_1
        0xc1 -> :sswitch_2
        0xe0 -> :sswitch_5
    .end sparse-switch
.end method

.method public final c(Ljava/lang/String;)S
    .locals 2

    .prologue
    .line 231
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/f;

    .line 233
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/f;->c()S

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 252
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/f;

    .line 254
    if-nez v0, :cond_2

    .line 256
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 257
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXDictionary.getInteger() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 259
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "key does not exist. "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 261
    :cond_2
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/f;->c()S

    move-result v1

    const/16 v2, 0xc0

    if-eq v1, v2, :cond_4

    .line 263
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 264
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXDictionary.getInteger() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a PDXInteger. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 266
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "key is of wrong type. "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 269
    :cond_4
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/k;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/k;->a()I

    move-result v0

    return v0
.end method

.method public d()[B
    .locals 1

    .prologue
    .line 618
    invoke-virtual {p0}, Lcom/nuance/a/a/a/b/c/b/b/i;->b()[B

    move-result-object v0

    invoke-super {p0, v0}, Lcom/nuance/a/a/a/b/c/b/b/f;->a([B)[B

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)[B
    .locals 3

    .prologue
    .line 286
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/f;

    .line 288
    if-nez v0, :cond_1

    .line 290
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXDictionary.getByteString() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 292
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "key does not exist. "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 294
    :cond_1
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/f;->c()S

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_2

    .line 296
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXDictionary.getByteString() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a PDXByteString. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 298
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "key is of wrong type. "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 301
    :cond_2
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/d;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/d;->a()[B

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 318
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/f;

    .line 320
    if-nez v0, :cond_1

    .line 322
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXDictionary.getUTF8String() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 324
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "key does not exist. "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 326
    :cond_1
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/f;->c()S

    move-result v1

    const/16 v2, 0xc1

    if-eq v1, v2, :cond_2

    .line 328
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXDictionary.getUTF8String() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a PDXUTF8String. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 330
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "key is of wrong type. "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 333
    :cond_2
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/aa;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/aa;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 350
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/f;

    .line 352
    if-nez v0, :cond_1

    .line 354
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXDictionary.getAsciiString() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 356
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "key does not exist. "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 358
    :cond_1
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/f;->c()S

    move-result v1

    const/16 v2, 0x16

    if-eq v1, v2, :cond_2

    .line 360
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXDictionary.getAsciiString() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a PDXAsciiString. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 362
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "key is of wrong type. "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 365
    :cond_2
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/b;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/b;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h(Ljava/lang/String;)Lcom/nuance/a/a/a/b/c/c/c;
    .locals 3

    .prologue
    .line 382
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/f;

    .line 384
    if-nez v0, :cond_1

    .line 386
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXDictionary.getDictionary() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 388
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "key does not exist. "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 390
    :cond_1
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/f;->c()S

    move-result v1

    const/16 v2, 0xe0

    if-eq v1, v2, :cond_2

    .line 392
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXDictionary.getDictionary() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a PDXDictionary. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 394
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "key is of wrong type. "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 397
    :cond_2
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/i;

    return-object v0
.end method

.method public final i(Ljava/lang/String;)Lcom/nuance/a/a/a/b/c/c/l;
    .locals 3

    .prologue
    .line 414
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/i;->b:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/f;

    .line 416
    if-nez v0, :cond_1

    .line 418
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXDictionary.getSequence() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 420
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "key does not exist. "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 422
    :cond_1
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/c/b/b/f;->c()S

    move-result v1

    const/16 v2, 0x10

    if-eq v1, v2, :cond_2

    .line 424
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/i;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PDXDictionary.getSequence() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a PDXSequence. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 426
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "key is of wrong type. "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 429
    :cond_2
    check-cast v0, Lcom/nuance/a/a/a/b/c/b/b/w;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 623
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/nuance/a/a/a/b/c/b/b/i;->b(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
