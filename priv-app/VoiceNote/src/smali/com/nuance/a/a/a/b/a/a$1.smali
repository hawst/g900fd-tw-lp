.class final Lcom/nuance/a/a/a/b/a/a$1;
.super Ljava/lang/Object;
.source "XMode.java"

# interfaces
.implements Lcom/nuance/a/a/a/a/b/a/e$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/nuance/a/a/a/b/a/a;->a([BLjava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/nuance/a/a/a/b/a/a;


# direct methods
.method constructor <init>(Lcom/nuance/a/a/a/b/a/a;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lcom/nuance/a/a/a/b/a/a$1;->a:Lcom/nuance/a/a/a/b/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 452
    :try_start_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a$1;->a:Lcom/nuance/a/a/a/b/a/a;

    invoke-static {v0}, Lcom/nuance/a/a/a/b/a/a;->a(Lcom/nuance/a/a/a/b/a/a;)S

    .line 454
    invoke-static {}, Lcom/nuance/a/a/a/b/a/a;->c()Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    invoke-static {}, Lcom/nuance/a/a/a/b/a/a;->c()Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Session Idle for too long, longer than ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/a/a/a/b/a/a$1;->a:Lcom/nuance/a/a/a/b/a/a;

    invoke-static {v2}, Lcom/nuance/a/a/a/b/a/a;->b(Lcom/nuance/a/a/a/b/a/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 457
    :cond_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a$1;->a:Lcom/nuance/a/a/a/b/a/a;

    const/4 v1, 0x2

    iput-byte v1, v0, Lcom/nuance/a/a/a/b/a/a;->d:B

    .line 458
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a$1;->a:Lcom/nuance/a/a/a/b/a/a;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/nuance/a/a/a/b/a/a;->a(Lcom/nuance/a/a/a/b/a/a;B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 465
    :cond_1
    :goto_0
    return-void

    .line 459
    :catch_0
    move-exception v0

    .line 461
    invoke-static {}, Lcom/nuance/a/a/a/b/a/a;->c()Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 462
    invoke-static {}, Lcom/nuance/a/a/a/b/a/a;->c()Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "XMode.sendXModeMsg() "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_0
.end method
