.class public Lcom/nuance/a/a/a/b/c/b/b/aa;
.super Lcom/nuance/a/a/a/b/c/b/b/f;
.source "PDXUTF8String.java"


# static fields
.field private static final a:Lcom/nuance/a/a/a/a/b/a/a$a;


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/nuance/a/a/a/b/c/b/b/aa;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/b/c/b/b/aa;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    const/16 v0, 0xc1

    invoke-direct {p0, v0}, Lcom/nuance/a/a/a/b/c/b/b/f;-><init>(S)V

    .line 46
    iput-object p1, p0, Lcom/nuance/a/a/a/b/c/b/b/aa;->b:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public constructor <init>([B)V
    .locals 4

    .prologue
    .line 50
    const/16 v0, 0xc1

    invoke-direct {p0, v0}, Lcom/nuance/a/a/a/b/c/b/b/f;-><init>(S)V

    .line 52
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p1, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/aa;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    return-void

    .line 53
    :catch_0
    move-exception v0

    .line 55
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/b/aa;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PDXUTF8String() "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 57
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/aa;->b:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/aa;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()[B
    .locals 4

    .prologue
    .line 76
    :try_start_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/aa;->b:Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-super {p0, v0}, Lcom/nuance/a/a/a/b/c/b/b/f;->a([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    .line 77
    :catch_0
    move-exception v0

    .line 79
    sget-object v1, Lcom/nuance/a/a/a/b/c/b/b/aa;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PDXUTF8String().toByteArray() "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 81
    iget-object v0, p0, Lcom/nuance/a/a/a/b/c/b/b/aa;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-super {p0, v0}, Lcom/nuance/a/a/a/b/c/b/b/f;->a([B)[B

    move-result-object v0

    goto :goto_0
.end method
