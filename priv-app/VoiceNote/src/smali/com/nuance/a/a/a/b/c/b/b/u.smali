.class public Lcom/nuance/a/a/a/b/c/b/b/u;
.super Lcom/nuance/a/a/a/b/c/b/b/l;
.source "PDXQueryRetry.java"

# interfaces
.implements Lcom/nuance/a/a/a/b/c/c/k;


# static fields
.field private static final a:Lcom/nuance/a/a/a/a/b/a/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/nuance/a/a/a/b/c/b/b/u;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/b/c/b/b/u;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 41
    const/16 v0, 0x7205

    invoke-direct {p0, v0, p1}, Lcom/nuance/a/a/a/b/c/b/b/l;-><init>(S[B)V

    .line 49
    return-void
.end method


# virtual methods
.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/u;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "PDXQueryRetry.getName()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 55
    const-string v0, "name"

    invoke-virtual {p0, v0}, Lcom/nuance/a/a/a/b/c/b/b/u;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_0

    .line 59
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final h()I
    .locals 2

    .prologue
    .line 65
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/u;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "PDXQueryRetry.getCause()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 69
    :try_start_0
    const-string v0, "cause"

    invoke-virtual {p0, v0}, Lcom/nuance/a/a/a/b/c/b/b/u;->d(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 74
    :goto_0
    return v0

    .line 71
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    sget-object v0, Lcom/nuance/a/a/a/b/c/b/b/u;->a:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "PDXQueryRetry.getPrompt()"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 83
    :try_start_0
    const-string v0, "prompt"

    invoke-virtual {p0, v0}, Lcom/nuance/a/a/a/b/c/b/b/u;->f(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 88
    :goto_0
    if-eqz v0, :cond_0

    .line 91
    :goto_1
    return-object v0

    .line 85
    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_0

    .line 91
    :cond_0
    const-string v0, ""

    goto :goto_1
.end method
