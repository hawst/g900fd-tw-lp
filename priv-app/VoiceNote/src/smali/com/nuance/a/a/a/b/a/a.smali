.class public Lcom/nuance/a/a/a/b/a/a;
.super Ljava/lang/Object;
.source "XMode.java"

# interfaces
.implements Lcom/nuance/a/a/a/a/b/a/b$b;
.implements Lcom/nuance/a/a/a/a/b/a/d$a;
.implements Lcom/nuance/a/a/a/a/b/a/d$d;
.implements Lcom/nuance/a/a/a/a/b/a/d$e;
.implements Lcom/nuance/a/a/a/a/b/a/d$f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nuance/a/a/a/b/a/a$a;
    }
.end annotation


# static fields
.field private static i:Lcom/nuance/a/a/a/a/b/a/a$a;


# instance fields
.field private A:Lcom/nuance/a/a/a/b/a/a$a;

.field protected a:Ljava/lang/String;

.field protected b:S

.field c:Lcom/nuance/a/a/a/a/b/a/b;

.field protected d:B

.field public e:[B

.field protected f:I

.field protected g:S

.field protected h:S

.field private j:Lcom/nuance/a/a/a/a/b/a/e$a;

.field private k:I

.field private l:Lcom/nuance/a/a/a/a/b/a/e$a;

.field private m:I

.field private n:Lcom/nuance/a/a/a/a/b/a/e$a;

.field private o:Ljava/util/Vector;

.field private p:Z

.field private q:Ljava/util/Vector;

.field private r:Ljava/lang/Object;

.field private s:Lcom/nuance/a/a/a/a/c/b;

.field private t:S

.field private u:S

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:[B

.field private y:Lcom/nuance/a/a/a/a/b/a/d;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/nuance/a/a/a/b/a/a;

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a;->a(Ljava/lang/Class;)Lcom/nuance/a/a/a/a/b/a/a$a;

    move-result-object v0

    sput-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;SLjava/lang/String;[BLjava/lang/String;Lcom/nuance/a/a/a/b/a/a$a;Ljava/util/Vector;Lcom/nuance/a/a/a/a/b/a/b;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const/16 v0, 0x1e

    iput v0, p0, Lcom/nuance/a/a/a/b/a/a;->k:I

    .line 81
    const/16 v0, 0x32

    iput v0, p0, Lcom/nuance/a/a/a/b/a/a;->m:I

    .line 89
    iput-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->a:Ljava/lang/String;

    .line 90
    iput-short v2, p0, Lcom/nuance/a/a/a/b/a/a;->b:S

    .line 96
    iput-boolean v2, p0, Lcom/nuance/a/a/a/b/a/a;->p:Z

    .line 97
    iput-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->q:Ljava/util/Vector;

    .line 100
    iput-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    .line 127
    iput-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->s:Lcom/nuance/a/a/a/a/c/b;

    .line 128
    iput-short v2, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    .line 129
    const/16 v0, 0x9

    iput-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->u:S

    .line 132
    const-string v0, "Not specified"

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->v:Ljava/lang/String;

    .line 133
    const-string v0, "Not specified"

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->w:Ljava/lang/String;

    .line 134
    iput-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->x:[B

    .line 135
    iput-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    .line 171
    const-string v0, ""

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->z:Ljava/lang/String;

    .line 210
    iput-object p1, p0, Lcom/nuance/a/a/a/b/a/a;->a:Ljava/lang/String;

    .line 211
    iput-short p2, p0, Lcom/nuance/a/a/a/b/a/a;->b:S

    .line 212
    iput-object p3, p0, Lcom/nuance/a/a/a/b/a/a;->v:Ljava/lang/String;

    .line 213
    iput-object p4, p0, Lcom/nuance/a/a/a/b/a/a;->x:[B

    .line 214
    iput-object p5, p0, Lcom/nuance/a/a/a/b/a/a;->w:Ljava/lang/String;

    .line 215
    iput-object p6, p0, Lcom/nuance/a/a/a/b/a/a;->A:Lcom/nuance/a/a/a/b/a/a$a;

    .line 216
    if-eqz p7, :cond_3

    .line 217
    iput-object p7, p0, Lcom/nuance/a/a/a/b/a/a;->o:Ljava/util/Vector;

    .line 222
    :goto_0
    iput-object p8, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    .line 224
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode() server: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " port: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 227
    :cond_0
    invoke-virtual {p7}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    .line 228
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 229
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/b/c;

    .line 231
    sget-object v2, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v2}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 232
    sget-object v2, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "XMode() "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->d()Lcom/nuance/a/a/a/b/b/c$a;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->b()[B

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 234
    :cond_2
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->d()Lcom/nuance/a/a/a/b/b/c$a;

    move-result-object v2

    sget-object v3, Lcom/nuance/a/a/a/b/b/c$a;->a:Lcom/nuance/a/a/a/b/b/c$a;

    if-ne v2, v3, :cond_1

    .line 235
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "IdleSessionTimeout"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 236
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->b()[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 237
    if-lez v0, :cond_1

    .line 238
    iput v0, p0, Lcom/nuance/a/a/a/b/a/a;->m:I

    goto :goto_1

    .line 220
    :cond_3
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->o:Ljava/util/Vector;

    goto/16 :goto_0

    .line 241
    :cond_4
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ConnectionTimeout"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 242
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->b()[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/nuance/a/a/a/b/a/a;->k:I

    goto/16 :goto_1

    .line 244
    :cond_5
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SSL_Socket_Enable"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SSL_Cert_Summary"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SSL_Cert_Data"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SSL_SelfSigned_Cert"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 249
    :cond_6
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SSL_Socket_Enable"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->b()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    const-string v3, "TRUE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->b()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 251
    :cond_7
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/nuance/a/a/a/b/a/a;->p:Z

    .line 253
    :cond_8
    iget-object v2, p0, Lcom/nuance/a/a/a/b/a/a;->q:Ljava/util/Vector;

    if-nez v2, :cond_9

    .line 254
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/nuance/a/a/a/b/a/a;->q:Ljava/util/Vector;

    .line 256
    :cond_9
    iget-object v2, p0, Lcom/nuance/a/a/a/b/a/a;->q:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 267
    :cond_a
    const/4 v0, 0x3

    iput-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    .line 279
    return-void
.end method

.method static synthetic a(Lcom/nuance/a/a/a/b/a/a;)S
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x3

    iput-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    return v0
.end method

.method private a(B)V
    .locals 5

    .prologue
    .line 1219
    :try_start_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    new-instance v1, Lcom/nuance/a/a/a/a/b/a/b$a;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/nuance/a/a/a/a/b/a/b$a;-><init>(BLjava/lang/Object;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    iget-object v3, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    invoke-interface {v3}, Lcom/nuance/a/a/a/a/b/a/b;->a()[Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-interface {v0, v1, p0, v2, v3}, Lcom/nuance/a/a/a/a/b/a/b;->a(Ljava/lang/Object;Lcom/nuance/a/a/a/a/b/a/b$b;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1226
    :cond_0
    :goto_0
    return-void

    .line 1220
    :catch_0
    move-exception v0

    .line 1222
    sget-object v1, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1223
    sget-object v1, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "XMode.sendCmdMsg() "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Lcom/nuance/a/a/a/a/c/b;[B)V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 982
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 983
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.parseXModeMsg() protocol: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-byte v2, p1, Lcom/nuance/a/a/a/a/c/b;->a:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cmd: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-short v2, p1, Lcom/nuance/a/a/a/a/c/b;->c:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 988
    :cond_0
    iget-byte v0, p1, Lcom/nuance/a/a/a/a/c/b;->a:B

    sparse-switch v0, :sswitch_data_0

    .line 1099
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1100
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.parseXModeMsg() unknown protocol: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-byte v2, p1, Lcom/nuance/a/a/a/a/c/b;->a:B

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 1104
    :cond_1
    :goto_0
    :sswitch_0
    return-void

    .line 990
    :sswitch_1
    iget-short v0, p1, Lcom/nuance/a/a/a/a/c/b;->c:S

    sparse-switch v0, :sswitch_data_1

    .line 1034
    :goto_1
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->A:Lcom/nuance/a/a/a/b/a/a$a;

    invoke-interface {v0, p1, p2}, Lcom/nuance/a/a/a/b/a/a$a;->a(Lcom/nuance/a/a/a/a/c/b;[B)V

    goto :goto_0

    .line 996
    :sswitch_2
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->l:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    new-array v0, v6, [B

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->e:[B

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->e:[B

    invoke-static {p2, v3, v0, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->e:[B

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->e:[B

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->e:[B

    invoke-static {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->z:Ljava/lang/String;

    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->z:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->f()V

    :try_start_0
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received COP_Connected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/a/a/a/b/a/a;->z:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    :cond_2
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/nuance/a/a/a/b/a/a;->a(B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->g()V

    .line 998
    new-instance v0, Lcom/nuance/a/a/a/b/a/a$5;

    invoke-direct {v0, p0}, Lcom/nuance/a/a/a/b/a/a$5;-><init>(Lcom/nuance/a/a/a/b/a/a;)V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    .line 1016
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    iget v2, p0, Lcom/nuance/a/a/a/b/a/a;->m:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;J)V

    goto :goto_1

    .line 996
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->g()V

    throw v0

    .line 1021
    :sswitch_3
    invoke-static {p2, v3}, Lcom/nuance/a/a/a/a/d/b;->b([BI)I

    move-result v0

    iput v0, p0, Lcom/nuance/a/a/a/b/a/a;->f:I

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    goto :goto_1

    .line 1026
    :sswitch_4
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    if-eqz v0, :cond_3

    .line 1027
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    .line 1028
    :cond_3
    invoke-static {p2, v3}, Lcom/nuance/a/a/a/a/d/b;->a([BI)S

    move-result v0

    iput-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->u:S

    iput-byte v4, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    const/4 v0, 0x6

    iput-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.parseXModeMsgCopDisconnect() Received COP DISCONNECT. "

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->d(Ljava/lang/Object;)V

    :cond_4
    invoke-direct {p0, v5}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    goto/16 :goto_1

    .line 1033
    :sswitch_5
    const/4 v0, 0x7

    iput-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    iput-byte v4, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.parseXModeMsgCopConnectFailed() COP CONNECT failure. "

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    :cond_5
    invoke-direct {p0, v5}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    goto/16 :goto_1

    .line 1043
    :sswitch_6
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1044
    new-instance v0, Lcom/nuance/a/a/a/b/a/a$6;

    invoke-direct {v0, p0}, Lcom/nuance/a/a/a/b/a/a$6;-><init>(Lcom/nuance/a/a/a/b/a/a;)V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    .line 1062
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    iget v2, p0, Lcom/nuance/a/a/a/b/a/a;->m:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;J)V

    .line 1065
    :cond_6
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->A:Lcom/nuance/a/a/a/b/a/a$a;

    invoke-interface {v0, p1, p2}, Lcom/nuance/a/a/a/b/a/a$a;->a(Lcom/nuance/a/a/a/a/c/b;[B)V

    goto/16 :goto_0

    .line 1070
    :sswitch_7
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1071
    new-instance v0, Lcom/nuance/a/a/a/b/a/a$7;

    invoke-direct {v0, p0}, Lcom/nuance/a/a/a/b/a/a$7;-><init>(Lcom/nuance/a/a/a/b/a/a;)V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    .line 1089
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    iget v2, p0, Lcom/nuance/a/a/a/b/a/a;->m:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;J)V

    .line 1092
    :cond_7
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->A:Lcom/nuance/a/a/a/b/a/a$a;

    invoke-interface {v0, p1, p2}, Lcom/nuance/a/a/a/b/a/a$a;->a(Lcom/nuance/a/a/a/a/c/b;[B)V

    goto/16 :goto_0

    .line 988
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_7
        0x2 -> :sswitch_6
        0x3 -> :sswitch_1
        0xf -> :sswitch_0
    .end sparse-switch

    .line 990
    :sswitch_data_1
    .sparse-switch
        0x101 -> :sswitch_2
        0x102 -> :sswitch_3
        0x200 -> :sswitch_4
        0x300 -> :sswitch_5
    .end sparse-switch
.end method

.method static synthetic a(Lcom/nuance/a/a/a/b/a/a;B)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    return-void
.end method

.method static synthetic b(Lcom/nuance/a/a/a/b/a/a;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/nuance/a/a/a/b/a/a;->m:I

    return v0
.end method

.method static synthetic c()Lcom/nuance/a/a/a/a/b/a/a$a;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 731
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 732
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.closeSocketCallback() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/nuance/a/a/a/b/a/a;->z:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 735
    :cond_0
    const/4 v0, 0x3

    iput-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    .line 737
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->j:Lcom/nuance/a/a/a/a/b/a/e$a;

    if-eqz v0, :cond_1

    .line 738
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->j:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    .line 739
    iput-object v3, p0, Lcom/nuance/a/a/a/b/a/a;->j:Lcom/nuance/a/a/a/a/b/a/e$a;

    .line 742
    :cond_1
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    if-eqz v0, :cond_2

    .line 743
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    .line 744
    iput-object v3, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    .line 747
    :cond_2
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    if-eqz v0, :cond_3

    .line 748
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->A:Lcom/nuance/a/a/a/b/a/a$a;

    iget-short v1, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    iget-short v2, p0, Lcom/nuance/a/a/a/b/a/a;->u:S

    invoke-interface {v0, v1, v2}, Lcom/nuance/a/a/a/b/a/a$a;->a(SS)V

    .line 750
    :cond_3
    iput-object v3, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    .line 751
    iput-object v3, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    .line 752
    iput-object v3, p0, Lcom/nuance/a/a/a/b/a/a;->e:[B

    .line 754
    const-string v0, ""

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->z:Ljava/lang/String;

    .line 756
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 363
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.startStreaming() audio id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 367
    :cond_0
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    if-eq v0, v3, :cond_1

    .line 372
    :goto_0
    return-void

    .line 370
    :cond_1
    const/4 v0, 0x6

    new-array v0, v0, [B

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/nuance/a/a/a/a/d/b;->a(I[BI)V

    iget-short v1, p0, Lcom/nuance/a/a/a/b/a/a;->g:S

    const/4 v2, 0x4

    invoke-static {v1, v0, v2}, Lcom/nuance/a/a/a/a/d/b;->a(S[BI)V

    const/16 v1, 0x12

    const/16 v2, 0x101

    invoke-static {v3, v1, v2, v0}, Lcom/nuance/a/a/a/a/c/a;->a(BBS[B)[B

    move-result-object v0

    const-string v1, "SEND_VAP_RECORD_BEGIN"

    invoke-virtual {p0, v0, v1}, Lcom/nuance/a/a/a/b/a/a;->a([BLjava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lcom/nuance/a/a/a/a/b/a/d$c;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v3, 0x0

    .line 508
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 509
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.openSocketCallback() "

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 511
    :cond_0
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->a:Lcom/nuance/a/a/a/a/b/a/d$c;

    if-ne p1, v0, :cond_3

    .line 512
    iput-object p2, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    .line 513
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    if-nez v0, :cond_2

    .line 517
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    .line 546
    :cond_1
    :goto_0
    return-void

    .line 520
    :cond_2
    const/4 v0, 0x2

    iput-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    .line 521
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/a/b/a/d;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 523
    :cond_3
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->b:Lcom/nuance/a/a/a/a/b/a/d$c;

    if-ne p1, v0, :cond_5

    .line 525
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 526
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.openSocketCallback() NETWORK_ERROR"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 528
    :cond_4
    const/4 v0, 0x3

    iput-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    .line 529
    iput-short v2, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    .line 530
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->A:Lcom/nuance/a/a/a/b/a/a$a;

    iget-short v1, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    iget-short v2, p0, Lcom/nuance/a/a/a/b/a/a;->u:S

    invoke-interface {v0, v1, v2}, Lcom/nuance/a/a/a/b/a/a$a;->a(SS)V

    .line 531
    iput-object v3, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    .line 532
    iput-object v3, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    .line 533
    iput-object v3, p0, Lcom/nuance/a/a/a/b/a/a;->e:[B

    .line 535
    const-string v0, ""

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->z:Ljava/lang/String;

    goto :goto_0

    .line 538
    :cond_5
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->c:Lcom/nuance/a/a/a/a/b/a/d$c;

    if-ne p1, v0, :cond_1

    .line 539
    iput-short v2, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    .line 541
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 542
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.openSocketCallback() NETWORK_MEMORY_ERROR"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lcom/nuance/a/a/a/a/b/a/d$c;Ljava/lang/Object;IILjava/lang/Object;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 671
    check-cast p5, Ljava/lang/String;

    .line 674
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->a:Lcom/nuance/a/a/a/a/b/a/d$c;

    if-ne p1, v0, :cond_2

    if-ne p3, p4, :cond_2

    .line 676
    const-string v0, "SEND_COP_CONNECT"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 677
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    .line 727
    :cond_0
    :goto_0
    return-void

    .line 682
    :cond_1
    const-string v0, "SEND_COP_DISCONNECT"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 683
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    invoke-interface {v0, p2}, Lcom/nuance/a/a/a/a/b/a/d;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 694
    :cond_2
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->b:Lcom/nuance/a/a/a/a/b/a/d$c;

    if-ne p1, v0, :cond_3

    .line 695
    iget-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    if-eq v0, v1, :cond_0

    iget-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    if-eq v0, v2, :cond_0

    .line 696
    iput-short v3, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    .line 698
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.writeSocketCallback() NETWORK_ERROR"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_0

    .line 711
    :cond_3
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->c:Lcom/nuance/a/a/a/a/b/a/d$c;

    if-ne p1, v0, :cond_0

    .line 712
    iget-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    if-eq v0, v1, :cond_0

    iget-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    if-eq v0, v2, :cond_0

    .line 713
    iput-short v3, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    .line 715
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 716
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.writeSocketCallback() NETWORK_MEMORY_ERROR"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lcom/nuance/a/a/a/a/b/a/d$c;Ljava/lang/Object;[BIILjava/lang/Object;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x6

    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x5

    .line 553
    check-cast p6, Ljava/lang/String;

    .line 557
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "Read callback"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 560
    :cond_0
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 561
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0, p3}, Lcom/nuance/a/a/a/a/b/a/a$a;->a([B)V

    .line 564
    :cond_1
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->a:Lcom/nuance/a/a/a/a/b/a/d$c;

    if-ne p1, v0, :cond_d

    .line 565
    const-string v0, "READ_XMODE_HEADER"

    invoke-virtual {p6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 566
    if-nez p5, :cond_3

    .line 567
    new-instance v0, Lcom/nuance/a/a/a/b/a/a$2;

    invoke-direct {v0, p0}, Lcom/nuance/a/a/a/b/a/a$2;-><init>(Lcom/nuance/a/a/a/b/a/a;)V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->j:Lcom/nuance/a/a/a/a/b/a/e$a;

    .line 579
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->j:Lcom/nuance/a/a/a/a/b/a/e$a;

    const-wide/16 v2, 0x14

    invoke-interface {v0, v1, v2, v3}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;J)V

    .line 664
    :cond_2
    :goto_0
    return-void

    .line 580
    :cond_3
    if-ne p5, p4, :cond_7

    .line 582
    new-instance v0, Lcom/nuance/a/a/a/a/c/b;

    invoke-direct {v0, p3}, Lcom/nuance/a/a/a/a/c/b;-><init>([B)V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->s:Lcom/nuance/a/a/a/a/c/b;

    .line 585
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->s:Lcom/nuance/a/a/a/a/c/b;

    iget v0, v0, Lcom/nuance/a/a/a/a/c/b;->d:I

    if-nez v0, :cond_4

    .line 586
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->s:Lcom/nuance/a/a/a/a/c/b;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/nuance/a/a/a/b/a/a;->a(Lcom/nuance/a/a/a/a/c/b;[B)V

    .line 587
    invoke-direct {p0, v2}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    goto :goto_0

    .line 589
    :cond_4
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->s:Lcom/nuance/a/a/a/a/c/b;

    iget v0, v0, Lcom/nuance/a/a/a/a/c/b;->d:I

    const v1, 0x7d000

    if-gt v0, v1, :cond_5

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->s:Lcom/nuance/a/a/a/a/c/b;

    iget v0, v0, Lcom/nuance/a/a/a/a/c/b;->d:I

    if-gez v0, :cond_6

    .line 591
    :cond_5
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    invoke-interface {v0, p2}, Lcom/nuance/a/a/a/a/b/a/d;->b(Ljava/lang/Object;)V

    .line 593
    invoke-direct {p0, v2}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    goto :goto_0

    .line 595
    :cond_6
    invoke-direct {p0, v5}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    goto :goto_0

    .line 600
    :cond_7
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 601
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "----***---- readSocketCallback fatal error in readSocketCallback NET_CONTEXT_READ_XMODE_HEADER bytesRead:["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] bufferLen:["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto :goto_0

    .line 605
    :cond_8
    const-string v0, "READ_XMODE_PAYLOAD"

    invoke-virtual {p6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 606
    if-nez p5, :cond_a

    .line 609
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 610
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/nuance/a/a/a/b/a/a;->s:Lcom/nuance/a/a/a/a/c/b;

    iget-short v2, v2, Lcom/nuance/a/a/a/a/c/b;->c:S

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " payload not read bytesRead is 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 612
    :cond_9
    new-instance v0, Lcom/nuance/a/a/a/b/a/a$3;

    invoke-direct {v0, p0}, Lcom/nuance/a/a/a/b/a/a$3;-><init>(Lcom/nuance/a/a/a/b/a/a;)V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->j:Lcom/nuance/a/a/a/a/b/a/e$a;

    .line 625
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->j:Lcom/nuance/a/a/a/a/b/a/e$a;

    const-wide/16 v2, 0x14

    invoke-interface {v0, v1, v2, v3}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;J)V

    goto/16 :goto_0

    .line 626
    :cond_a
    if-ne p5, p4, :cond_c

    .line 631
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->s:Lcom/nuance/a/a/a/a/c/b;

    iget v0, v0, Lcom/nuance/a/a/a/a/c/b;->d:I

    if-gt v0, p4, :cond_b

    .line 632
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->s:Lcom/nuance/a/a/a/a/c/b;

    invoke-direct {p0, v0, p3}, Lcom/nuance/a/a/a/b/a/a;->a(Lcom/nuance/a/a/a/a/c/b;[B)V

    .line 633
    :cond_b
    invoke-direct {p0, v2}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    goto/16 :goto_0

    .line 637
    :cond_c
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 638
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "----***---- readSocketCallback fatal error in readSocketCallback NET_CONTEXT_READ_XMODE_PAYLOAD bytesRead:["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] bufferLen:["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 643
    :cond_d
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->b:Lcom/nuance/a/a/a/a/b/a/d$c;

    if-ne p1, v0, :cond_f

    .line 644
    iget-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    if-eq v0, v3, :cond_e

    iget-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    if-eq v0, v4, :cond_e

    iget-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    if-eq v0, v5, :cond_e

    .line 646
    iput-short v6, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    .line 648
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 649
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.readSocketCallback() NETWORK_ERROR"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    .line 652
    :cond_e
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    goto/16 :goto_0

    .line 654
    :cond_f
    sget-object v0, Lcom/nuance/a/a/a/a/b/a/d$c;->c:Lcom/nuance/a/a/a/a/b/a/d$c;

    if-ne p1, v0, :cond_2

    .line 655
    iget-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    if-eq v0, v3, :cond_2

    iget-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    if-eq v0, v4, :cond_2

    .line 656
    iput-short v6, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    .line 658
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 659
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.readSocketCallback() NETWORK_MEMORY_ERROR"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/16 v10, 0x17

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v3, 0x0

    .line 1232
    check-cast p1, Lcom/nuance/a/a/a/a/b/a/b$a;

    .line 1233
    iget-byte v0, p1, Lcom/nuance/a/a/a/a/b/a/b$a;->a:B

    packed-switch v0, :pswitch_data_0

    .line 1313
    :cond_0
    :goto_0
    return-void

    .line 1236
    :pswitch_0
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1237
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.handleMessage() CMD_CONNECT"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 1239
    :cond_1
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    if-nez v0, :cond_2

    invoke-direct {p0, v8}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->A:Lcom/nuance/a/a/a/b/a/a$a;

    iget-short v1, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    iget-short v2, p0, Lcom/nuance/a/a/a/b/a/a;->u:S

    invoke-interface {v0, v1, v2}, Lcom/nuance/a/a/a/b/a/a$a;->a(SS)V

    goto :goto_0

    .line 1243
    :pswitch_1
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1244
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.handleMessage() CMD_OPEN_SOCKET"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 1246
    :cond_3
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    if-nez v0, :cond_5

    .line 1247
    new-instance v0, Lcom/nuance/a/a/a/c/g;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    invoke-direct {v0, v1}, Lcom/nuance/a/a/a/c/g;-><init>(Lcom/nuance/a/a/a/a/b/a/b;)V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    iget-boolean v0, p0, Lcom/nuance/a/a/a/b/a/a;->p:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->a:Ljava/lang/String;

    iget-short v2, p0, Lcom/nuance/a/a/a/b/a/a;->b:S

    iget-object v3, p0, Lcom/nuance/a/a/a/b/a/a;->q:Ljava/util/Vector;

    move-object v4, p0

    move-object v5, p0

    invoke-interface/range {v0 .. v5}, Lcom/nuance/a/a/a/a/b/a/d;->a(Ljava/lang/String;ILjava/util/Vector;Lcom/nuance/a/a/a/a/b/a/d$d;Lcom/nuance/a/a/a/a/b/a/d$a;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->a:Ljava/lang/String;

    iget-short v2, p0, Lcom/nuance/a/a/a/b/a/a;->b:S

    invoke-interface {v0, v1, v2, p0, p0}, Lcom/nuance/a/a/a/a/b/a/d;->a(Ljava/lang/String;ILcom/nuance/a/a/a/a/b/a/d$d;Lcom/nuance/a/a/a/a/b/a/d$a;)V

    goto :goto_0

    .line 1250
    :cond_5
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->A:Lcom/nuance/a/a/a/b/a/a$a;

    iget-short v1, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    iget-short v2, p0, Lcom/nuance/a/a/a/b/a/a;->u:S

    invoke-interface {v0, v1, v2}, Lcom/nuance/a/a/a/b/a/a$a;->a(SS)V

    goto :goto_0

    .line 1255
    :pswitch_2
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1256
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.handleMessage() CMD_COP_CONNECT"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 1258
    :cond_6
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    if-nez v0, :cond_0

    .line 1259
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<?xml version=\"1.0\"?><cc><s></s><t>7</t><b>20091023</b><tsc>"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v1, p0, Lcom/nuance/a/a/a/b/a/a;->g:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</tsc><fsc>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/nuance/a/a/a/b/a/a;->h:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</fsc><nmaid>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</nmaid><uid>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</uid>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->o:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v5

    move-object v1, v0

    move v2, v3

    :goto_1
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nuance/a/a/a/b/b/c;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->d()Lcom/nuance/a/a/a/b/b/c$a;

    move-result-object v6

    sget-object v7, Lcom/nuance/a/a/a/b/b/c$a;->b:Lcom/nuance/a/a/a/b/b/c$a;

    if-ne v6, v7, :cond_10

    new-instance v6, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->b()[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "<nmsp p=\""

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "\" v=\""

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v6}, Lcom/nuance/a/a/a/a/d/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "\"/>"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Ping_IntervalSecs"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    move-object v2, v1

    move v1, v4

    :goto_2
    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->d()Lcom/nuance/a/a/a/b/b/c$a;

    move-result-object v6

    sget-object v7, Lcom/nuance/a/a/a/b/b/c$a;->c:Lcom/nuance/a/a/a/b/b/c$a;

    if-ne v6, v7, :cond_f

    new-instance v6, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->b()[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "<app p=\""

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/nuance/a/a/a/b/b/c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\" v=\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Lcom/nuance/a/a/a/a/d/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"/>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    move v2, v1

    move-object v1, v0

    goto/16 :goto_1

    :cond_7
    if-nez v2, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<nmsp p=\"Ping_IntervalSecs\" v=\"0\"/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</cc>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v1, v0

    add-int/lit8 v2, v1, 0x4

    add-int/lit8 v2, v2, 0x1

    new-array v5, v2, [B

    add-int/lit8 v2, v2, -0x4

    invoke-static {v2, v5, v3}, Lcom/nuance/a/a/a/a/d/b;->a(I[BI)V

    aput-byte v3, v5, v9

    const/4 v2, 0x5

    invoke-static {v0, v3, v5, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/16 v0, 0x100

    invoke-static {v8, v10, v0, v5}, Lcom/nuance/a/a/a/a/c/a;->a(BBS[B)[B

    move-result-object v0

    const-string v1, "SEND_COP_CONNECT"

    invoke-virtual {p0, v0, v1}, Lcom/nuance/a/a/a/b/a/a;->a([BLjava/lang/Object;)V

    new-instance v0, Lcom/nuance/a/a/a/b/a/a$4;

    invoke-direct {v0, p0}, Lcom/nuance/a/a/a/b/a/a$4;-><init>(Lcom/nuance/a/a/a/b/a/a;)V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->l:Lcom/nuance/a/a/a/a/b/a/e$a;

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->l:Lcom/nuance/a/a/a/a/b/a/e$a;

    iget v2, p0, Lcom/nuance/a/a/a/b/a/a;->k:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;J)V

    .line 1260
    iput-byte v4, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    .line 1261
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->A:Lcom/nuance/a/a/a/b/a/a$a;

    goto/16 :goto_0

    .line 1265
    :pswitch_3
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    new-array v3, v0, [B

    const-string v6, "READ_XMODE_HEADER"

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    sget-object v2, Lcom/nuance/a/a/a/a/b/a/d$b;->b:Lcom/nuance/a/a/a/a/b/a/d$b;

    const/16 v4, 0x8

    move-object v5, p0

    invoke-interface/range {v0 .. v6}, Lcom/nuance/a/a/a/a/b/a/d;->a(Ljava/lang/Object;Lcom/nuance/a/a/a/a/b/a/d$b;[BILcom/nuance/a/a/a/a/b/a/d$e;Ljava/lang/Object;)Lcom/nuance/a/a/a/a/b/a/d$c;

    goto/16 :goto_0

    .line 1268
    :pswitch_4
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->s:Lcom/nuance/a/a/a/a/c/b;

    iget v0, v0, Lcom/nuance/a/a/a/a/c/b;->d:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->s:Lcom/nuance/a/a/a/a/c/b;

    iget v0, v0, Lcom/nuance/a/a/a/a/c/b;->d:I

    const v1, 0x7d000

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->s:Lcom/nuance/a/a/a/a/c/b;

    iget v0, v0, Lcom/nuance/a/a/a/a/c/b;->d:I

    new-array v3, v0, [B

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const-string v6, "READ_XMODE_PAYLOAD"

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    sget-object v2, Lcom/nuance/a/a/a/a/b/a/d$b;->b:Lcom/nuance/a/a/a/a/b/a/d$b;

    array-length v4, v3

    move-object v5, p0

    invoke-interface/range {v0 .. v6}, Lcom/nuance/a/a/a/a/b/a/d;->a(Ljava/lang/Object;Lcom/nuance/a/a/a/a/b/a/d$b;[BILcom/nuance/a/a/a/a/b/a/d$e;Ljava/lang/Object;)Lcom/nuance/a/a/a/a/b/a/d$c;

    goto/16 :goto_0

    .line 1272
    :pswitch_5
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1273
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.handleMessage() CMD_DISCONNECT"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 1275
    :cond_9
    const/4 v0, 0x2

    new-array v0, v0, [B

    invoke-static {v3, v0, v3}, Lcom/nuance/a/a/a/a/d/b;->a(S[BI)V

    const/16 v1, 0x200

    invoke-static {v8, v10, v1, v0}, Lcom/nuance/a/a/a/a/c/a;->a(BBS[B)[B

    move-result-object v0

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    iget-object v2, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lcom/nuance/a/a/a/a/b/a/d;->b(Ljava/lang/Object;)V

    const-string v1, "SEND_COP_DISCONNECT"

    invoke-virtual {p0, v0, v1}, Lcom/nuance/a/a/a/b/a/a;->a([BLjava/lang/Object;)V

    goto/16 :goto_0

    .line 1279
    :pswitch_6
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1280
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.handleMessage() CMD_CLOSE_SOCKET"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 1282
    :cond_a
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/a/b/a/d;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1286
    :pswitch_7
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1287
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.handleMessage() CMD_COP_PING_RESPONSE"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 1289
    :cond_b
    const/16 v0, 0x8

    new-array v0, v0, [B

    iget v1, p0, Lcom/nuance/a/a/a/b/a/a;->f:I

    invoke-static {v1, v0, v3}, Lcom/nuance/a/a/a/a/d/b;->a(I[BI)V

    invoke-static {v3, v0, v9}, Lcom/nuance/a/a/a/a/d/b;->a(I[BI)V

    const/16 v1, 0x103

    invoke-static {v8, v10, v1, v0}, Lcom/nuance/a/a/a/a/c/a;->a(BBS[B)[B

    move-result-object v0

    const-string v1, "SEND_COP_PING_RESPONSE"

    invoke-virtual {p0, v0, v1}, Lcom/nuance/a/a/a/b/a/a;->a([BLjava/lang/Object;)V

    goto/16 :goto_0

    .line 1293
    :pswitch_8
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1294
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.handleMessage() CMD_COP_CONNECT_TIMED_OUT"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 1296
    :cond_c
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    if-ne v0, v4, :cond_0

    const/4 v0, 0x2

    iput-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    const/4 v0, 0x5

    iput-short v0, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->e()Z

    move-result v0

    if-eqz v0, :cond_d

    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.handleCopConnectTimeout() COP CONNECT timed out. "

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->e(Ljava/lang/Object;)V

    :cond_d
    invoke-direct {p0, v9}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    goto/16 :goto_0

    .line 1300
    :pswitch_9
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1301
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "XMode.handleMessage() CMD_COP_CONFIRM"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 1303
    :cond_e
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->x:[B

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->e:[B

    invoke-static {v0, v1}, Lcom/nuance/a/a/a/a/b/a/c;->b([B[B)[B

    move-result-object v0

    array-length v1, v0

    add-int/lit8 v1, v1, 0x4

    new-array v1, v1, [B

    array-length v2, v0

    invoke-static {v2, v1, v3}, Lcom/nuance/a/a/a/a/d/b;->a(I[BI)V

    array-length v2, v0

    invoke-static {v0, v3, v1, v9, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/16 v0, 0x106

    invoke-static {v8, v10, v0, v1}, Lcom/nuance/a/a/a/a/c/a;->a(BBS[B)[B

    move-result-object v0

    const-string v1, "SEND_COP_CONFIRM"

    invoke-virtual {p0, v0, v1}, Lcom/nuance/a/a/a/b/a/a;->a([BLjava/lang/Object;)V

    goto/16 :goto_0

    .line 1306
    :pswitch_a
    iget-object v0, p1, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->A:Lcom/nuance/a/a/a/b/a/a$a;

    goto/16 :goto_0

    .line 1309
    :pswitch_b
    iget-object v0, p1, Lcom/nuance/a/a/a/a/b/a/b$a;->b:Ljava/lang/Object;

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->A:Lcom/nuance/a/a/a/b/a/a$a;

    goto/16 :goto_0

    :cond_f
    move-object v0, v2

    goto/16 :goto_3

    :cond_10
    move-object v11, v1

    move v1, v2

    move-object v2, v11

    goto/16 :goto_2

    .line 1233
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final a(SS)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 302
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.connect() codec: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 305
    :cond_0
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    if-eq v0, v3, :cond_3

    .line 307
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    if-eqz v0, :cond_3

    .line 310
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 311
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->j:Lcom/nuance/a/a/a/a/b/a/e$a;

    if-eqz v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->j:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    if-eqz v0, :cond_2

    .line 314
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    .line 316
    :cond_2
    iput-object v4, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    .line 317
    iput-object v4, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    .line 318
    iput-object v4, p0, Lcom/nuance/a/a/a/b/a/a;->e:[B

    .line 320
    const-string v0, ""

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->z:Ljava/lang/String;

    .line 322
    iput-short p1, p0, Lcom/nuance/a/a/a/b/a/a;->g:S

    .line 323
    iput-short p2, p0, Lcom/nuance/a/a/a/b/a/a;->h:S

    .line 324
    iput-byte v5, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    .line 325
    invoke-direct {p0, v3}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    .line 333
    :cond_3
    :goto_0
    return-void

    .line 327
    :cond_4
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 328
    iput-short p1, p0, Lcom/nuance/a/a/a/b/a/a;->g:S

    .line 329
    iput-short p2, p0, Lcom/nuance/a/a/a/b/a/a;->h:S

    .line 330
    iput-byte v5, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    .line 331
    invoke-direct {p0, v3}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    goto :goto_0
.end method

.method public final a([BI)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 376
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[LATCHK] XMode.sendVapRecordMsg() audioBuf.length:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", audio id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 380
    :cond_0
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    if-eq v0, v4, :cond_1

    .line 400
    :goto_0
    return-void

    .line 383
    :cond_1
    array-length v0, p1

    .line 384
    add-int/lit8 v1, v0, 0x8

    new-array v1, v1, [B

    .line 385
    invoke-static {p2, v1, v3}, Lcom/nuance/a/a/a/a/d/b;->a(I[BI)V

    .line 387
    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lcom/nuance/a/a/a/a/d/b;->a(I[BI)V

    .line 389
    const/16 v2, 0x8

    invoke-static {p1, v3, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 392
    const/16 v0, 0x12

    const/16 v2, 0x201

    invoke-static {v4, v0, v2, v1}, Lcom/nuance/a/a/a/a/c/a;->a(BBS[B)[B

    move-result-object v0

    .line 399
    const-string v1, "SEND_VAP_RECORD"

    invoke-virtual {p0, v0, v1}, Lcom/nuance/a/a/a/b/a/a;->a([BLjava/lang/Object;)V

    goto :goto_0
.end method

.method public final a([BLjava/lang/Object;)V
    .locals 6

    .prologue
    .line 426
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 427
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[LATCHK] XMode.sendXModeMsg() [IN] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", buffer.length:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 449
    new-instance v0, Lcom/nuance/a/a/a/b/a/a$1;

    invoke-direct {v0, p0}, Lcom/nuance/a/a/a/b/a/a$1;-><init>(Lcom/nuance/a/a/a/b/a/a;)V

    iput-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    .line 467
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->c:Lcom/nuance/a/a/a/a/b/a/b;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->n:Lcom/nuance/a/a/a/a/b/a/e$a;

    iget v2, p0, Lcom/nuance/a/a/a/b/a/a;->m:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/nuance/a/a/a/a/b/a/b;->a(Lcom/nuance/a/a/a/a/b/a/e$a;J)V

    .line 470
    :cond_1
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 476
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    array-length v3, p1

    move-object v2, p1

    move-object v4, p0

    move-object v5, p2

    invoke-interface/range {v0 .. v5}, Lcom/nuance/a/a/a/a/b/a/d;->a(Ljava/lang/Object;[BILcom/nuance/a/a/a/a/b/a/d$f;Ljava/lang/Object;)Lcom/nuance/a/a/a/a/b/a/d$c;

    .line 480
    :cond_2
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 481
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    const-string v1, "[LATCHK] XMode.sendXModeMsg() [OUT]"

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 483
    :cond_3
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 337
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.disconnect() state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-byte v2, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", socket:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 340
    :cond_0
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 341
    iput-short v3, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    .line 342
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->A:Lcom/nuance/a/a/a/b/a/a$a;

    iget-short v1, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    iget-short v2, p0, Lcom/nuance/a/a/a/b/a/a;->u:S

    invoke-interface {v0, v1, v2}, Lcom/nuance/a/a/a/b/a/a$a;->a(SS)V

    .line 359
    :cond_1
    :goto_0
    return-void

    .line 344
    :cond_2
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    if-eq v0, v4, :cond_1

    .line 347
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    if-nez v0, :cond_3

    .line 348
    iput-short v3, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    .line 349
    iput-byte v4, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    .line 350
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 351
    iget-object v0, p0, Lcom/nuance/a/a/a/b/a/a;->y:Lcom/nuance/a/a/a/a/b/a/d;

    iget-object v1, p0, Lcom/nuance/a/a/a/b/a/a;->r:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/nuance/a/a/a/a/b/a/d;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 354
    :cond_3
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    if-ne v0, v3, :cond_1

    .line 355
    iput-byte v4, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    .line 356
    iput-short v3, p0, Lcom/nuance/a/a/a/b/a/a;->t:S

    .line 357
    invoke-direct {p0, v4}, Lcom/nuance/a/a/a/b/a/a;->a(B)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 404
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    invoke-virtual {v0}, Lcom/nuance/a/a/a/a/b/a/a$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    sget-object v0, Lcom/nuance/a/a/a/b/a/a;->i:Lcom/nuance/a/a/a/a/b/a/a$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XMode.sendVapRecordEnd() audio id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/nuance/a/a/a/a/b/a/a$a;->b(Ljava/lang/Object;)V

    .line 408
    :cond_0
    iget-byte v0, p0, Lcom/nuance/a/a/a/b/a/a;->d:B

    if-eq v0, v3, :cond_1

    .line 422
    :goto_0
    return-void

    .line 411
    :cond_1
    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 412
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/nuance/a/a/a/a/d/b;->a(I[BI)V

    .line 414
    const/16 v1, 0x12

    const/16 v2, 0x100

    invoke-static {v3, v1, v2, v0}, Lcom/nuance/a/a/a/a/c/a;->a(BBS[B)[B

    move-result-object v0

    .line 421
    const-string v1, "SEND_VAP_RECORD_END"

    invoke-virtual {p0, v0, v1}, Lcom/nuance/a/a/a/b/a/a;->a([BLjava/lang/Object;)V

    goto :goto_0
.end method
