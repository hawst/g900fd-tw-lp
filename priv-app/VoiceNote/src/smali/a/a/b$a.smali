.class final La/a/b$a;
.super Ljava/lang/Object;
.source "IntTree.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = La/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/Integer;",
        "TV;>;>;"
    }
.end annotation


# instance fields
.field private a:La/a/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/g",
            "<",
            "La/a/b",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method constructor <init>(La/a/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/a/b",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 263
    invoke-static {}, La/a/a;->a()La/a/a;

    move-result-object v0

    iput-object v0, p0, La/a/b$a;->a:La/a/g;

    .line 264
    const/4 v0, 0x0

    iput v0, p0, La/a/b$a;->b:I

    .line 267
    invoke-direct {p0, p1}, La/a/b$a;->a(La/a/b;)V

    return-void
.end method

.method private a(La/a/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/a/b",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 302
    :goto_0
    invoke-static {p1}, La/a/b;->c(La/a/b;)I

    move-result v0

    if-lez v0, :cond_0

    .line 303
    iget-object v0, p0, La/a/b$a;->a:La/a/g;

    invoke-interface {v0, p1}, La/a/g;->a(Ljava/lang/Object;)La/a/g;

    move-result-object v0

    iput-object v0, p0, La/a/b$a;->a:La/a/g;

    .line 304
    iget v0, p0, La/a/b$a;->b:I

    int-to-long v0, v0

    invoke-static {p1}, La/a/b;->d(La/a/b;)J

    move-result-wide v2

    add-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, La/a/b$a;->b:I

    .line 305
    invoke-static {p1}, La/a/b;->e(La/a/b;)La/a/b;

    move-result-object p1

    goto :goto_0

    .line 307
    :cond_0
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, La/a/b$a;->a:La/a/g;

    invoke-interface {v0}, La/a/g;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 262
    iget-object v0, p0, La/a/b$a;->a:La/a/g;

    invoke-interface {v0, v6}, La/a/g;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b;

    new-instance v1, La/a/i;

    iget v2, p0, La/a/b$a;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, La/a/b;->a(La/a/b;)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v1, v2, v3}, La/a/i;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0}, La/a/b;->b(La/a/b;)La/a/b;

    move-result-object v2

    invoke-static {v2}, La/a/b;->c(La/a/b;)I

    move-result v2

    if-lez v2, :cond_1

    invoke-static {v0}, La/a/b;->b(La/a/b;)La/a/b;

    move-result-object v0

    invoke-direct {p0, v0}, La/a/b$a;->a(La/a/b;)V

    :cond_0
    return-object v1

    :cond_1
    :goto_0
    iget v2, p0, La/a/b$a;->b:I

    int-to-long v2, v2

    invoke-static {v0}, La/a/b;->d(La/a/b;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, p0, La/a/b$a;->b:I

    iget-object v2, p0, La/a/b$a;->a:La/a/g;

    invoke-interface {v2}, La/a/g;->b()La/a/g;

    move-result-object v2

    iput-object v2, p0, La/a/b$a;->a:La/a/g;

    invoke-static {v0}, La/a/b;->d(La/a/b;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    iget-object v0, p0, La/a/b$a;->a:La/a/g;

    invoke-interface {v0}, La/a/g;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, La/a/b$a;->a:La/a/g;

    invoke-interface {v0, v6}, La/a/g;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/b;

    goto :goto_0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 298
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
