.class public final La/a/c;
.super Ljava/util/AbstractMap;
.source "IntTreePMap.java"

# interfaces
.implements La/a/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<",
        "Ljava/lang/Integer;",
        "TV;>;",
        "La/a/e",
        "<",
        "Ljava/lang/Integer;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static final a:La/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/c",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:La/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/b",
            "<TV;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, La/a/c;

    sget-object v1, La/a/b;->a:La/a/b;

    invoke-direct {v0, v1}, La/a/c;-><init>(La/a/b;)V

    sput-object v0, La/a/c;->a:La/a/c;

    return-void
.end method

.method private constructor <init>(La/a/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/a/b",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, La/a/c;->c:Ljava/util/Set;

    .line 75
    iput-object p1, p0, La/a/c;->b:La/a/b;

    return-void
.end method

.method static synthetic a(La/a/c;)La/a/b;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, La/a/c;->b:La/a/b;

    return-object v0
.end method

.method public static a()La/a/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">()",
            "La/a/c",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 46
    sget-object v0, La/a/c;->a:La/a/c;

    return-object v0
.end method

.method private a(La/a/b;)La/a/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/a/b",
            "<TV;>;)",
            "La/a/c",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, La/a/c;->b:La/a/b;

    if-ne p1, v0, :cond_0

    .line 78
    :goto_0
    return-object p0

    :cond_0
    new-instance p0, La/a/c;

    invoke-direct {p0, p1}, La/a/c;-><init>(La/a/b;)V

    goto :goto_0
.end method


# virtual methods
.method final a(I)La/a/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "La/a/c",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, La/a/c;->b:La/a/b;

    int-to-long v2, p1

    const/4 v1, -0x1

    invoke-virtual {v0, v2, v3, v1}, La/a/b;->a(JI)La/a/b;

    move-result-object v0

    invoke-direct {p0, v0}, La/a/c;->a(La/a/b;)La/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Integer;Ljava/lang/Object;)La/a/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "TV;)",
            "La/a/c",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, La/a/c;->b:La/a/b;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3, p2}, La/a/b;->a(JLjava/lang/Object;)La/a/b;

    move-result-object v0

    invoke-direct {p0, v0}, La/a/c;->a(La/a/b;)La/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)La/a/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "La/a/c",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 146
    instance-of v0, p1, Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 147
    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, La/a/c;->b:La/a/b;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, La/a/b;->c(J)La/a/b;

    move-result-object v0

    invoke-direct {p0, v0}, La/a/c;->a(La/a/b;)La/a/c;

    move-result-object p0

    goto :goto_0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 128
    instance-of v0, p1, Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x0

    .line 130
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, La/a/c;->b:La/a/b;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, La/a/b;->a(J)Z

    move-result v0

    goto :goto_0
.end method

.method public final entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "TV;>;>;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, La/a/c;->c:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, La/a/c$1;

    invoke-direct {v0, p0}, La/a/c$1;-><init>(La/a/c;)V

    iput-object v0, p0, La/a/c;->c:Ljava/util/Set;

    .line 117
    :cond_0
    iget-object v0, p0, La/a/c;->c:Ljava/util/Set;

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 135
    instance-of v0, p1, Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 136
    const/4 v0, 0x0

    .line 137
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, La/a/c;->b:La/a/b;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, La/a/b;->b(J)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, La/a/c;->b:La/a/b;

    invoke-virtual {v0}, La/a/b;->b()I

    move-result v0

    return v0
.end method
