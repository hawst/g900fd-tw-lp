.class public final La/a/j;
.super Ljava/util/AbstractList;
.source "TreePVector.java"

# interfaces
.implements La/a/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractList",
        "<TE;>;",
        "La/a/h",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final a:La/a/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/j",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:La/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/c",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, La/a/j;

    invoke-static {}, La/a/c;->a()La/a/c;

    move-result-object v1

    invoke-direct {v0, v1}, La/a/j;-><init>(La/a/c;)V

    sput-object v0, La/a/j;->a:La/a/j;

    return-void
.end method

.method private constructor <init>(La/a/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/a/c",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 62
    iput-object p1, p0, La/a/j;->b:La/a/c;

    return-void
.end method

.method public static a()La/a/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "La/a/j",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 35
    sget-object v0, La/a/j;->a:La/a/j;

    return-object v0
.end method

.method private a(I)La/a/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "La/a/j",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 118
    if-ltz p1, :cond_0

    invoke-virtual {p0}, La/a/j;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 119
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 120
    :cond_1
    new-instance v0, La/a/j;

    iget-object v1, p0, La/a/j;->b:La/a/c;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, La/a/c;->a(Ljava/lang/Object;)La/a/c;

    move-result-object v1

    invoke-virtual {v1, p1}, La/a/c;->a(I)La/a/c;

    move-result-object v1

    invoke-direct {v0, v1}, La/a/j;-><init>(La/a/c;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)La/a/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "La/a/j",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 102
    new-instance v0, La/a/j;

    iget-object v1, p0, La/a/j;->b:La/a/c;

    invoke-virtual {p0}, La/a/j;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, La/a/c;->a(Ljava/lang/Integer;Ljava/lang/Object;)La/a/c;

    move-result-object v1

    invoke-direct {v0, v1}, La/a/j;-><init>(La/a/c;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)La/a/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "La/a/j",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, La/a/j;->b:La/a/c;

    invoke-virtual {v0}, La/a/c;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 112
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 113
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, La/a/j;->a(I)La/a/j;

    move-result-object p0

    .line 114
    :cond_1
    return-object p0
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 72
    if-ltz p1, :cond_0

    invoke-virtual {p0}, La/a/j;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 73
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 74
    :cond_1
    iget-object v0, p0, La/a/j;->b:La/a/c;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, La/a/j;->b:La/a/c;

    invoke-virtual {v0}, La/a/c;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, La/a/j;->b:La/a/c;

    invoke-virtual {v0}, La/a/c;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic subList(II)Ljava/util/List;
    .locals 1

    .prologue
    .line 25
    :goto_0
    invoke-virtual {p0}, La/a/j;->size()I

    move-result v0

    if-ltz p1, :cond_0

    if-gt p2, v0, :cond_0

    if-le p1, p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_1
    if-ne p1, p2, :cond_3

    sget-object p0, La/a/j;->a:La/a/j;

    :cond_2
    return-object p0

    :cond_3
    if-nez p1, :cond_4

    if-eq p2, v0, :cond_2

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, La/a/j;->a(I)La/a/j;

    move-result-object p0

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-direct {p0, v0}, La/a/j;->a(I)La/a/j;

    move-result-object p0

    add-int/lit8 p1, p1, -0x1

    add-int/lit8 p2, p2, -0x1

    goto :goto_0
.end method
