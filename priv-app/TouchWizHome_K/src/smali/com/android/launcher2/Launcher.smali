.class public final Lcom/android/launcher2/Launcher;
.super Landroid/app/ActivityGroup;
.source "Launcher.java"

# interfaces
.implements Lcom/android/launcher2/LauncherModel$Callbacks;
.implements Lcom/android/launcher2/popup/PopupMenu$OnMenuItemClickListener;
.implements Lcom/samsung/android/service/gesture/GestureListener;
.implements Lcom/samsung/android/service/gesture/GestureManager$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/launcher2/Launcher$14;,
        Lcom/android/launcher2/Launcher$AlwaysMicOnObserver;,
        Lcom/android/launcher2/Launcher$ActivityResultCallback;,
        Lcom/android/launcher2/Launcher$AirGestureSettingsChangeReceiver;,
        Lcom/android/launcher2/Launcher$WidgetAddInfo;,
        Lcom/android/launcher2/Launcher$TalkbackEnableIntentReceiver;,
        Lcom/android/launcher2/Launcher$CloseSystemDialogsIntentReceiver;,
        Lcom/android/launcher2/Launcher$StateAnimatorProvider;,
        Lcom/android/launcher2/Launcher$HardwareKeys;
    }
.end annotation


# static fields
.field private static final ACTION_HOME_MODE:Ljava/lang/String; = "com.android.launcher.action.HOME_MODE_CHANGE"

.field private static final ACTION_HOME_RESTORE:Ljava/lang/String; = "com.android.launcher.action.ACTION_HOME_RESTORE"

.field static final ADD_TOAST_POPUP_DISMISSED_KEY:Ljava/lang/String; = "add.toast.popup.dismissed.key"

.field public static CSCFEATURE_LAUNCHER_DISABLEHELPUI:Z = false

.field public static CSCFEATURE_LAUNCHER_DISABLETILTEFFECT:Z = false

.field public static CSCFEATURE_LAUNCHER_DYNAMICCSCFEATURE:Z = false

.field public static CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z = false

.field public static CSCFEATURE_LAUNCHER_REPLACEHOTSEATAREAINEDITMODE:Z = false

.field public static CSCFEATURE_LAUNCHER_THEME_ENABLE:Z = false

.field public static CSCFEATURE_LAUNCHER_USM_EXISTS:Z = false

.field private static final DEBUGGABLE:Z

.field private static final DEBUG_ADD_WIDGET_VIA_INTENT:Z = false

.field private static final DEBUG_ADD_WIDGET_VIA_INTENT_TAG:Ljava/lang/String; = "Launcher.AddWidgetViaIntent"

.field static final DISABLE_TOAST_POPUP_DISMISSED_KEY:Ljava/lang/String; = "disable.toast.popup.dismissed.key"

.field static final DUMP_STATE_PROPERTY:Ljava/lang/String; = "launcher_dump_state"

.field private static final EXTRA_HOME_VIEW_HIDDEN:Ljava/lang/String; = "extra_home_view_hidden"

.field private static final EXTRA_LAUNCHER_ACTION:Ljava/lang/String; = "sec.android.intent.extra.LAUNCHER_ACTION"

.field private static final HELP_ACTIVITY_TYPE:Ljava/lang/String; = "help_activity_type"

.field public static final HELP_HUB_CONTENT_URI:Ljava/lang/String; = "content://com.samsung.helphub.provider/help_page_status/"

.field private static final LAUNCHER_ACTION_ALL_APPS:Ljava/lang/String; = "com.android.launcher2.ALL_APPS"

.field private static final LAUNCHER_ACTION_IDLE:Ljava/lang/String; = "com.android.launcher2.IDLE"

.field public static final LAUNCHER_THEME_ACTION:Ljava/lang/String; = "com.sec.android.intent.action.THEME_CHOOSER"

.field private static final MORE_SERVICE_DOWNLOADABLE_APP_LIST_CLASS_NAME:Ljava/lang/String; = "com.sec.android.app.moreservices.moreservices"

.field private static final MORE_SERVICE_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.moreservices"

.field static final PREFERENCES:Ljava/lang/String; = "com.sec.android.app.launcher.prefs"

.field private static final REQUEST_CODE_UNINSTALL_PACKAGE:I = 0x6f

.field static final REQUEST_CREATE_APPWIDGET:I = 0x5

.field private static final SAMSUNG_APPS_DOWNLOADABLE_APP_LIST_CLASS_NAME:Ljava/lang/String; = "com.sec.android.app.samsungapps.downloadableapps.DownloadableAppsActivity"

.field private static final SAMSUNG_APPS_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.samsungapps"

.field public static SEC_PRODUCT_FEATURE_LAUNCHER_KITKAT_ADD_TO_HOMESCREEN_CONCEPT:Z = false

.field private static final SETTINGS_SYSTEMUI_TRANSPARENCY:Ljava/lang/String; = "android.wallpaper.settings_systemui_transparency"

.field public static SHOW_ALL_APPS_TRANSITION_DURATION:J = 0x0L

.field private static final SYSTEMUI_TRANSPARENCY:Z = true

.field private static final TAG:Ljava/lang/String; = "Launcher"

.field public static UseLauncherHighPriority:Z = false

.field public static UseQwertyKeypad:Z = false

.field static final VIBRATE_DURATION:I = 0x23

.field static final WIDGET_RESIZE_UPDATE_PAUSE:Ljava/lang/String; = "com.sec.android.intent.action.HOME_PAUSE"

.field static final WIDGET_RESIZE_UPDATE_RESUME:Ljava/lang/String; = "com.sec.android.intent.action.HOME_RESUME"

.field public static bSupportAirMove:Z

.field public static changeWallpaperStatus:I

.field static dragstate:Lcom/android/launcher2/DragState;

.field public static isExitingFromHelp:Z

.field public static isExitingFromWallpaperActivity:Z

.field public static isHelpAppRunning:Z

.field public static isHelpIntentFired:Z

.field public static isHomescreenRestoring:Z

.field public static isMotionDialogLaunching:Z

.field public static isSystemAppDisable:Z

.field public static is_TB:Z

.field private static launcher:Lcom/android/launcher2/Launcher;

.field public static mAlwaysMicOn:Z

.field private static mEasyStateForHelpApp:Z

.field public static mIsBabyCryingEnable:I

.field public static mIsDoorbellEnable:I

.field public static mNonDisableItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static mPackageTobeDisabled:Ljava/lang/String;

.field private static mPendingIntent:Landroid/content/Intent;

.field public static mRemovableAppItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static mRemovableCustomerAppItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static mRemovablePreloadAppItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static mTalkbackEnable:Z

.field public static sActivityOrientation:I

.field private static volatile sHelpHubAvailable:Ljava/lang/Boolean;

.field private static sHwPopupMenuShowing:Z

.field public static sInComingIntentHelpHub:Landroid/content/Intent;

.field public static sIsEditFromHome:Z

.field public static sIsFestivalModeOn:Z

.field public static sIsHeadlinesAppEnable:Z

.field private static sIsHomeEditMode:Z

.field private static sIsHomeRemoveMode:Z

.field public static sIsSecretModeOn:Z

.field public static sIsStopped:Z

.field static sMenuBgDarkenAmountNormal:F

.field private static sPopupMenuShowing:Z

.field public static sViewLayerPaint:Landroid/graphics/Paint;

.field public static sWhichTransitionEffect:I

.field public static wasWidgetsTabShown:Z


# instance fields
.field final AUTHORITY:Ljava/lang/String;

.field final content_uri:Landroid/net/Uri;

.field public isFolderCreatedFromMenuButton:Z

.field private mActivityCallbacks:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/launcher2/Launcher$ActivityResultCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mActivityName:Landroid/content/ComponentName;

.field private final mAirGestureSettingsChangeReceiver:Landroid/content/BroadcastReceiver;

.field private final mAlwaysMicOnObserver:Landroid/database/ContentObserver;

.field private mAnimationLayer:Lcom/android/launcher2/AnimationLayer;

.field private mAppMarketIntent:Landroid/content/Intent;

.field private mApplicationPolicy:Landroid/app/enterprise/ApplicationPolicy;

.field public mAttached:Z

.field private mAutoRestart:Z

.field private mBabyCryingDetector:Landroid/database/ContentObserver;

.field private mBadgeCountChangedReceiver:Lcom/android/launcher2/BadgeCountReceiver;

.field private mChangeCallAppReceiver:Lcom/android/launcher2/ChangeCallAppReceiver;

.field private final mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

.field private mDarkenView:Landroid/view/View;

.field private mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

.field private mDoorbellDetector:Landroid/database/ContentObserver;

.field private mEnterpriseDeviceManager:Landroid/app/enterprise/EnterpriseDeviceManager;

.field private mExitingAllApps:Z

.field private mFactoryModeString:Ljava/lang/String;

.field private mFestivalPageManager:Lcom/android/launcher2/FestivalPageManager;

.field public mFocusState:Z

.field private mGestureListenerRegistered:Z

.field private mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

.field private mGestureServiceConnected:Z

.field private mHasMenuKey:Z

.field mHomeView:Lcom/android/launcher2/HomeView;

.field private mHotwordServiceClient:Lcom/google/android/hotword/client/HotwordServiceClient;

.field mInMenu:Z

.field private mIsDestroyed:Z

.field private mIsExternalHelpActivity:Z

.field private mIsFactoryMode:Z

.field private mMarketLabelName:Ljava/lang/CharSequence;

.field private mMenuButtonView:Landroid/view/View;

.field private mMenuView:Lcom/android/launcher2/MenuView;

.field private mModel:Lcom/android/launcher2/LauncherModel;

.field private mOnResumeNeedsLoad:Z

.field private mOptionsMenu:Landroid/view/Menu;

.field private mPaused:Z

.field private mPkgResCache:Lcom/android/launcher2/PkgResCache;

.field private mPkgResCacheForMenu:Lcom/android/launcher2/PkgResCache;

.field private mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

.field private mPrevOrientationHelp:I

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field public mRemainSavedInstance:Z

.field private mRestoring:Z

.field private mResumed:Z

.field private mSamsungWidgetPackageManager:Lcom/android/launcher2/SamsungWidgetPackageManager;

.field private mSecretPageManager:Lcom/android/launcher2/SecretPageManager;

.field private mShowEmptyPageMsg:Z

.field private mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

.field private mStateAnimatorProviders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/launcher2/Launcher$StateAnimatorProvider;",
            ">;"
        }
    .end annotation
.end field

.field private mStatusBarManager:Landroid/app/StatusBarManager;

.field private mSurfaceWidgetPackageManager:Lcom/android/launcher2/SurfaceWidgetPackageManager;

.field private final mTalkbackEnableReceiver:Landroid/content/BroadcastReceiver;

.field private mTransitionAnimator:Landroid/animation/Animator;

.field private mTransitioningToAllApps:Z

.field private mWidgetAddInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/launcher2/Launcher$WidgetAddInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 180
    sput-object v1, Lcom/android/launcher2/Launcher;->launcher:Lcom/android/launcher2/Launcher;

    .line 186
    invoke-static {}, Lcom/android/launcher2/Utilities;->DEBUGGABLE()Z

    move-result v0

    sput-boolean v0, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    .line 201
    sput-boolean v2, Lcom/android/launcher2/Launcher;->isHomescreenRestoring:Z

    .line 205
    sput-boolean v2, Lcom/android/launcher2/Launcher;->sIsHeadlinesAppEnable:Z

    .line 208
    sput-boolean v3, Lcom/android/launcher2/Launcher;->isSystemAppDisable:Z

    .line 210
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/launcher2/Launcher;->mNonDisableItems:Ljava/util/ArrayList;

    .line 212
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/launcher2/Launcher;->mRemovablePreloadAppItems:Ljava/util/ArrayList;

    .line 213
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/launcher2/Launcher;->mRemovableCustomerAppItems:Ljava/util/ArrayList;

    .line 214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/launcher2/Launcher;->mRemovableAppItems:Ljava/util/ArrayList;

    .line 216
    const-string v0, ""

    sput-object v0, Lcom/android/launcher2/Launcher;->mPackageTobeDisabled:Ljava/lang/String;

    .line 220
    sput-boolean v2, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    .line 221
    sput-boolean v2, Lcom/android/launcher2/Launcher;->isHelpIntentFired:Z

    .line 222
    sput-boolean v2, Lcom/android/launcher2/Launcher;->wasWidgetsTabShown:Z

    .line 223
    sput-boolean v2, Lcom/android/launcher2/Launcher;->isExitingFromWallpaperActivity:Z

    .line 224
    sput-boolean v2, Lcom/android/launcher2/Launcher;->isMotionDialogLaunching:Z

    .line 225
    const/4 v0, -0x1

    sput v0, Lcom/android/launcher2/Launcher;->changeWallpaperStatus:I

    .line 251
    sput-boolean v2, Lcom/android/launcher2/Launcher;->sPopupMenuShowing:Z

    .line 252
    sput-boolean v2, Lcom/android/launcher2/Launcher;->sHwPopupMenuShowing:Z

    .line 288
    sput-object v1, Lcom/android/launcher2/Launcher;->sHelpHubAvailable:Ljava/lang/Boolean;

    .line 289
    sput-boolean v2, Lcom/android/launcher2/Launcher;->mEasyStateForHelpApp:Z

    .line 290
    const-wide/16 v0, 0x14a

    sput-wide v0, Lcom/android/launcher2/Launcher;->SHOW_ALL_APPS_TRANSITION_DURATION:J

    .line 302
    sput-boolean v2, Lcom/android/launcher2/Launcher;->is_TB:Z

    .line 323
    sput-boolean v2, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    .line 324
    sput-boolean v2, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_DISABLETILTEFFECT:Z

    .line 325
    sput-boolean v2, Lcom/android/launcher2/Launcher;->sIsHomeEditMode:Z

    .line 326
    sput-boolean v2, Lcom/android/launcher2/Launcher;->sIsHomeRemoveMode:Z

    .line 327
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_LAUNCHER_BOOT_HIGHPRIORITY"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/launcher2/Launcher;->UseLauncherHighPriority:Z

    .line 329
    sput-boolean v2, Lcom/android/launcher2/Launcher;->UseQwertyKeypad:Z

    .line 332
    sput-boolean v2, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_REPLACEHOTSEATAREAINEDITMODE:Z

    .line 333
    sput-boolean v2, Lcom/android/launcher2/Launcher;->sIsEditFromHome:Z

    .line 335
    sput-boolean v3, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_DISABLEHELPUI:Z

    .line 336
    sput-boolean v2, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_DYNAMICCSCFEATURE:Z

    .line 338
    sput-boolean v2, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_USM_EXISTS:Z

    .line 339
    sput-boolean v2, Lcom/android/launcher2/Launcher;->isExitingFromHelp:Z

    .line 342
    sput-boolean v2, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_THEME_ENABLE:Z

    .line 343
    sput-boolean v2, Lcom/android/launcher2/Launcher;->sIsFestivalModeOn:Z

    .line 350
    sput-boolean v2, Lcom/android/launcher2/Launcher;->SEC_PRODUCT_FEATURE_LAUNCHER_KITKAT_ADD_TO_HOMESCREEN_CONCEPT:Z

    .line 351
    sput-boolean v2, Lcom/android/launcher2/Launcher;->sIsSecretModeOn:Z

    .line 358
    sput-boolean v2, Lcom/android/launcher2/Launcher;->sIsStopped:Z

    .line 360
    sput v2, Lcom/android/launcher2/Launcher;->sActivityOrientation:I

    .line 363
    sput v2, Lcom/android/launcher2/Launcher;->sWhichTransitionEffect:I

    .line 368
    sput-boolean v3, Lcom/android/launcher2/Launcher;->mAlwaysMicOn:Z

    .line 369
    sput v2, Lcom/android/launcher2/Launcher;->mIsBabyCryingEnable:I

    .line 370
    sput v2, Lcom/android/launcher2/Launcher;->mIsDoorbellEnable:I

    .line 371
    sput-boolean v2, Lcom/android/launcher2/Launcher;->mTalkbackEnable:Z

    .line 373
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    .line 374
    sget-object v0, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 375
    sget-object v0, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 4047
    sput-boolean v2, Lcom/android/launcher2/Launcher;->bSupportAirMove:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 380
    invoke-direct {p0, v1}, Landroid/app/ActivityGroup;-><init>(Z)V

    .line 226
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->mIsExternalHelpActivity:Z

    .line 231
    new-instance v0, Lcom/android/launcher2/Launcher$CloseSystemDialogsIntentReceiver;

    invoke-direct {v0, p0, v2}, Lcom/android/launcher2/Launcher$CloseSystemDialogsIntentReceiver;-><init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/Launcher$1;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    .line 233
    new-instance v0, Lcom/android/launcher2/Launcher$TalkbackEnableIntentReceiver;

    invoke-direct {v0, p0, v2}, Lcom/android/launcher2/Launcher$TalkbackEnableIntentReceiver;-><init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/Launcher$1;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->mTalkbackEnableReceiver:Landroid/content/BroadcastReceiver;

    .line 240
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->mAttached:Z

    .line 241
    iput-boolean v3, p0, Lcom/android/launcher2/Launcher;->mRemainSavedInstance:Z

    .line 247
    iput-object v2, p0, Lcom/android/launcher2/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    .line 255
    iput-object v2, p0, Lcom/android/launcher2/Launcher;->mAppMarketIntent:Landroid/content/Intent;

    .line 258
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->mHasMenuKey:Z

    .line 260
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->mAutoRestart:Z

    .line 261
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->mPaused:Z

    .line 264
    iput-boolean v3, p0, Lcom/android/launcher2/Launcher;->mResumed:Z

    .line 317
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->mActivityCallbacks:Landroid/util/SparseArray;

    .line 347
    iput v1, p0, Lcom/android/launcher2/Launcher;->mPrevOrientationHelp:I

    .line 359
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->isFolderCreatedFromMenuButton:Z

    .line 365
    const-string v0, "content.alwaysmicon.provider"

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->AUTHORITY:Ljava/lang/String;

    .line 366
    const-string v0, "content://content.alwaysmicon.provider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->content_uri:Landroid/net/Uri;

    .line 367
    new-instance v0, Lcom/android/launcher2/Launcher$AlwaysMicOnObserver;

    invoke-direct {v0, p0}, Lcom/android/launcher2/Launcher$AlwaysMicOnObserver;-><init>(Lcom/android/launcher2/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->mAlwaysMicOnObserver:Landroid/database/ContentObserver;

    .line 1152
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->mIsDestroyed:Z

    .line 1240
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->mStateAnimatorProviders:Ljava/util/ArrayList;

    .line 1252
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->mInMenu:Z

    .line 1415
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->mExitingAllApps:Z

    .line 2168
    new-instance v0, Lcom/android/launcher2/Launcher$7;

    invoke-direct {v0, p0}, Lcom/android/launcher2/Launcher$7;-><init>(Lcom/android/launcher2/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 2274
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->mWidgetAddInfo:Ljava/util/ArrayList;

    .line 4045
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->mGestureListenerRegistered:Z

    .line 4046
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->mGestureServiceConnected:Z

    .line 4048
    new-instance v0, Lcom/android/launcher2/Launcher$AirGestureSettingsChangeReceiver;

    invoke-direct {v0, p0, v2}, Lcom/android/launcher2/Launcher$AirGestureSettingsChangeReceiver;-><init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/Launcher$1;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->mAirGestureSettingsChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 4836
    new-instance v0, Lcom/android/launcher2/Launcher$12;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/launcher2/Launcher$12;-><init>(Lcom/android/launcher2/Launcher;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->mBabyCryingDetector:Landroid/database/ContentObserver;

    .line 4845
    new-instance v0, Lcom/android/launcher2/Launcher$13;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/launcher2/Launcher$13;-><init>(Lcom/android/launcher2/Launcher;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->mDoorbellDetector:Landroid/database/ContentObserver;

    .line 381
    sput-object p0, Lcom/android/launcher2/Launcher;->launcher:Lcom/android/launcher2/Launcher;

    .line 382
    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Launcher ctor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    invoke-static {}, Lcom/android/launcher2/ShadowStyleable;->init()V

    .line 384
    return-void
.end method

.method private AllWidgetCount()I
    .locals 2

    .prologue
    .line 2201
    new-instance v0, Lcom/android/launcher2/AvailableAppWidgetListProvider;

    invoke-direct {v0, p0}, Lcom/android/launcher2/AvailableAppWidgetListProvider;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/android/launcher2/AvailableAppWidgetListProvider;->getAvailableWidgets()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Lcom/android/launcher2/AvailableSamsungWidgetListProvider;

    invoke-direct {v1, p0}, Lcom/android/launcher2/AvailableSamsungWidgetListProvider;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/android/launcher2/AvailableSamsungWidgetListProvider;->getAvailableWidgets()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    new-instance v1, Lcom/android/launcher2/AvailableShortcutWidgetListProvider;

    invoke-direct {v1, p0}, Lcom/android/launcher2/AvailableShortcutWidgetListProvider;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/android/launcher2/AvailableShortcutWidgetListProvider;->getAvailableWidgets()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    new-instance v1, Lcom/android/launcher2/AvailableSurfaceWidgetListProvider;

    invoke-direct {v1, p0}, Lcom/android/launcher2/AvailableSurfaceWidgetListProvider;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/android/launcher2/AvailableSurfaceWidgetListProvider;->getAvailableWidgets()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private acceptFilter()Z
    .locals 2

    .prologue
    .line 1763
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1765
    .local v0, "inputManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isFullscreenMode()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic access$1100()Z
    .locals 1

    .prologue
    .line 165
    sget-boolean v0, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/android/launcher2/Launcher;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/launcher2/Launcher;

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->bindAndUnBindGestureService()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/launcher2/Launcher;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/Launcher;

    .prologue
    .line 165
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mIsDestroyed:Z

    return v0
.end method

.method static synthetic access$300(Lcom/android/launcher2/Launcher;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/launcher2/Launcher;

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->finishShowAllApps()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/launcher2/Launcher;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/launcher2/Launcher;

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->finishExitAllApps()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/launcher2/Launcher;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/launcher2/Launcher;

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->exitWidgetResizeMode()V

    return-void
.end method

.method static synthetic access$600(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/LauncherModel;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/Launcher;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mModel:Lcom/android/launcher2/LauncherModel;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/launcher2/Launcher;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/Launcher;

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->AllWidgetCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$902(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 165
    sput-boolean p0, Lcom/android/launcher2/Launcher;->sPopupMenuShowing:Z

    return p0
.end method

.method private addNonDisableAppToList()V
    .locals 7

    .prologue
    .line 4464
    const-string v0, "nondisableapps"

    .line 4465
    .local v0, "TAG_APPORDER":Ljava/lang/String;
    const/4 v4, 0x0

    .line 4466
    .local v4, "resParser":Landroid/content/res/XmlResourceParser;
    const/4 v3, 0x0

    .line 4467
    .local v3, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070003

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v4

    .line 4469
    :try_start_0
    const-string v5, "nondisableapps"

    invoke-static {v4, v5}, Lcom/android/launcher2/LauncherProvider;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 4477
    :goto_0
    move-object v3, v4

    .line 4479
    :try_start_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    .line 4480
    .local v2, "eventType":I
    :goto_1
    const/4 v5, 0x1

    if-eq v2, v5, :cond_1

    .line 4481
    const/4 v5, 0x4

    if-ne v2, v5, :cond_0

    .line 4482
    sget-object v5, Lcom/android/launcher2/Launcher;->mNonDisableItems:Ljava/util/ArrayList;

    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4484
    :cond_0
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-result v2

    goto :goto_1

    .line 4470
    .end local v2    # "eventType":I
    :catch_0
    move-exception v1

    .line 4472
    .local v1, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_0

    .line 4473
    .end local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v1

    .line 4475
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 4486
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 4488
    .local v1, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 4494
    .end local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :cond_1
    :goto_2
    return-void

    .line 4489
    :catch_3
    move-exception v1

    .line 4491
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method private addNonDisableAppToListCsc()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 4497
    const/4 v6, 0x0

    .line 4498
    .local v6, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v0, 0x0

    .line 4501
    .local v0, "cscFile":Ljava/io/FileReader;
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v7, "/system/csc/default_disableapp_skiplist.xml"

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4503
    .local v2, "cscFileChk":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-lez v7, :cond_2

    .line 4504
    new-instance v1, Ljava/io/FileReader;

    const-string v7, "/system/csc/default_disableapp_skiplist.xml"

    invoke-direct {v1, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4505
    .end local v0    # "cscFile":Ljava/io/FileReader;
    .local v1, "cscFile":Ljava/io/FileReader;
    :try_start_1
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v5

    .line 4506
    .local v5, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 4507
    invoke-virtual {v5}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v6

    .line 4508
    invoke-interface {v6, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 4510
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    .line 4511
    .local v4, "eventType":I
    :goto_0
    if-eq v4, v12, :cond_1

    .line 4512
    const/4 v7, 0x4

    if-ne v4, v7, :cond_0

    .line 4513
    sget-object v7, Lcom/android/launcher2/Launcher;->mNonDisableItems:Ljava/util/ArrayList;

    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4515
    :cond_0
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 4525
    .end local v1    # "cscFile":Ljava/io/FileReader;
    .end local v4    # "eventType":I
    .end local v5    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v0    # "cscFile":Ljava/io/FileReader;
    :cond_2
    if-eqz v0, :cond_3

    .line 4527
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 4532
    .end local v2    # "cscFileChk":Ljava/io/File;
    :cond_3
    :goto_1
    return-void

    .line 4518
    :catch_0
    move-exception v3

    .line 4520
    .local v3, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_2
    :try_start_3
    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4525
    if-eqz v0, :cond_3

    .line 4527
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 4528
    :catch_1
    move-exception v7

    goto :goto_1

    .line 4521
    .end local v3    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_2
    move-exception v3

    .line 4523
    .local v3, "e":Ljava/io/IOException;
    :goto_3
    :try_start_5
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 4525
    if-eqz v0, :cond_3

    .line 4527
    :try_start_6
    invoke-virtual {v0}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_1

    .line 4528
    :catch_3
    move-exception v7

    goto :goto_1

    .line 4525
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_4
    if-eqz v0, :cond_4

    .line 4527
    :try_start_7
    invoke-virtual {v0}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 4529
    :cond_4
    :goto_5
    throw v7

    .line 4528
    .restart local v2    # "cscFileChk":Ljava/io/File;
    :catch_4
    move-exception v7

    goto :goto_1

    .end local v2    # "cscFileChk":Ljava/io/File;
    :catch_5
    move-exception v8

    goto :goto_5

    .line 4525
    .end local v0    # "cscFile":Ljava/io/FileReader;
    .restart local v1    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFileChk":Ljava/io/File;
    :catchall_1
    move-exception v7

    move-object v0, v1

    .end local v1    # "cscFile":Ljava/io/FileReader;
    .restart local v0    # "cscFile":Ljava/io/FileReader;
    goto :goto_4

    .line 4521
    .end local v0    # "cscFile":Ljava/io/FileReader;
    .restart local v1    # "cscFile":Ljava/io/FileReader;
    :catch_6
    move-exception v3

    move-object v0, v1

    .end local v1    # "cscFile":Ljava/io/FileReader;
    .restart local v0    # "cscFile":Ljava/io/FileReader;
    goto :goto_3

    .line 4518
    .end local v0    # "cscFile":Ljava/io/FileReader;
    .restart local v1    # "cscFile":Ljava/io/FileReader;
    :catch_7
    move-exception v3

    move-object v0, v1

    .end local v1    # "cscFile":Ljava/io/FileReader;
    .restart local v0    # "cscFile":Ljava/io/FileReader;
    goto :goto_2
.end method

.method private addRemovableAppToList()V
    .locals 13

    .prologue
    .line 4579
    const/4 v0, 0x0

    .line 4580
    .local v0, "appIdList":Ljava/lang/String;
    const/4 v7, 0x0

    .line 4581
    .local v7, "instream":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 4582
    .local v5, "inputreader":Ljava/io/InputStreamReader;
    const/4 v1, 0x0

    .line 4584
    .local v1, "buffreader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v10, Ljava/io/File;

    const-string v11, "/system/etc/removable_preload.txt"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4585
    .local v10, "mFile":Ljava/io/File;
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4586
    .end local v7    # "instream":Ljava/io/InputStream;
    .local v8, "instream":Ljava/io/InputStream;
    if-eqz v8, :cond_5

    .line 4587
    :try_start_1
    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 4588
    .end local v5    # "inputreader":Ljava/io/InputStreamReader;
    .local v6, "inputreader":Ljava/io/InputStreamReader;
    :try_start_2
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_b
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 4591
    .end local v1    # "buffreader":Ljava/io/BufferedReader;
    .local v2, "buffreader":Ljava/io/BufferedReader;
    :cond_0
    :goto_0
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    .local v9, "line":Ljava/lang/String;
    if-eqz v9, :cond_4

    .line 4592
    const-string v11, "name="

    invoke-virtual {v9, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 4593
    const-string v11, "name=\'"

    const-string v12, ""

    invoke-virtual {v9, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 4594
    const-string v11, "\'"

    const-string v12, ""

    invoke-virtual {v0, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 4595
    sget-object v11, Lcom/android/launcher2/Launcher;->mRemovableAppItems:Ljava/util/ArrayList;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_0

    .line 4599
    .end local v9    # "line":Ljava/lang/String;
    :catch_0
    move-exception v4

    move-object v1, v2

    .end local v2    # "buffreader":Ljava/io/BufferedReader;
    .restart local v1    # "buffreader":Ljava/io/BufferedReader;
    move-object v5, v6

    .end local v6    # "inputreader":Ljava/io/InputStreamReader;
    .restart local v5    # "inputreader":Ljava/io/InputStreamReader;
    move-object v7, v8

    .line 4601
    .end local v8    # "instream":Ljava/io/InputStream;
    .end local v10    # "mFile":Ljava/io/File;
    .local v4, "e1":Ljava/io/FileNotFoundException;
    .restart local v7    # "instream":Ljava/io/InputStream;
    :goto_1
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4608
    if-eqz v7, :cond_1

    .line 4609
    :try_start_5
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 4610
    :cond_1
    if-eqz v5, :cond_2

    .line 4611
    invoke-virtual {v5}, Ljava/io/InputStreamReader;->close()V

    .line 4612
    :cond_2
    if-eqz v1, :cond_3

    .line 4613
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 4619
    .end local v4    # "e1":Ljava/io/FileNotFoundException;
    :cond_3
    :goto_2
    return-void

    .end local v1    # "buffreader":Ljava/io/BufferedReader;
    .end local v5    # "inputreader":Ljava/io/InputStreamReader;
    .end local v7    # "instream":Ljava/io/InputStream;
    .restart local v2    # "buffreader":Ljava/io/BufferedReader;
    .restart local v6    # "inputreader":Ljava/io/InputStreamReader;
    .restart local v8    # "instream":Ljava/io/InputStream;
    .restart local v9    # "line":Ljava/lang/String;
    .restart local v10    # "mFile":Ljava/io/File;
    :cond_4
    move-object v1, v2

    .end local v2    # "buffreader":Ljava/io/BufferedReader;
    .restart local v1    # "buffreader":Ljava/io/BufferedReader;
    move-object v5, v6

    .line 4608
    .end local v6    # "inputreader":Ljava/io/InputStreamReader;
    .end local v9    # "line":Ljava/lang/String;
    .restart local v5    # "inputreader":Ljava/io/InputStreamReader;
    :cond_5
    if-eqz v8, :cond_6

    .line 4609
    :try_start_6
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    .line 4610
    :cond_6
    if-eqz v5, :cond_7

    .line 4611
    invoke-virtual {v5}, Ljava/io/InputStreamReader;->close()V

    .line 4612
    :cond_7
    if-eqz v1, :cond_8

    .line 4613
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    :cond_8
    move-object v7, v8

    .line 4617
    .end local v8    # "instream":Ljava/io/InputStream;
    .restart local v7    # "instream":Ljava/io/InputStream;
    goto :goto_2

    .line 4614
    .end local v7    # "instream":Ljava/io/InputStream;
    .restart local v8    # "instream":Ljava/io/InputStream;
    :catch_1
    move-exception v3

    .line 4616
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    move-object v7, v8

    .line 4618
    .end local v8    # "instream":Ljava/io/InputStream;
    .restart local v7    # "instream":Ljava/io/InputStream;
    goto :goto_2

    .line 4614
    .end local v3    # "e":Ljava/io/IOException;
    .end local v10    # "mFile":Ljava/io/File;
    .restart local v4    # "e1":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v3

    .line 4616
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 4602
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "e1":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v3

    .line 4604
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_7
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 4608
    if-eqz v7, :cond_9

    .line 4609
    :try_start_8
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 4610
    :cond_9
    if-eqz v5, :cond_a

    .line 4611
    invoke-virtual {v5}, Ljava/io/InputStreamReader;->close()V

    .line 4612
    :cond_a
    if-eqz v1, :cond_3

    .line 4613
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_2

    .line 4614
    :catch_4
    move-exception v3

    .line 4616
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 4607
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    .line 4608
    :goto_4
    if-eqz v7, :cond_b

    .line 4609
    :try_start_9
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 4610
    :cond_b
    if-eqz v5, :cond_c

    .line 4611
    invoke-virtual {v5}, Ljava/io/InputStreamReader;->close()V

    .line 4612
    :cond_c
    if-eqz v1, :cond_d

    .line 4613
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 4617
    :cond_d
    :goto_5
    throw v11

    .line 4614
    :catch_5
    move-exception v3

    .line 4616
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 4607
    .end local v3    # "e":Ljava/io/IOException;
    .end local v7    # "instream":Ljava/io/InputStream;
    .restart local v8    # "instream":Ljava/io/InputStream;
    .restart local v10    # "mFile":Ljava/io/File;
    :catchall_1
    move-exception v11

    move-object v7, v8

    .end local v8    # "instream":Ljava/io/InputStream;
    .restart local v7    # "instream":Ljava/io/InputStream;
    goto :goto_4

    .end local v5    # "inputreader":Ljava/io/InputStreamReader;
    .end local v7    # "instream":Ljava/io/InputStream;
    .restart local v6    # "inputreader":Ljava/io/InputStreamReader;
    .restart local v8    # "instream":Ljava/io/InputStream;
    :catchall_2
    move-exception v11

    move-object v5, v6

    .end local v6    # "inputreader":Ljava/io/InputStreamReader;
    .restart local v5    # "inputreader":Ljava/io/InputStreamReader;
    move-object v7, v8

    .end local v8    # "instream":Ljava/io/InputStream;
    .restart local v7    # "instream":Ljava/io/InputStream;
    goto :goto_4

    .end local v1    # "buffreader":Ljava/io/BufferedReader;
    .end local v5    # "inputreader":Ljava/io/InputStreamReader;
    .end local v7    # "instream":Ljava/io/InputStream;
    .restart local v2    # "buffreader":Ljava/io/BufferedReader;
    .restart local v6    # "inputreader":Ljava/io/InputStreamReader;
    .restart local v8    # "instream":Ljava/io/InputStream;
    :catchall_3
    move-exception v11

    move-object v1, v2

    .end local v2    # "buffreader":Ljava/io/BufferedReader;
    .restart local v1    # "buffreader":Ljava/io/BufferedReader;
    move-object v5, v6

    .end local v6    # "inputreader":Ljava/io/InputStreamReader;
    .restart local v5    # "inputreader":Ljava/io/InputStreamReader;
    move-object v7, v8

    .end local v8    # "instream":Ljava/io/InputStream;
    .restart local v7    # "instream":Ljava/io/InputStream;
    goto :goto_4

    .line 4602
    .end local v7    # "instream":Ljava/io/InputStream;
    .restart local v8    # "instream":Ljava/io/InputStream;
    :catch_6
    move-exception v3

    move-object v7, v8

    .end local v8    # "instream":Ljava/io/InputStream;
    .restart local v7    # "instream":Ljava/io/InputStream;
    goto :goto_3

    .end local v5    # "inputreader":Ljava/io/InputStreamReader;
    .end local v7    # "instream":Ljava/io/InputStream;
    .restart local v6    # "inputreader":Ljava/io/InputStreamReader;
    .restart local v8    # "instream":Ljava/io/InputStream;
    :catch_7
    move-exception v3

    move-object v5, v6

    .end local v6    # "inputreader":Ljava/io/InputStreamReader;
    .restart local v5    # "inputreader":Ljava/io/InputStreamReader;
    move-object v7, v8

    .end local v8    # "instream":Ljava/io/InputStream;
    .restart local v7    # "instream":Ljava/io/InputStream;
    goto :goto_3

    .end local v1    # "buffreader":Ljava/io/BufferedReader;
    .end local v5    # "inputreader":Ljava/io/InputStreamReader;
    .end local v7    # "instream":Ljava/io/InputStream;
    .restart local v2    # "buffreader":Ljava/io/BufferedReader;
    .restart local v6    # "inputreader":Ljava/io/InputStreamReader;
    .restart local v8    # "instream":Ljava/io/InputStream;
    :catch_8
    move-exception v3

    move-object v1, v2

    .end local v2    # "buffreader":Ljava/io/BufferedReader;
    .restart local v1    # "buffreader":Ljava/io/BufferedReader;
    move-object v5, v6

    .end local v6    # "inputreader":Ljava/io/InputStreamReader;
    .restart local v5    # "inputreader":Ljava/io/InputStreamReader;
    move-object v7, v8

    .end local v8    # "instream":Ljava/io/InputStream;
    .restart local v7    # "instream":Ljava/io/InputStream;
    goto :goto_3

    .line 4599
    .end local v10    # "mFile":Ljava/io/File;
    :catch_9
    move-exception v4

    goto :goto_1

    .end local v7    # "instream":Ljava/io/InputStream;
    .restart local v8    # "instream":Ljava/io/InputStream;
    .restart local v10    # "mFile":Ljava/io/File;
    :catch_a
    move-exception v4

    move-object v7, v8

    .end local v8    # "instream":Ljava/io/InputStream;
    .restart local v7    # "instream":Ljava/io/InputStream;
    goto :goto_1

    .end local v5    # "inputreader":Ljava/io/InputStreamReader;
    .end local v7    # "instream":Ljava/io/InputStream;
    .restart local v6    # "inputreader":Ljava/io/InputStreamReader;
    .restart local v8    # "instream":Ljava/io/InputStream;
    :catch_b
    move-exception v4

    move-object v5, v6

    .end local v6    # "inputreader":Ljava/io/InputStreamReader;
    .restart local v5    # "inputreader":Ljava/io/InputStreamReader;
    move-object v7, v8

    .end local v8    # "instream":Ljava/io/InputStream;
    .restart local v7    # "instream":Ljava/io/InputStream;
    goto :goto_1
.end method

.method private addRemovableCustomerAppToList()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    .line 4622
    const-string v0, "removablecustomerapps"

    .line 4623
    .local v0, "TAG_APPORDER":Ljava/lang/String;
    const/4 v8, 0x0

    .line 4624
    .local v8, "resParser":Landroid/content/res/XmlResourceParser;
    const/4 v7, 0x0

    .line 4625
    .local v7, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v1, 0x0

    .line 4628
    .local v1, "cscFile":Ljava/io/FileReader;
    :try_start_0
    new-instance v3, Ljava/io/File;

    const-string v9, "/system/csc/default_removablecustomerapp_list.xml"

    invoke-direct {v3, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4630
    .local v3, "cscFileChk":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-lez v9, :cond_1

    .line 4631
    new-instance v2, Ljava/io/FileReader;

    const-string v9, "/system/csc/default_removablecustomerapp_list.xml"

    invoke-direct {v2, v9}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4632
    .end local v1    # "cscFile":Ljava/io/FileReader;
    .local v2, "cscFile":Ljava/io/FileReader;
    :try_start_1
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v6

    .line 4633
    .local v6, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v9, 0x1

    invoke-virtual {v6, v9}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 4634
    invoke-virtual {v6}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v7

    .line 4635
    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v1, v2

    .line 4642
    .end local v2    # "cscFile":Ljava/io/FileReader;
    .end local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v1    # "cscFile":Ljava/io/FileReader;
    :goto_0
    :try_start_2
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v5

    .line 4643
    .local v5, "eventType":I
    :goto_1
    if-eq v5, v14, :cond_2

    .line 4644
    const/4 v9, 0x4

    if-ne v5, v9, :cond_0

    .line 4645
    sget-object v9, Lcom/android/launcher2/Launcher;->mRemovableCustomerAppItems:Ljava/util/ArrayList;

    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4647
    :cond_0
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    goto :goto_1

    .line 4637
    .end local v5    # "eventType":I
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070004

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v8

    .line 4638
    const-string v9, "removablecustomerapps"

    invoke-static {v8, v9}, Lcom/android/launcher2/LauncherProvider;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4639
    move-object v7, v8

    goto :goto_0

    .line 4656
    .restart local v5    # "eventType":I
    :cond_2
    if-eqz v1, :cond_3

    .line 4658
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    .line 4663
    .end local v3    # "cscFileChk":Ljava/io/File;
    .end local v5    # "eventType":I
    :cond_3
    :goto_2
    return-void

    .line 4649
    :catch_0
    move-exception v4

    .line 4651
    .local v4, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_3
    :try_start_4
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4656
    if-eqz v1, :cond_3

    .line 4658
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    .line 4659
    :catch_1
    move-exception v9

    goto :goto_2

    .line 4652
    .end local v4    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_2
    move-exception v4

    .line 4654
    .local v4, "e":Ljava/io/IOException;
    :goto_4
    :try_start_6
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 4656
    if-eqz v1, :cond_3

    .line 4658
    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_2

    .line 4659
    :catch_3
    move-exception v9

    goto :goto_2

    .line 4656
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_5
    if-eqz v1, :cond_4

    .line 4658
    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    .line 4660
    :cond_4
    :goto_6
    throw v9

    .line 4659
    .restart local v3    # "cscFileChk":Ljava/io/File;
    .restart local v5    # "eventType":I
    :catch_4
    move-exception v9

    goto :goto_2

    .end local v3    # "cscFileChk":Ljava/io/File;
    .end local v5    # "eventType":I
    :catch_5
    move-exception v10

    goto :goto_6

    .line 4656
    .end local v1    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v3    # "cscFileChk":Ljava/io/File;
    :catchall_1
    move-exception v9

    move-object v1, v2

    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v1    # "cscFile":Ljava/io/FileReader;
    goto :goto_5

    .line 4652
    .end local v1    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    :catch_6
    move-exception v4

    move-object v1, v2

    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v1    # "cscFile":Ljava/io/FileReader;
    goto :goto_4

    .line 4649
    .end local v1    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    :catch_7
    move-exception v4

    move-object v1, v2

    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v1    # "cscFile":Ljava/io/FileReader;
    goto :goto_3
.end method

.method private addRemovablePreloadAppToList()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    .line 4535
    const-string v0, "removablepreloadapps"

    .line 4536
    .local v0, "TAG_APPORDER":Ljava/lang/String;
    const/4 v8, 0x0

    .line 4537
    .local v8, "resParser":Landroid/content/res/XmlResourceParser;
    const/4 v7, 0x0

    .line 4538
    .local v7, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v1, 0x0

    .line 4541
    .local v1, "cscFile":Ljava/io/FileReader;
    :try_start_0
    new-instance v3, Ljava/io/File;

    const-string v9, "/system/csc/default_removablepreloadapp_list.xml"

    invoke-direct {v3, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4543
    .local v3, "cscFileChk":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-lez v9, :cond_1

    .line 4544
    new-instance v2, Ljava/io/FileReader;

    const-string v9, "/system/csc/default_removablepreloadapp_list.xml"

    invoke-direct {v2, v9}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4545
    .end local v1    # "cscFile":Ljava/io/FileReader;
    .local v2, "cscFile":Ljava/io/FileReader;
    :try_start_1
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v6

    .line 4546
    .local v6, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v9, 0x1

    invoke-virtual {v6, v9}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 4547
    invoke-virtual {v6}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v7

    .line 4548
    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v1, v2

    .line 4555
    .end local v2    # "cscFile":Ljava/io/FileReader;
    .end local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v1    # "cscFile":Ljava/io/FileReader;
    :goto_0
    :try_start_2
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v5

    .line 4556
    .local v5, "eventType":I
    :goto_1
    if-eq v5, v14, :cond_2

    .line 4557
    const/4 v9, 0x4

    if-ne v5, v9, :cond_0

    .line 4558
    sget-object v9, Lcom/android/launcher2/Launcher;->mRemovablePreloadAppItems:Ljava/util/ArrayList;

    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4560
    :cond_0
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    goto :goto_1

    .line 4550
    .end local v5    # "eventType":I
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070005

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v8

    .line 4551
    const-string v9, "removablepreloadapps"

    invoke-static {v8, v9}, Lcom/android/launcher2/LauncherProvider;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4552
    move-object v7, v8

    goto :goto_0

    .line 4569
    .restart local v5    # "eventType":I
    :cond_2
    if-eqz v1, :cond_3

    .line 4571
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    .line 4576
    .end local v3    # "cscFileChk":Ljava/io/File;
    .end local v5    # "eventType":I
    :cond_3
    :goto_2
    return-void

    .line 4562
    :catch_0
    move-exception v4

    .line 4564
    .local v4, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_3
    :try_start_4
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4569
    if-eqz v1, :cond_3

    .line 4571
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    .line 4572
    :catch_1
    move-exception v9

    goto :goto_2

    .line 4565
    .end local v4    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_2
    move-exception v4

    .line 4567
    .local v4, "e":Ljava/io/IOException;
    :goto_4
    :try_start_6
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 4569
    if-eqz v1, :cond_3

    .line 4571
    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_2

    .line 4572
    :catch_3
    move-exception v9

    goto :goto_2

    .line 4569
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_5
    if-eqz v1, :cond_4

    .line 4571
    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    .line 4573
    :cond_4
    :goto_6
    throw v9

    .line 4572
    .restart local v3    # "cscFileChk":Ljava/io/File;
    .restart local v5    # "eventType":I
    :catch_4
    move-exception v9

    goto :goto_2

    .end local v3    # "cscFileChk":Ljava/io/File;
    .end local v5    # "eventType":I
    :catch_5
    move-exception v10

    goto :goto_6

    .line 4569
    .end local v1    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v3    # "cscFileChk":Ljava/io/File;
    :catchall_1
    move-exception v9

    move-object v1, v2

    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v1    # "cscFile":Ljava/io/FileReader;
    goto :goto_5

    .line 4565
    .end local v1    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    :catch_6
    move-exception v4

    move-object v1, v2

    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v1    # "cscFile":Ljava/io/FileReader;
    goto :goto_4

    .line 4562
    .end local v1    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    :catch_7
    move-exception v4

    move-object v1, v2

    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v1    # "cscFile":Ljava/io/FileReader;
    goto :goto_3
.end method

.method private addToHelpMenuOption(Landroid/view/MenuItem;)V
    .locals 6
    .param p1, "homeHelp"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3099
    :try_start_0
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "easy_mode_switch"

    const/4 v5, 0x0

    invoke-static {v1, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_0
    sput-boolean v1, Lcom/android/launcher2/Launcher;->mEasyStateForHelpApp:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3106
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->isHelpAppAvailable()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3108
    sget-boolean v1, Lcom/android/launcher2/Launcher;->mEasyStateForHelpApp:Z

    if-nez v1, :cond_1

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/LauncherApplication;->isKnoxMode()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 3110
    :cond_1
    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3116
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v1, v3

    .line 3099
    goto :goto_0

    .line 3100
    :catch_0
    move-exception v0

    .line 3102
    .local v0, "e":Ljava/lang/Exception;
    sget-boolean v1, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    const-string v1, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Stuck to help app because of indicated Easymode now   Error="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 3113
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-interface {p1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method

.method private bindAndUnBindGestureService()V
    .locals 4

    .prologue
    .line 4150
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->isAirMoveOninSettings()Z

    move-result v0

    .line 4151
    .local v0, "enable":Z
    sget-boolean v1, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 4152
    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bindAndUnBindGestureService() isEnable : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4155
    :cond_0
    if-eqz v0, :cond_2

    .line 4156
    new-instance v1, Lcom/samsung/android/service/gesture/GestureManager;

    invoke-direct {v1, p0, p0}, Lcom/samsung/android/service/gesture/GestureManager;-><init>(Landroid/content/Context;Lcom/samsung/android/service/gesture/GestureManager$ServiceConnectionListener;)V

    iput-object v1, p0, Lcom/android/launcher2/Launcher;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    .line 4164
    :cond_1
    :goto_0
    return-void

    .line 4157
    :cond_2
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    if-eqz v1, :cond_1

    .line 4159
    iget-boolean v1, p0, Lcom/android/launcher2/Launcher;->mGestureListenerRegistered:Z

    if-eqz v1, :cond_3

    .line 4160
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v2, "ir_provider"

    invoke-virtual {v1, p0, v2}, Lcom/samsung/android/service/gesture/GestureManager;->unregisterListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;)V

    .line 4161
    :cond_3
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    invoke-virtual {v1}, Lcom/samsung/android/service/gesture/GestureManager;->unbindFromService()V

    .line 4162
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/launcher2/Launcher;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    goto :goto_0
.end method

.method private buildWidgetMap(Ljava/util/HashMap;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ComponentName;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1081
    .local p1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/content/ComponentName;Ljava/lang/Integer;>;"
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v5}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v4

    .line 1082
    .local v4, "ws":Lcom/android/launcher2/Workspace;
    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v3

    .line 1085
    .local v3, "screenCount":I
    const/4 v2, 0x0

    .local v2, "screen":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 1086
    invoke-virtual {v4, v2}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 1087
    .local v0, "cl":Lcom/android/launcher2/CellLayout;
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getChildrenLayout()Lcom/android/launcher2/CellLayoutChildren;

    move-result-object v1

    .line 1089
    .local v1, "clc":Lcom/android/launcher2/CellLayoutChildren;
    new-instance v5, Lcom/android/launcher2/Launcher$2;

    invoke-direct {v5, p0, p1}, Lcom/android/launcher2/Launcher$2;-><init>(Lcom/android/launcher2/Launcher;Ljava/util/HashMap;)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lcom/android/launcher2/CellLayoutChildren;->iterateBaseItems(Lcom/android/launcher2/CellLayoutChildren$BaseItemIterator;Ljava/lang/Object;)V

    .line 1085
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1103
    .end local v0    # "cl":Lcom/android/launcher2/CellLayout;
    .end local v1    # "clc":Lcom/android/launcher2/CellLayoutChildren;
    :cond_0
    return-void
.end method

.method private cancelAppFolderRemoveDialog()V
    .locals 3

    .prologue
    .line 2423
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 2424
    .local v1, "manager":Landroid/app/FragmentManager;
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 2425
    .local v2, "transaction":Landroid/app/FragmentTransaction;
    invoke-static {v1}, Lcom/android/launcher2/AppFolderRemoveDialog;->getCurrentInstance(Landroid/app/FragmentManager;)Lcom/android/launcher2/AppFolderRemoveDialog;

    move-result-object v0

    .line 2426
    .local v0, "dialog":Lcom/android/launcher2/AppFolderRemoveDialog;
    if-eqz v0, :cond_0

    .line 2427
    invoke-virtual {v0}, Lcom/android/launcher2/AppFolderRemoveDialog;->dismiss()V

    .line 2428
    invoke-virtual {v2, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 2429
    invoke-virtual {v0}, Lcom/android/launcher2/AppFolderRemoveDialog;->cancelDelete()V

    .line 2430
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 2432
    :cond_0
    return-void
.end method

.method private clearTypedText()V
    .locals 2

    .prologue
    .line 2112
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 2113
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clearSpans()V

    .line 2114
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 2115
    return-void
.end method

.method private exitWidgetResizeMode()V
    .locals 2

    .prologue
    .line 2586
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v0

    .line 2587
    .local v0, "workspace":Lcom/android/launcher2/Workspace;
    if-eqz v0, :cond_0

    .line 2588
    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->exitWidgetResizeMode()V

    .line 2589
    :cond_0
    return-void
.end method

.method private fillInHomeItem(Lcom/android/launcher2/HomeItem;Lcom/android/launcher2/Launcher$WidgetAddInfo;[I)V
    .locals 4
    .param p1, "homeInfo"    # Lcom/android/launcher2/HomeItem;
    .param p2, "addInfo"    # Lcom/android/launcher2/Launcher$WidgetAddInfo;
    .param p3, "span"    # [I

    .prologue
    const/4 v1, -0x1

    .line 947
    iget v0, p2, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mSizeX:I

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget v0, p3, v0

    :goto_0
    iput v0, p1, Lcom/android/launcher2/HomeItem;->spanX:I

    .line 948
    iget v0, p2, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mSizeY:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    aget v0, p3, v0

    :goto_1
    iput v0, p1, Lcom/android/launcher2/HomeItem;->spanY:I

    .line 949
    const-wide/16 v2, -0x64

    iput-wide v2, p1, Lcom/android/launcher2/HomeItem;->container:J

    .line 950
    iput v1, p1, Lcom/android/launcher2/HomeItem;->mScreen:I

    .line 951
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/android/launcher2/HomeItem;->dropPos:[I

    .line 952
    return-void

    :cond_0
    move v0, v1

    .line 947
    goto :goto_0

    :cond_1
    move v0, v1

    .line 948
    goto :goto_1
.end method

.method private finishExitAllApps()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1547
    iput-boolean v6, p0, Lcom/android/launcher2/Launcher;->mExitingAllApps:Z

    .line 1548
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v4}, Lcom/android/launcher2/MenuView;->onFinishExitAllApps()V

    .line 1549
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 1550
    .local v0, "fm":Landroid/app/FragmentManager;
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 1551
    .local v3, "transaction":Landroid/app/FragmentTransaction;
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4, v6}, Lcom/android/launcher2/HomeView;->setVisibility(I)V

    .line 1552
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/android/launcher2/MenuView;->setVisibility(I)V

    .line 1553
    iget-boolean v4, p0, Lcom/android/launcher2/Launcher;->mPaused:Z

    if-eqz v4, :cond_1

    .line 1554
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1563
    :goto_0
    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 1564
    iput-boolean v6, p0, Lcom/android/launcher2/Launcher;->mInMenu:Z

    .line 1565
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x20

    invoke-virtual {v4, v5}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 1566
    sget-boolean v4, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v4, :cond_0

    const-string v4, "create_folder"

    sget-object v5, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-boolean v4, Lcom/android/launcher2/guide/GuideFragment;->isViewAppsDialogDismissed:Z

    if-eqz v4, :cond_0

    .line 1569
    const/4 v2, 0x0

    .line 1570
    .local v2, "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    new-instance v2, Lcom/android/launcher2/guide/CreateFolderGuider;

    .end local v2    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-direct {v2, p0}, Lcom/android/launcher2/guide/CreateFolderGuider;-><init>(Landroid/app/Activity;)V

    .line 1571
    .restart local v2    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    check-cast v2, Lcom/android/launcher2/guide/CreateFolderGuider;

    .end local v2    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {v2}, Lcom/android/launcher2/guide/CreateFolderGuider;->showHelpDialog_step3()V

    .line 1574
    :cond_0
    return-void

    .line 1557
    :cond_1
    :try_start_0
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1558
    :catch_0
    move-exception v1

    .line 1559
    .local v1, "illegalStateException":Ljava/lang/IllegalStateException;
    const-string v4, "Launcher"

    const-string v5, "IllegalStateException should not happen in normal use but may be in stress testing"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1560
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method private finishShowAllApps()V
    .locals 6

    .prologue
    .line 1393
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1394
    .local v1, "transaction":Landroid/app/FragmentTransaction;
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/launcher2/MenuView;->setVisibility(I)V

    .line 1395
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/16 v3, 0x80

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/android/launcher2/HomeView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 1396
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 1397
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/android/launcher2/HomeView;->setVisibility(I)V

    .line 1398
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v2}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1399
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v2}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v2

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v3}, Lcom/android/launcher2/HomeView;->getCurrentPage()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Workspace;->pauseScreen(I)V

    .line 1401
    :cond_0
    iget-boolean v2, p0, Lcom/android/launcher2/Launcher;->mIsDestroyed:Z

    if-eqz v2, :cond_1

    .line 1413
    :goto_0
    return-void

    .line 1403
    :cond_1
    iget-boolean v2, p0, Lcom/android/launcher2/Launcher;->mPaused:Z

    if-eqz v2, :cond_2

    .line 1404
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0

    .line 1407
    :cond_2
    :try_start_0
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1408
    :catch_0
    move-exception v0

    .line 1409
    .local v0, "illegalStateException":Ljava/lang/IllegalStateException;
    const-string v2, "Launcher"

    const-string v3, "IllegalStateException should not happen in normal use but may be in stress testing"

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "commit"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static getInstance()Lcom/android/launcher2/Launcher;
    .locals 3

    .prologue
    .line 387
    sget-object v1, Lcom/android/launcher2/Launcher;->launcher:Lcom/android/launcher2/Launcher;

    if-nez v1, :cond_0

    .line 388
    const-string v1, "Launcher"

    const-string v2, "Please Call me after onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    .line 390
    .local v0, "nullPointerException":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 392
    :cond_0
    sget-object v1, Lcom/android/launcher2/Launcher;->launcher:Lcom/android/launcher2/Launcher;

    return-object v1
.end method

.method private getLaunchIntentForHelpHub()Landroid/content/Intent;
    .locals 7

    .prologue
    const/16 v6, 0x258

    .line 2532
    const/4 v2, 0x0

    .line 2533
    .local v2, "intent":Landroid/content/Intent;
    iget-boolean v5, p0, Lcom/android/launcher2/Launcher;->mIsExternalHelpActivity:Z

    if-eqz v5, :cond_7

    .line 2535
    :try_start_0
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.samsung.helphub.HELP"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    .line 2536
    .end local v2    # "intent":Landroid/content/Intent;
    .local v3, "intent":Landroid/content/Intent;
    :try_start_1
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    if-lt v5, v6, :cond_1

    .line 2537
    const-string v5, "helphub:section"

    const-string v6, "homescreen"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2551
    :goto_0
    const/high16 v5, 0x10200000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2552
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/android/launcher2/Launcher;->mIsExternalHelpActivity:Z

    move-object v2, v3

    .line 2576
    .end local v3    # "intent":Landroid/content/Intent;
    .restart local v2    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_1
    return-object v2

    .line 2540
    .end local v2    # "intent":Landroid/content/Intent;
    .restart local v3    # "intent":Landroid/content/Intent;
    :cond_1
    const/4 v1, 0x0

    .line 2541
    .local v1, "homescreenItem":Ljava/lang/String;
    sget-object v5, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v6, "addapps"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v6, "add_widgets"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2542
    :cond_2
    const-string v1, "homescreen_using_home"

    .line 2547
    :cond_3
    :goto_2
    if-nez v1, :cond_6

    const-string v5, "helphub:section"

    const-string v6, "homescreen"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2549
    :goto_3
    const-string v5, "isIntentFromLauncher"

    const/4 v6, 0x1

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2554
    .end local v1    # "homescreenItem":Ljava/lang/String;
    :catch_0
    move-exception v0

    move-object v2, v3

    .line 2555
    .end local v3    # "intent":Landroid/content/Intent;
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    .restart local v2    # "intent":Landroid/content/Intent;
    :goto_4
    sget-boolean v5, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v5, :cond_0

    .line 2556
    const-string v5, "Launcher"

    const-string v6, "Unable to launch  intent= com.samsung.helphub.HELP"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 2543
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v2    # "intent":Landroid/content/Intent;
    .restart local v1    # "homescreenItem":Ljava/lang/String;
    .restart local v3    # "intent":Landroid/content/Intent;
    :cond_4
    :try_start_2
    sget-object v5, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v6, "create_folder"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2544
    const-string v1, "homescreen_create_folder"

    goto :goto_2

    .line 2545
    :cond_5
    sget-object v5, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v6, "change_wallpaper"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2546
    const-string v1, "homescreen_change_wallpaper"

    goto :goto_2

    .line 2548
    :cond_6
    const-string v5, "helphub:item"

    invoke-virtual {v3, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    .line 2562
    .end local v1    # "homescreenItem":Ljava/lang/String;
    .end local v3    # "intent":Landroid/content/Intent;
    .restart local v2    # "intent":Landroid/content/Intent;
    :cond_7
    :try_start_3
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    if-lt v5, v6, :cond_8

    .line 2563
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 2564
    .local v4, "pm":Landroid/content/pm/PackageManager;
    const-string v5, "com.samsung.helphub"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2565
    goto :goto_1

    .line 2566
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    :cond_8
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2567
    .end local v2    # "intent":Landroid/content/Intent;
    .restart local v3    # "intent":Landroid/content/Intent;
    :try_start_4
    const-string v5, "com.samsung.helphub"

    const-string v6, "com.samsung.helphub.HelpHubSecondDepthActivity"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2568
    const/high16 v5, 0x10200000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
    :try_end_4
    .catch Landroid/content/ActivityNotFoundException; {:try_start_4 .. :try_end_4} :catch_2

    move-object v2, v3

    .end local v3    # "intent":Landroid/content/Intent;
    .restart local v2    # "intent":Landroid/content/Intent;
    goto :goto_1

    .line 2571
    :catch_1
    move-exception v0

    .line 2572
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    :goto_5
    sget-boolean v5, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v5, :cond_0

    .line 2573
    const-string v5, "Launcher"

    const-string v6, "Unable to launch HelpHub Activity"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 2571
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v2    # "intent":Landroid/content/Intent;
    .restart local v3    # "intent":Landroid/content/Intent;
    :catch_2
    move-exception v0

    move-object v2, v3

    .end local v3    # "intent":Landroid/content/Intent;
    .restart local v2    # "intent":Landroid/content/Intent;
    goto :goto_5

    .line 2554
    :catch_3
    move-exception v0

    goto :goto_4
.end method

.method public static getStatusBarHeight(Landroid/content/res/Resources;)I
    .locals 5
    .param p0, "r"    # Landroid/content/res/Resources;

    .prologue
    .line 4791
    const/4 v1, 0x0

    .line 4792
    .local v1, "result":I
    const-string v2, "status_bar_height"

    const-string v3, "dimen"

    const-string v4, "android"

    invoke-virtual {p0, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 4793
    .local v0, "resourceId":I
    if-lez v0, :cond_0

    .line 4794
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 4796
    :cond_0
    return v1
.end method

.method private getTypedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2108
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hasPackage(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "c"    # Landroid/content/Context;
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 4375
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 4376
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x1

    .line 4378
    .local v1, "hasPkg":Z
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4383
    :goto_0
    return v1

    .line 4379
    :catch_0
    move-exception v0

    .line 4380
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    .line 4381
    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Package not found : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isFactoryMode()Z
    .locals 3

    .prologue
    .line 3367
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mFactoryModeString:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 3368
    const-string v0, "/efs/FactoryApp/factorymode"

    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->mFactoryModeString:Ljava/lang/String;

    .line 3369
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mFactoryModeString:Ljava/lang/String;

    const-string v1, "ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->mIsFactoryMode:Z

    .line 3370
    sget-boolean v0, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v0, :cond_0

    .line 3371
    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isFactoryMode string: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mFactoryModeString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3372
    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isFactoryMode returns: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/launcher2/Launcher;->mIsFactoryMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3375
    :cond_0
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mIsFactoryMode:Z

    return v0

    .line 3369
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isFactorySim()Z
    .locals 6

    .prologue
    .line 3325
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 3351
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    const-string v0, "999999999999999"

    .line 3352
    .local v0, "IMSI":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v1

    .line 3353
    .local v1, "imsi":Ljava/lang/String;
    sget-boolean v3, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v3, :cond_0

    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isFactorySim() imsi = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3354
    :cond_0
    if-eqz v1, :cond_2

    const-string v3, "999999999999999"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 3355
    sget-boolean v3, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v3, :cond_1

    const-string v3, "Launcher"

    const-string v4, "isFactorySim() : true"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3356
    :cond_1
    const/4 v3, 0x1

    .line 3360
    :goto_0
    return v3

    .line 3359
    :cond_2
    sget-boolean v3, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v3, :cond_3

    const-string v3, "Launcher"

    const-string v4, "isFactorySim() : false"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3360
    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

.method static isHomeEditMode()Z
    .locals 1

    .prologue
    .line 4018
    sget-boolean v0, Lcom/android/launcher2/Launcher;->sIsHomeEditMode:Z

    return v0
.end method

.method static isHomeRemoveMode()Z
    .locals 1

    .prologue
    .line 4028
    sget-boolean v0, Lcom/android/launcher2/Launcher;->sIsHomeRemoveMode:Z

    return v0
.end method

.method public static isInValidDragState(Landroid/view/DragEvent;)Z
    .locals 3
    .param p0, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4801
    if-nez p0, :cond_1

    .line 4806
    :cond_0
    :goto_0
    return v0

    .line 4803
    :cond_1
    sget-boolean v2, Lcom/android/launcher2/LauncherApplication;->sDNDBinding:Z

    if-eqz v2, :cond_3

    .line 4804
    invoke-virtual {p0}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/android/launcher2/DragState;

    if-nez v2, :cond_2

    sget-object v2, Lcom/android/launcher2/Launcher;->dragstate:Lcom/android/launcher2/DragState;

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 4806
    :cond_3
    invoke-virtual {p0}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/android/launcher2/DragState;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private isKnoxLauncher()Z
    .locals 3

    .prologue
    .line 925
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    .line 927
    .local v0, "myUid":I
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v1

    const/16 v2, 0x64

    if-lt v1, v2, :cond_0

    .line 928
    const/4 v1, 0x1

    .line 930
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isOptionMenuShowing()Z
    .locals 1

    .prologue
    .line 3730
    sget-boolean v0, Lcom/android/launcher2/Launcher;->sPopupMenuShowing:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/launcher2/Launcher;->sHwPopupMenuShowing:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPopupMenuShowing()Z
    .locals 1

    .prologue
    .line 3726
    sget-boolean v0, Lcom/android/launcher2/Launcher;->sPopupMenuShowing:Z

    return v0
.end method

.method private static isPropertyEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p0, "propertyName"    # Ljava/lang/String;

    .prologue
    .line 396
    const/4 v0, 0x2

    invoke-static {p0, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method private static isWifiOnly(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 4450
    const/4 v2, 0x0

    .line 4452
    .local v2, "retVal":Z
    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 4454
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    .line 4455
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 4456
    .local v1, "ni":Landroid/net/NetworkInfo;
    const/4 v4, 0x0

    if-eqz v4, :cond_0

    .line 4457
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v2, 0x1

    .line 4460
    .end local v1    # "ni":Landroid/net/NetworkInfo;
    :cond_0
    :goto_0
    return v2

    .restart local v1    # "ni":Landroid/net/NetworkInfo;
    :cond_1
    move v2, v3

    .line 4457
    goto :goto_0
.end method

.method private processAsAndroidAppWidget(Lcom/android/launcher2/Launcher$WidgetAddInfo;)Z
    .locals 8
    .param p1, "info"    # Lcom/android/launcher2/Launcher$WidgetAddInfo;

    .prologue
    const/4 v7, 0x0

    .line 1019
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/appwidget/AppWidgetProviderInfo;

    .line 1024
    .local v3, "wdgtInfo":Landroid/appwidget/AppWidgetProviderInfo;
    iget-object v4, v3, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iget-object v5, p1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v4, v5}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1028
    new-instance v2, Lcom/android/launcher2/HomePendingWidget;

    invoke-direct {v2, v3, v7, v7}, Lcom/android/launcher2/HomePendingWidget;-><init>(Landroid/appwidget/AppWidgetProviderInfo;Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1029
    .local v2, "w":Lcom/android/launcher2/HomePendingWidget;
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    iget v5, p1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mSizeX:I

    iget v6, p1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mSizeY:I

    invoke-virtual {v4, v3, v5, v6, v7}, Lcom/android/launcher2/WorkspaceSpanCalculator;->getSpanForAppWidget(Landroid/appwidget/AppWidgetProviderInfo;II[I)[I

    move-result-object v1

    .line 1030
    .local v1, "span":[I
    invoke-direct {p0, v2, p1, v1}, Lcom/android/launcher2/Launcher;->fillInHomeItem(Lcom/android/launcher2/HomeItem;Lcom/android/launcher2/Launcher$WidgetAddInfo;[I)V

    .line 1031
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4, v2}, Lcom/android/launcher2/HomeView;->addAppWidgetFromDrop(Lcom/android/launcher2/HomePendingWidget;)V

    .line 1032
    const/4 v4, 0x1

    .line 1035
    .end local v1    # "span":[I
    .end local v2    # "w":Lcom/android/launcher2/HomePendingWidget;
    .end local v3    # "wdgtInfo":Landroid/appwidget/AppWidgetProviderInfo;
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private processAsSamsungSurfaceWidget(Lcom/android/launcher2/Launcher$WidgetAddInfo;)Z
    .locals 7
    .param p1, "info"    # Lcom/android/launcher2/Launcher$WidgetAddInfo;

    .prologue
    const/4 v6, 0x0

    .line 985
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mSurfaceWidgetPackageManager:Lcom/android/launcher2/SurfaceWidgetPackageManager;

    iget-object v4, p1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v3, v4, v6}, Lcom/android/launcher2/SurfaceWidgetPackageManager;->findWidget(Landroid/content/ComponentName;Ljava/lang/String;)Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;

    move-result-object v2

    .line 986
    .local v2, "widgetInfo":Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;
    if-nez v2, :cond_0

    .line 987
    const/4 v3, 0x0

    .line 998
    :goto_0
    return v3

    .line 994
    :cond_0
    new-instance v1, Lcom/android/launcher2/HomePendingSurfaceWidget;

    invoke-direct {v1, v2}, Lcom/android/launcher2/HomePendingSurfaceWidget;-><init>(Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;)V

    .line 995
    .local v1, "w":Lcom/android/launcher2/HomePendingSurfaceWidget;
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    iget v4, p1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mSizeX:I

    iget v5, p1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mSizeY:I

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/launcher2/WorkspaceSpanCalculator;->getSpanForSurfaceWidget(II[I)[I

    move-result-object v0

    .line 996
    .local v0, "span":[I
    invoke-direct {p0, v1, p1, v0}, Lcom/android/launcher2/Launcher;->fillInHomeItem(Lcom/android/launcher2/HomeItem;Lcom/android/launcher2/Launcher$WidgetAddInfo;[I)V

    .line 997
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v3, v1}, Lcom/android/launcher2/HomeView;->addSurfaceWidgetFromDrop(Lcom/android/launcher2/HomePendingSurfaceWidget;)Z

    .line 998
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private processAsSamsungWidget(Lcom/android/launcher2/Launcher$WidgetAddInfo;)Z
    .locals 9
    .param p1, "info"    # Lcom/android/launcher2/Launcher$WidgetAddInfo;

    .prologue
    const/4 v5, 0x0

    .line 959
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mSamsungWidgetPackageManager:Lcom/android/launcher2/SamsungWidgetPackageManager;

    iget-object v1, p1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v0, v1, v5}, Lcom/android/launcher2/SamsungWidgetPackageManager;->findWidget(Landroid/content/ComponentName;Ljava/lang/String;)Lcom/android/launcher2/SamsungWidgetProviderInfo;

    move-result-object v8

    .line 960
    .local v8, "widgetInfo":Lcom/android/launcher2/SamsungWidgetProviderInfo;
    if-nez v8, :cond_0

    .line 961
    const/4 v0, 0x0

    .line 971
    :goto_0
    return v0

    .line 967
    :cond_0
    new-instance v7, Lcom/android/launcher2/HomePendingSamsungWidget;

    invoke-direct {v7, v8}, Lcom/android/launcher2/HomePendingSamsungWidget;-><init>(Lcom/android/launcher2/SamsungWidgetProviderInfo;)V

    .line 968
    .local v7, "w":Lcom/android/launcher2/HomePendingSamsungWidget;
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    iget v1, p1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mSizeX:I

    iget v2, p1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mSizeY:I

    iget v3, p1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mSizeX:I

    iget v4, p1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mSizeY:I

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/WorkspaceSpanCalculator;->getSpanForSamsungWidget(IIII[I)[I

    move-result-object v6

    .line 969
    .local v6, "span":[I
    invoke-direct {p0, v7, p1, v6}, Lcom/android/launcher2/Launcher;->fillInHomeItem(Lcom/android/launcher2/HomeItem;Lcom/android/launcher2/Launcher$WidgetAddInfo;[I)V

    .line 970
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0, v7}, Lcom/android/launcher2/HomeView;->addSamsungWidgetFromDrop(Lcom/android/launcher2/HomePendingSamsungWidget;)Z

    .line 971
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 3405
    const-string v6, ""

    .line 3406
    .local v6, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 3407
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 3409
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3410
    .end local v4    # "fr":Ljava/io/FileReader;
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v7, 0x1fa0

    invoke-direct {v1, v5, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3411
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v6

    .line 3420
    if-eqz v5, :cond_0

    .line 3421
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 3422
    :cond_0
    if-eqz v1, :cond_1

    .line 3423
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_1
    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 3429
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_2
    :goto_0
    if-nez v6, :cond_7

    .line 3430
    const-string v6, ""

    .line 3433
    :goto_1
    return-object v6

    .line 3424
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v2

    .line 3425
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "Launcher"

    const-string v8, "IOException close()"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3426
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 3428
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 3412
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 3413
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    const-string v7, "Launcher"

    const-string v8, "FileNotFoundException"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3414
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3420
    if-eqz v4, :cond_3

    .line 3421
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 3422
    :cond_3
    if-eqz v0, :cond_2

    .line 3423
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 3424
    :catch_2
    move-exception v2

    .line 3425
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "Launcher"

    const-string v8, "IOException close()"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3426
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 3415
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 3416
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    const-string v7, "Launcher"

    const-string v8, "IOException"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3417
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 3420
    if-eqz v4, :cond_4

    .line 3421
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 3422
    :cond_4
    if-eqz v0, :cond_2

    .line 3423
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 3424
    :catch_4
    move-exception v2

    .line 3425
    const-string v7, "Launcher"

    const-string v8, "IOException close()"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3426
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 3419
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 3420
    :goto_4
    if-eqz v4, :cond_5

    .line 3421
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 3422
    :cond_5
    if-eqz v0, :cond_6

    .line 3423
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 3427
    :cond_6
    :goto_5
    throw v7

    .line 3424
    :catch_5
    move-exception v2

    .line 3425
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "Launcher"

    const-string v9, "IOException close()"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3426
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 3432
    .end local v2    # "e":Ljava/io/IOException;
    :cond_7
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 3419
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 3415
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 3412
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_8
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_9
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method public static readSalesCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4720
    const-string v0, ""

    .line 4722
    .local v0, "sales_code":Ljava/lang/String;
    :try_start_0
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4723
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4724
    const-string v1, "ril.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 4729
    :cond_0
    :goto_0
    return-object v0

    .line 4726
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private releaseShadows()V
    .locals 2

    .prologue
    .line 3963
    const-string v0, "Launcher"

    const-string v1, "releaseShadows called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3964
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->releaseShadows()V

    .line 3965
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v0}, Lcom/android/launcher2/MenuView;->releaseShadows()V

    .line 3967
    sget-object v0, Lcom/android/launcher2/MenuAppModel;->INSTANCE:Lcom/android/launcher2/MenuAppModel;

    invoke-virtual {v0}, Lcom/android/launcher2/MenuAppModel;->releaseShadows()V

    .line 3968
    return-void
.end method

.method public static requestDownloadingResource(Ljava/lang/String;Landroid/content/Context;)V
    .locals 5
    .param p0, "type"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 4387
    move-object v0, p1

    .line 4388
    .local v0, "c":Landroid/content/Context;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    const/4 v3, 0x5

    invoke-direct {v2, p1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v3, 0x7f100059

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f1000e7

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x1040000

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    new-instance v4, Lcom/android/launcher2/Launcher$10;

    invoke-direct {v4, v0}, Lcom/android/launcher2/Launcher$10;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 4407
    .local v1, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-static {p1}, Lcom/android/launcher2/Launcher;->isWifiOnly(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4408
    const v2, 0x7f1000e8

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 4409
    :cond_0
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 4410
    return-void
.end method

.method static setHomeEditMode(Z)V
    .locals 0
    .param p0, "b"    # Z

    .prologue
    .line 4023
    sput-boolean p0, Lcom/android/launcher2/Launcher;->sIsHomeEditMode:Z

    .line 4024
    return-void
.end method

.method static setHomeRemoveMode(Z)V
    .locals 0
    .param p0, "b"    # Z

    .prologue
    .line 4033
    sput-boolean p0, Lcom/android/launcher2/Launcher;->sIsHomeRemoveMode:Z

    .line 4034
    return-void
.end method

.method private setIndicatorTransparency()V
    .locals 2

    .prologue
    .line 2158
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, -0x7c000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 2166
    return-void
.end method

.method private setMarketLabel()V
    .locals 6

    .prologue
    const v5, 0x7f100030

    .line 1256
    :try_start_0
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mActivityName:Landroid/content/ComponentName;

    if-eqz v2, :cond_0

    .line 1257
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mActivityName:Landroid/content/ComponentName;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v1

    .line 1258
    .local v1, "marketAppInfo":Landroid/content/pm/ActivityInfo;
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/android/launcher2/Launcher;->mMarketLabelName:Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1263
    .end local v1    # "marketAppInfo":Landroid/content/pm/ActivityInfo;
    :cond_0
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mMarketLabelName:Ljava/lang/CharSequence;

    if-nez v2, :cond_1

    .line 1264
    invoke-virtual {p0, v5}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/launcher2/Launcher;->mMarketLabelName:Ljava/lang/CharSequence;

    .line 1266
    :cond_1
    :goto_0
    return-void

    .line 1260
    :catch_0
    move-exception v0

    .line 1261
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1263
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mMarketLabelName:Ljava/lang/CharSequence;

    if-nez v2, :cond_1

    .line 1264
    invoke-virtual {p0, v5}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/launcher2/Launcher;->mMarketLabelName:Ljava/lang/CharSequence;

    goto :goto_0

    .line 1263
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mMarketLabelName:Ljava/lang/CharSequence;

    if-nez v3, :cond_2

    .line 1264
    invoke-virtual {p0, v5}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/launcher2/Launcher;->mMarketLabelName:Ljava/lang/CharSequence;

    :cond_2
    throw v2
.end method

.method private setupOptionsMenu(Landroid/view/Menu;)Z
    .locals 41
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 2863
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v38

    .line 2864
    .local v38, "workspace":Lcom/android/launcher2/Workspace;
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getAnimationLayer()Lcom/android/launcher2/AnimationLayer;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/AnimationLayer;->areTouchEventsBlocked()Z

    move-result v39

    if-nez v39, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/16 v39, 0x0

    .line 3087
    :goto_0
    return v39

    .line 2866
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v39

    const v40, 0x7f100034

    invoke-virtual/range {v39 .. v40}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 2868
    .local v22, "mDisableText":Ljava/lang/String;
    const v39, 0x7f0f00e1

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v23

    .line 2869
    .local v23, "market":Landroid/view/MenuItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->mMarketLabelName:Ljava/lang/CharSequence;

    move-object/from16 v39, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 2870
    const v39, 0x7f0f00e2

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    .line 2871
    .local v8, "edit":Landroid/view/MenuItem;
    const v39, 0x7f0f00ea

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v27

    .line 2872
    .local v27, "search":Landroid/view/MenuItem;
    const v39, 0x7f0f00ed

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v35

    .line 2873
    .local v35, "uninstall":Landroid/view/MenuItem;
    const v39, 0x7f0f00ec

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 2874
    .local v6, "downloaded":Landroid/view/MenuItem;
    const v39, 0x7f0f00eb

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v36

    .line 2875
    .local v36, "viewType":Landroid/view/MenuItem;
    const v39, 0x7f0f00ee

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v31

    .line 2876
    .local v31, "shareApp":Landroid/view/MenuItem;
    const v39, 0x7f0f00ef

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v28

    .line 2877
    .local v28, "selAppsToHide":Landroid/view/MenuItem;
    const v39, 0x7f0f00f1

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v29

    .line 2878
    .local v29, "selAppsToUnHide":Landroid/view/MenuItem;
    const v39, 0x7f0f00f2

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v24

    .line 2879
    .local v24, "moveToSecretBox":Landroid/view/MenuItem;
    const v39, 0x7f0f00f3

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v26

    .line 2880
    .local v26, "removeFromSecretBox":Landroid/view/MenuItem;
    const v39, 0x7f0f00e4

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 2881
    .local v5, "createFolder":Landroid/view/MenuItem;
    const v39, 0x7f0f00e5

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v25

    .line 2882
    .local v25, "removeFolder":Landroid/view/MenuItem;
    const v39, 0x7f0f00e3

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v12

    .line 2883
    .local v12, "homeAddWidget":Landroid/view/MenuItem;
    const v39, 0x7f0f00e6

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v11

    .line 2884
    .local v11, "homeAddWallpaper":Landroid/view/MenuItem;
    const v39, 0x7f0f00e9

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v16

    .line 2886
    .local v16, "homeSearchViaSFinder":Landroid/view/MenuItem;
    const v39, 0x7f0f00f5

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    .line 2887
    .local v15, "homeSearch":Landroid/view/MenuItem;
    const v39, 0x7f0f00e8

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v13

    .line 2888
    .local v13, "homeEdit":Landroid/view/MenuItem;
    const v39, 0x7f0f00f7

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v18

    .line 2889
    .local v18, "homeSettings":Landroid/view/MenuItem;
    const v39, 0x7f0f00f8

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v14

    .line 2890
    .local v14, "homeHelp":Landroid/view/MenuItem;
    const v39, 0x7f0f00f6

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v19

    .line 2891
    .local v19, "homeUsm":Landroid/view/MenuItem;
    const v39, 0x7f0f00f0

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v30

    .line 2892
    .local v30, "selappstoenable":Landroid/view/MenuItem;
    const v39, 0x7f0f00e7

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v17

    .line 2893
    .local v17, "homeSetTheme":Landroid/view/MenuItem;
    const v39, 0x7f0f00f9

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 2896
    .local v2, "GalaxyEssentials":Landroid/view/MenuItem;
    const v39, 0x7f0f00f4

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    .line 2897
    .local v10, "homeAddPage":Landroid/view/MenuItem;
    sget-object v39, Lcom/android/launcher2/MenuAppModel;->INSTANCE:Lcom/android/launcher2/MenuAppModel;

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/MenuAppModel;->canUninstallApps()Z

    move-result v7

    .line 2900
    .local v7, "downloadedApp":Z
    const/16 v39, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2901
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-interface {v8, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2902
    const/16 v39, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2903
    const/16 v39, 0x0

    move-object/from16 v0, v35

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2904
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2905
    const/16 v39, 0x0

    move-object/from16 v0, v36

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2906
    const/16 v39, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2907
    const/16 v39, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2908
    const/16 v39, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2909
    const/16 v39, 0x0

    move-object/from16 v0, v30

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2910
    const/16 v39, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2911
    const/16 v39, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2912
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2913
    const/16 v39, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2914
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-interface {v12, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2915
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-interface {v11, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2916
    const/16 v39, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2919
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-interface {v15, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2920
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-interface {v13, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2921
    const/16 v39, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2922
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-interface {v14, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2923
    const/16 v39, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2924
    const/16 v39, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2925
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2928
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-interface {v10, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2930
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v39

    if-eqz v39, :cond_d

    .line 2931
    const/16 v21, 0x0

    .line 2932
    .local v21, "isEnableFindoFeature":Z
    if-eqz v21, :cond_b

    .line 2938
    :cond_2
    :goto_1
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-interface {v13, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2940
    sget-boolean v39, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v39, :cond_3

    .line 2941
    invoke-virtual/range {v38 .. v38}, Lcom/android/launcher2/Workspace;->isInResizeMode()Z

    move-result v39

    if-nez v39, :cond_c

    .line 2942
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-interface {v13, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2943
    const v39, 0x7f100056

    move/from16 v0, v39

    invoke-interface {v13, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 2949
    :cond_3
    :goto_2
    sget-boolean v39, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v39, :cond_4

    .line 2950
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-interface {v10, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2951
    :cond_4
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/launcher2/Launcher;->addToHelpMenuOption(Landroid/view/MenuItem;)V

    .line 2952
    const/16 v39, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2953
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2954
    const-string v39, "KNIGHT"

    const-string v40, "KNIGHT"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_5

    .line 2955
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-interface {v12, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2956
    :cond_5
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-interface {v11, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2958
    sget-boolean v39, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_USM_EXISTS:Z

    if-eqz v39, :cond_6

    .line 2959
    const/16 v39, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2962
    :cond_6
    sget-boolean v39, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_THEME_ENABLE:Z

    if-eqz v39, :cond_7

    .line 2963
    const/16 v39, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3076
    .end local v21    # "isEnableFindoFeature":Z
    :cond_7
    :goto_3
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v39

    const-string v40, "CscFeature_Launcher_DisableGoogleOption"

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_8

    .line 3078
    const/16 v39, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3079
    const/16 v39, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3082
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->mApplicationPolicy:Landroid/app/enterprise/ApplicationPolicy;

    move-object/from16 v39, v0

    if-eqz v39, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->mApplicationPolicy:Landroid/app/enterprise/ApplicationPolicy;

    move-object/from16 v39, v0

    const-string v40, "com.android.vending"

    invoke-virtual/range {v39 .. v40}, Landroid/app/enterprise/ApplicationPolicy;->getApplicationStateEnabled(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->mApplicationPolicy:Landroid/app/enterprise/ApplicationPolicy;

    move-object/from16 v39, v0

    const-string v40, "com.google.android.finsky"

    invoke-virtual/range {v39 .. v40}, Landroid/app/enterprise/ApplicationPolicy;->getApplicationStateEnabled(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_a

    .line 3084
    :cond_9
    const/16 v39, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3087
    :cond_a
    const/16 v39, 0x1

    goto/16 :goto_0

    .line 2935
    .restart local v21    # "isEnableFindoFeature":Z
    :cond_b
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/LauncherApplication;->isKnoxMode()Z

    move-result v39

    if-nez v39, :cond_2

    .line 2936
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-interface {v15, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    .line 2945
    :cond_c
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-interface {v13, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 2966
    .end local v21    # "isEnableFindoFeature":Z
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v39

    if-eqz v39, :cond_7

    .line 2967
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/MenuView;->isCurrentTabWidgets()Z

    move-result v39

    if-eqz v39, :cond_10

    .line 2968
    sget-boolean v39, Lcom/android/launcher2/Launcher;->isSystemAppDisable:Z

    if-nez v39, :cond_e

    .line 2969
    const/16 v39, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2970
    :cond_e
    const/16 v39, 0x1

    move-object/from16 v0, v27

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2972
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/MenuView;->getMenuWidgets()Lcom/android/launcher2/MenuWidgets;

    move-result-object v37

    .line 2973
    .local v37, "widgets":Lcom/android/launcher2/MenuWidgets;
    if-eqz v37, :cond_f

    invoke-virtual/range {v37 .. v37}, Lcom/android/launcher2/MenuWidgets;->hasUninstallableWidgets()Z

    move-result v39

    if-eqz v39, :cond_f

    const/16 v39, 0x1

    :goto_4
    move-object/from16 v0, v35

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_3

    :cond_f
    const/16 v39, 0x0

    goto :goto_4

    .line 2978
    .end local v37    # "widgets":Lcom/android/launcher2/MenuWidgets;
    :cond_10
    sget-object v39, Lcom/android/launcher2/Launcher$14;->$SwitchMap$com$android$launcher2$MenuView$ViewType:[I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/android/launcher2/MenuView;->getViewType()Lcom/android/launcher2/MenuView$ViewType;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Lcom/android/launcher2/MenuView$ViewType;->ordinal()I

    move-result v40

    aget v39, v39, v40

    packed-switch v39, :pswitch_data_0

    goto/16 :goto_3

    .line 2981
    :pswitch_0
    sget-boolean v39, Lcom/android/launcher2/Launcher;->isSystemAppDisable:Z

    if-nez v39, :cond_11

    .line 2983
    const/16 v39, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2984
    move-object/from16 v0, v31

    invoke-interface {v0, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2986
    :cond_11
    move-object/from16 v0, v35

    invoke-interface {v0, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2988
    sget-object v39, Lcom/android/launcher2/Launcher$14;->$SwitchMap$com$android$launcher2$MenuAppsGrid$State:[I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/android/launcher2/MenuView;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Lcom/android/launcher2/MenuAppsGrid$State;->ordinal()I

    move-result v40

    aget v39, v39, v40

    packed-switch v39, :pswitch_data_1

    goto/16 :goto_3

    .line 2991
    :pswitch_1
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isOwner()Z

    move-result v39

    if-eqz v39, :cond_12

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/LauncherApplication;->isKnoxMode()Z

    move-result v39

    if-nez v39, :cond_12

    const-string v39, "KNIGHT"

    const-string v40, "KNIGHT"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_12

    .line 2992
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2993
    :cond_12
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-interface {v8, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2994
    sget-object v39, Lcom/android/launcher2/MenuAppModel;->INSTANCE:Lcom/android/launcher2/MenuAppModel;

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/MenuAppModel;->getTopLevelItems()Ljava/util/List;

    move-result-object v3

    .line 2995
    .local v3, "_tmp":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/AppItem;>;"
    const/16 v33, 0x0

    .line 2996
    .local v33, "topLevelListHasAppItem":Z
    const/4 v9, 0x0

    .line 2997
    .local v9, "hasFolder":Z
    if-eqz v3, :cond_13

    .line 2998
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .local v20, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v39

    if-eqz v39, :cond_13

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/AppItem;

    .line 2999
    .local v4, "app":Lcom/android/launcher2/AppItem;
    if-eqz v33, :cond_16

    if-eqz v9, :cond_16

    .line 3012
    .end local v4    # "app":Lcom/android/launcher2/AppItem;
    .end local v20    # "i$":Ljava/util/Iterator;
    :cond_13
    if-eqz v3, :cond_19

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v39

    if-nez v39, :cond_19

    const/16 v34, 0x1

    .line 3013
    .local v34, "topLevelListNotEmpty":Z
    :goto_6
    move/from16 v0, v34

    invoke-interface {v8, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3014
    if-eqz v34, :cond_1a

    if-eqz v33, :cond_1a

    const/16 v34, 0x1

    .line 3015
    :goto_7
    move/from16 v0, v34

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3016
    move-object/from16 v0, v36

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3017
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/launcher2/Launcher;->addToHelpMenuOption(Landroid/view/MenuItem;)V

    .line 3021
    if-eqz v7, :cond_1b

    if-eqz v34, :cond_1b

    const/16 v39, 0x1

    :goto_8
    move/from16 v0, v39

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3022
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/LauncherApplication;->isKnoxMode()Z

    move-result v39

    if-eqz v39, :cond_14

    .line 3023
    sget-object v39, Lcom/android/launcher2/MenuAppModel;->INSTANCE:Lcom/android/launcher2/MenuAppModel;

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/MenuAppModel;->getUninstallableApps()Ljava/util/List;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Ljava/util/List;->size()I

    move-result v39

    if-nez v39, :cond_14

    .line 3024
    const/16 v39, 0x0

    move/from16 v0, v39

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3027
    :cond_14
    const/16 v39, 0x1

    move-object/from16 v0, v36

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3028
    const/16 v39, 0x1

    move-object/from16 v0, v28

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3029
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3030
    sget-object v39, Lcom/android/launcher2/MenuAppModel;->INSTANCE:Lcom/android/launcher2/MenuAppModel;

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/MenuAppModel;->getTopLevelItems()Ljava/util/List;

    move-result-object v32

    .line 3031
    .local v32, "topApps":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/AppItem;>;"
    if-eqz v32, :cond_1c

    .line 3032
    move-object/from16 v0, v28

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3036
    :goto_9
    sget-object v39, Lcom/android/launcher2/MenuAppModel;->INSTANCE:Lcom/android/launcher2/MenuAppModel;

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/MenuAppModel;->hasHiddenApps()Z

    move-result v39

    move-object/from16 v0, v29

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3038
    sget-boolean v39, Lcom/android/launcher2/Launcher;->isSystemAppDisable:Z

    if-eqz v39, :cond_15

    .line 3039
    move-object/from16 v0, v35

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 3043
    move-object/from16 v0, v35

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3044
    sget-object v39, Lcom/android/launcher2/MenuAppModel;->INSTANCE:Lcom/android/launcher2/MenuAppModel;

    invoke-virtual/range {v39 .. v39}, Lcom/android/launcher2/MenuAppModel;->isDisableAppListEmpty()Z

    move-result v39

    if-nez v39, :cond_1d

    const/16 v39, 0x1

    :goto_a
    move-object/from16 v0, v30

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3063
    :cond_15
    move-object/from16 v0, v25

    invoke-interface {v0, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_3

    .line 3002
    .end local v32    # "topApps":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/AppItem;>;"
    .end local v34    # "topLevelListNotEmpty":Z
    .restart local v4    # "app":Lcom/android/launcher2/AppItem;
    .restart local v20    # "i$":Ljava/util/Iterator;
    :cond_16
    invoke-static {v4}, Lcom/android/launcher2/AppItem;->isFolder(Lcom/android/launcher2/BaseItem;)Z

    move-result v39

    if-nez v39, :cond_17

    .line 3003
    const/16 v33, 0x1

    goto/16 :goto_5

    .line 3005
    :cond_17
    check-cast v4, Lcom/android/launcher2/FolderItem;

    .end local v4    # "app":Lcom/android/launcher2/AppItem;
    invoke-interface {v4}, Lcom/android/launcher2/FolderItem;->getItemCount()I

    move-result v39

    if-lez v39, :cond_18

    .line 3006
    const/16 v33, 0x1

    .line 3008
    :cond_18
    const/4 v9, 0x1

    goto/16 :goto_5

    .line 3012
    .end local v20    # "i$":Ljava/util/Iterator;
    :cond_19
    const/16 v34, 0x0

    goto/16 :goto_6

    .line 3014
    .restart local v34    # "topLevelListNotEmpty":Z
    :cond_1a
    const/16 v34, 0x0

    goto/16 :goto_7

    .line 3021
    :cond_1b
    const/16 v39, 0x0

    goto/16 :goto_8

    .line 3034
    .restart local v32    # "topApps":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/AppItem;>;"
    :cond_1c
    const/16 v39, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_9

    .line 3044
    :cond_1d
    const/16 v39, 0x0

    goto :goto_a

    .line 3069
    .end local v3    # "_tmp":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/AppItem;>;"
    .end local v9    # "hasFolder":Z
    .end local v32    # "topApps":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/AppItem;>;"
    .end local v33    # "topLevelListHasAppItem":Z
    .end local v34    # "topLevelListNotEmpty":Z
    :pswitch_2
    const/16 v39, 0x1

    move-object/from16 v0, v36

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3070
    const/16 v39, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v39

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_3

    .line 2978
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 2988
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method public static startDrag(Landroid/view/View;)Z
    .locals 1
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 3734
    new-instance v0, Lcom/android/launcher2/ShadowBuilder;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ShadowBuilder;-><init>(Landroid/view/View;)V

    invoke-static {p0, v0}, Lcom/android/launcher2/Launcher;->startDrag(Landroid/view/View;Lcom/android/launcher2/ShadowBuilder;)Z

    move-result v0

    return v0
.end method

.method public static startDrag(Landroid/view/View;F)Z
    .locals 1
    .param p0, "v"    # Landroid/view/View;
    .param p1, "shrinkfactor"    # F

    .prologue
    .line 3740
    new-instance v0, Lcom/android/launcher2/ShadowBuilder;

    invoke-direct {v0, p0, p1, p1}, Lcom/android/launcher2/ShadowBuilder;-><init>(Landroid/view/View;FF)V

    invoke-static {p0, v0}, Lcom/android/launcher2/Launcher;->startDrag(Landroid/view/View;Lcom/android/launcher2/ShadowBuilder;)Z

    move-result v0

    return v0
.end method

.method public static startDrag(Landroid/view/View;FF)Z
    .locals 1
    .param p0, "v"    # Landroid/view/View;
    .param p1, "shrinkfactor"    # F
    .param p2, "pointfactor"    # F

    .prologue
    .line 3737
    new-instance v0, Lcom/android/launcher2/ShadowBuilder;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/launcher2/ShadowBuilder;-><init>(Landroid/view/View;FF)V

    invoke-static {p0, v0}, Lcom/android/launcher2/Launcher;->startDrag(Landroid/view/View;Lcom/android/launcher2/ShadowBuilder;)Z

    move-result v0

    return v0
.end method

.method public static startDrag(Landroid/view/View;Lcom/android/launcher2/ShadowBuilder;)Z
    .locals 7
    .param p0, "v"    # Landroid/view/View;
    .param p1, "shadow"    # Lcom/android/launcher2/ShadowBuilder;

    .prologue
    const/4 v4, 0x0

    .line 3746
    new-instance v0, Lcom/android/launcher2/DragState;

    invoke-direct {v0, p0}, Lcom/android/launcher2/DragState;-><init>(Landroid/view/View;)V

    .line 3747
    .local v0, "myLocalState":Lcom/android/launcher2/DragState;
    const/4 v1, 0x0

    .line 3749
    .local v1, "okay":Z
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 3750
    .local v2, "shadowSize":Landroid/graphics/Point;
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 3751
    .local v3, "shadowTouchPoint":Landroid/graphics/Point;
    invoke-virtual {p1, v2, v3}, Lcom/android/launcher2/ShadowBuilder;->onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 3755
    iget v5, v3, Landroid/graphics/Point;->x:I

    if-ltz v5, :cond_0

    iget v5, v3, Landroid/graphics/Point;->y:I

    if-gez v5, :cond_1

    .line 3756
    :cond_0
    const-string v5, "Launcher"

    const-string v6, "Drag shadow touch point must not be negative"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3804
    :goto_0
    return v4

    .line 3760
    :cond_1
    iget v5, v2, Landroid/graphics/Point;->x:I

    if-ltz v5, :cond_2

    iget v5, v2, Landroid/graphics/Point;->y:I

    if-gez v5, :cond_3

    .line 3761
    :cond_2
    const-string v5, "Launcher"

    const-string v6, "Drag shadow dimensions must not be negative"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3792
    :cond_3
    const/4 v5, 0x0

    invoke-virtual {p0, v5, p1, v0, v4}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    move-result v1

    .line 3794
    if-eqz v1, :cond_5

    .line 3795
    invoke-virtual {v0, p1}, Lcom/android/launcher2/DragState;->setShadow(Lcom/android/launcher2/ShadowBuilder;)V

    .line 3796
    invoke-virtual {v0}, Lcom/android/launcher2/DragState;->onDragStartedSuccessfully()V

    .line 3803
    :cond_4
    :goto_1
    const-string v4, "Launcher"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Start Drag result "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v1

    .line 3804
    goto :goto_0

    .line 3798
    :cond_5
    invoke-virtual {v0}, Lcom/android/launcher2/DragState;->getView()Landroid/view/View;

    move-result-object v5

    instance-of v5, v5, Lcom/android/launcher2/SurfaceWidgetView;

    if-eqz v5, :cond_4

    .line 3799
    invoke-virtual {v0}, Lcom/android/launcher2/DragState;->getView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private tryQueueWidgetAddViaIntent(Landroid/content/Intent;Ljava/lang/String;)Z
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "overwriteAction"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, -0x1

    .line 2277
    if-eqz p1, :cond_4

    .line 2278
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2283
    .local v0, "action":Ljava/lang/String;
    const-string v7, "com.sec.launcher.action.INSTALL_WIDGET"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2284
    if-eqz p2, :cond_0

    .line 2285
    invoke-virtual {p1, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2287
    :cond_0
    const-string v7, "com.sec.launcher.intent.extra.COMPONENT"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 2288
    .local v2, "compName":Landroid/content/ComponentName;
    if-eqz v2, :cond_2

    .line 2292
    new-instance v1, Lcom/android/launcher2/Launcher$WidgetAddInfo;

    const/4 v7, 0x0

    invoke-direct {v1, v7}, Lcom/android/launcher2/Launcher$WidgetAddInfo;-><init>(Lcom/android/launcher2/Launcher$1;)V

    .line 2293
    .local v1, "addInfo":Lcom/android/launcher2/Launcher$WidgetAddInfo;
    iput-object v2, v1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mComponentName:Landroid/content/ComponentName;

    .line 2294
    const-string v7, "com.sec.launcher.intent.extra.DUPLICATE"

    invoke-virtual {p1, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, v1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mAllowDuplicate:Z

    .line 2296
    const-string v7, "com.sec.launcher.intent.extra.sizeX"

    invoke-virtual {p1, v7, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 2297
    .local v3, "sizeX":I
    if-ge v3, v6, :cond_1

    move v3, v5

    .end local v3    # "sizeX":I
    :cond_1
    iput v3, v1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mSizeX:I

    .line 2298
    const-string v7, "com.sec.launcher.intent.extra.sizeY"

    invoke-virtual {p1, v7, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 2299
    .local v4, "sizeY":I
    if-ge v4, v6, :cond_3

    :goto_0
    iput v5, v1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mSizeY:I

    .line 2300
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mWidgetAddInfo:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .end local v1    # "addInfo":Lcom/android/launcher2/Launcher$WidgetAddInfo;
    .end local v4    # "sizeY":I
    :cond_2
    move v5, v6

    .line 2305
    .end local v0    # "action":Ljava/lang/String;
    .end local v2    # "compName":Landroid/content/ComponentName;
    :goto_1
    return v5

    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "addInfo":Lcom/android/launcher2/Launcher$WidgetAddInfo;
    .restart local v2    # "compName":Landroid/content/ComponentName;
    .restart local v4    # "sizeY":I
    :cond_3
    move v5, v4

    .line 2299
    goto :goto_0

    .line 2305
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "addInfo":Lcom/android/launcher2/Launcher$WidgetAddInfo;
    .end local v2    # "compName":Landroid/content/ComponentName;
    .end local v4    # "sizeY":I
    :cond_4
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method public RestoreHomeScreen()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, -0x1

    .line 4414
    sput-boolean v9, Lcom/android/launcher2/Launcher;->isHomescreenRestoring:Z

    .line 4415
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    .line 4417
    .local v0, "app":Lcom/android/launcher2/LauncherApplication;
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v6}, Lcom/android/launcher2/HomeView;->getPageCount()I

    move-result v3

    .line 4419
    .local v3, "oldScreenCount":I
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getScreenCount()I

    move-result v4

    .line 4420
    .local v4, "screenCount":I
    if-ne v4, v8, :cond_0

    .line 4421
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getMaxScreenCount()I

    move-result v4

    .line 4423
    :cond_0
    sub-int v2, v4, v3

    .line 4424
    .local v2, "difference":I
    if-lez v2, :cond_1

    .line 4425
    const/4 v1, 0x0

    .local v1, "count":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 4426
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v6}, Lcom/android/launcher2/HomeView;->addPage()V

    .line 4425
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4428
    .end local v1    # "count":I
    :cond_1
    if-gez v2, :cond_2

    .line 4429
    const/4 v1, 0x0

    .restart local v1    # "count":I
    :goto_1
    mul-int/lit8 v6, v2, -0x1

    if-ge v1, v6, :cond_2

    .line 4430
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/android/launcher2/HomeView;->deletePage(I)V

    .line 4429
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4434
    .end local v1    # "count":I
    :cond_2
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getHomeScreenIndex()I

    move-result v5

    .line 4435
    .local v5, "screenIndex":I
    if-eq v5, v8, :cond_3

    if-ge v5, v4, :cond_3

    .line 4436
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v6, v5}, Lcom/android/launcher2/HomeView;->setHomeScreenAt(I)V

    .line 4438
    :cond_3
    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->getModel()Lcom/android/launcher2/LauncherModel;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/android/launcher2/LauncherModel;->setRefreshRequired(Z)V

    .line 4439
    new-instance v6, Landroid/os/Handler;

    invoke-direct {v6}, Landroid/os/Handler;-><init>()V

    new-instance v7, Lcom/android/launcher2/Launcher$11;

    invoke-direct {v7, p0, v0}, Lcom/android/launcher2/Launcher$11;-><init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/LauncherApplication;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 4445
    return-void
.end method

.method public addAnyPendingWidgetsToWorkspace()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 1045
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1046
    .local v2, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/content/ComponentName;Ljava/lang/Integer;>;"
    invoke-direct {p0, v2}, Lcom/android/launcher2/Launcher;->buildWidgetMap(Ljava/util/HashMap;)V

    .line 1047
    iget-object v7, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v7}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v6

    .line 1048
    .local v6, "ws":Lcom/android/launcher2/Workspace;
    invoke-virtual {v6}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v0

    .line 1050
    .local v0, "currentScreen":I
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/android/launcher2/Launcher;->mWidgetAddInfo:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_4

    iget-object v7, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v7}, Lcom/android/launcher2/HomeView;->isWorkspaceLocked()Z

    move-result v7

    if-nez v7, :cond_4

    .line 1051
    iget-object v7, p0, Lcom/android/launcher2/Launcher;->mWidgetAddInfo:Ljava/util/ArrayList;

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/Launcher$WidgetAddInfo;

    .line 1052
    .local v1, "info":Lcom/android/launcher2/Launcher$WidgetAddInfo;
    iget-boolean v7, v1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mAllowDuplicate:Z

    if-nez v7, :cond_1

    .line 1053
    iget-object v7, v1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v2, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 1054
    .local v5, "screen":Ljava/lang/Integer;
    if-eqz v5, :cond_1

    .line 1055
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/android/launcher2/Workspace;->snapToPage(I)V

    .line 1056
    const v7, 0x7f10001f

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, v1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v9}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {p0, v7, v8}, Lcom/android/launcher2/Launcher;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1057
    .local v3, "msg":Ljava/lang/String;
    invoke-static {p0, v3, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1061
    .end local v3    # "msg":Ljava/lang/String;
    .end local v5    # "screen":Ljava/lang/Integer;
    :cond_1
    invoke-direct {p0, v1}, Lcom/android/launcher2/Launcher;->processAsSamsungWidget(Lcom/android/launcher2/Launcher$WidgetAddInfo;)Z

    move-result v4

    .line 1062
    .local v4, "processed":Z
    if-nez v4, :cond_2

    .line 1063
    invoke-direct {p0, v1}, Lcom/android/launcher2/Launcher;->processAsSamsungSurfaceWidget(Lcom/android/launcher2/Launcher$WidgetAddInfo;)Z

    move-result v4

    .line 1065
    :cond_2
    if-nez v4, :cond_3

    .line 1066
    invoke-direct {p0, v1}, Lcom/android/launcher2/Launcher;->processAsAndroidAppWidget(Lcom/android/launcher2/Launcher$WidgetAddInfo;)Z

    move-result v4

    .line 1068
    :cond_3
    if-eqz v4, :cond_0

    iget-object v7, v1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v2, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1075
    iget-object v7, v1, Lcom/android/launcher2/Launcher$WidgetAddInfo;->mComponentName:Landroid/content/ComponentName;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1078
    .end local v1    # "info":Lcom/android/launcher2/Launcher$WidgetAddInfo;
    .end local v4    # "processed":Z
    :cond_4
    return-void
.end method

.method public addStateAnimatorProvider(Lcom/android/launcher2/Launcher$StateAnimatorProvider;)V
    .locals 1
    .param p1, "provider"    # Lcom/android/launcher2/Launcher$StateAnimatorProvider;

    .prologue
    .line 1244
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mStateAnimatorProviders:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1245
    return-void
.end method

.method public bindAllAppsSecretMode()V
    .locals 1

    .prologue
    .line 3665
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v0}, Lcom/android/launcher2/MenuView;->bindMenuSecretMode()V

    .line 3666
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->closePopupMenu()V

    .line 3667
    return-void
.end method

.method public bindAppsLoaded()V
    .locals 2

    .prologue
    .line 3570
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/launcher2/MenuView;->mMenuAppLoaded:Z

    .line 3571
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v0}, Lcom/android/launcher2/MenuView;->appModelLoaded()V

    .line 3572
    return-void
.end method

.method public bindBadgeUpdated(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/launcher2/AppItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/android/launcher2/HomeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3575
    .local p1, "appItems":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/AppItem;>;"
    .local p2, "homeItems":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/HomeItem;>;"
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3576
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Lcom/android/launcher2/HomeView;->bindHomeItemsUpdated(Ljava/util/List;Z)V

    .line 3578
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3579
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/MenuView;->appBadgeUpdated(Ljava/util/List;)V

    .line 3581
    :cond_1
    return-void
.end method

.method public bindFestivalModeChange(Z)V
    .locals 3
    .param p1, "isFestivalMode"    # Z

    .prologue
    .line 3672
    sput-boolean p1, Lcom/android/launcher2/Launcher;->sIsFestivalModeOn:Z

    .line 3674
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3675
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/launcher2/HomeView;->closeQuickViewWorkspace(Z)V

    .line 3683
    :cond_0
    :goto_0
    return-void

    .line 3676
    :cond_1
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3677
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->getMenuAppsGrid()Lcom/android/launcher2/MenuAppsGrid;

    move-result-object v0

    .line 3678
    .local v0, "menuGrid":Lcom/android/launcher2/MenuAppsGrid;
    invoke-virtual {v0}, Lcom/android/launcher2/MenuAppsGrid;->getAppsQuickViewState()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3679
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->onBackPressed()Z

    goto :goto_0

    .line 3680
    :cond_2
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3681
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getPageIndicatorManager()Lcom/android/launcher2/PageIndicatorManager;

    move-result-object v1

    sget-object v2, Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;->NORMAL_PAGE:Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/PageIndicatorManager;->setDisplayItem(Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;)V

    goto :goto_0
.end method

.method public bindHomeAppWidget(Lcom/android/launcher2/HomeWidgetItem;)V
    .locals 1
    .param p1, "info"    # Lcom/android/launcher2/HomeWidgetItem;

    .prologue
    .line 3546
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->setLoadOnResume()Z

    .line 3547
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/HomeView;->bindAppWidget(Lcom/android/launcher2/HomeWidgetItem;)V

    .line 3548
    return-void
.end method

.method public bindHomeBegin()V
    .locals 1

    .prologue
    .line 3518
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->startBinding()V

    .line 3519
    return-void
.end method

.method public bindHomeDeleteFestivalPage(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/launcher2/HomeWidgetItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3697
    .local p1, "widgets":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/HomeWidgetItem;>;"
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->closeFolder()V

    .line 3698
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->exitWidgetResizeMode()V

    .line 3699
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 3700
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/HomeView;->deleteWidgetFestivalPage(Ljava/util/List;)V

    .line 3701
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->bindHomeDeleteFestivalPage()V

    .line 3702
    return-void
.end method

.method public bindHomeDeleteSecretPage()V
    .locals 2

    .prologue
    .line 3654
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mSecretPageManager:Lcom/android/launcher2/SecretPageManager;

    invoke-virtual {v0}, Lcom/android/launcher2/SecretPageManager;->getSecretPageCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 3661
    :goto_0
    return-void

    .line 3657
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->closeFolder()V

    .line 3658
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->exitWidgetResizeMode()V

    .line 3659
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->bindHomeDeleteSecretPage()V

    .line 3660
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/HomeView;->refreshQuickViewWorkspace(Z)V

    goto :goto_0
.end method

.method public bindHomeEnd()V
    .locals 3

    .prologue
    .line 3532
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->setLoadOnResume()Z

    .line 3533
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v2}, Lcom/android/launcher2/HomeView;->finishBindingItems()V

    .line 3535
    sget-boolean v2, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v2, :cond_0

    .line 3536
    sget-object v1, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    .line 3537
    .local v1, "oldGMode":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->exitHelp(Z)V

    .line 3538
    const/4 v2, 0x1

    sput-boolean v2, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    .line 3539
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 3540
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "homescreen:guide_mode"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3541
    invoke-static {p0, v0}, Lcom/android/launcher2/guide/GuideFragment;->deployGuide(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 3543
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "oldGMode":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public bindHomeFolders(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/launcher2/HomeFolderItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3527
    .local p1, "folders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/android/launcher2/HomeFolderItem;>;"
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->setLoadOnResume()Z

    .line 3528
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/HomeView;->bindFolders(Ljava/util/Map;)V

    .line 3529
    return-void
.end method

.method public bindHomeInsertFestivalPage()V
    .locals 1

    .prologue
    .line 3687
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->closeFolder()V

    .line 3688
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->exitWidgetResizeMode()V

    .line 3689
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mFestivalPageManager:Lcom/android/launcher2/FestivalPageManager;

    invoke-virtual {v0}, Lcom/android/launcher2/FestivalPageManager;->getFestivalPageCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 3693
    :goto_0
    return-void

    .line 3692
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->bindHomeInsertFestivalPage()V

    goto :goto_0
.end method

.method public bindHomeInsertSecretPage()V
    .locals 2

    .prologue
    .line 3643
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->closeFolder()V

    .line 3644
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->exitWidgetResizeMode()V

    .line 3645
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mSecretPageManager:Lcom/android/launcher2/SecretPageManager;

    invoke-virtual {v0}, Lcom/android/launcher2/SecretPageManager;->getSecretPageCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 3650
    :goto_0
    return-void

    .line 3648
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->bindHomeInsertSecretPage()V

    .line 3649
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/HomeView;->refreshQuickViewWorkspace(Z)V

    goto :goto_0
.end method

.method public bindHomeItemsRemoved(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/launcher2/HomeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3598
    .local p1, "removed":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/HomeItem;>;"
    invoke-static {}, Lcom/android/launcher2/Utilities;->DEBUGGABLE()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 3599
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 3600
    .local v6, "now":J
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 3601
    .local v1, "date":Ljava/util/Date;
    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string v10, "yyyy/MM/dd HH:mm:ss"

    invoke-direct {v8, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 3602
    .local v8, "sdfNow":Ljava/text/SimpleDateFormat;
    invoke-virtual {v8, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    .line 3603
    .local v9, "strNow":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    .line 3604
    .local v0, "app":Lcom/android/launcher2/LauncherApplication;
    const-string v10, "com.sec.android.app.launcher.prefs"

    const/4 v11, 0x0

    invoke-virtual {v0, v10, v11}, Lcom/android/launcher2/LauncherApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 3605
    .local v5, "prefs":Landroid/content/SharedPreferences$Editor;
    const/4 v2, 0x0

    .line 3606
    .local v2, "i":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/HomeItem;

    .line 3607
    .local v4, "item":Lcom/android/launcher2/HomeItem;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "bindHomeItemsRemoved"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Lcom/android/launcher2/HomeItem;->getPackageName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", time : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v5, v10, v11}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 3608
    add-int/lit8 v2, v2, 0x1

    .line 3609
    goto :goto_0

    .line 3610
    .end local v4    # "item":Lcom/android/launcher2/HomeItem;
    :cond_0
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 3612
    .end local v0    # "app":Lcom/android/launcher2/LauncherApplication;
    .end local v1    # "date":Ljava/util/Date;
    .end local v2    # "i":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "prefs":Landroid/content/SharedPreferences$Editor;
    .end local v6    # "now":J
    .end local v8    # "sdfNow":Ljava/text/SimpleDateFormat;
    .end local v9    # "strNow":Ljava/lang/String;
    :cond_1
    iget-object v10, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v10, p1}, Lcom/android/launcher2/HomeView;->bindHomeItemsRemoved(Ljava/util/List;)V

    .line 3613
    return-void
.end method

.method public bindHomeItemsUpdated(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/launcher2/HomeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3616
    .local p1, "updated":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/HomeItem;>;"
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/android/launcher2/HomeView;->bindHomeItemsUpdated(Ljava/util/List;Z)V

    .line 3617
    return-void
.end method

.method public bindHomeSamsungWidget(Lcom/android/launcher2/SamsungWidgetItem;)V
    .locals 1
    .param p1, "item"    # Lcom/android/launcher2/SamsungWidgetItem;

    .prologue
    .line 3551
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->setLoadOnResume()Z

    .line 3552
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/HomeView;->bindSamsungWidget(Lcom/android/launcher2/SamsungWidgetItem;)V

    .line 3553
    return-void
.end method

.method public bindHomeShortcuts(Ljava/util/List;II)V
    .locals 1
    .param p2, "start"    # I
    .param p3, "end"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/launcher2/HomeItem;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 3522
    .local p1, "shortcuts":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/HomeItem;>;"
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->setLoadOnResume()Z

    .line 3523
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/launcher2/HomeView;->bindItems(Ljava/util/List;II)V

    .line 3524
    return-void
.end method

.method public bindHomeSurfaceWidget(Lcom/android/launcher2/SurfaceWidgetItem;)V
    .locals 1
    .param p1, "item"    # Lcom/android/launcher2/SurfaceWidgetItem;

    .prologue
    .line 3564
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->setLoadOnResume()Z

    .line 3565
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/HomeView;->bindSurfaceWidget(Lcom/android/launcher2/SurfaceWidgetItem;)V

    .line 3566
    return-void
.end method

.method public bindHotseatItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/launcher2/HomeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3556
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/HomeItem;>;"
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->setLoadOnResume()Z

    .line 3557
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/launcher2/HomeView;->sIsBindHotseat:Z

    .line 3558
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/HomeView;->bindHotseat(Ljava/util/List;)V

    .line 3559
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher2/HomeView;->sIsBindHotseat:Z

    .line 3560
    return-void
.end method

.method public bindModeChange()V
    .locals 1

    .prologue
    .line 3513
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->mAutoRestart:Z

    .line 3514
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->finish()V

    .line 3515
    return-void
.end method

.method public bindPackagesChanged(Z)V
    .locals 1
    .param p1, "appModelUpdated"    # Z

    .prologue
    .line 3584
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mSamsungWidgetPackageManager:Lcom/android/launcher2/SamsungWidgetPackageManager;

    invoke-virtual {v0}, Lcom/android/launcher2/SamsungWidgetPackageManager;->forceReload()V

    .line 3585
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/MenuView;->packagesChanged(Z)V

    .line 3595
    return-void
.end method

.method public bindSearchablesChanged()V
    .locals 1

    .prologue
    .line 3916
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->updateGlobalSearchIcon()Z

    .line 3917
    return-void
.end method

.method public bindSecretModeChange(Z)V
    .locals 3
    .param p1, "isSecretMode"    # Z

    .prologue
    .line 3621
    sput-boolean p1, Lcom/android/launcher2/Launcher;->sIsSecretModeOn:Z

    .line 3623
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3624
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/launcher2/HomeView;->closeQuickViewWorkspace(Z)V

    .line 3634
    :cond_0
    :goto_0
    sget-boolean v1, Lcom/android/launcher2/Launcher;->sIsSecretModeOn:Z

    sput-boolean v1, Lcom/android/launcher2/MenuAppsGrid;->mChangeMenuModeForSecretBox:Z

    .line 3636
    return-void

    .line 3625
    :cond_1
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3626
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->getMenuAppsGrid()Lcom/android/launcher2/MenuAppsGrid;

    move-result-object v0

    .line 3627
    .local v0, "menuGrid":Lcom/android/launcher2/MenuAppsGrid;
    invoke-virtual {v0}, Lcom/android/launcher2/MenuAppsGrid;->getAppsQuickViewState()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3628
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->onBackPressed()Z

    goto :goto_0

    .line 3629
    :cond_2
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3630
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getPageIndicatorManager()Lcom/android/launcher2/PageIndicatorManager;

    move-result-object v2

    if-eqz p1, :cond_3

    sget-object v1, Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;->SECRET_PAGE:Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;

    :goto_1
    invoke-virtual {v2, v1}, Lcom/android/launcher2/PageIndicatorManager;->setDisplayItem(Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;)V

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;->NORMAL_PAGE:Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;

    goto :goto_1
.end method

.method public bindWidgetsAfterConfigChange()V
    .locals 1

    .prologue
    .line 3506
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mIsDestroyed:Z

    if-nez v0, :cond_0

    .line 3507
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->bindWidgetsAfterConfigChange()V

    .line 3509
    :cond_0
    return-void
.end method

.method public callDisableDialog(Lcom/android/launcher2/BaseItem;)V
    .locals 6
    .param p1, "item"    # Lcom/android/launcher2/BaseItem;

    .prologue
    const/4 v5, 0x0

    .line 4199
    const/4 v0, 0x0

    .line 4201
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/launcher2/BaseItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 4207
    :goto_0
    if-eqz v0, :cond_1

    iget v2, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v2, v2, 0x80

    if-nez v2, :cond_1

    .line 4208
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->shouldDisablePopupRepeat()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4209
    invoke-virtual {p1}, Lcom/android/launcher2/BaseItem;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/android/launcher2/BaseItem;->mTitle:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-static {v2, v3, v4, v5}, Lcom/android/launcher2/DisableAppConfirmationDialog;->createAndShow(Ljava/lang/String;Ljava/lang/String;Landroid/app/FragmentManager;Z)V

    .line 4226
    :goto_1
    return-void

    .line 4202
    :catch_0
    move-exception v1

    .line 4204
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 4213
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/launcher2/BaseItem;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    goto :goto_1

    .line 4217
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->shouldDisablePopupRepeat()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4218
    invoke-virtual {p1}, Lcom/android/launcher2/BaseItem;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/android/launcher2/BaseItem;->mTitle:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v5}, Lcom/android/launcher2/DisableAppConfirmationDialog;->createAndShow(Ljava/lang/String;Ljava/lang/String;Landroid/app/FragmentManager;Z)V

    goto :goto_1

    .line 4222
    :cond_2
    invoke-virtual {p1}, Lcom/android/launcher2/BaseItem;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/android/launcher2/Launcher;->mPackageTobeDisabled:Ljava/lang/String;

    .line 4223
    sget-object v2, Lcom/android/launcher2/Launcher;->mPackageTobeDisabled:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->uninstallPackage(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public checkFactoryMode()Z
    .locals 1

    .prologue
    .line 3400
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->isFactorySim()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->isFactoryMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public closePopupMenu()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2041
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    if-eqz v0, :cond_0

    .line 2043
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    invoke-virtual {v0}, Lcom/android/launcher2/popup/PopupMenu;->dismiss()V

    .line 2044
    sput-boolean v1, Lcom/android/launcher2/Launcher;->sPopupMenuShowing:Z

    .line 2046
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->closeOptionsMenu()V

    .line 2047
    sput-boolean v1, Lcom/android/launcher2/Launcher;->sHwPopupMenuShowing:Z

    .line 2048
    return-void
.end method

.method public createPopupMenu(Landroid/view/View;)V
    .locals 3
    .param p1, "popupAnchorView"    # Landroid/view/View;

    .prologue
    .line 2808
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->setMarketLabel()V

    .line 2809
    if-nez p1, :cond_0

    .line 2810
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Could not find anchor for menu button."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2812
    :cond_0
    iput-object p1, p0, Lcom/android/launcher2/Launcher;->mMenuButtonView:Landroid/view/View;

    .line 2813
    new-instance v0, Lcom/android/launcher2/popup/PopupMenu;

    invoke-direct {v0, p0, p1}, Lcom/android/launcher2/popup/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    .line 2814
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    new-instance v1, Lcom/android/launcher2/Launcher$8;

    invoke-direct {v1, p0}, Lcom/android/launcher2/Launcher$8;-><init>(Lcom/android/launcher2/Launcher;)V

    invoke-virtual {v0, v1}, Lcom/android/launcher2/popup/PopupMenu;->setOnDismissListener(Lcom/android/launcher2/popup/PopupMenu$OnDismissListener;)V

    .line 2821
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    invoke-virtual {v0}, Lcom/android/launcher2/popup/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->mOptionsMenu:Landroid/view/Menu;

    .line 2822
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    invoke-virtual {v0}, Lcom/android/launcher2/popup/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f120000

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mOptionsMenu:Landroid/view/Menu;

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2823
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/popup/PopupMenu;->setOnMenuItemClickListener(Lcom/android/launcher2/popup/PopupMenu$OnMenuItemClickListener;)V

    .line 2824
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mOptionsMenu:Landroid/view/Menu;

    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->setupOptionsMenu(Landroid/view/Menu;)Z

    .line 2825
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const v9, 0x7f0f00ae

    const v8, 0x7f0f009b

    const/4 v0, 0x0

    const/4 v5, 0x1

    .line 1770
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v6

    if-nez v6, :cond_d

    .line 1771
    const/4 v1, 0x0

    .line 1772
    .local v1, "menuWidgets":Lcom/android/launcher2/MenuWidgets;
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    if-eqz v6, :cond_0

    .line 1773
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v6}, Lcom/android/launcher2/MenuView;->getMenuWidgets()Lcom/android/launcher2/MenuWidgets;

    move-result-object v1

    .line 1774
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    .line 1990
    .end local v1    # "menuWidgets":Lcom/android/launcher2/MenuWidgets;
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v5

    :cond_2
    :goto_1
    return v5

    .line 1777
    .restart local v1    # "menuWidgets":Lcom/android/launcher2/MenuWidgets;
    :sswitch_0
    sget v6, Lcom/android/launcher2/guide/GuideFragment;->sHelpHubStepNumber:I

    if-eq v6, v5, :cond_3

    sget v6, Lcom/android/launcher2/guide/GuideFragment;->sHelpHubStepNumber:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_4

    :cond_3
    sget-boolean v6, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v6, :cond_4

    .line 1780
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    const/16 v6, 0x17

    if-eq v5, v6, :cond_1

    move v5, v0

    .line 1784
    goto :goto_1

    .line 1788
    :cond_4
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/launcher2/HomeScreenOptionMenu;->hasFocus()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1792
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->isPageEdit()Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_1

    .line 1799
    :sswitch_1
    sget-boolean v6, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v6, :cond_2

    move v5, v0

    .line 1800
    goto :goto_1

    :sswitch_2
    move v5, v0

    .line 1805
    goto :goto_1

    .line 1807
    :sswitch_3
    const-string v6, "launcher_dump_state"

    invoke-static {v6}, Lcom/android/launcher2/Launcher;->isPropertyEnabled(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1808
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v6}, Lcom/android/launcher2/HomeView;->dumpState()V

    goto :goto_1

    .line 1816
    :sswitch_4
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->isPageEdit()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1817
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->isPageReorderingEdit()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1818
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher2/HomeScreenOptionMenu;->hasFocus()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1819
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher2/HomeScreenOptionMenu;->requestFocus()Z

    goto :goto_0

    .line 1823
    :cond_5
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/launcher2/MenuWidgets;->isShown()Z

    move-result v6

    if-eqz v6, :cond_2

    goto :goto_0

    .line 1830
    :cond_6
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->isEmptyPage()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1831
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHotseat()Lcom/android/launcher2/Hotseat;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 1832
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHotseat()Lcom/android/launcher2/Hotseat;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/launcher2/Hotseat;->requestFocus()Z

    goto :goto_1

    .line 1840
    :sswitch_5
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->isPageEdit()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1841
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->isPageReorderingEdit()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1842
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher2/HomeScreenOptionMenu;->hasFocus()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1843
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher2/HomeScreenOptionMenu;->requestFocus()Z

    goto/16 :goto_0

    .line 1846
    :cond_7
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/launcher2/MenuWidgets;->isShown()Z

    move-result v6

    if-eqz v6, :cond_2

    goto/16 :goto_0

    .line 1853
    :cond_8
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->isEmptyPage()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1854
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHotseat()Lcom/android/launcher2/Hotseat;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 1855
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHotseat()Lcom/android/launcher2/Hotseat;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/launcher2/Hotseat;->requestFocus()Z

    goto/16 :goto_1

    .line 1861
    :cond_9
    if-eqz v1, :cond_1

    .line 1862
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getCurrentFocus()Landroid/view/View;

    move-result-object v6

    instance-of v6, v6, Lcom/android/launcher2/PagedViewWidget;

    if-nez v6, :cond_a

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getCurrentFocus()Landroid/view/View;

    move-result-object v6

    instance-of v6, v6, Landroid/widget/SearchView$SearchAutoComplete;

    if-nez v6, :cond_a

    move v0, v5

    .line 1863
    .local v0, "allowFocus":Z
    :cond_a
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v6}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v1}, Lcom/android/launcher2/MenuWidgets;->getWidgetState()Lcom/android/launcher2/MenuWidgets$WidgetState;

    move-result-object v6

    sget-object v7, Lcom/android/launcher2/MenuWidgets$WidgetState;->SEARCH:Lcom/android/launcher2/MenuWidgets$WidgetState;

    if-ne v6, v7, :cond_1

    if-eqz v0, :cond_1

    .line 1864
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    const v7, 0x7f0f00c0

    invoke-virtual {v6, v7}, Lcom/android/launcher2/MenuView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 1865
    .local v2, "searchInput":Landroid/view/ViewGroup;
    if-eqz v2, :cond_2

    .line 1866
    invoke-virtual {v2}, Landroid/view/ViewGroup;->requestFocus()Z

    goto/16 :goto_1

    .end local v0    # "allowFocus":Z
    .end local v2    # "searchInput":Landroid/view/ViewGroup;
    :sswitch_6
    move v5, v0

    .line 1873
    goto/16 :goto_1

    .line 1875
    :sswitch_7
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    if-eqz v5, :cond_c

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v5}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v5

    if-eqz v5, :cond_c

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v5}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v5

    if-nez v5, :cond_c

    .line 1878
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getCurrentFocus()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/launcher2/HomeView;->clearChildFocus(Landroid/view/View;)V

    :cond_b
    :goto_2
    move v5, v0

    .line 1885
    goto/16 :goto_1

    .line 1880
    :cond_c
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->getMenuAppsGrid()Lcom/android/launcher2/MenuAppsGrid;

    move-result-object v5

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->getMenuAppsGrid()Lcom/android/launcher2/MenuAppsGrid;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher2/MenuAppsGrid;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v5

    if-nez v5, :cond_b

    .line 1883
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getCurrentFocus()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/launcher2/MenuView;->clearChildFocus(Landroid/view/View;)V

    goto :goto_2

    .line 1887
    .end local v1    # "menuWidgets":Lcom/android/launcher2/MenuWidgets;
    :cond_d
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v6

    if-ne v6, v5, :cond_1

    .line 1888
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_1

    goto/16 :goto_0

    .line 1902
    :sswitch_8
    sget-boolean v6, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v6, :cond_2

    move v5, v0

    .line 1903
    goto/16 :goto_1

    .line 1892
    :sswitch_9
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/launcher2/HomeScreenOptionMenu;->hasFocus()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1896
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->isPageEdit()Z

    move-result v6

    if-eqz v6, :cond_1

    goto/16 :goto_1

    .line 1909
    :sswitch_a
    sget-boolean v6, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v6, :cond_e

    move v5, v0

    .line 1910
    goto/16 :goto_1

    .line 1913
    :cond_e
    iget-boolean v6, p0, Lcom/android/launcher2/Launcher;->mHasMenuKey:Z

    if-nez v6, :cond_18

    .line 1914
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v6}, Lcom/android/launcher2/MenuView;->getMenuWidgets()Lcom/android/launcher2/MenuWidgets;

    move-result-object v1

    .line 1915
    .restart local v1    # "menuWidgets":Lcom/android/launcher2/MenuWidgets;
    if-eqz v1, :cond_10

    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v6}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_10

    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v6}, Lcom/android/launcher2/MenuView;->isCurrentTabWidgets()Z

    move-result v6

    if-eqz v6, :cond_10

    .line 1916
    invoke-virtual {v1}, Lcom/android/launcher2/MenuWidgets;->getWidgetFolder()Lcom/android/launcher2/WidgetFolder;

    move-result-object v4

    .line 1917
    .local v4, "widgetFolder":Lcom/android/launcher2/WidgetFolder;
    invoke-virtual {v1}, Lcom/android/launcher2/MenuWidgets;->getWidgetState()Lcom/android/launcher2/MenuWidgets$WidgetState;

    move-result-object v6

    sget-object v7, Lcom/android/launcher2/MenuWidgets$WidgetState;->NORMAL:Lcom/android/launcher2/MenuWidgets$WidgetState;

    if-eq v6, v7, :cond_f

    move v5, v0

    .line 1918
    goto/16 :goto_1

    .line 1920
    :cond_f
    if-eqz v4, :cond_12

    invoke-virtual {v4}, Lcom/android/launcher2/WidgetFolder;->isOpened()Z

    move-result v6

    if-eqz v6, :cond_12

    move v5, v0

    .line 1921
    goto/16 :goto_1

    .line 1924
    .end local v4    # "widgetFolder":Lcom/android/launcher2/WidgetFolder;
    :cond_10
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v6}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_11

    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v6}, Lcom/android/launcher2/MenuView;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v6

    sget-object v7, Lcom/android/launcher2/MenuAppsGrid$State;->NORMAL:Lcom/android/launcher2/MenuAppsGrid$State;

    if-eq v6, v7, :cond_11

    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v6}, Lcom/android/launcher2/MenuView;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v6

    sget-object v7, Lcom/android/launcher2/MenuAppsGrid$State;->DOWNLOADED_APPS:Lcom/android/launcher2/MenuAppsGrid$State;

    if-eq v6, v7, :cond_11

    move v5, v0

    .line 1927
    goto/16 :goto_1

    .line 1929
    :cond_11
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v6}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_12

    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v6}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/launcher2/Workspace;->getState()Lcom/android/launcher2/Workspace$State;

    move-result-object v6

    sget-object v7, Lcom/android/launcher2/Workspace$State;->NORMAL:Lcom/android/launcher2/Workspace$State;

    if-eq v6, v7, :cond_12

    move v5, v0

    .line 1931
    goto/16 :goto_1

    .line 1934
    :cond_12
    const/4 v3, 0x0

    .line 1935
    .local v3, "v":Landroid/view/View;
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v6}, Lcom/android/launcher2/MenuView;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v6

    sget-object v7, Lcom/android/launcher2/MenuAppsGrid$State;->DOWNLOADED_APPS:Lcom/android/launcher2/MenuAppsGrid$State;

    if-ne v6, v7, :cond_15

    .line 1936
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    if-eqz v6, :cond_13

    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuButtonView:Landroid/view/View;

    if-eqz v6, :cond_14

    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuButtonView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v6

    if-eq v6, v8, :cond_14

    .line 1938
    :cond_13
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v6, v8}, Lcom/android/launcher2/MenuView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1939
    invoke-virtual {p0, v3}, Lcom/android/launcher2/Launcher;->createPopupMenu(Landroid/view/View;)V

    .line 1954
    :cond_14
    :goto_3
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mOptionsMenu:Landroid/view/Menu;

    invoke-direct {p0, v6}, Lcom/android/launcher2/Launcher;->setupOptionsMenu(Landroid/view/Menu;)Z

    .line 1955
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    invoke-virtual {v6}, Lcom/android/launcher2/popup/PopupMenu;->dismiss()V

    .line 1956
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    invoke-virtual {v6}, Lcom/android/launcher2/popup/PopupMenu;->show()V

    .line 1957
    sput-boolean v5, Lcom/android/launcher2/Launcher;->sPopupMenuShowing:Z

    goto/16 :goto_1

    .line 1941
    :cond_15
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v6}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_16

    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v6}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/launcher2/Workspace;->getState()Lcom/android/launcher2/Workspace$State;

    move-result-object v6

    sget-object v7, Lcom/android/launcher2/Workspace$State;->NORMAL:Lcom/android/launcher2/Workspace$State;

    if-ne v6, v7, :cond_16

    .line 1942
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v6

    and-int/lit8 v6, v6, 0x20

    if-nez v6, :cond_2

    .line 1943
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v5}, Lcom/android/launcher2/HomeView;->openQuickViewWorkspace(Lcom/android/launcher2/HomeView$SavedState;Z)V

    goto/16 :goto_1

    .line 1946
    :cond_16
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    if-eqz v6, :cond_17

    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuButtonView:Landroid/view/View;

    if-eqz v6, :cond_14

    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mMenuButtonView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v6

    if-eq v6, v9, :cond_14

    .line 1948
    :cond_17
    invoke-virtual {p0, v9}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1949
    invoke-virtual {p0, v3}, Lcom/android/launcher2/Launcher;->createPopupMenu(Landroid/view/View;)V

    goto :goto_3

    .line 1968
    .end local v1    # "menuWidgets":Lcom/android/launcher2/MenuWidgets;
    .end local v3    # "v":Landroid/view/View;
    :cond_18
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v6}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/launcher2/Workspace;->getState()Lcom/android/launcher2/Workspace$State;

    move-result-object v6

    sget-object v7, Lcom/android/launcher2/Workspace$State;->EDIT:Lcom/android/launcher2/Workspace$State;

    if-eq v6, v7, :cond_2

    move v5, v0

    .line 1970
    goto/16 :goto_1

    .line 1977
    :sswitch_b
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->isPageEdit()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1978
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->isPageReorderingEdit()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1979
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher2/HomeScreenOptionMenu;->hasFocus()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1980
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher2/HomeScreenOptionMenu;->requestFocus()Z

    goto/16 :goto_0

    .line 1774
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x13 -> :sswitch_4
        0x14 -> :sswitch_4
        0x15 -> :sswitch_4
        0x16 -> :sswitch_5
        0x17 -> :sswitch_0
        0x19 -> :sswitch_3
        0x42 -> :sswitch_0
        0x52 -> :sswitch_2
        0x107 -> :sswitch_6
        0x10a -> :sswitch_7
    .end sparse-switch

    .line 1888
    :sswitch_data_1
    .sparse-switch
        0x3 -> :sswitch_8
        0x13 -> :sswitch_b
        0x14 -> :sswitch_b
        0x15 -> :sswitch_b
        0x16 -> :sswitch_b
        0x17 -> :sswitch_9
        0x42 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 1293
    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 1294
    .local v0, "result":Z
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    .line 1295
    .local v1, "text":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1296
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v2}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1297
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v2}, Lcom/android/launcher2/MenuView;->isCurrentTabAppsTab()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1298
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v2}, Lcom/android/launcher2/MenuView;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v2

    sget-object v3, Lcom/android/launcher2/MenuAppsGrid$State;->DOWNLOADED_APPS:Lcom/android/launcher2/MenuAppsGrid$State;

    if-ne v2, v3, :cond_1

    .line 1299
    const v2, 0x7f100035

    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1310
    :cond_0
    :goto_0
    return v0

    .line 1301
    :cond_1
    const v2, 0x7f1000d9

    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1304
    :cond_2
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v2}, Lcom/android/launcher2/MenuView;->isCurrentTabWidgets()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1305
    const v2, 0x7f100003

    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1308
    :cond_3
    const v2, 0x7f100018

    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public exitAllApps()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1421
    invoke-virtual {p0, v0, v0}, Lcom/android/launcher2/Launcher;->exitAllApps(ZZ)V

    .line 1422
    return-void
.end method

.method public exitAllApps(ZZ)V
    .locals 9
    .param p1, "withDrag"    # Z
    .param p2, "immediate"    # Z

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1432
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v4}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v4

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/android/launcher2/Launcher;->mTransitioningToAllApps:Z

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    invoke-virtual {v4}, Landroid/animation/Animator;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1434
    :cond_1
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    invoke-virtual {v4}, Landroid/animation/Animator;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1436
    iget-boolean v4, p0, Lcom/android/launcher2/Launcher;->mTransitioningToAllApps:Z

    if-nez v4, :cond_3

    .line 1437
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    invoke-virtual {v4}, Landroid/animation/Animator;->end()V

    .line 1438
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/launcher2/QuickViewWorkspace;->drawCloseAnimation()Z

    .line 1503
    :cond_2
    :goto_0
    return-void

    .line 1440
    :cond_3
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 1441
    .local v1, "mHandler":Landroid/os/Handler;
    new-instance v4, Lcom/android/launcher2/Launcher$5;

    invoke-direct {v4, p0}, Lcom/android/launcher2/Launcher$5;-><init>(Lcom/android/launcher2/Launcher;)V

    const-wide/16 v6, 0xc8

    invoke-virtual {v1, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1451
    .end local v1    # "mHandler":Landroid/os/Handler;
    :cond_4
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v4}, Lcom/android/launcher2/MenuView;->onExitAllApps()V

    .line 1452
    sget-boolean v4, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v4, :cond_5

    sget-object v4, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v5, "add_widgets"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1453
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4}, Lcom/android/launcher2/HomeView;->addHelpAppPage()V

    .line 1455
    :cond_5
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    if-eqz v4, :cond_6

    .line 1456
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    invoke-virtual {v4}, Landroid/animation/Animator;->cancel()V

    .line 1459
    :cond_6
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v3

    .line 1460
    .local v3, "workspace":Lcom/android/launcher2/Workspace;
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v2

    .line 1462
    .local v2, "quickviewWorkspace":Lcom/android/launcher2/QuickViewWorkspace;
    if-eqz v3, :cond_c

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v4

    if-nez v4, :cond_c

    .line 1463
    :cond_7
    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/launcher2/Workspace;->resumeScreen(I)V

    .line 1464
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4}, Lcom/android/launcher2/HomeView;->getHomeAppsBtn()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 1465
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4}, Lcom/android/launcher2/HomeView;->getHomeAppsBtn()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1466
    :cond_8
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4}, Lcom/android/launcher2/HomeView;->getHomePhoneBtn()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 1467
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4}, Lcom/android/launcher2/HomeView;->getHomePhoneBtn()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1472
    :cond_9
    :goto_1
    if-eqz p2, :cond_d

    .line 1475
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4, v6}, Lcom/android/launcher2/HomeView;->setVisibility(I)V

    .line 1476
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4, v8}, Lcom/android/launcher2/HomeView;->setAlpha(F)V

    .line 1477
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4, v8}, Lcom/android/launcher2/HomeView;->setScaleX(F)V

    .line 1478
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4, v8}, Lcom/android/launcher2/HomeView;->setScaleY(F)V

    .line 1479
    if-eqz v3, :cond_b

    .line 1480
    const/4 v0, 0x0

    .line 1481
    .local v0, "darkenAmount":F
    if-eqz v2, :cond_a

    invoke-virtual {v2}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1482
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0005

    invoke-virtual {v4, v5, v7, v7}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    .line 1484
    :cond_a
    invoke-virtual {v3, v0}, Lcom/android/launcher2/Workspace;->setBackgroundDarken(F)V

    .line 1486
    .end local v0    # "darkenAmount":F
    :cond_b
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->finishExitAllApps()V

    goto/16 :goto_0

    .line 1469
    :cond_c
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4}, Lcom/android/launcher2/HomeView;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/android/launcher2/HomeScreenOptionMenu;->setMakeActive(Z)V

    goto :goto_1

    .line 1490
    :cond_d
    iput-boolean v7, p0, Lcom/android/launcher2/Launcher;->mExitingAllApps:Z

    .line 1491
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4, v6}, Lcom/android/launcher2/HomeView;->setVisibility(I)V

    .line 1493
    if-eqz p1, :cond_f

    .line 1494
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1495
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4, v7, v6}, Lcom/android/launcher2/HomeView;->closeQuickViewWorkspace(ZZ)V

    .line 1497
    :cond_e
    if-eqz v3, :cond_2

    .line 1498
    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->onFadeStart()V

    goto/16 :goto_0

    .line 1501
    :cond_f
    invoke-virtual {p0, v6}, Lcom/android/launcher2/Launcher;->exitAllappsAnimation(Z)V

    goto/16 :goto_0
.end method

.method public exitAllappsAnimation(Z)V
    .locals 10
    .param p1, "withDrag"    # Z

    .prologue
    .line 1506
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v8}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v6

    .line 1507
    .local v6, "workspace":Lcom/android/launcher2/Workspace;
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v8}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v4

    .line 1509
    .local v4, "quickviewWorkspace":Lcom/android/launcher2/QuickViewWorkspace;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1510
    .local v0, "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mStateAnimatorProviders:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/Launcher$StateAnimatorProvider;

    .line 1511
    .local v3, "provider":Lcom/android/launcher2/Launcher$StateAnimatorProvider;
    invoke-interface {v3, v0}, Lcom/android/launcher2/Launcher$StateAnimatorProvider;->collectExitAllAppsAnimators(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 1514
    .end local v3    # "provider":Lcom/android/launcher2/Launcher$StateAnimatorProvider;
    :cond_0
    if-eqz v6, :cond_2

    if-nez p1, :cond_2

    .line 1515
    const v7, 0x7f050009

    .line 1516
    .local v7, "xmlID":I
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1517
    const v7, 0x7f05000a

    .line 1518
    :cond_1
    invoke-static {p0, v7}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v1

    .line 1519
    .local v1, "darkenAnimator":Landroid/animation/Animator;
    invoke-virtual {v1, v6}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 1520
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1523
    .end local v1    # "darkenAnimator":Landroid/animation/Animator;
    .end local v7    # "xmlID":I
    :cond_2
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1524
    .local v5, "set":Landroid/animation/AnimatorSet;
    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 1525
    const-wide/16 v8, 0x190

    invoke-virtual {v5, v8, v9}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1526
    new-instance v8, Landroid/view/animation/interpolator/SineInOut90;

    invoke-direct {v8}, Landroid/view/animation/interpolator/SineInOut90;-><init>()V

    invoke-virtual {v5, v8}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1527
    new-instance v8, Lcom/android/launcher2/Launcher$6;

    invoke-direct {v8, p0}, Lcom/android/launcher2/Launcher$6;-><init>(Lcom/android/launcher2/Launcher;)V

    invoke-virtual {v5, v8}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1541
    iput-object v5, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    .line 1542
    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    .line 1543
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getAnimationLayer()Lcom/android/launcher2/AnimationLayer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/launcher2/AnimationLayer;->cancelAnimations()V

    .line 1544
    return-void
.end method

.method public exitHelp(Z)V
    .locals 7
    .param p1, "isBackPressed"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2436
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHomeView()Lcom/android/launcher2/HomeView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2437
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHomeView()Lcom/android/launcher2/HomeView;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/android/launcher2/HomeView;->closeQuickViewWorkspace(Z)V

    .line 2438
    :cond_0
    sput-boolean v5, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    .line 2439
    sget-object v3, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v4, "add_widgets"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2440
    sput-boolean v6, Lcom/android/launcher2/Launcher;->isExitingFromHelp:Z

    .line 2441
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v3}, Lcom/android/launcher2/HomeView;->removehelpAppPage()V

    .line 2442
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    iput-boolean v5, v3, Lcom/android/launcher2/HomeView;->mIsHelpItemAdded:Z

    .line 2443
    sput-boolean v5, Lcom/android/launcher2/HomeEditBar;->isfolderCreated:Z

    .line 2444
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2445
    .local v1, "intentExitHelp":Landroid/content/Intent;
    const-string v3, "homescreen:guide_mode"

    const-string v4, "onBackPressed"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2446
    const-string v3, "currentGmode"

    sget-object v4, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2447
    invoke-static {p0, v1}, Lcom/android/launcher2/guide/GuideFragment;->deployGuide(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 2448
    sput-boolean v6, Lcom/android/launcher2/guide/ResizeWidgetsGuider;->isWidgetToBeAdded:Z

    .line 2449
    sput v6, Lcom/android/launcher2/guide/GuideFragment;->sHelpHubStepNumber:I

    .line 2450
    sput-boolean v5, Lcom/android/launcher2/guide/GuideFragment;->isViewAppsDialogDismissed:Z

    .line 2451
    if-eqz p1, :cond_1

    .line 2452
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->getLaunchIntentForHelpHub()Landroid/content/Intent;

    move-result-object v0

    .line 2453
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_1

    .line 2454
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 2522
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v4, 0x258

    if-ge v3, v4, :cond_2

    .line 2523
    iput v6, p0, Lcom/android/launcher2/Launcher;->mPrevOrientationHelp:I

    .line 2526
    :cond_2
    iget v3, p0, Lcom/android/launcher2/Launcher;->mPrevOrientationHelp:I

    invoke-virtual {p0, v3}, Lcom/android/launcher2/Launcher;->setRequestedOrientation(I)V

    .line 2528
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v3}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/android/launcher2/Workspace;->showPageIndicator(Z)V

    .line 2529
    return-void

    .line 2456
    .end local v1    # "intentExitHelp":Landroid/content/Intent;
    :cond_3
    sget-object v3, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v4, "change_wallpaper"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2457
    sput-boolean v6, Lcom/android/launcher2/Launcher;->isExitingFromHelp:Z

    .line 2458
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v3}, Lcom/android/launcher2/HomeView;->removehelpAppPage()V

    .line 2459
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    iput-boolean v5, v3, Lcom/android/launcher2/HomeView;->mIsHelpItemAdded:Z

    .line 2460
    sput-boolean v5, Lcom/android/launcher2/HomeEditBar;->isfolderCreated:Z

    .line 2461
    sget-boolean v3, Lcom/android/launcher2/HomeScreenDialogFragment;->isHomeScreenDialogVisible:Z

    if-eqz v3, :cond_4

    .line 2462
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-static {v3}, Lcom/android/launcher2/HomeScreenDialogFragment;->dismiss(Landroid/app/FragmentManager;)V

    .line 2464
    :cond_4
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2465
    .restart local v1    # "intentExitHelp":Landroid/content/Intent;
    const-string v3, "homescreen:guide_mode"

    const-string v4, "onBackPressed"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2466
    const-string v3, "currentGmode"

    sget-object v4, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2467
    invoke-static {p0, v1}, Lcom/android/launcher2/guide/GuideFragment;->deployGuide(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 2468
    sput-boolean v6, Lcom/android/launcher2/guide/ResizeWidgetsGuider;->isWidgetToBeAdded:Z

    .line 2469
    sput v6, Lcom/android/launcher2/guide/GuideFragment;->sHelpHubStepNumber:I

    .line 2470
    sput-boolean v5, Lcom/android/launcher2/guide/GuideFragment;->isViewAppsDialogDismissed:Z

    .line 2471
    if-eqz p1, :cond_1

    .line 2472
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->getLaunchIntentForHelpHub()Landroid/content/Intent;

    move-result-object v0

    .line 2473
    .restart local v0    # "intent":Landroid/content/Intent;
    if-eqz v0, :cond_1

    .line 2474
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2479
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "intentExitHelp":Landroid/content/Intent;
    :cond_5
    sget-boolean v3, Lcom/android/launcher2/Launcher;->wasWidgetsTabShown:Z

    if-eqz v3, :cond_7

    .line 2480
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v3}, Lcom/android/launcher2/MenuView;->getMenuWidgets()Lcom/android/launcher2/MenuWidgets;

    move-result-object v2

    .line 2481
    .local v2, "menuWidgets":Lcom/android/launcher2/MenuWidgets;
    if-eqz v2, :cond_6

    .line 2482
    const-string v3, "Launcher - exitHelp"

    invoke-virtual {v2, v3}, Lcom/android/launcher2/MenuWidgets;->updatePackages(Ljava/lang/String;)V

    .line 2484
    :cond_6
    sput-boolean v5, Lcom/android/launcher2/Launcher;->wasWidgetsTabShown:Z

    .line 2487
    .end local v2    # "menuWidgets":Lcom/android/launcher2/MenuWidgets;
    :cond_7
    invoke-static {}, Lcom/android/launcher2/guide/GuideFragment;->removeWrongActionDialog()V

    .line 2488
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v3}, Lcom/android/launcher2/HomeView;->removehelpAppPage()V

    .line 2489
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    iput-boolean v5, v3, Lcom/android/launcher2/HomeView;->mIsHelpItemAdded:Z

    .line 2490
    sput-boolean v5, Lcom/android/launcher2/HomeEditBar;->isfolderCreated:Z

    .line 2491
    sget-boolean v3, Lcom/android/launcher2/HomeScreenDialogFragment;->isHomeScreenDialogVisible:Z

    if-eqz v3, :cond_8

    .line 2492
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-static {v3}, Lcom/android/launcher2/HomeScreenDialogFragment;->dismiss(Landroid/app/FragmentManager;)V

    .line 2494
    :cond_8
    sput-boolean v5, Lcom/android/launcher2/CellLayout;->isWidgetResized:Z

    .line 2495
    sput-boolean v5, Lcom/android/launcher2/Launcher;->isExitingFromWallpaperActivity:Z

    .line 2496
    sput-boolean v6, Lcom/android/launcher2/AddToHomescreenDialogFragment;->setWallpaperSelected:Z

    .line 2497
    sput-boolean v6, Lcom/android/launcher2/HomeScreenDialogFragment;->homescreenSelected:Z

    .line 2499
    sget-object v3, Lcom/android/launcher2/MenuView;->mOldViewType:Lcom/android/launcher2/MenuView$ViewType;

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v4}, Lcom/android/launcher2/MenuView;->getViewType()Lcom/android/launcher2/MenuView$ViewType;

    move-result-object v4

    if-eq v3, v4, :cond_9

    .line 2500
    sget-object v3, Lcom/android/launcher2/MenuView;->mOldViewType:Lcom/android/launcher2/MenuView$ViewType;

    if-eqz v3, :cond_9

    .line 2501
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    sget-object v4, Lcom/android/launcher2/MenuView;->mOldViewType:Lcom/android/launcher2/MenuView$ViewType;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/MenuView;->setViewType(Lcom/android/launcher2/MenuView$ViewType;)V

    .line 2503
    :cond_9
    sget-object v3, Lcom/android/launcher2/MenuView;->oldState:Lcom/android/launcher2/MenuAppsGrid$State;

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v4}, Lcom/android/launcher2/MenuView;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v4

    if-eq v3, v4, :cond_a

    .line 2504
    sget-object v3, Lcom/android/launcher2/MenuView;->oldState:Lcom/android/launcher2/MenuAppsGrid$State;

    if-eqz v3, :cond_a

    .line 2505
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    sget-object v4, Lcom/android/launcher2/MenuView;->oldState:Lcom/android/launcher2/MenuAppsGrid$State;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/MenuView;->changeState(Lcom/android/launcher2/MenuAppsGrid$State;)V

    .line 2508
    :cond_a
    sput-boolean v6, Lcom/android/launcher2/Launcher;->isExitingFromHelp:Z

    .line 2509
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2510
    .restart local v1    # "intentExitHelp":Landroid/content/Intent;
    const-string v3, "homescreen:guide_mode"

    const-string v4, "onBackPressed"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2511
    const-string v3, "currentGmode"

    sget-object v4, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2512
    invoke-static {p0, v1}, Lcom/android/launcher2/guide/GuideFragment;->deployGuide(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 2513
    sput-boolean v6, Lcom/android/launcher2/guide/ResizeWidgetsGuider;->isWidgetToBeAdded:Z

    .line 2514
    sput v6, Lcom/android/launcher2/guide/GuideFragment;->sHelpHubStepNumber:I

    .line 2515
    sput-boolean v5, Lcom/android/launcher2/guide/GuideFragment;->isViewAppsDialogDismissed:Z

    .line 2516
    if-eqz p1, :cond_1

    .line 2517
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->getLaunchIntentForHelpHub()Landroid/content/Intent;

    move-result-object v0

    .line 2518
    .restart local v0    # "intent":Landroid/content/Intent;
    if-eqz v0, :cond_1

    .line 2519
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public finishWallpaperGuide(Z)V
    .locals 4
    .param p1, "isSuccess"    # Z

    .prologue
    const/4 v2, 0x1

    .line 4299
    if-eqz p1, :cond_1

    .line 4300
    const v1, 0x7f1000ac

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 4301
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 4302
    .local v0, "mHandler":Landroid/os/Handler;
    new-instance v1, Lcom/android/launcher2/Launcher$9;

    invoke-direct {v1, p0}, Lcom/android/launcher2/Launcher$9;-><init>(Lcom/android/launcher2/Launcher;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4313
    .end local v0    # "mHandler":Landroid/os/Handler;
    :cond_0
    :goto_0
    return-void

    .line 4310
    :cond_1
    sget-boolean v1, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v1, :cond_0

    .line 4311
    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->exitHelp(Z)V

    goto :goto_0
.end method

.method public getAnimationLayer()Lcom/android/launcher2/AnimationLayer;
    .locals 1

    .prologue
    .line 786
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mAnimationLayer:Lcom/android/launcher2/AnimationLayer;

    return-object v0
.end method

.method public getDisplayOrientation()I
    .locals 3

    .prologue
    .line 3437
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 3438
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 3440
    .local v1, "rotation":I
    packed-switch v1, :pswitch_data_0

    .line 3447
    :pswitch_0
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 3443
    :pswitch_1
    const/4 v2, 0x2

    goto :goto_0

    .line 3440
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getFestivalPageManager()Lcom/android/launcher2/FestivalPageManager;
    .locals 1

    .prologue
    .line 4041
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mFestivalPageManager:Lcom/android/launcher2/FestivalPageManager;

    return-object v0
.end method

.method public getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;
    .locals 1

    .prologue
    .line 2603
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v0

    return-object v0
.end method

.method public getHomeView()Lcom/android/launcher2/HomeView;
    .locals 1

    .prologue
    .line 2599
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    return-object v0
.end method

.method getHotseat()Lcom/android/launcher2/Hotseat;
    .locals 1

    .prologue
    .line 2595
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->getHotseat()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    return-object v0
.end method

.method public getHotwordServiceClient()Lcom/google/android/hotword/client/HotwordServiceClient;
    .locals 1

    .prologue
    .line 2209
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHotwordServiceClient:Lcom/google/android/hotword/client/HotwordServiceClient;

    return-object v0
.end method

.method public getMenuView()Lcom/android/launcher2/MenuView;
    .locals 1

    .prologue
    .line 1577
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    return-object v0
.end method

.method public getQuickViewWorkspaceOpen()Z
    .locals 1

    .prologue
    .line 3971
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3972
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v0

    .line 3974
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSamsungWidgetPackageManager()Lcom/android/launcher2/SamsungWidgetPackageManager;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mSamsungWidgetPackageManager:Lcom/android/launcher2/SamsungWidgetPackageManager;

    return-object v0
.end method

.method public getSecretPageManager()Lcom/android/launcher2/SecretPageManager;
    .locals 1

    .prologue
    .line 4036
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mSecretPageManager:Lcom/android/launcher2/SecretPageManager;

    return-object v0
.end method

.method public getShowEmptyPageMessagePref()Z
    .locals 1

    .prologue
    .line 3920
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mShowEmptyPageMsg:Z

    return v0
.end method

.method public getSurfaceWidgetPackageManager()Lcom/android/launcher2/SurfaceWidgetPackageManager;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mSurfaceWidgetPackageManager:Lcom/android/launcher2/SurfaceWidgetPackageManager;

    return-object v0
.end method

.method public getTransitionToAllApps()Z
    .locals 1

    .prologue
    .line 1273
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mTransitioningToAllApps:Z

    return v0
.end method

.method public getWindowToken()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 782
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget-object v0, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    return-object v0
.end method

.method public isAddToScreenDialogShowing()Z
    .locals 1

    .prologue
    .line 2606
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/android/launcher2/AddToHomescreenDialogFragment;->isActive(Landroid/app/FragmentManager;)Z

    move-result v0

    return v0
.end method

.method public isAirMoveOninSettings()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 4141
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4142
    .local v0, "mResolver":Landroid/content/ContentResolver;
    const-string v3, "air_motion_item_move"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    const-string v3, "air_motion_engine"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    sget-boolean v3, Lcom/android/launcher2/Launcher;->bSupportAirMove:Z

    if-eqz v3, :cond_0

    .line 4146
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public isDialogChecked()Z
    .locals 3

    .prologue
    .line 4253
    const-string v1, "com.sec.android.app.launcher.prefs"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/android/launcher2/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 4255
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "add.toast.popup.dismissed.key"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public isEmptyPage()Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1994
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v6}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1995
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    iget-object v7, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v7}, Lcom/android/launcher2/HomeView;->getCurrentPage()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/android/launcher2/HomeView;->getPage(I)Lcom/android/launcher2/CellLayout;

    move-result-object v4

    .line 1996
    .local v4, "workspace":Lcom/android/launcher2/CellLayout;
    invoke-virtual {v4, v5}, Lcom/android/launcher2/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    instance-of v6, v6, Lcom/android/launcher2/CellLayoutChildren;

    if-nez v6, :cond_1

    .line 2014
    .end local v4    # "workspace":Lcom/android/launcher2/CellLayout;
    :cond_0
    :goto_0
    return v5

    .line 1999
    .restart local v4    # "workspace":Lcom/android/launcher2/CellLayout;
    :cond_1
    invoke-virtual {v4, v5}, Lcom/android/launcher2/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/CellLayoutChildren;

    .line 2000
    .local v3, "parent":Lcom/android/launcher2/CellLayoutChildren;
    const/4 v0, 0x0

    .line 2002
    .local v0, "countAppIcon":I
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_1
    invoke-virtual {v3}, Lcom/android/launcher2/CellLayoutChildren;->getChildCount()I

    move-result v6

    if-ge v2, v6, :cond_3

    .line 2003
    invoke-virtual {v3, v2}, Lcom/android/launcher2/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2004
    .local v1, "countItem":Landroid/view/View;
    instance-of v6, v1, Lcom/android/launcher2/AppIconView;

    if-eqz v6, :cond_2

    .line 2005
    add-int/lit8 v0, v0, 0x1

    .line 2002
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2009
    .end local v1    # "countItem":Landroid/view/View;
    :cond_3
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getCurrentFocus()Landroid/view/View;

    move-result-object v6

    if-nez v6, :cond_0

    if-nez v0, :cond_0

    .line 2010
    const/4 v5, 0x1

    goto :goto_0
.end method

.method public isExitingAllApps()Z
    .locals 1

    .prologue
    .line 1417
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mExitingAllApps:Z

    return v0
.end method

.method public isHapticFeedbackExtraOn()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4666
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "haptic_feedback_extra"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 4667
    .local v0, "sIsHapticFeedbackExtraOn":Z
    :goto_0
    return v0

    .end local v0    # "sIsHapticFeedbackExtraOn":Z
    :cond_0
    move v0, v1

    .line 4666
    goto :goto_0
.end method

.method public isHelpAppAvailable()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 3471
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.samsung.helphub.HELP"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3472
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 3473
    .local v2, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 3475
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    const/4 v3, 0x1

    :cond_0
    return v3
.end method

.method public isHelpHubAvailable()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 3454
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isTabletLayout()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3455
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/android/launcher2/Launcher;->sHelpHubAvailable:Ljava/lang/Boolean;

    .line 3467
    :cond_0
    :goto_0
    sget-object v0, Lcom/android/launcher2/Launcher;->sHelpHubAvailable:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 3456
    :cond_1
    sget-object v0, Lcom/android/launcher2/Launcher;->sHelpHubAvailable:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 3458
    const-string v0, "content://com.samsung.helphub.provider/help_page_status/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 3459
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "displayed"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3460
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_2

    .line 3461
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v0, Lcom/android/launcher2/Launcher;->sHelpHubAvailable:Ljava/lang/Boolean;

    goto :goto_0

    .line 3463
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 3464
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sput-object v0, Lcom/android/launcher2/Launcher;->sHelpHubAvailable:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public isKeyStringBlocked()Z
    .locals 2

    .prologue
    .line 3379
    const/4 v0, 0x0

    .line 3395
    .local v0, "imeiBlocked":Ljava/lang/String;
    const/4 v1, 0x0

    return v1
.end method

.method public isLauncherDestroyed()Z
    .locals 1

    .prologue
    .line 1169
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mIsDestroyed:Z

    return v0
.end method

.method public isPageEdit()Z
    .locals 3

    .prologue
    .line 2018
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    if-eqz v1, :cond_2

    .line 2019
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->getMenuWidgets()Lcom/android/launcher2/MenuWidgets;

    move-result-object v0

    .line 2020
    .local v0, "menuWidget":Lcom/android/launcher2/MenuWidgets;
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/launcher2/MenuWidgets;->isShown()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v1

    sget-object v2, Lcom/android/launcher2/MenuAppsGrid$State;->PAGE_EDIT:Lcom/android/launcher2/MenuAppsGrid$State;

    if-ne v1, v2, :cond_2

    .line 2022
    :cond_1
    const/4 v1, 0x1

    .line 2025
    .end local v0    # "menuWidget":Lcom/android/launcher2/MenuWidgets;
    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPageReorderingEdit()Z
    .locals 2

    .prologue
    .line 2029
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    if-eqz v1, :cond_1

    .line 2030
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->getMenuWidgets()Lcom/android/launcher2/MenuWidgets;

    move-result-object v0

    .line 2031
    .local v0, "menuWidget":Lcom/android/launcher2/MenuWidgets;
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/HomeScreenOptionMenu;->isSettingVisible()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/launcher2/MenuWidgets;->isShown()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2034
    :cond_0
    const/4 v1, 0x1

    .line 2037
    .end local v0    # "menuWidget":Lcom/android/launcher2/MenuWidgets;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 3912
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mPaused:Z

    return v0
.end method

.method public isResumed_()Z
    .locals 1

    .prologue
    .line 4787
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mResumed:Z

    return v0
.end method

.method public isTransitioningRunning()Z
    .locals 1

    .prologue
    .line 1284
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1285
    const/4 v0, 0x1

    .line 1287
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTransitioningToShowAllApps()Z
    .locals 1

    .prologue
    .line 1277
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mTransitioningToAllApps:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mInMenu:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1278
    :cond_0
    const/4 v0, 0x1

    .line 1280
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public launchOptionWallpaper()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2609
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->isKnoxMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2610
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHomeView()Lcom/android/launcher2/HomeView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/HomeView;->startWallpaper(I)V

    .line 2614
    :goto_0
    return-void

    .line 2612
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHomeView()Lcom/android/launcher2/HomeView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/HomeView;->startWallpaper(I)V

    goto :goto_0
.end method

.method public menuWidgetsUpdated(Z)V
    .locals 3
    .param p1, "hasUninstallable"    # Z

    .prologue
    .line 3713
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/launcher2/Launcher;->isOptionMenuShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->isCurrentTabWidgets()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3714
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mOptionsMenu:Landroid/view/Menu;

    const v2, 0x7f0f00ed

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 3715
    .local v0, "uninstall":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->getMenuWidgets()Lcom/android/launcher2/MenuWidgets;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3716
    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 3720
    .end local v0    # "uninstall":Landroid/view/MenuItem;
    :cond_0
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3721
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/QuickViewWorkspace;->updateWidgetButtonState()V

    .line 3723
    :cond_1
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4260
    const/16 v6, 0x6f

    if-ne p1, v6, :cond_1

    .line 4262
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 4264
    .local v3, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    sget-object v4, Lcom/android/launcher2/Launcher;->mPackageTobeDisabled:Ljava/lang/String;

    const/16 v5, 0x2200

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 4267
    .local v0, "ainfo":Landroid/content/pm/ApplicationInfo;
    iget v4, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v4, v4, 0x80

    if-nez v4, :cond_0

    .line 4268
    sget-object v4, Lcom/android/launcher2/Launcher;->mPackageTobeDisabled:Ljava/lang/String;

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4292
    .end local v0    # "ainfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    :goto_0
    return-void

    .line 4271
    .restart local v3    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v2

    .line 4272
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "Launcher"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onActivityResult pkg:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/android/launcher2/Launcher;->mPackageTobeDisabled:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 4276
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    const/16 v6, 0xa

    if-ne p1, v6, :cond_3

    .line 4277
    sget-boolean v6, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v6, :cond_3

    .line 4278
    const/4 v6, -0x1

    if-ne p2, v6, :cond_2

    move v4, v5

    :cond_2
    invoke-virtual {p0, v4}, Lcom/android/launcher2/Launcher;->finishWallpaperGuide(Z)V

    .line 4281
    :cond_3
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mActivityCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/Launcher$ActivityResultCallback;

    .line 4282
    .local v1, "callback":Lcom/android/launcher2/Launcher$ActivityResultCallback;
    if-eqz v1, :cond_0

    .line 4283
    invoke-interface {v1, p1, p2, p3}, Lcom/android/launcher2/Launcher$ActivityResultCallback;->onActivityResult(IILandroid/content/Intent;)V

    .line 4284
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mActivityCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 4286
    sget-boolean v4, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v4, :cond_0

    .line 4287
    const/4 v4, 0x5

    if-ne p1, v4, :cond_0

    if-nez p2, :cond_0

    .line 4288
    invoke-virtual {p0, v5}, Lcom/android/launcher2/Launcher;->exitHelp(Z)V

    goto :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2214
    const-string v1, "Launcher"

    const-string v2, "onAttachedToWindow"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2215
    invoke-super {p0}, Landroid/app/ActivityGroup;->onAttachedToWindow()V

    .line 2218
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHotwordServiceClient:Lcom/google/android/hotword/client/HotwordServiceClient;

    invoke-virtual {v1}, Lcom/google/android/hotword/client/HotwordServiceClient;->onAttachedToWindow()V

    .line 2219
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->setEnableHotWord(Z)V

    .line 2223
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2224
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2225
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2227
    const-string v1, "android.intent.action.REQUEST_HOME_COUNT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2228
    const-string v1, "android.intent.action.REQUEST_WIDGET_COUNT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2232
    const-string v1, "com.android.launcher.action.ACTION_HOME_RESTORE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2234
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher2/Launcher;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2236
    iput-boolean v3, p0, Lcom/android/launcher2/Launcher;->mAttached:Z

    .line 2237
    sget-object v1, Lcom/android/launcher2/Launcher;->mPendingIntent:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 2238
    iget-boolean v1, p0, Lcom/android/launcher2/Launcher;->mRemainSavedInstance:Z

    if-eqz v1, :cond_0

    .line 2241
    sget-object v1, Lcom/android/launcher2/Launcher;->mPendingIntent:Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->onNewIntent(Landroid/content/Intent;)V

    .line 2244
    :cond_0
    const/4 v1, 0x0

    sput-object v1, Lcom/android/launcher2/Launcher;->mPendingIntent:Landroid/content/Intent;

    .line 2245
    iput-boolean v3, p0, Lcom/android/launcher2/Launcher;->mRemainSavedInstance:Z

    .line 2246
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2053
    sget-boolean v0, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v0, :cond_0

    .line 2056
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->handleResizeWidget()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2057
    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->exitHelp(Z)V

    .line 2064
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v0}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mTransitioningToAllApps:Z

    if-eqz v0, :cond_5

    .line 2065
    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v0}, Lcom/android/launcher2/MenuView;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2066
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->closeOptionsMenu()V

    .line 2067
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->exitAllApps()V

    .line 2073
    :cond_2
    :goto_1
    return-void

    .line 2059
    :cond_3
    sget-object v0, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v1, "add_widgets"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v1, "change_wallpaper"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2060
    :cond_4
    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->exitHelp(Z)V

    goto :goto_0

    .line 2071
    :cond_5
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->onBackPressed()Z

    goto :goto_1
.end method

.method public onClickAppMarketButton()V
    .locals 2

    .prologue
    .line 3143
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mAppMarketIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 3144
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mAppMarketIntent:Landroid/content/Intent;

    const-string v1, "app market"

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z

    .line 3146
    :cond_0
    return-void
.end method

.method public onClickAppMarketButton(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 3134
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->onClickAppMarketButton()V

    .line 3135
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 3140
    :goto_0
    return-void

    .line 3137
    :pswitch_0
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    sget-object v1, Lcom/android/launcher2/MenuAppsGrid$State;->NORMAL:Lcom/android/launcher2/MenuAppsGrid$State;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/MenuView;->changeState(Lcom/android/launcher2/MenuAppsGrid$State;)V

    goto :goto_0

    .line 3135
    nop

    :pswitch_data_0
    .packed-switch 0x7f0f009c
        :pswitch_0
    .end packed-switch
.end method

.method public onClickDownloadedButton(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 3149
    sget-object v0, Lcom/sec/dtl/launcher/Talk;->INSTANCE:Lcom/sec/dtl/launcher/Talk;

    const/16 v1, 0x20

    const v2, 0x7f100035

    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/dtl/launcher/Talk;->sayByTalkback(ILjava/lang/String;Z)V

    .line 3150
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v0}, Lcom/android/launcher2/MenuView;->onOptionSelectedDownload()V

    .line 3151
    return-void
.end method

.method public onClickPopupMenuButton(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2841
    sget-boolean v0, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v0, :cond_1

    .line 2855
    :cond_0
    :goto_0
    return-void

    .line 2843
    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/android/launcher2/Launcher;->sPopupMenuShowing:Z

    if-eqz v0, :cond_2

    .line 2844
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->closePopupMenu()V

    goto :goto_0

    .line 2847
    :cond_2
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    iget-boolean v0, v0, Lcom/android/launcher2/MenuView;->mMenuAppLoaded:Z

    if-eqz v0, :cond_0

    .line 2850
    :cond_4
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Launcher;->createPopupMenu(Landroid/view/View;)V

    .line 2852
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    invoke-virtual {v0}, Lcom/android/launcher2/popup/PopupMenu;->show()V

    .line 2853
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/launcher2/Launcher;->sPopupMenuShowing:Z

    goto :goto_0
.end method

.method public onClickonlineHelp()V
    .locals 7

    .prologue
    .line 3194
    :try_start_0
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.samsung.helphub"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 3195
    .local v2, "info":Landroid/content/pm/PackageInfo;
    iget v4, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v1, v4, 0xa

    .line 3196
    .local v1, "helpVersionCode":I
    const/4 v4, 0x1

    if-ne v1, v4, :cond_1

    .line 3227
    .end local v1    # "helpVersionCode":I
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return-void

    .line 3201
    .restart local v1    # "helpVersionCode":I
    .restart local v2    # "info":Landroid/content/pm/PackageInfo;
    :cond_1
    const/4 v4, 0x2

    if-ne v1, v4, :cond_2

    .line 3206
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.samsung.helphub.HELP"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3207
    .local v3, "intent":Landroid/content/Intent;
    const-string v4, "helphub:section"

    const-string v5, "homescreen"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3208
    invoke-virtual {p0, v3}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3222
    .end local v1    # "helpVersionCode":I
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    .end local v3    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 3224
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 3210
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1    # "helpVersionCode":I
    .restart local v2    # "info":Landroid/content/pm/PackageInfo;
    :cond_2
    const/4 v4, 0x3

    if-ne v1, v4, :cond_0

    .line 3218
    :try_start_1
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.samsung.helphub.HELP"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3219
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v4, "helphub:appid"

    const-string v5, "home_screen"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3220
    invoke-virtual {p0, v3}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public onClickstartDownloadableApps()V
    .locals 7

    .prologue
    const v6, 0x7f100002

    const/4 v5, 0x0

    .line 3163
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 3164
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/android/launcher2/Launcher;->readSalesCode()Ljava/lang/String;

    move-result-object v2

    .line 3165
    .local v2, "salesCode":Ljava/lang/String;
    const-string v3, "CHM"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3166
    const-string v3, "com.sec.android.app.moreservices"

    const-string v4, "com.sec.android.app.moreservices.moreservices"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3170
    :goto_0
    const v3, 0x14000020

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3173
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 3189
    :cond_0
    :goto_1
    return-void

    .line 3168
    :cond_1
    const-string v3, "com.sec.android.app.samsungapps"

    const-string v4, "com.sec.android.app.samsungapps.downloadableapps.DownloadableAppsActivity"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 3174
    :catch_0
    move-exception v0

    .line 3175
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-static {p0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 3176
    sget-boolean v3, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v3, :cond_0

    .line 3177
    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to launch. tag=samsungapps downloadable page intent="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 3179
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :catch_1
    move-exception v0

    .line 3180
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-static {p0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 3181
    sget-boolean v3, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v3, :cond_0

    .line 3182
    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Launcher does not have the permission to launch "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". Make sure to create a MAIN intent-filter for the corresponding activity "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "or use the exported attribute for this activity. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "tag="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "samsungapps downloadable page"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " intent="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 8
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 3997
    const-string v5, "Launcher"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onConfigurationChanged. orientation: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3999
    sget v5, Lcom/android/launcher2/Launcher;->sActivityOrientation:I

    iget v6, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v5, v6, :cond_1

    .line 4001
    invoke-static {}, Lcom/android/launcher2/SurfaceWidgetView;->isRotationEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v5

    if-nez v5, :cond_1

    .line 4002
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v5}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v2

    .line 4003
    .local v2, "currentPage":I
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v5}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 4004
    .local v0, "cl":Lcom/android/launcher2/CellLayout;
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getChildrenLayout()Lcom/android/launcher2/CellLayoutChildren;

    move-result-object v1

    .line 4005
    .local v1, "clChildren":Lcom/android/launcher2/CellLayoutChildren;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_0
    invoke-virtual {v1}, Lcom/android/launcher2/CellLayoutChildren;->getChildCount()I

    move-result v5

    if-ge v3, v5, :cond_1

    .line 4006
    invoke-virtual {v1, v3}, Lcom/android/launcher2/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 4007
    .local v4, "v":Landroid/view/View;
    instance-of v5, v4, Lcom/android/launcher2/SurfaceWidgetView;

    if-eqz v5, :cond_0

    .line 4008
    check-cast v4, Lcom/android/launcher2/SurfaceWidgetView;

    .end local v4    # "v":Landroid/view/View;
    invoke-virtual {v4}, Lcom/android/launcher2/SurfaceWidgetView;->orientationChange()V

    .line 4005
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 4012
    .end local v0    # "cl":Lcom/android/launcher2/CellLayout;
    .end local v1    # "clChildren":Lcom/android/launcher2/CellLayoutChildren;
    .end local v2    # "currentPage":I
    .end local v3    # "j":I
    :cond_1
    iget v5, p1, Landroid/content/res/Configuration;->orientation:I

    sput v5, Lcom/android/launcher2/Launcher;->sActivityOrientation:I

    .line 4013
    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 4014
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 36
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 401
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v12

    check-cast v12, Lcom/android/launcher2/LauncherApplication;

    .line 402
    .local v12, "app":Lcom/android/launcher2/LauncherApplication;
    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Lcom/android/launcher2/LauncherApplication;->setLauncher(Lcom/android/launcher2/Launcher;)V

    .line 403
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.feature.cocktailbar"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 404
    const/4 v3, 0x1

    sput-boolean v3, Lcom/android/launcher2/Launcher;->is_TB:Z

    .line 405
    :cond_0
    const-string v4, "Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCreate. savedInstanceState: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez p1, :cond_14

    const-string v3, "null"

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", Launcher: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {p0 .. p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    invoke-super/range {p0 .. p1}, Landroid/app/ActivityGroup;->onCreate(Landroid/os/Bundle;)V

    .line 408
    if-eqz p1, :cond_15

    const/4 v3, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/launcher2/Launcher;->mRemainSavedInstance:Z

    .line 410
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    .line 411
    .local v27, "res":Landroid/content/res/Resources;
    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "config.densityDpi : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v27 .. v27}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    sget v3, Lcom/android/launcher2/LauncherApplication;->sDensityDpi:I

    if-lez v3, :cond_1

    .line 414
    sget v3, Lcom/android/launcher2/LauncherApplication;->sDensityDpi:I

    invoke-virtual/range {v27 .. v27}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    if-eq v3, v4, :cond_1

    .line 415
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3}, Landroid/os/Process;->killProcess(I)V

    .line 418
    :cond_1
    invoke-virtual/range {v27 .. v27}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    sput v3, Lcom/android/launcher2/LauncherApplication;->sDensityDpi:I

    .line 420
    invoke-virtual {v12}, Lcom/android/launcher2/LauncherApplication;->getSpanCalculator()Lcom/android/launcher2/WorkspaceSpanCalculator;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    .line 422
    const/high16 v3, 0x7f0d0000

    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v3

    sput v3, Lcom/android/launcher2/Launcher;->sMenuBgDarkenAmountNormal:F

    .line 424
    invoke-static/range {v27 .. v27}, Lcom/android/launcher2/FolderIconHelper;->initFolderResources(Landroid/content/res/Resources;)V

    .line 426
    invoke-virtual/range {v27 .. v27}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    sput v3, Lcom/android/launcher2/Launcher;->sActivityOrientation:I

    .line 428
    sget-boolean v3, Lcom/android/launcher2/Launcher;->UseLauncherHighPriority:Z

    if-eqz v3, :cond_2

    .line 429
    const/4 v3, -0x4

    invoke-static {v3}, Landroid/os/Process;->setThreadPriority(I)V

    .line 433
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->isFactorySim()Z

    move-result v3

    if-nez v3, :cond_18

    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->isFactoryMode()Z

    move-result v3

    if-nez v3, :cond_18

    .line 435
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "device_provisioned"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v10

    .line 436
    .local v10, "IsProvisioned":Z
    if-eqz v10, :cond_16

    .line 437
    const-string v3, "Launcher"

    const-string v4, "Set device_provisioned: 1"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "device_provisioned"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 440
    const-string v3, "Launcher"

    const-string v4, "Confirm device_provisioned: 1"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    .end local v10    # "IsProvisioned":Z
    :cond_3
    :goto_2
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "user_setup_complete"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v9

    .line 451
    .local v9, "IsCompleteUserSetup":Z
    if-eqz v9, :cond_17

    .line 452
    const-string v3, "Launcher"

    const-string v4, "Set user_setup_complete: 1"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "user_setup_complete"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 455
    const-string v3, "Launcher"

    const-string v4, "Confirm user_setup_complete: 1"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 474
    .end local v9    # "IsCompleteUserSetup":Z
    :cond_4
    :goto_3
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_Launcher_ReplaceHotseatAreaInEditMode"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 475
    const/4 v3, 0x1

    sput-boolean v3, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    .line 476
    const/4 v3, 0x1

    sput-boolean v3, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_REPLACEHOTSEATAREAINEDITMODE:Z

    .line 487
    :cond_5
    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DYNAMICCSCFEATURE="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_DYNAMICCSCFEATURE:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    sget-boolean v3, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_DYNAMICCSCFEATURE:Z

    if-eqz v3, :cond_6

    .line 489
    if-nez p1, :cond_6

    .line 490
    new-instance v20, Landroid/content/Intent;

    const-string v3, "android.intent.action.CSC_UPDATE_LAUNCHER_READY"

    move-object/from16 v0, v20

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 491
    .local v20, "launcherReady":Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    .line 492
    const-string v3, "DYNAMIC_CSC"

    const-string v4, "Launcher Ready intent BroadCasted"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    .end local v20    # "launcherReady":Landroid/content/Intent;
    :cond_6
    const/4 v3, 0x0

    sput-boolean v3, Lcom/android/launcher2/Launcher;->SEC_PRODUCT_FEATURE_LAUNCHER_KITKAT_ADD_TO_HOMESCREEN_CONCEPT:Z

    .line 510
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/launcher2/LauncherApplication;->getThemeLoader()Lcom/android/launcher2/ThemeLoader;

    move-result-object v21

    .line 511
    .local v21, "loader":Lcom/android/launcher2/ThemeLoader;
    invoke-virtual/range {v21 .. v21}, Lcom/android/launcher2/ThemeLoader;->isSupportTheme()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 512
    const/4 v3, 0x1

    sput-boolean v3, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_THEME_ENABLE:Z

    .line 515
    :cond_7
    sget-boolean v3, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v3, :cond_8

    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "HOMESCREENEDITMODE="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " REPLACEHOTSEATAREAINEDITMODE="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_REPLACEHOTSEATAREAINEDITMODE:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->keyboard:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_19

    const/4 v3, 0x1

    :goto_4
    sput-boolean v3, Lcom/android/launcher2/Launcher;->UseQwertyKeypad:Z

    .line 518
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->hasMenuKey()Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/launcher2/Launcher;->mHasMenuKey:Z

    .line 520
    invoke-virtual {v12}, Lcom/android/launcher2/LauncherApplication;->getModel()Lcom/android/launcher2/LauncherModel;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mModel:Lcom/android/launcher2/LauncherModel;

    .line 521
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mModel:Lcom/android/launcher2/LauncherModel;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/android/launcher2/LauncherModel;->initialize(Lcom/android/launcher2/LauncherModel$Callbacks;)V

    .line 522
    invoke-virtual {v12}, Lcom/android/launcher2/LauncherApplication;->getPkgResCache()Lcom/android/launcher2/PkgResCache;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mPkgResCache:Lcom/android/launcher2/PkgResCache;

    .line 523
    invoke-virtual {v12}, Lcom/android/launcher2/LauncherApplication;->getPkgResCacheForMenu()Lcom/android/launcher2/PkgResCache;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mPkgResCacheForMenu:Lcom/android/launcher2/PkgResCache;

    .line 526
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mModel:Lcom/android/launcher2/LauncherModel;

    invoke-virtual {v3}, Lcom/android/launcher2/LauncherModel;->hasLocaleChanged()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isMenuIconSizeChanged()Z

    move-result v3

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mPkgResCacheForMenu:Lcom/android/launcher2/PkgResCache;

    if-eqz v3, :cond_9

    .line 527
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mPkgResCacheForMenu:Lcom/android/launcher2/PkgResCache;

    invoke-virtual {v3}, Lcom/android/launcher2/PkgResCache;->clear()V

    .line 530
    :cond_9
    const-string v3, "com.sec.android.app.launcher.prefs"

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/android/launcher2/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v26

    .line 531
    .local v26, "prefs":Landroid/content/SharedPreferences;
    const-string v3, "SHOW_EMPTY_PAGE_MSG"

    const/4 v4, 0x1

    move-object/from16 v0, v26

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/launcher2/Launcher;->mShowEmptyPageMsg:Z

    .line 533
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v25

    .line 535
    .local v25, "pref":Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isTabletLayout()Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "ALTIUS"

    const-string v4, "KNIGHT"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "HERA"

    const-string v4, "KNIGHT"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 537
    :cond_a
    const/4 v3, 0x0

    sput-boolean v3, Lcom/android/launcher2/Launcher;->sIsHeadlinesAppEnable:Z

    .line 557
    :goto_5
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isTabletLayout()Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 558
    const/4 v3, 0x0

    sput v3, Lcom/android/launcher2/Launcher;->sWhichTransitionEffect:I

    .line 565
    :goto_6
    const v3, 0x7f030030

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->setContentView(I)V

    .line 567
    sget-object v3, Lcom/android/launcher2/SamsungWidgetPackageManager;->INSTANCE:Lcom/android/launcher2/SamsungWidgetPackageManager;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mSamsungWidgetPackageManager:Lcom/android/launcher2/SamsungWidgetPackageManager;

    .line 568
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mSamsungWidgetPackageManager:Lcom/android/launcher2/SamsungWidgetPackageManager;

    invoke-virtual {v3}, Lcom/android/launcher2/SamsungWidgetPackageManager;->loadIfNeeded()V

    .line 570
    sget-object v3, Lcom/android/launcher2/SurfaceWidgetPackageManager;->INST:Lcom/android/launcher2/SurfaceWidgetPackageManager;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mSurfaceWidgetPackageManager:Lcom/android/launcher2/SurfaceWidgetPackageManager;

    .line 571
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mSurfaceWidgetPackageManager:Lcom/android/launcher2/SurfaceWidgetPackageManager;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/android/launcher2/SurfaceWidgetPackageManager;->loadIfNeeded(Landroid/content/Context;)V

    .line 573
    const v3, 0x7f0f0077

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/HomeView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    .line 574
    const v3, 0x7f0f00aa

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/MenuView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    .line 575
    const v3, 0x7f0f008f

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mDarkenView:Landroid/view/View;

    .line 577
    const v3, 0x7f10002f

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 578
    .local v22, "optionsMenu":Ljava/lang/String;
    const-string v3, "layout_inflater"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/LayoutInflater;

    .line 579
    .local v18, "inflate":Landroid/view/LayoutInflater;
    const/16 v32, 0x0

    .line 580
    .local v32, "v":Landroid/view/View;
    const v3, 0x7f030055

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v32

    .line 582
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v33

    .line 583
    .local v33, "viewToast":Landroid/widget/Toast;
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 584
    const v3, 0x102000b

    move-object/from16 v0, v32

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    .line 585
    .local v31, "tv":Landroid/widget/TextView;
    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 587
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    const v4, 0x7f0f00ae

    invoke-virtual {v3, v4}, Lcom/android/launcher2/MenuView;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/FrameLayout;

    .line 588
    .local v24, "popup_icon_layout":Landroid/widget/FrameLayout;
    if-eqz v24, :cond_b

    .line 589
    const v3, 0x7f0f00c4

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/ImageView;

    .line 590
    .local v23, "popup_icon":Landroid/widget/ImageView;
    if-eqz v23, :cond_b

    .line 591
    new-instance v3, Lcom/android/launcher2/Launcher$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v3, v0, v1}, Lcom/android/launcher2/Launcher$1;-><init>(Lcom/android/launcher2/Launcher;Landroid/widget/Toast;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 602
    invoke-virtual/range {v23 .. v23}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    if-eqz v3, :cond_b

    .line 603
    const/4 v3, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 604
    invoke-virtual/range {v23 .. v23}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Landroid/widget/HoverPopupWindow;->setContent(Ljava/lang/CharSequence;)V

    .line 605
    invoke-virtual/range {v23 .. v23}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    const/16 v4, 0x3035

    invoke-virtual {v3, v4}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 606
    invoke-virtual/range {v23 .. v23}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    const v4, 0x7f0e0018

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    const v5, 0x7f0e0019

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 608
    invoke-virtual/range {v23 .. v23}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 613
    .end local v23    # "popup_icon":Landroid/widget/ImageView;
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v30

    .line 614
    .local v30, "transaction":Landroid/app/FragmentTransaction;
    if-eqz p1, :cond_1e

    .line 615
    const-string v3, "extra_home_view_hidden"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v34

    .line 616
    .local v34, "wasHomeHidden":Z
    if-eqz v34, :cond_1d

    .line 617
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/android/launcher2/MenuView;->setVisibility(I)V

    .line 618
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/launcher2/Launcher;->mInMenu:Z

    .line 619
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/android/launcher2/HomeView;->setVisibility(I)V

    .line 620
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mDarkenView:Landroid/view/View;

    sget v4, Lcom/android/launcher2/Launcher;->sMenuBgDarkenAmountNormal:F

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    .line 631
    .end local v34    # "wasHomeHidden":Z
    :goto_7
    invoke-virtual/range {v30 .. v30}, Landroid/app/FragmentTransaction;->commit()I

    .line 634
    sget v3, Lcom/android/launcher2/Launcher;->sWhichTransitionEffect:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->setWhichTransitionEffect(I)V

    .line 636
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/launcher2/Launcher;->mRestoring:Z

    if-nez v3, :cond_c

    .line 637
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mModel:Lcom/android/launcher2/LauncherModel;

    sget-boolean v4, Lcom/android/launcher2/Launcher;->sIsSecretModeOn:Z

    invoke-virtual {v3, v4}, Lcom/android/launcher2/LauncherModel;->startLoader(Z)V

    .line 639
    :cond_c
    new-instance v15, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v15, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 640
    .local v15, "filter":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v15}, Lcom/android/launcher2/Launcher;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 641
    new-instance v29, Landroid/content/IntentFilter;

    const-string v3, "com.android.settings.action.talkback_off"

    move-object/from16 v0, v29

    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 642
    .local v29, "talkbackFilter":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mTalkbackEnableReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v3, v1}, Lcom/android/launcher2/Launcher;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 645
    invoke-static/range {p0 .. p0}, Lcom/android/launcher2/BadgeCountReceiver;->initialize(Landroid/content/Context;)Lcom/android/launcher2/BadgeCountReceiver;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mBadgeCountChangedReceiver:Lcom/android/launcher2/BadgeCountReceiver;

    .line 648
    const v3, 0x7f0b0009

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-nez v3, :cond_d

    .line 649
    new-instance v3, Lcom/android/launcher2/ChangeCallAppReceiver;

    invoke-direct {v3}, Lcom/android/launcher2/ChangeCallAppReceiver;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mChangeCallAppReceiver:Lcom/android/launcher2/ChangeCallAppReceiver;

    .line 650
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mChangeCallAppReceiver:Lcom/android/launcher2/ChangeCallAppReceiver;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/ChangeCallAppReceiver;->init(Lcom/android/launcher2/HomeView;)V

    .line 660
    :cond_d
    sget-boolean v3, Lcom/android/launcher2/LauncherApplication;->sFestivalPageLauncher:Z

    if-eqz v3, :cond_e

    .line 661
    new-instance v3, Lcom/android/launcher2/FestivalPageManager;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/android/launcher2/FestivalPageManager;-><init>(Lcom/android/launcher2/Launcher;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mFestivalPageManager:Lcom/android/launcher2/FestivalPageManager;

    .line 662
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mFestivalPageManager:Lcom/android/launcher2/FestivalPageManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/FestivalPageManager;->initFestivalPage(Lcom/android/launcher2/HomeView;)V

    .line 666
    :cond_e
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    .line 667
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 670
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "android.intent.category.APP_MARKET"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    .line 673
    .local v19, "intent":Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mActivityName:Landroid/content/ComponentName;

    .line 674
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mActivityName:Landroid/content/ComponentName;

    if-eqz v3, :cond_f

    .line 675
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/launcher2/Launcher;->mAppMarketIntent:Landroid/content/Intent;

    .line 678
    :cond_f
    const v3, 0x7f0f008d

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/AnimationLayer;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mAnimationLayer:Lcom/android/launcher2/AnimationLayer;

    .line 680
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->setIndicatorTransparency()V

    .line 684
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->setSystemUiTransparency(Z)V

    .line 687
    sget-boolean v3, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v3, :cond_1f

    .line 689
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/android/launcher2/Launcher;->setHomeEditMode(Z)V

    .line 690
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/android/launcher2/Launcher;->setHomeRemoveMode(Z)V

    .line 697
    :goto_8
    invoke-virtual/range {v27 .. v27}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v4, 0x258

    if-lt v3, v4, :cond_20

    .line 698
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->setRequestedOrientation(I)V

    .line 703
    :goto_9
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "android.intent.action.MAIN"

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/android/launcher2/Launcher;->tryQueueWidgetAddViaIntent(Landroid/content/Intent;Ljava/lang/String;)Z

    .line 705
    sget-boolean v3, Lcom/android/launcher2/Launcher;->isSystemAppDisable:Z

    if-eqz v3, :cond_10

    .line 706
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->addNonDisableAppToList()V

    .line 707
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->addNonDisableAppToListCsc()V

    .line 710
    :cond_10
    sget v3, Lcom/android/launcher2/LauncherApplication;->RemovablePreloadEnabled:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_11

    .line 711
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->addRemovablePreloadAppToList()V

    .line 712
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->addRemovableCustomerAppToList()V

    .line 713
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->addRemovableAppToList()V

    .line 717
    :cond_11
    const-string v3, "enterprise_policy"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/EnterpriseDeviceManager;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mEnterpriseDeviceManager:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 719
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mEnterpriseDeviceManager:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-eqz v3, :cond_12

    .line 720
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mEnterpriseDeviceManager:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mApplicationPolicy:Landroid/app/enterprise/ApplicationPolicy;

    .line 724
    :cond_12
    new-instance v3, Lcom/google/android/hotword/client/HotwordServiceClient;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/hotword/client/HotwordServiceClient;-><init>(Landroid/app/Activity;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher2/Launcher;->mHotwordServiceClient:Lcom/google/android/hotword/client/HotwordServiceClient;

    .line 726
    sget-boolean v3, Lcom/android/launcher2/LauncherApplication;->sIsTheFisrt:Z

    if-eqz v3, :cond_13

    .line 727
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v3

    const-string v4, "HOME"

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v6}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v6

    int-to-long v6, v6

    const/4 v8, 0x1

    invoke-virtual/range {v3 .. v8}, Lcom/android/launcher2/LauncherApplication;->insertLog(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 728
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 731
    .local v2, "cr":Landroid/content/ContentResolver;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->content_uri:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v13

    .line 736
    .local v13, "cursor":Landroid/database/Cursor;
    :goto_a
    if-eqz v13, :cond_27

    .line 737
    :goto_b
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_22

    .line 738
    const-string v3, "isDSPEnabled"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 739
    .local v28, "s":Ljava/lang/String;
    const-string v3, "true"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 740
    const/4 v3, 0x0

    sput-boolean v3, Lcom/android/launcher2/Launcher;->mAlwaysMicOn:Z

    goto :goto_b

    .line 405
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v13    # "cursor":Landroid/database/Cursor;
    .end local v15    # "filter":Landroid/content/IntentFilter;
    .end local v18    # "inflate":Landroid/view/LayoutInflater;
    .end local v19    # "intent":Landroid/content/Intent;
    .end local v21    # "loader":Lcom/android/launcher2/ThemeLoader;
    .end local v22    # "optionsMenu":Ljava/lang/String;
    .end local v24    # "popup_icon_layout":Landroid/widget/FrameLayout;
    .end local v25    # "pref":Landroid/content/SharedPreferences;
    .end local v26    # "prefs":Landroid/content/SharedPreferences;
    .end local v27    # "res":Landroid/content/res/Resources;
    .end local v28    # "s":Ljava/lang/String;
    .end local v29    # "talkbackFilter":Landroid/content/IntentFilter;
    .end local v30    # "transaction":Landroid/app/FragmentTransaction;
    .end local v31    # "tv":Landroid/widget/TextView;
    .end local v32    # "v":Landroid/view/View;
    .end local v33    # "viewToast":Landroid/widget/Toast;
    :cond_14
    const-string v3, "not null"

    goto/16 :goto_0

    .line 408
    :cond_15
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 443
    .restart local v10    # "IsProvisioned":Z
    .restart local v27    # "res":Landroid/content/res/Resources;
    :cond_16
    :try_start_3
    const-string v3, "Launcher"

    const-string v4, "database error"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_2

    .line 445
    .end local v10    # "IsProvisioned":Z
    :catch_0
    move-exception v14

    .line 446
    .local v14, "e":Ljava/lang/Exception;
    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error Setting device_provisioned: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v14}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 458
    .end local v14    # "e":Ljava/lang/Exception;
    .restart local v9    # "IsCompleteUserSetup":Z
    :cond_17
    :try_start_4
    const-string v3, "Launcher"

    const-string v4, "database error"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_3

    .line 460
    .end local v9    # "IsCompleteUserSetup":Z
    :catch_1
    move-exception v14

    .line 461
    .restart local v14    # "e":Ljava/lang/Exception;
    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error Setting user_setup_complete: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v14}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 465
    .end local v14    # "e":Ljava/lang/Exception;
    :cond_18
    const-string v3, "Launcher"

    const-string v4, "FactoryMode"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 517
    .restart local v21    # "loader":Lcom/android/launcher2/ThemeLoader;
    :cond_19
    const/4 v3, 0x0

    goto/16 :goto_4

    .line 538
    .restart local v25    # "pref":Landroid/content/SharedPreferences;
    .restart local v26    # "prefs":Landroid/content/SharedPreferences;
    :cond_1a
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isOwner()Z

    move-result v3

    if-eqz v3, :cond_1b

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/launcher2/LauncherApplication;->isKnoxMode()Z

    move-result v3

    if-nez v3, :cond_1b

    .line 539
    const-string v3, "pref_my_magazine"

    const/4 v4, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    sput-boolean v3, Lcom/android/launcher2/Launcher;->sIsHeadlinesAppEnable:Z

    goto/16 :goto_5

    .line 541
    :cond_1b
    const/4 v3, 0x0

    sput-boolean v3, Lcom/android/launcher2/Launcher;->sIsHeadlinesAppEnable:Z

    goto/16 :goto_5

    .line 560
    :cond_1c
    const-string v3, "pref_list_transition_effect"

    const-string v4, "1"

    move-object/from16 v0, v25

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 562
    .local v35, "whichTransitionEffect":Ljava/lang/String;
    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sput v3, Lcom/android/launcher2/Launcher;->sWhichTransitionEffect:I

    goto/16 :goto_6

    .line 622
    .end local v35    # "whichTransitionEffect":Ljava/lang/String;
    .restart local v18    # "inflate":Landroid/view/LayoutInflater;
    .restart local v22    # "optionsMenu":Ljava/lang/String;
    .restart local v24    # "popup_icon_layout":Landroid/widget/FrameLayout;
    .restart local v30    # "transaction":Landroid/app/FragmentTransaction;
    .restart local v31    # "tv":Landroid/widget/TextView;
    .restart local v32    # "v":Landroid/view/View;
    .restart local v33    # "viewToast":Landroid/widget/Toast;
    .restart local v34    # "wasHomeHidden":Z
    :cond_1d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/android/launcher2/HomeView;->setVisibility(I)V

    .line 623
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/android/launcher2/MenuView;->setVisibility(I)V

    .line 624
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mDarkenView:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_7

    .line 628
    .end local v34    # "wasHomeHidden":Z
    :cond_1e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/android/launcher2/HomeView;->setVisibility(I)V

    .line 629
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/android/launcher2/MenuView;->setVisibility(I)V

    goto/16 :goto_7

    .line 692
    .restart local v15    # "filter":Landroid/content/IntentFilter;
    .restart local v19    # "intent":Landroid/content/Intent;
    .restart local v29    # "talkbackFilter":Landroid/content/IntentFilter;
    :cond_1f
    const/4 v3, 0x0

    sput-boolean v3, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_REPLACEHOTSEATAREAINEDITMODE:Z

    .line 693
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/android/launcher2/Launcher;->setHomeEditMode(Z)V

    .line 694
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/android/launcher2/Launcher;->setHomeRemoveMode(Z)V

    goto/16 :goto_8

    .line 700
    :cond_20
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->setRequestedOrientation(I)V

    goto/16 :goto_9

    .line 732
    .restart local v2    # "cr":Landroid/content/ContentResolver;
    :catch_2
    move-exception v14

    .line 733
    .restart local v14    # "e":Ljava/lang/Exception;
    const/4 v13, 0x0

    .restart local v13    # "cursor":Landroid/database/Cursor;
    goto/16 :goto_a

    .line 742
    .end local v14    # "e":Ljava/lang/Exception;
    .restart local v28    # "s":Ljava/lang/String;
    :cond_21
    const/4 v3, 0x1

    sput-boolean v3, Lcom/android/launcher2/Launcher;->mAlwaysMicOn:Z

    goto/16 :goto_b

    .line 744
    .end local v28    # "s":Ljava/lang/String;
    :cond_22
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 748
    :goto_c
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "isBabyCryingEnable"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    sput v3, Lcom/android/launcher2/Launcher;->mIsBabyCryingEnable:I

    .line 749
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "isDoorbellEnable"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    sput v3, Lcom/android/launcher2/Launcher;->mIsDoorbellEnable:I

    .line 750
    sget-object v3, Lcom/sec/dtl/launcher/Talk;->INSTANCE:Lcom/sec/dtl/launcher/Talk;

    invoke-virtual {v3}, Lcom/sec/dtl/launcher/Talk;->checkTalkbackEnabled()Z

    move-result v3

    sput-boolean v3, Lcom/android/launcher2/Launcher;->mTalkbackEnable:Z

    .line 752
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->content_uri:Landroid/net/Uri;

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Launcher;->mAlwaysMicOnObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 753
    const-string v3, "isBabyCryingEnable"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Launcher;->mBabyCryingDetector:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 754
    const-string v3, "isDoorbellEnable"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Launcher;->mDoorbellDetector:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 756
    new-instance v17, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getIntent()Landroid/content/Intent;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 757
    .local v17, "helpintent":Landroid/content/Intent;
    invoke-virtual/range {v17 .. v17}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_24

    const-string v3, "homescreen:guide_mode"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_24

    sget-boolean v3, Lcom/android/launcher2/Launcher;->isExitingFromHelp:Z

    if-nez v3, :cond_24

    .line 759
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getRequestedOrientation()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/launcher2/Launcher;->mPrevOrientationHelp:I

    .line 761
    const-string v3, "help_activity_type"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 762
    .local v16, "helpActivityName":Ljava/lang/String;
    const-string v3, "ExternalHelpActivity"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_23

    .line 763
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/launcher2/Launcher;->mIsExternalHelpActivity:Z

    .line 764
    :cond_23
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/launcher2/guide/GuideFragment;->deployGuide(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 765
    const/4 v3, 0x1

    sput-boolean v3, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    .line 767
    .end local v16    # "helpActivityName":Ljava/lang/String;
    :cond_24
    const-string v3, "ALTIUS"

    const-string v4, "KNIGHT"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_25

    .line 768
    const/4 v3, 0x1

    sput-boolean v3, Lcom/android/launcher2/Launcher;->bSupportAirMove:Z

    .line 770
    :cond_25
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_COMMON_GESTURE_WITH_IRSENSOR"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_26

    sget-boolean v3, Lcom/android/launcher2/Launcher;->bSupportAirMove:Z

    if-eqz v3, :cond_26

    .line 772
    new-instance v11, Landroid/content/IntentFilter;

    invoke-direct {v11}, Landroid/content/IntentFilter;-><init>()V

    .line 773
    .local v11, "airGestureFilter":Landroid/content/IntentFilter;
    const-string v3, "com.sec.gesture.AIR_MOTION_SETTINGS_CHANGED"

    invoke-virtual {v11, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 774
    const-string v3, "com.sec.gesture.AIR_MOVE_SETTINGS_CHANGED"

    invoke-virtual {v11, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 775
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mAirGestureSettingsChangeReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v11}, Lcom/android/launcher2/Launcher;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 778
    .end local v11    # "airGestureFilter":Landroid/content/IntentFilter;
    :cond_26
    return-void

    .line 746
    .end local v17    # "helpintent":Landroid/content/Intent;
    :cond_27
    const/4 v3, 0x1

    sput-boolean v3, Lcom/android/launcher2/Launcher;->mAlwaysMicOn:Z

    goto/16 :goto_c
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1583
    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 1584
    iget-boolean v1, p0, Lcom/android/launcher2/Launcher;->mHasMenuKey:Z

    if-eqz v1, :cond_0

    .line 1586
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 1587
    .local v0, "menuInflater":Landroid/view/MenuInflater;
    const/high16 v1, 0x7f120000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1589
    .end local v0    # "menuInflater":Landroid/view/MenuInflater;
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 1176
    sget-boolean v4, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v4, :cond_0

    .line 1177
    invoke-virtual {p0, v7}, Lcom/android/launcher2/Launcher;->exitHelp(Z)V

    .line 1179
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1180
    .local v1, "cr":Landroid/content/ContentResolver;
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mAlwaysMicOnObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v4}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1181
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mBabyCryingDetector:Landroid/database/ContentObserver;

    invoke-virtual {v1, v4}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1182
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mDoorbellDetector:Landroid/database/ContentObserver;

    invoke-virtual {v1, v4}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1184
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v4}, Lcom/android/launcher2/HomeView;->onDestroy()V

    .line 1189
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/launcher2/Launcher;->mIsDestroyed:Z

    .line 1190
    const-string v4, "Launcher"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onDestroy, Launcher: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1191
    invoke-super {p0}, Landroid/app/ActivityGroup;->onDestroy()V

    .line 1192
    invoke-static {}, Lcom/android/launcher2/Utilities;->onDestroy()V

    .line 1198
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    .line 1199
    .local v0, "app":Lcom/android/launcher2/LauncherApplication;
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mModel:Lcom/android/launcher2/LauncherModel;

    invoke-virtual {v4}, Lcom/android/launcher2/LauncherModel;->stopLoader()V

    .line 1200
    invoke-virtual {v0, v8}, Lcom/android/launcher2/LauncherApplication;->setLauncher(Lcom/android/launcher2/Launcher;)V

    .line 1202
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4}, Lcom/android/launcher2/Launcher;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1203
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mTalkbackEnableReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4}, Lcom/android/launcher2/Launcher;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1205
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    if-eqz v4, :cond_1

    .line 1207
    sget-boolean v3, Lcom/android/launcher2/Launcher;->sPopupMenuShowing:Z

    .line 1208
    .local v3, "tempPopupStatus":Z
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    invoke-virtual {v4}, Lcom/android/launcher2/popup/PopupMenu;->dismiss()V

    .line 1209
    iput-object v8, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    .line 1210
    sput-boolean v3, Lcom/android/launcher2/Launcher;->sPopupMenuShowing:Z

    .line 1213
    .end local v3    # "tempPopupStatus":Z
    :cond_1
    sget-object v4, Lcom/sec/dtl/launcher/Talk;->INSTANCE:Lcom/sec/dtl/launcher/Talk;

    invoke-virtual {v4}, Lcom/sec/dtl/launcher/Talk;->shutdown()V

    .line 1216
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->mBadgeCountChangedReceiver:Lcom/android/launcher2/BadgeCountReceiver;

    invoke-virtual {v4}, Lcom/android/launcher2/BadgeCountReceiver;->unregister()V

    .line 1219
    iget-boolean v4, p0, Lcom/android/launcher2/Launcher;->mAutoRestart:Z

    if-eqz v4, :cond_2

    .line 1220
    iput-boolean v7, p0, Lcom/android/launcher2/Launcher;->mAutoRestart:Z

    .line 1221
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/android/launcher2/Launcher;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1222
    .local v2, "home":Landroid/content/Intent;
    const/high16 v4, 0x14000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1226
    const-string v4, "android.intent.action.MAIN"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1227
    const/high16 v4, 0x200000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1228
    const-string v4, "android.intent.category.HOME"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1230
    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 1232
    .end local v2    # "home":Landroid/content/Intent;
    :cond_2
    invoke-static {}, Lcom/android/launcher2/ShadowGen;->getInstance()Lcom/android/launcher2/ShadowGen;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/launcher2/ShadowGen;->onDestroy()V

    .line 1233
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 2250
    const-string v0, "Launcher"

    const-string v1, "onDetachedFromWindow"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2251
    invoke-super {p0}, Landroid/app/ActivityGroup;->onDetachedFromWindow()V

    .line 2254
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHotwordServiceClient:Lcom/google/android/hotword/client/HotwordServiceClient;

    invoke-virtual {v0}, Lcom/google/android/hotword/client/HotwordServiceClient;->onDetachedFromWindow()V

    .line 2256
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->exitWidgetResizeMode()V

    .line 2258
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mAttached:Z

    if-eqz v0, :cond_0

    .line 2259
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2260
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->mAttached:Z

    .line 2262
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->updateRunning()V

    .line 2263
    return-void
.end method

.method public onGestureEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/samsung/android/service/gesture/GestureEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 4062
    sget-boolean v1, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v1, :cond_1

    .line 4095
    :cond_0
    :goto_0
    return-void

    .line 4066
    :cond_1
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 4067
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4070
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getState()Lcom/android/launcher2/Workspace$State;

    move-result-object v1

    sget-object v2, Lcom/android/launcher2/Workspace$State;->EDIT:Lcom/android/launcher2/Workspace$State;

    if-ne v1, v2, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v1

    if-ne v1, v3, :cond_3

    .line 4072
    sget-boolean v1, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v1, :cond_2

    const-string v1, "Launcher"

    const-string v2, "GESTURE_EVENT_SWEEP_LEFT"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4073
    :cond_2
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v1

    invoke-virtual {v1, v4, v3}, Lcom/android/launcher2/Workspace;->moveToLeftScreen(ZZ)V

    goto :goto_0

    .line 4074
    :cond_3
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getState()Lcom/android/launcher2/Workspace$State;

    move-result-object v1

    sget-object v2, Lcom/android/launcher2/Workspace$State;->EDIT:Lcom/android/launcher2/Workspace$State;

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v1

    if-nez v1, :cond_0

    .line 4076
    sget-boolean v1, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v1, :cond_4

    const-string v1, "Launcher"

    const-string v2, "GESTURE_EVENT_SWEEP_RIGHT"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4077
    :cond_4
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, Lcom/android/launcher2/Workspace;->moveToLeftScreen(ZZ)V

    goto :goto_0

    .line 4079
    :cond_5
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4081
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->getTabManager()Lcom/android/launcher2/tabs/TabManager;

    move-result-object v1

    const-string v2, "APPS"

    invoke-virtual {v1, v2}, Lcom/android/launcher2/tabs/TabManager;->getFragmentForTab(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 4082
    .local v0, "frag":Landroid/app/Fragment;
    instance-of v1, v0, Lcom/android/launcher2/MenuAppsGridFragment;

    if-eqz v1, :cond_0

    .line 4083
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->getMenuAppsGrid()Lcom/android/launcher2/MenuAppsGrid;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/MenuAppsGrid;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v1

    sget-object v2, Lcom/android/launcher2/MenuAppsGrid$State;->EDIT:Lcom/android/launcher2/MenuAppsGrid$State;

    if-ne v1, v2, :cond_7

    invoke-virtual {p1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v1

    if-ne v1, v3, :cond_7

    .line 4085
    sget-boolean v1, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v1, :cond_6

    const-string v1, "Launcher"

    const-string v2, "GESTURE_EVENT_SWEEP_LEFT"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4086
    :cond_6
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->getMenuAppsGrid()Lcom/android/launcher2/MenuAppsGrid;

    move-result-object v1

    invoke-virtual {v1, v4, v3}, Lcom/android/launcher2/MenuAppsGrid;->scrollToLeft(ZZ)V

    goto/16 :goto_0

    .line 4087
    :cond_7
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->getMenuAppsGrid()Lcom/android/launcher2/MenuAppsGrid;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/MenuAppsGrid;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v1

    sget-object v2, Lcom/android/launcher2/MenuAppsGrid$State;->EDIT:Lcom/android/launcher2/MenuAppsGrid$State;

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v1

    if-nez v1, :cond_0

    .line 4089
    sget-boolean v1, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v1, :cond_8

    const-string v1, "Launcher"

    const-string v2, "GESTURE_EVENT_SWEEP_RIGHT"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4090
    :cond_8
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v1}, Lcom/android/launcher2/MenuView;->getMenuAppsGrid()Lcom/android/launcher2/MenuAppsGrid;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, Lcom/android/launcher2/MenuAppsGrid;->scrollToLeft(ZZ)V

    goto/16 :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 13
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1685
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v8

    .line 1686
    .local v8, "uniChar":I
    invoke-super {p0, p1, p2}, Landroid/app/ActivityGroup;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    .line 1687
    .local v3, "handled":Z
    if-lez v8, :cond_3

    invoke-static {v8}, Ljava/lang/Character;->isWhitespace(I)Z

    move-result v10

    if-nez v10, :cond_3

    const/4 v5, 0x1

    .line 1688
    .local v5, "isKeyNotWhitespace":Z
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1690
    .local v0, "config":Landroid/content/res/Configuration;
    iget v10, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v11, 0x3

    if-ne v10, v11, :cond_7

    .line 1691
    const/4 v10, 0x7

    if-lt p1, v10, :cond_6

    const/16 v10, 0x12

    if-gt p1, v10, :cond_6

    .line 1692
    const/4 v6, 0x0

    .line 1693
    .local v6, "myIntentDial":Landroid/content/Intent;
    const/4 v10, 0x7

    if-lt p1, v10, :cond_4

    const/16 v10, 0x10

    if-gt p1, v10, :cond_4

    .line 1694
    new-instance v6, Landroid/content/Intent;

    .end local v6    # "myIntentDial":Landroid/content/Intent;
    const-string v10, "android.intent.action.DIAL"

    const-string v11, "tel:"

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-direct {v6, v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1705
    .restart local v6    # "myIntentDial":Landroid/content/Intent;
    :goto_1
    invoke-static {}, Lcom/android/launcher2/Launcher;->readSalesCode()Ljava/lang/String;

    move-result-object v7

    .line 1706
    .local v7, "salesCode":Ljava/lang/String;
    if-eqz v7, :cond_1

    const-string v10, "CHZ"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "CHN"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "CHM"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "CHU"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "CTC"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "CHC"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1708
    :cond_0
    const-string v10, "firstKeycode"

    invoke-virtual {v6, v10, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1710
    :cond_1
    const-string v10, "isKeyTone"

    const/4 v11, 0x1

    invoke-virtual {v6, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1712
    invoke-virtual {p0, v6}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 1713
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->clearTypedText()V

    .line 1760
    .end local v3    # "handled":Z
    .end local v6    # "myIntentDial":Landroid/content/Intent;
    .end local v7    # "salesCode":Ljava/lang/String;
    :cond_2
    :goto_2
    return v3

    .line 1687
    .end local v0    # "config":Landroid/content/res/Configuration;
    .end local v5    # "isKeyNotWhitespace":Z
    .restart local v3    # "handled":Z
    :cond_3
    const/4 v5, 0x0

    goto :goto_0

    .line 1696
    .restart local v0    # "config":Landroid/content/res/Configuration;
    .restart local v5    # "isKeyNotWhitespace":Z
    .restart local v6    # "myIntentDial":Landroid/content/Intent;
    :cond_4
    const/16 v10, 0x12

    if-ne p1, v10, :cond_5

    .line 1697
    new-instance v6, Landroid/content/Intent;

    .end local v6    # "myIntentDial":Landroid/content/Intent;
    const-string v10, "android.intent.action.DIAL"

    const-string v11, "tel:"

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-direct {v6, v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1699
    .restart local v6    # "myIntentDial":Landroid/content/Intent;
    const-string v10, "isPoundKey"

    const/4 v11, 0x1

    invoke-virtual {v6, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1

    .line 1701
    :cond_5
    new-instance v6, Landroid/content/Intent;

    .end local v6    # "myIntentDial":Landroid/content/Intent;
    const-string v10, "android.intent.action.DIAL"

    const-string v11, "tel:"

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-direct {v6, v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .restart local v6    # "myIntentDial":Landroid/content/Intent;
    goto :goto_1

    .line 1716
    .end local v6    # "myIntentDial":Landroid/content/Intent;
    :cond_6
    const/16 v10, 0x1b

    if-ne p1, v10, :cond_9

    .line 1717
    new-instance v4, Landroid/content/Intent;

    const-string v10, "android.media.action.STILL_IMAGE_CAMERA"

    invoke-direct {v4, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1718
    .local v4, "intent":Landroid/content/Intent;
    const/high16 v10, 0x4000000

    invoke-virtual {v4, v10}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1719
    const/high16 v10, 0x10000000

    invoke-virtual {v4, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1720
    invoke-virtual {p0, v4}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 1735
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_7
    :goto_3
    const/16 v10, 0x106

    if-ne p1, v10, :cond_8

    .line 1736
    iget-object v10, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v10}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1737
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->showAllApps()V

    .line 1741
    :cond_8
    if-nez v3, :cond_a

    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->acceptFilter()Z

    move-result v10

    if-eqz v10, :cond_a

    if-eqz v5, :cond_a

    .line 1742
    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    move-result-object v10

    iget-object v11, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v11}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v11

    iget-object v12, p0, Lcom/android/launcher2/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v10, v11, v12, p1, p2}, Landroid/text/method/TextKeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v2

    .line 1744
    .local v2, "gotKey":Z
    if-eqz v2, :cond_a

    iget-object v10, p0, Lcom/android/launcher2/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    if-eqz v10, :cond_a

    iget-object v10, p0, Lcom/android/launcher2/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v10}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v10

    if-lez v10, :cond_a

    .line 1751
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->onSearchRequested()Z

    move-result v3

    goto :goto_2

    .line 1721
    .end local v2    # "gotKey":Z
    :cond_9
    const/16 v10, 0x43

    if-ne p1, v10, :cond_7

    .line 1722
    iget-object v10, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v10}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v9

    .line 1723
    .local v9, "workspace":Lcom/android/launcher2/Workspace;
    if-eqz v9, :cond_7

    sget-boolean v10, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-nez v10, :cond_7

    .line 1724
    invoke-virtual {v9}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v1

    .line 1725
    .local v1, "folder":Lcom/android/launcher2/Folder;
    if-nez v1, :cond_7

    .line 1727
    iget-object v10, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v10}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1728
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->showAllApps()V

    goto :goto_3

    .line 1756
    .end local v1    # "folder":Lcom/android/launcher2/Folder;
    .end local v9    # "workspace":Lcom/android/launcher2/Workspace;
    :cond_a
    const/16 v10, 0x52

    if-ne p1, v10, :cond_2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1757
    const/4 v3, 0x1

    goto/16 :goto_2
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 3125
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Launcher;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/high16 v13, 0x400000

    const/high16 v12, 0x200000

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 2310
    sput-boolean v10, Lcom/android/launcher2/Launcher;->isHelpIntentFired:Z

    .line 2311
    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onNewIntent(Landroid/content/Intent;)V

    .line 2312
    sput-object p1, Lcom/android/launcher2/Launcher;->sInComingIntentHelpHub:Landroid/content/Intent;

    .line 2317
    const-string v9, "android.intent.action.MAIN"

    invoke-direct {p0, p1, v9}, Lcom/android/launcher2/Launcher;->tryQueueWidgetAddViaIntent(Landroid/content/Intent;Ljava/lang/String;)Z

    move-result v8

    .line 2318
    .local v8, "widgetAdded":Z
    iget-boolean v9, p0, Lcom/android/launcher2/Launcher;->mAttached:Z

    if-nez v9, :cond_1

    .line 2323
    sput-object p1, Lcom/android/launcher2/Launcher;->mPendingIntent:Landroid/content/Intent;

    .line 2420
    :cond_0
    :goto_0
    return-void

    .line 2326
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->closePopupMenu()V

    .line 2328
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Launcher;->setIntent(Landroid/content/Intent;)V

    .line 2329
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2330
    .local v0, "action":Ljava/lang/String;
    const-string v9, "android.intent.action.MAIN"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2331
    const/4 v6, 0x0

    .line 2333
    .local v6, "notiID":I
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    if-eqz v9, :cond_8

    const-string v9, "homescreen:guide_mode"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_8

    .line 2334
    sget-boolean v9, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v9, :cond_2

    .line 2335
    invoke-virtual {p0, v11}, Lcom/android/launcher2/Launcher;->exitHelp(Z)V

    .line 2337
    :cond_2
    iget-object v9, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v9}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2338
    iget-object v9, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v9, v11}, Lcom/android/launcher2/HomeView;->closeQuickViewWorkspace(Z)V

    .line 2339
    :cond_3
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getRequestedOrientation()I

    move-result v9

    iput v9, p0, Lcom/android/launcher2/Launcher;->mPrevOrientationHelp:I

    .line 2346
    const-string v9, "help_activity_type"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2347
    .local v4, "helpActivityName":Ljava/lang/String;
    const-string v9, "ExternalHelpActivity"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2348
    iput-boolean v10, p0, Lcom/android/launcher2/Launcher;->mIsExternalHelpActivity:Z

    .line 2349
    :cond_4
    invoke-static {p0, p1}, Lcom/android/launcher2/guide/GuideFragment;->deployGuide(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 2350
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    invoke-static {v9}, Lcom/android/launcher2/CreateFolderDialog;->isActive(Landroid/app/FragmentManager;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2352
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    invoke-static {v9, v11}, Lcom/android/launcher2/CreateFolderDialog;->dismiss(Landroid/app/FragmentManager;Z)V

    .line 2354
    :cond_5
    sput-boolean v10, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    .line 2356
    iget-object v9, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v9, v10}, Lcom/android/launcher2/HomeView;->showWorkspace(Z)V

    .line 2357
    iget-object v9, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v9}, Lcom/android/launcher2/MenuView;->onBackPressed()Z

    .line 2405
    .end local v4    # "helpActivityName":Ljava/lang/String;
    :cond_6
    :goto_1
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->exitWidgetResizeMode()V

    .line 2407
    iget-object v9, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    iget-boolean v9, v9, Lcom/android/launcher2/HomeView;->mHomeKeyPress:Z

    if-eqz v9, :cond_13

    .line 2408
    iget-object v9, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/android/launcher2/HomeView;->closeSystemDialogs(Ljava/lang/Boolean;)V

    .line 2411
    :goto_2
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 2412
    .local v2, "fm":Landroid/app/FragmentManager;
    invoke-static {v2}, Lcom/android/launcher2/CreateFolderDialog;->isActive(Landroid/app/FragmentManager;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 2413
    invoke-static {v2, v10}, Lcom/android/launcher2/CreateFolderDialog;->dismiss(Landroid/app/FragmentManager;Z)V

    .line 2414
    :cond_7
    invoke-static {v2}, Lcom/android/launcher2/DisableAppConfirmationDialog;->isActive(Landroid/app/FragmentManager;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2415
    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 2416
    .local v3, "ft":Landroid/app/FragmentTransaction;
    invoke-static {v3, v2}, Lcom/android/launcher2/DisableAppConfirmationDialog;->dismiss(Landroid/app/FragmentTransaction;Landroid/app/FragmentManager;)V

    .line 2417
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I

    goto/16 :goto_0

    .line 2359
    .end local v2    # "fm":Landroid/app/FragmentManager;
    .end local v3    # "ft":Landroid/app/FragmentTransaction;
    :cond_8
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v9

    and-int/2addr v9, v13

    if-eq v9, v13, :cond_f

    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v9

    and-int/2addr v9, v12

    if-ne v9, v12, :cond_f

    .line 2362
    const-string v9, "sec.android.intent.extra.LAUNCHER_ACTION"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_9

    .line 2363
    iget-object v9, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v9}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v9

    if-eqz v9, :cond_c

    .line 2364
    iget-object v12, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    if-nez v8, :cond_a

    move v9, v10

    :goto_3
    invoke-virtual {v12, v9}, Lcom/android/launcher2/MenuView;->onHomePressed(Z)Z

    move-result v9

    if-nez v9, :cond_9

    .line 2365
    const-string v9, "streetlife"

    const-string v12, "onNewIntent exitAllApps"

    invoke-static {v9, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2366
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->hasWindowFocus()Z

    move-result v9

    if-eqz v9, :cond_b

    move v9, v11

    :goto_4
    invoke-virtual {p0, v11, v9}, Lcom/android/launcher2/Launcher;->exitAllApps(ZZ)V

    .line 2367
    iget-object v9, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v9}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v9

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v9}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v9

    if-eqz v9, :cond_9

    .line 2369
    iget-object v9, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v9}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/launcher2/QuickViewWorkspace;->drawCloseAnimation()Z

    .line 2381
    :cond_9
    :goto_5
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Window;->peekDecorView()Landroid/view/View;

    move-result-object v7

    .line 2382
    .local v7, "v":Landroid/view/View;
    if-eqz v7, :cond_6

    invoke-virtual {v7}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v9

    if-eqz v9, :cond_6

    .line 2383
    const-string v9, "input_method"

    invoke-virtual {p0, v9}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    .line 2384
    .local v5, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v7}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v9

    invoke-virtual {v5, v9, v11}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto/16 :goto_1

    .end local v5    # "imm":Landroid/view/inputmethod/InputMethodManager;
    .end local v7    # "v":Landroid/view/View;
    :cond_a
    move v9, v11

    .line 2364
    goto :goto_3

    :cond_b
    move v9, v10

    .line 2366
    goto :goto_4

    .line 2373
    :cond_c
    sget-boolean v9, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-nez v9, :cond_d

    iget-object v9, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v9}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/launcher2/Workspace;->getState()Lcom/android/launcher2/Workspace$State;

    move-result-object v9

    sget-object v12, Lcom/android/launcher2/Workspace$State;->EDIT:Lcom/android/launcher2/Workspace$State;

    if-eq v9, v12, :cond_9

    .line 2375
    :cond_d
    if-nez v6, :cond_9

    .line 2376
    iget-object v12, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    if-nez v8, :cond_e

    move v9, v10

    :goto_6
    invoke-virtual {v12, v9}, Lcom/android/launcher2/HomeView;->onHomePressed(Z)Z

    goto :goto_5

    :cond_e
    move v9, v11

    goto :goto_6

    .line 2387
    :cond_f
    const-string v9, "sec.android.intent.extra.LAUNCHER_ACTION"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_11

    iget-object v9, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v9}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v9

    if-eqz v9, :cond_11

    .line 2389
    const-string v9, "sec.android.intent.extra.LAUNCHER_ACTION"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2390
    .local v1, "extra":Ljava/lang/String;
    const-string v9, "com.android.launcher2.IDLE"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 2391
    invoke-virtual {p0, v11, v10}, Lcom/android/launcher2/Launcher;->exitAllApps(ZZ)V

    goto/16 :goto_1

    .line 2393
    :cond_10
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->cancelAppFolderRemoveDialog()V

    goto/16 :goto_1

    .line 2399
    .end local v1    # "extra":Ljava/lang/String;
    :cond_11
    sget-boolean v9, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v9, :cond_12

    iget-object v9, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v9}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/launcher2/Workspace;->isStartDragStarted()Z

    move-result v9

    if-nez v9, :cond_12

    .line 2400
    invoke-virtual {p0, v11}, Lcom/android/launcher2/Launcher;->exitHelp(Z)V

    .line 2402
    :cond_12
    invoke-virtual {p0, v11, v10}, Lcom/android/launcher2/Launcher;->exitAllApps(ZZ)V

    goto/16 :goto_1

    .line 2410
    :cond_13
    iget-object v9, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/android/launcher2/HomeView;->closeSystemDialogs(Ljava/lang/Boolean;)V

    goto/16 :goto_2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 20
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 2617
    const/4 v3, 0x0

    sput-boolean v3, Lcom/android/launcher2/Launcher;->sHwPopupMenuShowing:Z

    .line 2618
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->invalidateOptionsMenu()V

    .line 2620
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 2791
    const/4 v3, 0x0

    :goto_0
    return v3

    .line 2622
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->onClickstartDownloadableApps()V

    .line 2623
    const/4 v3, 0x1

    goto :goto_0

    .line 2625
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->onClickAppMarketButton()V

    .line 2626
    const/4 v3, 0x1

    goto :goto_0

    .line 2628
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    sget-object v4, Lcom/android/launcher2/MenuAppsGrid$State;->EDIT:Lcom/android/launcher2/MenuAppsGrid$State;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/MenuView;->changeState(Lcom/android/launcher2/MenuAppsGrid$State;)V

    .line 2629
    const/4 v3, 0x1

    goto :goto_0

    .line 2631
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v3}, Lcom/android/launcher2/MenuView;->onOptionSelectedSearch()V

    .line 2632
    const/4 v3, 0x1

    goto :goto_0

    .line 2634
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v3}, Lcom/android/launcher2/MenuView;->onOptionSelectedUninstall()V

    .line 2635
    const/4 v3, 0x1

    goto :goto_0

    .line 2637
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v3}, Lcom/android/launcher2/MenuView;->onOptionSelectedDownload()V

    .line 2638
    const/4 v3, 0x1

    goto :goto_0

    .line 2640
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v3}, Lcom/android/launcher2/MenuView;->chooseViewType()V

    .line 2641
    const/4 v3, 0x1

    goto :goto_0

    .line 2643
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    sget-object v4, Lcom/android/launcher2/MenuAppsGrid$State;->SHARE_SELECT:Lcom/android/launcher2/MenuAppsGrid$State;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/MenuView;->changeState(Lcom/android/launcher2/MenuAppsGrid$State;)V

    .line 2644
    const/4 v3, 0x1

    goto :goto_0

    .line 2646
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    sget-object v4, Lcom/android/launcher2/MenuAppsGrid$State;->SEL_APPS_TO_HIDE:Lcom/android/launcher2/MenuAppsGrid$State;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/MenuView;->changeState(Lcom/android/launcher2/MenuAppsGrid$State;)V

    .line 2647
    const/4 v3, 0x1

    goto :goto_0

    .line 2649
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    sget-object v4, Lcom/android/launcher2/MenuAppsGrid$State;->SEL_APPS_TO_UNHIDE:Lcom/android/launcher2/MenuAppsGrid$State;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/MenuView;->changeState(Lcom/android/launcher2/MenuAppsGrid$State;)V

    .line 2650
    const/4 v3, 0x1

    goto :goto_0

    .line 2652
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    sget-object v4, Lcom/android/launcher2/MenuAppsGrid$State;->SEL_APPS_TO_ENABLE:Lcom/android/launcher2/MenuAppsGrid$State;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/MenuView;->changeState(Lcom/android/launcher2/MenuAppsGrid$State;)V

    .line 2653
    const/4 v3, 0x1

    goto :goto_0

    .line 2655
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    sget-object v4, Lcom/android/launcher2/MenuAppsGrid$State;->MOVE_TO_SECRET_BOX:Lcom/android/launcher2/MenuAppsGrid$State;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/MenuView;->changeState(Lcom/android/launcher2/MenuAppsGrid$State;)V

    .line 2656
    const/4 v3, 0x1

    goto :goto_0

    .line 2658
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    sget-object v4, Lcom/android/launcher2/MenuAppsGrid$State;->REMOVE_FROM_SECRET_BOX:Lcom/android/launcher2/MenuAppsGrid$State;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/MenuView;->changeState(Lcom/android/launcher2/MenuAppsGrid$State;)V

    .line 2659
    const/4 v3, 0x1

    goto :goto_0

    .line 2663
    :pswitch_d
    const-string v3, "search"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/SearchManager;

    .line 2664
    .local v2, "searchManager":Landroid/app/SearchManager;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getComponentName()Landroid/content/ComponentName;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual/range {v2 .. v7}, Landroid/app/SearchManager;->startSearch(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V

    .line 2665
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 2668
    .end local v2    # "searchManager":Landroid/app/SearchManager;
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/android/launcher2/HomeView;->openQuickViewWorkspace(Lcom/android/launcher2/HomeView$SavedState;Z)V

    .line 2669
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 2672
    :pswitch_f
    sget-boolean v3, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v3, :cond_0

    .line 2673
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/android/launcher2/Launcher;->setHomeEditMode(Z)V

    .line 2674
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/launcher2/HomeView;->showWorkspaceEditmode(Z)V

    .line 2678
    :goto_1
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 2676
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/android/launcher2/HomeView;->openQuickViewWorkspace(Lcom/android/launcher2/HomeView$SavedState;Z)V

    goto :goto_1

    .line 2680
    :pswitch_10
    new-instance v15, Landroid/content/Intent;

    const-string v3, "android.settings.SETTINGS"

    invoke-direct {v15, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2681
    .local v15, "i":Landroid/content/Intent;
    const/high16 v3, 0x10200000

    invoke-virtual {v15, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2682
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v15, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 2683
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 2684
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 2686
    .end local v15    # "i":Landroid/content/Intent;
    :pswitch_11
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->onClickonlineHelp()V

    .line 2704
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 2706
    :pswitch_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v3}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v19

    .line 2707
    .local v19, "workspace":Lcom/android/launcher2/Workspace;
    if-eqz v19, :cond_1

    .line 2708
    invoke-virtual/range {v19 .. v19}, Lcom/android/launcher2/Workspace;->exitWidgetResizeMode()V

    .line 2710
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v3}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_3

    const-wide/16 v8, -0x66

    .line 2713
    .local v8, "container":J
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v3}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2714
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/launcher2/Launcher;->isFolderCreatedFromMenuButton:Z

    .line 2715
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, -0x1

    const/4 v13, -0x1

    invoke-static/range {v4 .. v13}, Lcom/android/launcher2/CreateFolderDialog;->createAndShow(Landroid/app/FragmentManager;Lcom/android/launcher2/BaseItem;JJZZII)V

    .line 2717
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 2710
    .end local v8    # "container":J
    :cond_3
    const-wide/16 v8, -0x64

    goto :goto_2

    .line 2719
    .end local v19    # "workspace":Lcom/android/launcher2/Workspace;
    :pswitch_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    sget-object v4, Lcom/android/launcher2/MenuAppsGrid$State;->REMOVE_FOLDER:Lcom/android/launcher2/MenuAppsGrid$State;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/MenuView;->changeState(Lcom/android/launcher2/MenuAppsGrid$State;)V

    .line 2720
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 2754
    :pswitch_14
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->showAllApps()V

    .line 2755
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->shouldToastRepeat()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2756
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-static {v3}, Lcom/android/launcher2/AddAppsWidgetToastPopUp;->createAndShow(Landroid/app/FragmentManager;)V

    .line 2759
    :goto_3
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 2758
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f100020

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_3

    .line 2761
    :pswitch_15
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->launchOptionWallpaper()V

    .line 2762
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 2764
    :pswitch_16
    new-instance v16, Landroid/content/Intent;

    invoke-direct/range {v16 .. v16}, Landroid/content/Intent;-><init>()V

    .line 2765
    .local v16, "intent":Landroid/content/Intent;
    const-string v3, "com.samsung.android.app.galaxyfinder"

    const-string v4, "com.samsung.android.app.galaxyfinder.GalaxyFinderActivity"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2766
    new-instance v14, Landroid/content/ComponentName;

    const-string v3, "com.sec.android.app.launcher"

    const-string v4, "com.sec.android.app.launcher.Launcher"

    invoke-direct {v14, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2767
    .local v14, "component":Landroid/content/ComponentName;
    const-string v3, "componentname"

    invoke-virtual {v14}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2768
    const-string v3, "callername"

    const/4 v4, 0x2

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2769
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v3}, Lcom/android/launcher2/Launcher;->startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z

    .line 2770
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 2773
    .end local v14    # "component":Landroid/content/ComponentName;
    .end local v16    # "intent":Landroid/content/Intent;
    :pswitch_17
    sget-boolean v3, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_USM_EXISTS:Z

    if-eqz v3, :cond_5

    .line 2774
    new-instance v18, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2775
    .local v18, "usmIntent":Landroid/content/Intent;
    const/high16 v3, 0x10200000

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2776
    const-string v3, "android.intent.category.USM_HOME"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 2777
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 2779
    .end local v18    # "usmIntent":Landroid/content/Intent;
    :cond_5
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 2783
    :pswitch_18
    sget-boolean v3, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_THEME_ENABLE:Z

    if-eqz v3, :cond_6

    .line 2784
    new-instance v17, Landroid/content/Intent;

    const-string v3, "com.sec.android.intent.action.THEME_CHOOSER"

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2786
    .local v17, "intentTheme":Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 2788
    .end local v17    # "intentTheme":Landroid/content/Intent;
    :cond_6
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 2620
    :pswitch_data_0
    .packed-switch 0x7f0f00e1
        :pswitch_1
        :pswitch_2
        :pswitch_14
        :pswitch_12
        :pswitch_13
        :pswitch_15
        :pswitch_18
        :pswitch_f
        :pswitch_16
        :pswitch_3
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_e
        :pswitch_d
        :pswitch_17
        :pswitch_10
        :pswitch_11
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1673
    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onOptionsMenuClosed(Landroid/view/Menu;)V

    .line 1674
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->invalidateOptionsMenu()V

    .line 1675
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher2/Launcher;->sHwPopupMenuShowing:Z

    .line 1676
    return-void
.end method

.method public onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 815
    sget-boolean v1, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 816
    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPause, Launcher: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    :cond_0
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->onPause()V

    .line 818
    invoke-super {p0}, Landroid/app/ActivityGroup;->onPause()V

    .line 819
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->mPaused:Z

    .line 820
    iput-boolean v4, p0, Lcom/android/launcher2/Launcher;->mResumed:Z

    .line 824
    invoke-virtual {p0, v4}, Lcom/android/launcher2/Launcher;->setSystemUiTransparency(Z)V

    .line 826
    sget-boolean v1, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-nez v1, :cond_1

    .line 827
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->exitWidgetResizeMode()V

    .line 829
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->closePopupMenu()V

    .line 832
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 833
    .local v0, "pauseIntent":Landroid/content/Intent;
    const-string v1, "com.sec.android.intent.action.HOME_PAUSE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 834
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    .line 835
    invoke-static {}, Lcom/sec/dtl/launcher/GyroForShadow;->pauseSensor()V

    .line 836
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 1594
    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 1596
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v5}, Lcom/android/launcher2/HomeView;->getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->isCurrentTabWidgets()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1668
    :cond_0
    :goto_0
    return v3

    .line 1599
    :cond_1
    sget-boolean v5, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-nez v5, :cond_0

    .line 1605
    sget-boolean v5, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v5, :cond_2

    .line 1606
    invoke-static {}, Lcom/android/launcher2/Launcher;->isHomeEditMode()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1610
    :cond_2
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v5}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1612
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v5}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v4

    .line 1613
    .local v4, "workspace":Lcom/android/launcher2/Workspace;
    if-eqz v4, :cond_3

    .line 1614
    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v0

    .line 1615
    .local v0, "folder":Lcom/android/launcher2/Folder;
    if-eqz v0, :cond_3

    .line 1616
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->isEditingName()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->isFolderOpenAnimationEnded()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, v0, Lcom/android/launcher2/Folder;->mFolderAnimator:Lcom/android/launcher2/FolderAnimator;

    invoke-virtual {v5}, Lcom/android/launcher2/FolderAnimator;->isAnimating()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1619
    invoke-virtual {v0, v3, v3}, Lcom/android/launcher2/Folder;->close(ZZ)V

    .line 1654
    .end local v0    # "folder":Lcom/android/launcher2/Folder;
    .end local v4    # "workspace":Lcom/android/launcher2/Workspace;
    :cond_3
    :goto_1
    iget-boolean v5, p0, Lcom/android/launcher2/Launcher;->mTransitioningToAllApps:Z

    if-eq v5, v7, :cond_0

    iget-boolean v5, p0, Lcom/android/launcher2/Launcher;->mExitingAllApps:Z

    if-eq v5, v7, :cond_0

    .line 1657
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->setMarketLabel()V

    .line 1664
    const/4 v3, 0x1

    .line 1665
    .local v3, "res":Z
    iget-boolean v5, p0, Lcom/android/launcher2/Launcher;->mHasMenuKey:Z

    if-eqz v5, :cond_4

    .line 1666
    invoke-direct {p0, p1}, Lcom/android/launcher2/Launcher;->setupOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    .line 1667
    :cond_4
    sput-boolean v3, Lcom/android/launcher2/Launcher;->sHwPopupMenuShowing:Z

    goto :goto_0

    .line 1625
    .end local v3    # "res":Z
    :cond_5
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->isCurrentTabWidgets()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1626
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->getMenuWidgets()Lcom/android/launcher2/MenuWidgets;

    move-result-object v2

    .line 1627
    .local v2, "menuWidgets":Lcom/android/launcher2/MenuWidgets;
    if-eqz v2, :cond_3

    .line 1628
    invoke-virtual {v2}, Lcom/android/launcher2/MenuWidgets;->getWidgetState()Lcom/android/launcher2/MenuWidgets$WidgetState;

    move-result-object v5

    sget-object v6, Lcom/android/launcher2/MenuWidgets$WidgetState;->NORMAL:Lcom/android/launcher2/MenuWidgets$WidgetState;

    if-eq v5, v6, :cond_3

    goto :goto_0

    .line 1633
    .end local v2    # "menuWidgets":Lcom/android/launcher2/MenuWidgets;
    :cond_6
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v5

    sget-object v6, Lcom/android/launcher2/MenuAppsGrid$State;->NORMAL:Lcom/android/launcher2/MenuAppsGrid$State;

    if-eq v5, v6, :cond_7

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v5

    sget-object v6, Lcom/android/launcher2/MenuAppsGrid$State;->DOWNLOADED_APPS:Lcom/android/launcher2/MenuAppsGrid$State;

    if-ne v5, v6, :cond_0

    .line 1638
    :cond_7
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v5

    sget-object v6, Lcom/android/launcher2/MenuAppsGrid$State;->NORMAL:Lcom/android/launcher2/MenuAppsGrid$State;

    if-ne v5, v6, :cond_3

    .line 1639
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->getMenuAppsGrid()Lcom/android/launcher2/MenuAppsGrid;

    move-result-object v1

    .line 1640
    .local v1, "menuGrid":Lcom/android/launcher2/MenuAppsGrid;
    if-eqz v1, :cond_3

    .line 1641
    invoke-virtual {v1}, Lcom/android/launcher2/MenuAppsGrid;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v0

    .line 1642
    .restart local v0    # "folder":Lcom/android/launcher2/Folder;
    if-eqz v0, :cond_3

    .line 1643
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->isEditingName()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->isFolderOpenAnimationEnded()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1646
    invoke-virtual {v0, v3, v3}, Lcom/android/launcher2/Folder;->close(ZZ)V

    goto :goto_1
.end method

.method protected onRestart()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 800
    sget-boolean v0, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    sput-boolean v0, Lcom/android/launcher2/Launcher;->isHelpIntentFired:Z

    .line 801
    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRestart, Launcher: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 802
    sget-boolean v0, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v0, :cond_0

    .line 803
    sget-object v0, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v1, "change_wallpaper"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/launcher2/Launcher;->isExitingFromWallpaperActivity:Z

    if-eqz v0, :cond_0

    .line 804
    sput-boolean v3, Lcom/android/launcher2/Launcher;->isExitingFromWallpaperActivity:Z

    .line 805
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->onBackPressed()V

    .line 809
    :cond_0
    sput-boolean v3, Lcom/android/launcher2/Launcher;->sIsStopped:Z

    .line 810
    invoke-super {p0}, Landroid/app/ActivityGroup;->onRestart()V

    .line 811
    return-void
.end method

.method public onResume()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 841
    sget-boolean v5, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    sput-boolean v5, Lcom/android/launcher2/Launcher;->isHelpIntentFired:Z

    .line 842
    const-string v5, "Launcher"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onResume, Launcher: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 843
    invoke-super {p0}, Landroid/app/ActivityGroup;->onResume()V

    .line 844
    iput-boolean v10, p0, Lcom/android/launcher2/Launcher;->mResumed:Z

    .line 845
    iput-boolean v8, p0, Lcom/android/launcher2/Launcher;->mPaused:Z

    .line 846
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "kids_home_mode"

    invoke-static {v5, v6, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 848
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v5}, Lcom/android/launcher2/HomeView;->onResume()V

    .line 849
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->clearTypedText()V

    .line 853
    invoke-virtual {p0, v10}, Lcom/android/launcher2/Launcher;->setSystemUiTransparency(Z)V

    .line 855
    invoke-static {p0}, Lcom/android/launcher2/Utilities;->broadcastStkIntent(Landroid/content/Context;)V

    .line 856
    iget-boolean v5, p0, Lcom/android/launcher2/Launcher;->mRestoring:Z

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/android/launcher2/Launcher;->mOnResumeNeedsLoad:Z

    if-eqz v5, :cond_1

    .line 857
    :cond_0
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mModel:Lcom/android/launcher2/LauncherModel;

    sget-boolean v6, Lcom/android/launcher2/Launcher;->sIsSecretModeOn:Z

    invoke-virtual {v5, v6}, Lcom/android/launcher2/LauncherModel;->startLoader(Z)V

    .line 858
    iput-boolean v8, p0, Lcom/android/launcher2/Launcher;->mRestoring:Z

    .line 859
    iget-boolean v5, p0, Lcom/android/launcher2/Launcher;->mOnResumeNeedsLoad:Z

    if-eqz v5, :cond_1

    .line 860
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mSamsungWidgetPackageManager:Lcom/android/launcher2/SamsungWidgetPackageManager;

    invoke-virtual {v5}, Lcom/android/launcher2/SamsungWidgetPackageManager;->loadIfNeeded()V

    .line 861
    iput-boolean v8, p0, Lcom/android/launcher2/Launcher;->mOnResumeNeedsLoad:Z

    .line 866
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 867
    .local v1, "homeMode":Landroid/content/Intent;
    const-string v5, "com.android.launcher.action.HOME_MODE_CHANGE"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 868
    const-string v5, "currentMode"

    const-string v6, "twlauncher"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 869
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    .line 872
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "sec.android.intent.extra.LAUNCHER_ACTION"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 873
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 874
    const-string v5, "com.android.launcher2.ALL_APPS"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 875
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v5

    if-nez v5, :cond_7

    iget-boolean v5, p0, Lcom/android/launcher2/Launcher;->mTransitioningToAllApps:Z

    if-nez v5, :cond_7

    .line 876
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->selectAppsTab()V

    .line 877
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->showAllApps()V

    .line 882
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "sec.android.intent.extra.LAUNCHER_ACTION"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 886
    :cond_3
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->isKnoxLauncher()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 888
    sget-boolean v5, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v5, :cond_4

    const-string v5, "Launcher"

    const-string v6, "Resumed Type : Knox Launcher"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    :cond_4
    :goto_1
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    iput-boolean v8, v5, Lcom/android/launcher2/HomeView;->mHomeKeyPress:Z

    .line 898
    sput-boolean v8, Lcom/android/launcher2/Launcher;->sHwPopupMenuShowing:Z

    .line 900
    sget-object v5, Lcom/sec/dtl/launcher/Talk;->INSTANCE:Lcom/sec/dtl/launcher/Talk;

    invoke-virtual {v5}, Lcom/sec/dtl/launcher/Talk;->onResume()V

    .line 902
    sget-boolean v5, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v5, :cond_5

    .line 903
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 904
    .local v2, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 905
    const-string v5, "Launcher"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Resumed Orientation : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 906
    const-string v5, "Launcher"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Resumed small width : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    const-string v5, "Launcher"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Resumed density : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 909
    .end local v2    # "metrics":Landroid/util/DisplayMetrics;
    :cond_5
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->addAnyPendingWidgetsToWorkspace()V

    .line 910
    invoke-static {}, Lcom/sec/dtl/launcher/GyroForShadow;->resumeSensor()V

    .line 912
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v5}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v4

    .line 913
    .local v4, "ws":Lcom/android/launcher2/Workspace;
    if-eqz v4, :cond_6

    .line 914
    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->removeHoverScrollHandler()V

    .line 915
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v5}, Lcom/android/launcher2/HomeView;->getVisibility()I

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->isQuickViewWorkspaceOpend()Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getBackgroundDarken()F

    move-result v5

    cmpl-float v5, v5, v9

    if-lez v5, :cond_6

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getIsDragOccuring()Z

    move-result v5

    if-nez v5, :cond_6

    .line 917
    invoke-virtual {v4, v9}, Lcom/android/launcher2/Workspace;->setBackgroundDarken(F)V

    .line 921
    :cond_6
    return-void

    .line 878
    .end local v4    # "ws":Lcom/android/launcher2/Workspace;
    :cond_7
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->isVisible()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->isCurrentTabWidgets()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 879
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v5}, Lcom/android/launcher2/MenuView;->selectAppsTab()V

    goto/16 :goto_0

    .line 892
    :cond_8
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 893
    .local v3, "resumeIntent":Landroid/content/Intent;
    const-string v5, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 894
    invoke-virtual {p0, v3}, Lcom/android/launcher2/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 3907
    const-string v1, "extra_home_view_hidden"

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3908
    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 3909
    return-void

    .line 3907
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSearchRequested()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2077
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0, v1, v2}, Lcom/android/launcher2/Launcher;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    .line 2079
    const v0, 0x7f040003

    const v1, 0x7f040004

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->overridePendingTransition(II)V

    .line 2080
    return v2
.end method

.method public onServiceConnected()V
    .locals 1

    .prologue
    .line 4052
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->mGestureServiceConnected:Z

    .line 4053
    return-void
.end method

.method public onServiceDisconnected()V
    .locals 1

    .prologue
    .line 4057
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->mGestureServiceConnected:Z

    .line 4058
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 792
    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStart, Launcher: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 793
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher2/Launcher;->sIsStopped:Z

    .line 794
    invoke-super {p0}, Landroid/app/ActivityGroup;->onStart()V

    .line 795
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v0}, Lcom/android/launcher2/HomeView;->onStart()V

    .line 796
    return-void
.end method

.method protected onStop()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1129
    sget-boolean v1, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 1130
    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onStop, Launcher: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1132
    :cond_0
    sget-boolean v1, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v1, :cond_2

    .line 1133
    sget-boolean v1, Lcom/android/launcher2/guide/GuideFragment;->isWrongActionDialogVisible:Z

    if-nez v1, :cond_1

    const-string v1, "resize_widgets"

    sget-object v2, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget v1, Lcom/android/launcher2/guide/GuideFragment;->sHelpHubStepNumber:I

    const/4 v2, 0x2

    if-lt v1, v2, :cond_2

    .line 1135
    :cond_1
    invoke-static {}, Lcom/android/launcher2/guide/GuideFragment;->removeWrongActionDialog()V

    .line 1136
    invoke-virtual {p0, v4}, Lcom/android/launcher2/Launcher;->exitHelp(Z)V

    .line 1140
    :cond_2
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->onStop()V

    .line 1141
    const/4 v1, 0x1

    sput-boolean v1, Lcom/android/launcher2/Launcher;->sIsStopped:Z

    .line 1142
    iput-boolean v4, p0, Lcom/android/launcher2/Launcher;->mResumed:Z

    .line 1143
    invoke-super {p0}, Landroid/app/ActivityGroup;->onStop()V

    .line 1145
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v0

    .line 1146
    .local v0, "priority":I
    const/4 v1, -0x4

    if-ne v0, v1, :cond_3

    sget-boolean v1, Lcom/android/launcher2/Launcher;->UseLauncherHighPriority:Z

    if-eqz v1, :cond_3

    .line 1147
    invoke-static {v4}, Landroid/os/Process;->setThreadPriority(I)V

    .line 1149
    :cond_3
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 3929
    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTrimMemory. Level: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3960
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 4735
    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onWindowFocusChanged(Z)V

    .line 4736
    if-eqz p1, :cond_0

    .line 4742
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mPaused:Z

    if-nez v0, :cond_0

    .line 4743
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->setSystemUiTransparency(Z)V

    .line 4745
    :cond_0
    return-void
.end method

.method public registerForAirMotionGestureListner()V
    .locals 4

    .prologue
    .line 4101
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_COMMON_GESTURE_WITH_IRSENSOR"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mGestureServiceConnected:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/launcher2/Launcher;->bSupportAirMove:Z

    if-eqz v0, :cond_0

    .line 4102
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mGestureListenerRegistered:Z

    if-nez v0, :cond_0

    .line 4103
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v1, "ir_provider"

    const-string v2, "air_motion_item_move"

    const/4 v3, 0x0

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/samsung/android/service/gesture/GestureManager;->registerListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 4104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->mGestureListenerRegistered:Z

    .line 4107
    :cond_0
    return-void
.end method

.method public removeStateAnimatorProvider(Lcom/android/launcher2/Launcher$StateAnimatorProvider;)V
    .locals 1
    .param p1, "provider"    # Lcom/android/launcher2/Launcher$StateAnimatorProvider;

    .prologue
    .line 1247
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mStateAnimatorProviders:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1248
    return-void
.end method

.method public restorePopupMenu()Z
    .locals 4

    .prologue
    .line 1110
    sget-boolean v2, Lcom/android/launcher2/Launcher;->sPopupMenuShowing:Z

    if-eqz v2, :cond_2

    .line 1111
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    if-nez v2, :cond_0

    .line 1112
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v2}, Lcom/android/launcher2/MenuView;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v2

    sget-object v3, Lcom/android/launcher2/MenuAppsGrid$State;->DOWNLOADED_APPS:Lcom/android/launcher2/MenuAppsGrid$State;

    if-ne v2, v3, :cond_1

    .line 1113
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    const v3, 0x7f0f009b

    invoke-virtual {v2, v3}, Lcom/android/launcher2/MenuView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1114
    .local v0, "downloadPopupButton":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->createPopupMenu(Landroid/view/View;)V

    .line 1121
    .end local v0    # "downloadPopupButton":Landroid/view/View;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mPopupMenu:Lcom/android/launcher2/popup/PopupMenu;

    invoke-virtual {v2}, Lcom/android/launcher2/popup/PopupMenu;->show()V

    .line 1122
    const/4 v2, 0x1

    .line 1124
    :goto_1
    return v2

    .line 1116
    :cond_1
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    const v3, 0x7f0f00ae

    invoke-virtual {v2, v3}, Lcom/android/launcher2/MenuView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1118
    .local v1, "popupButton":Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->createPopupMenu(Landroid/view/View;)V

    goto :goto_0

    .line 1124
    .end local v1    # "popupButton":Landroid/view/View;
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public saveToastPopup(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "mIsRepeat"    # Ljava/lang/Boolean;

    .prologue
    .line 4239
    const-string v2, "com.sec.android.app.launcher.prefs"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/android/launcher2/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 4241
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 4242
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "add.toast.popup.dismissed.key"

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 4243
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 4244
    return-void
.end method

.method public saveToastPopupForDisableDialog(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "mIsRepeat"    # Ljava/lang/Boolean;

    .prologue
    .line 4185
    const-string v2, "com.sec.android.app.launcher.prefs"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/android/launcher2/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 4187
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 4188
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "disable.toast.popup.dismissed.key"

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 4189
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 4190
    return-void
.end method

.method public setEnableHeadlines(Z)V
    .locals 0
    .param p1, "isEnableHeadlines"    # Z

    .prologue
    .line 4680
    sput-boolean p1, Lcom/android/launcher2/Launcher;->sIsHeadlinesAppEnable:Z

    .line 4681
    return-void
.end method

.method public setEnableHotWord(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 4685
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHotwordServiceClient()Lcom/google/android/hotword/client/HotwordServiceClient;

    move-result-object v0

    .line 4686
    .local v0, "hotwordServiceClient":Lcom/google/android/hotword/client/HotwordServiceClient;
    if-eqz v0, :cond_0

    .line 4687
    sget-boolean v1, Lcom/android/launcher2/Launcher;->mTalkbackEnable:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/android/launcher2/Launcher;->mAlwaysMicOn:Z

    if-eqz v1, :cond_1

    sget v1, Lcom/android/launcher2/Launcher;->mIsBabyCryingEnable:I

    if-nez v1, :cond_1

    sget v1, Lcom/android/launcher2/Launcher;->mIsDoorbellEnable:I

    if-nez v1, :cond_1

    .line 4688
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHotwordServiceClient()Lcom/google/android/hotword/client/HotwordServiceClient;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/hotword/client/HotwordServiceClient;->requestHotwordDetection(Z)V

    .line 4692
    :cond_0
    :goto_0
    return-void

    .line 4690
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getHotwordServiceClient()Lcom/google/android/hotword/client/HotwordServiceClient;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/hotword/client/HotwordServiceClient;->requestHotwordDetection(Z)V

    goto :goto_0
.end method

.method public setEnableMenuButton(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 2829
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuButtonView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2831
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mMenuButtonView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 2833
    :cond_0
    return-void
.end method

.method public setLoadOnResume()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3496
    iget-boolean v1, p0, Lcom/android/launcher2/Launcher;->mPaused:Z

    if-eqz v1, :cond_0

    .line 3497
    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->mOnResumeNeedsLoad:Z

    .line 3502
    :goto_0
    return v0

    .line 3500
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mSamsungWidgetPackageManager:Lcom/android/launcher2/SamsungWidgetPackageManager;

    if-eqz v0, :cond_1

    .line 3501
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mSamsungWidgetPackageManager:Lcom/android/launcher2/SamsungWidgetPackageManager;

    invoke-virtual {v0}, Lcom/android/launcher2/SamsungWidgetPackageManager;->loadIfNeeded()V

    .line 3502
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setShowEmptyPageMessagePref(Z)V
    .locals 0
    .param p1, "show"    # Z

    .prologue
    .line 3924
    iput-boolean p1, p0, Lcom/android/launcher2/Launcher;->mShowEmptyPageMsg:Z

    .line 3925
    return-void
.end method

.method setSystemUiTransparency(Z)V
    .locals 8
    .param p1, "bTransparent"    # Z

    .prologue
    const v7, 0x7fffffff

    const/high16 v6, 0x40000000    # 2.0f

    const v5, -0x8001

    .line 4749
    const-string v3, "statusbar"

    invoke-virtual {p0, v3}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/StatusBarManager;

    iput-object v3, p0, Lcom/android/launcher2/Launcher;->mStatusBarManager:Landroid/app/StatusBarManager;

    .line 4750
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->mStatusBarManager:Landroid/app/StatusBarManager;

    if-nez v3, :cond_0

    .line 4784
    :goto_0
    return-void

    .line 4753
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 4755
    .local v2, "wmLp":Landroid/view/WindowManager$LayoutParams;
    const/4 v3, 0x1

    if-ne p1, v3, :cond_2

    .line 4756
    const/4 v1, -0x1

    .line 4758
    .local v1, "wallpaper_transparent":I
    :try_start_0
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "android.wallpaper.settings_systemui_transparency"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4765
    :goto_1
    if-eqz v1, :cond_1

    .line 4766
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v4, -0x80000000

    or-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 4767
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    const v4, -0x40000001    # -1.9999999f

    invoke-virtual {v3, v4}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 4768
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    const v4, 0x8000

    invoke-virtual {v3, v4}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 4780
    .end local v1    # "wallpaper_transparent":I
    :goto_2
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 4782
    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Launcher::setSystemUiTransparency(): getSystemUiVisibility() after setting  = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 4760
    .restart local v1    # "wallpaper_transparent":I
    :catch_0
    move-exception v0

    .line 4761
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v3, "Launcher"

    const-string v4, "setSystemUiTransparency.SettingNotFoundException : set as TRUE"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4762
    const/4 v1, 0x1

    goto :goto_1

    .line 4771
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_1
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/2addr v3, v7

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 4772
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 4773
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_2

    .line 4776
    .end local v1    # "wallpaper_transparent":I
    :cond_2
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/2addr v3, v7

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 4777
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 4778
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_2
.end method

.method public setTransitionToAllApps(Z)V
    .locals 0
    .param p1, "stat"    # Z

    .prologue
    .line 1269
    iput-boolean p1, p0, Lcom/android/launcher2/Launcher;->mTransitioningToAllApps:Z

    .line 1270
    return-void
.end method

.method public setWhichTransitionEffect(I)V
    .locals 4
    .param p1, "whichTransitionEffect"    # I

    .prologue
    .line 4695
    sput p1, Lcom/android/launcher2/Launcher;->sWhichTransitionEffect:I

    .line 4697
    packed-switch p1, :pswitch_data_0

    .line 4708
    sget-object v0, Lcom/android/launcher2/PagedView$TransitionEffect;->NONE:Lcom/android/launcher2/PagedView$TransitionEffect;

    .line 4711
    .local v0, "effect":Lcom/android/launcher2/PagedView$TransitionEffect;
    :goto_0
    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting default transition effect:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4712
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4714
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4715
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Workspace;->setDefaultTransitionEffect(Lcom/android/launcher2/PagedView$TransitionEffect;)V

    .line 4717
    :cond_0
    return-void

    .line 4699
    .end local v0    # "effect":Lcom/android/launcher2/PagedView$TransitionEffect;
    :pswitch_0
    sget-object v0, Lcom/android/launcher2/PagedView$TransitionEffect;->NONE:Lcom/android/launcher2/PagedView$TransitionEffect;

    .line 4700
    .restart local v0    # "effect":Lcom/android/launcher2/PagedView$TransitionEffect;
    goto :goto_0

    .line 4702
    .end local v0    # "effect":Lcom/android/launcher2/PagedView$TransitionEffect;
    :pswitch_1
    sget-object v0, Lcom/android/launcher2/PagedView$TransitionEffect;->CARD:Lcom/android/launcher2/PagedView$TransitionEffect;

    .line 4703
    .restart local v0    # "effect":Lcom/android/launcher2/PagedView$TransitionEffect;
    goto :goto_0

    .line 4705
    .end local v0    # "effect":Lcom/android/launcher2/PagedView$TransitionEffect;
    :pswitch_2
    sget-object v0, Lcom/android/launcher2/PagedView$TransitionEffect;->SPIRAL:Lcom/android/launcher2/PagedView$TransitionEffect;

    .line 4706
    .restart local v0    # "effect":Lcom/android/launcher2/PagedView$TransitionEffect;
    goto :goto_0

    .line 4697
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public shouldDisablePopupRepeat()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4193
    const-string v2, "com.sec.android.app.launcher.prefs"

    invoke-virtual {p0, v2, v1}, Lcom/android/launcher2/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 4195
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "disable.toast.popup.dismissed.key"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public shouldToastRepeat()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4247
    const-string v2, "com.sec.android.app.launcher.prefs"

    invoke-virtual {p0, v2, v1}, Lcom/android/launcher2/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 4249
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "add.toast.popup.dismissed.key"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public showAllApps()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 1314
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    invoke-virtual {v8}, Landroid/animation/Animator;->isRunning()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1391
    :cond_0
    :goto_0
    return-void

    .line 1317
    :cond_1
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v8}, Lcom/android/launcher2/HomeView;->updateRunning()V

    .line 1318
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v8}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->closeFolder()V

    .line 1319
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v8}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->onPreShowAllApps()V

    .line 1320
    sget-boolean v8, Lcom/android/launcher2/HomeScreenOptionMenu;->isWidgetTab:Z

    if-nez v8, :cond_4

    .line 1321
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isTabletLayout()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1322
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v8}, Lcom/android/launcher2/MenuView;->selectAppsTab()V

    .line 1323
    :cond_2
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v8, v10}, Lcom/android/launcher2/HomeView;->closeQuickViewWorkspace(Z)V

    .line 1329
    :goto_1
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->exitWidgetResizeMode()V

    .line 1334
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v8}, Lcom/android/launcher2/MenuView;->onShowAllApps()V

    .line 1335
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    if-eqz v8, :cond_3

    .line 1336
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    invoke-virtual {v8}, Landroid/animation/Animator;->cancel()V

    .line 1338
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1339
    .local v0, "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mStateAnimatorProviders:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/launcher2/Launcher$StateAnimatorProvider;

    .line 1340
    .local v6, "provider":Lcom/android/launcher2/Launcher$StateAnimatorProvider;
    invoke-interface {v6, v0}, Lcom/android/launcher2/Launcher$StateAnimatorProvider;->collectShowAllAppsAnimators(Ljava/util/ArrayList;)V

    goto :goto_2

    .line 1325
    .end local v0    # "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "provider":Lcom/android/launcher2/Launcher$StateAnimatorProvider;
    :cond_4
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isTabletLayout()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1326
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v8}, Lcom/android/launcher2/MenuView;->selectWidgetsTab()V

    .line 1327
    :cond_5
    sput-boolean v11, Lcom/android/launcher2/HomeScreenOptionMenu;->isWidgetTab:Z

    goto :goto_1

    .line 1342
    .restart local v0    # "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_6
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v8}, Lcom/android/launcher2/MenuView;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v8

    sget-object v9, Lcom/android/launcher2/MenuAppsGrid$State;->EDIT:Lcom/android/launcher2/MenuAppsGrid$State;

    if-eq v8, v9, :cond_7

    .line 1343
    const v8, 0x7f050006

    invoke-static {p0, v8}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v1

    .line 1344
    .local v1, "darkenAnimator":Landroid/animation/Animator;
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v8}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 1345
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1348
    .end local v1    # "darkenAnimator":Landroid/animation/Animator;
    :cond_7
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->mMenuView:Lcom/android/launcher2/MenuView;

    invoke-virtual {v8, v11}, Lcom/android/launcher2/MenuView;->setVisibility(I)V

    .line 1349
    sget-wide v2, Lcom/android/launcher2/Launcher;->SHOW_ALL_APPS_TRANSITION_DURATION:J

    .line 1350
    .local v2, "duration":J
    new-instance v7, Landroid/animation/AnimatorSet;

    invoke-direct {v7}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1351
    .local v7, "set":Landroid/animation/AnimatorSet;
    invoke-virtual {v7, v0}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 1352
    invoke-virtual {v7, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1353
    new-instance v8, Landroid/view/animation/interpolator/SineInOut90;

    invoke-direct {v8}, Landroid/view/animation/interpolator/SineInOut90;-><init>()V

    invoke-virtual {v7, v8}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1354
    new-instance v8, Lcom/android/launcher2/Launcher$3;

    invoke-direct {v8, p0}, Lcom/android/launcher2/Launcher$3;-><init>(Lcom/android/launcher2/Launcher;)V

    invoke-virtual {v7, v8}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1370
    iput-boolean v10, p0, Lcom/android/launcher2/Launcher;->mTransitioningToAllApps:Z

    .line 1371
    iput-boolean v10, p0, Lcom/android/launcher2/Launcher;->mInMenu:Z

    .line 1372
    iput-object v7, p0, Lcom/android/launcher2/Launcher;->mTransitionAnimator:Landroid/animation/Animator;

    .line 1373
    invoke-virtual {v7}, Landroid/animation/AnimatorSet;->start()V

    .line 1376
    sget-boolean v8, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v8, :cond_0

    .line 1377
    sget-object v8, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v9, "view_all_apps"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1379
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    .line 1380
    .local v5, "mHandler":Landroid/os/Handler;
    new-instance v8, Lcom/android/launcher2/Launcher$4;

    invoke-direct {v8, p0}, Lcom/android/launcher2/Launcher$4;-><init>(Lcom/android/launcher2/Launcher;)V

    const-wide/16 v10, 0x3e8

    invoke-virtual {v5, v8, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method

.method public startActivityForResult(Landroid/content/Intent;ILcom/android/launcher2/Launcher$ActivityResultCallback;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I
    .param p3, "callback"    # Lcom/android/launcher2/Launcher$ActivityResultCallback;

    .prologue
    .line 4326
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mActivityCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v0, p2, p3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 4327
    invoke-virtual {p0, p1, p2}, Lcom/android/launcher2/Launcher;->startActivityForResult(Landroid/content/Intent;I)V

    .line 4328
    return-void
.end method

.method startActivitySafely(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 3304
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher2/Launcher;->startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "tag"    # Ljava/lang/Object;

    .prologue
    .line 3299
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/android/launcher2/Launcher;->startActivitySafely(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method startActivitySafely(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z
    .locals 17
    .param p1, "view"    # Landroid/view/View;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "tag"    # Ljava/lang/Object;

    .prologue
    .line 3230
    const/high16 v13, 0x10000000

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3232
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 3234
    new-instance v2, Landroid/os/DVFSHelper;

    const/16 v13, 0xc

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v13}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    .line 3235
    .local v2, "dvfsHelper":Landroid/os/DVFSHelper;
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/os/DVFSHelper;->onAppLaunchEvent(Landroid/content/Context;Ljava/lang/String;)V

    .line 3238
    .end local v2    # "dvfsHelper":Landroid/os/DVFSHelper;
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 3239
    .local v4, "extras":Landroid/os/Bundle;
    const/4 v11, 0x0

    .line 3240
    .local v11, "shortcutIntent":Landroid/content/Intent;
    if-eqz v4, :cond_2

    .line 3241
    sget-boolean v13, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v13, :cond_1

    .line 3242
    const-string v13, "Launcher"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "shortcut extras:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3243
    :cond_1
    const-string v13, "shortcutIntent"

    invoke-virtual {v4, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v5

    .line 3244
    .local v5, "intentUri":Ljava/lang/String;
    if-eqz v5, :cond_2

    .line 3246
    const/4 v13, 0x0

    :try_start_1
    invoke-static {v5, v13}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v11

    .line 3253
    .end local v5    # "intentUri":Ljava/lang/String;
    :cond_2
    :goto_0
    if-eqz v11, :cond_5

    .line 3254
    :try_start_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/android/launcher2/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    .line 3271
    :goto_1
    const/4 v13, 0x1

    .line 3295
    .end local v4    # "extras":Landroid/os/Bundle;
    .end local v11    # "shortcutIntent":Landroid/content/Intent;
    :goto_2
    return v13

    .line 3247
    .restart local v4    # "extras":Landroid/os/Bundle;
    .restart local v5    # "intentUri":Ljava/lang/String;
    .restart local v11    # "shortcutIntent":Landroid/content/Intent;
    :catch_0
    move-exception v3

    .line 3248
    .local v3, "e":Ljava/net/URISyntaxException;
    const-string v13, "Launcher"

    const-string v14, "failed to parse shortcut intent URI"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 3272
    .end local v3    # "e":Ljava/net/URISyntaxException;
    .end local v4    # "extras":Landroid/os/Bundle;
    .end local v5    # "intentUri":Ljava/lang/String;
    .end local v11    # "shortcutIntent":Landroid/content/Intent;
    :catch_1
    move-exception v3

    .line 3274
    .local v3, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v9

    .line 3276
    .local v9, "restore_cn":Landroid/content/ComponentName;
    if-eqz v9, :cond_3

    .line 3277
    :try_start_3
    new-instance v10, Landroid/content/Intent;

    const-string v13, "android.intent.action.VIEW"

    invoke-direct {v10, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3278
    .local v10, "restore_intent":Landroid/content/Intent;
    invoke-virtual {v9}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    .line 3279
    .local v8, "packageName":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "market://details?id="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    invoke-virtual {v10, v13}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 3280
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    .line 3286
    .end local v8    # "packageName":Ljava/lang/String;
    .end local v10    # "restore_intent":Landroid/content/Intent;
    :cond_3
    :goto_3
    const v13, 0x7f100002

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    .line 3287
    sget-boolean v13, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v13, :cond_4

    const-string v13, "Launcher"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Unable to launch. tag="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " intent="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3295
    .end local v3    # "e":Landroid/content/ActivityNotFoundException;
    .end local v9    # "restore_cn":Landroid/content/ComponentName;
    :cond_4
    :goto_4
    const/4 v13, 0x0

    goto :goto_2

    .line 3257
    .restart local v4    # "extras":Landroid/os/Bundle;
    .restart local v11    # "shortcutIntent":Landroid/content/Intent;
    :cond_5
    if-eqz p1, :cond_6

    const/4 v12, 0x1

    .line 3259
    .local v12, "useLaunchAnimation":Z
    :goto_5
    const/4 v7, 0x0

    .line 3261
    .local v7, "optsBundle":Landroid/os/Bundle;
    if-eqz v12, :cond_8

    .line 3262
    :try_start_4
    sget v13, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v14, 0x15

    if-lt v13, v14, :cond_7

    const v13, 0x7f04000f

    const v14, 0x7f04000a

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;

    move-result-object v6

    .line 3265
    .local v6, "opts":Landroid/app/ActivityOptions;
    :goto_6
    invoke-virtual {v6}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v7

    .line 3266
    invoke-virtual {v6}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v13

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v13}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    :try_end_4
    .catch Landroid/content/ActivityNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_1

    .line 3288
    .end local v4    # "extras":Landroid/os/Bundle;
    .end local v6    # "opts":Landroid/app/ActivityOptions;
    .end local v7    # "optsBundle":Landroid/os/Bundle;
    .end local v11    # "shortcutIntent":Landroid/content/Intent;
    .end local v12    # "useLaunchAnimation":Z
    :catch_2
    move-exception v3

    .line 3289
    .local v3, "e":Ljava/lang/SecurityException;
    const v13, 0x7f100002

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    .line 3290
    sget-boolean v13, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v13, :cond_4

    const-string v13, "Launcher"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Launcher does not have the permission to launch "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ". Make sure to create a MAIN intent-filter for the corresponding activity "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "or use the exported attribute for this activity. "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "tag="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " intent="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 3257
    .end local v3    # "e":Ljava/lang/SecurityException;
    .restart local v4    # "extras":Landroid/os/Bundle;
    .restart local v11    # "shortcutIntent":Landroid/content/Intent;
    :cond_6
    const/4 v12, 0x0

    goto :goto_5

    .line 3262
    .restart local v7    # "optsBundle":Landroid/os/Bundle;
    .restart local v12    # "useLaunchAnimation":Z
    :cond_7
    const/4 v13, 0x0

    const/4 v14, 0x0

    :try_start_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-static {v0, v13, v14, v15, v1}, Landroid/app/ActivityOptions;->makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/app/ActivityOptions;

    move-result-object v6

    goto :goto_6

    .line 3268
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V
    :try_end_5
    .catch Landroid/content/ActivityNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_1

    .line 3282
    .end local v4    # "extras":Landroid/os/Bundle;
    .end local v7    # "optsBundle":Landroid/os/Bundle;
    .end local v11    # "shortcutIntent":Landroid/content/Intent;
    .end local v12    # "useLaunchAnimation":Z
    .local v3, "e":Landroid/content/ActivityNotFoundException;
    .restart local v9    # "restore_cn":Landroid/content/ComponentName;
    :catch_3
    move-exception v13

    goto/16 :goto_3
.end method

.method public startActivityWithTransition(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 4336
    const-string v0, "from_right"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4337
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Launcher;->startActivitySafely(Landroid/content/Intent;)Z

    .line 4338
    const/high16 v0, 0x7f040000

    const v1, 0x7f040002

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->overridePendingTransition(II)V

    .line 4339
    return-void
.end method

.method public startActivityWithTransitionForHeadlines(Landroid/content/Intent;Z)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "moveFromFirstPage"    # Z

    .prologue
    const v6, 0x7f100002

    const/4 v5, 0x0

    .line 4342
    const/high16 v3, 0x30200000

    invoke-virtual {p1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 4346
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 4348
    new-instance v0, Landroid/os/DVFSHelper;

    const/16 v3, 0xc

    invoke-direct {v0, p0, v3}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    .line 4349
    .local v0, "dvfsHelper":Landroid/os/DVFSHelper;
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/DVFSHelper;->onAppLaunchEvent(Landroid/content/Context;Ljava/lang/String;)V

    .line 4352
    .end local v0    # "dvfsHelper":Landroid/os/DVFSHelper;
    :cond_0
    const/4 v2, 0x0

    .line 4353
    .local v2, "opts":Landroid/app/ActivityOptions;
    if-eqz p2, :cond_2

    .line 4354
    const v3, 0x7f040005

    const v4, 0x7f040008

    invoke-static {p0, v3, v4}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;

    move-result-object v2

    .line 4359
    :goto_0
    invoke-virtual {v2}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0, p1, v3}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 4372
    .end local v2    # "opts":Landroid/app/ActivityOptions;
    :cond_1
    :goto_1
    return-void

    .line 4357
    .restart local v2    # "opts":Landroid/app/ActivityOptions;
    :cond_2
    const v3, 0x7f040006

    const v4, 0x7f040007

    invoke-static {p0, v3, v4}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    goto :goto_0

    .line 4360
    .end local v2    # "opts":Landroid/app/ActivityOptions;
    :catch_0
    move-exception v1

    .line 4361
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-static {p0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 4362
    sget-boolean v3, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v3, :cond_1

    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to launch. intent="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 4363
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catch_1
    move-exception v1

    .line 4364
    .local v1, "e":Ljava/lang/SecurityException;
    invoke-static {p0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 4365
    sget-boolean v3, Lcom/android/launcher2/Launcher;->DEBUGGABLE:Z

    if-eqz v3, :cond_1

    .line 4366
    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Launcher does not have the permission to launch "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". Make sure to create a MAIN intent-filter for the corresponding activity "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "or use the exported attribute for this activity. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " intent="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .locals 6
    .param p1, "initialQuery"    # Ljava/lang/String;
    .param p2, "selectInitialQuery"    # Z
    .param p3, "appSearchData"    # Landroid/os/Bundle;
    .param p4, "globalSearch"    # Z

    .prologue
    .line 2092
    if-nez p1, :cond_0

    .line 2094
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->getTypedText()Ljava/lang/String;

    move-result-object p1

    .line 2096
    :cond_0
    if-nez p3, :cond_1

    .line 2097
    new-instance p3, Landroid/os/Bundle;

    .end local p3    # "appSearchData":Landroid/os/Bundle;
    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    .line 2098
    .restart local p3    # "appSearchData":Landroid/os/Bundle;
    const-string v1, "source"

    const-string v2, "launcher-search"

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2101
    :cond_1
    const-string v1, "search"

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 2103
    .local v0, "searchManager":Landroid/app/SearchManager;
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/app/SearchManager;->startSearch(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V

    .line 2105
    return-void
.end method

.method public unRegisterAirMotionGestureListner()V
    .locals 2

    .prologue
    .line 4112
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_COMMON_GESTURE_WITH_IRSENSOR"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mGestureServiceConnected:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/launcher2/Launcher;->bSupportAirMove:Z

    if-eqz v0, :cond_0

    .line 4113
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->mGestureListenerRegistered:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 4114
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v1, "ir_provider"

    invoke-virtual {v0, p0, v1}, Lcom/samsung/android/service/gesture/GestureManager;->unregisterListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;)V

    .line 4115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->mGestureListenerRegistered:Z

    .line 4118
    :cond_0
    return-void
.end method

.method public uninstallPackage(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 4229
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "package:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 4230
    .local v0, "packageURI":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.UNINSTALL_PACKAGE"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 4232
    .local v1, "uninstallIntent":Landroid/content/Intent;
    const/16 v2, 0x6f

    invoke-virtual {p0, v1, v2}, Lcom/android/launcher2/Launcher;->startActivityForResult(Landroid/content/Intent;I)V

    .line 4233
    return-void
.end method

.method public updateMotionGestureListner(Lcom/android/launcher2/MenuAppsGrid$State;Lcom/android/launcher2/MenuAppsGrid$State;)V
    .locals 1
    .param p1, "newState"    # Lcom/android/launcher2/MenuAppsGrid$State;
    .param p2, "oldState"    # Lcom/android/launcher2/MenuAppsGrid$State;

    .prologue
    .line 4131
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->isAirMoveOninSettings()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/launcher2/Launcher;->bSupportAirMove:Z

    if-eqz v0, :cond_0

    .line 4132
    sget-object v0, Lcom/android/launcher2/MenuAppsGrid$State;->NORMAL:Lcom/android/launcher2/MenuAppsGrid$State;

    if-ne p2, v0, :cond_1

    sget-object v0, Lcom/android/launcher2/MenuAppsGrid$State;->EDIT:Lcom/android/launcher2/MenuAppsGrid$State;

    if-ne p1, v0, :cond_1

    .line 4133
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->registerForAirMotionGestureListner()V

    .line 4138
    :cond_0
    :goto_0
    return-void

    .line 4134
    :cond_1
    sget-object v0, Lcom/android/launcher2/MenuAppsGrid$State;->EDIT:Lcom/android/launcher2/MenuAppsGrid$State;

    if-ne p2, v0, :cond_0

    sget-object v0, Lcom/android/launcher2/MenuAppsGrid$State;->NORMAL:Lcom/android/launcher2/MenuAppsGrid$State;

    if-ne p1, v0, :cond_0

    .line 4135
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->unRegisterAirMotionGestureListner()V

    goto :goto_0
.end method

.method public updateMotionGestureListner(Lcom/android/launcher2/Workspace$State;Lcom/android/launcher2/Workspace$State;)V
    .locals 1
    .param p1, "newState"    # Lcom/android/launcher2/Workspace$State;
    .param p2, "oldState"    # Lcom/android/launcher2/Workspace$State;

    .prologue
    .line 4121
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->isAirMoveOninSettings()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/android/launcher2/Launcher;->bSupportAirMove:Z

    if-eqz v0, :cond_1

    .line 4122
    sget-object v0, Lcom/android/launcher2/Workspace$State;->NORMAL:Lcom/android/launcher2/Workspace$State;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/android/launcher2/Workspace$State;->RESIZE:Lcom/android/launcher2/Workspace$State;

    if-ne p2, v0, :cond_2

    :cond_0
    sget-object v0, Lcom/android/launcher2/Workspace$State;->EDIT:Lcom/android/launcher2/Workspace$State;

    if-ne p1, v0, :cond_2

    .line 4123
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->registerForAirMotionGestureListner()V

    .line 4128
    :cond_1
    :goto_0
    return-void

    .line 4124
    :cond_2
    sget-object v0, Lcom/android/launcher2/Workspace$State;->EDIT:Lcom/android/launcher2/Workspace$State;

    if-ne p2, v0, :cond_1

    sget-object v0, Lcom/android/launcher2/Workspace$State;->NORMAL:Lcom/android/launcher2/Workspace$State;

    if-eq p1, v0, :cond_3

    sget-object v0, Lcom/android/launcher2/Workspace$State;->RESIZE:Lcom/android/launcher2/Workspace$State;

    if-ne p1, v0, :cond_1

    .line 4125
    :cond_3
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->unRegisterAirMotionGestureListner()V

    goto :goto_0
.end method
