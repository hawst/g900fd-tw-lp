.class Lcom/android/launcher2/CreateFolderDialog$5;
.super Ljava/lang/Object;
.source "CreateFolderDialog.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/launcher2/CreateFolderDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/launcher2/CreateFolderDialog;

.field final synthetic val$res:Landroid/content/res/Resources;

.field final synthetic val$viewToast:Landroid/widget/Toast;


# direct methods
.method constructor <init>(Lcom/android/launcher2/CreateFolderDialog;Landroid/content/res/Resources;Landroid/widget/Toast;)V
    .locals 0

    .prologue
    .line 518
    iput-object p1, p0, Lcom/android/launcher2/CreateFolderDialog$5;->this$0:Lcom/android/launcher2/CreateFolderDialog;

    iput-object p2, p0, Lcom/android/launcher2/CreateFolderDialog$5;->val$res:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/android/launcher2/CreateFolderDialog$5;->val$viewToast:Landroid/widget/Toast;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x1

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    .line 521
    const/4 v0, 0x0

    .line 522
    .local v0, "offsetX":I
    const/4 v3, 0x4

    new-array v2, v3, [I

    .line 523
    .local v2, "rect":[I
    iget-object v3, p0, Lcom/android/launcher2/CreateFolderDialog$5;->val$res:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 524
    iget-object v3, p0, Lcom/android/launcher2/CreateFolderDialog$5;->this$0:Lcom/android/launcher2/CreateFolderDialog;

    iget-object v3, v3, Lcom/android/launcher2/CreateFolderDialog;->mCreateFolderDialog:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v3

    div-int/lit8 v0, v3, 0x3

    .line 527
    :goto_0
    iget-object v3, p0, Lcom/android/launcher2/CreateFolderDialog$5;->this$0:Lcom/android/launcher2/CreateFolderDialog;

    iget-object v3, v3, Lcom/android/launcher2/CreateFolderDialog;->mCreateFolderDialog:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->getLocationOnScreen([I)V

    .line 528
    aget v3, v2, v10

    int-to-double v4, v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-double v6, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-object v3, p0, Lcom/android/launcher2/CreateFolderDialog$5;->this$0:Lcom/android/launcher2/CreateFolderDialog;

    invoke-virtual {v3}, Lcom/android/launcher2/CreateFolderDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0e0077

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-double v6, v3

    sub-double/2addr v4, v6

    double-to-int v1, v4

    .line 529
    .local v1, "offsetY":I
    iget-object v3, p0, Lcom/android/launcher2/CreateFolderDialog$5;->val$viewToast:Landroid/widget/Toast;

    const/16 v4, 0x35

    invoke-virtual {v3, v4, v0, v1}, Landroid/widget/Toast;->setGravity(III)V

    .line 530
    iget-object v3, p0, Lcom/android/launcher2/CreateFolderDialog$5;->val$viewToast:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 531
    return v10

    .line 526
    .end local v1    # "offsetY":I
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-double v4, v3

    mul-double/2addr v4, v8

    iget-object v3, p0, Lcom/android/launcher2/CreateFolderDialog$5;->this$0:Lcom/android/launcher2/CreateFolderDialog;

    invoke-virtual {v3}, Lcom/android/launcher2/CreateFolderDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0e0076

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-double v6, v3

    add-double/2addr v4, v6

    double-to-int v0, v4

    goto :goto_0
.end method
