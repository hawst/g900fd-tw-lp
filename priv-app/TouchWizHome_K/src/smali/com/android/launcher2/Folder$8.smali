.class Lcom/android/launcher2/Folder$8;
.super Ljava/lang/Object;
.source "Folder.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/launcher2/Folder;->showAddFolderItem()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/launcher2/Folder;

.field final synthetic val$viewAddToast:Landroid/widget/Toast;


# direct methods
.method constructor <init>(Lcom/android/launcher2/Folder;Landroid/widget/Toast;)V
    .locals 0

    .prologue
    .line 1107
    iput-object p1, p0, Lcom/android/launcher2/Folder$8;->this$0:Lcom/android/launcher2/Folder;

    iput-object p2, p0, Lcom/android/launcher2/Folder$8;->val$viewAddToast:Landroid/widget/Toast;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1110
    iget-object v2, p0, Lcom/android/launcher2/Folder$8;->this$0:Lcom/android/launcher2/Folder;

    invoke-virtual {v2}, Lcom/android/launcher2/Folder;->getLeft()I

    move-result v0

    .line 1111
    .local v0, "offsetX":I
    iget-object v2, p0, Lcom/android/launcher2/Folder$8;->this$0:Lcom/android/launcher2/Folder;

    invoke-virtual {v2}, Lcom/android/launcher2/Folder;->getTop()I

    move-result v1

    .line 1112
    .local v1, "offsetY":I
    iget-object v2, p0, Lcom/android/launcher2/Folder$8;->val$viewAddToast:Landroid/widget/Toast;

    const/16 v3, 0x30

    invoke-virtual {v2, v3, v0, v1}, Landroid/widget/Toast;->setGravity(III)V

    .line 1113
    iget-object v2, p0, Lcom/android/launcher2/Folder$8;->val$viewAddToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1114
    const/4 v2, 0x1

    return v2
.end method
