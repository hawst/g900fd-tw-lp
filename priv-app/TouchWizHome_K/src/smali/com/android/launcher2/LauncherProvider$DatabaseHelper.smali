.class public Lcom/android/launcher2/LauncherProvider$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "LauncherProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/launcher2/LauncherProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "DatabaseHelper"
.end annotation


# static fields
.field private static final TAGS_ALLOWED_HOME:I = 0xf

.field private static final TAGS_ALLOWED_HOTSEAT:I = 0x7

.field private static final TAG_ALLOWED_APP:I = 0x1

.field private static final TAG_ALLOWED_FOLDER:I = 0x4

.field private static final TAG_ALLOWED_SHORTCUT:I = 0x2

.field private static final TAG_ALLOWED_WIDGET:I = 0x8

.field private static final TAG_APP:Ljava/lang/String; = "favorite"

.field private static final TAG_APPORDER:Ljava/lang/String; = "appOrder"

.field private static final TAG_APPWIDGET:Ljava/lang/String; = "appwidget"

.field private static final TAG_CLOCK:Ljava/lang/String; = "clock"

.field private static final TAG_FAVORITES:Ljava/lang/String; = "favorites"

.field private static final TAG_FOLDER:Ljava/lang/String; = "folder"

.field private static final TAG_HOME:Ljava/lang/String; = "home"

.field private static final TAG_HOTSEAT:Ljava/lang/String; = "hotseat"

.field private static final TAG_PAGE_COUNT:Ljava/lang/String; = "PageCount"

.field private static final TAG_SACTIVITYWIDGET:Ljava/lang/String; = "sactivitywidget"

.field private static final TAG_SCREEN_INDEX:Ljava/lang/String; = "ScreenIndex"

.field private static final TAG_SEARCH:Ljava/lang/String; = "search"

.field private static final TAG_SHORTCUT:Ljava/lang/String; = "shortcut"


# instance fields
.field private frontMenuApps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

.field private final mContext:Landroid/content/Context;

.field private mFavoritesPath:Ljava/lang/String;

.field private mIsAttReady2GoEnable:Z

.field private mMaxAppOrderId:Ljava/util/concurrent/atomic/AtomicLong;

.field private mMaxFavoriteCPId:Ljava/util/concurrent/atomic/AtomicLong;

.field private mMaxFavoriteId:Ljava/util/concurrent/atomic/AtomicLong;

.field private mOnlyLoadFrontApps:Z

.field private mParserRestore:Lorg/xmlpull/v1/XmlPullParser;

.field private mPreSecretPackageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSamsungActivityWidgetId:I

.field private mSurfaceWidgetId:I

.field private mWidgetTitle:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 596
    const-string v0, "launcher.db"

    const/16 v1, 0xe

    invoke-direct {p0, p1, v0, v2, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 585
    iput-boolean v3, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mIsAttReady2GoEnable:Z

    .line 589
    iput-object v2, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mParserRestore:Lorg/xmlpull/v1/XmlPullParser;

    .line 590
    iput-object v2, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mWidgetTitle:Ljava/lang/String;

    .line 593
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mPreSecretPackageList:Ljava/util/ArrayList;

    .line 1544
    iput-boolean v3, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mOnlyLoadFrontApps:Z

    .line 597
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LauncherProvider"

    const-string v1, "DATABASE_VERSION = 14"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    :cond_0
    iput-object p1, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    .line 600
    new-instance v0, Landroid/appwidget/AppWidgetHost;

    const/16 v1, 0x400

    invoke-direct {v0, p1, v1}, Landroid/appwidget/AppWidgetHost;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    .line 609
    iget-object v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mMaxFavoriteId:Ljava/util/concurrent/atomic/AtomicLong;

    if-nez v0, :cond_1

    .line 610
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->getMaxFavoriteID(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mMaxFavoriteId:Ljava/util/concurrent/atomic/AtomicLong;

    .line 612
    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mMaxAppOrderId:Ljava/util/concurrent/atomic/AtomicLong;

    if-nez v0, :cond_2

    .line 613
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "appOrder"

    invoke-direct {p0, v1, v2}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->initializeMaxId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mMaxAppOrderId:Ljava/util/concurrent/atomic/AtomicLong;

    .line 614
    :cond_2
    return-void
.end method

.method static synthetic access$502(Lcom/android/launcher2/LauncherProvider$DatabaseHelper;Lorg/xmlpull/v1/XmlPullParser;)Lorg/xmlpull/v1/XmlPullParser;
    .locals 0
    .param p0, "x0"    # Lcom/android/launcher2/LauncherProvider$DatabaseHelper;
    .param p1, "x1"    # Lorg/xmlpull/v1/XmlPullParser;

    .prologue
    .line 546
    iput-object p1, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mParserRestore:Lorg/xmlpull/v1/XmlPullParser;

    return-object p1
.end method

.method static synthetic access$600(Lcom/android/launcher2/LauncherProvider$DatabaseHelper;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Z)I
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/LauncherProvider$DatabaseHelper;
    .param p1, "x1"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z

    .prologue
    .line 546
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->loadFavorites(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method

.method private addAppFolder(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 2194
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->generateNewAppOrderId()J

    move-result-wide v0

    .line 2195
    .local v0, "id":J
    const-string v2, "_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2196
    const-string v2, "appOrder"

    const/4 v3, 0x0

    invoke-static {p0, p1, v2, v3, p2}, Lcom/android/launcher2/LauncherProvider;->dbInsertAndCheck(Lcom/android/launcher2/LauncherProvider$DatabaseHelper;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 2197
    const-wide/16 v0, -0x1

    .line 2198
    :cond_0
    return-wide v0
.end method

.method private addAppShortcut(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)J
    .locals 14
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "pkgMgr"    # Landroid/content/pm/PackageManager;
    .param p4, "pkgName"    # Ljava/lang/String;
    .param p5, "className"    # Ljava/lang/String;

    .prologue
    .line 2104
    const-wide/16 v6, -0x1

    .line 2106
    .local v6, "id":J
    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    .line 2107
    new-instance v2, Landroid/content/ComponentName;

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-direct {v2, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2111
    .local v2, "cn":Landroid/content/ComponentName;
    const/4 v9, 0x0

    :try_start_0
    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v9}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 2118
    .local v5, "info":Landroid/content/pm/ActivityInfo;
    :goto_0
    :try_start_1
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->generateNewAppOrderId()J

    move-result-wide v6

    .line 2119
    const-string v9, "_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2120
    const-string v9, "componentName"

    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2121
    const-string v9, "title"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2130
    const-string v9, "appOrder"

    const/4 v10, 0x0

    move-object/from16 v0, p2

    invoke-static {p0, p1, v9, v10, v0}, Lcom/android/launcher2/LauncherProvider;->dbInsertAndCheck(Lcom/android/launcher2/LauncherProvider$DatabaseHelper;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-gez v9, :cond_0

    .line 2131
    const-wide/16 v6, -0x1

    .line 2137
    .end local v2    # "cn":Landroid/content/ComponentName;
    .end local v5    # "info":Landroid/content/pm/ActivityInfo;
    :cond_0
    :goto_1
    return-wide v6

    .line 2113
    .restart local v2    # "cn":Landroid/content/ComponentName;
    :catch_0
    move-exception v4

    .line 2114
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object p4, v9, v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v9}, Landroid/content/pm/PackageManager;->currentToCanonicalPackageNames([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 2115
    .local v8, "packages":[Ljava/lang/String;
    new-instance v3, Landroid/content/ComponentName;

    const/4 v9, 0x0

    aget-object v9, v8, v9

    move-object/from16 v0, p5

    invoke-direct {v3, v9, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2116
    .end local v2    # "cn":Landroid/content/ComponentName;
    .local v3, "cn":Landroid/content/ComponentName;
    const/4 v9, 0x0

    :try_start_2
    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v9}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v5

    .restart local v5    # "info":Landroid/content/pm/ActivityInfo;
    move-object v2, v3

    .end local v3    # "cn":Landroid/content/ComponentName;
    .restart local v2    # "cn":Landroid/content/ComponentName;
    goto :goto_0

    .line 2133
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v5    # "info":Landroid/content/pm/ActivityInfo;
    .end local v8    # "packages":[Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 2134
    .restart local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_2
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "LauncherProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unable to get ActivityInfo for : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 2133
    .end local v2    # "cn":Landroid/content/ComponentName;
    .restart local v3    # "cn":Landroid/content/ComponentName;
    .restart local v8    # "packages":[Ljava/lang/String;
    :catch_2
    move-exception v4

    move-object v2, v3

    .end local v3    # "cn":Landroid/content/ComponentName;
    .restart local v2    # "cn":Landroid/content/ComponentName;
    goto :goto_2
.end method

.method private addAppWidget(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ComponentName;II)Z
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "values"    # Landroid/content/ContentValues;
    .param p4, "cn"    # Landroid/content/ComponentName;
    .param p5, "spanX"    # I
    .param p6, "spanY"    # I

    .prologue
    .line 2311
    const/4 v0, 0x0

    .line 2312
    .local v0, "allocatedAppWidgets":Z
    iget-object v6, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 2315
    .local v2, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    const/4 v1, -0x1

    .line 2317
    .local v1, "appWidgetId":I
    :try_start_0
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->generateNewFavoritesId()J

    move-result-wide v4

    .line 2329
    .local v4, "genNewFavoritesId":J
    iget-object v6, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    invoke-virtual {v6}, Landroid/appwidget/AppWidgetHost;->allocateAppWidgetId()I

    move-result v1

    .line 2330
    const-string v6, "itemType"

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2331
    const-string v6, "spanX"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2332
    const-string v6, "spanY"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2333
    const-string v6, "appWidgetId"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2334
    const-string v6, "_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2336
    const/4 v6, 0x0

    invoke-static {p0, p1, p2, v6, p3}, Lcom/android/launcher2/LauncherProvider;->dbInsertAndCheck(Lcom/android/launcher2/LauncherProvider$DatabaseHelper;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2337
    const/4 v0, 0x1

    .line 2339
    const/4 v6, -0x1

    if-le v1, v6, :cond_0

    invoke-virtual {v2, v1, p4}, Landroid/appwidget/AppWidgetManager;->bindAppWidgetIdIfAllowed(ILandroid/content/ComponentName;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2340
    const-string v6, "LauncherProvider"

    const-string v7, "Problem allocating appWidgetId; bindAppWidgetIdIfAllowed returned false"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2347
    .end local v4    # "genNewFavoritesId":J
    :cond_0
    :goto_0
    return v0

    .line 2342
    :catch_0
    move-exception v3

    .line 2343
    .local v3, "ex":Ljava/lang/RuntimeException;
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2344
    const-string v6, "LauncherProvider"

    const-string v7, "Problem allocating appWidgetId"

    invoke-static {v6, v7, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private addAppWidget(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;II)Z
    .locals 17
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "values"    # Landroid/content/ContentValues;
    .param p4, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p5, "packageName"    # Ljava/lang/String;
    .param p6, "className"    # Ljava/lang/String;
    .param p7, "spanX"    # I
    .param p8, "spanY"    # I

    .prologue
    .line 2265
    if-eqz p5, :cond_0

    if-nez p6, :cond_1

    .line 2266
    :cond_0
    const/4 v2, 0x0

    .line 2306
    :goto_0
    return v2

    .line 2269
    :cond_1
    const/4 v15, 0x1

    .line 2270
    .local v15, "hasPackage":Z
    new-instance v6, Landroid/content/ComponentName;

    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-direct {v6, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2272
    .local v6, "cn":Landroid/content/ComponentName;
    const/4 v2, 0x0

    :try_start_0
    move-object/from16 v0, p4

    invoke-virtual {v0, v6, v2}, Landroid/content/pm/PackageManager;->getReceiverInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2302
    :cond_2
    :goto_1
    if-eqz v15, :cond_3

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move/from16 v7, p7

    move/from16 v8, p8

    .line 2303
    invoke-direct/range {v2 .. v8}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->addAppWidget(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ComponentName;II)Z

    move-result v2

    goto :goto_0

    .line 2273
    :catch_0
    move-exception v12

    .line 2274
    .local v12, "e":Ljava/lang/Exception;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p5, v2, v3

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->currentToCanonicalPackageNames([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 2276
    .local v16, "packages":[Ljava/lang/String;
    new-instance v6, Landroid/content/ComponentName;

    .end local v6    # "cn":Landroid/content/ComponentName;
    const/4 v2, 0x0

    aget-object v2, v16, v2

    move-object/from16 v0, p6

    invoke-direct {v6, v2, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2278
    .restart local v6    # "cn":Landroid/content/ComponentName;
    const/4 v2, 0x0

    :try_start_1
    move-object/from16 v0, p4

    invoke-virtual {v0, v6, v2}, Landroid/content/pm/PackageManager;->getReceiverInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 2279
    :catch_1
    move-exception v13

    .line 2282
    .local v13, "e1":Ljava/lang/Exception;
    :try_start_2
    # getter for: Lcom/android/launcher2/LauncherProvider;->mIsRestoreHomeScreen:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$200()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2283
    const-string v11, "com.sec.android.app.launcher"

    .line 2284
    .local v11, "dummyPackageName":Ljava/lang/String;
    const-string v10, "com.android.launcher2.WidgetReceiver"

    .line 2285
    .local v10, "dummyClassName":Ljava/lang/String;
    new-instance v9, Landroid/content/ComponentName;

    invoke-direct {v9, v11, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 2286
    .end local v6    # "cn":Landroid/content/ComponentName;
    .local v9, "cn":Landroid/content/ComponentName;
    const/4 v2, 0x0

    :try_start_3
    move-object/from16 v0, p4

    invoke-virtual {v0, v9, v2}, Landroid/content/pm/PackageManager;->getReceiverInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    .line 2288
    const-string v2, "intent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p6

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2289
    const-string v2, "title"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mWidgetTitle:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-object v6, v9

    .end local v9    # "cn":Landroid/content/ComponentName;
    .restart local v6    # "cn":Landroid/content/ComponentName;
    goto :goto_1

    .line 2295
    .end local v10    # "dummyClassName":Ljava/lang/String;
    .end local v11    # "dummyPackageName":Ljava/lang/String;
    :catch_2
    move-exception v14

    .line 2296
    .local v14, "e2":Ljava/lang/Exception;
    :goto_2
    const/4 v15, 0x0

    goto :goto_1

    .line 2306
    .end local v12    # "e":Ljava/lang/Exception;
    .end local v13    # "e1":Ljava/lang/Exception;
    .end local v14    # "e2":Ljava/lang/Exception;
    .end local v16    # "packages":[Ljava/lang/String;
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2295
    .end local v6    # "cn":Landroid/content/ComponentName;
    .restart local v9    # "cn":Landroid/content/ComponentName;
    .restart local v10    # "dummyClassName":Ljava/lang/String;
    .restart local v11    # "dummyPackageName":Ljava/lang/String;
    .restart local v12    # "e":Ljava/lang/Exception;
    .restart local v13    # "e1":Ljava/lang/Exception;
    .restart local v16    # "packages":[Ljava/lang/String;
    :catch_3
    move-exception v14

    move-object v6, v9

    .end local v9    # "cn":Landroid/content/ComponentName;
    .restart local v6    # "cn":Landroid/content/ComponentName;
    goto :goto_2
.end method

.method private addClockWidget(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v5, 0x2

    .line 2257
    new-instance v4, Landroid/content/ComponentName;

    const-string v0, "com.android.alarmclock"

    const-string v1, "com.android.alarmclock.AnalogAppWidgetProvider"

    invoke-direct {v4, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .local v4, "cn":Landroid/content/ComponentName;
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v6, v5

    .line 2259
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->addAppWidget(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ComponentName;II)Z

    move-result v0

    return v0
.end method

.method private addFolder(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)J
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v4, 0x1

    .line 2207
    const-string v2, "itemType"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2208
    const-string v2, "spanX"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2209
    const-string v2, "spanY"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2210
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->generateNewFavoritesId()J

    move-result-wide v0

    .line 2211
    .local v0, "id":J
    const-string v2, "_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2212
    const/4 v2, 0x0

    invoke-static {p0, p1, p2, v2, p3}, Lcom/android/launcher2/LauncherProvider;->dbInsertAndCheck(Lcom/android/launcher2/LauncherProvider$DatabaseHelper;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 2213
    const-wide/16 v0, -0x1

    .line 2215
    .end local v0    # "id":J
    :cond_0
    return-wide v0
.end method

.method private addHomeShortcut(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/pm/PackageManager;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 14
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "values"    # Landroid/content/ContentValues;
    .param p4, "pkgMgr"    # Landroid/content/pm/PackageManager;
    .param p5, "intent"    # Landroid/content/Intent;
    .param p6, "pkgName"    # Ljava/lang/String;
    .param p7, "className"    # Ljava/lang/String;
    .param p8, "title"    # Ljava/lang/String;

    .prologue
    .line 2142
    const-wide/16 v6, -0x1

    .line 2144
    .local v6, "id":J
    if-eqz p6, :cond_0

    if-eqz p7, :cond_0

    .line 2145
    new-instance v2, Landroid/content/ComponentName;

    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-direct {v2, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2149
    .local v2, "cn":Landroid/content/ComponentName;
    const/4 v9, 0x0

    :try_start_0
    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v9}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 2156
    .local v5, "info":Landroid/content/pm/ActivityInfo;
    :goto_0
    :try_start_1
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->generateNewFavoritesId()J

    move-result-wide v6

    .line 2157
    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2158
    const/high16 v9, 0x10200000

    move-object/from16 v0, p5

    invoke-virtual {v0, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2160
    const-string v9, "intent"

    const/4 v10, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v10}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2161
    const-string v9, "title"

    move-object/from16 v0, p4

    invoke-virtual {v5, v0}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2162
    const-string v9, "itemType"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2163
    const-string v9, "spanX"

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2164
    const-string v9, "spanY"

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2165
    const-string v9, "_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2166
    const/4 v9, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {p0, p1, v0, v9, v1}, Lcom/android/launcher2/LauncherProvider;->dbInsertAndCheck(Lcom/android/launcher2/LauncherProvider$DatabaseHelper;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-gez v9, :cond_0

    .line 2167
    const-wide/16 v6, -0x1

    .line 2190
    .end local v2    # "cn":Landroid/content/ComponentName;
    .end local v5    # "info":Landroid/content/pm/ActivityInfo;
    :cond_0
    :goto_1
    return-wide v6

    .line 2150
    .restart local v2    # "cn":Landroid/content/ComponentName;
    :catch_0
    move-exception v4

    .line 2151
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object p6, v9, v10

    move-object/from16 v0, p4

    invoke-virtual {v0, v9}, Landroid/content/pm/PackageManager;->currentToCanonicalPackageNames([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 2153
    .local v8, "packages":[Ljava/lang/String;
    new-instance v3, Landroid/content/ComponentName;

    const/4 v9, 0x0

    aget-object v9, v8, v9

    move-object/from16 v0, p7

    invoke-direct {v3, v9, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2154
    .end local v2    # "cn":Landroid/content/ComponentName;
    .local v3, "cn":Landroid/content/ComponentName;
    const/4 v9, 0x0

    :try_start_2
    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v9}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v5

    .restart local v5    # "info":Landroid/content/pm/ActivityInfo;
    move-object v2, v3

    .end local v3    # "cn":Landroid/content/ComponentName;
    .restart local v2    # "cn":Landroid/content/ComponentName;
    goto/16 :goto_0

    .line 2168
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v5    # "info":Landroid/content/pm/ActivityInfo;
    .end local v8    # "packages":[Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 2169
    .restart local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_2
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v9

    if-eqz v9, :cond_1

    const-string v9, "LauncherProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unable to add Home app: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p6

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p7

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2173
    :cond_1
    # getter for: Lcom/android/launcher2/LauncherProvider;->mIsRestoreHomeScreen:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$200()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2174
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->generateNewFavoritesId()J

    move-result-wide v6

    .line 2175
    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2176
    const/high16 v9, 0x10200000

    move-object/from16 v0, p5

    invoke-virtual {v0, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2178
    const-string v9, "intent"

    const/4 v10, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v10}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2179
    const-string v9, "title"

    move-object/from16 v0, p3

    move-object/from16 v1, p8

    invoke-virtual {v0, v9, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2180
    const-string v9, "itemType"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2181
    const-string v9, "spanX"

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2182
    const-string v9, "spanY"

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2183
    const-string v9, "_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2184
    const/4 v9, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {p0, p1, v0, v9, v1}, Lcom/android/launcher2/LauncherProvider;->dbInsertAndCheck(Lcom/android/launcher2/LauncherProvider$DatabaseHelper;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-gez v9, :cond_0

    .line 2185
    const-wide/16 v6, -0x1

    goto/16 :goto_1

    .line 2168
    .end local v2    # "cn":Landroid/content/ComponentName;
    .restart local v3    # "cn":Landroid/content/ComponentName;
    .restart local v8    # "packages":[Ljava/lang/String;
    :catch_2
    move-exception v4

    move-object v2, v3

    .end local v3    # "cn":Landroid/content/ComponentName;
    .restart local v2    # "cn":Landroid/content/ComponentName;
    goto/16 :goto_2
.end method

.method private addSamsungActivityWidget(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Z
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "values"    # Landroid/content/ContentValues;
    .param p4, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p5, "pkgName"    # Ljava/lang/String;
    .param p6, "cName"    # Ljava/lang/String;
    .param p7, "themeName"    # Ljava/lang/String;
    .param p8, "spanX"    # I
    .param p9, "spanY"    # I

    .prologue
    .line 2449
    const/4 v2, 0x0

    .line 2450
    .local v2, "rc":Z
    if-eqz p5, :cond_1

    if-eqz p6, :cond_1

    .line 2451
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, p5, p6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2452
    .local v0, "cn":Landroid/content/ComponentName;
    sget-object v3, Lcom/android/launcher2/SamsungWidgetPackageManager;->INSTANCE:Lcom/android/launcher2/SamsungWidgetPackageManager;

    invoke-virtual {v3, v0, p7}, Lcom/android/launcher2/SamsungWidgetPackageManager;->findWidget(Landroid/content/ComponentName;Ljava/lang/String;)Lcom/android/launcher2/SamsungWidgetProviderInfo;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2453
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2454
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2455
    if-eqz p7, :cond_0

    .line 2456
    const-string v3, "com.samsung.sec.android.SAMSUNG_WIDGET.themename"

    invoke-virtual {v1, v3, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2458
    :cond_0
    const-string v3, "intent"

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459
    const-string v3, "itemType"

    const/16 v4, 0x384

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2460
    const-string v3, "spanX"

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2461
    const-string v3, "spanY"

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2462
    const-string v3, "appWidgetId"

    iget v4, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mSamsungActivityWidgetId:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mSamsungActivityWidgetId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2463
    const-string v3, "_id"

    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->generateNewFavoritesId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2464
    const/4 v3, 0x0

    invoke-static {p0, p1, p2, v3, p3}, Lcom/android/launcher2/LauncherProvider;->dbInsertAndCheck(Lcom/android/launcher2/LauncherProvider$DatabaseHelper;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-ltz v3, :cond_2

    const/4 v2, 0x1

    .line 2467
    .end local v0    # "cn":Landroid/content/ComponentName;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return v2

    .line 2464
    .restart local v0    # "cn":Landroid/content/ComponentName;
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private addSearchWidget(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;II)Z
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "values"    # Landroid/content/ContentValues;
    .param p4, "spanX"    # I
    .param p5, "spanY"    # I

    .prologue
    .line 2250
    invoke-direct {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->getSearchWidgetProvider()Landroid/content/ComponentName;

    move-result-object v4

    .line 2251
    .local v4, "cn":Landroid/content/ComponentName;
    if-eqz v4, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    move v6, p5

    .line 2252
    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->addAppWidget(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ComponentName;II)Z

    move-result v0

    .line 2253
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private addSurfaceWidget(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Z
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "values"    # Landroid/content/ContentValues;
    .param p4, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p5, "pkgName"    # Ljava/lang/String;
    .param p6, "cName"    # Ljava/lang/String;
    .param p7, "themeName"    # Ljava/lang/String;
    .param p8, "instance"    # Ljava/lang/String;
    .param p9, "spanX"    # I
    .param p10, "spanY"    # I

    .prologue
    .line 2485
    const/4 v2, 0x0

    .line 2486
    .local v2, "rc":Z
    if-eqz p5, :cond_1

    if-eqz p6, :cond_1

    .line 2487
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, p5, p6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2488
    .local v0, "cn":Landroid/content/ComponentName;
    sget-object v3, Lcom/android/launcher2/SurfaceWidgetPackageManager;->INST:Lcom/android/launcher2/SurfaceWidgetPackageManager;

    invoke-virtual {v3, v0, p7}, Lcom/android/launcher2/SurfaceWidgetPackageManager;->findWidget(Landroid/content/ComponentName;Ljava/lang/String;)Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2489
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2490
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2491
    if-eqz p7, :cond_0

    .line 2492
    const-string v3, "com.samsung.sec.android.SURFACE_WIDGET.themename"

    invoke-virtual {v1, v3, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2494
    :cond_0
    if-eqz p8, :cond_2

    invoke-virtual {p8}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2495
    const-string v3, "instance"

    invoke-static {p8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2502
    :goto_0
    const-string v3, "intent"

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2503
    const-string v3, "itemType"

    const/16 v4, 0x385

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2504
    const-string v3, "spanX"

    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2505
    const-string v3, "spanY"

    invoke-static/range {p10 .. p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2506
    const-string v3, "_id"

    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->generateNewFavoritesId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2507
    const-string v3, "appWidgetId"

    iget v4, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mSurfaceWidgetId:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mSurfaceWidgetId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2508
    const/4 v3, 0x0

    invoke-static {p0, p1, p2, v3, p3}, Lcom/android/launcher2/LauncherProvider;->dbInsertAndCheck(Lcom/android/launcher2/LauncherProvider$DatabaseHelper;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2509
    const/4 v2, 0x1

    .line 2512
    .end local v0    # "cn":Landroid/content/ComponentName;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    return v2

    .line 2500
    .restart local v0    # "cn":Landroid/content/ComponentName;
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_2
    const-string v3, "instance"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private addUriShortcut(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/res/TypedArray;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 24
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "values"    # Landroid/content/ContentValues;
    .param p4, "a"    # Landroid/content/res/TypedArray;
    .param p5, "uri"    # Ljava/lang/String;
    .param p6, "titleId"    # Ljava/lang/String;
    .param p7, "imgId"    # Ljava/lang/String;

    .prologue
    .line 2351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 2353
    .local v16, "r":Landroid/content/res/Resources;
    const/4 v12, 0x0

    .line 2354
    .local v12, "iconResId":I
    const/16 v19, 0x0

    .line 2356
    .local v19, "titleResId":I
    if-eqz p4, :cond_0

    .line 2357
    const/16 v20, 0x7

    const/16 v21, 0x0

    move-object/from16 v0, p4

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v12

    .line 2358
    const/16 v20, 0x8

    const/16 v21, 0x0

    move-object/from16 v0, p4

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v19

    .line 2364
    :cond_0
    if-eqz p4, :cond_1

    .line 2365
    const/16 v20, 0x9

    :try_start_0
    move-object/from16 v0, p4

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p5

    .line 2367
    :cond_1
    const/16 v20, 0x0

    move-object/from16 v0, p5

    move/from16 v1, v20

    invoke-static {v0, v1}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 2373
    .local v13, "intent":Landroid/content/Intent;
    if-eqz p4, :cond_5

    if-eqz v12, :cond_2

    if-nez v19, :cond_5

    .line 2374
    :cond_2
    const-string v20, "LauncherProvider"

    const-string v21, "Shortcut is missing title or icon resource ID"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2375
    const-wide/16 v14, -0x1

    .line 2442
    .end local v13    # "intent":Landroid/content/Intent;
    :cond_3
    :goto_0
    return-wide v14

    .line 2368
    :catch_0
    move-exception v10

    .line 2369
    .local v10, "e":Ljava/net/URISyntaxException;
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v20

    if-eqz v20, :cond_4

    const-string v20, "LauncherProvider"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Shortcut has malformed uri: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2370
    :cond_4
    const-wide/16 v14, -0x1

    goto :goto_0

    .line 2378
    .end local v10    # "e":Ljava/net/URISyntaxException;
    .restart local v13    # "intent":Landroid/content/Intent;
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->generateNewFavoritesId()J

    move-result-wide v14

    .line 2379
    .local v14, "id":J
    const/high16 v20, 0x10000000

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2380
    const-string v20, "intent"

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2381
    const-string v20, "itemType"

    const/16 v21, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2382
    const-string v20, "spanX"

    const/16 v21, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2383
    const-string v20, "spanY"

    const/16 v21, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2384
    const-string v20, "_id"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2385
    if-eqz p4, :cond_6

    .line 2386
    const-string v20, "iconType"

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2387
    const-string v20, "iconPackage"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2389
    const-string v20, "title"

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2390
    const-string v20, "iconResource"

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2439
    :goto_1
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v20

    move-object/from16 v4, p3

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/launcher2/LauncherProvider;->dbInsertAndCheck(Lcom/android/launcher2/LauncherProvider$DatabaseHelper;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v20

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-gez v20, :cond_3

    .line 2440
    const-wide/16 v14, -0x1

    goto/16 :goto_0

    .line 2393
    :cond_6
    const-string v6, ""

    .line 2394
    .local v6, "appTitle":Ljava/lang/String;
    const/4 v8, 0x0

    .line 2396
    .local v8, "d":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p6

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getCSCPackageItemText(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    .end local v6    # "appTitle":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 2397
    .restart local v6    # "appTitle":Ljava/lang/String;
    const-string v20, "title"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2399
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p7

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getCSCPackageItemIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 2400
    if-eqz v6, :cond_7

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_7

    if-nez v8, :cond_8

    .line 2403
    :cond_7
    # getter for: Lcom/android/launcher2/LauncherProvider;->mIsRestoreHomeScreen:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$200()Z

    move-result v20

    if-eqz v20, :cond_a

    .line 2404
    if-nez v6, :cond_8

    .line 2405
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mWidgetTitle:Ljava/lang/String;

    .line 2406
    const-string v20, "title"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2415
    :cond_8
    new-instance v17, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v17 .. v17}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2416
    .local v17, "stream":Ljava/io/ByteArrayOutputStream;
    const/4 v7, 0x0

    .line 2419
    .local v7, "bitmap":Landroid/graphics/Bitmap;
    # getter for: Lcom/android/launcher2/LauncherProvider;->mIsRestoreHomeScreen:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$200()Z

    move-result v20

    if-eqz v20, :cond_b

    if-nez v8, :cond_b

    .line 2420
    # getter for: Lcom/android/launcher2/LauncherProvider;->mBasePathForRestore:Ljava/lang/String;
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$300()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p7

    # invokes: Lcom/android/launcher2/LauncherProvider;->getBitmapFromBackupPath(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;
    invoke-static {v0, v1}, Lcom/android/launcher2/LauncherProvider;->access$400(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 2421
    if-nez v7, :cond_9

    .line 2422
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v18

    .line 2423
    .local v18, "sysRes":Landroid/content/res/Resources;
    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    .line 2424
    .local v9, "dm":Landroid/util/DisplayMetrics;
    iget v11, v9, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 2425
    .local v11, "iconDpi":I
    const/high16 v20, 0x10d0000

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1, v11}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    move-object/from16 v20, v8

    .line 2426
    check-cast v20, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    .line 2433
    .end local v9    # "dm":Landroid/util/DisplayMetrics;
    .end local v11    # "iconDpi":I
    .end local v18    # "sysRes":Landroid/content/res/Resources;
    :cond_9
    :goto_2
    sget-object v20, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v21, 0x64

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v17

    invoke-virtual {v7, v0, v1, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2435
    const-string v20, "iconType"

    const/16 v21, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2436
    const-string v20, "icon"

    invoke-virtual/range {v17 .. v17}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v21

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto/16 :goto_1

    .line 2410
    .end local v7    # "bitmap":Landroid/graphics/Bitmap;
    .end local v17    # "stream":Ljava/io/ByteArrayOutputStream;
    :cond_a
    const-string v20, "LauncherProvider"

    const-string v21, "Shortcut is missing title or icon resource ID from csc resource"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2411
    const-wide/16 v14, -0x1

    goto/16 :goto_0

    .restart local v7    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v17    # "stream":Ljava/io/ByteArrayOutputStream;
    :cond_b
    move-object/from16 v20, v8

    .line 2430
    check-cast v20, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    goto :goto_2
.end method

.method private convertDatabase(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v8, 0x0

    .line 755
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "LauncherProvider"

    const-string v3, "converting database from an older format, but not onUpgrade"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 756
    :cond_0
    const/4 v6, 0x0

    .line 758
    .local v6, "converted":Z
    const-string v2, "content://settings/old_favorites?notify=true"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 760
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 761
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v7, 0x0

    .line 764
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 770
    :goto_0
    if-eqz v7, :cond_2

    .line 772
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 773
    invoke-direct {p0, p1, v7}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->copyFromCursor(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-lez v2, :cond_5

    const/4 v6, 0x1

    .line 776
    :cond_1
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 779
    if-eqz v6, :cond_2

    .line 780
    invoke-virtual {v0, v1, v8, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 784
    :cond_2
    if-eqz v6, :cond_4

    .line 786
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "LauncherProvider"

    const-string v3, "converted and now triggering widget upgrade"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    :cond_3
    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->convertWidgets(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 790
    :cond_4
    return v6

    .line 773
    :cond_5
    const/4 v6, 0x0

    goto :goto_1

    .line 776
    :catchall_0
    move-exception v2

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v2

    .line 765
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private convertWidgets(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 22
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1329
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v13

    .line 1330
    .local v13, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    const/4 v4, 0x3

    new-array v14, v4, [I

    fill-array-data v14, :array_0

    .line 1336
    .local v14, "bindSources":[I
    const-string v4, "itemType"

    invoke-static {v4, v14}, Lcom/android/launcher2/LauncherProvider;->buildOrWhereString(Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object v7

    .line 1338
    .local v7, "selectWhere":Ljava/lang/String;
    const/4 v15, 0x0

    .line 1340
    .local v15, "c":Landroid/database/Cursor;
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1343
    :try_start_0
    const-string v5, "favorites"

    const/4 v4, 0x2

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v4

    const/4 v4, 0x1

    const-string v8, "itemType"

    aput-object v8, v6, v4

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 1346
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v5, "LauncherProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "found upgrade cursor count="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v15, :cond_5

    const-string v4, "null"

    :goto_0
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1348
    :cond_0
    new-instance v21, Landroid/content/ContentValues;

    invoke-direct/range {v21 .. v21}, Landroid/content/ContentValues;-><init>()V

    .line 1349
    .local v21, "values":Landroid/content/ContentValues;
    :cond_1
    :goto_1
    if-eqz v15, :cond_a

    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1350
    const/4 v4, 0x0

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 1351
    .local v18, "favoriteId":J
    const/4 v4, 0x1

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v17

    .line 1355
    .local v17, "favoriteType":I
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    invoke-virtual {v4}, Landroid/appwidget/AppWidgetHost;->allocateAppWidgetId()I

    move-result v12

    .line 1357
    .local v12, "appWidgetId":I
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1358
    const-string v4, "LauncherProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "allocated appWidgetId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for favoriteId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1361
    :cond_2
    invoke-virtual/range {v21 .. v21}, Landroid/content/ContentValues;->clear()V

    .line 1362
    const-string v4, "itemType"

    const/4 v5, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1363
    const-string v4, "appWidgetId"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1366
    const/16 v4, 0x3e9

    move/from16 v0, v17

    if-ne v0, v4, :cond_6

    .line 1367
    const-string v4, "spanX"

    const/4 v5, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1368
    const-string v4, "spanY"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1374
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1375
    .local v20, "updateWhere":Ljava/lang/String;
    const-string v4, "favorites"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move-object/from16 v2, v20

    invoke-virtual {v0, v4, v1, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1377
    const/16 v4, 0x3e8

    move/from16 v0, v17

    if-ne v0, v4, :cond_8

    .line 1378
    new-instance v4, Landroid/content/ComponentName;

    const-string v5, "com.android.alarmclock"

    const-string v6, "com.android.alarmclock.AnalogAppWidgetProvider"

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v12, v4}, Landroid/appwidget/AppWidgetManager;->bindAppWidgetIdIfAllowed(ILandroid/content/ComponentName;)Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 1389
    .end local v12    # "appWidgetId":I
    .end local v20    # "updateWhere":Ljava/lang/String;
    :catch_0
    move-exception v16

    .line 1390
    .local v16, "ex":Ljava/lang/RuntimeException;
    :try_start_2
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "LauncherProvider"

    const-string v5, "Problem allocating appWidgetId"

    move-object/from16 v0, v16

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 1395
    .end local v16    # "ex":Ljava/lang/RuntimeException;
    .end local v17    # "favoriteType":I
    .end local v18    # "favoriteId":J
    .end local v21    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v16

    .line 1396
    .local v16, "ex":Landroid/database/SQLException;
    :try_start_3
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "LauncherProvider"

    const-string v5, "Problem while allocating appWidgetIds for existing widgets"

    move-object/from16 v0, v16

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1398
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1399
    if-eqz v15, :cond_4

    .line 1400
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 1403
    .end local v16    # "ex":Landroid/database/SQLException;
    :cond_4
    :goto_3
    return-void

    .line 1346
    :cond_5
    :try_start_4
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v4

    goto/16 :goto_0

    .line 1370
    .restart local v12    # "appWidgetId":I
    .restart local v17    # "favoriteType":I
    .restart local v18    # "favoriteId":J
    .restart local v21    # "values":Landroid/content/ContentValues;
    :cond_6
    :try_start_5
    const-string v4, "spanX"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1371
    const-string v4, "spanY"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_2

    .line 1398
    .end local v12    # "appWidgetId":I
    .end local v17    # "favoriteType":I
    .end local v18    # "favoriteId":J
    .end local v21    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v4

    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1399
    if-eqz v15, :cond_7

    .line 1400
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v4

    .line 1381
    .restart local v12    # "appWidgetId":I
    .restart local v17    # "favoriteType":I
    .restart local v18    # "favoriteId":J
    .restart local v20    # "updateWhere":Ljava/lang/String;
    .restart local v21    # "values":Landroid/content/ContentValues;
    :cond_8
    const/16 v4, 0x3ea

    move/from16 v0, v17

    if-ne v0, v4, :cond_9

    .line 1382
    :try_start_6
    new-instance v4, Landroid/content/ComponentName;

    const-string v5, "com.android.camera"

    const-string v6, "com.android.camera.PhotoAppWidgetProvider"

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v12, v4}, Landroid/appwidget/AppWidgetManager;->bindAppWidgetIdIfAllowed(ILandroid/content/ComponentName;)Z

    goto/16 :goto_1

    .line 1385
    :cond_9
    const/16 v4, 0x3e9

    move/from16 v0, v17

    if-ne v0, v4, :cond_1

    .line 1386
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->getSearchWidgetProvider()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v13, v12, v4}, Landroid/appwidget/AppWidgetManager;->bindAppWidgetIdIfAllowed(ILandroid/content/ComponentName;)Z
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 1394
    .end local v12    # "appWidgetId":I
    .end local v17    # "favoriteType":I
    .end local v18    # "favoriteId":J
    .end local v20    # "updateWhere":Ljava/lang/String;
    :cond_a
    :try_start_7
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_7
    .catch Landroid/database/SQLException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1398
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1399
    if-eqz v15, :cond_4

    .line 1400
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 1330
    nop

    :array_0
    .array-data 4
        0x3e8
        0x3ea
        0x3e9
    .end array-data
.end method

.method private copyFromCursor(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;)I
    .locals 28
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "c"    # Landroid/database/Cursor;

    .prologue
    .line 794
    const-string v24, "_id"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    .line 795
    .local v15, "idIndex":I
    const-string v24, "intent"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v16

    .line 796
    .local v16, "intentIndex":I
    const-string v24, "title"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v21

    .line 797
    .local v21, "titleIndex":I
    const-string v24, "iconType"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    .line 798
    .local v14, "iconTypeIndex":I
    const-string v24, "icon"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 799
    .local v11, "iconIndex":I
    const-string v24, "iconPackage"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 800
    .local v12, "iconPackageIndex":I
    const-string v24, "iconResource"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    .line 801
    .local v13, "iconResourceIndex":I
    const-string v24, "container"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 802
    .local v8, "containerIndex":I
    const-string v24, "itemType"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    .line 803
    .local v17, "itemTypeIndex":I
    const-string v24, "screen"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v20

    .line 804
    .local v20, "screenIndex":I
    const-string v24, "cellX"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 805
    .local v6, "cellXIndex":I
    const-string v24, "cellY"

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 807
    .local v7, "cellYIndex":I
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v24

    move/from16 v0, v24

    new-array v0, v0, [Landroid/content/ContentValues;

    move-object/from16 v19, v0

    .line 808
    .local v19, "rows":[Landroid/content/ContentValues;
    const/4 v9, 0x0

    .line 809
    .local v9, "i":I
    :goto_0
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v24

    if-eqz v24, :cond_0

    .line 810
    new-instance v23, Landroid/content/ContentValues;

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getColumnCount()I

    move-result v24

    invoke-direct/range {v23 .. v24}, Landroid/content/ContentValues;-><init>(I)V

    .line 811
    .local v23, "values":Landroid/content/ContentValues;
    const-string v24, "_id"

    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 812
    const-string v24, "intent"

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    const-string v24, "title"

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    const-string v24, "iconType"

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 815
    const-string v24, "icon"

    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 816
    const-string v24, "iconPackage"

    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    const-string v24, "iconResource"

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 818
    const-string v24, "container"

    move-object/from16 v0, p2

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 819
    const-string v24, "itemType"

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 820
    const-string v24, "appWidgetId"

    const/16 v25, -0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 821
    const-string v24, "screen"

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 822
    const-string v24, "cellX"

    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 823
    const-string v24, "cellY"

    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 824
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "i":I
    .local v10, "i":I
    aput-object v23, v19, v9

    move v9, v10

    .line 825
    .end local v10    # "i":I
    .restart local v9    # "i":I
    goto/16 :goto_0

    .line 827
    .end local v23    # "values":Landroid/content/ContentValues;
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 828
    const/16 v22, 0x0

    .line 830
    .local v22, "total":I
    :try_start_0
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v18, v0

    .line 831
    .local v18, "numValues":I
    const/4 v9, 0x0

    :goto_1
    move/from16 v0, v18

    if-ge v9, v0, :cond_2

    .line 832
    const-string v24, "favorites"

    const/16 v25, 0x0

    aget-object v26, v19, v9

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    move-object/from16 v4, v26

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/launcher2/LauncherProvider;->dbInsertAndCheck(Lcom/android/launcher2/LauncherProvider$DatabaseHelper;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v24

    const-wide/16 v26, 0x0

    cmp-long v24, v24, v26

    if-gez v24, :cond_1

    .line 833
    const/16 v22, 0x0

    .line 839
    .end local v22    # "total":I
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 842
    :goto_2
    return v22

    .line 835
    .restart local v22    # "total":I
    :cond_1
    add-int/lit8 v22, v22, 0x1

    .line 831
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 837
    :cond_2
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 839
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_2

    .end local v18    # "numValues":I
    :catchall_0
    move-exception v24

    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v24
.end method

.method private createAppOrderTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 686
    const-string v0, "CREATE table appOrder (_id integer primary key,folderId integer not null default -1,screen integer not null default -1,cell integer not null default -1,hidden integer not null default 0,title text,componentName text,color integer not null default -1, secret integer not null default 0);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 698
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mMaxAppOrderId:Ljava/util/concurrent/atomic/AtomicLong;

    .line 699
    return-void
.end method

.method private createFavorites(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;

    .prologue
    .line 662
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CREATE table "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id INTEGER PRIMARY KEY,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "itemType INTEGER,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "container INTEGER,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "screen INTEGER,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cellX INTEGER,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cellY INTEGER,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "spanX INTEGER,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "spanY INTEGER,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "title TEXT,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "intent TEXT,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "appWidgetId INTEGER NOT NULL DEFAULT -1,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "iconType INTEGER,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "iconPackage TEXT,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "iconResource TEXT,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "iconMovieUri TEXT,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "icon BLOB,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "color INTEGER NOT NULL DEFAULT -1, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "secret INTEGER NOT NULL DEFAULT 0, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "festival INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 684
    return-void
.end method

.method private createPublicPreferences(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 12
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 702
    const-string v7, "prefs"

    invoke-direct {p0, p1, v7}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->doesTableExist(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 703
    # getter for: Lcom/android/launcher2/LauncherProvider;->mApp:Lcom/android/launcher2/LauncherApplication;
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$100()Lcom/android/launcher2/LauncherApplication;

    move-result-object v7

    const-string v8, "com.sec.android.app.launcher.prefs"

    invoke-virtual {v7, v8, v9}, Lcom/android/launcher2/LauncherApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 704
    .local v2, "prefs":Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    .line 705
    .local v0, "csc":Lcom/sec/android/app/CscFeature;
    iget-object v7, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 706
    .local v3, "res":Landroid/content/res/Resources;
    const v7, 0x7f0c0009

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 710
    .local v1, "maxCount":I
    const-string v7, "screencount"

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 711
    .local v4, "screenCount":I
    if-lez v4, :cond_0

    if-le v4, v1, :cond_3

    .line 712
    :cond_0
    const-string v7, "CscFeature_Launcher_TotalPageCount"

    invoke-virtual {v0, v7}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v4

    .line 714
    if-lez v4, :cond_1

    if-le v4, v1, :cond_3

    .line 715
    :cond_1
    const v7, 0x7f0c0007

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 716
    if-lez v4, :cond_2

    if-le v4, v1, :cond_3

    .line 717
    :cond_2
    move v4, v1

    .line 722
    :cond_3
    const-string v7, "homescreenindex"

    const/4 v8, -0x1

    invoke-interface {v2, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 723
    .local v5, "screenIndex":I
    if-ltz v5, :cond_4

    if-lt v5, v4, :cond_7

    .line 724
    :cond_4
    const-string v7, "CscFeature_Launcher_DefaultPageNumber"

    invoke-virtual {v0, v7}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v5

    .line 726
    if-ltz v5, :cond_5

    if-lt v5, v4, :cond_7

    .line 727
    :cond_5
    const v7, 0x7f0c000a

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    .line 728
    if-ltz v5, :cond_6

    if-lt v5, v4, :cond_7

    .line 729
    :cond_6
    const/4 v5, 0x0

    .line 734
    :cond_7
    const-string v7, "CREATE TABLE IF NOT EXISTS prefs(key TEXT PRIMARY KEY,value INTEGER);"

    invoke-virtual {p1, v7}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 739
    const-string v7, "insert into prefs values(?, ?)"

    invoke-virtual {p1, v7}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v6

    .line 742
    .local v6, "statement":Landroid/database/sqlite/SQLiteStatement;
    const-string v7, "defaultScreen"

    invoke-virtual {v6, v10, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 743
    int-to-long v8, v5

    invoke-virtual {v6, v11, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 744
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 746
    const-string v7, "numScreens"

    invoke-virtual {v6, v10, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 747
    int-to-long v8, v4

    invoke-virtual {v6, v11, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 748
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 750
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 752
    .end local v0    # "csc":Lcom/sec/android/app/CscFeature;
    .end local v1    # "maxCount":I
    .end local v2    # "prefs":Landroid/content/SharedPreferences;
    .end local v3    # "res":Landroid/content/res/Resources;
    .end local v4    # "screenCount":I
    .end local v5    # "screenIndex":I
    .end local v6    # "statement":Landroid/database/sqlite/SQLiteStatement;
    :cond_8
    return-void
.end method

.method private delAppFolder(Landroid/database/sqlite/SQLiteDatabase;J)Z
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "folderId"    # J

    .prologue
    .line 2202
    const-string v0, "appOrder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private delFolder(Landroid/database/sqlite/SQLiteDatabase;J)Z
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "folderId"    # J

    .prologue
    .line 2219
    const-string v0, "favorites"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private doesTableExist(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 846
    const/4 v1, 0x0

    .line 847
    .local v1, "exists":Z
    const-string v4, "SELECT count(*) from sqlite_master where name=? and type=\'table\'"

    new-array v5, v2, [Ljava/lang/String;

    aput-object p2, v5, v3

    invoke-virtual {p1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 849
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 850
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 851
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    move v1, v2

    .line 853
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 855
    :cond_1
    return v1

    :cond_2
    move v1, v3

    .line 851
    goto :goto_0
.end method

.method private getMaxFavoriteID(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/concurrent/atomic/AtomicLong;
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1281
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-string v2, "favorites"

    invoke-direct {p0, p1, v2}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->initializeMaxId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    .line 1282
    .local v0, "favoriteId":Ljava/util/concurrent/atomic/AtomicLong;
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    const-string v2, "favorites"

    invoke-direct {p0, p1, v2}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->initializeMaxId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    .line 1284
    .local v1, "otherFavoriteId":Ljava/util/concurrent/atomic/AtomicLong;
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->intValue()I

    move-result v2

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->intValue()I

    move-result v3

    if-le v2, v3, :cond_0

    .end local v0    # "favoriteId":Ljava/util/concurrent/atomic/AtomicLong;
    :goto_0
    return-object v0

    .restart local v0    # "favoriteId":Ljava/util/concurrent/atomic/AtomicLong;
    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private getProviderInPackage(Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 2236
    iget-object v5, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 2237
    .local v0, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v0}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v4

    .line 2238
    .local v4, "providers":Ljava/util/List;, "Ljava/util/List<Landroid/appwidget/AppWidgetProviderInfo;>;"
    if-nez v4, :cond_1

    move-object v2, v6

    .line 2246
    :cond_0
    :goto_0
    return-object v2

    .line 2239
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 2240
    .local v3, "providerCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_3

    .line 2241
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v2, v5, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    .line 2242
    .local v2, "provider":Landroid/content/ComponentName;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2240
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v2    # "provider":Landroid/content/ComponentName;
    :cond_3
    move-object v2, v6

    .line 2246
    goto :goto_0
.end method

.method private getSearchWidgetProvider()Landroid/content/ComponentName;
    .locals 4

    .prologue
    .line 2224
    iget-object v2, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    const-string v3, "search"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/SearchManager;

    .line 2226
    .local v1, "searchManager":Landroid/app/SearchManager;
    invoke-virtual {v1}, Landroid/app/SearchManager;->getGlobalSearchActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 2227
    .local v0, "searchComponent":Landroid/content/ComponentName;
    if-nez v0, :cond_0

    const/4 v2, 0x0

    .line 2228
    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->getProviderInPackage(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v2

    goto :goto_0
.end method

.method private getTablesList(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1096
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1097
    .local v2, "tablesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "SELECT name FROM sqlite_master WHERE type=\'table\'"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1098
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_3

    .line 1099
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1100
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1101
    .local v1, "name":Ljava/lang/String;
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "LauncherProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getTablesList()::"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1102
    :cond_1
    const-string v3, "android_metadata"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1105
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1108
    .end local v1    # "name":Ljava/lang/String;
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1110
    :cond_3
    return-object v2
.end method

.method private initializeMaxId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "table"    # Ljava/lang/String;

    .prologue
    .line 1262
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "select ifnull(max(_id),0) from "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {p1, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1265
    .local v0, "c":Landroid/database/Cursor;
    const-wide/16 v2, -0x1

    .line 1266
    .local v2, "id":J
    if-eqz v0, :cond_1

    .line 1267
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1268
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1270
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1273
    :cond_1
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 1274
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error: could not query max id from table "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1277
    :cond_2
    return-wide v2
.end method

.method private loadAppOrderContainer(Landroid/database/sqlite/SQLiteDatabase;Lorg/xmlpull/v1/XmlPullParser;JZ)I
    .locals 27
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p3, "folderId"    # J
    .param p5, "isCSC"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1420
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "LauncherProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "loadAppOrderContainer. folderId: "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1421
    :cond_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1422
    .local v4, "values":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 1424
    .local v5, "pkgMgr":Landroid/content/pm/PackageManager;
    invoke-static/range {p2 .. p2}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v15

    .line 1425
    .local v15, "attrs":Landroid/util/AttributeSet;
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v23

    .line 1426
    .local v23, "startDepth":I
    const/16 v16, 0x0

    .line 1427
    .local v16, "cell":I
    const/16 v20, 0x0

    .line 1430
    .local v20, "items":I
    const/4 v6, 0x0

    .line 1431
    .local v6, "pkgName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1432
    .local v7, "className":Ljava/lang/String;
    const/16 v19, 0x0

    .line 1433
    .local v19, "isFrontPosition":Z
    const/16 v18, 0x0

    .line 1435
    .local v18, "hidden":Z
    :cond_1
    :goto_0
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v26

    .local v26, "type":I
    const/4 v2, 0x3

    move/from16 v0, v26

    if-ne v0, v2, :cond_2

    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v2

    move/from16 v0, v23

    if-le v2, v0, :cond_3

    .line 1437
    :cond_2
    const/4 v2, 0x1

    move/from16 v0, v26

    if-ne v0, v2, :cond_4

    .line 1540
    :cond_3
    return v20

    .line 1440
    :cond_4
    const/4 v2, 0x2

    move/from16 v0, v26

    if-ne v0, v2, :cond_1

    .line 1443
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v21

    .line 1444
    .local v21, "name":Ljava/lang/String;
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "LauncherProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "loadApplicationsContainer. Process tag: "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1446
    :cond_5
    const/4 v8, 0x0

    .line 1447
    .local v8, "a":Landroid/content/res/TypedArray;
    const-string v22, ""

    .line 1449
    .local v22, "screen":Ljava/lang/String;
    if-eqz p5, :cond_7

    .line 1450
    const-string v6, ""

    .line 1451
    const-string v7, ""

    .line 1452
    const/4 v2, 0x0

    const-string v3, "screen"

    move-object/from16 v0, p2

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 1453
    const/16 v19, 0x0

    .line 1461
    :goto_1
    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    .line 1463
    const-string v2, "favorite"

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1467
    if-eqz p5, :cond_8

    .line 1468
    const/4 v2, 0x0

    const-string v3, "packageName"

    move-object/from16 v0, p2

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1469
    const/4 v2, 0x0

    const-string v3, "className"

    move-object/from16 v0, p2

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1470
    const/4 v2, 0x0

    const-string v3, "AppOrder_isFrontPosition"

    move-object/from16 v0, p2

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v19

    .line 1471
    const/4 v2, 0x0

    const-string v3, "hidden"

    move-object/from16 v0, p2

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v18

    .line 1477
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mOnlyLoadFrontApps:Z

    if-eqz v2, :cond_9

    .line 1478
    if-nez v18, :cond_6

    if-eqz v19, :cond_6

    if-eqz v7, :cond_6

    .line 1479
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->frontMenuApps:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1535
    :cond_6
    :goto_3
    if-eqz v8, :cond_1

    .line 1536
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    goto/16 :goto_0

    .line 1455
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/sec/android/app/launcher/R$styleable;->AppOrder:[I

    invoke-virtual {v2, v15, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v8

    .line 1456
    const/4 v2, 0x1

    invoke-virtual {v8, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1457
    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1458
    const/4 v2, 0x2

    invoke-virtual {v8, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 1459
    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v19

    goto :goto_1

    .line 1473
    :cond_8
    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v19

    .line 1474
    const/4 v2, 0x5

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v18

    goto :goto_2

    .line 1482
    :cond_9
    if-eqz v18, :cond_a

    .line 1483
    const-string v2, "hidden"

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1485
    :cond_a
    const-wide/16 v2, -0x1

    cmp-long v2, p3, v2

    if-nez v2, :cond_b

    .line 1486
    const-string v2, "screen"

    move-object/from16 v0, v22

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1487
    const-string v2, "cell"

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 1494
    invoke-direct/range {v2 .. v7}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->addAppShortcut(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v10, 0x0

    cmp-long v2, v2, v10

    if-ltz v2, :cond_6

    .line 1495
    add-int/lit8 v20, v20, 0x1

    .line 1496
    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    .line 1490
    :cond_b
    const-string v2, "folderId"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1491
    const-string v2, "screen"

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 1501
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mOnlyLoadFrontApps:Z

    if-nez v2, :cond_11

    const-wide/16 v2, -0x1

    cmp-long v2, p3, v2

    if-nez v2, :cond_11

    const-string v2, "folder"

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1502
    const/16 v24, 0x0

    .line 1503
    .local v24, "title":Ljava/lang/String;
    const/16 v25, -0x1

    .line 1504
    .local v25, "titleResId":I
    if-eqz p5, :cond_f

    .line 1505
    const/4 v2, 0x0

    const-string v3, "title"

    move-object/from16 v0, p2

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 1513
    :cond_d
    :goto_5
    const-string v2, "title"

    move-object/from16 v0, v24

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1514
    const-string v2, "screen"

    move-object/from16 v0, v22

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1515
    const-string v2, "cell"

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1516
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->addAppFolder(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v12

    .line 1517
    .local v12, "newFolderId":J
    const-wide/16 v2, -0x1

    cmp-long v2, v12, v2

    if-eqz v2, :cond_6

    .line 1518
    add-int/lit8 v20, v20, 0x1

    .line 1519
    add-int/lit8 v16, v16, 0x1

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move/from16 v14, p5

    .line 1520
    invoke-direct/range {v9 .. v14}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->loadAppOrderContainer(Landroid/database/sqlite/SQLiteDatabase;Lorg/xmlpull/v1/XmlPullParser;JZ)I

    move-result v17

    .line 1521
    .local v17, "folderItemCount":I
    if-nez v17, :cond_e

    .line 1522
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12, v13}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->delAppFolder(Landroid/database/sqlite/SQLiteDatabase;J)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1523
    add-int/lit8 v20, v20, -0x1

    .line 1524
    add-int/lit8 v16, v16, -0x1

    .line 1529
    :cond_e
    :goto_6
    add-int v20, v20, v17

    goto/16 :goto_3

    .line 1507
    .end local v12    # "newFolderId":J
    .end local v17    # "folderItemCount":I
    :cond_f
    const/4 v2, 0x3

    const/4 v3, -0x1

    invoke-virtual {v8, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v25

    .line 1509
    const/4 v2, -0x1

    move/from16 v0, v25

    if-eq v0, v2, :cond_d

    .line 1510
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move/from16 v0, v25

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    goto :goto_5

    .line 1525
    .restart local v12    # "newFolderId":J
    .restart local v17    # "folderItemCount":I
    :cond_10
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1526
    const-string v2, "LauncherProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to delete empty Appfolder. _id: "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 1533
    .end local v12    # "newFolderId":J
    .end local v17    # "folderItemCount":I
    .end local v24    # "title":Ljava/lang/String;
    .end local v25    # "titleResId":I
    :cond_11
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "LauncherProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invalid tag <"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "> detected while parsing favorites at line "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getLineNumber()I

    move-result v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3
.end method

.method private loadAppOrderDefaults(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 20
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1565
    const/4 v15, 0x0

    .line 1566
    .local v15, "items":I
    const-string v3, "LauncherProvider"

    const-string v4, "loadAppOrderDefaults."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1567
    const/4 v2, 0x0

    .line 1570
    .local v2, "cscFile":Ljava/io/FileReader;
    const/4 v8, 0x0

    .line 1571
    .local v8, "isCSC":Z
    const/4 v5, 0x0

    .line 1572
    .local v5, "parser":Lorg/xmlpull/v1/XmlPullParser;
    :try_start_0
    new-instance v10, Ljava/io/File;

    const-string v3, "/system/csc/default_application_order.xml"

    invoke-direct {v10, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1580
    .local v10, "cscFileChk":Ljava/io/File;
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/launcher2/LauncherApplication;->isKnoxMode()Z

    move-result v13

    .line 1581
    .local v13, "isKnoxMode":Z
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isOwner()Z

    move-result v14

    .line 1583
    .local v14, "isOwner":Z
    if-eqz v13, :cond_1

    .line 1584
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v16

    .line 1585
    .local v16, "resParser":Landroid/content/res/XmlResourceParser;
    const-string v3, "appOrder"

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/android/launcher2/LauncherProvider;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 1586
    move-object/from16 v5, v16

    .line 1606
    .end local v16    # "resParser":Landroid/content/res/XmlResourceParser;
    :goto_0
    const-wide/16 v6, -0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->loadAppOrderContainer(Landroid/database/sqlite/SQLiteDatabase;Lorg/xmlpull/v1/XmlPullParser;JZ)I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v15

    .line 1617
    if-eqz v2, :cond_0

    .line 1619
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6

    .line 1624
    .end local v10    # "cscFileChk":Ljava/io/File;
    .end local v13    # "isKnoxMode":Z
    .end local v14    # "isOwner":Z
    :cond_0
    :goto_1
    return v15

    .line 1587
    .restart local v10    # "cscFileChk":Ljava/io/File;
    .restart local v13    # "isKnoxMode":Z
    .restart local v14    # "isOwner":Z
    :cond_1
    if-nez v14, :cond_2

    .line 1588
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070001

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v16

    .line 1589
    .restart local v16    # "resParser":Landroid/content/res/XmlResourceParser;
    const-string v3, "appOrder"

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/android/launcher2/LauncherProvider;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 1590
    move-object/from16 v5, v16

    .line 1591
    goto :goto_0

    .end local v16    # "resParser":Landroid/content/res/XmlResourceParser;
    :cond_2
    invoke-virtual {v10}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v18, 0x0

    cmp-long v3, v6, v18

    if-lez v3, :cond_3

    .line 1593
    new-instance v9, Ljava/io/FileReader;

    const-string v3, "/system/csc/default_application_order.xml"

    invoke-direct {v9, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1594
    .end local v2    # "cscFile":Ljava/io/FileReader;
    .local v9, "cscFile":Ljava/io/FileReader;
    :try_start_3
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v12

    .line 1595
    .local v12, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v3, 0x1

    invoke-virtual {v12, v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 1596
    invoke-virtual {v12}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 1597
    invoke-interface {v5, v9}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1598
    const/4 v8, 0x1

    move-object v2, v9

    .line 1600
    .end local v9    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    goto :goto_0

    .line 1601
    .end local v12    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    :cond_3
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f070000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v16

    .line 1602
    .restart local v16    # "resParser":Landroid/content/res/XmlResourceParser;
    const-string v3, "appOrder"

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/android/launcher2/LauncherProvider;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1603
    move-object/from16 v5, v16

    goto :goto_0

    .line 1608
    .end local v10    # "cscFileChk":Ljava/io/File;
    .end local v13    # "isKnoxMode":Z
    .end local v14    # "isOwner":Z
    .end local v16    # "resParser":Landroid/content/res/XmlResourceParser;
    :catch_0
    move-exception v11

    .line 1609
    .local v11, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_2
    :try_start_5
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "LauncherProvider"

    const-string v4, "Got exception parsing applications."

    invoke-static {v3, v4, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1617
    :cond_4
    if-eqz v2, :cond_0

    .line 1619
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    .line 1620
    :catch_1
    move-exception v3

    goto :goto_1

    .line 1611
    .end local v11    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_2
    move-exception v11

    .line 1612
    .local v11, "e":Ljava/io/IOException;
    :goto_3
    :try_start_7
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "LauncherProvider"

    const-string v4, "Got exception parsing applications."

    invoke-static {v3, v4, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1617
    :cond_5
    if-eqz v2, :cond_0

    .line 1619
    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_1

    .line 1620
    :catch_3
    move-exception v3

    goto/16 :goto_1

    .line 1614
    .end local v11    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v11

    .line 1615
    .local v11, "e":Landroid/content/res/Resources$NotFoundException;
    :goto_4
    :try_start_9
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "LauncherProvider"

    const-string v4, "Got exception parsing applications."

    invoke-static {v3, v4, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1617
    :cond_6
    if-eqz v2, :cond_0

    .line 1619
    :try_start_a
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    goto/16 :goto_1

    .line 1620
    :catch_5
    move-exception v3

    goto/16 :goto_1

    .line 1617
    .end local v11    # "e":Landroid/content/res/Resources$NotFoundException;
    :catchall_0
    move-exception v3

    :goto_5
    if-eqz v2, :cond_7

    .line 1619
    :try_start_b
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7

    .line 1620
    :cond_7
    :goto_6
    throw v3

    .restart local v10    # "cscFileChk":Ljava/io/File;
    .restart local v13    # "isKnoxMode":Z
    .restart local v14    # "isOwner":Z
    :catch_6
    move-exception v3

    goto/16 :goto_1

    .end local v10    # "cscFileChk":Ljava/io/File;
    .end local v13    # "isKnoxMode":Z
    .end local v14    # "isOwner":Z
    :catch_7
    move-exception v4

    goto :goto_6

    .line 1617
    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v9    # "cscFile":Ljava/io/FileReader;
    .restart local v10    # "cscFileChk":Ljava/io/File;
    .restart local v13    # "isKnoxMode":Z
    .restart local v14    # "isOwner":Z
    :catchall_1
    move-exception v3

    move-object v2, v9

    .end local v9    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    goto :goto_5

    .line 1614
    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v9    # "cscFile":Ljava/io/FileReader;
    :catch_8
    move-exception v11

    move-object v2, v9

    .end local v9    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    goto :goto_4

    .line 1611
    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v9    # "cscFile":Ljava/io/FileReader;
    :catch_9
    move-exception v11

    move-object v2, v9

    .end local v9    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    goto :goto_3

    .line 1608
    .end local v2    # "cscFile":Ljava/io/FileReader;
    .restart local v9    # "cscFile":Ljava/io/FileReader;
    :catch_a
    move-exception v11

    move-object v2, v9

    .end local v9    # "cscFile":Ljava/io/FileReader;
    .restart local v2    # "cscFile":Ljava/io/FileReader;
    goto :goto_2
.end method

.method private loadDynamicCscFavorites(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Z)I
    .locals 22
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "hotseatOnly"    # Z

    .prologue
    .line 1863
    const/16 v16, 0x0

    .line 1864
    .local v16, "items":I
    const/4 v14, 0x0

    .line 1868
    .local v14, "fileReader":Ljava/io/FileReader;
    const/4 v9, 0x0

    .line 1869
    .local v9, "isCSC":Z
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v13

    .line 1871
    .local v13, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v2, 0x1

    invoke-virtual {v13, v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 1872
    const/4 v5, 0x0

    .line 1873
    .local v5, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/16 v20, 0x0

    .line 1875
    .local v20, "resParser":Landroid/content/res/XmlResourceParser;
    new-instance v10, Ljava/io/File;

    const-string v2, "/data/data/com.samsung.snmc.dynamiccsc/files/default_workspace_dynamic_csc.xml"

    invoke-direct {v10, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1877
    .local v10, "cscFileChk":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-lez v2, :cond_4

    .line 1878
    new-instance v15, Ljava/io/FileReader;

    invoke-direct {v15, v10}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1879
    .end local v14    # "fileReader":Ljava/io/FileReader;
    .local v15, "fileReader":Ljava/io/FileReader;
    :try_start_1
    invoke-virtual {v13}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 1880
    invoke-interface {v5, v15}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1881
    const/4 v9, 0x1

    move-object v14, v15

    .line 1891
    .end local v15    # "fileReader":Ljava/io/FileReader;
    .restart local v14    # "fileReader":Ljava/io/FileReader;
    :goto_0
    :try_start_2
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v11

    .line 1895
    .local v11, "depth":I
    :cond_0
    :goto_1
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v21

    .local v21, "type":I
    const/4 v2, 0x3

    move/from16 v0, v21

    if-ne v0, v2, :cond_1

    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    if-le v2, v11, :cond_2

    :cond_1
    const/4 v2, 0x1

    move/from16 v0, v21

    if-eq v0, v2, :cond_2

    .line 1897
    const/4 v2, 0x1

    move/from16 v0, v21

    if-ne v0, v2, :cond_5

    .line 1930
    :cond_2
    if-eqz v14, :cond_3

    .line 1932
    :try_start_3
    invoke-virtual {v14}, Ljava/io/FileReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6

    .line 1937
    .end local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v10    # "cscFileChk":Ljava/io/File;
    .end local v11    # "depth":I
    .end local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v21    # "type":I
    :cond_3
    :goto_2
    return v16

    .line 1883
    .restart local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v10    # "cscFileChk":Ljava/io/File;
    .restart local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    :cond_4
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 1884
    .local v18, "res":Landroid/content/res/Resources;
    const/16 v19, 0x0

    .line 1885
    .local v19, "resId":I
    const v19, 0x7f07000c

    .line 1886
    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v20

    .line 1887
    const-string v2, "favorites"

    move-object/from16 v0, v20

    invoke-static {v0, v2}, Lcom/android/launcher2/LauncherProvider;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 1888
    move-object/from16 v5, v20

    goto :goto_0

    .line 1900
    .end local v18    # "res":Landroid/content/res/Resources;
    .end local v19    # "resId":I
    .restart local v11    # "depth":I
    .restart local v21    # "type":I
    :cond_5
    const/4 v2, 0x2

    move/from16 v0, v21

    if-ne v0, v2, :cond_0

    .line 1904
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v17

    .line 1905
    .local v17, "name":Ljava/lang/String;
    const-string v2, "home"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1906
    if-nez p3, :cond_0

    .line 1907
    const-wide/16 v6, -0x64

    const/16 v8, 0xf

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v9}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->loadFavoritesContainer(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;JIZ)I

    move-result v2

    add-int v16, v16, v2

    goto :goto_1

    .line 1913
    :cond_6
    const-string v2, "hotseat"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1914
    const-wide/16 v6, -0x65

    const/4 v8, 0x7

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v9}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->loadFavoritesContainer(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;JIZ)I

    move-result v2

    add-int v16, v16, v2

    goto :goto_1

    .line 1918
    :cond_7
    const-string v2, "LauncherProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid tag <"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "> detected while parsing favorites at line "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getLineNumber()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 1923
    .end local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v10    # "cscFileChk":Ljava/io/File;
    .end local v11    # "depth":I
    .end local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v17    # "name":Ljava/lang/String;
    .end local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v21    # "type":I
    :catch_0
    move-exception v12

    .line 1924
    .local v12, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_3
    :try_start_5
    const-string v2, "LauncherProvider"

    const-string v3, "Got exception parsing favorites."

    invoke-static {v2, v3, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1930
    if-eqz v14, :cond_3

    .line 1932
    :try_start_6
    invoke-virtual {v14}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_2

    .line 1933
    :catch_1
    move-exception v2

    goto/16 :goto_2

    .line 1925
    .end local v12    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_2
    move-exception v12

    .line 1926
    .local v12, "e":Ljava/io/IOException;
    :goto_4
    :try_start_7
    const-string v2, "LauncherProvider"

    const-string v3, "Got exception parsing favorites."

    invoke-static {v2, v3, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1930
    if-eqz v14, :cond_3

    .line 1932
    :try_start_8
    invoke-virtual {v14}, Ljava/io/FileReader;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_2

    .line 1933
    :catch_3
    move-exception v2

    goto/16 :goto_2

    .line 1927
    .end local v12    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v12

    .line 1928
    .local v12, "e":Landroid/content/res/Resources$NotFoundException;
    :goto_5
    :try_start_9
    const-string v2, "LauncherProvider"

    const-string v3, "Got exception parsing favorites."

    invoke-static {v2, v3, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1930
    if-eqz v14, :cond_3

    .line 1932
    :try_start_a
    invoke-virtual {v14}, Ljava/io/FileReader;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    goto/16 :goto_2

    .line 1933
    :catch_5
    move-exception v2

    goto/16 :goto_2

    .line 1930
    .end local v12    # "e":Landroid/content/res/Resources$NotFoundException;
    :catchall_0
    move-exception v2

    :goto_6
    if-eqz v14, :cond_8

    .line 1932
    :try_start_b
    invoke-virtual {v14}, Ljava/io/FileReader;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7

    .line 1933
    :cond_8
    :goto_7
    throw v2

    .restart local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v10    # "cscFileChk":Ljava/io/File;
    .restart local v11    # "depth":I
    .restart local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    .restart local v21    # "type":I
    :catch_6
    move-exception v2

    goto/16 :goto_2

    .end local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v10    # "cscFileChk":Ljava/io/File;
    .end local v11    # "depth":I
    .end local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v21    # "type":I
    :catch_7
    move-exception v3

    goto :goto_7

    .line 1930
    .end local v14    # "fileReader":Ljava/io/FileReader;
    .restart local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v10    # "cscFileChk":Ljava/io/File;
    .restart local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v15    # "fileReader":Ljava/io/FileReader;
    .restart local v20    # "resParser":Landroid/content/res/XmlResourceParser;
    :catchall_1
    move-exception v2

    move-object v14, v15

    .end local v15    # "fileReader":Ljava/io/FileReader;
    .restart local v14    # "fileReader":Ljava/io/FileReader;
    goto :goto_6

    .line 1927
    .end local v14    # "fileReader":Ljava/io/FileReader;
    .restart local v15    # "fileReader":Ljava/io/FileReader;
    :catch_8
    move-exception v12

    move-object v14, v15

    .end local v15    # "fileReader":Ljava/io/FileReader;
    .restart local v14    # "fileReader":Ljava/io/FileReader;
    goto :goto_5

    .line 1925
    .end local v14    # "fileReader":Ljava/io/FileReader;
    .restart local v15    # "fileReader":Ljava/io/FileReader;
    :catch_9
    move-exception v12

    move-object v14, v15

    .end local v15    # "fileReader":Ljava/io/FileReader;
    .restart local v14    # "fileReader":Ljava/io/FileReader;
    goto :goto_4

    .line 1923
    .end local v14    # "fileReader":Ljava/io/FileReader;
    .restart local v15    # "fileReader":Ljava/io/FileReader;
    :catch_a
    move-exception v12

    move-object v14, v15

    .end local v15    # "fileReader":Ljava/io/FileReader;
    .restart local v14    # "fileReader":Ljava/io/FileReader;
    goto :goto_3
.end method

.method private loadFavorites(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Z)I
    .locals 31
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "hotseatOnly"    # Z

    .prologue
    .line 1952
    const/16 v19, 0x0

    .line 1953
    .local v19, "items":I
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "LauncherProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadFavorites table: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", hotseatOnly: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1955
    :cond_0
    const/4 v14, 0x0

    .line 1957
    .local v14, "fileReader":Ljava/io/FileReader;
    const/4 v9, 0x0

    .line 1958
    .local v9, "isCSC":Z
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v13

    .line 1959
    .local v13, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v2, 0x1

    invoke-virtual {v13, v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 1960
    const/4 v5, 0x0

    .line 1961
    .local v5, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/16 v26, 0x0

    .line 1962
    .local v26, "resParser":Landroid/content/res/XmlResourceParser;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 1963
    .local v25, "res":Landroid/content/res/Resources;
    new-instance v10, Ljava/io/File;

    const-string v2, "/system/csc/default_workspace.xml"

    invoke-direct {v10, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1965
    .local v10, "cscFileChk":Ljava/io/File;
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/launcher2/LauncherApplication;->isKnoxMode()Z

    move-result v16

    .line 1966
    .local v16, "isKnoxMode":Z
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isOwner()Z

    move-result v17

    .line 1967
    .local v17, "isOwner":Z
    const/16 v18, 0x0

    .line 1970
    .local v18, "isReady2Go":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v23

    .line 1971
    .local v23, "pm":Landroid/content/pm/PackageManager;
    const-string v2, "android.hardware.telephony"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v28

    .line 1984
    .local v28, "supportTelephony":Z
    # getter for: Lcom/android/launcher2/LauncherProvider;->mIsRestoreHomeScreen:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$200()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1985
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mParserRestore:Lorg/xmlpull/v1/XmlPullParser;

    .line 1986
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    .line 1987
    const/4 v9, 0x1

    .line 1988
    # getter for: Lcom/android/launcher2/LauncherProvider;->mApp:Lcom/android/launcher2/LauncherApplication;
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$100()Lcom/android/launcher2/LauncherApplication;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1989
    # getter for: Lcom/android/launcher2/LauncherProvider;->mApp:Lcom/android/launcher2/LauncherApplication;
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$100()Lcom/android/launcher2/LauncherApplication;

    move-result-object v2

    const-string v3, "com.sec.android.app.launcher.prefs"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/android/launcher2/LauncherApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v24

    .line 1990
    .local v24, "prefs":Landroid/content/SharedPreferences$Editor;
    const-string v2, "PrefsIsCSCLoad"

    move-object/from16 v0, v24

    invoke-interface {v0, v2, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1991
    invoke-interface/range {v24 .. v24}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2031
    .end local v24    # "prefs":Landroid/content/SharedPreferences$Editor;
    :cond_1
    :goto_0
    if-eqz v26, :cond_2

    .line 2033
    const-string v2, "favorites"

    move-object/from16 v0, v26

    invoke-static {v0, v2}, Lcom/android/launcher2/LauncherProvider;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 2034
    move-object/from16 v5, v26

    .line 2037
    :cond_2
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v11

    .line 2040
    .local v11, "depth":I
    :cond_3
    :goto_1
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v29

    .local v29, "type":I
    const/4 v2, 0x3

    move/from16 v0, v29

    if-ne v0, v2, :cond_4

    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-le v2, v11, :cond_5

    :cond_4
    const/4 v2, 0x1

    move/from16 v0, v29

    if-eq v0, v2, :cond_5

    .line 2042
    const/4 v2, 0x1

    move/from16 v0, v29

    if-ne v0, v2, :cond_11

    .line 2087
    :cond_5
    if-eqz v14, :cond_6

    .line 2089
    :try_start_1
    invoke-virtual {v14}, Ljava/io/FileReader;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7

    .line 2095
    .end local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v10    # "cscFileChk":Ljava/io/File;
    .end local v11    # "depth":I
    .end local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v16    # "isKnoxMode":Z
    .end local v17    # "isOwner":Z
    .end local v18    # "isReady2Go":Z
    .end local v23    # "pm":Landroid/content/pm/PackageManager;
    .end local v25    # "res":Landroid/content/res/Resources;
    .end local v26    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v28    # "supportTelephony":Z
    .end local v29    # "type":I
    :cond_6
    :goto_2
    # getter for: Lcom/android/launcher2/LauncherProvider;->mIsRestoreHomeScreen:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$200()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2096
    const/4 v2, 0x0

    # setter for: Lcom/android/launcher2/LauncherProvider;->mIsRestoreHomeScreen:Z
    invoke-static {v2}, Lcom/android/launcher2/LauncherProvider;->access$202(Z)Z

    :cond_7
    move/from16 v20, v19

    .line 2099
    .end local v19    # "items":I
    .local v20, "items":I
    :goto_3
    return v20

    .line 1994
    .end local v20    # "items":I
    .restart local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v10    # "cscFileChk":Ljava/io/File;
    .restart local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v16    # "isKnoxMode":Z
    .restart local v17    # "isOwner":Z
    .restart local v18    # "isReady2Go":Z
    .restart local v19    # "items":I
    .restart local v23    # "pm":Landroid/content/pm/PackageManager;
    .restart local v25    # "res":Landroid/content/res/Resources;
    .restart local v26    # "resParser":Landroid/content/res/XmlResourceParser;
    .restart local v28    # "supportTelephony":Z
    :cond_8
    if-eqz v16, :cond_9

    .line 1995
    const v2, 0x7f070009

    :try_start_2
    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v26

    goto :goto_0

    .line 1996
    :cond_9
    if-nez v17, :cond_a

    .line 1997
    const v2, 0x7f070008

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v26

    goto :goto_0

    .line 1998
    :cond_a
    if-eqz v18, :cond_e

    .line 2004
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mIsAttReady2GoEnable:Z

    .line 2005
    new-instance v30, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mFavoritesPath:Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2006
    .local v30, "xmlFile":Ljava/io/File;
    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->length()J
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-nez v2, :cond_d

    .line 2087
    :cond_b
    if-eqz v14, :cond_c

    .line 2089
    :try_start_3
    invoke-virtual {v14}, Ljava/io/FileReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6

    :cond_c
    :goto_4
    move/from16 v20, v19

    .line 2090
    .end local v19    # "items":I
    .restart local v20    # "items":I
    goto :goto_3

    .line 2009
    .end local v20    # "items":I
    .restart local v19    # "items":I
    :cond_d
    :try_start_4
    new-instance v15, Ljava/io/FileReader;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mFavoritesPath:Ljava/lang/String;

    invoke-direct {v15, v2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2010
    .end local v14    # "fileReader":Ljava/io/FileReader;
    .local v15, "fileReader":Ljava/io/FileReader;
    :try_start_5
    invoke-virtual {v13}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 2011
    invoke-interface {v5, v15}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_b
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_a
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v14, v15

    .line 2013
    .end local v15    # "fileReader":Ljava/io/FileReader;
    .restart local v14    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_0

    .end local v30    # "xmlFile":Ljava/io/File;
    :cond_e
    :try_start_6
    invoke-virtual {v10}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-lez v2, :cond_f

    .line 2015
    new-instance v15, Ljava/io/FileReader;

    invoke-direct {v15, v10}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_6
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2016
    .end local v14    # "fileReader":Ljava/io/FileReader;
    .restart local v15    # "fileReader":Ljava/io/FileReader;
    :try_start_7
    invoke-virtual {v13}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 2017
    invoke-interface {v5, v15}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 2018
    const/4 v9, 0x1

    .line 2019
    # getter for: Lcom/android/launcher2/LauncherProvider;->mApp:Lcom/android/launcher2/LauncherApplication;
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$100()Lcom/android/launcher2/LauncherApplication;

    move-result-object v2

    if-eqz v2, :cond_1a

    .line 2020
    # getter for: Lcom/android/launcher2/LauncherProvider;->mApp:Lcom/android/launcher2/LauncherApplication;
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$100()Lcom/android/launcher2/LauncherApplication;

    move-result-object v2

    const-string v3, "com.sec.android.app.launcher.prefs"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/android/launcher2/LauncherApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v24

    .line 2021
    .restart local v24    # "prefs":Landroid/content/SharedPreferences$Editor;
    const-string v2, "PrefsIsCSCLoad"

    move-object/from16 v0, v24

    invoke-interface {v0, v2, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2022
    invoke-interface/range {v24 .. v24}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_7
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7 .. :try_end_7} :catch_b
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_a
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object v14, v15

    .line 2023
    .end local v15    # "fileReader":Ljava/io/FileReader;
    .restart local v14    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_0

    .line 2025
    .end local v24    # "prefs":Landroid/content/SharedPreferences$Editor;
    :cond_f
    if-nez v28, :cond_10

    .line 2026
    const v2, 0x7f07000b

    :try_start_8
    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v26

    goto/16 :goto_0

    .line 2028
    :cond_10
    const v2, 0x7f070007

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v26

    goto/16 :goto_0

    .line 2045
    .restart local v11    # "depth":I
    .restart local v29    # "type":I
    :cond_11
    const/4 v2, 0x2

    move/from16 v0, v29

    if-ne v0, v2, :cond_3

    .line 2049
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v21

    .line 2050
    .local v21, "name":Ljava/lang/String;
    const-string v2, "home"

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 2051
    if-nez p3, :cond_3

    .line 2052
    const-wide/16 v6, -0x64

    const/16 v8, 0xf

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v9}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->loadFavoritesContainer(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;JIZ)I

    move-result v2

    add-int v19, v19, v2

    goto/16 :goto_1

    .line 2054
    :cond_12
    const-string v2, "hotseat"

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 2055
    const-wide/16 v6, -0x65

    const/4 v8, 0x7

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v9}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->loadFavoritesContainer(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;JIZ)I

    move-result v2

    add-int v19, v19, v2

    goto/16 :goto_1

    .line 2058
    :cond_13
    const-string v2, "PageCount"

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 2059
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v29

    const/4 v2, 0x4

    move/from16 v0, v29

    if-ne v0, v2, :cond_3

    .line 2060
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    .line 2061
    .local v22, "pageCount":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    move/from16 v0, v22

    invoke-static {v2, v0, v3}, Lcom/android/launcher2/LauncherApplication;->setScreenCount(Landroid/content/Context;IZ)V
    :try_end_8
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 2078
    .end local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v10    # "cscFileChk":Ljava/io/File;
    .end local v11    # "depth":I
    .end local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v16    # "isKnoxMode":Z
    .end local v17    # "isOwner":Z
    .end local v18    # "isReady2Go":Z
    .end local v21    # "name":Ljava/lang/String;
    .end local v22    # "pageCount":I
    .end local v23    # "pm":Landroid/content/pm/PackageManager;
    .end local v25    # "res":Landroid/content/res/Resources;
    .end local v26    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v28    # "supportTelephony":Z
    .end local v29    # "type":I
    :catch_0
    move-exception v12

    .line 2079
    .local v12, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_5
    :try_start_9
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_14

    const-string v2, "LauncherProvider"

    const-string v3, "Got exception parsing favorites."

    invoke-static {v2, v3, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 2087
    :cond_14
    if-eqz v14, :cond_6

    .line 2089
    :try_start_a
    invoke-virtual {v14}, Ljava/io/FileReader;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    goto/16 :goto_2

    .line 2090
    :catch_1
    move-exception v2

    goto/16 :goto_2

    .line 2065
    .end local v12    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v10    # "cscFileChk":Ljava/io/File;
    .restart local v11    # "depth":I
    .restart local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v16    # "isKnoxMode":Z
    .restart local v17    # "isOwner":Z
    .restart local v18    # "isReady2Go":Z
    .restart local v21    # "name":Ljava/lang/String;
    .restart local v23    # "pm":Landroid/content/pm/PackageManager;
    .restart local v25    # "res":Landroid/content/res/Resources;
    .restart local v26    # "resParser":Landroid/content/res/XmlResourceParser;
    .restart local v28    # "supportTelephony":Z
    .restart local v29    # "type":I
    :cond_15
    :try_start_b
    const-string v2, "ScreenIndex"

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 2066
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v29

    const/4 v2, 0x4

    move/from16 v0, v29

    if-ne v0, v2, :cond_3

    .line 2067
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v27

    .line 2068
    .local v27, "screenIndex":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    move/from16 v0, v27

    invoke-static {v2, v0, v3}, Lcom/android/launcher2/LauncherApplication;->setHomeScreenIndex(Landroid/content/Context;IZ)V
    :try_end_b
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_1

    .line 2081
    .end local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v10    # "cscFileChk":Ljava/io/File;
    .end local v11    # "depth":I
    .end local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v16    # "isKnoxMode":Z
    .end local v17    # "isOwner":Z
    .end local v18    # "isReady2Go":Z
    .end local v21    # "name":Ljava/lang/String;
    .end local v23    # "pm":Landroid/content/pm/PackageManager;
    .end local v25    # "res":Landroid/content/res/Resources;
    .end local v26    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v27    # "screenIndex":I
    .end local v28    # "supportTelephony":Z
    .end local v29    # "type":I
    :catch_2
    move-exception v12

    .line 2082
    .local v12, "e":Ljava/io/IOException;
    :goto_6
    :try_start_c
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_16

    const-string v2, "LauncherProvider"

    const-string v3, "Got exception parsing favorites."

    invoke-static {v2, v3, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 2087
    :cond_16
    if-eqz v14, :cond_6

    .line 2089
    :try_start_d
    invoke-virtual {v14}, Ljava/io/FileReader;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3

    goto/16 :goto_2

    .line 2090
    :catch_3
    move-exception v2

    goto/16 :goto_2

    .line 2074
    .end local v12    # "e":Ljava/io/IOException;
    .restart local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v10    # "cscFileChk":Ljava/io/File;
    .restart local v11    # "depth":I
    .restart local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v16    # "isKnoxMode":Z
    .restart local v17    # "isOwner":Z
    .restart local v18    # "isReady2Go":Z
    .restart local v21    # "name":Ljava/lang/String;
    .restart local v23    # "pm":Landroid/content/pm/PackageManager;
    .restart local v25    # "res":Landroid/content/res/Resources;
    .restart local v26    # "resParser":Landroid/content/res/XmlResourceParser;
    .restart local v28    # "supportTelephony":Z
    .restart local v29    # "type":I
    :cond_17
    :try_start_e
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "LauncherProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid tag <"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "> detected while parsing favorites at line "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getLineNumber()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_1

    .line 2084
    .end local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v10    # "cscFileChk":Ljava/io/File;
    .end local v11    # "depth":I
    .end local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v16    # "isKnoxMode":Z
    .end local v17    # "isOwner":Z
    .end local v18    # "isReady2Go":Z
    .end local v21    # "name":Ljava/lang/String;
    .end local v23    # "pm":Landroid/content/pm/PackageManager;
    .end local v25    # "res":Landroid/content/res/Resources;
    .end local v26    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v28    # "supportTelephony":Z
    .end local v29    # "type":I
    :catch_4
    move-exception v12

    .line 2085
    .local v12, "e":Landroid/content/res/Resources$NotFoundException;
    :goto_7
    :try_start_f
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_18

    const-string v2, "LauncherProvider"

    const-string v3, "Got exception parsing favorites."

    invoke-static {v2, v3, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 2087
    :cond_18
    if-eqz v14, :cond_6

    .line 2089
    :try_start_10
    invoke-virtual {v14}, Ljava/io/FileReader;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_5

    goto/16 :goto_2

    .line 2090
    :catch_5
    move-exception v2

    goto/16 :goto_2

    .line 2087
    .end local v12    # "e":Landroid/content/res/Resources$NotFoundException;
    :catchall_0
    move-exception v2

    :goto_8
    if-eqz v14, :cond_19

    .line 2089
    :try_start_11
    invoke-virtual {v14}, Ljava/io/FileReader;->close()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_8

    .line 2090
    :cond_19
    :goto_9
    throw v2

    .restart local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v10    # "cscFileChk":Ljava/io/File;
    .restart local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v16    # "isKnoxMode":Z
    .restart local v17    # "isOwner":Z
    .restart local v18    # "isReady2Go":Z
    .restart local v23    # "pm":Landroid/content/pm/PackageManager;
    .restart local v25    # "res":Landroid/content/res/Resources;
    .restart local v26    # "resParser":Landroid/content/res/XmlResourceParser;
    .restart local v28    # "supportTelephony":Z
    .restart local v30    # "xmlFile":Ljava/io/File;
    :catch_6
    move-exception v2

    goto/16 :goto_4

    .end local v30    # "xmlFile":Ljava/io/File;
    .restart local v11    # "depth":I
    .restart local v29    # "type":I
    :catch_7
    move-exception v2

    goto/16 :goto_2

    .end local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v10    # "cscFileChk":Ljava/io/File;
    .end local v11    # "depth":I
    .end local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v16    # "isKnoxMode":Z
    .end local v17    # "isOwner":Z
    .end local v18    # "isReady2Go":Z
    .end local v23    # "pm":Landroid/content/pm/PackageManager;
    .end local v25    # "res":Landroid/content/res/Resources;
    .end local v26    # "resParser":Landroid/content/res/XmlResourceParser;
    .end local v28    # "supportTelephony":Z
    .end local v29    # "type":I
    :catch_8
    move-exception v3

    goto :goto_9

    .line 2087
    .end local v14    # "fileReader":Ljava/io/FileReader;
    .restart local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v10    # "cscFileChk":Ljava/io/File;
    .restart local v13    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v15    # "fileReader":Ljava/io/FileReader;
    .restart local v16    # "isKnoxMode":Z
    .restart local v17    # "isOwner":Z
    .restart local v18    # "isReady2Go":Z
    .restart local v23    # "pm":Landroid/content/pm/PackageManager;
    .restart local v25    # "res":Landroid/content/res/Resources;
    .restart local v26    # "resParser":Landroid/content/res/XmlResourceParser;
    .restart local v28    # "supportTelephony":Z
    :catchall_1
    move-exception v2

    move-object v14, v15

    .end local v15    # "fileReader":Ljava/io/FileReader;
    .restart local v14    # "fileReader":Ljava/io/FileReader;
    goto :goto_8

    .line 2084
    .end local v14    # "fileReader":Ljava/io/FileReader;
    .restart local v15    # "fileReader":Ljava/io/FileReader;
    :catch_9
    move-exception v12

    move-object v14, v15

    .end local v15    # "fileReader":Ljava/io/FileReader;
    .restart local v14    # "fileReader":Ljava/io/FileReader;
    goto :goto_7

    .line 2081
    .end local v14    # "fileReader":Ljava/io/FileReader;
    .restart local v15    # "fileReader":Ljava/io/FileReader;
    :catch_a
    move-exception v12

    move-object v14, v15

    .end local v15    # "fileReader":Ljava/io/FileReader;
    .restart local v14    # "fileReader":Ljava/io/FileReader;
    goto :goto_6

    .line 2078
    .end local v14    # "fileReader":Ljava/io/FileReader;
    .restart local v15    # "fileReader":Ljava/io/FileReader;
    :catch_b
    move-exception v12

    move-object v14, v15

    .end local v15    # "fileReader":Ljava/io/FileReader;
    .restart local v14    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_5

    .end local v14    # "fileReader":Ljava/io/FileReader;
    .restart local v15    # "fileReader":Ljava/io/FileReader;
    :cond_1a
    move-object v14, v15

    .end local v15    # "fileReader":Ljava/io/FileReader;
    .restart local v14    # "fileReader":Ljava/io/FileReader;
    goto/16 :goto_0
.end method

.method private loadFavoritesContainer(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;JIZ)I
    .locals 60
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p4, "containerType"    # J
    .param p6, "allowedTags"    # I
    .param p7, "isCSC"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1641
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "LauncherProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "loadFavoritesContainer. containerType: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p4

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", allowedTags: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p6

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1642
    :cond_0
    new-instance v9, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    const/4 v5, 0x0

    invoke-direct {v9, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1643
    .local v9, "intent":Landroid/content/Intent;
    const-string v4, "android.intent.category.LAUNCHER"

    invoke-virtual {v9, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1644
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 1645
    .local v7, "values":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 1647
    .local v8, "packageManager":Landroid/content/pm/PackageManager;
    invoke-static/range {p3 .. p3}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v39

    .line 1648
    .local v39, "attrs":Landroid/util/AttributeSet;
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v53

    .line 1649
    .local v53, "startDepth":I
    const/16 v46, 0x0

    .line 1652
    .local v46, "items":I
    const/4 v10, 0x0

    .line 1653
    .local v10, "pkgName":Ljava/lang/String;
    const/4 v11, 0x0

    .line 1654
    .local v11, "className":Ljava/lang/String;
    const/16 v57, 0x0

    .line 1655
    .local v57, "x":Ljava/lang/String;
    const/16 v58, 0x0

    .line 1656
    .local v58, "y":Ljava/lang/String;
    const/16 v18, 0x0

    .line 1657
    .local v18, "uri":Ljava/lang/String;
    const/16 v51, 0x0

    .line 1658
    .local v51, "spanX":Ljava/lang/String;
    const/16 v52, 0x0

    .line 1659
    .local v52, "spanY":Ljava/lang/String;
    const/4 v12, 0x0

    .line 1660
    .local v12, "titleId":Ljava/lang/String;
    const/16 v20, 0x0

    .line 1661
    .local v20, "imgId":Ljava/lang/String;
    const/16 v50, 0x0

    .line 1662
    .local v50, "secret":Ljava/lang/String;
    const/16 v41, 0x0

    .line 1663
    .local v41, "festival":Ljava/lang/String;
    const/16 v42, 0x0

    .line 1665
    .local v42, "folderIds":Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v56

    .local v56, "type":I
    const/4 v4, 0x3

    move/from16 v0, v56

    if-ne v0, v4, :cond_2

    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v4

    move/from16 v0, v53

    if-le v4, v0, :cond_3

    .line 1666
    :cond_2
    const/4 v4, 0x1

    move/from16 v0, v56

    if-ne v0, v4, :cond_5

    .line 1848
    :cond_3
    if-eqz v42, :cond_4

    .line 1849
    # getter for: Lcom/android/launcher2/LauncherProvider;->mApp:Lcom/android/launcher2/LauncherApplication;
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$100()Lcom/android/launcher2/LauncherApplication;

    move-result-object v4

    const-string v5, "com.sec.android.app.launcher.prefs"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/android/launcher2/LauncherApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v48

    .line 1850
    .local v48, "prefs":Landroid/content/SharedPreferences$Editor;
    const-string v4, "HomeFolderIds"

    move-object/from16 v0, v48

    move-object/from16 v1, v42

    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1851
    invoke-interface/range {v48 .. v48}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1854
    .end local v48    # "prefs":Landroid/content/SharedPreferences$Editor;
    :cond_4
    return v46

    .line 1669
    :cond_5
    const/4 v4, 0x2

    move/from16 v0, v56

    if-ne v0, v4, :cond_1

    .line 1672
    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v47

    .line 1673
    .local v47, "name":Ljava/lang/String;
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "LauncherProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "loadFavoritesContainer. Process tag: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v47

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1676
    :cond_6
    const-string v49, ""

    .line 1677
    .local v49, "screen":Ljava/lang/String;
    if-eqz p7, :cond_9

    .line 1678
    const/16 v17, 0x0

    .line 1679
    .local v17, "a":Landroid/content/res/TypedArray;
    const/4 v4, 0x0

    const-string v5, "packageName"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1680
    const/4 v4, 0x0

    const-string v5, "className"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1681
    const/4 v4, 0x0

    const-string v5, "screen"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    .line 1682
    const/4 v4, 0x0

    const-string v5, "x"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v57

    .line 1683
    const/4 v4, 0x0

    const-string v5, "y"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v58

    .line 1684
    const/4 v4, 0x0

    const-string v5, "secret"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    .line 1685
    const/4 v4, 0x0

    const-string v5, "festival"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    .line 1686
    const/4 v4, 0x0

    const-string v5, "title"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mWidgetTitle:Ljava/lang/String;

    .line 1701
    :goto_1
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    .line 1702
    const-string v4, "container"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1703
    const-string v4, "screen"

    move-object/from16 v0, v49

    invoke-virtual {v7, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1704
    const-string v4, "cellX"

    move-object/from16 v0, v57

    invoke-virtual {v7, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1705
    const-string v4, "cellY"

    move-object/from16 v0, v58

    invoke-virtual {v7, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1708
    sget-boolean v4, Lcom/android/launcher2/LauncherApplication;->sFestivalPageLauncher:Z

    if-eqz v4, :cond_7

    .line 1709
    const-string v5, "festival"

    if-nez v41, :cond_a

    const-string v4, "0"

    :goto_2
    invoke-virtual {v7, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1711
    :cond_7
    and-int/lit8 v4, p6, 0x1

    if-eqz v4, :cond_b

    const-string v4, "favorite"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1712
    const/4 v4, 0x0

    const-string v5, "title"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    .line 1713
    invoke-direct/range {v4 .. v12}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->addHomeShortcut(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/pm/PackageManager;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v14, 0x0

    cmp-long v4, v4, v14

    if-ltz v4, :cond_8

    .line 1714
    add-int/lit8 v46, v46, 0x1

    .line 1844
    :cond_8
    :goto_3
    const/4 v4, 0x1

    move/from16 v0, p7

    if-eq v0, v4, :cond_1

    .line 1845
    invoke-virtual/range {v17 .. v17}, Landroid/content/res/TypedArray;->recycle()V

    goto/16 :goto_0

    .line 1689
    .end local v17    # "a":Landroid/content/res/TypedArray;
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/sec/android/app/launcher/R$styleable;->Favorite:[I

    move-object/from16 v0, v39

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v17

    .line 1690
    .restart local v17    # "a":Landroid/content/res/TypedArray;
    const/4 v4, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1691
    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1692
    const/4 v4, 0x2

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v49

    .line 1693
    const/4 v4, 0x3

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v57

    .line 1694
    const/4 v4, 0x4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v58

    .line 1695
    const/16 v4, 0xc

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v50

    .line 1696
    const/16 v4, 0xd

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v41

    goto/16 :goto_1

    :cond_a
    move-object/from16 v4, v41

    .line 1709
    goto :goto_2

    .line 1715
    :cond_b
    and-int/lit8 v4, p6, 0x2

    if-eqz v4, :cond_d

    const-string v4, "shortcut"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1716
    if-eqz p7, :cond_c

    .line 1717
    const/4 v4, 0x0

    const-string v5, "uri"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 1718
    const/4 v4, 0x0

    const-string v5, "title"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1719
    const/4 v4, 0x0

    const-string v5, "icon"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    :cond_c
    move-object/from16 v13, p0

    move-object/from16 v14, p1

    move-object/from16 v15, p2

    move-object/from16 v16, v7

    move-object/from16 v19, v12

    .line 1722
    invoke-direct/range {v13 .. v20}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->addUriShortcut(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/res/TypedArray;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v14, 0x0

    cmp-long v4, v4, v14

    if-ltz v4, :cond_8

    .line 1723
    add-int/lit8 v46, v46, 0x1

    goto/16 :goto_3

    .line 1725
    :cond_d
    and-int/lit8 v4, p6, 0x8

    if-eqz v4, :cond_15

    const-string v4, "search"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 1726
    const-string v51, "4"

    .line 1727
    const-string v52, "1"

    .line 1729
    if-eqz p7, :cond_13

    .line 1730
    const/4 v4, 0x0

    const-string v5, "spanX"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    .line 1731
    const/4 v4, 0x0

    const-string v5, "spanY"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    .line 1738
    :cond_e
    :goto_4
    if-eqz v51, :cond_f

    const-string v4, "null"

    move-object/from16 v0, v51

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1739
    :cond_f
    const-string v51, "4"

    .line 1740
    :cond_10
    if-eqz v52, :cond_11

    const-string v4, "null"

    move-object/from16 v0, v52

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1741
    :cond_11
    const-string v52, "1"

    .line 1743
    :cond_12
    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v25

    invoke-static/range {v52 .. v52}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v26

    move-object/from16 v21, p0

    move-object/from16 v22, p1

    move-object/from16 v23, p2

    move-object/from16 v24, v7

    invoke-direct/range {v21 .. v26}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->addSearchWidget(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;II)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1744
    add-int/lit8 v46, v46, 0x1

    goto/16 :goto_3

    .line 1733
    :cond_13
    const/4 v4, 0x5

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_14

    .line 1734
    const/4 v4, 0x5

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v51

    .line 1735
    :cond_14
    const/4 v4, 0x6

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_e

    .line 1736
    const/4 v4, 0x6

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v52

    goto :goto_4

    .line 1746
    :cond_15
    and-int/lit8 v4, p6, 0x8

    if-eqz v4, :cond_16

    const-string v4, "clock"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 1747
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v7}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->addClockWidget(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1748
    add-int/lit8 v46, v46, 0x1

    goto/16 :goto_3

    .line 1750
    :cond_16
    and-int/lit8 v4, p6, 0x8

    if-eqz v4, :cond_1c

    const-string v4, "appwidget"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 1751
    if-eqz p7, :cond_1b

    .line 1752
    const/4 v4, 0x0

    const-string v5, "spanX"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    .line 1753
    const/4 v4, 0x0

    const-string v5, "spanY"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    .line 1759
    :goto_5
    if-eqz v51, :cond_17

    const-string v4, "null"

    move-object/from16 v0, v51

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 1760
    :cond_17
    const-string v51, "1"

    .line 1761
    :cond_18
    if-eqz v52, :cond_19

    const-string v4, "null"

    move-object/from16 v0, v52

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 1762
    :cond_19
    const-string v52, "1"

    .line 1763
    :cond_1a
    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v28

    invoke-static/range {v52 .. v52}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v29

    move-object/from16 v21, p0

    move-object/from16 v22, p1

    move-object/from16 v23, p2

    move-object/from16 v24, v7

    move-object/from16 v25, v8

    move-object/from16 v26, v10

    move-object/from16 v27, v11

    invoke-direct/range {v21 .. v29}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->addAppWidget(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1764
    add-int/lit8 v46, v46, 0x1

    goto/16 :goto_3

    .line 1756
    :cond_1b
    const/4 v4, 0x5

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v51

    .line 1757
    const/4 v4, 0x6

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v52

    goto :goto_5

    .line 1766
    :cond_1c
    and-int/lit8 v4, p6, 0x4

    if-eqz v4, :cond_23

    const-string v4, "folder"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 1767
    const/16 v54, 0x0

    .line 1768
    .local v54, "title":Ljava/lang/String;
    const/16 v55, -0x1

    .line 1769
    .local v55, "titleResId":I
    const/16 v45, 0x0

    .line 1771
    .local v45, "isHiddenAddButton":Z
    const/4 v4, 0x1

    move/from16 v0, p7

    if-ne v0, v4, :cond_1e

    .line 1772
    const/4 v4, 0x0

    const-string v5, "title"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v54

    .line 1773
    const/4 v4, 0x0

    const-string v5, "hideAddButton"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v45

    .line 1782
    :goto_6
    const-string v4, "title"

    move-object/from16 v0, v54

    invoke-virtual {v7, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1783
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v7}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->addFolder(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v26

    .line 1784
    .local v26, "folderId":J
    const-wide/16 v4, 0x0

    cmp-long v4, v26, v4

    if-ltz v4, :cond_8

    .line 1785
    add-int/lit8 v46, v46, 0x1

    .line 1787
    and-int/lit8 v28, p6, -0xd

    move-object/from16 v22, p0

    move-object/from16 v23, p1

    move-object/from16 v24, p2

    move-object/from16 v25, p3

    move/from16 v29, p7

    :try_start_0
    invoke-direct/range {v22 .. v29}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->loadFavoritesContainer(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;JIZ)I

    move-result v43

    .line 1790
    .local v43, "folderItemCount":I
    if-nez v43, :cond_21

    .line 1791
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->delFolder(Landroid/database/sqlite/SQLiteDatabase;J)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_20

    .line 1792
    add-int/lit8 v46, v46, -0x1

    .line 1805
    :cond_1d
    :goto_7
    add-int v46, v46, v43

    goto/16 :goto_3

    .line 1775
    .end local v26    # "folderId":J
    .end local v43    # "folderItemCount":I
    :cond_1e
    const/16 v4, 0x8

    const/4 v5, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v55

    .line 1776
    const/4 v4, -0x1

    move/from16 v0, v55

    if-eq v0, v4, :cond_1f

    .line 1777
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move/from16 v0, v55

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v54

    .line 1779
    :cond_1f
    const/16 v4, 0xe

    const/4 v5, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v45

    goto :goto_6

    .line 1793
    .restart local v26    # "folderId":J
    .restart local v43    # "folderItemCount":I
    :cond_20
    :try_start_1
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 1794
    const-string v4, "LauncherProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to delete empty folder. _id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v26

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_7

    .line 1807
    .end local v43    # "folderItemCount":I
    :catch_0
    move-exception v40

    .line 1808
    .local v40, "e":Ljava/io/IOException;
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1809
    const-string v4, "LauncherProvider"

    const-string v5, "Failed to load favorites"

    move-object/from16 v0, v40

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3

    .line 1797
    .end local v40    # "e":Ljava/io/IOException;
    .restart local v43    # "folderItemCount":I
    :cond_21
    if-eqz v45, :cond_1d

    .line 1798
    if-eqz v42, :cond_22

    .line 1799
    :try_start_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v42

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    goto :goto_7

    .line 1801
    :cond_22
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v42

    goto/16 :goto_7

    .line 1813
    .end local v26    # "folderId":J
    .end local v43    # "folderItemCount":I
    .end local v45    # "isHiddenAddButton":Z
    .end local v54    # "title":Ljava/lang/String;
    .end local v55    # "titleResId":I
    :cond_23
    and-int/lit8 v4, p6, 0x8

    if-eqz v4, :cond_2c

    const-string v4, "sactivitywidget"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2c

    .line 1816
    if-eqz p7, :cond_2b

    .line 1817
    const/4 v4, 0x0

    const-string v5, "themeName"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 1818
    .local v35, "themeName":Ljava/lang/String;
    const/4 v4, 0x0

    const-string v5, "instance"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    .line 1819
    .local v44, "instance":Ljava/lang/String;
    const/4 v4, 0x0

    const-string v5, "spanX"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    .line 1820
    const/4 v4, 0x0

    const-string v5, "spanY"

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    .line 1827
    :goto_8
    if-eqz v35, :cond_25

    .line 1828
    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_24

    const-string v4, "null"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_25

    .line 1829
    :cond_24
    const/16 v35, 0x0

    .line 1832
    :cond_25
    if-eqz v51, :cond_26

    const-string v4, "null"

    move-object/from16 v0, v51

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_27

    .line 1833
    :cond_26
    const-string v51, "1"

    .line 1834
    :cond_27
    if-eqz v52, :cond_28

    const-string v4, "null"

    move-object/from16 v0, v52

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 1835
    :cond_28
    const-string v52, "1"

    .line 1836
    :cond_29
    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v36

    invoke-static/range {v52 .. v52}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v37

    move-object/from16 v28, p0

    move-object/from16 v29, p1

    move-object/from16 v30, p2

    move-object/from16 v31, v7

    move-object/from16 v32, v8

    move-object/from16 v33, v10

    move-object/from16 v34, v11

    invoke-direct/range {v28 .. v37}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->addSamsungActivityWidget(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v4

    if-eqz v4, :cond_2a

    .line 1837
    add-int/lit8 v46, v46, 0x1

    .line 1838
    :cond_2a
    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v37

    invoke-static/range {v52 .. v52}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v38

    move-object/from16 v28, p0

    move-object/from16 v29, p1

    move-object/from16 v30, p2

    move-object/from16 v31, v7

    move-object/from16 v32, v8

    move-object/from16 v33, v10

    move-object/from16 v34, v11

    move-object/from16 v36, v44

    invoke-direct/range {v28 .. v38}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->addSurfaceWidget(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1839
    add-int/lit8 v46, v46, 0x1

    goto/16 :goto_3

    .line 1822
    .end local v35    # "themeName":Ljava/lang/String;
    .end local v44    # "instance":Ljava/lang/String;
    :cond_2b
    const/16 v4, 0xa

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v35

    .line 1823
    .restart local v35    # "themeName":Ljava/lang/String;
    const/16 v4, 0xb

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v44

    .line 1824
    .restart local v44    # "instance":Ljava/lang/String;
    const/4 v4, 0x5

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v51

    .line 1825
    const/4 v4, 0x6

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v52

    goto/16 :goto_8

    .line 1842
    .end local v35    # "themeName":Ljava/lang/String;
    .end local v44    # "instance":Ljava/lang/String;
    :cond_2c
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "LauncherProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid tag <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v47

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "> detected while parsing favorites at line "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getLineNumber()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3
.end method

.method private normalizeIcons(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 14
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1186
    const-string v11, "LauncherProvider"

    const-string v12, "normalizing icons"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1188
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1189
    const/4 v1, 0x0

    .line 1190
    .local v1, "c":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 1192
    .local v10, "update":Landroid/database/sqlite/SQLiteStatement;
    const/4 v9, 0x0

    .line 1193
    .local v9, "logged":Z
    :try_start_0
    const-string v11, "UPDATE favorites SET icon=? WHERE _id=?"

    invoke-virtual {p1, v11}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v10

    .line 1196
    const-string v11, "SELECT _id, icon FROM favorites WHERE iconType=1"

    const/4 v12, 0x0

    invoke-virtual {p1, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1199
    if-eqz v1, :cond_9

    if-eqz v10, :cond_9

    .line 1200
    const-string v11, "_id"

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 1201
    .local v8, "idIndex":I
    const-string v11, "icon"

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 1203
    .local v5, "iconIndex":I
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1204
    invoke-interface {v1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1205
    .local v6, "id":J
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1207
    .local v2, "data":[B
    const/4 v11, 0x0

    :try_start_1
    array-length v12, v2

    invoke-static {v2, v11, v12}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v11

    iget-object v12, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v11, v12}, Lcom/android/launcher2/Utilities;->resampleIconBitmap(Landroid/graphics/Bitmap;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1210
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 1211
    const/4 v11, 0x1

    invoke-virtual {v10, v11, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1212
    invoke-static {v0}, Lcom/android/launcher2/HomeItem;->flattenBitmap(Landroid/graphics/Bitmap;)[B

    move-result-object v2

    .line 1213
    if-eqz v2, :cond_1

    .line 1214
    const/4 v11, 0x2

    invoke-virtual {v10, v11, v2}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    .line 1215
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 1217
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1219
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v3

    .line 1220
    .local v3, "e":Ljava/lang/Exception;
    if-nez v9, :cond_3

    .line 1221
    :try_start_2
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v11

    if-eqz v11, :cond_2

    const-string v11, "LauncherProvider"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed normalizing icon "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1225
    :cond_2
    :goto_1
    const/4 v9, 0x1

    goto :goto_0

    .line 1223
    :cond_3
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v11

    if-eqz v11, :cond_2

    const-string v11, "LauncherProvider"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Also failed normalizing icon "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1232
    .end local v2    # "data":[B
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v5    # "iconIndex":I
    .end local v6    # "id":J
    .end local v8    # "idIndex":I
    :catch_1
    move-exception v4

    .line 1233
    .local v4, "ex":Landroid/database/SQLException;
    :try_start_3
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v11

    if-eqz v11, :cond_4

    const-string v11, "LauncherProvider"

    const-string v12, "Problem while allocating appWidgetIds for existing widgets"

    invoke-static {v11, v12, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1235
    :cond_4
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1236
    if-eqz v10, :cond_5

    .line 1237
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1239
    :cond_5
    if-eqz v1, :cond_6

    .line 1240
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1243
    .end local v4    # "ex":Landroid/database/SQLException;
    :cond_6
    :goto_2
    return-void

    .line 1228
    .restart local v5    # "iconIndex":I
    .restart local v8    # "idIndex":I
    :cond_7
    :try_start_4
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1235
    .end local v5    # "iconIndex":I
    .end local v8    # "idIndex":I
    :goto_3
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1236
    if-eqz v10, :cond_8

    .line 1237
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1239
    :cond_8
    if-eqz v1, :cond_6

    .line 1240
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 1230
    :cond_9
    :try_start_5
    const-string v11, "LauncherProvider"

    const-string v12, "Failed to create cursor while normalizing icons"

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 1235
    :catchall_0
    move-exception v11

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1236
    if-eqz v10, :cond_a

    .line 1237
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1239
    :cond_a
    if-eqz v1, :cond_b

    .line 1240
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v11
.end method

.method private renamePrefsFile()V
    .locals 5

    .prologue
    .line 1082
    const-string v0, "com.android.launcher2.prefs"

    .line 1086
    .local v0, "PREFERENCES_NAME_V12":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    const-string v3, "/data/data/com.sec.android.app.launcher/shared_prefs/com.android.launcher2.prefs.xml"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1087
    .local v1, "f":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    const-string v4, "/data/data/com.sec.android.app.launcher/shared_prefs/com.sec.android.app.launcher.prefs.xml"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    .line 1088
    .local v2, "result":Z
    if-eqz v2, :cond_0

    .line 1089
    const-string v3, "LauncherProvider"

    const-string v4, "renamePrefsFile:: File renamed successfull !"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1093
    :goto_0
    return-void

    .line 1091
    :cond_0
    const-string v3, "LauncherProvider"

    const-string v4, "renamePrefsFile:: File renamed operation failed"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private resetCount()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 2471
    iget-object v2, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.launcher.prefs"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 2472
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2473
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "homescreenindex"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2474
    const-string v2, "homescreenindex.simple"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2475
    const-string v2, "screencount"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2476
    const-string v2, "screencount.simple"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2477
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2478
    return-void
.end method

.method private sendAppWidgetResetNotify()V
    .locals 3

    .prologue
    .line 623
    iget-object v1, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 624
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/android/launcher2/LauncherProvider;->CONTENT_APPWIDGET_RESET_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 625
    return-void
.end method

.method private updateContactsShortcuts(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 25
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1114
    const/4 v12, 0x0

    .line 1115
    .local v12, "c":Landroid/database/Cursor;
    const-string v4, "itemType"

    const/4 v5, 0x1

    new-array v5, v5, [I

    const/4 v6, 0x0

    const/4 v8, 0x1

    aput v8, v5, v6

    invoke-static {v4, v5}, Lcom/android/launcher2/LauncherProvider;->buildOrWhereString(Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object v7

    .line 1118
    .local v7, "selectWhere":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1121
    :try_start_0
    const-string v5, "favorites"

    const/4 v4, 0x2

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v4

    const/4 v4, 0x1

    const-string v8, "intent"

    aput-object v8, v6, v4

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1124
    if-eqz v12, :cond_8

    .line 1125
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "LauncherProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "found upgrade cursor count="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1127
    :cond_0
    new-instance v24, Landroid/content/ContentValues;

    invoke-direct/range {v24 .. v24}, Landroid/content/ContentValues;-><init>()V

    .line 1128
    .local v24, "values":Landroid/content/ContentValues;
    const-string v4, "_id"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 1129
    .local v18, "idIndex":I
    const-string v4, "intent"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 1131
    .local v20, "intentIndex":I
    :cond_1
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1132
    move/from16 v0, v18

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 1133
    .local v16, "favoriteId":J
    move/from16 v0, v20

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v21

    .line 1134
    .local v21, "intentUri":Ljava/lang/String;
    if-eqz v21, :cond_1

    .line 1136
    const/4 v4, 0x0

    :try_start_1
    move-object/from16 v0, v21

    invoke-static {v0, v4}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v19

    .line 1137
    .local v19, "intent":Landroid/content/Intent;
    const-string v4, "Home"

    invoke-virtual/range {v19 .. v19}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1138
    invoke-virtual/range {v19 .. v19}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v23

    .line 1139
    .local v23, "uri":Landroid/net/Uri;
    invoke-virtual/range {v23 .. v23}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    .line 1140
    .local v13, "data":Ljava/lang/String;
    const-string v4, "android.intent.action.VIEW"

    invoke-virtual/range {v19 .. v19}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "content://contacts/people/"

    invoke-virtual {v13, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "content://com.android.contacts/contacts/lookup/"

    invoke-virtual {v13, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1144
    :cond_2
    new-instance v19, Landroid/content/Intent;

    .end local v19    # "intent":Landroid/content/Intent;
    const-string v4, "com.android.contacts.action.QUICK_CONTACT"

    move-object/from16 v0, v19

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1145
    .restart local v19    # "intent":Landroid/content/Intent;
    const/high16 v4, 0x14200000

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1149
    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1150
    const-string v4, "mode"

    const/4 v5, 0x3

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1151
    const-string v5, "exclude_mimes"

    const/4 v4, 0x0

    check-cast v4, [Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1153
    invoke-virtual/range {v24 .. v24}, Landroid/content/ContentValues;->clear()V

    .line 1154
    const-string v4, "intent"

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 1157
    .local v22, "updateWhere":Ljava/lang/String;
    const-string v4, "favorites"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    invoke-virtual {v0, v4, v1, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1159
    .end local v13    # "data":Ljava/lang/String;
    .end local v19    # "intent":Landroid/content/Intent;
    .end local v22    # "updateWhere":Ljava/lang/String;
    .end local v23    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v15

    .line 1160
    .local v15, "ex":Ljava/lang/RuntimeException;
    :try_start_2
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1161
    const-string v4, "LauncherProvider"

    const-string v5, "Problem upgrading shortcut"

    invoke-static {v4, v5, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 1172
    .end local v15    # "ex":Ljava/lang/RuntimeException;
    .end local v16    # "favoriteId":J
    .end local v18    # "idIndex":I
    .end local v20    # "intentIndex":I
    .end local v21    # "intentUri":Ljava/lang/String;
    .end local v24    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v15

    .line 1173
    .local v15, "ex":Landroid/database/SQLException;
    :try_start_3
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "LauncherProvider"

    const-string v5, "Problem while upgrading contacts"

    invoke-static {v4, v5, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1174
    :cond_3
    const/4 v4, 0x0

    .line 1176
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1177
    if-eqz v12, :cond_4

    .line 1178
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1182
    .end local v15    # "ex":Landroid/database/SQLException;
    :cond_4
    :goto_1
    return v4

    .line 1162
    .restart local v16    # "favoriteId":J
    .restart local v18    # "idIndex":I
    .restart local v20    # "intentIndex":I
    .restart local v21    # "intentUri":Ljava/lang/String;
    .restart local v24    # "values":Landroid/content/ContentValues;
    :catch_2
    move-exception v14

    .line 1163
    .local v14, "e":Ljava/net/URISyntaxException;
    :try_start_4
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1164
    const-string v4, "LauncherProvider"

    const-string v5, "Problem upgrading shortcut"

    invoke-static {v4, v5, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1176
    .end local v14    # "e":Ljava/net/URISyntaxException;
    .end local v16    # "favoriteId":J
    .end local v18    # "idIndex":I
    .end local v20    # "intentIndex":I
    .end local v21    # "intentUri":Ljava/lang/String;
    .end local v24    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v4

    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1177
    if-eqz v12, :cond_5

    .line 1178
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v4

    .line 1168
    .restart local v18    # "idIndex":I
    .restart local v20    # "intentIndex":I
    .restart local v24    # "values":Landroid/content/ContentValues;
    :cond_6
    :try_start_5
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1176
    .end local v18    # "idIndex":I
    .end local v20    # "intentIndex":I
    .end local v24    # "values":Landroid/content/ContentValues;
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1177
    if-eqz v12, :cond_7

    .line 1178
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1182
    :cond_7
    const/4 v4, 0x1

    goto :goto_1

    .line 1170
    :cond_8
    :try_start_6
    const-string v4, "LauncherProvider"

    const-string v5, "Failed to create cursor for updateContactsShortcuts"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2
.end method


# virtual methods
.method public generateNewAppOrderId()J
    .locals 2

    .prologue
    .line 1255
    iget-object v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mMaxAppOrderId:Ljava/util/concurrent/atomic/AtomicLong;

    if-nez v0, :cond_0

    .line 1256
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error: mMaxAppOrderId was not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1258
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mMaxAppOrderId:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    return-wide v0
.end method

.method public generateNewFavoritesId()J
    .locals 2

    .prologue
    .line 1247
    iget-object v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mMaxFavoriteId:Ljava/util/concurrent/atomic/AtomicLong;

    if-nez v0, :cond_0

    .line 1248
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error: mMaxFavoriteId was not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1250
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mMaxFavoriteId:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    return-wide v0
.end method

.method public loadFrontAppOrder()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1548
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->frontMenuApps:Ljava/util/ArrayList;

    .line 1549
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mOnlyLoadFrontApps:Z

    .line 1551
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->loadAppOrderDefaults(Landroid/database/sqlite/SQLiteDatabase;)I

    .line 1553
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mOnlyLoadFrontApps:Z

    .line 1554
    iget-object v0, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->frontMenuApps:Ljava/util/ArrayList;

    return-object v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const-wide/16 v4, 0x0

    .line 629
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "LauncherProvider"

    const-string v2, "creating new launcher database"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    :cond_0
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/databases"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 633
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 634
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 638
    :cond_1
    iget-object v1, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    invoke-virtual {v1}, Landroid/appwidget/AppWidgetHost;->deleteHost()V

    .line 639
    invoke-direct {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->sendAppWidgetResetNotify()V

    .line 641
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v1, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v1, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mMaxFavoriteId:Ljava/util/concurrent/atomic/AtomicLong;

    .line 642
    const-string v1, "favorites"

    invoke-direct {p0, p1, v1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->createFavorites(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 643
    iget-object v1, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mMaxFavoriteId:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 644
    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->createAppOrderTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 646
    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->convertDatabase(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 648
    const-string v1, "favorites"

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->loadFavorites(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Z)I

    .line 650
    :cond_2
    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->loadAppOrderDefaults(Landroid/database/sqlite/SQLiteDatabase;)I

    .line 651
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 1034
    move v6, p2

    .line 1035
    .local v6, "version":I
    const-string v7, "LauncherProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onDowngrade triggered :: oldVersion:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " newVersion:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1038
    const/16 v7, 0xd

    if-ge p3, v7, :cond_0

    .line 1040
    const-string v0, "com.sec.android.app.launcher.prefs"

    .line 1043
    .local v0, "PREFERENCES_NAME_V13":Ljava/lang/String;
    # getter for: Lcom/android/launcher2/LauncherProvider;->mApp:Lcom/android/launcher2/LauncherApplication;
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$100()Lcom/android/launcher2/LauncherApplication;

    move-result-object v7

    const-string v8, "com.sec.android.app.launcher.prefs"

    invoke-virtual {v7, v8}, Lcom/android/launcher2/LauncherApplication;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 1044
    .local v2, "oldFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1047
    .end local v0    # "PREFERENCES_NAME_V13":Ljava/lang/String;
    .end local v2    # "oldFile":Ljava/io/File;
    :cond_0
    const/16 v7, 0xe

    if-eq v6, v7, :cond_7

    .line 1048
    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->getTablesList(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;

    move-result-object v5

    .line 1049
    .local v5, "tablesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1050
    .local v4, "table":Ljava/lang/String;
    const-string v7, "favorites"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "appOrder"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "prefs"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1056
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DROP TABLE IF EXISTS "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1057
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "LauncherProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onDownGrade:: Dropping extra table:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1060
    .end local v4    # "table":Ljava/lang/String;
    :cond_2
    const/4 v3, 0x0

    .line 1062
    .local v3, "reCreateDatabase":Z
    const-string v7, "favorites"

    invoke-direct {p0, p1, v7}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->doesTableExist(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    const-string v7, "appOrder"

    invoke-direct {p0, p1, v7}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->doesTableExist(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 1063
    :cond_3
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1064
    const-string v7, "LauncherProvider"

    const-string v8, "onDownGrade:: favorites/appOrder doesnot exist"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1065
    :cond_4
    const/4 v3, 0x1

    .line 1067
    :cond_5
    if-eqz v3, :cond_7

    .line 1069
    const-string v7, "LauncherProvider"

    const-string v8, "Destroying all old data."

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1070
    invoke-direct {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->resetCount()V

    .line 1071
    const-string v7, "DROP TABLE IF EXISTS favorites"

    invoke-virtual {p1, v7}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1072
    const-string v7, "DROP TABLE IF EXISTS appOrder"

    invoke-virtual {p1, v7}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1073
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v7

    const-string v8, "CscFeature_Common_EnableSprintExtension"

    invoke-virtual {v7, v8}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1074
    const-string v7, "DROP TABLE IF EXISTS prefs"

    invoke-virtual {p1, v7}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1075
    :cond_6
    invoke-virtual {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1078
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "reCreateDatabase":Z
    .end local v5    # "tablesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_7
    return-void
.end method

.method public onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 860
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Common_EnableSprintExtension"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 861
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LauncherProvider"

    const-string v1, "onOpen triggered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->createPublicPreferences(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 866
    :goto_0
    return-void

    .line 864
    :cond_1
    invoke-super {p0, p1}, Landroid/database/sqlite/SQLiteOpenHelper;->onOpen(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    const/16 v6, 0xe

    const/16 v5, 0x9

    const/4 v4, 0x3

    .line 870
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "LauncherProvider"

    const-string v3, "onUpgrade triggered"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 872
    :cond_0
    move v1, p2

    .line 873
    .local v1, "version":I
    if-ge v1, v4, :cond_1

    .line 875
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 878
    :try_start_0
    const-string v2, "ALTER TABLE favorites ADD COLUMN appWidgetId INTEGER NOT NULL DEFAULT -1;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 880
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 881
    const/4 v1, 0x3

    .line 886
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 890
    :goto_0
    if-ne v1, v4, :cond_1

    .line 891
    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->convertWidgets(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 895
    :cond_1
    const/4 v2, 0x4

    if-ge v1, v2, :cond_2

    .line 896
    const/4 v1, 0x4

    .line 907
    :cond_2
    const/4 v2, 0x6

    if-ge v1, v2, :cond_3

    .line 909
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 911
    :try_start_1
    const-string v2, "UPDATE favorites SET screen=(screen + 1);"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 912
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 917
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 921
    :goto_1
    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->updateContactsShortcuts(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 922
    const/4 v1, 0x6

    .line 926
    :cond_3
    const/4 v2, 0x7

    if-ge v1, v2, :cond_4

    .line 928
    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->convertWidgets(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 929
    const/4 v1, 0x7

    .line 932
    :cond_4
    const/16 v2, 0x8

    if-ge v1, v2, :cond_5

    .line 936
    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->normalizeIcons(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 937
    const/16 v1, 0x8

    .line 940
    :cond_5
    if-ge v1, v5, :cond_7

    .line 943
    iget-object v2, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mMaxFavoriteId:Ljava/util/concurrent/atomic/AtomicLong;

    if-nez v2, :cond_6

    .line 944
    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->getMaxFavoriteID(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v2

    iput-object v2, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mMaxFavoriteId:Ljava/util/concurrent/atomic/AtomicLong;

    .line 948
    :cond_6
    const-string v2, "favorites"

    const/4 v3, 0x1

    invoke-direct {p0, p1, v2, v3}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->loadFavorites(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Z)I

    .line 949
    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->createAppOrderTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 950
    invoke-direct {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->loadAppOrderDefaults(Landroid/database/sqlite/SQLiteDatabase;)I

    .line 951
    const/16 v1, 0x9

    .line 953
    :cond_7
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getSmallestWidth()I

    move-result v2

    const/16 v3, 0x258

    if-lt v2, v3, :cond_8

    if-eq v1, v5, :cond_9

    :cond_8
    const/16 v2, 0xb

    if-ge v1, v2, :cond_9

    .line 954
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 957
    :try_start_2
    const-string v2, "ALTER TABLE favorites ADD COLUMN iconMovieUri TEXT"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 959
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 960
    const/16 v1, 0xb

    .line 965
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 969
    :cond_9
    :goto_2
    const/16 v2, 0xc

    if-ne v1, v2, :cond_a

    .line 971
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 973
    :try_start_3
    const-string v2, "ALTER TABLE favorites ADD COLUMN festival INTEGER"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 974
    const-string v2, "ALTER TABLE favorites ADD COLUMN color INTEGER NOT NULL DEFAULT -1"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 975
    const-string v2, "ALTER TABLE appOrder ADD COLUMN color INTEGER NOT NULL DEFAULT -1"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 976
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 977
    const/16 v1, 0xe

    .line 981
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 983
    :goto_3
    invoke-direct {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->renamePrefsFile()V

    .line 986
    :cond_a
    const/16 v2, 0xd

    if-ge v1, v2, :cond_b

    .line 988
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 991
    :try_start_4
    const-string v2, "ALTER TABLE favorites ADD COLUMN color INTEGER NOT NULL DEFAULT -1"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 993
    const-string v2, "ALTER TABLE appOrder ADD COLUMN color INTEGER NOT NULL DEFAULT -1"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 995
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 997
    const/16 v1, 0xd

    .line 1002
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1004
    :goto_4
    invoke-direct {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->renamePrefsFile()V

    .line 1007
    :cond_b
    if-ge v1, v6, :cond_c

    .line 1008
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1010
    :try_start_5
    const-string v2, "ALTER TABLE favorites ADD COLUMN secret INTEGER"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1011
    const-string v2, "ALTER TABLE favorites ADD COLUMN festival INTEGER"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1012
    const-string v2, "ALTER TABLE favorites ADD COLUMN pkgName TEXT"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1013
    const-string v2, "ALTER TABLE appOrder ADD COLUMN secret INTEGER"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1014
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 1015
    const/16 v1, 0xe

    .line 1019
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1023
    :cond_c
    :goto_5
    if-eq v1, v6, :cond_d

    .line 1024
    const-string v2, "LauncherProvider"

    const-string v3, "Destroying all old data."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1025
    invoke-direct {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->resetCount()V

    .line 1026
    const-string v2, "DROP TABLE IF EXISTS favorites"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1027
    const-string v2, "DROP TABLE IF EXISTS appOrder"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1028
    invoke-virtual {p0, p1}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1030
    :cond_d
    return-void

    .line 882
    :catch_0
    move-exception v0

    .line 884
    .local v0, "ex":Landroid/database/SQLException;
    :try_start_6
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "LauncherProvider"

    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 886
    :cond_e
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0

    .end local v0    # "ex":Landroid/database/SQLException;
    :catchall_0
    move-exception v2

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    .line 913
    :catch_1
    move-exception v0

    .line 915
    .restart local v0    # "ex":Landroid/database/SQLException;
    :try_start_7
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_f

    const-string v2, "LauncherProvider"

    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 917
    :cond_f
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_1

    .end local v0    # "ex":Landroid/database/SQLException;
    :catchall_1
    move-exception v2

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    .line 961
    :catch_2
    move-exception v0

    .line 963
    .restart local v0    # "ex":Landroid/database/SQLException;
    :try_start_8
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_10

    const-string v2, "LauncherProvider"

    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 965
    :cond_10
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_2

    .end local v0    # "ex":Landroid/database/SQLException;
    :catchall_2
    move-exception v2

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    .line 978
    :catch_3
    move-exception v0

    .line 979
    .restart local v0    # "ex":Landroid/database/SQLException;
    :try_start_9
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_11

    const-string v2, "LauncherProvider"

    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 981
    :cond_11
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_3

    .end local v0    # "ex":Landroid/database/SQLException;
    :catchall_3
    move-exception v2

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    .line 998
    :catch_4
    move-exception v0

    .line 1000
    .restart local v0    # "ex":Landroid/database/SQLException;
    :try_start_a
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_12

    const-string v2, "LauncherProvider"

    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 1002
    :cond_12
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_4

    .end local v0    # "ex":Landroid/database/SQLException;
    :catchall_4
    move-exception v2

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    .line 1016
    :catch_5
    move-exception v0

    .line 1017
    .restart local v0    # "ex":Landroid/database/SQLException;
    :try_start_b
    # getter for: Lcom/android/launcher2/LauncherProvider;->DEBUGGABLE:Z
    invoke-static {}, Lcom/android/launcher2/LauncherProvider;->access$000()Z

    move-result v2

    if-eqz v2, :cond_13

    const-string v2, "LauncherProvider"

    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    .line 1019
    :cond_13
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_5

    .end local v0    # "ex":Landroid/database/SQLException;
    :catchall_5
    move-exception v2

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method switchToDynamicIfNecessary(I)Z
    .locals 6
    .param p1, "basicEasymode"    # I

    .prologue
    const/4 v5, 0x0

    .line 1305
    const-string v2, "DYNAMIC_CSC_J-TDD"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "switchToDynamicIfNecessary basicEasymode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1306
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1307
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 1308
    .local v0, "changed":Z
    iget-object v2, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1309
    iget-object v2, p0, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "content://com.android.launcher2.settings/favorites"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1313
    :cond_0
    const-string v2, "favorites"

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/android/launcher2/LauncherProvider$DatabaseHelper;->loadDynamicCscFavorites(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Z)I

    .line 1314
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    .line 1315
    const/4 v0, 0x1

    .line 1320
    :cond_1
    return v0
.end method
