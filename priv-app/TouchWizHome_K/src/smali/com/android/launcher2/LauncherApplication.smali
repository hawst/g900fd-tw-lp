.class public Lcom/android/launcher2/LauncherApplication;
.super Landroid/app/Application;
.source "LauncherApplication.java"


# static fields
.field public static final ACTION_DYNAMICCSC_MODE:Ljava/lang/String; = "com.android.launcher.action.DYNAMICCSC_MODE_CHANGE"

.field public static final APP_ID:Ljava/lang/String; = "com.sec.android.app.launcher"

.field public static DEVICE_NAME:Ljava/lang/String; = null

.field public static final FEATURE_NAME_ENTER_APPS_MENU:Ljava/lang/String; = "MENU"

.field public static final FEATURE_NAME_HOME_PAGE_COUNT:Ljava/lang/String; = "HOME"

.field public static final FEATURE_NAME_HOTSEAT_ADD:Ljava/lang/String; = "HSAD"

.field public static final FEATURE_NAME_HOTSEAT_DELETE:Ljava/lang/String; = "HSDT"

.field public static final FEATURE_NAME_WIDGET_ADD:Ljava/lang/String; = "WGAD"

.field public static final FEATURE_NAME_WIDGET_COUNT:Ljava/lang/String; = "WGCT"

.field public static final FEATURE_NAME_WIDGET_DELETE:Ljava/lang/String; = "WGDT"

.field static final PREFERENCES_SCREENCOUNT:Ljava/lang/String; = "screencount"

.field static final PREFERENCES_SCREENCOUNT_FESTIVAL:Ljava/lang/String; = "screencount.festival"

.field static final PREFERENCES_SCREENCOUNT_SECRET:Ljava/lang/String; = "screencount.secret"

.field static final PREFERENCES_SCREENINDEX:Ljava/lang/String; = "homescreenindex"

.field static final PREFERENCES_SCREENINDEX_FESTIVAL:Ljava/lang/String; = "homescreenindex.festival"

.field static final PREFERENCES_SCREENINDEX_SECRET:Ljava/lang/String; = "homescreenindex.secret"

.field static final PREFERENCES_SCREENMODE_FOR_FESTIVAL:Ljava/lang/String; = "screenmode.festival"

.field static final PREFERENCES_SCREENMODE_FOR_SECRET:Ljava/lang/String; = "screenmode.secret"

.field public static RemovablePreloadEnabled:I = 0x0

.field public static final SHOW_DEFAULT_WALLPAPER_KEY:Ljava/lang/String; = "SHOW_DEFAULT_WALLPAPER"

.field public static final SHOW_EMPTY_PAGE_MSG_KEY:Ljava/lang/String; = "SHOW_EMPTY_PAGE_MSG"

.field private static final TAG:Ljava/lang/String; = "LauncherApplication"

.field public static sDNDBinding:Z

.field public static sDensityDpi:I

.field public static sFestivalPageLauncher:Z

.field private static sHasMenuKey:Z

.field private static sInst:Lcom/android/launcher2/LauncherApplication;

.field private static sIsAppsListAllowed:Z

.field private static sIsDeleteDropTargetTextRight:Z

.field private static sIsDeleteDropTargetTop:Z

.field public static sIsFolderColorSupport:Z

.field private static sIsLargeTabletLayout:Z

.field private static sIsScreenLarge:Z

.field private static sIsScreenSmall:Z

.field private static sIsTabletLayout:Z

.field public static sIsTheFisrt:Z

.field private static sIsUsingMoreLineinFolder:Z

.field private static sIsWidgetSearchTextColorLight:Z

.field private static sLauncher:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/launcher2/Launcher;",
            ">;"
        }
    .end annotation
.end field

.field private static sLauncherProvider:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/launcher2/LauncherProvider;",
            ">;"
        }
    .end annotation
.end field

.field private static sMaxFestivalScreenCount:I

.field private static sMaxScreenCount:I

.field private static sMaxSecretScreenCount:I

.field private static sMenuIconSize:I

.field private static sScreenCountFestival:I

.field private static sScreenCountNormal:I

.field private static sScreenCountSecret:I

.field private static sScreenDensity:F

.field private static sScreenIndexNormal:I

.field private static sScreenModeForFestival:I

.field private static sScreenModeForSecret:I

.field public static final sSecretPageLauncher:Z


# instance fields
.field public SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE:Z

.field private busBooster:Landroid/os/DVFSHelper;

.field private cpuBooster:Landroid/os/DVFSHelper;

.field private cpuMaxBooster:Landroid/os/DVFSHelper;

.field private gpuBooster:Landroid/os/DVFSHelper;

.field private final mBadgeObserver:Landroid/database/ContentObserver;

.field private mFactoryModeString:Ljava/lang/String;

.field private final mFavoritesObserver:Landroid/database/ContentObserver;

.field private mHandler:Landroid/os/Handler;

.field private mIsFactoryMode:Z

.field private mMenuAppModel:Lcom/android/launcher2/MenuAppModel;

.field private mModel:Lcom/android/launcher2/LauncherModel;

.field private mPkgResCache:Lcom/android/launcher2/PkgResCache;

.field private mPkgResCacheForMenu:Lcom/android/launcher2/PkgResCache;

.field private mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

.field private mThemeLoader:Lcom/android/launcher2/ThemeLoader;

.field private sKnox:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 76
    sput-boolean v3, Lcom/android/launcher2/LauncherApplication;->sIsFolderColorSupport:Z

    .line 148
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/launcher2/LauncherApplication;->DEVICE_NAME:Ljava/lang/String;

    .line 156
    const-string v0, "ro.config.rm_preload_enabled"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/launcher2/LauncherApplication;->RemovablePreloadEnabled:I

    .line 160
    sput-boolean v2, Lcom/android/launcher2/LauncherApplication;->sFestivalPageLauncher:Z

    .line 175
    sput-boolean v3, Lcom/android/launcher2/LauncherApplication;->sDNDBinding:Z

    .line 176
    sput v2, Lcom/android/launcher2/LauncherApplication;->sDensityDpi:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 183
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 79
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/LauncherApplication;->sKnox:I

    .line 134
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/LauncherApplication;->mHandler:Landroid/os/Handler;

    .line 147
    iput-object v1, p0, Lcom/android/launcher2/LauncherApplication;->cpuBooster:Landroid/os/DVFSHelper;

    .line 149
    iput-object v1, p0, Lcom/android/launcher2/LauncherApplication;->busBooster:Landroid/os/DVFSHelper;

    .line 150
    iput-object v1, p0, Lcom/android/launcher2/LauncherApplication;->cpuMaxBooster:Landroid/os/DVFSHelper;

    .line 151
    iput-object v1, p0, Lcom/android/launcher2/LauncherApplication;->gpuBooster:Landroid/os/DVFSHelper;

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/LauncherApplication;->SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE:Z

    .line 531
    new-instance v0, Lcom/android/launcher2/LauncherApplication$1;

    iget-object v1, p0, Lcom/android/launcher2/LauncherApplication;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/launcher2/LauncherApplication$1;-><init>(Lcom/android/launcher2/LauncherApplication;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/launcher2/LauncherApplication;->mBadgeObserver:Landroid/database/ContentObserver;

    .line 541
    new-instance v0, Lcom/android/launcher2/LauncherApplication$2;

    iget-object v1, p0, Lcom/android/launcher2/LauncherApplication;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/launcher2/LauncherApplication$2;-><init>(Lcom/android/launcher2/LauncherApplication;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/launcher2/LauncherApplication;->mFavoritesObserver:Landroid/database/ContentObserver;

    .line 184
    sput-object p0, Lcom/android/launcher2/LauncherApplication;->sInst:Lcom/android/launcher2/LauncherApplication;

    .line 185
    return-void
.end method

.method static synthetic access$000(Lcom/android/launcher2/LauncherApplication;)Lcom/android/launcher2/LauncherModel;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/LauncherApplication;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->mModel:Lcom/android/launcher2/LauncherModel;

    return-object v0
.end method

.method public static getFestivalScreenCount()I
    .locals 1

    .prologue
    .line 749
    sget v0, Lcom/android/launcher2/LauncherApplication;->sScreenCountFestival:I

    return v0
.end method

.method public static getHomeScreenIndex()I
    .locals 1

    .prologue
    .line 705
    sget v0, Lcom/android/launcher2/LauncherApplication;->sScreenIndexNormal:I

    return v0
.end method

.method public static getInst()Lcom/android/launcher2/LauncherApplication;
    .locals 1

    .prologue
    .line 179
    sget-object v0, Lcom/android/launcher2/LauncherApplication;->sInst:Lcom/android/launcher2/LauncherApplication;

    return-object v0
.end method

.method public static getMaxFestivalScreenCount()I
    .locals 1

    .prologue
    .line 843
    sget v0, Lcom/android/launcher2/LauncherApplication;->sMaxFestivalScreenCount:I

    return v0
.end method

.method public static getMaxScreenCount()I
    .locals 1

    .prologue
    .line 821
    sget v0, Lcom/android/launcher2/LauncherApplication;->sMaxScreenCount:I

    return v0
.end method

.method public static getMaxSecretScreenCount()I
    .locals 1

    .prologue
    .line 825
    sget v0, Lcom/android/launcher2/LauncherApplication;->sMaxSecretScreenCount:I

    return v0
.end method

.method public static getScreenCount()I
    .locals 1

    .prologue
    .line 743
    sget v0, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    return v0
.end method

.method public static getScreenDensity()F
    .locals 1

    .prologue
    .line 636
    sget v0, Lcom/android/launcher2/LauncherApplication;->sScreenDensity:F

    return v0
.end method

.method public static getScreenModeForFestival()I
    .locals 1

    .prologue
    .line 839
    sget v0, Lcom/android/launcher2/LauncherApplication;->sScreenModeForFestival:I

    return v0
.end method

.method public static getScreenModeForSecret()I
    .locals 1

    .prologue
    .line 816
    sget v0, Lcom/android/launcher2/LauncherApplication;->sScreenModeForSecret:I

    return v0
.end method

.method public static getSecretScreenCount()I
    .locals 1

    .prologue
    .line 746
    sget v0, Lcom/android/launcher2/LauncherApplication;->sScreenCountSecret:I

    return v0
.end method

.method public static getSmallestWidth()I
    .locals 1

    .prologue
    .line 640
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    return v0
.end method

.method public static hasMenuKey()Z
    .locals 1

    .prologue
    .line 599
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->sHasMenuKey:Z

    return v0
.end method

.method public static isAppsListAllowed()Z
    .locals 1

    .prologue
    .line 623
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->sIsAppsListAllowed:Z

    return v0
.end method

.method public static isDeleteDropTargetTextRight()Z
    .locals 1

    .prologue
    .line 619
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->sIsDeleteDropTargetTextRight:Z

    return v0
.end method

.method public static isDeleteDropTargetTop()Z
    .locals 1

    .prologue
    .line 615
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->sIsDeleteDropTargetTop:Z

    return v0
.end method

.method private isFactoryMode()Z
    .locals 2

    .prologue
    .line 445
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->mFactoryModeString:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 446
    const-string v0, "/efs/FactoryApp/factorymode"

    invoke-direct {p0, v0}, Lcom/android/launcher2/LauncherApplication;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/LauncherApplication;->mFactoryModeString:Ljava/lang/String;

    .line 447
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->mFactoryModeString:Ljava/lang/String;

    const-string v1, "ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/launcher2/LauncherApplication;->mIsFactoryMode:Z

    .line 449
    :cond_0
    iget-boolean v0, p0, Lcom/android/launcher2/LauncherApplication;->mIsFactoryMode:Z

    return v0

    .line 447
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isFactorySim()Z
    .locals 5

    .prologue
    .line 491
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 493
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    const-string v0, "999999999999999"

    .line 494
    .local v0, "IMSI":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v1

    .line 495
    .local v1, "imsi":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, "999999999999999"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 496
    const/4 v3, 0x1

    .line 498
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isKioskModeEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 666
    const/4 v0, 0x0

    .line 668
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "isKioskModeEnabled"

    invoke-static {p0, v1}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 670
    const-string v1, "true"

    const-string v2, "isKioskModeEnabled"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 671
    const/4 v1, 0x1

    .line 673
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isLargeTabletLayout()Z
    .locals 1

    .prologue
    .line 632
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->sIsLargeTabletLayout:Z

    return v0
.end method

.method public static isMenuIconSizeChanged()Z
    .locals 1

    .prologue
    .line 710
    sget v0, Lcom/android/launcher2/LauncherApplication;->sMenuIconSize:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isOwner()Z
    .locals 1

    .prologue
    .line 697
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPossibleAddToPersonal(Ljava/lang/String;)Z
    .locals 6
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 677
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " isPossibleAddToPersonal(name) = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    const/4 v2, 0x0

    .line 679
    .local v2, "isPossible":Z
    const/4 v0, 0x0

    .line 682
    .local v0, "bundle":Landroid/os/Bundle;
    :try_start_0
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/launcher2/LauncherApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/android/launcher2/LauncherApplication;->isKioskModeEnabled(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0}, Landroid/os/PersonaManager;->isPossibleAddToPersonal(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 692
    :goto_0
    return v2

    .line 682
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 684
    :catch_0
    move-exception v1

    .line 685
    .local v1, "e":Ljava/lang/NoClassDefFoundError;
    const-string v3, "LauncherApplication"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "not call android.os.PersonaManager."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 686
    .end local v1    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v1

    .line 687
    .local v1, "e":Ljava/lang/NoSuchMethodError;
    const-string v3, "LauncherApplication"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "not call isPossibleAddToPersonal."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 688
    .end local v1    # "e":Ljava/lang/NoSuchMethodError;
    :catch_2
    move-exception v1

    .line 689
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isScreenLarge()Z
    .locals 1

    .prologue
    .line 607
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->sIsScreenLarge:Z

    return v0
.end method

.method public static isScreenSmall()Z
    .locals 1

    .prologue
    .line 603
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->sIsScreenSmall:Z

    return v0
.end method

.method public static isTabletLayout()Z
    .locals 1

    .prologue
    .line 611
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->sIsTabletLayout:Z

    return v0
.end method

.method public static isUsingMoreLineinFolder()Z
    .locals 1

    .prologue
    .line 885
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->sIsUsingMoreLineinFolder:Z

    return v0
.end method

.method public static isWidgetSearchTextColorLight()Z
    .locals 1

    .prologue
    .line 627
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->sIsWidgetSearchTextColorLight:Z

    return v0
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 458
    const-string v6, ""

    .line 459
    .local v6, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 460
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 462
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463
    .end local v4    # "fr":Ljava/io/FileReader;
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v7, 0x1fa0

    invoke-direct {v1, v5, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 464
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v6

    .line 473
    if-eqz v5, :cond_0

    .line 474
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 475
    :cond_0
    if-eqz v1, :cond_1

    .line 476
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_1
    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 482
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_2
    :goto_0
    if-nez v6, :cond_7

    .line 483
    const-string v6, ""

    .line 486
    :goto_1
    return-object v6

    .line 477
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v2

    .line 478
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "LauncherApplication"

    const-string v8, "IOException close()"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 481
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 465
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 466
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    const-string v7, "LauncherApplication"

    const-string v8, "FileNotFoundException"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 473
    if-eqz v4, :cond_3

    .line 474
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 475
    :cond_3
    if-eqz v0, :cond_2

    .line 476
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 477
    :catch_2
    move-exception v2

    .line 478
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "LauncherApplication"

    const-string v8, "IOException close()"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 468
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 469
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    const-string v7, "LauncherApplication"

    const-string v8, "IOException"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 473
    if-eqz v4, :cond_4

    .line 474
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 475
    :cond_4
    if-eqz v0, :cond_2

    .line 476
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 477
    :catch_4
    move-exception v2

    .line 478
    const-string v7, "LauncherApplication"

    const-string v8, "IOException close()"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 472
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 473
    :goto_4
    if-eqz v4, :cond_5

    .line 474
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 475
    :cond_5
    if-eqz v0, :cond_6

    .line 476
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 480
    :cond_6
    :goto_5
    throw v7

    .line 477
    :catch_5
    move-exception v2

    .line 478
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "LauncherApplication"

    const-string v9, "IOException close()"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 485
    .end local v2    # "e":Ljava/io/IOException;
    :cond_7
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    goto :goto_1

    .line 472
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 468
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 465
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_8
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_9
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method public static setFestivalScreenCount(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "screenCount"    # I

    .prologue
    .line 793
    sget v2, Lcom/android/launcher2/LauncherApplication;->sScreenCountFestival:I

    if-eq v2, p1, :cond_0

    .line 794
    sput p1, Lcom/android/launcher2/LauncherApplication;->sScreenCountFestival:I

    .line 796
    const-string v2, "com.sec.android.app.launcher.prefs"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 797
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 798
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "screencount.festival"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 799
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 804
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method public static setHomeScreenIndex(Landroid/content/Context;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "homeScreenIndex"    # I

    .prologue
    .line 718
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/android/launcher2/LauncherApplication;->setHomeScreenIndex(Landroid/content/Context;IZ)V

    .line 719
    return-void
.end method

.method public static setHomeScreenIndex(Landroid/content/Context;IZ)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "homeScreenIndex"    # I
    .param p2, "updatePrefsTable"    # Z

    .prologue
    .line 723
    sget v2, Lcom/android/launcher2/LauncherApplication;->sScreenIndexNormal:I

    if-eq v2, p1, :cond_0

    .line 724
    sput p1, Lcom/android/launcher2/LauncherApplication;->sScreenIndexNormal:I

    .line 725
    const-string v2, "com.sec.android.app.launcher.prefs"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 726
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 727
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "homescreenindex"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 728
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 738
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method public static setScreenCount(Landroid/content/Context;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "screenCount"    # I

    .prologue
    .line 753
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/android/launcher2/LauncherApplication;->setScreenCount(Landroid/content/Context;IZ)V

    .line 754
    return-void
.end method

.method public static setScreenCount(Landroid/content/Context;IZ)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "screenCount"    # I
    .param p2, "updatePrefsTable"    # Z

    .prologue
    .line 758
    sget v1, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    if-eq v1, p1, :cond_0

    .line 759
    sput p1, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    .line 760
    const-string v1, "com.sec.android.app.launcher.prefs"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 761
    .local v7, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 762
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "screencount"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 763
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 775
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v7    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v1

    const-string v2, "HOME"

    const/4 v3, 0x0

    int-to-long v4, p1

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Lcom/android/launcher2/LauncherApplication;->insertLog(Ljava/lang/String;Ljava/lang/String;JZ)V

    .line 776
    return-void
.end method

.method public static setScreenModeForFestival(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "screenMode"    # I

    .prologue
    .line 830
    const-string v2, "com.sec.android.app.launcher.prefs"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 831
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 832
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "screenmode.festival"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 833
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 835
    sput p1, Lcom/android/launcher2/LauncherApplication;->sScreenModeForFestival:I

    .line 836
    return-void
.end method

.method public static setScreenModeForSecret(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "screenMode"    # I

    .prologue
    .line 807
    const-string v2, "com.sec.android.app.launcher.prefs"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 808
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 809
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "screenmode.secret"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 810
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 812
    sput p1, Lcom/android/launcher2/LauncherApplication;->sScreenModeForSecret:I

    .line 813
    return-void
.end method

.method public static setSecretScreenCount(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "screenCount"    # I

    .prologue
    .line 779
    sget v2, Lcom/android/launcher2/LauncherApplication;->sScreenCountSecret:I

    if-eq v2, p1, :cond_0

    .line 780
    sput p1, Lcom/android/launcher2/LauncherApplication;->sScreenCountSecret:I

    .line 782
    const-string v2, "com.sec.android.app.launcher.prefs"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 783
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 784
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "screencount.secret"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 785
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 790
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method


# virtual methods
.method public checkFactoryMode()Z
    .locals 1

    .prologue
    .line 454
    invoke-direct {p0}, Lcom/android/launcher2/LauncherApplication;->isFactorySim()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/launcher2/LauncherApplication;->isFactoryMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBinaryType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 506
    const-string v1, "ro.build.type"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 507
    .local v0, "type":Ljava/lang/String;
    return-object v0
.end method

.method public getBusBoosterObject()Landroid/os/DVFSHelper;
    .locals 1

    .prologue
    .line 893
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->busBooster:Landroid/os/DVFSHelper;

    return-object v0
.end method

.method public getCpuBoosterObject()Landroid/os/DVFSHelper;
    .locals 1

    .prologue
    .line 889
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->cpuBooster:Landroid/os/DVFSHelper;

    return-object v0
.end method

.method public getCpuMaxBoosterObject()Landroid/os/DVFSHelper;
    .locals 1

    .prologue
    .line 897
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->cpuMaxBooster:Landroid/os/DVFSHelper;

    return-object v0
.end method

.method public getGpuBoosterObject()Landroid/os/DVFSHelper;
    .locals 1

    .prologue
    .line 901
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->gpuBooster:Landroid/os/DVFSHelper;

    return-object v0
.end method

.method getLauncher()Lcom/android/launcher2/Launcher;
    .locals 1

    .prologue
    .line 579
    sget-object v0, Lcom/android/launcher2/LauncherApplication;->sLauncher:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 580
    sget-object v0, Lcom/android/launcher2/LauncherApplication;->sLauncher:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    .line 582
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getLauncherProvider()Lcom/android/launcher2/LauncherProvider;
    .locals 1

    .prologue
    .line 590
    sget-object v0, Lcom/android/launcher2/LauncherApplication;->sLauncherProvider:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherProvider;

    return-object v0
.end method

.method getModel()Lcom/android/launcher2/LauncherModel;
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->mModel:Lcom/android/launcher2/LauncherModel;

    return-object v0
.end method

.method getPkgResCache()Lcom/android/launcher2/PkgResCache;
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->mPkgResCache:Lcom/android/launcher2/PkgResCache;

    return-object v0
.end method

.method getPkgResCacheForMenu()Lcom/android/launcher2/PkgResCache;
    .locals 1

    .prologue
    .line 714
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->mPkgResCacheForMenu:Lcom/android/launcher2/PkgResCache;

    return-object v0
.end method

.method public getSpanCalculator()Lcom/android/launcher2/WorkspaceSpanCalculator;
    .locals 1

    .prologue
    .line 571
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    return-object v0
.end method

.method getThemeLoader()Lcom/android/launcher2/ThemeLoader;
    .locals 1

    .prologue
    .line 567
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->mThemeLoader:Lcom/android/launcher2/ThemeLoader;

    return-object v0
.end method

.method public insertLog(Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 7
    .param p1, "feature"    # Ljava/lang/String;
    .param p2, "extra"    # Ljava/lang/String;
    .param p3, "value"    # J
    .param p5, "status"    # Z

    .prologue
    .line 923
    iget-boolean v2, p0, Lcom/android/launcher2/LauncherApplication;->SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE:Z

    if-eqz v2, :cond_2

    .line 924
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 925
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v2, "app_id"

    const-string v3, "com.sec.android.app.launcher"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    const-string v2, "feature"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    if-eqz p2, :cond_0

    .line 928
    const-string v2, "extra"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 929
    :cond_0
    const-wide/16 v2, -0x1

    cmp-long v2, p3, v2

    if-eqz v2, :cond_1

    .line 930
    const-string v2, "value"

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 932
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 934
    .local v0, "broadcastIntent":Landroid/content/Intent;
    if-eqz p5, :cond_3

    .line 935
    const-string v2, "com.samsung.android.providers.context.log.action.REPORT_APP_STATUS_SURVEY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 939
    :goto_0
    const-string v2, "data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 941
    const-string v2, "com.samsung.android.providers.context"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 943
    invoke-virtual {p0, v0}, Lcom/android/launcher2/LauncherApplication;->sendBroadcast(Landroid/content/Intent;)V

    .line 945
    .end local v0    # "broadcastIntent":Landroid/content/Intent;
    .end local v1    # "cv":Landroid/content/ContentValues;
    :cond_2
    return-void

    .line 937
    .restart local v0    # "broadcastIntent":Landroid/content/Intent;
    .restart local v1    # "cv":Landroid/content/ContentValues;
    :cond_3
    const-string v2, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public isKnoxMode()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 646
    iget v4, p0, Lcom/android/launcher2/LauncherApplication;->sKnox:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 647
    iput v3, p0, Lcom/android/launcher2/LauncherApplication;->sKnox:I

    .line 648
    const/4 v0, 0x0

    .line 650
    .local v0, "bundle":Landroid/os/Bundle;
    :try_start_0
    const-string v4, "isKnoxMode"

    invoke-static {p0, v4}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 652
    const-string v4, "2.0"

    const-string v5, "version"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "true"

    const-string v5, "isKnoxMode"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 654
    const/4 v4, 0x1

    iput v4, p0, Lcom/android/launcher2/LauncherApplication;->sKnox:I
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    .line 662
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    :goto_0
    iget v4, p0, Lcom/android/launcher2/LauncherApplication;->sKnox:I

    if-ne v4, v2, :cond_1

    :goto_1
    return v2

    .line 656
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 657
    .local v1, "e":Ljava/lang/NoClassDefFoundError;
    const-string v4, "LauncherApplication"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "not call android.os.PersonaManager."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 658
    .end local v1    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v1

    .line 659
    .local v1, "e":Ljava/lang/NoSuchMethodError;
    const-string v4, "LauncherApplication"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "not call getKnoxInfoForApp."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "e":Ljava/lang/NoSuchMethodError;
    :cond_1
    move v2, v3

    .line 662
    goto :goto_1
.end method

.method public onCreate()V
    .locals 14

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 189
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 192
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 193
    .local v6, "res":Landroid/content/res/Resources;
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v9

    if-nez v9, :cond_0

    const v9, 0x7f0b0002

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    if-eqz v9, :cond_11

    :cond_0
    move v9, v11

    :goto_0
    sput-boolean v9, Lcom/android/launcher2/LauncherApplication;->sHasMenuKey:Z

    .line 194
    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v8, v9, 0xf

    .line 195
    .local v8, "screenSize":I
    if-ne v8, v11, :cond_12

    move v9, v11

    :goto_1
    sput-boolean v9, Lcom/android/launcher2/LauncherApplication;->sIsScreenSmall:Z

    .line 196
    const/4 v9, 0x3

    if-eq v8, v9, :cond_1

    const/4 v9, 0x4

    if-ne v8, v9, :cond_13

    :cond_1
    move v9, v11

    :goto_2
    sput-boolean v9, Lcom/android/launcher2/LauncherApplication;->sIsScreenLarge:Z

    .line 198
    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->density:F

    sput v9, Lcom/android/launcher2/LauncherApplication;->sScreenDensity:F

    .line 199
    const v9, 0x7f0b0003

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    sput-boolean v9, Lcom/android/launcher2/LauncherApplication;->sIsTabletLayout:Z

    .line 200
    const v9, 0x7f0b0007

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    sput-boolean v9, Lcom/android/launcher2/LauncherApplication;->sIsDeleteDropTargetTop:Z

    .line 201
    const v9, 0x7f0b0008

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    sput-boolean v9, Lcom/android/launcher2/LauncherApplication;->sIsDeleteDropTargetTextRight:Z

    .line 202
    const v9, 0x7f0b0004

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    sput-boolean v9, Lcom/android/launcher2/LauncherApplication;->sIsAppsListAllowed:Z

    .line 203
    const v9, 0x7f0b0006

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    sput-boolean v9, Lcom/android/launcher2/LauncherApplication;->sIsWidgetSearchTextColorLight:Z

    .line 204
    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v12, 0x320

    if-lt v9, v12, :cond_14

    move v9, v11

    :goto_3
    sput-boolean v9, Lcom/android/launcher2/LauncherApplication;->sIsLargeTabletLayout:Z

    .line 205
    const v9, 0x7f0b000f

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    sput-boolean v9, Lcom/android/launcher2/LauncherApplication;->sIsUsingMoreLineinFolder:Z

    .line 207
    new-instance v9, Lcom/android/launcher2/ThemeLoader;

    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-direct {v9, v12}, Lcom/android/launcher2/ThemeLoader;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mThemeLoader:Lcom/android/launcher2/ThemeLoader;

    .line 209
    sget-object v9, Lcom/android/launcher2/MenuAppModel;->INSTANCE:Lcom/android/launcher2/MenuAppModel;

    iput-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mMenuAppModel:Lcom/android/launcher2/MenuAppModel;

    .line 210
    iget-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mMenuAppModel:Lcom/android/launcher2/MenuAppModel;

    invoke-virtual {v9, p0}, Lcom/android/launcher2/MenuAppModel;->setLauncherApplication(Lcom/android/launcher2/LauncherApplication;)V

    .line 211
    new-instance v9, Lcom/android/launcher2/PkgResCache;

    invoke-direct {v9, p0}, Lcom/android/launcher2/PkgResCache;-><init>(Lcom/android/launcher2/LauncherApplication;)V

    iput-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mPkgResCache:Lcom/android/launcher2/PkgResCache;

    .line 213
    const v9, 0x7f0b000d

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    .line 214
    .local v4, "isMenuAppsIconSizeChanged":Z
    sput v10, Lcom/android/launcher2/LauncherApplication;->sMenuIconSize:I

    .line 215
    if-eqz v4, :cond_2

    .line 216
    const/4 v0, 0x0

    .line 218
    .local v0, "appIconSize":I
    :try_start_0
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v12, 0x7f0e0285

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v9, v9

    sput v9, Lcom/android/launcher2/LauncherApplication;->sMenuIconSize:I

    .line 219
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v12, 0x7f0e00b4

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDimension(I)F
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    float-to-int v0, v9

    .line 223
    :goto_4
    sget v9, Lcom/android/launcher2/LauncherApplication;->sMenuIconSize:I

    if-ne v9, v0, :cond_2

    .line 224
    sput v10, Lcom/android/launcher2/LauncherApplication;->sMenuIconSize:I

    .line 230
    .end local v0    # "appIconSize":I
    :cond_2
    iget-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mThemeLoader:Lcom/android/launcher2/ThemeLoader;

    invoke-virtual {v9}, Lcom/android/launcher2/ThemeLoader;->isSupportFestival()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 231
    sput-boolean v11, Lcom/android/launcher2/LauncherApplication;->sFestivalPageLauncher:Z

    .line 251
    :cond_3
    sget-boolean v9, Lcom/android/launcher2/LauncherApplication;->sFestivalPageLauncher:Z

    if-eqz v9, :cond_15

    .line 252
    new-instance v9, Lcom/android/launcher2/LauncherExModel;

    iget-object v12, p0, Lcom/android/launcher2/LauncherApplication;->mPkgResCache:Lcom/android/launcher2/PkgResCache;

    invoke-direct {v9, p0, v12}, Lcom/android/launcher2/LauncherExModel;-><init>(Lcom/android/launcher2/LauncherApplication;Lcom/android/launcher2/PkgResCache;)V

    iput-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mModel:Lcom/android/launcher2/LauncherModel;

    .line 253
    new-instance v3, Landroid/content/IntentFilter;

    const-string v9, "com.bst.action.PICKUP_FESTIVAL"

    invoke-direct {v3, v9}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 254
    .local v3, "filter":Landroid/content/IntentFilter;
    const-string v9, "com.bst.action.PICKUP_COMMON"

    invoke-virtual {v3, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 255
    const-string v9, "com.sec.festival.FESTIVAL_SETTINGS_CHANGED"

    invoke-virtual {v3, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 256
    const-string v9, "com.sec.festival.FESTIVAL_HOME_CHANGED"

    invoke-virtual {v3, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 261
    iget-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mModel:Lcom/android/launcher2/LauncherModel;

    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/LauncherApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 270
    .end local v3    # "filter":Landroid/content/IntentFilter;
    :goto_5
    sget-object v9, Lcom/android/launcher2/WorkspaceSpanCalculator;->INSTANCE:Lcom/android/launcher2/WorkspaceSpanCalculator;

    iput-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    .line 271
    iget-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    invoke-virtual {v9, p0}, Lcom/android/launcher2/WorkspaceSpanCalculator;->setLauncherApplication(Lcom/android/launcher2/LauncherApplication;)V

    .line 274
    new-instance v3, Landroid/content/IntentFilter;

    const-string v9, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v3, v9}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 275
    .restart local v3    # "filter":Landroid/content/IntentFilter;
    const-string v9, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v3, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 276
    const-string v9, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v3, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 278
    const-string v9, "android.intent.action.STK_TITLE_IS_LOADED"

    invoke-virtual {v3, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 280
    const-string v9, "package"

    invoke-virtual {v3, v9}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 281
    iget-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mModel:Lcom/android/launcher2/LauncherModel;

    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/LauncherApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 283
    new-instance v3, Landroid/content/IntentFilter;

    .end local v3    # "filter":Landroid/content/IntentFilter;
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 284
    .restart local v3    # "filter":Landroid/content/IntentFilter;
    const-string v9, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    invoke-virtual {v3, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 285
    const-string v9, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    invoke-virtual {v3, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 286
    const-string v9, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v3, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 288
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v9

    const-string v12, "CscFeature_Common_EnableSprintExtension"

    invoke-virtual {v9, v12}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 289
    const-string v9, "com.sec.sprextension.FORCE_LAUNCHER_REFRESH"

    invoke-virtual {v3, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 298
    :cond_4
    iget-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mModel:Lcom/android/launcher2/LauncherModel;

    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/LauncherApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 301
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 302
    .local v7, "resolver":Landroid/content/ContentResolver;
    sget-object v9, Lcom/android/launcher2/LauncherSettings$Favorites;->CONTENT_URI:Landroid/net/Uri;

    iget-object v12, p0, Lcom/android/launcher2/LauncherApplication;->mFavoritesObserver:Landroid/database/ContentObserver;

    invoke-virtual {v7, v9, v11, v12}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 307
    const-string v9, "com.sec.android.app.launcher.prefs"

    invoke-virtual {p0, v9, v10}, Lcom/android/launcher2/LauncherApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 308
    .local v5, "prefs":Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    .line 313
    .local v1, "csc":Lcom/sec/android/app/CscFeature;
    const v9, 0x7f0c0009

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    sput v9, Lcom/android/launcher2/LauncherApplication;->sMaxScreenCount:I

    .line 321
    const-string v9, "screencount"

    invoke-interface {v5, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    sput v9, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    .line 322
    sget v9, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    if-lez v9, :cond_5

    sget v9, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    sget v12, Lcom/android/launcher2/LauncherApplication;->sMaxScreenCount:I

    if-le v9, v12, :cond_9

    .line 323
    :cond_5
    const-string v9, "CscFeature_Launcher_TotalPageCount"

    invoke-virtual {v1, v9}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v9

    sput v9, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    .line 325
    sget v9, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    if-lez v9, :cond_6

    sget v9, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    sget v12, Lcom/android/launcher2/LauncherApplication;->sMaxScreenCount:I

    if-le v9, v12, :cond_8

    .line 326
    :cond_6
    const v9, 0x7f0c0007

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    sput v9, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    .line 327
    sget v9, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    if-lez v9, :cond_7

    sget v9, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    sget v12, Lcom/android/launcher2/LauncherApplication;->sMaxScreenCount:I

    if-le v9, v12, :cond_8

    .line 328
    :cond_7
    sget v9, Lcom/android/launcher2/LauncherApplication;->sMaxScreenCount:I

    sput v9, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    .line 342
    :cond_8
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 343
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v9, "screencount"

    sget v12, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    invoke-interface {v2, v9, v12}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 344
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 353
    .end local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_9
    const-string v9, "homescreenindex"

    const/4 v12, -0x1

    invoke-interface {v5, v9, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    sput v9, Lcom/android/launcher2/LauncherApplication;->sScreenIndexNormal:I

    .line 354
    sget v9, Lcom/android/launcher2/LauncherApplication;->sScreenIndexNormal:I

    if-ltz v9, :cond_a

    sget v9, Lcom/android/launcher2/LauncherApplication;->sScreenIndexNormal:I

    sget v12, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    if-lt v9, v12, :cond_e

    .line 355
    :cond_a
    const-string v9, "CscFeature_Launcher_DefaultPageNumber"

    invoke-virtual {v1, v9}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v9

    sput v9, Lcom/android/launcher2/LauncherApplication;->sScreenIndexNormal:I

    .line 356
    sget v9, Lcom/android/launcher2/LauncherApplication;->sScreenIndexNormal:I

    if-ltz v9, :cond_b

    sget v9, Lcom/android/launcher2/LauncherApplication;->sScreenIndexNormal:I

    sget v12, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    if-lt v9, v12, :cond_d

    .line 357
    :cond_b
    const v9, 0x7f0c000a

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    sput v9, Lcom/android/launcher2/LauncherApplication;->sScreenIndexNormal:I

    .line 358
    sget v9, Lcom/android/launcher2/LauncherApplication;->sScreenIndexNormal:I

    if-ltz v9, :cond_c

    sget v9, Lcom/android/launcher2/LauncherApplication;->sScreenIndexNormal:I

    sget v12, Lcom/android/launcher2/LauncherApplication;->sScreenCountNormal:I

    if-lt v9, v12, :cond_d

    .line 359
    :cond_c
    sput v10, Lcom/android/launcher2/LauncherApplication;->sScreenIndexNormal:I

    .line 364
    :cond_d
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 365
    .restart local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v9, "homescreenindex"

    sget v12, Lcom/android/launcher2/LauncherApplication;->sScreenIndexNormal:I

    invoke-interface {v2, v9, v12}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 366
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 388
    .end local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_e
    sget-boolean v9, Lcom/android/launcher2/LauncherApplication;->sFestivalPageLauncher:Z

    if-eqz v9, :cond_10

    .line 389
    const v9, 0x7f0c000d

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    sput v9, Lcom/android/launcher2/LauncherApplication;->sMaxFestivalScreenCount:I

    .line 391
    const-string v9, "screencount.festival"

    invoke-interface {v5, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    sput v9, Lcom/android/launcher2/LauncherApplication;->sScreenCountFestival:I

    .line 392
    const-string v9, "screenmode.festival"

    invoke-interface {v5, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    sput v9, Lcom/android/launcher2/LauncherApplication;->sScreenModeForFestival:I

    .line 394
    sget v9, Lcom/android/launcher2/LauncherApplication;->sScreenCountFestival:I

    if-ltz v9, :cond_f

    sget v9, Lcom/android/launcher2/LauncherApplication;->sScreenCountFestival:I

    sget v12, Lcom/android/launcher2/LauncherApplication;->sMaxFestivalScreenCount:I

    if-le v9, v12, :cond_10

    .line 396
    :cond_f
    sput v10, Lcom/android/launcher2/LauncherApplication;->sScreenCountFestival:I

    .line 398
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 399
    .restart local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v9, "screencount.festival"

    sget v10, Lcom/android/launcher2/LauncherApplication;->sScreenCountFestival:I

    invoke-interface {v2, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 400
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 417
    .end local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_10
    new-instance v9, Landroid/os/DVFSHelper;

    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const/16 v12, 0xc

    invoke-direct {v9, v10, v12}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    iput-object v9, p0, Lcom/android/launcher2/LauncherApplication;->cpuBooster:Landroid/os/DVFSHelper;

    .line 418
    new-instance v9, Landroid/os/DVFSHelper;

    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const/16 v12, 0x13

    invoke-direct {v9, v10, v12}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    iput-object v9, p0, Lcom/android/launcher2/LauncherApplication;->busBooster:Landroid/os/DVFSHelper;

    .line 419
    new-instance v9, Landroid/os/DVFSHelper;

    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const/16 v12, 0xe

    invoke-direct {v9, v10, v12}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    iput-object v9, p0, Lcom/android/launcher2/LauncherApplication;->cpuMaxBooster:Landroid/os/DVFSHelper;

    .line 423
    new-instance v9, Landroid/os/DVFSHelper;

    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const/16 v12, 0x10

    invoke-direct {v9, v10, v12}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    iput-object v9, p0, Lcom/android/launcher2/LauncherApplication;->gpuBooster:Landroid/os/DVFSHelper;

    .line 424
    iget-object v9, p0, Lcom/android/launcher2/LauncherApplication;->gpuBooster:Landroid/os/DVFSHelper;

    const-string v10, "Launcher_homemenu"

    invoke-virtual {v9, v10}, Landroid/os/DVFSHelper;->addExtraOptionsByDefaultPolicy(Ljava/lang/String;)V

    .line 427
    sget-object v9, Lcom/android/launcher2/BadgeCache;->BADGE_URI:Landroid/net/Uri;

    iget-object v10, p0, Lcom/android/launcher2/LauncherApplication;->mBadgeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v7, v9, v11, v10}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 428
    iget-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mModel:Lcom/android/launcher2/LauncherModel;

    invoke-virtual {v9}, Lcom/android/launcher2/LauncherModel;->reloadBadges()V

    .line 433
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v9

    const-string v10, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v9, v10}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, p0, Lcom/android/launcher2/LauncherApplication;->SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE:Z

    .line 434
    sput-boolean v11, Lcom/android/launcher2/LauncherApplication;->sIsTheFisrt:Z

    .line 435
    return-void

    .end local v1    # "csc":Lcom/sec/android/app/CscFeature;
    .end local v3    # "filter":Landroid/content/IntentFilter;
    .end local v4    # "isMenuAppsIconSizeChanged":Z
    .end local v5    # "prefs":Landroid/content/SharedPreferences;
    .end local v7    # "resolver":Landroid/content/ContentResolver;
    .end local v8    # "screenSize":I
    :cond_11
    move v9, v10

    .line 193
    goto/16 :goto_0

    .restart local v8    # "screenSize":I
    :cond_12
    move v9, v10

    .line 195
    goto/16 :goto_1

    :cond_13
    move v9, v10

    .line 196
    goto/16 :goto_2

    :cond_14
    move v9, v10

    .line 204
    goto/16 :goto_3

    .line 263
    .restart local v4    # "isMenuAppsIconSizeChanged":Z
    :cond_15
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isMenuIconSizeChanged()Z

    move-result v9

    if-eqz v9, :cond_16

    .line 264
    new-instance v9, Lcom/android/launcher2/PkgResCache;

    sget v12, Lcom/android/launcher2/LauncherApplication;->sMenuIconSize:I

    invoke-direct {v9, p0, v12}, Lcom/android/launcher2/PkgResCache;-><init>(Lcom/android/launcher2/LauncherApplication;I)V

    iput-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mPkgResCacheForMenu:Lcom/android/launcher2/PkgResCache;

    .line 265
    new-instance v9, Lcom/android/launcher2/LauncherModel;

    iget-object v12, p0, Lcom/android/launcher2/LauncherApplication;->mPkgResCache:Lcom/android/launcher2/PkgResCache;

    iget-object v13, p0, Lcom/android/launcher2/LauncherApplication;->mPkgResCacheForMenu:Lcom/android/launcher2/PkgResCache;

    invoke-direct {v9, p0, v12, v13}, Lcom/android/launcher2/LauncherModel;-><init>(Lcom/android/launcher2/LauncherApplication;Lcom/android/launcher2/PkgResCache;Lcom/android/launcher2/PkgResCache;)V

    iput-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mModel:Lcom/android/launcher2/LauncherModel;

    goto/16 :goto_5

    .line 267
    :cond_16
    new-instance v9, Lcom/android/launcher2/LauncherModel;

    iget-object v12, p0, Lcom/android/launcher2/LauncherApplication;->mPkgResCache:Lcom/android/launcher2/PkgResCache;

    invoke-direct {v9, p0, v12}, Lcom/android/launcher2/LauncherModel;-><init>(Lcom/android/launcher2/LauncherApplication;Lcom/android/launcher2/PkgResCache;)V

    iput-object v9, p0, Lcom/android/launcher2/LauncherApplication;->mModel:Lcom/android/launcher2/LauncherModel;

    goto/16 :goto_5

    .line 220
    .restart local v0    # "appIconSize":I
    :catch_0
    move-exception v9

    goto/16 :goto_4
.end method

.method public onTerminate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 515
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 517
    iget-object v1, p0, Lcom/android/launcher2/LauncherApplication;->mModel:Lcom/android/launcher2/LauncherModel;

    invoke-virtual {p0, v1}, Lcom/android/launcher2/LauncherApplication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 519
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 520
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/android/launcher2/LauncherApplication;->mFavoritesObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 521
    iget-object v1, p0, Lcom/android/launcher2/LauncherApplication;->mBadgeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 522
    iput-object v2, p0, Lcom/android/launcher2/LauncherApplication;->cpuBooster:Landroid/os/DVFSHelper;

    .line 523
    iput-object v2, p0, Lcom/android/launcher2/LauncherApplication;->busBooster:Landroid/os/DVFSHelper;

    .line 524
    iput-object v2, p0, Lcom/android/launcher2/LauncherApplication;->cpuMaxBooster:Landroid/os/DVFSHelper;

    .line 525
    iput-object v2, p0, Lcom/android/launcher2/LauncherApplication;->gpuBooster:Landroid/os/DVFSHelper;

    .line 526
    return-void
.end method

.method setLauncher(Lcom/android/launcher2/Launcher;)V
    .locals 1
    .param p1, "provider"    # Lcom/android/launcher2/Launcher;

    .prologue
    .line 575
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/android/launcher2/LauncherApplication;->sLauncher:Ljava/lang/ref/WeakReference;

    .line 576
    return-void
.end method

.method setLauncherProvider(Lcom/android/launcher2/LauncherProvider;)V
    .locals 1
    .param p1, "provider"    # Lcom/android/launcher2/LauncherProvider;

    .prologue
    .line 586
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/android/launcher2/LauncherApplication;->sLauncherProvider:Ljava/lang/ref/WeakReference;

    .line 587
    return-void
.end method

.method public updatePageCount()V
    .locals 0

    .prologue
    .line 851
    return-void
.end method
