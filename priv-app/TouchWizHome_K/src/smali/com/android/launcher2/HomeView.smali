.class public final Lcom/android/launcher2/HomeView;
.super Landroid/widget/FrameLayout;
.source "HomeView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/android/launcher2/CreateFolderDialog$CreateFolderInterface;
.implements Lcom/android/launcher2/Launcher$ActivityResultCallback;
.implements Lcom/android/launcher2/Launcher$HardwareKeys;
.implements Lcom/android/launcher2/Launcher$StateAnimatorProvider;
.implements Lcom/android/launcher2/LifeCycle;
.implements Lcom/android/launcher2/PagedView$PageSwitchListener;
.implements Lcom/android/launcher2/QuickViewWorkspace$WorkspaceQuickViewInfoProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/launcher2/HomeView$28;,
        Lcom/android/launcher2/HomeView$LayoutParams;,
        Lcom/android/launcher2/HomeView$DropPos;,
        Lcom/android/launcher2/HomeView$HomeEditTabClickListener;,
        Lcom/android/launcher2/HomeView$AppWidgetResetObserver;,
        Lcom/android/launcher2/HomeView$SavedState;,
        Lcom/android/launcher2/HomeView$PendingAddArguments;
    }
.end annotation


# static fields
.field static final APPWIDGET_HOST_ID:I = 0x400

.field public static final CHOOSER_MODE:Ljava/lang/String; = "mode"

.field private static final DEBUGGABLE:Z

.field static final DEBUG_WIDGETS:Z = false

.field static final EXTRA_SHORTCUT_DUPLICATE:Ljava/lang/String; = "duplicate"

.field static final LOGD:Z = false

.field static final MENU_GROUP_WALLPAPER:I = 0x1

.field static final MENU_HELP:I = 0x5

.field static final MENU_MANAGE_APPS:I = 0x3

.field static final MENU_SYSTEM_SETTINGS:I = 0x4

.field static final MENU_WALLPAPER_SETTINGS:I = 0x2

.field private static final OPEN_FOLDER_ID:Ljava/lang/String; = "launcher.workspace_open_folder_id"

.field private static final PENDING_FOLDER_EDIT_TEXT:Ljava/lang/String; = "launcher.workspace_pending_folder_edit_text"

.field private static final PENDING_FOLDER_EDIT_TEXT_SELECTION_END:Ljava/lang/String; = "launcher.workspace_pending_folder_edit_text_selection_end"

.field private static final PENDING_FOLDER_EDIT_TEXT_SELECTION_START:Ljava/lang/String; = "launcher.workspace_pending_folder_edit_text_selection_start"

.field static final PROFILE_STARTUP:Z = false

.field static final REQUEST_CREATE_APPWIDGET:I = 0x5

.field static final REQUEST_CREATE_NEW_FOLDER_WITH_SHORTCUT_ITEM:I = 0x2

.field static final REQUEST_CREATE_SHORTCUT:I = 0x1

.field static final REQUEST_PICK_APPWIDGET:I = 0x9

.field static final REQUEST_PICK_SHORTCUT:I = 0x7

.field static final REQUEST_PICK_WALLPAPER:I = 0xa

.field public static final RESPONSE_CODE_SHOW_WIDGETS:I = -0x2710

.field private static final TAG:Ljava/lang/String; = "Launcher.HomeView"

.field private static final TOOLBAR_ICON_METADATA_NAME:Ljava/lang/String; = "com.android.launcher.toolbar_icon"

.field static final WIDGET_BACKGROUND:I = 0x1000000

.field private static cpuBooster:Landroid/os/DVFSHelper; = null

.field static mDestinationNewFolderId:J = 0x0L

.field static mIsAllAppsButtonDisable:Z = false

.field private static sCurrentRotationAngle:I = 0x0

.field static final sDumpLogs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static sFolders:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/launcher2/HomeFolderItem;",
            ">;"
        }
    .end annotation
.end field

.field public static sIsBindHotseat:Z = false

.field static sIsDragState:Z = false

.field static sPanelDrawer:Lcom/android/launcher2/PanelDrawer; = null

.field private static sPendingAddList:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/launcher2/HomeView$PendingAddArguments;",
            ">;"
        }
    .end annotation
.end field

.field private static sTouchPt:Landroid/graphics/PointF; = null

.field private static final sTransitionInDuration:I = 0xc8

.field private static final sTransitionOutDuration:I = 0xaf


# instance fields
.field private final ADVANCE_MSG:I

.field private MyFilesButtonClickListener:Landroid/view/View$OnClickListener;

.field private final REMOVE_MARKETSAMPLE:I

.field private final SHOW_MARKETSAMPLE:I

.field private SearchButtonClickListener:Landroid/view/View$OnClickListener;

.field private VoiceSearchButtonClickListener:Landroid/view/View$OnClickListener;

.field public isHelpAppPageAdded:Z

.field public isHelpAppPageDeleted:Z

.field private final mAdvanceInterval:I

.field private final mAdvanceStagger:I

.field private mAfterSavedInstanceState:Z

.field private mAllAppsIconTouchListener:Landroid/view/View$OnTouchListener;

.field private mAppWidgetBinder:Lcom/android/launcher2/AppWidgetBinder;

.field private mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

.field private mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

.field private mAutoAdvanceRunning:Z

.field private mAutoAdvanceSentTime:J

.field private mAutoAdvanceTimeLeft:J

.field private mCameraCut:Landroid/widget/ImageView;

.field private final mChangeTphoneModeObserver:Landroid/database/ContentObserver;

.field private mCreateFolderColor:I

.field private mCurrentDragItem:Lcom/android/launcher2/BaseItem;

.field private mCurrentResizeWidgetItem:Lcom/android/launcher2/BaseItem;

.field private mDarkenLayerTouchListener:Landroid/view/View$OnTouchListener;

.field private mDarkenView:Landroid/view/View;

.field private mDeleteDropLayout:Lcom/android/launcher2/QuickViewDragBar;

.field private mEditBar:Lcom/android/launcher2/HomeEditBar;

.field private mEditIcon:Landroid/view/View;

.field private mEditIconDivider:Landroid/view/View;

.field private mFolderBundle:Landroid/os/Bundle;

.field private mGoogleSearchView:Landroid/view/View;

.field private final mHandler:Landroid/os/Handler;

.field private final mHelphubObserver:Landroid/database/ContentObserver;

.field private mHomeAppsBtn:Landroid/view/View;

.field private mHomeBottomBar:Landroid/view/View;

.field private mHomeContainer:Landroid/view/View;

.field private mHomeDarkenLayer:Landroid/view/View;

.field private mHomeEditButtonListener:Lcom/android/launcher2/HomeView$HomeEditTabClickListener;

.field mHomeEditItem:Landroid/view/MenuItem;

.field private mHomeEditTitleBar:Landroid/view/ViewGroup;

.field public mHomeKeyPress:Z

.field private mHomePhoneBtn:Landroid/view/View;

.field private mHomeScreenOptionMenu:Lcom/android/launcher2/HomeScreenOptionMenu;

.field private mHomeSearchBtn:Landroid/view/View;

.field private mHomeTopBar:Landroid/view/View;

.field private mHotseat:Lcom/android/launcher2/Hotseat;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mIsDeletePopup:Z

.field public mIsHelpItemAdded:Z

.field private mMarketSample:Landroid/view/View;

.field private mMarketSampleTouchListener:Landroid/view/View$OnTouchListener;

.field private mModel:Lcom/android/launcher2/LauncherModel;

.field private mMyFilesView:Landroid/view/View;

.field private mPanelBackgroundAlpha:F

.field private mPendingAddInfo:Lcom/android/launcher2/HomeItem;

.field private mPkgResCache:Lcom/android/launcher2/PkgResCache;

.field private mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

.field private mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

.field private mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

.field private mRestoring:Z

.field private mSavedState:Lcom/android/launcher2/HomeView$SavedState;

.field private mShadow:Lcom/android/launcher2/ShadowBuilder;

.field private mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

.field private mTargetFolderId:J

.field private mTiltWallpaperScroller:Lcom/sec/dtl/launcher/WallpaperScroller;

.field private mTmpAddItemCellCoordinates:[I

.field private mTmpAddItemSpans:[I

.field private mVoiceSearchView:Landroid/view/View;

.field mWaitingForResult:Z

.field private final mWidgetObserver:Landroid/database/ContentObserver;

.field private mWidgetsToAdvance:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Landroid/appwidget/AppWidgetProviderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mWorkspace:Lcom/android/launcher2/Workspace;

.field private mWorkspaceLoading:Z

.field private removeHotseat:Z

.field private sIsHeadlinesHiddenForEditMode:Z

.field private screenIndexBeforeHelpAppPageAddition:I

.field private screenIndexFestivalHelpAppPageAddition:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 125
    invoke-static {}, Lcom/android/launcher2/Utilities;->DEBUGGABLE()Z

    move-result v0

    sput-boolean v0, Lcom/android/launcher2/HomeView;->DEBUGGABLE:Z

    .line 127
    const/4 v0, 0x0

    sput-object v0, Lcom/android/launcher2/HomeView;->cpuBooster:Landroid/os/DVFSHelper;

    .line 132
    sput-boolean v2, Lcom/android/launcher2/HomeView;->mIsAllAppsButtonDisable:Z

    .line 219
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/launcher2/HomeView;->sFolders:Ljava/util/HashMap;

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/launcher2/HomeView;->sDumpLogs:Ljava/util/ArrayList;

    .line 248
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/launcher2/HomeView;->sPendingAddList:Ljava/util/ArrayList;

    .line 254
    new-instance v0, Lcom/android/launcher2/PanelDrawer;

    invoke-direct {v0}, Lcom/android/launcher2/PanelDrawer;-><init>()V

    sput-object v0, Lcom/android/launcher2/HomeView;->sPanelDrawer:Lcom/android/launcher2/PanelDrawer;

    .line 1422
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/android/launcher2/HomeView;->mDestinationNewFolderId:J

    .line 5110
    sput v2, Lcom/android/launcher2/HomeView;->sCurrentRotationAngle:I

    .line 5186
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    sput-object v0, Lcom/android/launcher2/HomeView;->sTouchPt:Landroid/graphics/PointF;

    .line 5254
    sput-boolean v2, Lcom/android/launcher2/HomeView;->sIsDragState:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 285
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/HomeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 286
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 289
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/HomeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 290
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v7, -0x1

    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 293
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 133
    iput-boolean v4, p0, Lcom/android/launcher2/HomeView;->removeHotseat:Z

    .line 162
    new-instance v1, Lcom/android/launcher2/HomeView$AppWidgetResetObserver;

    invoke-direct {v1, p0}, Lcom/android/launcher2/HomeView$AppWidgetResetObserver;-><init>(Lcom/android/launcher2/HomeView;)V

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mWidgetObserver:Landroid/database/ContentObserver;

    .line 175
    new-instance v1, Lcom/android/launcher2/HomeItem;

    invoke-direct {v1}, Lcom/android/launcher2/HomeItem;-><init>()V

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    .line 176
    new-array v1, v2, [I

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mTmpAddItemCellCoordinates:[I

    .line 177
    new-array v1, v2, [I

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mTmpAddItemSpans:[I

    .line 193
    iput-object v5, p0, Lcom/android/launcher2/HomeView;->mHomePhoneBtn:Landroid/view/View;

    .line 194
    iput-object v5, p0, Lcom/android/launcher2/HomeView;->mHomeAppsBtn:Landroid/view/View;

    .line 201
    new-instance v1, Lcom/android/launcher2/HomeView$HomeEditTabClickListener;

    invoke-direct {v1, p0}, Lcom/android/launcher2/HomeView$HomeEditTabClickListener;-><init>(Lcom/android/launcher2/HomeView;)V

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mHomeEditButtonListener:Lcom/android/launcher2/HomeView$HomeEditTabClickListener;

    .line 202
    iput-object v5, p0, Lcom/android/launcher2/HomeView;->mEditIcon:Landroid/view/View;

    .line 207
    iput-boolean v4, p0, Lcom/android/launcher2/HomeView;->mAutoAdvanceRunning:Z

    .line 209
    iput-boolean v6, p0, Lcom/android/launcher2/HomeView;->mWorkspaceLoading:Z

    .line 223
    iput v6, p0, Lcom/android/launcher2/HomeView;->ADVANCE_MSG:I

    .line 224
    const/16 v1, 0x4e20

    iput v1, p0, Lcom/android/launcher2/HomeView;->mAdvanceInterval:I

    .line 225
    const/16 v1, 0xfa

    iput v1, p0, Lcom/android/launcher2/HomeView;->mAdvanceStagger:I

    .line 226
    iput v2, p0, Lcom/android/launcher2/HomeView;->SHOW_MARKETSAMPLE:I

    .line 227
    const/4 v1, 0x3

    iput v1, p0, Lcom/android/launcher2/HomeView;->REMOVE_MARKETSAMPLE:I

    .line 229
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/launcher2/HomeView;->mAutoAdvanceTimeLeft:J

    .line 230
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mWidgetsToAdvance:Ljava/util/HashMap;

    .line 237
    iput-boolean v4, p0, Lcom/android/launcher2/HomeView;->mAfterSavedInstanceState:Z

    .line 251
    iput-boolean v4, p0, Lcom/android/launcher2/HomeView;->mIsDeletePopup:Z

    .line 270
    iput-boolean v4, p0, Lcom/android/launcher2/HomeView;->mHomeKeyPress:Z

    .line 273
    iput-object v5, p0, Lcom/android/launcher2/HomeView;->mCurrentResizeWidgetItem:Lcom/android/launcher2/BaseItem;

    .line 276
    iput-object v5, p0, Lcom/android/launcher2/HomeView;->mTiltWallpaperScroller:Lcom/sec/dtl/launcher/WallpaperScroller;

    .line 282
    iput-boolean v4, p0, Lcom/android/launcher2/HomeView;->sIsHeadlinesHiddenForEditMode:Z

    .line 1124
    new-instance v1, Lcom/android/launcher2/HomeView$2;

    invoke-direct {v1, p0}, Lcom/android/launcher2/HomeView$2;-><init>(Lcom/android/launcher2/HomeView;)V

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->VoiceSearchButtonClickListener:Landroid/view/View$OnClickListener;

    .line 1141
    new-instance v1, Lcom/android/launcher2/HomeView$3;

    invoke-direct {v1, p0}, Lcom/android/launcher2/HomeView$3;-><init>(Lcom/android/launcher2/HomeView;)V

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->SearchButtonClickListener:Landroid/view/View$OnClickListener;

    .line 1159
    new-instance v1, Lcom/android/launcher2/HomeView$4;

    invoke-direct {v1, p0}, Lcom/android/launcher2/HomeView$4;-><init>(Lcom/android/launcher2/HomeView;)V

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->MyFilesButtonClickListener:Landroid/view/View$OnClickListener;

    .line 1199
    new-instance v1, Lcom/android/launcher2/HomeView$5;

    invoke-direct {v1, p0}, Lcom/android/launcher2/HomeView$5;-><init>(Lcom/android/launcher2/HomeView;)V

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mDarkenLayerTouchListener:Landroid/view/View$OnTouchListener;

    .line 1210
    new-instance v1, Lcom/android/launcher2/HomeView$6;

    invoke-direct {v1, p0}, Lcom/android/launcher2/HomeView$6;-><init>(Lcom/android/launcher2/HomeView;)V

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mAllAppsIconTouchListener:Landroid/view/View$OnTouchListener;

    .line 1506
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/launcher2/HomeView;->mTargetFolderId:J

    .line 1909
    new-instance v1, Lcom/android/launcher2/HomeView$15;

    invoke-direct {v1, p0}, Lcom/android/launcher2/HomeView$15;-><init>(Lcom/android/launcher2/HomeView;)V

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mHandler:Landroid/os/Handler;

    .line 1945
    new-instance v1, Lcom/android/launcher2/HomeView$16;

    invoke-direct {v1, p0}, Lcom/android/launcher2/HomeView$16;-><init>(Lcom/android/launcher2/HomeView;)V

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mMarketSampleTouchListener:Landroid/view/View$OnTouchListener;

    .line 3174
    new-instance v1, Lcom/android/launcher2/HomeView$21;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/android/launcher2/HomeView$21;-><init>(Lcom/android/launcher2/HomeView;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mHelphubObserver:Landroid/database/ContentObserver;

    .line 3182
    new-instance v1, Lcom/android/launcher2/HomeView$22;

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/android/launcher2/HomeView$22;-><init>(Lcom/android/launcher2/HomeView;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mChangeTphoneModeObserver:Landroid/database/ContentObserver;

    .line 4011
    iput-object v5, p0, Lcom/android/launcher2/HomeView;->mHomeEditItem:Landroid/view/MenuItem;

    .line 4355
    iput-boolean v6, p0, Lcom/android/launcher2/HomeView;->isHelpAppPageDeleted:Z

    .line 4356
    iput-boolean v4, p0, Lcom/android/launcher2/HomeView;->isHelpAppPageAdded:Z

    .line 4357
    iput v7, p0, Lcom/android/launcher2/HomeView;->screenIndexBeforeHelpAppPageAddition:I

    .line 4358
    iput v7, p0, Lcom/android/launcher2/HomeView;->screenIndexFestivalHelpAppPageAddition:I

    .line 4666
    iput v4, p0, Lcom/android/launcher2/HomeView;->mCreateFolderColor:I

    .line 4940
    iput-boolean v4, p0, Lcom/android/launcher2/HomeView;->mIsHelpItemAdded:Z

    .line 5272
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/android/launcher2/HomeView;->mPanelBackgroundAlpha:F

    .line 294
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    .line 295
    .local v0, "app":Lcom/android/launcher2/LauncherApplication;
    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->getModel()Lcom/android/launcher2/LauncherModel;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mModel:Lcom/android/launcher2/LauncherModel;

    .line 296
    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->getPkgResCache()Lcom/android/launcher2/PkgResCache;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mPkgResCache:Lcom/android/launcher2/PkgResCache;

    .line 297
    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->getSpanCalculator()Lcom/android/launcher2/WorkspaceSpanCalculator;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    .line 298
    iput-boolean v4, p0, Lcom/android/launcher2/HomeView;->mAfterSavedInstanceState:Z

    .line 300
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isTabletLayout()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/launcher2/HomeView;->removeHotseat:Z

    .line 301
    invoke-direct {p0}, Lcom/android/launcher2/HomeView;->registerContentObservers()V

    .line 306
    return-void
.end method

.method static synthetic access$000(Lcom/android/launcher2/HomeView;)Lcom/android/launcher2/Workspace;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/launcher2/HomeView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;
    .param p1, "x1"    # Z

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/android/launcher2/HomeView;->performOnHomePressed(Z)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/launcher2/HomeView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mMarketSample:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/launcher2/HomeView;)Landroid/view/View$OnTouchListener;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mMarketSampleTouchListener:Landroid/view/View$OnTouchListener;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/launcher2/HomeView;ILcom/android/launcher2/HomePendingWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/android/launcher2/HomePendingWidget;

    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Lcom/android/launcher2/HomeView;->addAppWidgetImpl(ILcom/android/launcher2/HomePendingWidget;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/launcher2/HomeView;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/android/launcher2/HomeView;->onAppWidgetReset()V

    return-void
.end method

.method static synthetic access$1400(Lcom/android/launcher2/HomeView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeSearchBtn:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/launcher2/HomeView;)Lcom/android/launcher2/Hotseat;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/launcher2/HomeView;)Lcom/android/launcher2/QuickLaunch;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/launcher2/HomeView;)Lcom/android/launcher2/QuickLaunch;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/launcher2/HomeView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeTopBar:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/launcher2/HomeView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/launcher2/HomeView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/android/launcher2/HomeView;->removeHotseat:Z

    return v0
.end method

.method static synthetic access$300()Landroid/os/DVFSHelper;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/android/launcher2/HomeView;->cpuBooster:Landroid/os/DVFSHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/launcher2/HomeView;)Landroid/appwidget/AppWidgetManager;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/launcher2/HomeView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/android/launcher2/HomeView;->mRestoring:Z

    return v0
.end method

.method static synthetic access$600(Lcom/android/launcher2/HomeView;)Lcom/android/launcher2/LauncherAppWidgetHost;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/launcher2/HomeView;)Lcom/android/launcher2/WorkspaceSpanCalculator;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/launcher2/HomeView;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWidgetsToAdvance:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/launcher2/HomeView;J)V
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/HomeView;
    .param p1, "x1"    # J

    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Lcom/android/launcher2/HomeView;->sendAdvanceMessage(J)V

    return-void
.end method

.method private addAppWidgetImpl(ILcom/android/launcher2/HomePendingWidget;)V
    .locals 20
    .param p1, "appWidgetId"    # I
    .param p2, "info"    # Lcom/android/launcher2/HomePendingWidget;

    .prologue
    .line 2072
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/HomeView;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v4

    .line 2073
    .local v4, "appWidget":Landroid/appwidget/AppWidgetProviderInfo;
    if-eqz p2, :cond_0

    if-nez v4, :cond_1

    .line 2129
    :cond_0
    :goto_0
    return-void

    .line 2076
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/android/launcher2/HomePendingWidget;->getDefSpans()[I

    move-result-object v7

    .line 2077
    .local v7, "defaultSpan":[I
    invoke-virtual/range {p2 .. p2}, Lcom/android/launcher2/HomePendingWidget;->getResizeSpans()[I

    move-result-object v15

    .line 2079
    .local v15, "resizeSpan":[I
    iget v0, v4, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v7, v15}, Lcom/android/launcher2/HomeView;->calcFinalSpan(Lcom/android/launcher2/HomeItem;I[I[I)V

    .line 2081
    iget-object v0, v4, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    .line 2083
    new-instance v9, Landroid/content/Intent;

    const-string v18, "android.appwidget.action.APPWIDGET_CONFIGURE"

    move-object/from16 v0, v18

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2084
    .local v9, "intent":Landroid/content/Intent;
    iget-object v0, v4, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2085
    const-string v18, "appWidgetId"

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2086
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/launcher2/HomePendingWidget;->mimeType:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/launcher2/HomePendingWidget;->mimeType:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_2

    .line 2087
    const-string v18, "com.android.launcher.extra.widget.CONFIGURATION_DATA_MIME_TYPE"

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/launcher2/HomePendingWidget;->mimeType:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2091
    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/android/launcher2/HomePendingWidget;->mimeType:Ljava/lang/String;

    .line 2092
    .local v13, "mimeType":Ljava/lang/String;
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/launcher2/HomePendingWidget;->configurationData:Landroid/os/Parcelable;

    check-cast v5, Landroid/content/ClipData;

    .line 2093
    .local v5, "clipData":Landroid/content/ClipData;
    invoke-virtual {v5}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v6

    .line 2094
    .local v6, "clipDesc":Landroid/content/ClipDescription;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    invoke-virtual {v6}, Landroid/content/ClipDescription;->getMimeTypeCount()I

    move-result v18

    move/from16 v0, v18

    if-ge v8, v0, :cond_2

    .line 2095
    invoke-virtual {v6, v8}, Landroid/content/ClipDescription;->getMimeType(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 2096
    invoke-virtual {v5, v8}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v11

    .line 2097
    .local v11, "item":Landroid/content/ClipData$Item;
    invoke-virtual {v11}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v16

    .line 2098
    .local v16, "stringData":Ljava/lang/CharSequence;
    invoke-virtual {v11}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v17

    .line 2099
    .local v17, "uriData":Landroid/net/Uri;
    invoke-virtual {v11}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    move-result-object v10

    .line 2100
    .local v10, "intentData":Landroid/content/Intent;
    const-string v12, "com.android.launcher.extra.widget.CONFIGURATION_DATA"

    .line 2102
    .local v12, "key":Ljava/lang/String;
    if-eqz v17, :cond_3

    .line 2103
    const-string v18, "com.android.launcher.extra.widget.CONFIGURATION_DATA"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2114
    .end local v5    # "clipData":Landroid/content/ClipData;
    .end local v6    # "clipDesc":Landroid/content/ClipDescription;
    .end local v8    # "i":I
    .end local v10    # "intentData":Landroid/content/Intent;
    .end local v11    # "item":Landroid/content/ClipData$Item;
    .end local v12    # "key":Ljava/lang/String;
    .end local v13    # "mimeType":Ljava/lang/String;
    .end local v16    # "stringData":Ljava/lang/CharSequence;
    .end local v17    # "uriData":Landroid/net/Uri;
    :cond_2
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/LauncherApplication;

    .line 2115
    .local v3, "app":Lcom/android/launcher2/LauncherApplication;
    const-string v18, "com.sec.android.app.launcher.prefs"

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Lcom/android/launcher2/LauncherApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    .line 2116
    .local v14, "prefs":Landroid/content/SharedPreferences$Editor;
    const-string v18, "tempScreen"

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/launcher2/HomePendingWidget;->mScreen:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2117
    const-string v18, "tempCellX"

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/launcher2/HomePendingWidget;->cellX:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2118
    const-string v18, "tempCellY"

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/launcher2/HomePendingWidget;->cellY:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2119
    const-string v18, "tempSpanX"

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/launcher2/HomePendingWidget;->spanX:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2120
    const-string v18, "tempSpanY"

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/launcher2/HomePendingWidget;->spanY:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2121
    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2123
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/android/launcher2/HomeView;->copyToPendingAddInfo(Lcom/android/launcher2/HomePendingWidget;)V

    .line 2125
    const/16 v18, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v9, v1}, Lcom/android/launcher2/HomeView;->startActivityForResultSafely(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 2104
    .end local v3    # "app":Lcom/android/launcher2/LauncherApplication;
    .end local v14    # "prefs":Landroid/content/SharedPreferences$Editor;
    .restart local v5    # "clipData":Landroid/content/ClipData;
    .restart local v6    # "clipDesc":Landroid/content/ClipDescription;
    .restart local v8    # "i":I
    .restart local v10    # "intentData":Landroid/content/Intent;
    .restart local v11    # "item":Landroid/content/ClipData$Item;
    .restart local v12    # "key":Ljava/lang/String;
    .restart local v13    # "mimeType":Ljava/lang/String;
    .restart local v16    # "stringData":Ljava/lang/CharSequence;
    .restart local v17    # "uriData":Landroid/net/Uri;
    :cond_3
    if-eqz v10, :cond_4

    .line 2105
    const-string v18, "com.android.launcher.extra.widget.CONFIGURATION_DATA"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 2106
    :cond_4
    if-eqz v16, :cond_2

    .line 2107
    const-string v18, "com.android.launcher.extra.widget.CONFIGURATION_DATA"

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 2094
    .end local v10    # "intentData":Landroid/content/Intent;
    .end local v11    # "item":Landroid/content/ClipData$Item;
    .end local v12    # "key":Ljava/lang/String;
    .end local v16    # "stringData":Ljava/lang/CharSequence;
    .end local v17    # "uriData":Landroid/net/Uri;
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 2127
    .end local v5    # "clipData":Landroid/content/ClipData;
    .end local v6    # "clipDesc":Landroid/content/ClipDescription;
    .end local v8    # "i":I
    .end local v9    # "intent":Landroid/content/Intent;
    .end local v13    # "mimeType":Ljava/lang/String;
    :cond_6
    invoke-direct/range {p0 .. p2}, Lcom/android/launcher2/HomeView;->completeAddAppWidget(ILcom/android/launcher2/HomeItem;)V

    goto/16 :goto_0
.end method

.method private addSamsungWidget(Lcom/android/launcher2/HomePendingSamsungWidget;)Z
    .locals 35
    .param p1, "pending"    # Lcom/android/launcher2/HomePendingSamsungWidget;

    .prologue
    .line 2225
    sget-object v29, Lcom/android/launcher2/SamsungWidgetPackageManager;->INSTANCE:Lcom/android/launcher2/SamsungWidgetPackageManager;

    .line 2226
    .local v29, "packageManager":Lcom/android/launcher2/SamsungWidgetPackageManager;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/launcher2/HomePendingSamsungWidget;->mInfo:Lcom/android/launcher2/SamsungWidgetProviderInfo;

    move-object/from16 v26, v0

    .line 2227
    .local v26, "info":Lcom/android/launcher2/SamsungWidgetProviderInfo;
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v4

    move-object/from16 v0, v29

    move-object/from16 v1, v26

    invoke-virtual {v0, v4, v1}, Lcom/android/launcher2/SamsungWidgetPackageManager;->createWidget(Landroid/content/Context;Lcom/android/launcher2/SamsungWidgetProviderInfo;)Lcom/android/launcher2/SamsungWidgetItem;

    move-result-object v25

    .line 2228
    .local v25, "homeItem":Lcom/android/launcher2/SamsungWidgetItem;
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/launcher2/HomePendingSamsungWidget;->container:J

    move-wide/from16 v16, v0

    .line 2229
    .local v16, "container":J
    move-object/from16 v0, p1

    iget v10, v0, Lcom/android/launcher2/HomePendingSamsungWidget;->mScreen:I

    .line 2231
    .local v10, "screen":I
    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v10}, Lcom/android/launcher2/HomeView;->getCellLayout(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v28

    .line 2233
    .local v28, "layout":Lcom/android/launcher2/CellLayout;
    const/16 v27, 0x2

    .line 2234
    .local v27, "land":I
    const/16 v30, 0x1

    .line 2235
    .local v30, "port":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    const/4 v5, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Lcom/android/launcher2/SamsungWidgetProviderInfo;->getWidth(I)I

    move-result v5

    const/4 v6, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Lcom/android/launcher2/SamsungWidgetProviderInfo;->getHeight(I)I

    move-result v6

    const/4 v7, 0x2

    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Lcom/android/launcher2/SamsungWidgetProviderInfo;->getWidth(I)I

    move-result v7

    const/4 v8, 0x2

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Lcom/android/launcher2/SamsungWidgetProviderInfo;->getHeight(I)I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Lcom/android/launcher2/WorkspaceSpanCalculator;->getSpanForSamsungWidget(IIII[I)[I

    move-result-object v22

    .line 2237
    .local v22, "defaultSpan":[I
    const/4 v4, 0x4

    new-array v0, v4, [I

    move-object/from16 v31, v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    aget v5, v22, v5

    aput v5, v31, v4

    const/4 v4, 0x1

    const/4 v5, 0x1

    aget v5, v22, v5

    aput v5, v31, v4

    const/4 v4, 0x2

    const/4 v5, -0x1

    aput v5, v31, v4

    const/4 v4, 0x3

    const/4 v5, -0x1

    aput v5, v31, v4

    .line 2238
    .local v31, "resizeSpan":[I
    const/4 v4, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    move-object/from16 v3, v31

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/android/launcher2/HomeView;->calcFinalSpan(Lcom/android/launcher2/HomeItem;I[I[I)V

    .line 2240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/HomeView;->mTmpAddItemSpans:[I

    move-object/from16 v33, v0

    .line 2241
    .local v33, "spanXY":[I
    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget v5, v0, Lcom/android/launcher2/HomePendingSamsungWidget;->spanX:I

    aput v5, v33, v4

    .line 2242
    const/4 v4, 0x1

    move-object/from16 v0, p1

    iget v5, v0, Lcom/android/launcher2/HomePendingSamsungWidget;->spanY:I

    aput v5, v33, v4

    .line 2247
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher2/HomeView;->mTmpAddItemCellCoordinates:[I

    .line 2248
    .local v9, "cellXY":[I
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/launcher2/HomePendingSamsungWidget;->dropPos:[I

    move-object/from16 v34, v0

    .line 2249
    .local v34, "touchXY":[I
    const/16 v23, 0x0

    .line 2250
    .local v23, "foundCellSpan":Z
    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/launcher2/HomePendingSamsungWidget;->cellX:I

    if-ltz v4, :cond_2

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/launcher2/HomePendingSamsungWidget;->cellY:I

    if-ltz v4, :cond_2

    .line 2251
    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget v5, v0, Lcom/android/launcher2/HomePendingSamsungWidget;->cellX:I

    aput v5, v9, v4

    .line 2252
    const/4 v4, 0x1

    move-object/from16 v0, p1

    iget v5, v0, Lcom/android/launcher2/HomePendingSamsungWidget;->cellY:I

    aput v5, v9, v4

    .line 2253
    const/16 v23, 0x1

    .line 2264
    :cond_0
    :goto_0
    if-nez v23, :cond_4

    .line 2265
    new-instance v12, Lcom/android/launcher2/HomeView$DropPos;

    invoke-direct {v12}, Lcom/android/launcher2/HomeView$DropPos;-><init>()V

    .line 2266
    .local v12, "dropPos":Lcom/android/launcher2/HomeView$DropPos;
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v11

    const/4 v4, 0x0

    aget v13, v33, v4

    const/4 v4, 0x1

    aget v14, v33, v4

    const/4 v15, 0x0

    invoke-static/range {v9 .. v15}, Lcom/android/launcher2/HomeView;->findEmptySpanOnHomeScreen([IILandroid/content/Context;Lcom/android/launcher2/HomeView$DropPos;IIZ)Z

    move-result v23

    .line 2267
    iget v10, v12, Lcom/android/launcher2/HomeView$DropPos;->mScreen:I

    .line 2268
    const/4 v4, 0x0

    iget v5, v12, Lcom/android/launcher2/HomeView$DropPos;->mCellX:I

    aput v5, v9, v4

    .line 2269
    const/4 v4, 0x1

    iget v5, v12, Lcom/android/launcher2/HomeView$DropPos;->mCellY:I

    aput v5, v9, v4

    .line 2270
    iget-boolean v4, v12, Lcom/android/launcher2/HomeView$DropPos;->mFoundInCurrentScreen:Z

    if-nez v4, :cond_1

    .line 2271
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->showOutOfSpaceMessage()V

    .line 2273
    :cond_1
    if-nez v23, :cond_4

    .line 2274
    const/4 v4, 0x0

    .line 2307
    .end local v12    # "dropPos":Lcom/android/launcher2/HomeView$DropPos;
    :goto_1
    return v4

    .line 2254
    :cond_2
    if-eqz v34, :cond_0

    .line 2255
    if-eqz v28, :cond_0

    .line 2258
    const/4 v4, 0x0

    aget v5, v34, v4

    const/4 v4, 0x1

    aget v6, v34, v4

    const/4 v4, 0x0

    aget v7, v33, v4

    const/4 v4, 0x1

    aget v8, v33, v4

    move-object/from16 v4, v28

    invoke-virtual/range {v4 .. v9}, Lcom/android/launcher2/CellLayout;->findNearestVacantArea(IIII[I)[I

    move-result-object v32

    .line 2260
    .local v32, "result":[I
    if-eqz v32, :cond_3

    const/16 v23, 0x1

    :goto_2
    goto :goto_0

    :cond_3
    const/16 v23, 0x0

    goto :goto_2

    .line 2277
    .end local v32    # "result":[I
    :cond_4
    const/4 v4, 0x0

    aget v4, v33, v4

    move-object/from16 v0, v25

    iput v4, v0, Lcom/android/launcher2/SamsungWidgetItem;->spanX:I

    .line 2278
    const/4 v4, 0x1

    aget v4, v33, v4

    move-object/from16 v0, v25

    iput v4, v0, Lcom/android/launcher2/SamsungWidgetItem;->spanY:I

    .line 2280
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v14

    const/4 v4, 0x0

    aget v19, v9, v4

    const/4 v4, 0x1

    aget v20, v9, v4

    const/16 v21, 0x0

    move-object/from16 v15, v25

    move/from16 v18, v10

    invoke-static/range {v14 .. v21}, Lcom/android/launcher2/LauncherModel;->addItemToDatabase(Landroid/content/Context;Lcom/android/launcher2/HomeItem;JIIIZ)V

    .line 2283
    move-object/from16 v0, v25

    iget-object v4, v0, Lcom/android/launcher2/SamsungWidgetItem;->mWidgetView:Lcom/android/launcher2/SamsungWidgetView;

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Lcom/android/launcher2/SamsungWidgetView;->setTag(Ljava/lang/Object;)V

    .line 2285
    move-object/from16 v0, v25

    iget-object v4, v0, Lcom/android/launcher2/SamsungWidgetItem;->mWidgetView:Lcom/android/launcher2/SamsungWidgetView;

    const/high16 v5, 0x1000000

    invoke-virtual {v4, v5}, Lcom/android/launcher2/SamsungWidgetView;->setBackgroundColor(I)V

    .line 2286
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Lcom/android/launcher2/Workspace;->addItem(Lcom/android/launcher2/HomeItem;)V

    .line 2287
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v4

    if-ne v4, v10, :cond_6

    .line 2288
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4, v10}, Lcom/android/launcher2/Workspace;->resumeScreen(I)V

    .line 2292
    :goto_3
    sget-boolean v4, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v4, :cond_5

    .line 2293
    const-string v4, "add_widgets"

    sget-object v5, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2294
    const/4 v4, 0x1

    sput-boolean v4, Lcom/android/launcher2/guide/GuideFragment;->isGuideCompleted:Z

    .line 2295
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f1000ac

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 2298
    new-instance v24, Landroid/os/Handler;

    invoke-direct/range {v24 .. v24}, Landroid/os/Handler;-><init>()V

    .line 2299
    .local v24, "handler":Landroid/os/Handler;
    new-instance v4, Lcom/android/launcher2/HomeView$18;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/android/launcher2/HomeView$18;-><init>(Lcom/android/launcher2/HomeView;)V

    const-wide/16 v6, 0x7d0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2307
    .end local v24    # "handler":Landroid/os/Handler;
    :cond_5
    const/4 v4, 0x1

    goto/16 :goto_1

    .line 2290
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4, v10}, Lcom/android/launcher2/Workspace;->snapToPage(I)V

    goto :goto_3
.end method

.method private addSurfaceWidget(Lcom/android/launcher2/HomePendingSurfaceWidget;)Z
    .locals 31
    .param p1, "info"    # Lcom/android/launcher2/HomePendingSurfaceWidget;

    .prologue
    .line 2337
    new-instance v25, Lcom/android/launcher2/SurfaceWidgetItem;

    invoke-direct/range {v25 .. v25}, Lcom/android/launcher2/SurfaceWidgetItem;-><init>()V

    .line 2338
    .local v25, "homeItem":Lcom/android/launcher2/SurfaceWidgetItem;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/launcher2/HomePendingSurfaceWidget;->mInfo:Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;

    move-object/from16 v28, v0

    .line 2339
    .local v28, "sInfo":Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;
    const-string v5, "SurfaceWidgetFlow"

    const-string v6, "making surface widget on drop"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2340
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    invoke-virtual {v0, v5, v1, v6, v7}, Lcom/android/launcher2/SurfaceWidgetItem;->makeSurfaceWidget(Landroid/content/Context;Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;Lcom/android/launcher2/SurfaceWidgetView;Z)V

    .line 2343
    move-object/from16 v0, v25

    iget-object v5, v0, Lcom/android/launcher2/SurfaceWidgetItem;->mWidgetView:Lcom/android/launcher2/SurfaceWidgetView;

    if-nez v5, :cond_0

    const/4 v5, 0x1

    .line 2442
    :goto_0
    return v5

    .line 2346
    :cond_0
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/launcher2/HomePendingSurfaceWidget;->container:J

    move-wide/from16 v16, v0

    .line 2347
    .local v16, "container":J
    move-object/from16 v0, p1

    iget v10, v0, Lcom/android/launcher2/HomePendingSurfaceWidget;->mScreen:I

    .line 2349
    .local v10, "screen":I
    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v10}, Lcom/android/launcher2/HomeView;->getCellLayout(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v4

    .line 2351
    .local v4, "layout":Lcom/android/launcher2/CellLayout;
    invoke-virtual/range {v25 .. v25}, Lcom/android/launcher2/SurfaceWidgetItem;->getDefSpans()[I

    move-result-object v22

    .line 2352
    .local v22, "defaultSpan":[I
    invoke-virtual/range {v25 .. v25}, Lcom/android/launcher2/SurfaceWidgetItem;->getResizeSpans()[I

    move-result-object v26

    .line 2353
    .local v26, "resizeSpan":[I
    move-object/from16 v0, v28

    iget v5, v0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->resizeMode:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/android/launcher2/HomeView;->calcFinalSpan(Lcom/android/launcher2/HomeItem;I[I[I)V

    .line 2355
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/HomeView;->mTmpAddItemSpans:[I

    move-object/from16 v29, v0

    .line 2356
    .local v29, "spanXY":[I
    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget v6, v0, Lcom/android/launcher2/HomePendingSurfaceWidget;->spanX:I

    aput v6, v29, v5

    .line 2357
    const/4 v5, 0x1

    move-object/from16 v0, p1

    iget v6, v0, Lcom/android/launcher2/HomePendingSurfaceWidget;->spanY:I

    aput v6, v29, v5

    .line 2359
    sget-boolean v5, Lcom/android/launcher2/HomeView;->DEBUGGABLE:Z

    if-eqz v5, :cond_1

    const-string v5, "Launcher.HomeView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "adding surface width cells wide = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget v7, v29, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " cells tall = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    aget v7, v29, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2364
    :cond_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher2/HomeView;->mTmpAddItemCellCoordinates:[I

    .line 2365
    .local v9, "cellXY":[I
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/launcher2/HomePendingSurfaceWidget;->dropPos:[I

    move-object/from16 v30, v0

    .line 2366
    .local v30, "touchXY":[I
    const/16 v23, 0x0

    .line 2367
    .local v23, "foundCellSpan":Z
    move-object/from16 v0, p1

    iget v5, v0, Lcom/android/launcher2/HomePendingSurfaceWidget;->cellX:I

    if-ltz v5, :cond_4

    move-object/from16 v0, p1

    iget v5, v0, Lcom/android/launcher2/HomePendingSurfaceWidget;->cellY:I

    if-ltz v5, :cond_4

    .line 2368
    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget v6, v0, Lcom/android/launcher2/HomePendingSurfaceWidget;->cellX:I

    aput v6, v9, v5

    .line 2369
    const/4 v5, 0x1

    move-object/from16 v0, p1

    iget v6, v0, Lcom/android/launcher2/HomePendingSurfaceWidget;->cellY:I

    aput v6, v9, v5

    .line 2370
    const/16 v23, 0x1

    .line 2381
    :cond_2
    :goto_1
    if-nez v23, :cond_6

    .line 2382
    new-instance v12, Lcom/android/launcher2/HomeView$DropPos;

    invoke-direct {v12}, Lcom/android/launcher2/HomeView$DropPos;-><init>()V

    .line 2383
    .local v12, "dropPos":Lcom/android/launcher2/HomeView$DropPos;
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v11

    const/4 v5, 0x0

    aget v13, v29, v5

    const/4 v5, 0x1

    aget v14, v29, v5

    const/4 v15, 0x0

    invoke-static/range {v9 .. v15}, Lcom/android/launcher2/HomeView;->findEmptySpanOnHomeScreen([IILandroid/content/Context;Lcom/android/launcher2/HomeView$DropPos;IIZ)Z

    move-result v23

    .line 2384
    iget v10, v12, Lcom/android/launcher2/HomeView$DropPos;->mScreen:I

    .line 2385
    const/4 v5, 0x0

    iget v6, v12, Lcom/android/launcher2/HomeView$DropPos;->mCellX:I

    aput v6, v9, v5

    .line 2386
    const/4 v5, 0x1

    iget v6, v12, Lcom/android/launcher2/HomeView$DropPos;->mCellY:I

    aput v6, v9, v5

    .line 2387
    iget-boolean v5, v12, Lcom/android/launcher2/HomeView$DropPos;->mFoundInCurrentScreen:Z

    if-nez v5, :cond_3

    .line 2388
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->showOutOfSpaceMessage()V

    .line 2390
    :cond_3
    if-nez v23, :cond_6

    .line 2391
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 2371
    .end local v12    # "dropPos":Lcom/android/launcher2/HomeView$DropPos;
    :cond_4
    if-eqz v30, :cond_2

    .line 2372
    if-eqz v4, :cond_2

    .line 2375
    const/4 v5, 0x0

    aget v5, v30, v5

    const/4 v6, 0x1

    aget v6, v30, v6

    const/4 v7, 0x0

    aget v7, v29, v7

    const/4 v8, 0x1

    aget v8, v29, v8

    invoke-virtual/range {v4 .. v9}, Lcom/android/launcher2/CellLayout;->findNearestVacantArea(IIII[I)[I

    move-result-object v27

    .line 2377
    .local v27, "result":[I
    if-eqz v27, :cond_5

    const/16 v23, 0x1

    :goto_2
    goto :goto_1

    :cond_5
    const/16 v23, 0x0

    goto :goto_2

    .line 2394
    .end local v27    # "result":[I
    :cond_6
    const/4 v5, 0x0

    aget v5, v29, v5

    move-object/from16 v0, v25

    iput v5, v0, Lcom/android/launcher2/SurfaceWidgetItem;->spanX:I

    .line 2395
    const/4 v5, 0x1

    aget v5, v29, v5

    move-object/from16 v0, v25

    iput v5, v0, Lcom/android/launcher2/SurfaceWidgetItem;->spanY:I

    .line 2397
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v14

    const/4 v5, 0x0

    aget v19, v9, v5

    const/4 v5, 0x1

    aget v20, v9, v5

    const/16 v21, 0x0

    move-object/from16 v15, v25

    move/from16 v18, v10

    invoke-static/range {v14 .. v21}, Lcom/android/launcher2/LauncherModel;->addItemToDatabase(Landroid/content/Context;Lcom/android/launcher2/HomeItem;JIIIZ)V

    .line 2401
    move-object/from16 v0, v25

    iget-object v5, v0, Lcom/android/launcher2/SurfaceWidgetItem;->mWidgetView:Lcom/android/launcher2/SurfaceWidgetView;

    new-instance v6, Lcom/android/launcher2/HomeView$19;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v6, v0, v1, v4}, Lcom/android/launcher2/HomeView$19;-><init>(Lcom/android/launcher2/HomeView;Lcom/android/launcher2/SurfaceWidgetItem;Lcom/android/launcher2/CellLayout;)V

    invoke-virtual {v5, v6}, Lcom/android/launcher2/SurfaceWidgetView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2421
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/android/launcher2/Workspace;->addItem(Lcom/android/launcher2/HomeItem;)V

    .line 2422
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v5

    if-ne v5, v10, :cond_8

    .line 2423
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5, v10}, Lcom/android/launcher2/Workspace;->resumeScreen(I)V

    .line 2427
    :goto_3
    sget-boolean v5, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v5, :cond_7

    .line 2428
    const-string v5, "add_widgets"

    sget-object v6, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2429
    const/4 v5, 0x1

    sput-boolean v5, Lcom/android/launcher2/guide/GuideFragment;->isGuideCompleted:Z

    .line 2430
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f1000ac

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 2433
    new-instance v24, Landroid/os/Handler;

    invoke-direct/range {v24 .. v24}, Landroid/os/Handler;-><init>()V

    .line 2434
    .local v24, "handler":Landroid/os/Handler;
    new-instance v5, Lcom/android/launcher2/HomeView$20;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/android/launcher2/HomeView$20;-><init>(Lcom/android/launcher2/HomeView;)V

    const-wide/16 v6, 0x7d0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2442
    .end local v24    # "handler":Landroid/os/Handler;
    :cond_7
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 2425
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5, v10}, Lcom/android/launcher2/Workspace;->snapToPage(I)V

    goto :goto_3
.end method

.method private calcFinalSpan(Lcom/android/launcher2/HomeItem;I[I[I)V
    .locals 6
    .param p1, "info"    # Lcom/android/launcher2/HomeItem;
    .param p2, "resizeFlags"    # I
    .param p3, "defaultSpan"    # [I
    .param p4, "resizeSpan"    # [I

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 2051
    iget v0, p1, Lcom/android/launcher2/HomeItem;->spanX:I

    if-ne v2, v0, :cond_0

    .line 2052
    aget v0, p3, v1

    iput v0, p1, Lcom/android/launcher2/HomeItem;->spanX:I

    .line 2054
    :cond_0
    iget v0, p1, Lcom/android/launcher2/HomeItem;->spanY:I

    if-ne v2, v0, :cond_1

    .line 2055
    aget v0, p3, v3

    iput v0, p1, Lcom/android/launcher2/HomeItem;->spanY:I

    .line 2057
    :cond_1
    and-int/lit8 v0, p2, 0x1

    if-ne v0, v3, :cond_2

    .line 2058
    iget v0, p1, Lcom/android/launcher2/HomeItem;->spanX:I

    aget v1, p4, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p1, Lcom/android/launcher2/HomeItem;->spanX:I

    .line 2059
    aget v0, p4, v4

    if-eq v2, v0, :cond_2

    .line 2060
    iget v0, p1, Lcom/android/launcher2/HomeItem;->spanX:I

    aget v1, p4, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p1, Lcom/android/launcher2/HomeItem;->spanX:I

    .line 2063
    :cond_2
    and-int/lit8 v0, p2, 0x2

    if-ne v0, v4, :cond_3

    .line 2064
    iget v0, p1, Lcom/android/launcher2/HomeItem;->spanY:I

    aget v1, p4, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p1, Lcom/android/launcher2/HomeItem;->spanY:I

    .line 2065
    aget v0, p4, v5

    if-eq v2, v0, :cond_3

    .line 2066
    iget v0, p1, Lcom/android/launcher2/HomeItem;->spanY:I

    aget v1, p4, v5

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p1, Lcom/android/launcher2/HomeItem;->spanY:I

    .line 2069
    :cond_3
    return-void
.end method

.method private closeQuickViewWorkspaceStartAnimation()V
    .locals 10

    .prologue
    const/4 v7, 0x1

    const-wide/16 v8, 0x168

    const/4 v6, 0x2

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 3831
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v4}, Lcom/android/launcher2/Workspace;->setVisibility(I)V

    .line 3832
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getPageIndicatorManager()Lcom/android/launcher2/PageIndicatorManager;

    move-result-object v2

    sget-object v3, Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;->ALL_PAGE:Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;

    invoke-virtual {v2, v3}, Lcom/android/launcher2/PageIndicatorManager;->setDisplayItem(Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;)V

    .line 3834
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomePhoneBtn:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 3835
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomePhoneBtn:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3837
    :cond_0
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeAppsBtn:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 3838
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeAppsBtn:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3840
    :cond_1
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v2}, Lcom/android/launcher2/QuickViewWorkspace;->getCurrentPage()I

    move-result v1

    .line 3848
    .local v1, "newWorkspace":I
    if-ltz v1, :cond_2

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v2

    if-eq v2, v1, :cond_2

    .line 3850
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v1}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    .line 3853
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->getAnimationLayer()Lcom/android/launcher2/AnimationLayer;

    move-result-object v0

    .line 3854
    .local v0, "l":Lcom/android/launcher2/AnimationLayer;
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v4}, Lcom/android/launcher2/Workspace;->setVisibility(I)V

    .line 3855
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-eqz v2, :cond_3

    .line 3856
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v2}, Lcom/android/launcher2/Hotseat;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 3857
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v2, v4}, Lcom/android/launcher2/Hotseat;->setVisibility(I)V

    .line 3858
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    sget-object v3, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6, v3}, Lcom/android/launcher2/Hotseat;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3859
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v2}, Lcom/android/launcher2/Hotseat;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-object v3, v0, Lcom/android/launcher2/AnimationLayer;->mBlockEventsListener:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 3868
    :cond_3
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    if-eqz v2, :cond_4

    .line 3869
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v2}, Lcom/android/launcher2/QuickLaunch;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 3870
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v2, v4}, Lcom/android/launcher2/QuickLaunch;->setVisibility(I)V

    .line 3871
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    sget-object v3, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6, v3}, Lcom/android/launcher2/QuickLaunch;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3872
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v2}, Lcom/android/launcher2/QuickLaunch;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-object v3, v0, Lcom/android/launcher2/AnimationLayer;->mBlockEventsListener:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 3875
    :cond_4
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v2}, Lcom/android/launcher2/QuickLaunch;->isLandscape()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 3876
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v2}, Lcom/android/launcher2/QuickLaunch;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 3877
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v2, v4}, Lcom/android/launcher2/QuickLaunch;->setVisibility(I)V

    .line 3878
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    sget-object v3, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6, v3}, Lcom/android/launcher2/QuickLaunch;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3879
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v2}, Lcom/android/launcher2/QuickLaunch;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-object v3, v0, Lcom/android/launcher2/AnimationLayer;->mBlockEventsListener:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 3885
    :cond_5
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeSearchBtn:Landroid/view/View;

    if-eqz v2, :cond_6

    .line 3886
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeSearchBtn:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setEnabled(Z)V

    .line 3888
    :cond_6
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeTopBar:Landroid/view/View;

    if-eqz v2, :cond_7

    .line 3889
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeTopBar:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 3890
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeTopBar:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3891
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeTopBar:Landroid/view/View;

    sget-object v3, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3892
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeTopBar:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-object v3, v0, Lcom/android/launcher2/AnimationLayer;->mBlockEventsListener:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 3896
    :cond_7
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    if-eqz v2, :cond_8

    iget-boolean v2, p0, Lcom/android/launcher2/HomeView;->removeHotseat:Z

    if-eqz v2, :cond_8

    .line 3897
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 3898
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3899
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    sget-object v3, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3900
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-object v3, v0, Lcom/android/launcher2/AnimationLayer;->mBlockEventsListener:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 3903
    :cond_8
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 3905
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v7}, Lcom/android/launcher2/Workspace;->showPageIndicator(Z)V

    .line 3906
    return-void
.end method

.method private completeAdd(Lcom/android/launcher2/HomeView$PendingAddArguments;)Z
    .locals 12
    .param p1, "args"    # Lcom/android/launcher2/HomeView$PendingAddArguments;

    .prologue
    const/4 v7, 0x1

    const/4 v8, -0x1

    .line 430
    const/4 v11, 0x0

    .line 431
    .local v11, "result":Z
    iget v0, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->requestCode:I

    packed-switch v0, :pswitch_data_0

    .line 460
    :goto_0
    :pswitch_0
    invoke-direct {p0}, Lcom/android/launcher2/HomeView;->resetAddInfo()V

    .line 461
    return v11

    .line 433
    :pswitch_1
    iget-object v0, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v0, v7}, Lcom/android/launcher2/HomeView;->startActivityForResultSafely(Landroid/content/Intent;I)V

    goto :goto_0

    .line 436
    :pswitch_2
    iget-object v1, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->intent:Landroid/content/Intent;

    iget-wide v2, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->container:J

    iget v4, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->screen:I

    iget v5, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->cellX:I

    iget v6, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->cellY:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/HomeView;->completeAddShortcut(Landroid/content/Intent;JIII)V

    .line 438
    const/4 v11, 0x1

    .line 439
    goto :goto_0

    .line 441
    :pswitch_3
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mModel:Lcom/android/launcher2/LauncherModel;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->intent:Landroid/content/Intent;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Lcom/android/launcher2/LauncherModel;->infoFromShortcutIntent(Landroid/content/Context;Landroid/content/Intent;Landroid/graphics/Bitmap;)Lcom/android/launcher2/HomeShortcutItem;

    move-result-object v1

    .line 442
    .local v1, "item":Lcom/android/launcher2/HomeShortcutItem;
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-wide/16 v2, -0x1

    const-wide/16 v4, -0x64

    const/4 v6, 0x0

    move v9, v8

    invoke-static/range {v0 .. v9}, Lcom/android/launcher2/CreateFolderDialog;->createAndShow(Landroid/app/FragmentManager;Lcom/android/launcher2/BaseItem;JJZZII)V

    goto :goto_0

    .line 446
    .end local v1    # "item":Lcom/android/launcher2/HomeShortcutItem;
    :pswitch_4
    iget-object v0, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/HomeView;->addAppWidgetFromPick(Landroid/content/Intent;)V

    goto :goto_0

    .line 449
    :pswitch_5
    iget-object v0, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->intent:Landroid/content/Intent;

    const-string v2, "appWidgetId"

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    .line 450
    .local v10, "appWidgetId":I
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    invoke-static {v0, p1}, Lcom/android/launcher2/HomeView;->copyPendingAddToHomeItem(Lcom/android/launcher2/HomeItem;Lcom/android/launcher2/HomeView$PendingAddArguments;)V

    .line 451
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    invoke-direct {p0, v10, v0}, Lcom/android/launcher2/HomeView;->completeAddAppWidget(ILcom/android/launcher2/HomeItem;)V

    .line 452
    const/4 v11, 0x1

    .line 453
    goto :goto_0

    .line 431
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private completeAddAppWidget(ILcom/android/launcher2/HomeItem;)V
    .locals 36
    .param p1, "appWidgetId"    # I
    .param p2, "info"    # Lcom/android/launcher2/HomeItem;

    .prologue
    .line 1716
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v26

    .line 1718
    .local v26, "appWidgetInfo":Landroid/appwidget/AppWidgetProviderInfo;
    if-nez v26, :cond_1

    .line 1722
    sget-boolean v4, Lcom/android/launcher2/HomeView;->DEBUGGABLE:Z

    if-eqz v4, :cond_0

    const-string v4, "Launcher.HomeView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "App widget info is null. AppWidgetID = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1869
    :cond_0
    :goto_0
    return-void

    .line 1725
    :cond_1
    move-object/from16 v0, p2

    iget-wide v6, v0, Lcom/android/launcher2/HomeItem;->container:J

    .line 1726
    .local v6, "container":J
    move-object/from16 v0, p2

    iget v8, v0, Lcom/android/launcher2/HomeItem;->mScreen:I

    .line 1729
    .local v8, "screen":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7, v8}, Lcom/android/launcher2/HomeView;->getCellLayout(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v5

    .local v5, "layoutForScreen":Lcom/android/launcher2/CellLayout;
    move-object/from16 v4, p0

    move-object/from16 v9, p2

    .line 1730
    invoke-direct/range {v4 .. v9}, Lcom/android/launcher2/HomeView;->reAddCreatedPageForDragIfNeeded(Lcom/android/launcher2/CellLayout;JILcom/android/launcher2/HomeItem;)Lcom/android/launcher2/CellLayout;

    move-result-object v9

    .line 1732
    .local v9, "layout":Lcom/android/launcher2/CellLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/HomeView;->mTmpAddItemSpans:[I

    move-object/from16 v32, v0

    .line 1733
    .local v32, "spanXY":[I
    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget v10, v0, Lcom/android/launcher2/HomeItem;->spanX:I

    aput v10, v32, v4

    .line 1734
    const/4 v4, 0x1

    move-object/from16 v0, p2

    iget v10, v0, Lcom/android/launcher2/HomeItem;->spanY:I

    aput v10, v32, v4

    .line 1738
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher2/HomeView;->mTmpAddItemCellCoordinates:[I

    .line 1739
    .local v14, "cellXY":[I
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/launcher2/HomeItem;->dropPos:[I

    move-object/from16 v34, v0

    .line 1740
    .local v34, "touchXY":[I
    const/16 v27, 0x0

    .line 1741
    .local v27, "foundCellSpan":Z
    if-eqz v9, :cond_6

    .line 1742
    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/launcher2/HomeItem;->cellX:I

    if-ltz v4, :cond_4

    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/launcher2/HomeItem;->cellY:I

    if-ltz v4, :cond_4

    .line 1743
    const/4 v4, 0x0

    move-object/from16 v0, p2

    iget v10, v0, Lcom/android/launcher2/HomeItem;->cellX:I

    aput v10, v14, v4

    .line 1744
    const/4 v4, 0x1

    move-object/from16 v0, p2

    iget v10, v0, Lcom/android/launcher2/HomeItem;->cellY:I

    aput v10, v14, v4

    .line 1745
    const/16 v27, 0x1

    .line 1756
    :cond_2
    :goto_1
    if-nez v27, :cond_7

    .line 1757
    new-instance v17, Lcom/android/launcher2/HomeView$DropPos;

    invoke-direct/range {v17 .. v17}, Lcom/android/launcher2/HomeView$DropPos;-><init>()V

    .line 1758
    .local v17, "dropPos":Lcom/android/launcher2/HomeView$DropPos;
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v16

    const/4 v4, 0x0

    aget v18, v32, v4

    const/4 v4, 0x1

    aget v19, v32, v4

    const/16 v20, 0x0

    move v15, v8

    invoke-static/range {v14 .. v20}, Lcom/android/launcher2/HomeView;->findEmptySpanOnHomeScreen([IILandroid/content/Context;Lcom/android/launcher2/HomeView$DropPos;IIZ)Z

    move-result v27

    .line 1759
    move-object/from16 v0, v17

    iget v8, v0, Lcom/android/launcher2/HomeView$DropPos;->mScreen:I

    .line 1760
    const/4 v4, 0x0

    move-object/from16 v0, v17

    iget v10, v0, Lcom/android/launcher2/HomeView$DropPos;->mCellX:I

    aput v10, v14, v4

    .line 1761
    const/4 v4, 0x1

    move-object/from16 v0, v17

    iget v10, v0, Lcom/android/launcher2/HomeView$DropPos;->mCellY:I

    aput v10, v14, v4

    .line 1762
    move-object/from16 v0, v17

    iget-boolean v4, v0, Lcom/android/launcher2/HomeView$DropPos;->mFoundInCurrentScreen:Z

    if-nez v4, :cond_3

    .line 1763
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->showOutOfSpaceMessage()V

    .line 1765
    :cond_3
    if-nez v27, :cond_7

    .line 1766
    const/4 v4, -0x1

    move/from16 v0, p1

    if-eq v0, v4, :cond_0

    .line 1769
    new-instance v4, Lcom/android/launcher2/HomeView$11;

    const-string v10, "deleteAppWidgetId"

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v4, v0, v10, v1}, Lcom/android/launcher2/HomeView$11;-><init>(Lcom/android/launcher2/HomeView;Ljava/lang/String;I)V

    invoke-virtual {v4}, Lcom/android/launcher2/HomeView$11;->start()V

    goto/16 :goto_0

    .line 1746
    .end local v17    # "dropPos":Lcom/android/launcher2/HomeView$DropPos;
    :cond_4
    if-eqz v34, :cond_2

    .line 1749
    const/4 v4, 0x0

    aget v10, v34, v4

    const/4 v4, 0x1

    aget v11, v34, v4

    const/4 v4, 0x0

    aget v12, v32, v4

    const/4 v4, 0x1

    aget v13, v32, v4

    invoke-virtual/range {v9 .. v14}, Lcom/android/launcher2/CellLayout;->findNearestVacantArea(IIII[I)[I

    move-result-object v31

    .line 1751
    .local v31, "result":[I
    if-eqz v31, :cond_5

    const/16 v27, 0x1

    .line 1752
    :goto_2
    goto :goto_1

    .line 1751
    :cond_5
    const/16 v27, 0x0

    goto :goto_2

    .line 1754
    .end local v31    # "result":[I
    :cond_6
    const/4 v8, -0x1

    goto :goto_1

    .line 1782
    :cond_7
    new-instance v19, Lcom/android/launcher2/HomeWidgetItem;

    const/4 v4, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    move/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lcom/android/launcher2/HomeWidgetItem;-><init>(Landroid/appwidget/AppWidgetProviderInfo;IZ)V

    .line 1783
    .local v19, "launcherInfo":Lcom/android/launcher2/HomeWidgetItem;
    const/4 v4, 0x0

    aget v4, v32, v4

    move-object/from16 v0, v19

    iput v4, v0, Lcom/android/launcher2/HomeWidgetItem;->spanX:I

    .line 1784
    const/4 v4, 0x1

    aget v4, v32, v4

    move-object/from16 v0, v19

    iput v4, v0, Lcom/android/launcher2/HomeWidgetItem;->spanY:I

    .line 1788
    move-object/from16 v0, p2

    instance-of v4, v0, Lcom/android/launcher2/HomePendingWidget;

    if-eqz v4, :cond_b

    move-object/from16 v33, p2

    .line 1789
    check-cast v33, Lcom/android/launcher2/HomePendingWidget;

    .line 1790
    .local v33, "temp":Lcom/android/launcher2/HomePendingWidget;
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/android/launcher2/HomeWidgetItem;->resizeSpans:[I

    invoke-virtual/range {v33 .. v33}, Lcom/android/launcher2/HomePendingWidget;->getResizeSpans()[I

    move-result-object v10

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v10}, Lcom/android/launcher2/HomeView;->copySpans([I[I)V

    .line 1798
    .end local v33    # "temp":Lcom/android/launcher2/HomePendingWidget;
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v18

    const/4 v4, 0x0

    aget v23, v14, v4

    const/4 v4, 0x1

    aget v24, v14, v4

    const/16 v25, 0x0

    move-wide/from16 v20, v6

    move/from16 v22, v8

    invoke-static/range {v18 .. v25}, Lcom/android/launcher2/LauncherModel;->addItemToDatabase(Landroid/content/Context;Lcom/android/launcher2/HomeItem;JIIIZ)V

    .line 1801
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/launcher2/HomeView;->mRestoring:Z

    if-nez v4, :cond_9

    .line 1803
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v10

    move/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v4, v10, v0, v1}, Lcom/android/launcher2/LauncherAppWidgetHost;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v4

    move-object/from16 v0, v19

    iput-object v4, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    .line 1805
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    move/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v4, v0, v1}, Landroid/appwidget/AppWidgetHostView;->setAppWidget(ILandroid/appwidget/AppWidgetProviderInfo;)V

    .line 1806
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/appwidget/AppWidgetHostView;->setTag(Ljava/lang/Object;)V

    .line 1807
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    move-object/from16 v0, v26

    iget-object v10, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v4, v10}, Lcom/android/launcher2/WorkspaceSpanCalculator;->getPaddingForWidget(Landroid/content/ComponentName;)Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;

    move-result-object v29

    .line 1808
    .local v29, "padding":Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v0, v29

    iget v10, v0, Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;->left:I

    move-object/from16 v0, v29

    iget v11, v0, Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;->top:I

    move-object/from16 v0, v29

    iget v12, v0, Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;->right:I

    move-object/from16 v0, v29

    iget v13, v0, Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;->bottom:I

    invoke-virtual {v4, v10, v11, v12, v13}, Landroid/appwidget/AppWidgetHostView;->setPadding(IIII)V

    .line 1811
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    new-instance v10, Lcom/android/launcher2/HomeView$12;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1, v9}, Lcom/android/launcher2/HomeView$12;-><init>(Lcom/android/launcher2/HomeView;Lcom/android/launcher2/HomeWidgetItem;Lcom/android/launcher2/CellLayout;)V

    invoke-virtual {v4, v10}, Landroid/appwidget/AppWidgetHostView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1830
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher2/HomeView;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    move-object/from16 v0, v19

    iget-object v11, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/Launcher;

    move-object/from16 v0, v19

    iget v12, v0, Lcom/android/launcher2/HomeWidgetItem;->spanX:I

    move-object/from16 v0, v19

    iget v13, v0, Lcom/android/launcher2/HomeWidgetItem;->spanY:I

    invoke-virtual {v10, v11, v4, v12, v13}, Lcom/android/launcher2/WorkspaceSpanCalculator;->updateWidgetSizeRanges(Landroid/appwidget/AppWidgetHostView;Lcom/android/launcher2/Launcher;II)V

    .line 1832
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4, v8}, Lcom/android/launcher2/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Lcom/android/launcher2/CellLayout;

    .line 1833
    .local v30, "page":Lcom/android/launcher2/CellLayout;
    move-object/from16 v0, v30

    iget-object v4, v0, Lcom/android/launcher2/CellLayout;->mChildren:Lcom/android/launcher2/CellLayoutChildren;

    iget-object v4, v4, Lcom/android/launcher2/CellLayoutChildren;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_d

    const/16 v35, 0x1

    .line 1835
    .local v35, "wasPageEmpty":Z
    :goto_4
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    const/high16 v10, 0x1000000

    invoke-virtual {v4, v10}, Landroid/appwidget/AppWidgetHostView;->setBackgroundColor(I)V

    .line 1836
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/android/launcher2/Workspace;->addItem(Lcom/android/launcher2/HomeItem;)V

    .line 1839
    if-eqz v35, :cond_8

    move-object/from16 v0, v30

    iget-object v4, v0, Lcom/android/launcher2/CellLayout;->mChildren:Lcom/android/launcher2/CellLayoutChildren;

    iget-object v4, v4, Lcom/android/launcher2/CellLayoutChildren;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_8

    .line 1840
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->turnOffEmptyPageMsg()V

    .line 1842
    :cond_8
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v4, v1}, Lcom/android/launcher2/HomeView;->addWidgetToAutoAdvanceIfNeeded(Landroid/view/View;Landroid/appwidget/AppWidgetProviderInfo;)V

    .line 1846
    .end local v29    # "padding":Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;
    .end local v30    # "page":Lcom/android/launcher2/CellLayout;
    .end local v35    # "wasPageEmpty":Z
    :cond_9
    sget-boolean v4, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v4, :cond_a

    .line 1847
    sget-object v4, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v10, "add_widgets"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1848
    const/4 v4, 0x1

    sput-boolean v4, Lcom/android/launcher2/guide/GuideFragment;->isGuideCompleted:Z

    .line 1849
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v4

    const v10, 0x7f1000ac

    const/4 v11, 0x1

    invoke-static {v4, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1850
    new-instance v28, Landroid/os/Handler;

    invoke-direct/range {v28 .. v28}, Landroid/os/Handler;-><init>()V

    .line 1851
    .local v28, "handler":Landroid/os/Handler;
    new-instance v4, Lcom/android/launcher2/HomeView$13;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/android/launcher2/HomeView$13;-><init>(Lcom/android/launcher2/HomeView;)V

    const-wide/16 v10, 0x7d0

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1863
    .end local v28    # "handler":Landroid/os/Handler;
    :cond_a
    :goto_5
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v10, Lcom/android/launcher2/HomeView$14;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/android/launcher2/HomeView$14;-><init>(Lcom/android/launcher2/HomeView;)V

    const-wide/16 v12, 0x1f4

    invoke-virtual {v4, v10, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1791
    :cond_b
    move-object/from16 v0, p2

    instance-of v4, v0, Lcom/android/launcher2/HomeWidgetItem;

    if-eqz v4, :cond_c

    move-object/from16 v33, p2

    .line 1792
    check-cast v33, Lcom/android/launcher2/HomeWidgetItem;

    .line 1793
    .local v33, "temp":Lcom/android/launcher2/HomeWidgetItem;
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/android/launcher2/HomeWidgetItem;->resizeSpans:[I

    invoke-virtual/range {v33 .. v33}, Lcom/android/launcher2/HomeWidgetItem;->getResizeSpans()[I

    move-result-object v10

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v10}, Lcom/android/launcher2/HomeView;->copySpans([I[I)V

    goto/16 :goto_3

    .line 1795
    .end local v33    # "temp":Lcom/android/launcher2/HomeWidgetItem;
    :cond_c
    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/android/launcher2/HomeWidgetItem;->calcResizeSpan(Landroid/appwidget/AppWidgetProviderInfo;)V

    goto/16 :goto_3

    .line 1833
    .restart local v29    # "padding":Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;
    .restart local v30    # "page":Lcom/android/launcher2/CellLayout;
    :cond_d
    const/16 v35, 0x0

    goto/16 :goto_4

    .line 1858
    .end local v29    # "padding":Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;
    .end local v30    # "page":Lcom/android/launcher2/CellLayout;
    :cond_e
    sget-object v4, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v10, "resize_widgets"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1859
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->exitWidgetResizeMode()V

    goto :goto_5
.end method

.method private completeAddShortcut(Landroid/content/Intent;JIII)V
    .locals 8
    .param p1, "data"    # Landroid/content/Intent;
    .param p2, "container"    # J
    .param p4, "screen"    # I
    .param p5, "cellX"    # I
    .param p6, "cellY"    # I

    .prologue
    .line 1693
    const-string v0, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1698
    :goto_0
    return-void

    .line 1695
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mModel:Lcom/android/launcher2/LauncherModel;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Lcom/android/launcher2/LauncherModel;->infoFromShortcutIntent(Landroid/content/Context;Landroid/content/Intent;Landroid/graphics/Bitmap;)Lcom/android/launcher2/HomeShortcutItem;

    move-result-object v1

    .local v1, "info":Lcom/android/launcher2/HomeShortcutItem;
    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    move v6, p6

    .line 1697
    invoke-virtual/range {v0 .. v6}, Lcom/android/launcher2/HomeView;->completeAddShortcut(Lcom/android/launcher2/HomeShortcutItem;JIII)V

    goto :goto_0
.end method

.method private static copyHomeItemToPendingAdd(Lcom/android/launcher2/HomeView$PendingAddArguments;Lcom/android/launcher2/HomeItem;)V
    .locals 2
    .param p0, "to"    # Lcom/android/launcher2/HomeView$PendingAddArguments;
    .param p1, "from"    # Lcom/android/launcher2/HomeItem;

    .prologue
    .line 415
    iget-wide v0, p1, Lcom/android/launcher2/HomeItem;->container:J

    iput-wide v0, p0, Lcom/android/launcher2/HomeView$PendingAddArguments;->container:J

    .line 416
    iget v0, p1, Lcom/android/launcher2/HomeItem;->mScreen:I

    iput v0, p0, Lcom/android/launcher2/HomeView$PendingAddArguments;->screen:I

    .line 417
    iget v0, p1, Lcom/android/launcher2/HomeItem;->cellX:I

    iput v0, p0, Lcom/android/launcher2/HomeView$PendingAddArguments;->cellX:I

    .line 418
    iget v0, p1, Lcom/android/launcher2/HomeItem;->cellY:I

    iput v0, p0, Lcom/android/launcher2/HomeView$PendingAddArguments;->cellY:I

    .line 419
    iget v0, p1, Lcom/android/launcher2/HomeItem;->spanX:I

    iput v0, p0, Lcom/android/launcher2/HomeView$PendingAddArguments;->spanX:I

    .line 420
    iget v0, p1, Lcom/android/launcher2/HomeItem;->spanY:I

    iput v0, p0, Lcom/android/launcher2/HomeView$PendingAddArguments;->spanY:I

    .line 421
    iget-object v0, p1, Lcom/android/launcher2/HomeItem;->dropPos:[I

    iput-object v0, p0, Lcom/android/launcher2/HomeView$PendingAddArguments;->dropPos:[I

    .line 422
    iget-boolean v0, p1, Lcom/android/launcher2/HomeItem;->mSecretItem:Z

    iput-boolean v0, p0, Lcom/android/launcher2/HomeView$PendingAddArguments;->isSecretItem:Z

    .line 423
    return-void
.end method

.method private static copyPendingAddToHomeItem(Lcom/android/launcher2/HomeItem;Lcom/android/launcher2/HomeView$PendingAddArguments;)V
    .locals 2
    .param p0, "to"    # Lcom/android/launcher2/HomeItem;
    .param p1, "from"    # Lcom/android/launcher2/HomeView$PendingAddArguments;

    .prologue
    .line 403
    iget-wide v0, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->container:J

    iput-wide v0, p0, Lcom/android/launcher2/HomeItem;->container:J

    .line 404
    iget v0, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->screen:I

    iput v0, p0, Lcom/android/launcher2/HomeItem;->mScreen:I

    .line 405
    iget v0, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->cellX:I

    iput v0, p0, Lcom/android/launcher2/HomeItem;->cellX:I

    .line 406
    iget v0, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->cellY:I

    iput v0, p0, Lcom/android/launcher2/HomeItem;->cellY:I

    .line 407
    iget v0, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->spanX:I

    iput v0, p0, Lcom/android/launcher2/HomeItem;->spanX:I

    .line 408
    iget v0, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->spanY:I

    iput v0, p0, Lcom/android/launcher2/HomeItem;->spanY:I

    .line 409
    iget-object v0, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->dropPos:[I

    iput-object v0, p0, Lcom/android/launcher2/HomeItem;->dropPos:[I

    .line 410
    iget-boolean v0, p1, Lcom/android/launcher2/HomeView$PendingAddArguments;->isSecretItem:Z

    iput-boolean v0, p0, Lcom/android/launcher2/HomeItem;->mSecretItem:Z

    .line 412
    return-void
.end method

.method private copySpans([I[I)V
    .locals 3
    .param p1, "dst"    # [I
    .param p2, "src"    # [I

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 1701
    array-length v0, p2

    if-ne v0, v2, :cond_0

    array-length v0, p1

    if-eq v0, v2, :cond_1

    .line 1702
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad array passed to copySpans src "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") dst "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1705
    :cond_1
    array-length v0, p2

    invoke-static {p2, v1, p1, v1, v0}, Ljava/lang/System;->arraycopy([II[III)V

    .line 1706
    return-void
.end method

.method private copyToPendingAddInfo(Lcom/android/launcher2/HomePendingWidget;)V
    .locals 4
    .param p1, "info"    # Lcom/android/launcher2/HomePendingWidget;

    .prologue
    .line 2132
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-wide v2, p1, Lcom/android/launcher2/HomePendingWidget;->container:J

    iput-wide v2, v0, Lcom/android/launcher2/HomeItem;->container:J

    .line 2133
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v1, p1, Lcom/android/launcher2/HomePendingWidget;->mScreen:I

    iput v1, v0, Lcom/android/launcher2/HomeItem;->mScreen:I

    .line 2134
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v1, p1, Lcom/android/launcher2/HomePendingWidget;->cellX:I

    iput v1, v0, Lcom/android/launcher2/HomeItem;->cellX:I

    .line 2135
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v1, p1, Lcom/android/launcher2/HomePendingWidget;->cellY:I

    iput v1, v0, Lcom/android/launcher2/HomeItem;->cellY:I

    .line 2136
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v1, p1, Lcom/android/launcher2/HomePendingWidget;->spanX:I

    iput v1, v0, Lcom/android/launcher2/HomeItem;->spanX:I

    .line 2137
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v1, p1, Lcom/android/launcher2/HomePendingWidget;->spanY:I

    iput v1, v0, Lcom/android/launcher2/HomeItem;->spanY:I

    .line 2138
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-object v1, p1, Lcom/android/launcher2/HomePendingWidget;->dropPos:[I

    iput-object v1, v0, Lcom/android/launcher2/HomeItem;->dropPos:[I

    .line 2139
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-boolean v1, p1, Lcom/android/launcher2/HomePendingWidget;->mSecretItem:Z

    iput-boolean v1, v0, Lcom/android/launcher2/HomeItem;->mSecretItem:Z

    .line 2140
    return-void
.end method

.method private createWorkspaceChildren()V
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v6, 0x0

    .line 1177
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getScreenCount()I

    move-result v4

    .line 1178
    .local v4, "screenCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_2

    .line 1179
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v7, 0x7f030059

    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5, v7, v8, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayoutWithResizableWidgets;

    .line 1182
    .local v0, "cell":Lcom/android/launcher2/CellLayoutWithResizableWidgets;
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayoutWithResizableWidgets;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 1183
    .local v3, "lp":Landroid/view/ViewGroup$LayoutParams;
    if-nez v3, :cond_0

    .line 1184
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    .end local v3    # "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v3, v9, v9}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1187
    .restart local v3    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5, v0, v3}, Lcom/android/launcher2/Workspace;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1188
    invoke-virtual {p0, v2}, Lcom/android/launcher2/HomeView;->determineEmptyPageMsgVisibility(I)Z

    move-result v5

    if-eqz v5, :cond_1

    move v5, v6

    :goto_1
    invoke-virtual {v0, v5}, Lcom/android/launcher2/CellLayoutWithResizableWidgets;->setEmptyMessageVisibility(I)V

    .line 1178
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1188
    :cond_1
    const/16 v5, 0x8

    goto :goto_1

    .line 1191
    .end local v0    # "cell":Lcom/android/launcher2/CellLayoutWithResizableWidgets;
    .end local v3    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_2
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getHomeScreenIndex()I

    move-result v1

    .line 1192
    .local v1, "homeScreenIndex":I
    if-ltz v1, :cond_3

    if-lt v1, v4, :cond_4

    .line 1193
    :cond_3
    if-gez v1, :cond_5

    move v1, v6

    .line 1194
    :goto_2
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/android/launcher2/LauncherApplication;->setHomeScreenIndex(Landroid/content/Context;I)V

    .line 1196
    :cond_4
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5, v1}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    .line 1197
    return-void

    .line 1193
    :cond_5
    add-int/lit8 v1, v4, -0x1

    goto :goto_2
.end method

.method public static findEmptySpanOnHomeScreen([IILandroid/content/Context;Lcom/android/launcher2/HomeView$DropPos;IIZ)Z
    .locals 10
    .param p0, "pos"    # [I
    .param p1, "screen"    # I
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "dropPos"    # Lcom/android/launcher2/HomeView$DropPos;
    .param p4, "spanX"    # I
    .param p5, "spanY"    # I
    .param p6, "showFullToast"    # Z

    .prologue
    .line 4703
    if-nez p3, :cond_1

    .line 4704
    const/4 v5, 0x0

    .line 4753
    :cond_0
    :goto_0
    return v5

    :cond_1
    move-object v8, p2

    .line 4706
    check-cast v8, Lcom/android/launcher2/Launcher;

    iget-object v8, v8, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v8}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v7

    .line 4708
    .local v7, "ws":Lcom/android/launcher2/Workspace;
    const/4 v8, -0x1

    if-ne p1, v8, :cond_2

    .line 4709
    invoke-virtual {v7}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result p1

    .line 4711
    :cond_2
    invoke-virtual {v7}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v1

    .line 4713
    .local v1, "count":I
    const/4 v5, 0x0

    .line 4715
    .local v5, "rc":Z
    const/4 v6, 0x0

    .line 4717
    .local v6, "reachedEnd":Z
    if-eqz p0, :cond_3

    array-length v8, p0

    const/4 v9, 0x2

    if-eq v8, v9, :cond_4

    .line 4718
    :cond_3
    const/4 v8, 0x2

    new-array p0, v8, [I

    .line 4720
    :cond_4
    const/4 v8, 0x0

    iput-boolean v8, p3, Lcom/android/launcher2/HomeView$DropPos;->mFoundInCurrentScreen:Z

    .line 4721
    move v3, p1

    .line 4722
    .local v3, "i":I
    :goto_1
    if-lt v3, v1, :cond_5

    .line 4723
    const/4 v3, 0x0

    .line 4724
    const/4 v6, 0x1

    .line 4726
    :cond_5
    if-eqz v6, :cond_7

    if-lt v3, p1, :cond_7

    .line 4740
    :goto_2
    if-nez v5, :cond_6

    .line 4741
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/android/launcher2/Workspace;->newPage(Z)I

    move-result v4

    .line 4742
    .local v4, "newPage":I
    const/high16 v8, -0x80000000

    if-eq v4, v8, :cond_6

    .line 4743
    invoke-virtual {v7, v4}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/CellLayout;

    .line 4744
    .local v2, "currentPage":Lcom/android/launcher2/CellLayout;
    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-virtual {v2, p0, v8, v9}, Lcom/android/launcher2/CellLayout;->findCellForSpan([III)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 4745
    const/4 v8, 0x0

    aget v8, p0, v8

    const/4 v9, 0x1

    aget v9, p0, v9

    invoke-virtual {p3, v2, v4, v8, v9}, Lcom/android/launcher2/HomeView$DropPos;->assign(Lcom/android/launcher2/CellLayout;III)V

    .line 4746
    const/4 v5, 0x1

    .line 4750
    .end local v2    # "currentPage":Lcom/android/launcher2/CellLayout;
    .end local v4    # "newPage":I
    :cond_6
    iget-boolean v8, p3, Lcom/android/launcher2/HomeView$DropPos;->mFoundInCurrentScreen:Z

    if-nez v8, :cond_0

    if-eqz p6, :cond_0

    .line 4751
    invoke-static {p2}, Lcom/android/launcher2/HomeView;->showOutOfSpaceMessage(Landroid/content/Context;)V

    goto :goto_0

    .line 4729
    :cond_7
    invoke-virtual {v7, v3}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4730
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_9

    instance-of v8, v0, Lcom/android/launcher2/CellLayout;

    if-eqz v8, :cond_9

    move-object v2, v0

    .line 4731
    check-cast v2, Lcom/android/launcher2/CellLayout;

    .line 4732
    .restart local v2    # "currentPage":Lcom/android/launcher2/CellLayout;
    invoke-virtual {v2, p0, p4, p5}, Lcom/android/launcher2/CellLayout;->findCellForSpan([III)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 4733
    const/4 v8, 0x0

    aget v8, p0, v8

    const/4 v9, 0x1

    aget v9, p0, v9

    invoke-virtual {p3, v2, v3, v8, v9}, Lcom/android/launcher2/HomeView$DropPos;->assign(Lcom/android/launcher2/CellLayout;III)V

    .line 4734
    const/4 v5, 0x1

    .line 4735
    if-ne v3, p1, :cond_8

    const/4 v8, 0x1

    :goto_3
    iput-boolean v8, p3, Lcom/android/launcher2/HomeView$DropPos;->mFoundInCurrentScreen:Z

    goto :goto_2

    :cond_8
    const/4 v8, 0x0

    goto :goto_3

    .line 4721
    .end local v2    # "currentPage":Lcom/android/launcher2/CellLayout;
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private getExternalPackageToolbarIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
    .locals 9
    .param p1, "activityName"    # Landroid/content/ComponentName;

    .prologue
    .line 3090
    :try_start_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 3092
    .local v4, "packageManager":Landroid/content/pm/PackageManager;
    const/16 v6, 0x80

    invoke-virtual {v4, p1, v6}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v6

    iget-object v2, v6, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    .line 3094
    .local v2, "metaData":Landroid/os/Bundle;
    if-eqz v2, :cond_0

    .line 3095
    const-string v6, "com.android.launcher.toolbar_icon"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 3096
    .local v1, "iconResId":I
    if-eqz v1, :cond_0

    .line 3097
    invoke-virtual {v4, p1}, Landroid/content/pm/PackageManager;->getResourcesForActivity(Landroid/content/ComponentName;)Landroid/content/res/Resources;

    move-result-object v5

    .line 3098
    .local v5, "res":Landroid/content/res/Resources;
    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 3110
    .end local v1    # "iconResId":I
    .end local v2    # "metaData":Landroid/os/Bundle;
    .end local v4    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v5    # "res":Landroid/content/res/Resources;
    :goto_0
    return-object v6

    .line 3101
    :catch_0
    move-exception v0

    .line 3103
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-boolean v6, Lcom/android/launcher2/HomeView;->DEBUGGABLE:Z

    if-eqz v6, :cond_0

    const-string v6, "Launcher.HomeView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to load toolbar icon; "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " not found"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3110
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    :goto_1
    const/4 v6, 0x0

    goto :goto_0

    .line 3105
    :catch_1
    move-exception v3

    .line 3107
    .local v3, "nfe":Landroid/content/res/Resources$NotFoundException;
    sget-boolean v6, Lcom/android/launcher2/HomeView;->DEBUGGABLE:Z

    if-eqz v6, :cond_0

    const-string v6, "Launcher.HomeView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to load toolbar icon from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public static getFolderById(J)Lcom/android/launcher2/HomeFolderItem;
    .locals 2
    .param p0, "id"    # J

    .prologue
    .line 4501
    sget-object v0, Lcom/android/launcher2/HomeView;->sFolders:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/HomeFolderItem;

    return-object v0
.end method

.method private onAppWidgetReset()V
    .locals 1

    .prologue
    .line 2505
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    if-eqz v0, :cond_0

    .line 2506
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherAppWidgetHost;->startListening()V

    .line 2508
    :cond_0
    return-void
.end method

.method private performOnHomePressed(Z)V
    .locals 8
    .param p1, "moveToDefaultScreen"    # Z

    .prologue
    const v7, 0x7f0f008f

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 766
    const-string v2, "Launcher.HomeView"

    const-string v3, "performOnHomePressed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v2}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 768
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    .line 771
    .local v0, "launcher":Lcom/android/launcher2/Launcher;
    sget-boolean v2, Lcom/android/launcher2/HomescreenWallpaperOptionsDialogFragment;->isHomeScreenWallpaperDialogVisible:Z

    if-eqz v2, :cond_0

    .line 772
    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-static {v2}, Lcom/android/launcher2/HomescreenWallpaperOptionsDialogFragment;->dismiss(Landroid/app/FragmentManager;)V

    .line 775
    :cond_0
    sget-boolean v2, Lcom/android/launcher2/HomeScreenDialogFragment;->isHomeScreenDialogVisible:Z

    if-eqz v2, :cond_2

    .line 776
    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-static {v2}, Lcom/android/launcher2/HomeScreenDialogFragment;->dismiss(Landroid/app/FragmentManager;)V

    .line 777
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v2}, Lcom/android/launcher2/QuickViewWorkspace;->drawCloseAnimation()Z

    .line 826
    .end local v0    # "launcher":Lcom/android/launcher2/Launcher;
    :cond_1
    :goto_0
    return-void

    .line 779
    .restart local v0    # "launcher":Lcom/android/launcher2/Launcher;
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/launcher2/HomeScreenOptionMenu;->isActive()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 780
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v2}, Lcom/android/launcher2/QuickViewWorkspace;->drawCloseAnimation()Z

    goto :goto_0

    .line 782
    :cond_3
    invoke-virtual {p0, v4}, Lcom/android/launcher2/HomeView;->closeQuickViewWorkspace(Z)V

    goto :goto_0

    .line 786
    .end local v0    # "launcher":Lcom/android/launcher2/Launcher;
    :cond_4
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    if-eqz v2, :cond_1

    .line 789
    sget-boolean v2, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v2, :cond_5

    .line 790
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getIsDragOccuring()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/android/launcher2/Launcher;->isHomeEditMode()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/android/launcher2/Launcher;->isHomeRemoveMode()Z

    move-result v2

    if-nez v2, :cond_1

    .line 795
    :cond_5
    sget-boolean v2, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v2, :cond_8

    .line 796
    invoke-static {}, Lcom/android/launcher2/Launcher;->isHomeEditMode()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 797
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->exitWidgetResizeMode()V

    .line 798
    invoke-static {v6}, Lcom/android/launcher2/Launcher;->setHomeRemoveMode(Z)V

    .line 799
    invoke-static {v6}, Lcom/android/launcher2/Launcher;->setHomeEditMode(Z)V

    .line 800
    invoke-virtual {p0, v4}, Lcom/android/launcher2/HomeView;->showWorkspace(Z)V

    .line 809
    :goto_1
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->closeFolder()V

    .line 810
    iput-boolean v4, p0, Lcom/android/launcher2/HomeView;->mHomeKeyPress:Z

    .line 813
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 814
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v1

    .line 815
    .local v1, "openFolder":Lcom/android/launcher2/Folder;
    invoke-virtual {v1}, Lcom/android/launcher2/Folder;->isEditingName()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 816
    invoke-virtual {v1, v4}, Lcom/android/launcher2/Folder;->dismissEditingName(Z)V

    .line 822
    .end local v1    # "openFolder":Lcom/android/launcher2/Folder;
    :cond_6
    :goto_2
    if-eqz p1, :cond_1

    .line 823
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v4}, Lcom/android/launcher2/Workspace;->moveToDefaultScreen(Z)V

    goto :goto_0

    .line 803
    :cond_7
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1

    .line 806
    :cond_8
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1

    .line 818
    .restart local v1    # "openFolder":Lcom/android/launcher2/Folder;
    :cond_9
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->closeFolder()V

    goto :goto_2
.end method

.method private reAddCreatedPageForDragIfNeeded(Lcom/android/launcher2/CellLayout;JILcom/android/launcher2/HomeItem;)Lcom/android/launcher2/CellLayout;
    .locals 6
    .param p1, "layout"    # Lcom/android/launcher2/CellLayout;
    .param p2, "container"    # J
    .param p4, "screen"    # I
    .param p5, "info"    # Lcom/android/launcher2/HomeItem;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1531
    if-nez p1, :cond_0

    const-wide/16 v2, -0x64

    cmp-long v1, p2, v2

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getChildCount()I

    move-result v1

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getMaxScreenCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getPageCount()I

    move-result v1

    if-ne p4, v1, :cond_0

    .line 1534
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getPageCount()I

    move-result v2

    invoke-virtual {v1, v2, v4}, Lcom/android/launcher2/Workspace;->insertWorkspaceScreen(IZ)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 1535
    .local v0, "temp":Lcom/android/launcher2/CellLayout;
    if-eqz v0, :cond_0

    .line 1536
    move-object p1, v0

    .line 1540
    .end local v0    # "temp":Lcom/android/launcher2/CellLayout;
    :cond_0
    iget-boolean v1, p5, Lcom/android/launcher2/HomeItem;->mSecretItem:Z

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->getPageType()I

    move-result v1

    if-eq v1, v5, :cond_1

    .line 1541
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1, p4, v4, v5}, Lcom/android/launcher2/Workspace;->insertWorkspaceSecretScreen(IZZ)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 1542
    .restart local v0    # "temp":Lcom/android/launcher2/CellLayout;
    if-eqz v0, :cond_1

    .line 1543
    move-object p1, v0

    .line 1547
    .end local v0    # "temp":Lcom/android/launcher2/CellLayout;
    :cond_1
    sget-boolean v1, Lcom/android/launcher2/LauncherApplication;->sFestivalPageLauncher:Z

    if-eqz v1, :cond_2

    sget-boolean v1, Lcom/android/launcher2/Launcher;->sIsFestivalModeOn:Z

    if-eqz v1, :cond_2

    .line 1548
    iget-boolean v1, p5, Lcom/android/launcher2/HomeItem;->mSecretItem:Z

    if-nez v1, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/launcher2/CellLayout;->getPageType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 1549
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1, p4, v4}, Lcom/android/launcher2/Workspace;->insertWorkspaceScreen(IZ)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 1550
    .restart local v0    # "temp":Lcom/android/launcher2/CellLayout;
    if-eqz v0, :cond_2

    .line 1551
    move-object p1, v0

    .line 1556
    .end local v0    # "temp":Lcom/android/launcher2/CellLayout;
    :cond_2
    return-object p1
.end method

.method private registerContentObservers()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2488
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2489
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/android/launcher2/LauncherProvider;->CONTENT_APPWIDGET_RESET_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWidgetObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 2492
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->isHelpHubAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2493
    const-string v2, "content://com.samsung.helphub.provider/help_page_status/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2494
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHelphubObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 2497
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    const-string v2, "skt_phone20_settings"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mChangeTphoneModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 2499
    return-void
.end method

.method private resetAddInfo()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 2025
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/android/launcher2/HomeItem;->container:J

    .line 2026
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iput v4, v0, Lcom/android/launcher2/HomeItem;->mScreen:I

    .line 2027
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iput v4, v1, Lcom/android/launcher2/HomeItem;->cellY:I

    iput v4, v0, Lcom/android/launcher2/HomeItem;->cellX:I

    .line 2028
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iput v4, v1, Lcom/android/launcher2/HomeItem;->spanY:I

    iput v4, v0, Lcom/android/launcher2/HomeItem;->spanX:I

    .line 2029
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/launcher2/HomeItem;->dropPos:[I

    .line 2030
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/launcher2/HomeItem;->mSecretItem:Z

    .line 2031
    return-void
.end method

.method private restoreState(Lcom/android/launcher2/HomeView$SavedState;)V
    .locals 11
    .param p1, "ss"    # Lcom/android/launcher2/HomeView$SavedState;

    .prologue
    .line 664
    if-nez p1, :cond_0

    .line 711
    :goto_0
    return-void

    .line 668
    :cond_0
    iget v0, p1, Lcom/android/launcher2/HomeView$SavedState;->currentScreen:I

    .line 669
    .local v0, "currentScreen":I
    const/4 v8, -0x1

    if-le v0, v8, :cond_1

    .line 670
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8, v0}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    .line 673
    :cond_1
    iget-wide v4, p1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddContainer:J

    .line 674
    .local v4, "pendingAddContainer":J
    iget v3, p1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddScreen:I

    .line 676
    .local v3, "pendingAddScreen":I
    const-wide/16 v8, -0x1

    cmp-long v8, v4, v8

    if-eqz v8, :cond_3

    const/4 v8, -0x1

    if-le v3, v8, :cond_3

    .line 677
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iput-wide v4, v8, Lcom/android/launcher2/HomeItem;->container:J

    .line 678
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iput v3, v8, Lcom/android/launcher2/HomeItem;->mScreen:I

    .line 679
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v9, p1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddCellX:I

    iput v9, v8, Lcom/android/launcher2/HomeItem;->cellX:I

    .line 680
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v9, p1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddCellY:I

    iput v9, v8, Lcom/android/launcher2/HomeItem;->cellY:I

    .line 681
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v9, p1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddSpanX:I

    iput v9, v8, Lcom/android/launcher2/HomeItem;->spanX:I

    .line 682
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v9, p1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddSpanY:I

    iput v9, v8, Lcom/android/launcher2/HomeItem;->spanY:I

    .line 683
    iget-object v9, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v8, p1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddSecretItem:I

    const/4 v10, 0x1

    if-ne v8, v10, :cond_7

    const/4 v8, 0x1

    :goto_1
    iput-boolean v8, v9, Lcom/android/launcher2/HomeItem;->mSecretItem:Z

    .line 684
    iget v1, p1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddDropPosX:I

    .line 685
    .local v1, "dropPosX":I
    iget v2, p1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddDropPoxY:I

    .line 686
    .local v2, "dropPosY":I
    const/4 v8, -0x1

    if-eq v8, v1, :cond_2

    const/4 v8, -0x1

    if-eq v8, v2, :cond_2

    .line 687
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    const/4 v9, 0x2

    new-array v9, v9, [I

    iput-object v9, v8, Lcom/android/launcher2/HomeItem;->dropPos:[I

    .line 688
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-object v8, v8, Lcom/android/launcher2/HomeItem;->dropPos:[I

    const/4 v9, 0x0

    aput v1, v8, v9

    .line 689
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-object v8, v8, Lcom/android/launcher2/HomeItem;->dropPos:[I

    const/4 v9, 0x1

    aput v2, v8, v9

    .line 691
    :cond_2
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/launcher2/HomeView;->mRestoring:Z

    .line 693
    .end local v1    # "dropPosX":I
    .end local v2    # "dropPosY":I
    :cond_3
    iget-object v8, p1, Lcom/android/launcher2/HomeView$SavedState;->folderBundle:Landroid/os/Bundle;

    iput-object v8, p0, Lcom/android/launcher2/HomeView;->mFolderBundle:Landroid/os/Bundle;

    .line 694
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mFolderBundle:Landroid/os/Bundle;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHomeDarkenLayer:Landroid/view/View;

    if-eqz v8, :cond_4

    .line 698
    iget v6, p1, Lcom/android/launcher2/HomeView$SavedState;->darkenLayerVisibility:I

    .line 699
    .local v6, "visibility":I
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHomeDarkenLayer:Landroid/view/View;

    invoke-virtual {v8, v6}, Landroid/view/View;->setVisibility(I)V

    .line 701
    .end local v6    # "visibility":I
    :cond_4
    iget-boolean v7, p1, Lcom/android/launcher2/HomeView$SavedState;->inQuickView:Z

    .line 703
    .local v7, "wasInQuickView":Z
    sget-boolean v8, Lcom/android/launcher2/LauncherApplication;->sFestivalPageLauncher:Z

    if-eqz v8, :cond_5

    .line 704
    const/4 v7, 0x0

    .line 706
    :cond_5
    if-eqz v7, :cond_6

    .line 707
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget-object v8, v8, Lcom/android/launcher2/Workspace;->mPagePreviewManager:Lcom/android/launcher2/PageIndicatorPreviewManager;

    invoke-virtual {v8}, Lcom/android/launcher2/PageIndicatorPreviewManager;->layoutPageIndicator()V

    .line 708
    const/4 v8, 0x0

    invoke-virtual {p0, p1, v8}, Lcom/android/launcher2/HomeView;->openQuickViewWorkspace(Lcom/android/launcher2/HomeView$SavedState;Z)V

    .line 710
    :cond_6
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/HomeView;->mAfterSavedInstanceState:Z

    goto/16 :goto_0

    .line 683
    .end local v7    # "wasInQuickView":Z
    :cond_7
    const/4 v8, 0x0

    goto :goto_1
.end method

.method private sendAdvanceMessage(J)V
    .locals 5
    .param p1, "delay"    # J

    .prologue
    const/4 v2, 0x1

    .line 1885
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1886
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1887
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1888
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/launcher2/HomeView;->mAutoAdvanceSentTime:J

    .line 1889
    return-void
.end method

.method private setupHomeViewAfterRotationForTB(I)V
    .locals 10
    .param p1, "rotationAngle"    # I

    .prologue
    const v9, 0x7f0e0250

    const v8, 0x7f0e024f

    const/4 v7, 0x2

    .line 5146
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mHomeAppsBtn:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 5147
    .local v0, "homeAppBtnLp":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mHomePhoneBtn:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 5149
    .local v1, "homePhoneBtnLp":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 5150
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5, v2}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/CellLayoutWithResizableWidgets;

    .line 5151
    .local v4, "workspaceScreen":Lcom/android/launcher2/CellLayoutWithResizableWidgets;
    invoke-virtual {v4}, Lcom/android/launcher2/CellLayoutWithResizableWidgets;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 5152
    .local v3, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    if-ne p1, v7, :cond_0

    .line 5153
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e00b0

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 5156
    :goto_1
    invoke-virtual {v4, v3}, Lcom/android/launcher2/CellLayoutWithResizableWidgets;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5149
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 5155
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e00c6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    goto :goto_1

    .line 5159
    .end local v3    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v4    # "workspaceScreen":Lcom/android/launcher2/CellLayoutWithResizableWidgets;
    :cond_1
    if-ne p1, v7, :cond_2

    .line 5160
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout$LayoutParams;->setMarginEnd(I)V

    .line 5162
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout$LayoutParams;->setMarginStart(I)V

    .line 5164
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5}, Lcom/android/launcher2/Workspace;->getPageIndicatorManager()Lcom/android/launcher2/PageIndicatorManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher2/PageIndicatorManager;->getPageIndicator()Lcom/android/launcher2/PageIndicator;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e00ad

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/android/launcher2/PageIndicator;->setPageIndicatorPanelRightAdjust(I)V

    .line 5175
    :goto_2
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mHomeAppsBtn:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5176
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mHomePhoneBtn:Landroid/view/View;

    invoke-virtual {v5, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5178
    new-instance v5, Lcom/android/launcher2/HomeView$25;

    invoke-direct {v5, p0}, Lcom/android/launcher2/HomeView$25;-><init>(Lcom/android/launcher2/HomeView;)V

    invoke-virtual {p0, v5}, Lcom/android/launcher2/HomeView;->post(Ljava/lang/Runnable;)Z

    .line 5184
    return-void

    .line 5167
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout$LayoutParams;->setMarginEnd(I)V

    .line 5169
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout$LayoutParams;->setMarginStart(I)V

    .line 5171
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5}, Lcom/android/launcher2/Workspace;->getPageIndicatorManager()Lcom/android/launcher2/PageIndicatorManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher2/PageIndicatorManager;->getPageIndicator()Lcom/android/launcher2/PageIndicator;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e00ac

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/android/launcher2/PageIndicator;->setPageIndicatorPanelRightAdjust(I)V

    goto :goto_2
.end method

.method private setupViews()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 864
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 866
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0f0079

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/Workspace;

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    .line 867
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1, p0}, Lcom/android/launcher2/Workspace;->setPageSwitchListener(Lcom/android/launcher2/PagedView$PageSwitchListener;)V

    .line 869
    const v1, 0x7f0f00c7

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/QuickViewWorkspace;

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    .line 870
    const v1, 0x7f0f00c8

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/QuickViewDragBar;

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mDeleteDropLayout:Lcom/android/launcher2/QuickViewDragBar;

    .line 871
    const v1, 0x7f0f00c9

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/HomeScreenOptionMenu;

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mHomeScreenOptionMenu:Lcom/android/launcher2/HomeScreenOptionMenu;

    .line 873
    const v1, 0x7f0f007c

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mHomeDarkenLayer:Landroid/view/View;

    .line 874
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHomeDarkenLayer:Landroid/view/View;

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mDarkenLayerTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 876
    const v1, 0x7f0f0078

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mHomeContainer:Landroid/view/View;

    .line 878
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/LauncherApplication;->getCpuBoosterObject()Landroid/os/DVFSHelper;

    move-result-object v1

    sput-object v1, Lcom/android/launcher2/HomeView;->cpuBooster:Landroid/os/DVFSHelper;

    .line 881
    const v1, 0x7f0f007b

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/Hotseat;

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    .line 882
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-eqz v1, :cond_0

    .line 883
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v1, p0}, Lcom/android/launcher2/Hotseat;->setup(Lcom/android/launcher2/HomeView;)V

    .line 1021
    :cond_0
    invoke-static {p0}, Lcom/android/launcher2/FocusHelper;->setup(Lcom/android/launcher2/HomeView;)V

    .line 1024
    sget-boolean v1, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v1, :cond_1

    .line 1025
    const v1, 0x7f0f007a

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mHomeEditTitleBar:Landroid/view/ViewGroup;

    .line 1026
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHomeEditTitleBar:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 1027
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHomeEditTitleBar:Landroid/view/ViewGroup;

    const v2, 0x7f0f0076

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeEditButtonListener:Lcom/android/launcher2/HomeView$HomeEditTabClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1028
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHomeEditTitleBar:Landroid/view/ViewGroup;

    const v2, 0x7f0f004f

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHomeEditButtonListener:Lcom/android/launcher2/HomeView$HomeEditTabClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1032
    :cond_1
    const v1, 0x7f0f0041

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/HomeEditBar;

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mEditBar:Lcom/android/launcher2/HomeEditBar;

    .line 1041
    sget-boolean v1, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v1, :cond_4

    .line 1042
    const v1, 0x7f0f007f

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mEditIcon:Landroid/view/View;

    .line 1043
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mEditIcon:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 1044
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mEditIcon:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1045
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mEditIcon:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setHoverPopupType(I)V

    .line 1046
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mEditIcon:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    const v2, 0x7f100031

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/HoverPopupWindow;->setContent(Ljava/lang/CharSequence;)V

    .line 1047
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mEditIcon:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    const/16 v2, 0x3035

    invoke-virtual {v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 1048
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mEditIcon:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    const v2, 0x7f0e0018

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 1049
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mEditIcon:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 1051
    :cond_2
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mEditIcon:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1052
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mEditIcon:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1055
    :cond_3
    const v1, 0x7f0f0083

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mEditIconDivider:Landroid/view/View;

    .line 1056
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mEditIconDivider:Landroid/view/View;

    if-eqz v1, :cond_4

    .line 1057
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mEditIconDivider:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1077
    :cond_4
    const v1, 0x7f0f007e

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mHomeTopBar:Landroid/view/View;

    .line 1078
    iget-boolean v1, p0, Lcom/android/launcher2/HomeView;->removeHotseat:Z

    if-eqz v1, :cond_5

    .line 1079
    const v1, 0x7f0f0087

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    .line 1080
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    if-eqz v1, :cond_5

    .line 1081
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1083
    const v1, 0x7f0f0089

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mGoogleSearchView:Landroid/view/View;

    .line 1084
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mGoogleSearchView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->SearchButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1085
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mGoogleSearchView:Landroid/view/View;

    sget-object v2, Lcom/android/launcher2/FocusHelper;->GOOGLE_SEARCH_KEY_LISTENER:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1086
    const v1, 0x7f0f008a

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mVoiceSearchView:Landroid/view/View;

    .line 1087
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mVoiceSearchView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->VoiceSearchButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1088
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mVoiceSearchView:Landroid/view/View;

    sget-object v2, Lcom/android/launcher2/FocusHelper;->GOOGLE_SEARCH_KEY_LISTENER:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1090
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->setTabletHotseat()V

    .line 1095
    :cond_5
    const v1, 0x7f0f008b

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mMyFilesView:Landroid/view/View;

    .line 1096
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mMyFilesView:Landroid/view/View;

    if-eqz v1, :cond_6

    .line 1097
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mMyFilesView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->MyFilesButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1100
    :cond_6
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1, v3}, Lcom/android/launcher2/Workspace;->setHapticFeedbackEnabled(Z)V

    .line 1101
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1, p0}, Lcom/android/launcher2/Workspace;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1102
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1, p0}, Lcom/android/launcher2/Workspace;->setup(Lcom/android/launcher2/HomeView;)V

    .line 1104
    invoke-direct {p0}, Lcom/android/launcher2/HomeView;->createWorkspaceChildren()V

    .line 1106
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-eqz v1, :cond_7

    .line 1107
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Workspace;->addStateAnimatorProvider(Lcom/android/launcher2/Workspace$StateAnimatorProvider;)V

    .line 1108
    :cond_7
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    if-eqz v1, :cond_8

    .line 1109
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Workspace;->addStateAnimatorProvider(Lcom/android/launcher2/Workspace$StateAnimatorProvider;)V

    .line 1110
    :cond_8
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    if-eqz v1, :cond_9

    .line 1111
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Workspace;->addStateAnimatorProvider(Lcom/android/launcher2/Workspace$StateAnimatorProvider;)V

    .line 1112
    :cond_9
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mEditBar:Lcom/android/launcher2/HomeEditBar;

    if-eqz v1, :cond_a

    .line 1113
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mEditBar:Lcom/android/launcher2/HomeEditBar;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Workspace;->addStateAnimatorProvider(Lcom/android/launcher2/Workspace$StateAnimatorProvider;)V

    .line 1123
    :cond_a
    return-void
.end method

.method public static showNoRoomAnyPageMessage(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1980
    const v0, 0x7f10001b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1981
    return-void
.end method

.method public static showOutOfSpaceMessage(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1984
    const v0, 0x7f10001a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1985
    return-void
.end method

.method private updateButtonWithIconFromExternalActivity(ILandroid/content/ComponentName;I)Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 3
    .param p1, "buttonId"    # I
    .param p2, "activityName"    # Landroid/content/ComponentName;
    .param p3, "fallbackDrawableId"    # I

    .prologue
    .line 3142
    invoke-virtual {p0, p1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 3143
    .local v0, "button":Landroid/widget/ImageView;
    invoke-direct {p0, p2}, Lcom/android/launcher2/HomeView;->getExternalPackageToolbarIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 3145
    .local v1, "toolbarIcon":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 3148
    if-nez v1, :cond_1

    .line 3149
    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 3155
    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    :goto_1
    return-object v2

    .line 3151
    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 3155
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private updateTextButtonWithIconFromExternalActivity(ILandroid/content/ComponentName;I)Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 8
    .param p1, "buttonId"    # I
    .param p2, "activityName"    # Landroid/content/ComponentName;
    .param p3, "fallbackDrawableId"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 3118
    invoke-virtual {p0, p1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 3119
    .local v0, "button":Landroid/widget/TextView;
    invoke-direct {p0, p2}, Lcom/android/launcher2/HomeView;->getExternalPackageToolbarIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 3120
    .local v3, "toolbarIcon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 3121
    .local v2, "r":Landroid/content/res/Resources;
    const v6, 0x7f0e00fd

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 3122
    .local v4, "w":I
    const v6, 0x7f0e00fe

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 3125
    .local v1, "h":I
    if-nez v3, :cond_0

    .line 3126
    invoke-virtual {v2, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 3127
    invoke-virtual {v3, v7, v7, v4, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 3128
    invoke-virtual {v0, v3, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 3133
    :goto_0
    return-object v5

    .line 3131
    :cond_0
    invoke-virtual {v3, v7, v7, v4, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 3132
    invoke-virtual {v0, v3, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 3133
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    goto :goto_0
.end method


# virtual methods
.method public ChangeTphoneMode()V
    .locals 3

    .prologue
    .line 3190
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 3191
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f0b0009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3192
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getModel()Lcom/android/launcher2/LauncherModel;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/android/launcher2/LauncherModel;->ChangeTphoneMode(Lcom/android/launcher2/HomeView;)V

    .line 3198
    :goto_0
    return-void

    .line 3194
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 3195
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.samsung.android.cocktail.action.CHANGE_CALL_ITEM"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3196
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public HelpAppAddwidget()Lcom/android/launcher2/HomeWidgetItem;
    .locals 24

    .prologue
    .line 4842
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->addHelpAppPage()V

    .line 4844
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getNumPages()I

    move-result v2

    add-int/lit8 v22, v2, -0x1

    .line 4848
    .local v22, "screen":I
    const/4 v2, 0x2

    new-array v12, v2, [I

    fill-array-data v12, :array_0

    .line 4850
    .local v12, "cellXY":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    move-object/from16 v23, v0

    .line 4851
    .local v23, "workspace":Lcom/android/launcher2/Workspace;
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getAppWidgetHost()Lcom/android/launcher2/LauncherAppWidgetHost;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/launcher2/LauncherAppWidgetHost;->allocateAppWidgetId()I

    move-result v10

    .line 4853
    .local v10, "appWidgetId":I
    const/16 v20, 0x0

    .line 4854
    .local v20, "pkgName":Ljava/lang/String;
    const/4 v13, 0x0

    .line 4856
    .local v13, "className":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v4, "CscFeature_Launcher_DisableGoogleOption"

    invoke-virtual {v2, v4}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4858
    const-string v20, "com.sec.android.app.sbrowser"

    .line 4859
    const-string v13, "com.sec.android.app.sbrowser.BookmarkThumbnailWidgetProvider"

    .line 4866
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v16

    .line 4868
    .local v16, "mPkgMgr":Landroid/content/pm/PackageManager;
    const/16 v2, 0x207

    :try_start_0
    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v19

    .line 4874
    .local v19, "pkgInfo":Landroid/content/pm/PackageInfo;
    :goto_1
    if-eqz v19, :cond_0

    move-object/from16 v0, v19

    iget-object v2, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v2, v2, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v2, :cond_1

    .line 4875
    :cond_0
    const-string v20, "com.sec.android.widgetapp.SPlannerAppWidget"

    .line 4876
    const-string v13, "com.sec.android.widgetapp.SPlannerAppWidget.AgendaWidget.CalendarAppWidgetProvider"

    .line 4879
    :cond_1
    new-instance v21, Landroid/content/ComponentName;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v13}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 4880
    .local v21, "provider":Landroid/content/ComponentName;
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v10, v0}, Landroid/appwidget/AppWidgetManager;->bindAppWidgetIdIfAllowed(ILandroid/content/ComponentName;)Z

    .line 4881
    sget-object v2, Lcom/android/launcher2/Workspace$State;->NORMAL:Lcom/android/launcher2/Workspace$State;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v4, v5, v6}, Lcom/android/launcher2/Workspace;->changeState(Lcom/android/launcher2/Workspace$State;ZILcom/android/launcher2/BaseItem;)V

    .line 4882
    invoke-virtual/range {v23 .. v23}, Lcom/android/launcher2/Workspace;->exitWidgetResizeMode()V

    .line 4884
    const/4 v2, -0x1

    if-ne v10, v2, :cond_3

    const/4 v3, 0x0

    .line 4937
    :goto_2
    return-object v3

    .line 4861
    .end local v16    # "mPkgMgr":Landroid/content/pm/PackageManager;
    .end local v19    # "pkgInfo":Landroid/content/pm/PackageInfo;
    .end local v21    # "provider":Landroid/content/ComponentName;
    :cond_2
    const-string v20, "com.android.chrome"

    .line 4862
    const-string v13, "com.google.android.apps.chrome.appwidget.bookmarks.BookmarkThumbnailWidgetProvider"

    goto :goto_0

    .line 4871
    .restart local v16    # "mPkgMgr":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v14

    .line 4872
    .local v14, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/16 v19, 0x0

    .restart local v19    # "pkgInfo":Landroid/content/pm/PackageInfo;
    goto :goto_1

    .line 4886
    .end local v14    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v21    # "provider":Landroid/content/ComponentName;
    :cond_3
    new-instance v17, Landroid/appwidget/AppWidgetProviderInfo;

    invoke-direct/range {v17 .. v17}, Landroid/appwidget/AppWidgetProviderInfo;-><init>()V

    .line 4889
    .local v17, "newAppWidgetProviderInfo":Landroid/appwidget/AppWidgetProviderInfo;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 4890
    .local v11, "appWidgetInfos":Ljava/util/List;, "Ljava/util/List<Landroid/appwidget/AppWidgetProviderInfo;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v2}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v11

    .line 4892
    const/4 v15, 0x0

    .local v15, "j":I
    :goto_3
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    if-ge v15, v2, :cond_4

    .line 4894
    invoke-interface {v11, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v2, v2, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v11, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v2, v2, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 4898
    invoke-interface {v11, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "newAppWidgetProviderInfo":Landroid/appwidget/AppWidgetProviderInfo;
    check-cast v17, Landroid/appwidget/AppWidgetProviderInfo;

    .line 4902
    .restart local v17    # "newAppWidgetProviderInfo":Landroid/appwidget/AppWidgetProviderInfo;
    :cond_4
    new-instance v3, Lcom/android/launcher2/HomeWidgetItem;

    move-object/from16 v0, v17

    invoke-direct {v3, v0, v10}, Lcom/android/launcher2/HomeWidgetItem;-><init>(Landroid/appwidget/AppWidgetProviderInfo;I)V

    .line 4904
    .local v3, "item":Lcom/android/launcher2/HomeWidgetItem;
    move/from16 v0, v22

    iput v0, v3, Lcom/android/launcher2/HomeWidgetItem;->mScreen:I

    .line 4905
    const/4 v2, 0x0

    aget v2, v12, v2

    iput v2, v3, Lcom/android/launcher2/HomeWidgetItem;->cellX:I

    .line 4906
    const/4 v2, 0x1

    aget v2, v12, v2

    iput v2, v3, Lcom/android/launcher2/HomeWidgetItem;->cellY:I

    .line 4907
    const/4 v2, 0x3

    iput v2, v3, Lcom/android/launcher2/HomeWidgetItem;->spanX:I

    .line 4908
    const/4 v2, 0x3

    iput v2, v3, Lcom/android/launcher2/HomeWidgetItem;->spanY:I

    .line 4912
    iget-object v2, v3, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    if-eqz v2, :cond_5

    iget-object v2, v3, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual {v2}, Landroid/appwidget/AppWidgetHostView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 4917
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    if-nez v2, :cond_7

    .line 4919
    const/4 v3, 0x0

    goto :goto_2

    .line 4892
    .end local v3    # "item":Lcom/android/launcher2/HomeWidgetItem;
    :cond_6
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 4921
    .restart local v3    # "item":Lcom/android/launcher2/HomeWidgetItem;
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v2, v4, v10, v0}, Lcom/android/launcher2/LauncherAppWidgetHost;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v2

    iput-object v2, v3, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    .line 4922
    iget-object v2, v3, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v0, v17

    invoke-virtual {v2, v10, v0}, Landroid/appwidget/AppWidgetHostView;->setAppWidget(ILandroid/appwidget/AppWidgetProviderInfo;)V

    .line 4923
    iget-object v2, v3, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual {v2, v3}, Landroid/appwidget/AppWidgetHostView;->setTag(Ljava/lang/Object;)V

    .line 4925
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    move-object/from16 v0, v17

    iget-object v4, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v2, v4}, Lcom/android/launcher2/WorkspaceSpanCalculator;->getPaddingForWidget(Landroid/content/ComponentName;)Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;

    move-result-object v18

    .line 4926
    .local v18, "padding":Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;
    iget-object v2, v3, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v0, v18

    iget v4, v0, Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;->left:I

    move-object/from16 v0, v18

    iget v5, v0, Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;->top:I

    move-object/from16 v0, v18

    iget v6, v0, Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;->right:I

    move-object/from16 v0, v18

    iget v7, v0, Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;->bottom:I

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/appwidget/AppWidgetHostView;->setPadding(IIII)V

    .line 4930
    .end local v18    # "padding":Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;
    :cond_8
    iget-object v2, v3, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    const/high16 v4, 0x1000000

    invoke-virtual {v2, v4}, Landroid/appwidget/AppWidgetHostView;->setBackgroundColor(I)V

    .line 4931
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-wide/16 v4, -0x64

    iget v6, v3, Lcom/android/launcher2/HomeWidgetItem;->mScreen:I

    iget v7, v3, Lcom/android/launcher2/HomeWidgetItem;->cellX:I

    iget v8, v3, Lcom/android/launcher2/HomeWidgetItem;->cellY:I

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/android/launcher2/LauncherModel;->addItemToDatabase(Landroid/content/Context;Lcom/android/launcher2/HomeItem;JIIIZ)V

    .line 4934
    iget-object v2, v3, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    check-cast v2, Lcom/android/launcher2/LauncherAppWidgetHostView;

    iget-object v2, v2, Lcom/android/launcher2/LauncherAppWidgetHostView;->mLastTouch:[I

    const/4 v4, 0x0

    const/16 v5, 0xde

    aput v5, v2, v4

    .line 4935
    iget-object v2, v3, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    check-cast v2, Lcom/android/launcher2/LauncherAppWidgetHostView;

    iget-object v2, v2, Lcom/android/launcher2/LauncherAppWidgetHostView;->mLastTouch:[I

    const/4 v4, 0x0

    const/16 v5, 0x113

    aput v5, v2, v4

    .line 4936
    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Workspace;->addItem(Lcom/android/launcher2/HomeItem;)V

    goto/16 :goto_2

    .line 4848
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public addAppWidgetFromDrop(Lcom/android/launcher2/HomePendingWidget;)V
    .locals 3
    .param p1, "info"    # Lcom/android/launcher2/HomePendingWidget;

    .prologue
    .line 2183
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mAppWidgetBinder:Lcom/android/launcher2/AppWidgetBinder;

    iget-object v1, p1, Lcom/android/launcher2/HomePendingWidget;->componentName:Landroid/content/ComponentName;

    new-instance v2, Lcom/android/launcher2/HomeView$17;

    invoke-direct {v2, p0, p1}, Lcom/android/launcher2/HomeView$17;-><init>(Lcom/android/launcher2/HomeView;Lcom/android/launcher2/HomePendingWidget;)V

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/AppWidgetBinder;->allocateBindNew(Landroid/content/ComponentName;Lcom/android/launcher2/AppWidgetBinder$Callback;)V

    .line 2199
    return-void
.end method

.method addAppWidgetFromPick(Landroid/content/Intent;)V
    .locals 3
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 2035
    const-string v1, "appWidgetId"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2038
    .local v0, "appWidgetId":I
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/HomeView;->addAppWidgetImpl(ILcom/android/launcher2/HomePendingWidget;)V

    .line 2039
    return-void
.end method

.method public addHelpAppPage()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4361
    iget-boolean v0, p0, Lcom/android/launcher2/HomeView;->isHelpAppPageAdded:Z

    if-eqz v0, :cond_0

    .line 4379
    :goto_0
    return-void

    .line 4364
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/HomeView;->screenIndexBeforeHelpAppPageAddition:I

    .line 4365
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getNumPages()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getNumFestivalPages()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/HomeView;->screenIndexFestivalHelpAppPageAddition:I

    .line 4367
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->sFestivalPageLauncher:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/android/launcher2/Launcher;->sIsFestivalModeOn:Z

    if-eqz v0, :cond_1

    .line 4368
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget v1, p0, Lcom/android/launcher2/HomeView;->screenIndexFestivalHelpAppPageAddition:I

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/Workspace;->insertWorkspaceScreen(IZ)Lcom/android/launcher2/CellLayout;

    .line 4371
    :goto_1
    iput-boolean v2, p0, Lcom/android/launcher2/HomeView;->isHelpAppPageDeleted:Z

    .line 4372
    iput-boolean v3, p0, Lcom/android/launcher2/HomeView;->isHelpAppPageAdded:Z

    .line 4373
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Workspace;->hidePageIndicator(Z)V

    .line 4374
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->disableRollNavigation()V

    .line 4375
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->sFestivalPageLauncher:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/android/launcher2/Launcher;->sIsFestivalModeOn:Z

    if-eqz v0, :cond_2

    .line 4376
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget v1, p0, Lcom/android/launcher2/HomeView;->screenIndexFestivalHelpAppPageAddition:I

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    goto :goto_0

    .line 4370
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->addPage()V

    goto :goto_1

    .line 4378
    :cond_2
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getNumPages()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    goto :goto_0
.end method

.method public addItemToHomeScreen()V
    .locals 19

    .prologue
    .line 4942
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/launcher2/HomeView;->mIsHelpItemAdded:Z

    if-eqz v2, :cond_1

    .line 4990
    :cond_0
    :goto_0
    return-void

    .line 4944
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    move-object/from16 v18, v0

    .line 4945
    .local v18, "workspace":Lcom/android/launcher2/Workspace;
    if-eqz v18, :cond_0

    .line 4947
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/launcher2/HomeView;->mIsHelpItemAdded:Z

    .line 4948
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->addHelpAppPage()V

    .line 4949
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->disableRollNavigation()V

    .line 4950
    const/4 v2, 0x2

    new-array v0, v2, [I

    move-object/from16 v16, v0

    fill-array-data v16, :array_0

    .line 4951
    .local v16, "spanXY":[I
    new-instance v10, Landroid/content/ComponentName;

    const-string v2, "com.sec.android.app.popupcalculator"

    const-string v4, "com.sec.android.app.popupcalculator.Calculator"

    invoke-direct {v10, v2, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 4954
    .local v10, "cn":Landroid/content/ComponentName;
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    .line 4956
    .local v14, "mPkgMgr":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v2, "com.sec.android.app.popupcalculator"

    const/16 v4, 0x207

    invoke-virtual {v14, v2, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v15

    .line 4962
    .local v15, "pkgInfo":Landroid/content/pm/PackageInfo;
    :goto_1
    if-eqz v15, :cond_2

    iget-object v2, v15, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v2, v2, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v2, :cond_3

    .line 4963
    :cond_2
    new-instance v10, Landroid/content/ComponentName;

    .end local v10    # "cn":Landroid/content/ComponentName;
    const-string v2, "com.samsung.helphub"

    const-string v4, "com.samsung.helphub.HelpHubActivity"

    invoke-direct {v10, v2, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 4966
    .restart local v10    # "cn":Landroid/content/ComponentName;
    :cond_3
    new-instance v13, Lcom/android/launcher2/AppItem;

    const/4 v2, 0x0

    invoke-direct {v13, v10, v2}, Lcom/android/launcher2/AppItem;-><init>(Landroid/content/ComponentName;Z)V

    .line 4967
    .local v13, "item1":Lcom/android/launcher2/AppItem;
    new-instance v12, Landroid/content/Intent;

    invoke-direct {v12}, Landroid/content/Intent;-><init>()V

    .line 4968
    .local v12, "intent":Landroid/content/Intent;
    invoke-virtual {v12, v10}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 4969
    invoke-virtual {v13}, Lcom/android/launcher2/AppItem;->makeHomeItem()Lcom/android/launcher2/HomeShortcutItem;

    move-result-object v3

    .line 4971
    .local v3, "info":Lcom/android/launcher2/HomeShortcutItem;
    new-instance v3, Lcom/android/launcher2/HomeShortcutItem;

    .end local v3    # "info":Lcom/android/launcher2/HomeShortcutItem;
    sget-object v2, Lcom/android/launcher2/BaseItem$Type;->HOME_APPLICATION:Lcom/android/launcher2/BaseItem$Type;

    invoke-direct {v3, v2}, Lcom/android/launcher2/HomeShortcutItem;-><init>(Lcom/android/launcher2/BaseItem$Type;)V

    .line 4972
    .restart local v3    # "info":Lcom/android/launcher2/HomeShortcutItem;
    new-instance v17, Lcom/android/launcher2/PkgResCache$TitleIconInfo;

    invoke-direct/range {v17 .. v17}, Lcom/android/launcher2/PkgResCache$TitleIconInfo;-><init>()V

    .line 4973
    .local v17, "titleAndIcon":Lcom/android/launcher2/PkgResCache$TitleIconInfo;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mPkgResCache:Lcom/android/launcher2/PkgResCache;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v12}, Lcom/android/launcher2/PkgResCache;->getTitleAndIcon(Lcom/android/launcher2/PkgResCache$TitleIconInfo;Landroid/content/Intent;)V

    .line 4974
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/android/launcher2/PkgResCache$TitleIconInfo;->mIcon:Landroid/graphics/Bitmap;

    iput-object v2, v3, Lcom/android/launcher2/HomeShortcutItem;->mIconBitmap:Landroid/graphics/Bitmap;

    .line 4975
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/android/launcher2/PkgResCache$TitleIconInfo;->mTitle:Ljava/lang/String;

    iput-object v2, v3, Lcom/android/launcher2/HomeShortcutItem;->mTitle:Ljava/lang/String;

    .line 4977
    const/4 v2, 0x0

    iput v2, v3, Lcom/android/launcher2/HomeShortcutItem;->cellX:I

    .line 4978
    const/4 v2, 0x0

    iput v2, v3, Lcom/android/launcher2/HomeShortcutItem;->cellY:I

    .line 4979
    const-wide/16 v4, -0x64

    iput-wide v4, v3, Lcom/android/launcher2/HomeShortcutItem;->container:J

    .line 4980
    const/4 v2, 0x0

    aget v2, v16, v2

    iput v2, v3, Lcom/android/launcher2/HomeShortcutItem;->spanX:I

    .line 4981
    const/4 v2, 0x1

    aget v2, v16, v2

    iput v2, v3, Lcom/android/launcher2/HomeShortcutItem;->spanY:I

    .line 4982
    iput-object v12, v3, Lcom/android/launcher2/HomeShortcutItem;->intent:Landroid/content/Intent;

    .line 4983
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getNumPages()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, v3, Lcom/android/launcher2/HomeShortcutItem;->mScreen:I

    .line 4984
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-wide/16 v4, -0x64

    iget v6, v3, Lcom/android/launcher2/HomeShortcutItem;->mScreen:I

    iget v7, v3, Lcom/android/launcher2/HomeShortcutItem;->cellX:I

    iget v8, v3, Lcom/android/launcher2/HomeShortcutItem;->cellY:I

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/android/launcher2/LauncherModel;->addItemToDatabase(Landroid/content/Context;Lcom/android/launcher2/HomeItem;JIIIZ)V

    .line 4986
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Workspace;->addItem(Lcom/android/launcher2/HomeItem;)V

    .line 4987
    invoke-virtual/range {v18 .. v18}, Lcom/android/launcher2/Workspace;->setDataIsReady()V

    .line 4988
    invoke-virtual/range {v18 .. v18}, Lcom/android/launcher2/Workspace;->invalidatePageData()V

    .line 4989
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/android/launcher2/Workspace;->hidePageIndicator(Z)V

    goto/16 :goto_0

    .line 4959
    .end local v3    # "info":Lcom/android/launcher2/HomeShortcutItem;
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v13    # "item1":Lcom/android/launcher2/AppItem;
    .end local v15    # "pkgInfo":Landroid/content/pm/PackageInfo;
    .end local v17    # "titleAndIcon":Lcom/android/launcher2/PkgResCache$TitleIconInfo;
    :catch_0
    move-exception v11

    .line 4960
    .local v11, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v15, 0x0

    .restart local v15    # "pkgInfo":Landroid/content/pm/PackageInfo;
    goto/16 :goto_1

    .line 4950
    :array_0
    .array-data 4
        0x1
        0x1
    .end array-data
.end method

.method public addPage()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, -0x1

    .line 4398
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030059

    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3, v4, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 4400
    .local v0, "cell":Lcom/android/launcher2/CellLayout;
    invoke-virtual {v0, p0}, Lcom/android/launcher2/CellLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 4401
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 4402
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    if-nez v1, :cond_0

    .line 4403
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    .end local v1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v1, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 4409
    .restart local v1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher2/Launcher;->getShowEmptyPageMessagePref()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 4411
    .local v2, "visibility":I
    :goto_0
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    if-eqz v3, :cond_1

    .line 4412
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3, v0, v1}, Lcom/android/launcher2/Workspace;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 4413
    :cond_1
    invoke-virtual {v0, v2}, Lcom/android/launcher2/CellLayout;->setEmptyMessageVisibility(I)V

    .line 4414
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getNumPages()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/launcher2/WorkspacePages;->addPageAt(ILandroid/content/Context;)Z

    .line 4416
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->saveScreenInfo()V

    .line 4417
    return-void

    .line 4409
    .end local v2    # "visibility":I
    :cond_2
    const/16 v2, 0x8

    goto :goto_0
.end method

.method public addSamsungWidgetFromDrop(Lcom/android/launcher2/HomePendingSamsungWidget;)Z
    .locals 5
    .param p1, "pending"    # Lcom/android/launcher2/HomePendingSamsungWidget;

    .prologue
    .line 2210
    const/4 v0, 0x0

    .line 2212
    .local v0, "addOk":Z
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/launcher2/HomeView;->addSamsungWidget(Lcom/android/launcher2/HomePendingSamsungWidget;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2220
    :goto_0
    return v0

    .line 2213
    :catch_0
    move-exception v1

    .line 2214
    .local v1, "e":Ljava/lang/SecurityException;
    const-string v2, "Launcher.HomeView"

    const-string v3, "Problem binding samsung widget. This should only happen when running in a separate process"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2216
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "Widget is not running in the same process"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public addShortcutItemsIntoExistingFolder(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/launcher2/HomeShortcutItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/HomeShortcutItem;>;"
    const/4 v5, 0x0

    .line 1512
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1513
    .local v0, "activity":Landroid/app/Activity;
    iget-wide v2, p0, Lcom/android/launcher2/HomeView;->mTargetFolderId:J

    invoke-static {v2, v3}, Lcom/android/launcher2/HomeView;->getFolderById(J)Lcom/android/launcher2/HomeFolderItem;

    move-result-object v9

    .line 1514
    .local v9, "existingFolder":Lcom/android/launcher2/HomeFolderItem;
    if-eqz v9, :cond_1

    .line 1515
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/HomeShortcutItem;

    .line 1516
    .local v1, "folderItem":Lcom/android/launcher2/HomeShortcutItem;
    iget-wide v2, v9, Lcom/android/launcher2/HomeFolderItem;->mId:J

    invoke-virtual {v9}, Lcom/android/launcher2/HomeFolderItem;->getItemCount()I

    move-result v4

    iget v7, v1, Lcom/android/launcher2/HomeShortcutItem;->spanX:I

    iget v8, v1, Lcom/android/launcher2/HomeShortcutItem;->spanY:I

    move v6, v5

    invoke-static/range {v0 .. v8}, Lcom/android/launcher2/LauncherModel;->addOrMoveItemInDatabase(Landroid/content/Context;Lcom/android/launcher2/HomeItem;JIIIII)V

    .line 1517
    invoke-virtual {v9, v1}, Lcom/android/launcher2/HomeFolderItem;->addItem(Lcom/android/launcher2/BaseItem;)V

    goto :goto_0

    .line 1520
    .end local v1    # "folderItem":Lcom/android/launcher2/HomeShortcutItem;
    :cond_0
    const-wide/16 v2, -0x1

    invoke-virtual {p0, v2, v3}, Lcom/android/launcher2/HomeView;->setDestinationNewFolderId(J)V

    .line 1522
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method public addShortcutItemsIntoNewFolder(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/launcher2/HomeShortcutItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/HomeShortcutItem;>;"
    const/4 v5, 0x0

    .line 1494
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1495
    .local v0, "activity":Landroid/app/Activity;
    sget-wide v2, Lcom/android/launcher2/HomeView;->mDestinationNewFolderId:J

    invoke-static {v2, v3}, Lcom/android/launcher2/HomeView;->getFolderById(J)Lcom/android/launcher2/HomeFolderItem;

    move-result-object v10

    .line 1496
    .local v10, "newFolder":Lcom/android/launcher2/HomeFolderItem;
    if-eqz v10, :cond_1

    .line 1497
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/HomeShortcutItem;

    .line 1498
    .local v1, "folderItem":Lcom/android/launcher2/HomeShortcutItem;
    iget-wide v2, v10, Lcom/android/launcher2/HomeFolderItem;->mId:J

    invoke-virtual {v10}, Lcom/android/launcher2/HomeFolderItem;->getItemCount()I

    move-result v4

    iget v7, v1, Lcom/android/launcher2/HomeShortcutItem;->spanX:I

    iget v8, v1, Lcom/android/launcher2/HomeShortcutItem;->spanY:I

    move v6, v5

    invoke-static/range {v0 .. v8}, Lcom/android/launcher2/LauncherModel;->addOrMoveItemInDatabase(Landroid/content/Context;Lcom/android/launcher2/HomeItem;JIIIII)V

    .line 1499
    invoke-virtual {v10, v1}, Lcom/android/launcher2/HomeFolderItem;->addItem(Lcom/android/launcher2/BaseItem;)V

    goto :goto_0

    .line 1501
    .end local v1    # "folderItem":Lcom/android/launcher2/HomeShortcutItem;
    :cond_0
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget v3, v10, Lcom/android/launcher2/HomeFolderItem;->mScreen:I

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Workspace;->snapToPage(I)V

    .line 1502
    const-wide/16 v2, -0x1

    invoke-virtual {p0, v2, v3}, Lcom/android/launcher2/HomeView;->setDestinationNewFolderId(J)V

    .line 1504
    .end local v9    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method public addSurfaceWidgetFromDrop(Lcom/android/launcher2/HomePendingSurfaceWidget;)Z
    .locals 5
    .param p1, "pending"    # Lcom/android/launcher2/HomePendingSurfaceWidget;

    .prologue
    .line 2319
    const/4 v0, 0x0

    .line 2321
    .local v0, "addOk":Z
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/launcher2/HomeView;->addSurfaceWidget(Lcom/android/launcher2/HomePendingSurfaceWidget;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2330
    :goto_0
    return v0

    .line 2322
    :catch_0
    move-exception v1

    .line 2323
    .local v1, "e":Ljava/lang/SecurityException;
    const-string v2, "Launcher.HomeView"

    const-string v3, "Problem binding surface widget. This should only happen when running in a separate process"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2325
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "Widget is not running in the same process"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2328
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addWidgetFromIntent(Lcom/android/launcher2/HomeWidgetItem;)V
    .locals 3
    .param p1, "item"    # Lcom/android/launcher2/HomeWidgetItem;

    .prologue
    .line 1431
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mAppWidgetBinder:Lcom/android/launcher2/AppWidgetBinder;

    invoke-virtual {p1}, Lcom/android/launcher2/HomeWidgetItem;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    new-instance v2, Lcom/android/launcher2/HomeView$10;

    invoke-direct {v2, p0, p1}, Lcom/android/launcher2/HomeView$10;-><init>(Lcom/android/launcher2/HomeView;Lcom/android/launcher2/HomeWidgetItem;)V

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/AppWidgetBinder;->allocateBindNew(Landroid/content/ComponentName;Lcom/android/launcher2/AppWidgetBinder$Callback;)V

    .line 1489
    return-void
.end method

.method addWidgetToAutoAdvanceIfNeeded(Landroid/view/View;Landroid/appwidget/AppWidgetProviderInfo;)V
    .locals 3
    .param p1, "hostView"    # Landroid/view/View;
    .param p2, "appWidgetInfo"    # Landroid/appwidget/AppWidgetProviderInfo;

    .prologue
    .line 1954
    if-eqz p2, :cond_0

    iget v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 1961
    :cond_0
    :goto_0
    return-void

    .line 1955
    :cond_1
    iget v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1956
    .local v0, "v":Landroid/view/View;
    instance-of v1, v0, Landroid/widget/Advanceable;

    if-eqz v1, :cond_0

    .line 1957
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWidgetsToAdvance:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1958
    check-cast v0, Landroid/widget/Advanceable;

    .end local v0    # "v":Landroid/view/View;
    invoke-interface {v0}, Landroid/widget/Advanceable;->fyiWillBeAdvancedByHostKThx()V

    .line 1959
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->updateRunning()V

    goto :goto_0
.end method

.method public bindAppWidget(Lcom/android/launcher2/HomeWidgetItem;)V
    .locals 24
    .param p1, "item"    # Lcom/android/launcher2/HomeWidgetItem;

    .prologue
    .line 3399
    const-wide/16 v16, 0x0

    .line 3403
    .local v16, "start":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    move-object/from16 v18, v0

    .line 3405
    .local v18, "workspace":Lcom/android/launcher2/Workspace;
    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/launcher2/HomeWidgetItem;->spanX:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/android/launcher2/Workspace;->mCellCountX:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-gt v0, v1, :cond_0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/launcher2/HomeWidgetItem;->spanY:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/android/launcher2/Workspace;->mCellCountY:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_3

    .line 3406
    :cond_0
    const-string v19, "Launcher.HomeView"

    const-string v20, "Attempted to bind a widget larger than what will fit. Removing."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3407
    invoke-static {}, Lcom/android/launcher2/Utilities;->DEBUGGABLE()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 3408
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 3409
    .local v10, "now":J
    new-instance v9, Ljava/util/Date;

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    .line 3410
    .local v9, "date":Ljava/util/Date;
    new-instance v14, Ljava/text/SimpleDateFormat;

    const-string v19, "yyyy/MM/dd HH:mm:ss"

    move-object/from16 v0, v19

    invoke-direct {v14, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 3411
    .local v14, "sdfNow":Ljava/text/SimpleDateFormat;
    invoke-virtual {v14, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    .line 3412
    .local v15, "strNow":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v19

    check-cast v19, Landroid/app/Activity;

    invoke-virtual/range {v19 .. v19}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v6

    check-cast v6, Lcom/android/launcher2/LauncherApplication;

    .line 3413
    .local v6, "app":Lcom/android/launcher2/LauncherApplication;
    const-string v19, "com.sec.android.app.launcher.prefs"

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Lcom/android/launcher2/LauncherApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v13

    .line 3414
    .local v13, "prefs":Landroid/content/SharedPreferences$Editor;
    const-string v19, "delelteFrombindAppWidget"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/android/launcher2/HomeWidgetItem;->getPackageName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", time : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 3415
    invoke-interface {v13}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 3417
    .end local v6    # "app":Lcom/android/launcher2/LauncherApplication;
    .end local v9    # "date":Ljava/util/Date;
    .end local v10    # "now":J
    .end local v13    # "prefs":Landroid/content/SharedPreferences$Editor;
    .end local v14    # "sdfNow":Ljava/text/SimpleDateFormat;
    .end local v15    # "strNow":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/android/launcher2/LauncherModel;->deleteItemFromDatabase(Landroid/content/Context;Lcom/android/launcher2/HomeItem;)V

    .line 3486
    :cond_2
    :goto_0
    return-void

    .line 3438
    :cond_3
    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/launcher2/HomeWidgetItem;->appWidgetId:I

    move/from16 v19, v0

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_2

    .line 3440
    move-object/from16 v0, p1

    iget v7, v0, Lcom/android/launcher2/HomeWidgetItem;->appWidgetId:I

    .line 3441
    .local v7, "appWidgetId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/HomeView;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v8

    .line 3442
    .local v8, "appWidgetInfo":Landroid/appwidget/AppWidgetProviderInfo;
    if-nez v8, :cond_4

    .line 3446
    sget-boolean v19, Lcom/android/launcher2/HomeView;->DEBUGGABLE:Z

    if-eqz v19, :cond_2

    const-string v19, "Launcher.HomeView"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "App widget info is null. AppWidgetID = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3455
    :cond_4
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v19, v0

    if-eqz v19, :cond_5

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/appwidget/AppWidgetHostView;->getParent()Landroid/view/ViewParent;

    move-result-object v19

    if-eqz v19, :cond_8

    .line 3460
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    move-object/from16 v19, v0

    if-nez v19, :cond_6

    .line 3461
    const-string v19, "Launcher.HomeView"

    const-string v20, "mAppWidgetHost is null. Was HomeView.onDetachedFromWindow() called previously?"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3464
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v7, v8}, Lcom/android/launcher2/LauncherAppWidgetHost;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    .line 3466
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v7, v8}, Landroid/appwidget/AppWidgetHostView;->setAppWidget(ILandroid/appwidget/AppWidgetProviderInfo;)V

    .line 3467
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetHostView;->setTag(Ljava/lang/Object;)V

    .line 3469
    invoke-virtual/range {p1 .. p1}, Lcom/android/launcher2/HomeWidgetItem;->getComponentName()Landroid/content/ComponentName;

    move-result-object v19

    sget-object v20, Lcom/android/launcher2/Workspace;->GOOGLE_SEARCH_WIDGET:Landroid/content/ComponentName;

    invoke-virtual/range {v19 .. v20}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/launcher2/HomeWidgetItem;->mScreen:I

    move/from16 v19, v0

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getHomeScreenIndex()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_7

    .line 3471
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v19

    check-cast v19, Lcom/android/launcher2/Launcher;

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Lcom/android/launcher2/Launcher;->setEnableHotWord(Z)V

    .line 3474
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/HomeView;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    move-object/from16 v19, v0

    iget-object v0, v8, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Lcom/android/launcher2/WorkspaceSpanCalculator;->getPaddingForWidget(Landroid/content/ComponentName;)Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;

    move-result-object v12

    .line 3475
    .local v12, "padding":Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v19, v0

    iget v0, v12, Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;->left:I

    move/from16 v20, v0

    iget v0, v12, Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;->top:I

    move/from16 v21, v0

    iget v0, v12, Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;->right:I

    move/from16 v22, v0

    iget v0, v12, Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;->bottom:I

    move/from16 v23, v0

    invoke-virtual/range {v19 .. v23}, Landroid/appwidget/AppWidgetHostView;->setPadding(IIII)V

    .line 3476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/HomeView;->mSpanCalculator:Lcom/android/launcher2/WorkspaceSpanCalculator;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v21, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v19

    check-cast v19, Lcom/android/launcher2/Launcher;

    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/launcher2/HomeWidgetItem;->spanX:I

    move/from16 v22, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/launcher2/HomeWidgetItem;->spanY:I

    move/from16 v23, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/launcher2/WorkspaceSpanCalculator;->updateWidgetSizeRanges(Landroid/appwidget/AppWidgetHostView;Lcom/android/launcher2/Launcher;II)V

    .line 3479
    .end local v12    # "padding":Lcom/android/launcher2/WorkspaceSpanCalculator$Padding;
    :cond_8
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v19, v0

    const/high16 v20, 0x1000000

    invoke-virtual/range {v19 .. v20}, Landroid/appwidget/AppWidgetHostView;->setBackgroundColor(I)V

    .line 3480
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->addItem(Lcom/android/launcher2/HomeItem;)V

    goto/16 :goto_0
.end method

.method public bindFolders(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/launcher2/HomeFolderItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3275
    .local p1, "folders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/android/launcher2/HomeFolderItem;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3276
    .local v0, "changes":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/HomeItem;>;"
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/HomeFolderItem;

    .line 3277
    .local v1, "folder":Lcom/android/launcher2/HomeFolderItem;
    invoke-virtual {v1, v0}, Lcom/android/launcher2/HomeFolderItem;->normalize(Ljava/util/List;)V

    goto :goto_0

    .line 3281
    .end local v1    # "folder":Lcom/android/launcher2/HomeFolderItem;
    :cond_0
    sget-object v3, Lcom/android/launcher2/HomeView;->sFolders:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 3283
    sget-object v3, Lcom/android/launcher2/HomeView;->sFolders:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 3284
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 3285
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/android/launcher2/LauncherModel;->moveItemsInDatabase(Landroid/content/Context;Ljava/util/List;)V

    .line 3287
    :cond_1
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mFolderBundle:Landroid/os/Bundle;

    invoke-virtual {p0, v3}, Lcom/android/launcher2/HomeView;->restoreOpenFolder(Landroid/os/Bundle;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 3292
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mHomeDarkenLayer:Landroid/view/View;

    if-eqz v3, :cond_2

    .line 3293
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mHomeDarkenLayer:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3296
    :cond_2
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mHomeContainer:Landroid/view/View;

    if-eqz v3, :cond_3

    .line 3297
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mHomeContainer:Landroid/view/View;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    .line 3301
    :cond_3
    return-void
.end method

.method public bindHomeDeleteFestivalPage()V
    .locals 6

    .prologue
    .line 4609
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v0

    .line 4611
    .local v0, "currentPage":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 4612
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/CellLayout;

    .line 4614
    .local v2, "v":Lcom/android/launcher2/CellLayout;
    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getPageType()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 4615
    const-string v3, "Launcher.HomeView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bindHomeDeleteFestivalPage : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4616
    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->deletePage(I)V

    .line 4617
    add-int/lit8 v1, v1, -0x1

    .line 4611
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4621
    .end local v2    # "v":Lcom/android/launcher2/CellLayout;
    :cond_1
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v3

    if-lt v0, v3, :cond_2

    .line 4622
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getHomeScreenIndex()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    .line 4623
    :cond_2
    return-void
.end method

.method public bindHomeDeleteSecretPage()V
    .locals 6

    .prologue
    .line 4543
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v0

    .line 4545
    .local v0, "currentPage":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 4546
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4, v2}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/CellLayout;

    .line 4548
    .local v3, "v":Lcom/android/launcher2/CellLayout;
    invoke-virtual {v3}, Lcom/android/launcher2/CellLayout;->getPageType()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 4549
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4, v3}, Lcom/android/launcher2/Workspace;->removeView(Landroid/view/View;)V

    .line 4550
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/android/launcher2/WorkspacePages;->deletePageAt(ILandroid/content/Context;)Z

    .line 4551
    add-int/lit8 v2, v2, -0x1

    .line 4545
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4555
    .end local v3    # "v":Lcom/android/launcher2/CellLayout;
    :cond_1
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getSecretScreenCount()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 4557
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getHomeScreenIndex()I

    move-result v1

    .line 4562
    .local v1, "homeIdx":I
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4, v1}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    .line 4565
    .end local v1    # "homeIdx":I
    :goto_1
    return-void

    .line 4564
    :cond_2
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getSecretScreenCount()I

    move-result v5

    sub-int v5, v0, v5

    invoke-virtual {v4, v5}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    goto :goto_1
.end method

.method public bindHomeInsertFestivalPage()V
    .locals 12

    .prologue
    .line 4568
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v9

    check-cast v9, Lcom/android/launcher2/Launcher;

    invoke-virtual {v9}, Lcom/android/launcher2/Launcher;->getFestivalPageManager()Lcom/android/launcher2/FestivalPageManager;

    move-result-object v5

    .line 4569
    .local v5, "fpMgr":Lcom/android/launcher2/FestivalPageManager;
    invoke-static {}, Lcom/android/launcher2/FestivalPageManager;->getFestivalString()Ljava/lang/String;

    move-result-object v1

    .line 4571
    .local v1, "festivalDayList":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 4605
    :cond_0
    :goto_0
    return-void

    .line 4574
    :cond_1
    const-string v9, ";"

    invoke-virtual {v1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 4575
    .local v4, "festivalName":[Ljava/lang/String;
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getScreenCount()I

    move-result v0

    .line 4576
    .local v0, "currentPageCount":I
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getMaxFestivalScreenCount()I

    move-result v3

    .line 4577
    .local v3, "festivalMaxCount":I
    array-length v8, v4

    .line 4578
    .local v8, "toBeCount":I
    const/4 v7, -0x1

    .line 4579
    .local v7, "index":I
    const/4 v2, -0x1

    .line 4584
    .local v2, "festivalKey":I
    sget-boolean v9, Lcom/android/launcher2/HomeView;->DEBUGGABLE:Z

    if-eqz v9, :cond_2

    .line 4585
    const-string v9, "Launcher.HomeView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "bindHomeInsertFestivalPage toBeCount : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    array-length v11, v4

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " festivalDayList : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4587
    :cond_2
    if-le v8, v3, :cond_3

    .line 4588
    move v8, v3

    .line 4590
    :cond_3
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-ge v6, v8, :cond_4

    .line 4591
    aget-object v9, v4, v6

    if-nez v9, :cond_5

    .line 4603
    :cond_4
    const-string v9, "Launcher.HomeView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "bindHomeInsertFestivalPage currentPageCount [ "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4604
    iget-object v9, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v9, v0}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    goto :goto_0

    .line 4594
    :cond_5
    add-int v7, v0, v6

    .line 4595
    aget-object v9, v4, v6

    invoke-static {v9}, Lcom/android/launcher2/FestivalPageManager;->getFestivalType(Ljava/lang/String;)I

    move-result v2

    .line 4596
    iget-object v9, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/4 v10, 0x0

    invoke-virtual {v9, v7, v2, v10}, Lcom/android/launcher2/Workspace;->insertWorkspaceFestivalScreen(IIZ)Lcom/android/launcher2/CellLayout;

    .line 4597
    iget-object v9, p0, Lcom/android/launcher2/HomeView;->mAppWidgetBinder:Lcom/android/launcher2/AppWidgetBinder;

    const/4 v10, 0x1

    aget-object v11, v4, v6

    invoke-virtual {v5, v9, v7, v10, v11}, Lcom/android/launcher2/FestivalPageManager;->bindFestivalWidget(Lcom/android/launcher2/AppWidgetBinder;IILjava/lang/String;)V

    .line 4598
    iget-object v9, p0, Lcom/android/launcher2/HomeView;->mAppWidgetBinder:Lcom/android/launcher2/AppWidgetBinder;

    const/4 v10, 0x2

    aget-object v11, v4, v6

    invoke-virtual {v5, v9, v7, v10, v11}, Lcom/android/launcher2/FestivalPageManager;->bindFestivalWidget(Lcom/android/launcher2/AppWidgetBinder;IILjava/lang/String;)V

    .line 4599
    iget-object v9, p0, Lcom/android/launcher2/HomeView;->mAppWidgetBinder:Lcom/android/launcher2/AppWidgetBinder;

    const/4 v10, 0x3

    aget-object v11, v4, v6

    invoke-virtual {v5, v9, v7, v10, v11}, Lcom/android/launcher2/FestivalPageManager;->bindFestivalWidget(Lcom/android/launcher2/AppWidgetBinder;IILjava/lang/String;)V

    .line 4601
    const-string v9, "Launcher.HomeView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "bindHomeInsertFestivalPage festivalName [ "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " ]  = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    aget-object v11, v4, v6

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " fesivalKey : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4590
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method public bindHomeInsertSecretPage()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 4533
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getSecretScreenCount()I

    move-result v1

    .line 4534
    .local v1, "toBeCount":I
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 4535
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3, v4}, Lcom/android/launcher2/Workspace;->insertWorkspaceSecretScreen(IZZ)Lcom/android/launcher2/CellLayout;

    .line 4534
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4537
    :cond_0
    if-lez v1, :cond_1

    .line 4538
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v4}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    .line 4539
    :cond_1
    return-void
.end method

.method public bindHomeItemsRemoved(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/launcher2/HomeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3641
    .local p1, "removed":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/HomeItem;>;"
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    if-eqz v0, :cond_0

    .line 3642
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/Workspace;->removeItems(Ljava/util/List;)V

    .line 3644
    :cond_0
    return-void
.end method

.method public bindHomeItemsUpdated(Ljava/util/List;Z)V
    .locals 1
    .param p2, "isBadgeUpdate"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/launcher2/HomeItem;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 3630
    .local p1, "updated":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/HomeItem;>;"
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    if-eqz v0, :cond_0

    .line 3631
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, p1, p2}, Lcom/android/launcher2/Workspace;->updateShortcutsAndSurfaceWidgets(Ljava/util/List;Z)V

    .line 3633
    :cond_0
    return-void
.end method

.method public bindHotseat(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/launcher2/HomeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3319
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/HomeItem;>;"
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-nez v2, :cond_0

    .line 3338
    :goto_0
    return-void

    .line 3324
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/android/launcher2/Hotseat;->normalizeContents(Landroid/content/Context;Ljava/util/List;)V

    .line 3325
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    iget-object v2, v2, Lcom/android/launcher2/Hotseat;->mContent:Lcom/android/launcher2/CellLayoutHotseat;

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayoutHotseat;->removeAllItems()V

    .line 3326
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    iget-object v2, v2, Lcom/android/launcher2/Hotseat;->mContent:Lcom/android/launcher2/CellLayoutHotseat;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/launcher2/CellLayoutHotseat;->beginBind(I)V

    .line 3327
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/HomeItem;

    .line 3328
    .local v1, "item":Lcom/android/launcher2/HomeItem;
    iget v2, v1, Lcom/android/launcher2/HomeItem;->mScreen:I

    if-gez v2, :cond_1

    .line 3329
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    iget v3, v1, Lcom/android/launcher2/HomeItem;->cellX:I

    iget v4, v1, Lcom/android/launcher2/HomeItem;->cellY:I

    invoke-virtual {v2, v3, v4}, Lcom/android/launcher2/Hotseat;->getOrderInHotseat(II)I

    move-result v2

    iput v2, v1, Lcom/android/launcher2/HomeItem;->mScreen:I

    .line 3331
    :cond_1
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    iget v3, v1, Lcom/android/launcher2/HomeItem;->mScreen:I

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Hotseat;->getCellXFromOrder(I)I

    move-result v2

    iput v2, v1, Lcom/android/launcher2/HomeItem;->cellX:I

    .line 3332
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    iget v3, v1, Lcom/android/launcher2/HomeItem;->mScreen:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/android/launcher2/Hotseat;->getCellYFromOrder(II)I

    move-result v2

    iput v2, v1, Lcom/android/launcher2/HomeItem;->cellY:I

    .line 3334
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v2, v1}, Lcom/android/launcher2/Hotseat;->addItem(Lcom/android/launcher2/HomeItem;)Z

    goto :goto_1

    .line 3337
    .end local v1    # "item":Lcom/android/launcher2/HomeItem;
    :cond_2
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    iget-object v2, v2, Lcom/android/launcher2/Hotseat;->mContent:Lcom/android/launcher2/CellLayoutHotseat;

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayoutHotseat;->endBind()V

    goto :goto_0
.end method

.method public bindItems(Ljava/util/List;II)V
    .locals 8
    .param p2, "start"    # I
    .param p3, "end"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/launcher2/HomeItem;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 3242
    .local p1, "shortcuts":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/HomeItem;>;"
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    .line 3243
    .local v2, "workspace":Lcom/android/launcher2/Workspace;
    if-nez v2, :cond_0

    .line 3266
    :goto_0
    return-void

    .line 3245
    :cond_0
    move v0, p2

    .local v0, "i":I
    :goto_1
    if-ge v0, p3, :cond_2

    .line 3246
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/HomeItem;

    .line 3251
    .local v1, "item":Lcom/android/launcher2/HomeItem;
    iget-wide v4, v1, Lcom/android/launcher2/HomeItem;->container:J

    const-wide/16 v6, -0x65

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 3245
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3255
    :cond_1
    sget-object v3, Lcom/android/launcher2/HomeView$28;->$SwitchMap$com$android$launcher2$BaseItem$Type:[I

    iget-object v4, v1, Lcom/android/launcher2/HomeItem;->mType:Lcom/android/launcher2/BaseItem$Type;

    invoke-virtual {v4}, Lcom/android/launcher2/BaseItem$Type;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_2

    .line 3259
    :pswitch_0
    invoke-virtual {v2, v1}, Lcom/android/launcher2/Workspace;->addItem(Lcom/android/launcher2/HomeItem;)V

    goto :goto_2

    .line 3264
    .end local v1    # "item":Lcom/android/launcher2/HomeItem;
    :cond_2
    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->setDataIsReady()V

    .line 3265
    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->invalidatePageData()V

    goto :goto_0

    .line 3255
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bindSamsungWidget(Lcom/android/launcher2/SamsungWidgetItem;)V
    .locals 9
    .param p1, "item"    # Lcom/android/launcher2/SamsungWidgetItem;

    .prologue
    .line 3495
    const-wide/16 v4, 0x0

    .line 3499
    .local v4, "start":J
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    .line 3501
    .local v3, "workspace":Lcom/android/launcher2/Workspace;
    iget-object v6, p1, Lcom/android/launcher2/SamsungWidgetItem;->intent:Landroid/content/Intent;

    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 3502
    .local v0, "cn":Landroid/content/ComponentName;
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v6

    check-cast v6, Lcom/android/launcher2/Launcher;

    invoke-virtual {v6}, Lcom/android/launcher2/Launcher;->getSamsungWidgetPackageManager()Lcom/android/launcher2/SamsungWidgetPackageManager;

    move-result-object v2

    .line 3504
    .local v2, "packageManager":Lcom/android/launcher2/SamsungWidgetPackageManager;
    iget-object v6, p1, Lcom/android/launcher2/SamsungWidgetItem;->intent:Landroid/content/Intent;

    const-string v7, "com.samsung.sec.android.SAMSUNG_WIDGET.themename"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v0, v6}, Lcom/android/launcher2/SamsungWidgetPackageManager;->findWidget(Landroid/content/ComponentName;Ljava/lang/String;)Lcom/android/launcher2/SamsungWidgetProviderInfo;

    move-result-object v1

    .line 3506
    .local v1, "info":Lcom/android/launcher2/SamsungWidgetProviderInfo;
    if-nez v1, :cond_1

    .line 3507
    sget-boolean v6, Lcom/android/launcher2/HomeView;->DEBUGGABLE:Z

    if-eqz v6, :cond_0

    const-string v6, "Launcher.HomeView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to find Samsung widget "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3526
    :cond_0
    :goto_0
    return-void

    .line 3511
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v2, v6, v1, p1}, Lcom/android/launcher2/SamsungWidgetPackageManager;->createWidget(Landroid/content/Context;Lcom/android/launcher2/SamsungWidgetProviderInfo;Lcom/android/launcher2/SamsungWidgetItem;)Lcom/android/launcher2/SamsungWidgetItem;

    .line 3513
    iget-object v6, p1, Lcom/android/launcher2/SamsungWidgetItem;->mWidgetView:Lcom/android/launcher2/SamsungWidgetView;

    invoke-virtual {v6, p1}, Lcom/android/launcher2/SamsungWidgetView;->setTag(Ljava/lang/Object;)V

    .line 3515
    iget-object v6, p1, Lcom/android/launcher2/SamsungWidgetItem;->mWidgetView:Lcom/android/launcher2/SamsungWidgetView;

    const/high16 v7, 0x1000000

    invoke-virtual {v6, v7}, Lcom/android/launcher2/SamsungWidgetView;->setBackgroundColor(I)V

    .line 3516
    invoke-virtual {v3, p1}, Lcom/android/launcher2/Workspace;->addItem(Lcom/android/launcher2/HomeItem;)V

    .line 3518
    iget v6, p1, Lcom/android/launcher2/SamsungWidgetItem;->mScreen:I

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getCurrentPage()I

    move-result v7

    if-ne v6, v7, :cond_0

    .line 3519
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p1, v6}, Lcom/android/launcher2/SamsungWidgetItem;->fireOnResume(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public bindSurfaceWidget(Lcom/android/launcher2/SurfaceWidgetItem;)V
    .locals 9
    .param p1, "item"    # Lcom/android/launcher2/SurfaceWidgetItem;

    .prologue
    .line 3534
    const-wide/16 v4, 0x0

    .line 3536
    .local v4, "start":J
    const-string v6, "Launcher.HomeView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "bindSurfaceWidget: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3538
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    .line 3540
    .local v3, "workspace":Lcom/android/launcher2/Workspace;
    invoke-virtual {p1}, Lcom/android/launcher2/SurfaceWidgetItem;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    .line 3541
    .local v0, "cn":Landroid/content/ComponentName;
    sget-object v2, Lcom/android/launcher2/SurfaceWidgetPackageManager;->INST:Lcom/android/launcher2/SurfaceWidgetPackageManager;

    .line 3542
    .local v2, "packageManager":Lcom/android/launcher2/SurfaceWidgetPackageManager;
    invoke-virtual {p1}, Lcom/android/launcher2/SurfaceWidgetItem;->getThemName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v0, v6}, Lcom/android/launcher2/SurfaceWidgetPackageManager;->findWidget(Landroid/content/ComponentName;Ljava/lang/String;)Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;

    move-result-object v1

    .line 3543
    .local v1, "info":Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;
    if-nez v1, :cond_1

    .line 3544
    const-string v6, "Launcher.HomeView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to find Surface widget "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3564
    :cond_0
    :goto_0
    return-void

    .line 3548
    :cond_1
    const-string v6, "SurfaceWidgetFlow"

    const-string v7, "making surface widget for rebinding"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3549
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {p1, v6, v1, v7, v8}, Lcom/android/launcher2/SurfaceWidgetItem;->makeSurfaceWidget(Landroid/content/Context;Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;Lcom/android/launcher2/SurfaceWidgetView;Z)V

    .line 3552
    iget-object v6, p1, Lcom/android/launcher2/SurfaceWidgetItem;->mWidgetView:Lcom/android/launcher2/SurfaceWidgetView;

    if-eqz v6, :cond_0

    .line 3554
    invoke-virtual {v3, p1}, Lcom/android/launcher2/Workspace;->addItem(Lcom/android/launcher2/HomeItem;)V

    .line 3556
    iget v6, p1, Lcom/android/launcher2/SurfaceWidgetItem;->mScreen:I

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getCurrentPage()I

    move-result v7

    if-ne v6, v7, :cond_0

    .line 3557
    invoke-virtual {p1}, Lcom/android/launcher2/SurfaceWidgetItem;->onResume()V

    goto :goto_0
.end method

.method public bindWidgetsAfterConfigChange()V
    .locals 1

    .prologue
    .line 3567
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    if-eqz v0, :cond_0

    .line 3568
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->changeOrientationExtras()V

    .line 3569
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->bindWidgetsAfterConfigChange()V

    .line 3571
    :cond_0
    return-void
.end method

.method public cancelClicksOnHome()V
    .locals 4

    .prologue
    .line 4801
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->cancelClicksOnChildrenForCurrentPage()V

    .line 4802
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-eqz v3, :cond_0

    .line 4803
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v3}, Lcom/android/launcher2/Hotseat;->getLayout()Lcom/android/launcher2/CellLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/launcher2/CellLayout;->getChildrenLayout()Lcom/android/launcher2/CellLayoutChildren;

    move-result-object v1

    .line 4804
    .local v1, "clChildren":Lcom/android/launcher2/CellLayoutChildren;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Lcom/android/launcher2/CellLayoutChildren;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 4805
    invoke-virtual {v1, v2}, Lcom/android/launcher2/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4806
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    .line 4804
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4809
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "clChildren":Lcom/android/launcher2/CellLayoutChildren;
    .end local v2    # "i":I
    :cond_0
    return-void
.end method

.method cancelRemovePage()V
    .locals 1

    .prologue
    .line 4083
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/HomeView;->mIsDeletePopup:Z

    .line 4084
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v0}, Lcom/android/launcher2/QuickViewWorkspace;->cancelDeleteView()V

    .line 4085
    return-void
.end method

.method public closeHomeScreenOptions()V
    .locals 1

    .prologue
    .line 753
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->isVisibleHomeOptionMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 754
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->exitHomeOptionMenu()V

    .line 762
    :cond_0
    return-void
.end method

.method public closeQuickViewWorkspace(Z)V
    .locals 1
    .param p1, "immediate"    # Z

    .prologue
    .line 3922
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher2/HomeView;->closeQuickViewWorkspace(ZZ)V

    .line 3923
    return-void
.end method

.method public closeQuickViewWorkspace(ZZ)V
    .locals 8
    .param p1, "immediate"    # Z
    .param p2, "bResetAlpha"    # Z

    .prologue
    const/4 v7, 0x0

    .line 3926
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v5}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v5

    if-nez v5, :cond_1

    .line 4005
    :cond_0
    return-void

    .line 3928
    :cond_1
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v5, p2}, Lcom/android/launcher2/QuickViewWorkspace;->close(Z)V

    .line 3929
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5, v7}, Lcom/android/launcher2/Workspace;->setHideItems(Z)V

    .line 3930
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    new-instance v6, Lcom/android/launcher2/HomeView$24;

    invoke-direct {v6, p0, p1}, Lcom/android/launcher2/HomeView$24;-><init>(Lcom/android/launcher2/HomeView;Z)V

    invoke-virtual {v5, v6}, Lcom/android/launcher2/Workspace;->post(Ljava/lang/Runnable;)Z

    .line 3987
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getCurrentPage()I

    move-result v6

    invoke-virtual {v5, v6, v7}, Lcom/android/launcher2/Workspace;->setPageVisibility(II)V

    .line 3988
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5, v7}, Lcom/android/launcher2/Workspace;->updateIndicatorWidth(Z)V

    .line 3989
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5, v7}, Lcom/android/launcher2/Workspace;->showPageIndicator(Z)V

    .line 3990
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5}, Lcom/android/launcher2/Workspace;->updateVisiblePages()I

    .line 3991
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getCurrentPage()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/android/launcher2/Workspace;->resumeScreen(I)V

    .line 3993
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 3994
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getCurrentPage()I

    move-result v5

    if-ne v2, v5, :cond_3

    .line 3993
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3995
    :cond_3
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5, v2}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 3996
    .local v0, "cl":Lcom/android/launcher2/CellLayout;
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getChildrenLayout()Lcom/android/launcher2/CellLayoutChildren;

    move-result-object v1

    .line 3997
    .local v1, "clChildren":Lcom/android/launcher2/CellLayoutChildren;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-virtual {v1}, Lcom/android/launcher2/CellLayoutChildren;->getChildCount()I

    move-result v5

    if-ge v3, v5, :cond_2

    .line 3998
    invoke-virtual {v1, v3}, Lcom/android/launcher2/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 3999
    .local v4, "v":Landroid/view/View;
    instance-of v5, v4, Lcom/android/launcher2/SurfaceWidgetView;

    if-eqz v5, :cond_4

    move-object v5, v4

    .line 4000
    check-cast v5, Lcom/android/launcher2/SurfaceWidgetView;

    const/4 v6, 0x4

    invoke-virtual {v5, v4, v6}, Lcom/android/launcher2/SurfaceWidgetView;->onVisibilityChanged(Landroid/view/View;I)V

    .line 3997
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method closeSystemDialogs(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "isHomeKeyPressed"    # Ljava/lang/Boolean;

    .prologue
    .line 1997
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    .line 1998
    .local v0, "launcher":Lcom/android/launcher2/Launcher;
    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->closeAllPanels()V

    .line 2000
    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 2005
    .local v1, "manager":Landroid/app/FragmentManager;
    invoke-static {v1}, Lcom/android/launcher2/AddToHomescreenDialogFragment;->dismiss(Landroid/app/FragmentManager;)V

    .line 2006
    sget-boolean v2, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2007
    invoke-static {v1}, Lcom/android/launcher2/HomeScreenDialogFragment;->dismiss(Landroid/app/FragmentManager;)V

    .line 2010
    :cond_0
    sget-boolean v2, Lcom/android/launcher2/HomescreenWallpaperOptionsDialogFragment;->isHomeScreenWallpaperDialogVisible:Z

    if-eqz v2, :cond_1

    .line 2011
    invoke-static {v1}, Lcom/android/launcher2/HomescreenWallpaperOptionsDialogFragment;->dismiss(Landroid/app/FragmentManager;)V

    .line 2014
    :cond_1
    invoke-static {v1}, Lcom/android/launcher2/DeleteWorkscreenDialogFragment;->dismiss(Landroid/app/FragmentManager;)V

    .line 2017
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/launcher2/HomeView;->mWaitingForResult:Z

    .line 2018
    return-void
.end method

.method public collectExitAllAppsAnimators(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4114
    .local p1, "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f050005

    invoke-static {v1, v2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    .line 4115
    .local v0, "animator":Landroid/animation/Animator;
    invoke-virtual {v0, p0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 4116
    invoke-virtual {v0, p0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 4117
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4119
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->setPivotX(F)V

    .line 4120
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->setPivotY(F)V

    .line 4121
    return-void
.end method

.method public collectShowAllAppsAnimators(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4103
    .local p1, "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f050008

    invoke-static {v1, v2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    .line 4104
    .local v0, "animator":Landroid/animation/Animator;
    invoke-virtual {v0, p0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 4105
    invoke-virtual {v0, p0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 4106
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4108
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->setPivotX(F)V

    .line 4109
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->setPivotY(F)V

    .line 4110
    return-void
.end method

.method completeAddFolderWithShortcutItem(Lcom/android/launcher2/HomeShortcutItem;Ljava/lang/String;JZZ)V
    .locals 29
    .param p1, "folderItem"    # Lcom/android/launcher2/HomeShortcutItem;
    .param p2, "folderTitle"    # Ljava/lang/String;
    .param p3, "destinationContainerId"    # J
    .param p5, "removeItem"    # Z
    .param p6, "findNewPosition"    # Z

    .prologue
    .line 1309
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v4

    .line 1310
    .local v4, "activity":Landroid/app/Activity;
    if-eqz p1, :cond_0

    if-nez p6, :cond_0

    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/launcher2/HomeShortcutItem;->mScreen:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/launcher2/HomeShortcutItem;->cellX:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/launcher2/HomeShortcutItem;->cellY:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_9

    .line 1312
    :cond_0
    new-instance v5, Lcom/android/launcher2/HomeView$DropPos;

    invoke-direct {v5}, Lcom/android/launcher2/HomeView$DropPos;-><init>()V

    .line 1313
    .local v5, "dropPos":Lcom/android/launcher2/HomeView$DropPos;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mTmpAddItemCellCoordinates:[I

    const/4 v3, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/android/launcher2/HomeView;->findEmptySpanOnHomeScreen([IILandroid/content/Context;Lcom/android/launcher2/HomeView$DropPos;IIZ)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1314
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/launcher2/HomeView;->showNoRoomAnyPageMessage(Landroid/content/Context;)V

    .line 1420
    .end local v5    # "dropPos":Lcom/android/launcher2/HomeView$DropPos;
    :cond_1
    :goto_0
    return-void

    .line 1317
    .restart local v5    # "dropPos":Lcom/android/launcher2/HomeView$DropPos;
    :cond_2
    const-wide/16 p3, -0x64

    .line 1318
    iget v10, v5, Lcom/android/launcher2/HomeView$DropPos;->mScreen:I

    .line 1319
    .local v10, "screen":I
    iget v11, v5, Lcom/android/launcher2/HomeView$DropPos;->mCellX:I

    .line 1320
    .local v11, "cellX":I
    iget v12, v5, Lcom/android/launcher2/HomeView$DropPos;->mCellY:I

    .line 1328
    .end local v5    # "dropPos":Lcom/android/launcher2/HomeView$DropPos;
    .local v12, "cellY":I
    :goto_1
    const-wide/16 v2, -0x65

    cmp-long v2, p3, v2

    if-nez v2, :cond_a

    .line 1329
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v2}, Lcom/android/launcher2/Hotseat;->getLayout()Lcom/android/launcher2/CellLayout;

    move-result-object v23

    .line 1335
    .local v23, "destinationPage":Lcom/android/launcher2/CellLayout;
    :goto_2
    const/16 v25, 0x0

    .line 1336
    .local v25, "rc":Z
    if-eqz v23, :cond_7

    .line 1338
    new-instance v7, Lcom/android/launcher2/HomeFolderItem;

    invoke-direct {v7}, Lcom/android/launcher2/HomeFolderItem;-><init>()V

    .line 1340
    .local v7, "newFolder":Lcom/android/launcher2/HomeFolderItem;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mPkgResCache:Lcom/android/launcher2/PkgResCache;

    invoke-virtual {v2}, Lcom/android/launcher2/PkgResCache;->getFolderIcon()Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, v7, Lcom/android/launcher2/HomeFolderItem;->mIconBitmap:Landroid/graphics/Bitmap;

    .line 1341
    const/4 v2, 0x0

    iput-boolean v2, v7, Lcom/android/launcher2/HomeFolderItem;->viewForThisHasSeenLightOfDayBefore:Z

    .line 1346
    const/4 v13, 0x0

    move-object v6, v4

    move-wide/from16 v8, p3

    invoke-static/range {v6 .. v13}, Lcom/android/launcher2/LauncherModel;->addItemToDatabase(Landroid/content/Context;Lcom/android/launcher2/HomeItem;JIIIZ)V

    .line 1348
    move-object/from16 v0, p2

    invoke-virtual {v7, v4, v0}, Lcom/android/launcher2/HomeFolderItem;->setTitle(Landroid/content/Context;Ljava/lang/String;)V

    .line 1349
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher2/HomeView;->mCreateFolderColor:I

    invoke-virtual {v7, v2}, Lcom/android/launcher2/HomeFolderItem;->setFolderColor(I)V

    .line 1350
    if-eqz p1, :cond_5

    .line 1351
    if-eqz p5, :cond_4

    .line 1353
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/android/launcher2/HomeShortcutItem;->container:J

    const-wide/16 v8, -0x65

    cmp-long v2, v2, v8

    if-nez v2, :cond_b

    .line 1354
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v2}, Lcom/android/launcher2/Hotseat;->getLayout()Lcom/android/launcher2/CellLayout;

    move-result-object v27

    .line 1368
    .local v27, "sourcePage":Lcom/android/launcher2/CellLayout;
    :cond_3
    :goto_3
    if-eqz v27, :cond_4

    .line 1369
    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->removeItem(Lcom/android/launcher2/BaseItem;)Z

    .line 1373
    .end local v27    # "sourcePage":Lcom/android/launcher2/CellLayout;
    :cond_4
    iget-wide v2, v7, Lcom/android/launcher2/HomeFolderItem;->mId:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/android/launcher2/HomeView;->setDestinationNewFolderId(J)V

    .line 1375
    iget-wide v0, v7, Lcom/android/launcher2/HomeFolderItem;->mId:J

    move-wide/from16 v16, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/launcher2/HomeShortcutItem;->spanX:I

    move/from16 v21, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/launcher2/HomeShortcutItem;->spanY:I

    move/from16 v22, v0

    move-object v14, v4

    move-object/from16 v15, p1

    invoke-static/range {v14 .. v22}, Lcom/android/launcher2/LauncherModel;->addOrMoveItemInDatabase(Landroid/content/Context;Lcom/android/launcher2/HomeItem;JIIIII)V

    .line 1376
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/android/launcher2/HomeFolderItem;->addItem(Lcom/android/launcher2/BaseItem;)V

    .line 1379
    :cond_5
    const-wide/16 v2, -0x64

    cmp-long v2, p3, v2

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v2

    if-eq v10, v2, :cond_6

    .line 1380
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v10}, Lcom/android/launcher2/Workspace;->snapToPage(I)V

    .line 1382
    :cond_6
    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Lcom/android/launcher2/CellLayout;->addItem(Lcom/android/launcher2/BaseItem;)Z

    .line 1383
    const/16 v25, 0x1

    .line 1386
    const-wide/16 v2, -0x64

    cmp-long v2, p3, v2

    if-nez v2, :cond_7

    .line 1387
    move-object/from16 v0, v23

    iget-object v2, v0, Lcom/android/launcher2/CellLayout;->mPositioner:Lcom/android/launcher2/CellPositioner;

    new-instance v3, Lcom/android/launcher2/PositionDelta;

    invoke-direct {v3, v7}, Lcom/android/launcher2/PositionDelta;-><init>(Lcom/android/launcher2/BaseItem;)V

    invoke-interface {v2, v3}, Lcom/android/launcher2/CellPositioner;->addDelta(Lcom/android/launcher2/PositionDelta;)V

    .line 1390
    .end local v7    # "newFolder":Lcom/android/launcher2/HomeFolderItem;
    :cond_7
    if-nez v25, :cond_8

    .line 1391
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->showOutOfSpaceMessage()V

    .line 1395
    :cond_8
    sget-boolean v2, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v2, :cond_e

    .line 1396
    sget-object v2, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v3, "create_folder"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1397
    const/4 v2, 0x1

    sput-boolean v2, Lcom/android/launcher2/guide/GuideFragment;->isGuideCompleted:Z

    .line 1398
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f1000ac

    const/4 v6, 0x1

    invoke-static {v2, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1399
    const/4 v2, 0x1

    sput-boolean v2, Lcom/android/launcher2/HomeView;->mIsAllAppsButtonDisable:Z

    .line 1400
    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/launcher2/HomeEditBar;->isfolderCreated:Z

    .line 1401
    new-instance v24, Landroid/os/Handler;

    invoke-direct/range {v24 .. v24}, Landroid/os/Handler;-><init>()V

    .line 1402
    .local v24, "handler":Landroid/os/Handler;
    new-instance v2, Lcom/android/launcher2/HomeView$8;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/launcher2/HomeView$8;-><init>(Lcom/android/launcher2/HomeView;)V

    const-wide/16 v8, 0x384

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1322
    .end local v10    # "screen":I
    .end local v11    # "cellX":I
    .end local v12    # "cellY":I
    .end local v23    # "destinationPage":Lcom/android/launcher2/CellLayout;
    .end local v24    # "handler":Landroid/os/Handler;
    .end local v25    # "rc":Z
    :cond_9
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/launcher2/HomeShortcutItem;->container:J

    move-wide/from16 p3, v0

    .line 1323
    move-object/from16 v0, p1

    iget v10, v0, Lcom/android/launcher2/HomeShortcutItem;->mScreen:I

    .line 1324
    .restart local v10    # "screen":I
    move-object/from16 v0, p1

    iget v11, v0, Lcom/android/launcher2/HomeShortcutItem;->cellX:I

    .line 1325
    .restart local v11    # "cellX":I
    move-object/from16 v0, p1

    iget v12, v0, Lcom/android/launcher2/HomeShortcutItem;->cellY:I

    .restart local v12    # "cellY":I
    goto/16 :goto_1

    .line 1332
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v10}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Lcom/android/launcher2/CellLayout;

    .restart local v23    # "destinationPage":Lcom/android/launcher2/CellLayout;
    goto/16 :goto_2

    .line 1355
    .restart local v7    # "newFolder":Lcom/android/launcher2/HomeFolderItem;
    .restart local v25    # "rc":Z
    :cond_b
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/android/launcher2/HomeShortcutItem;->container:J

    const-wide/16 v8, -0x64

    cmp-long v2, v2, v8

    if-nez v2, :cond_c

    .line 1356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v10}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v27

    check-cast v27, Lcom/android/launcher2/CellLayout;

    .restart local v27    # "sourcePage":Lcom/android/launcher2/CellLayout;
    goto/16 :goto_3

    .line 1360
    .end local v27    # "sourcePage":Lcom/android/launcher2/CellLayout;
    :cond_c
    const/16 v27, 0x0

    .line 1361
    .restart local v27    # "sourcePage":Lcom/android/launcher2/CellLayout;
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/android/launcher2/HomeShortcutItem;->container:J

    invoke-static {v2, v3}, Lcom/android/launcher2/HomeView;->getFolderById(J)Lcom/android/launcher2/HomeFolderItem;

    move-result-object v26

    .line 1362
    .local v26, "sourceFolder":Lcom/android/launcher2/HomeFolderItem;
    if-eqz v26, :cond_d

    .line 1363
    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/HomeFolderItem;->removeItem(Lcom/android/launcher2/BaseItem;)V

    goto/16 :goto_3

    .line 1365
    :cond_d
    sget-boolean v2, Lcom/android/launcher2/HomeView;->DEBUGGABLE:Z

    if-eqz v2, :cond_3

    const-string v2, "Launcher.HomeView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "completeAddFolderWithShortcutItem. removeItem is true but container "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/launcher2/HomeShortcutItem;->container:J

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " is unknown"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1412
    .end local v7    # "newFolder":Lcom/android/launcher2/HomeFolderItem;
    .end local v26    # "sourceFolder":Lcom/android/launcher2/HomeFolderItem;
    .end local v27    # "sourcePage":Lcom/android/launcher2/CellLayout;
    :cond_e
    new-instance v24, Landroid/os/Handler;

    invoke-direct/range {v24 .. v24}, Landroid/os/Handler;-><init>()V

    .line 1413
    .restart local v24    # "handler":Landroid/os/Handler;
    new-instance v2, Lcom/android/launcher2/HomeView$9;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/launcher2/HomeView$9;-><init>(Lcom/android/launcher2/HomeView;)V

    const-wide/16 v8, 0x12c

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method

.method public completeAddShortcut(Lcom/android/launcher2/HomeShortcutItem;JIII)V
    .locals 38
    .param p1, "info"    # Lcom/android/launcher2/HomeShortcutItem;
    .param p2, "container"    # J
    .param p4, "screen"    # I
    .param p5, "cellX"    # I
    .param p6, "cellY"    # I

    .prologue
    .line 1563
    const-wide/16 v6, 0x0

    cmp-long v4, p2, v6

    if-gez v4, :cond_12

    .line 1564
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher2/HomeView;->mTmpAddItemCellCoordinates:[I

    .line 1565
    .local v14, "cellXY":[I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-object v0, v4, Lcom/android/launcher2/HomeItem;->dropPos:[I

    move-object/from16 v36, v0

    .line 1570
    .local v36, "touchXY":[I
    move/from16 v8, p4

    .line 1571
    .local v8, "realScreen":I
    sget-boolean v4, Lcom/android/launcher2/Launcher;->sIsSecretModeOn:Z

    if-eqz v4, :cond_0

    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/android/launcher2/HomeShortcutItem;->mSecretItem:Z

    if-nez v4, :cond_0

    .line 1572
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getSecretScreenCount()I

    move-result v4

    add-int/2addr v8, v4

    .line 1574
    :cond_0
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2, v8}, Lcom/android/launcher2/HomeView;->getCellLayout(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v5

    .local v5, "layout":Lcom/android/launcher2/CellLayout;
    move-object/from16 v4, p0

    move-wide/from16 v6, p2

    move-object/from16 v9, p1

    .line 1575
    invoke-direct/range {v4 .. v9}, Lcom/android/launcher2/HomeView;->reAddCreatedPageForDragIfNeeded(Lcom/android/launcher2/CellLayout;JILcom/android/launcher2/HomeItem;)Lcom/android/launcher2/CellLayout;

    move-result-object v5

    .line 1577
    const/16 v29, 0x0

    .line 1579
    .local v29, "foundCellSpan":Z
    if-eqz v5, :cond_5

    .line 1581
    if-ltz p5, :cond_3

    if-ltz p6, :cond_3

    .line 1582
    const/4 v4, 0x0

    aput p5, v14, v4

    .line 1583
    const/4 v4, 0x1

    aput p6, v14, v4

    .line 1584
    const/16 v29, 0x1

    .line 1595
    :cond_1
    :goto_0
    if-nez v29, :cond_8

    .line 1596
    const-wide/16 v6, -0x64

    cmp-long v4, p2, v6

    if-eqz v4, :cond_6

    .line 1680
    .end local v5    # "layout":Lcom/android/launcher2/CellLayout;
    .end local v8    # "realScreen":I
    .end local v14    # "cellXY":[I
    .end local v29    # "foundCellSpan":Z
    .end local v36    # "touchXY":[I
    :cond_2
    :goto_1
    return-void

    .line 1585
    .restart local v5    # "layout":Lcom/android/launcher2/CellLayout;
    .restart local v8    # "realScreen":I
    .restart local v14    # "cellXY":[I
    .restart local v29    # "foundCellSpan":Z
    .restart local v36    # "touchXY":[I
    :cond_3
    if-eqz v36, :cond_1

    .line 1589
    const/4 v4, 0x0

    aget v10, v36, v4

    const/4 v4, 0x1

    aget v11, v36, v4

    const/4 v12, 0x1

    const/4 v13, 0x1

    move-object v9, v5

    invoke-virtual/range {v9 .. v14}, Lcom/android/launcher2/CellLayout;->findNearestVacantArea(IIII[I)[I

    move-result-object v33

    .line 1590
    .local v33, "result":[I
    if-eqz v33, :cond_4

    const/16 v29, 0x1

    .line 1591
    :goto_2
    goto :goto_0

    .line 1590
    :cond_4
    const/16 v29, 0x0

    goto :goto_2

    .line 1593
    .end local v33    # "result":[I
    :cond_5
    const/16 p4, -0x1

    goto :goto_0

    .line 1599
    :cond_6
    new-instance v17, Lcom/android/launcher2/HomeView$DropPos;

    invoke-direct/range {v17 .. v17}, Lcom/android/launcher2/HomeView$DropPos;-><init>()V

    .line 1600
    .local v17, "dropPos":Lcom/android/launcher2/HomeView$DropPos;
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v16

    const/16 v18, 0x1

    const/16 v19, 0x1

    const/16 v20, 0x0

    move/from16 v15, p4

    invoke-static/range {v14 .. v20}, Lcom/android/launcher2/HomeView;->findEmptySpanOnHomeScreen([IILandroid/content/Context;Lcom/android/launcher2/HomeView$DropPos;IIZ)Z

    move-result v29

    .line 1601
    move-object/from16 v0, v17

    iget v0, v0, Lcom/android/launcher2/HomeView$DropPos;->mScreen:I

    move/from16 p4, v0

    .line 1602
    const/4 v4, 0x0

    move-object/from16 v0, v17

    iget v6, v0, Lcom/android/launcher2/HomeView$DropPos;->mCellX:I

    aput v6, v14, v4

    .line 1603
    const/4 v4, 0x1

    move-object/from16 v0, v17

    iget v6, v0, Lcom/android/launcher2/HomeView$DropPos;->mCellY:I

    aput v6, v14, v4

    .line 1604
    move-object/from16 v0, v17

    iget-boolean v4, v0, Lcom/android/launcher2/HomeView$DropPos;->mFoundInCurrentScreen:Z

    if-nez v4, :cond_7

    .line 1605
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/launcher2/HomeView;->showNoRoomAnyPageMessage(Landroid/content/Context;)V

    .line 1607
    :cond_7
    if-eqz v29, :cond_2

    .line 1611
    .end local v17    # "dropPos":Lcom/android/launcher2/HomeView$DropPos;
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v18

    const/4 v4, 0x0

    aget v23, v14, v4

    const/4 v4, 0x1

    aget v24, v14, v4

    const/16 v25, 0x0

    move-object/from16 v19, p1

    move-wide/from16 v20, p2

    move/from16 v22, p4

    invoke-static/range {v18 .. v25}, Lcom/android/launcher2/LauncherModel;->addItemToDatabase(Landroid/content/Context;Lcom/android/launcher2/HomeItem;JIIIZ)V

    .line 1612
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/launcher2/HomeView;->mRestoring:Z

    if-nez v4, :cond_9

    .line 1613
    const-wide/16 v6, -0x65

    cmp-long v4, p2, v6

    if-nez v4, :cond_a

    .line 1614
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    iget-object v0, v4, Lcom/android/launcher2/Hotseat;->mContent:Lcom/android/launcher2/CellLayoutHotseat;

    move-object/from16 v19, v0

    const/4 v4, 0x0

    aget v21, v14, v4

    const/4 v4, 0x1

    aget v22, v14, v4

    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/launcher2/HomeShortcutItem;->spanX:I

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/launcher2/HomeShortcutItem;->spanY:I

    move/from16 v24, v0

    move-object/from16 v20, p1

    move/from16 v25, p4

    move-wide/from16 v26, p2

    invoke-virtual/range {v19 .. v27}, Lcom/android/launcher2/CellLayoutHotseat;->setItemLocation(Lcom/android/launcher2/BaseItem;IIIIIJ)V

    .line 1615
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/android/launcher2/Hotseat;->addItem(Lcom/android/launcher2/HomeItem;)Z

    .line 1666
    :cond_9
    :goto_3
    if-eqz v5, :cond_2

    .line 1667
    invoke-virtual {v5}, Lcom/android/launcher2/CellLayout;->commitDeltas()V

    goto/16 :goto_1

    .line 1617
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Lcom/android/launcher2/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Lcom/android/launcher2/CellLayout;

    .line 1619
    .local v32, "page":Lcom/android/launcher2/CellLayout;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v4

    move/from16 v0, p4

    if-eq v0, v4, :cond_10

    .line 1621
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget-object v4, v4, Lcom/android/launcher2/Workspace;->mDefaultTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

    sget-object v6, Lcom/android/launcher2/PagedView$TransitionEffect;->CARD:Lcom/android/launcher2/PagedView$TransitionEffect;

    if-ne v4, v6, :cond_f

    .line 1622
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/android/launcher2/Workspace;->setContentIsRefreshable(Z)V

    .line 1623
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Lcom/android/launcher2/Workspace;->snapToPage(I)V

    .line 1624
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/android/launcher2/Workspace;->addItem(Lcom/android/launcher2/HomeItem;)V

    .line 1633
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/4 v6, 0x0

    move/from16 v0, p4

    invoke-virtual {v4, v0, v6}, Lcom/android/launcher2/Workspace;->invalidatePageData(IZ)V

    .line 1634
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getPageCount()I

    move-result v35

    .line 1636
    .local v35, "totalpagecount":I
    const/4 v4, 0x3

    move/from16 v0, v35

    if-le v0, v4, :cond_e

    .line 1637
    const/16 v34, 0x0

    .line 1638
    .local v34, "startindex":I
    move/from16 v28, v35

    .line 1639
    .local v28, "endindex":I
    add-int/lit8 v4, v35, -0x1

    move/from16 v0, p4

    if-ne v0, v4, :cond_b

    sget-boolean v4, Lcom/android/launcher2/Launcher;->sIsHeadlinesAppEnable:Z

    if-nez v4, :cond_b

    .line 1640
    const/16 v34, 0x1

    .line 1642
    :cond_b
    move/from16 v30, v34

    .local v30, "i":I
    :goto_4
    add-int/lit8 v4, p4, -0x1

    move/from16 v0, v30

    if-ge v0, v4, :cond_c

    .line 1643
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/4 v6, 0x0

    move/from16 v0, v30

    invoke-virtual {v4, v0, v6}, Lcom/android/launcher2/Workspace;->syncPageItems(IZ)V

    .line 1642
    add-int/lit8 v30, v30, 0x1

    goto :goto_4

    .line 1645
    :cond_c
    if-nez p4, :cond_d

    sget-boolean v4, Lcom/android/launcher2/Launcher;->sIsHeadlinesAppEnable:Z

    if-nez v4, :cond_d

    .line 1646
    add-int/lit8 v28, v35, -0x1

    .line 1648
    :cond_d
    add-int/lit8 v30, p4, 0x2

    :goto_5
    move/from16 v0, v30

    move/from16 v1, v28

    if-ge v0, v1, :cond_e

    .line 1649
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/4 v6, 0x0

    move/from16 v0, v30

    invoke-virtual {v4, v0, v6}, Lcom/android/launcher2/Workspace;->syncPageItems(IZ)V

    .line 1648
    add-int/lit8 v30, v30, 0x1

    goto :goto_5

    .line 1652
    .end local v28    # "endindex":I
    .end local v30    # "i":I
    .end local v34    # "startindex":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/android/launcher2/Workspace;->setContentIsRefreshable(Z)V

    .line 1660
    .end local v35    # "totalpagecount":I
    :goto_6
    move-object/from16 v0, v32

    iget-object v4, v0, Lcom/android/launcher2/CellLayout;->mChildren:Lcom/android/launcher2/CellLayoutChildren;

    iget-object v4, v4, Lcom/android/launcher2/CellLayoutChildren;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_11

    const/16 v37, 0x1

    .line 1661
    .local v37, "wasPageEmpty":Z
    :goto_7
    if-eqz v37, :cond_9

    move-object/from16 v0, v32

    iget-object v4, v0, Lcom/android/launcher2/CellLayout;->mChildren:Lcom/android/launcher2/CellLayoutChildren;

    iget-object v4, v4, Lcom/android/launcher2/CellLayoutChildren;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_9

    .line 1662
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->turnOffEmptyPageMsg()V

    goto/16 :goto_3

    .line 1654
    .end local v37    # "wasPageEmpty":Z
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Lcom/android/launcher2/Workspace;->snapToPage(I)V

    .line 1655
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/android/launcher2/Workspace;->addItem(Lcom/android/launcher2/HomeItem;)V

    goto :goto_6

    .line 1658
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/android/launcher2/Workspace;->addItem(Lcom/android/launcher2/HomeItem;)V

    goto :goto_6

    .line 1660
    :cond_11
    const/16 v37, 0x0

    goto :goto_7

    .line 1672
    .end local v5    # "layout":Lcom/android/launcher2/CellLayout;
    .end local v8    # "realScreen":I
    .end local v14    # "cellXY":[I
    .end local v29    # "foundCellSpan":Z
    .end local v32    # "page":Lcom/android/launcher2/CellLayout;
    .end local v36    # "touchXY":[I
    :cond_12
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/HomeView;->findItemById(J)Lcom/android/launcher2/HomeItem;

    move-result-object v31

    .line 1673
    .local v31, "item":Lcom/android/launcher2/BaseItem;
    if-eqz v31, :cond_2

    move-object/from16 v0, v31

    instance-of v4, v0, Lcom/android/launcher2/HomeFolderItem;

    if-eqz v4, :cond_2

    .line 1674
    check-cast v31, Lcom/android/launcher2/HomeFolderItem;

    .end local v31    # "item":Lcom/android/launcher2/BaseItem;
    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/HomeFolderItem;->addItem(Lcom/android/launcher2/BaseItem;)V

    .line 1675
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v18

    const/16 v25, 0x0

    move-object/from16 v19, p1

    move-wide/from16 v20, p2

    move/from16 v22, p4

    move/from16 v23, p5

    move/from16 v24, p5

    invoke-static/range {v18 .. v25}, Lcom/android/launcher2/LauncherModel;->addItemToDatabase(Landroid/content/Context;Lcom/android/launcher2/HomeItem;JIIIZ)V

    goto/16 :goto_1
.end method

.method createFolder(Lcom/android/launcher2/HomeFolderItem;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "item"    # Lcom/android/launcher2/HomeFolderItem;
    .param p2, "layoutResId"    # I
    .param p3, "parentView"    # Landroid/view/ViewGroup;

    .prologue
    .line 1237
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mPkgResCache:Lcom/android/launcher2/PkgResCache;

    invoke-virtual {v1}, Lcom/android/launcher2/PkgResCache;->getFolderIcon()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p1, Lcom/android/launcher2/HomeFolderItem;->mIconBitmap:Landroid/graphics/Bitmap;

    .line 1238
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mInflater:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/FolderIconView;

    .line 1239
    .local v0, "folder":Lcom/android/launcher2/FolderIconView;
    invoke-virtual {v0, p1}, Lcom/android/launcher2/FolderIconView;->applyBaseItem(Lcom/android/launcher2/BaseItem;)V

    .line 1240
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/FolderIconView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1241
    invoke-virtual {v0}, Lcom/android/launcher2/FolderIconView;->showBadge()V

    .line 1242
    return-object v0
.end method

.method createShortcut(Lcom/android/launcher2/HomeShortcutItem;I)Landroid/view/View;
    .locals 3
    .param p1, "item"    # Lcom/android/launcher2/HomeShortcutItem;
    .param p2, "layoutResId"    # I

    .prologue
    .line 1253
    invoke-static {p1}, Lcom/android/launcher2/AppIconView;->getHomeIconLayout(Lcom/android/launcher2/BaseItem;)I

    move-result v0

    .line 1254
    .local v0, "layout":I
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/launcher2/HomeView;->createShortcut(Lcom/android/launcher2/HomeShortcutItem;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method createShortcut(Lcom/android/launcher2/HomeShortcutItem;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "info"    # Lcom/android/launcher2/HomeShortcutItem;
    .param p2, "layoutResId"    # I
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1268
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v3, p2, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/AppIconView;

    .line 1269
    .local v0, "favorite":Lcom/android/launcher2/AppIconView;
    invoke-virtual {v0, p1}, Lcom/android/launcher2/AppIconView;->applyBaseItem(Lcom/android/launcher2/BaseItem;)V

    .line 1270
    invoke-virtual {v0, p0}, Lcom/android/launcher2/AppIconView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1271
    iget v3, p1, Lcom/android/launcher2/HomeShortcutItem;->mBadgeCount:I

    if-lez v3, :cond_0

    .line 1272
    invoke-virtual {v0}, Lcom/android/launcher2/AppIconView;->showBadge()V

    .line 1274
    :cond_0
    sget-boolean v3, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget-boolean v3, v3, Lcom/android/launcher2/Workspace;->mIsHelpOrientationChanged:Z

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/android/launcher2/HomeView;->mWorkspaceLoading:Z

    if-nez v3, :cond_2

    .line 1275
    sget-object v3, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v4, "addapps"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1276
    sput-boolean v6, Lcom/android/launcher2/guide/GuideFragment;->isGuideCompleted:Z

    .line 1277
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f1000ac

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 1278
    sput-boolean v6, Lcom/android/launcher2/HomeView;->mIsAllAppsButtonDisable:Z

    .line 1279
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 1280
    .local v1, "handler":Landroid/os/Handler;
    new-instance v3, Lcom/android/launcher2/HomeView$7;

    invoke-direct {v3, p0}, Lcom/android/launcher2/HomeView$7;-><init>(Lcom/android/launcher2/HomeView;)V

    const-wide/16 v4, 0x7d0

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1290
    .end local v1    # "handler":Landroid/os/Handler;
    :cond_1
    sget-object v3, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v4, "add_widgets"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1291
    const/4 v2, 0x0

    .line 1292
    .local v2, "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    new-instance v2, Lcom/android/launcher2/guide/AddWidgetsGuider;

    .end local v2    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/launcher2/guide/AddWidgetsGuider;-><init>(Landroid/app/Activity;)V

    .line 1293
    .restart local v2    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    check-cast v2, Lcom/android/launcher2/guide/AddWidgetsGuider;

    .end local v2    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {v2}, Lcom/android/launcher2/guide/AddWidgetsGuider;->dismissShowHelpDialog_step3()V

    .line 1298
    :cond_2
    return-object v0
.end method

.method public deletePage(I)V
    .locals 19
    .param p1, "page"    # I

    .prologue
    .line 4421
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    move/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/android/launcher2/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/CellLayout;

    .line 4422
    .local v2, "cell":Lcom/android/launcher2/CellLayout;
    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getChildrenLayout()Lcom/android/launcher2/CellLayoutChildren;

    move-result-object v9

    .line 4423
    .local v9, "layout":Lcom/android/launcher2/CellLayoutChildren;
    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getPageType()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    const/4 v6, 0x1

    .line 4424
    .local v6, "isSecret":Z
    :goto_0
    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getPageType()I

    move-result v15

    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v15, v0, :cond_1

    const/4 v5, 0x1

    .line 4429
    .local v5, "isFestival":Z
    :goto_1
    invoke-virtual {v9}, Lcom/android/launcher2/CellLayoutChildren;->getChildCount()I

    move-result v15

    add-int/lit8 v4, v15, -0x1

    .local v4, "i":I
    :goto_2
    if-ltz v4, :cond_5

    .line 4430
    invoke-virtual {v9, v4}, Lcom/android/launcher2/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    .line 4432
    .local v12, "view":Landroid/view/View;
    instance-of v15, v12, Lcom/android/launcher2/Folder;

    if-eqz v15, :cond_2

    .line 4429
    :goto_3
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 4423
    .end local v4    # "i":I
    .end local v5    # "isFestival":Z
    .end local v6    # "isSecret":Z
    .end local v12    # "view":Landroid/view/View;
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 4424
    .restart local v6    # "isSecret":Z
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 4440
    .restart local v4    # "i":I
    .restart local v5    # "isFestival":Z
    .restart local v12    # "view":Landroid/view/View;
    :cond_2
    invoke-virtual {v12}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/launcher2/HomeItem;

    .line 4442
    .local v7, "item":Lcom/android/launcher2/HomeItem;
    instance-of v15, v7, Lcom/android/launcher2/HomeWidgetItem;

    if-eqz v15, :cond_4

    move-object v13, v7

    .line 4443
    check-cast v13, Lcom/android/launcher2/HomeWidgetItem;

    .line 4444
    .local v13, "widgetItem":Lcom/android/launcher2/HomeWidgetItem;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    .line 4445
    .local v1, "appWidgetHost":Lcom/android/launcher2/LauncherAppWidgetHost;
    if-eqz v1, :cond_3

    .line 4446
    iget v15, v13, Lcom/android/launcher2/HomeWidgetItem;->appWidgetId:I

    invoke-virtual {v1, v15}, Lcom/android/launcher2/LauncherAppWidgetHost;->deleteAppWidgetId(I)V

    .line 4453
    .end local v1    # "appWidgetHost":Lcom/android/launcher2/LauncherAppWidgetHost;
    .end local v13    # "widgetItem":Lcom/android/launcher2/HomeWidgetItem;
    :cond_3
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v15

    invoke-static {v15, v7}, Lcom/android/launcher2/LauncherModel;->deleteItemFromDatabase(Landroid/content/Context;Lcom/android/launcher2/HomeItem;)V

    goto :goto_3

    .line 4448
    :cond_4
    instance-of v15, v7, Lcom/android/launcher2/SurfaceWidgetItem;

    if-eqz v15, :cond_3

    .line 4449
    const-string v15, "SurfaceWidgetFlow"

    const-string v16, "destroying surface widget with delete page"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v15, v7

    .line 4450
    check-cast v15, Lcom/android/launcher2/SurfaceWidgetItem;

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v16

    sget-object v17, Lcom/android/launcher2/SurfaceWidgetPackageManager;->INST:Lcom/android/launcher2/SurfaceWidgetPackageManager;

    const/16 v18, 0x1

    invoke-virtual/range {v15 .. v18}, Lcom/android/launcher2/SurfaceWidgetItem;->destroyWidget(Landroid/content/Context;Lcom/android/launcher2/SurfaceWidgetPackageManager;Z)V

    goto :goto_4

    .line 4457
    .end local v7    # "item":Lcom/android/launcher2/HomeItem;
    .end local v12    # "view":Landroid/view/View;
    :cond_5
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 4458
    .local v10, "toSave":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/launcher2/HomeItem;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v15}, Lcom/android/launcher2/Workspace;->getPageCount()I

    move-result v3

    .line 4460
    .local v3, "count":I
    const/4 v4, 0x0

    :goto_5
    if-ge v4, v3, :cond_7

    .line 4461
    move/from16 v0, p1

    if-gt v4, v0, :cond_6

    .line 4460
    :goto_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 4465
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v15, v4}, Lcom/android/launcher2/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v15}, Lcom/android/launcher2/CellLayout;->getChildrenLayout()Lcom/android/launcher2/CellLayoutChildren;

    move-result-object v8

    .line 4466
    .local v8, "l":Lcom/android/launcher2/CellLayoutChildren;
    add-int/lit8 v15, v4, -0x1

    invoke-virtual {v8, v15, v10}, Lcom/android/launcher2/CellLayoutChildren;->updateChildrenToNewPage(ILjava/util/List;)V

    goto :goto_6

    .line 4468
    .end local v8    # "l":Lcom/android/launcher2/CellLayoutChildren;
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v15

    invoke-static {v15, v10}, Lcom/android/launcher2/LauncherModel;->moveItemsInDatabase(Landroid/content/Context;Ljava/util/List;)V

    .line 4470
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    move/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 4471
    .local v11, "v":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v15, v11}, Lcom/android/launcher2/Workspace;->removeView(Landroid/view/View;)V

    .line 4472
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v15}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v15

    move/from16 v0, p1

    invoke-static {v0, v15}, Lcom/android/launcher2/WorkspacePages;->deletePageAt(ILandroid/content/Context;)Z

    .line 4474
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v15}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v14

    .line 4475
    .local v14, "workScreenCount":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v15}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v15

    if-gt v14, v15, :cond_8

    .line 4476
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    add-int/lit8 v16, v14, -0x1

    invoke-virtual/range {v15 .. v16}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    .line 4479
    :cond_8
    if-eqz v6, :cond_9

    .line 4480
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->saveSecretScreenInfo()V

    .line 4485
    :goto_7
    return-void

    .line 4481
    :cond_9
    if-eqz v5, :cond_a

    .line 4482
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->saveFestivalScreenInfo()V

    goto :goto_7

    .line 4484
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/HomeView;->saveScreenInfo()V

    goto :goto_7
.end method

.method public deleteWidgetFestivalPage(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/launcher2/HomeWidgetItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4626
    .local p1, "widgets":Ljava/util/List;, "Ljava/util/List<Lcom/android/launcher2/HomeWidgetItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/HomeWidgetItem;

    .line 4627
    .local v1, "widgetItem":Lcom/android/launcher2/HomeWidgetItem;
    iget v2, v1, Lcom/android/launcher2/HomeWidgetItem;->mFestivalType:I

    if-lez v2, :cond_0

    .line 4628
    const-string v2, "Launcher.HomeView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteWidgetIFestivalPage appWidgetId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/android/launcher2/HomeWidgetItem;->appWidgetId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4629
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    if-eqz v2, :cond_0

    .line 4630
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    iget v3, v1, Lcom/android/launcher2/HomeWidgetItem;->appWidgetId:I

    invoke-virtual {v2, v3}, Lcom/android/launcher2/LauncherAppWidgetHost;->deleteAppWidgetId(I)V

    .line 4631
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/android/launcher2/LauncherModel;->deleteItemFromDatabase(Landroid/content/Context;Lcom/android/launcher2/HomeItem;)V

    goto :goto_0

    .line 4635
    .end local v1    # "widgetItem":Lcom/android/launcher2/HomeWidgetItem;
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->saveFestivalScreenInfo()V

    .line 4636
    return-void
.end method

.method public determineEmptyPageMsgVisibility(I)Z
    .locals 3
    .param p1, "pageIndex"    # I

    .prologue
    .line 4788
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->getShowEmptyPageMessagePref()Z

    move-result v0

    .line 4791
    .local v0, "allowEmptyMessage":Z
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/android/launcher2/WorkspacePages;->pageHasAlwaysBeenEmpty(ILandroid/content/Context;)Z

    move-result v1

    .line 4793
    .local v1, "isPageEmpty":Z
    and-int/2addr v0, v1

    .line 4794
    return v0
.end method

.method public dispatchDragEvent(Landroid/view/DragEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5257
    invoke-static {p1}, Lcom/android/launcher2/Launcher;->isInValidDragState(Landroid/view/DragEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    .line 5268
    :goto_0
    return v1

    .line 5258
    :cond_0
    invoke-virtual {p1}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/DragState;

    move-object v0, v1

    .line 5261
    .local v0, "dragState":Lcom/android/launcher2/DragState;
    :goto_1
    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_3

    .line 5262
    invoke-virtual {v0}, Lcom/android/launcher2/DragState;->getShadow()Lcom/android/launcher2/ShadowBuilder;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mShadow:Lcom/android/launcher2/ShadowBuilder;

    .line 5263
    sput-boolean v3, Lcom/android/launcher2/HomeView;->sIsDragState:Z

    .line 5268
    :cond_1
    :goto_2
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    move-result v1

    goto :goto_0

    .line 5258
    .end local v0    # "dragState":Lcom/android/launcher2/DragState;
    :cond_2
    sget-object v0, Lcom/android/launcher2/Launcher;->dragstate:Lcom/android/launcher2/DragState;

    goto :goto_1

    .line 5264
    .restart local v0    # "dragState":Lcom/android/launcher2/DragState;
    :cond_3
    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    move-result v1

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 5265
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mShadow:Lcom/android/launcher2/ShadowBuilder;

    .line 5266
    sput-boolean v2, Lcom/android/launcher2/HomeView;->sIsDragState:Z

    goto :goto_2
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 5190
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "fd"    # Ljava/io/FileDescriptor;
    .param p3, "writer"    # Ljava/io/PrintWriter;
    .param p4, "args"    # [Ljava/lang/String;

    .prologue
    .line 4164
    const-string v1, " "

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 4165
    const-string v1, "Debug logs: "

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 4166
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/android/launcher2/HomeView;->sDumpLogs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 4167
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v1, Lcom/android/launcher2/HomeView;->sDumpLogs:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 4166
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4169
    :cond_0
    return-void
.end method

.method public dumpState()V
    .locals 3

    .prologue
    .line 4149
    sget-boolean v0, Lcom/android/launcher2/HomeView;->DEBUGGABLE:Z

    if-eqz v0, :cond_0

    .line 4150
    const-string v0, "Launcher.HomeView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BEGIN launcher2 dump state for launcher "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4151
    const-string v0, "Launcher.HomeView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSavedState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mSavedState:Lcom/android/launcher2/HomeView$SavedState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4152
    const-string v0, "Launcher.HomeView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mWorkspaceLoading="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/launcher2/HomeView;->mWorkspaceLoading:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4153
    const-string v0, "Launcher.HomeView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mRestoring="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/launcher2/HomeView;->mRestoring:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4154
    const-string v0, "Launcher.HomeView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mWaitingForResult="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/launcher2/HomeView;->mWaitingForResult:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4155
    const-string v0, "Launcher.HomeView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sFolders.size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/launcher2/HomeView;->sFolders:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4156
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mModel:Lcom/android/launcher2/LauncherModel;

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherModel;->dumpState()V

    .line 4158
    const-string v0, "Launcher.HomeView"

    const-string v1, "END launcher2 dump state"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4160
    :cond_0
    return-void
.end method

.method public exitHomeOptionMenu()V
    .locals 1

    .prologue
    .line 5403
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/launcher2/HomeView;->setVisibilityHomeOptionMenu(I)V

    .line 5404
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/HomeView;->setVisibilityHotseat(I)V

    .line 5405
    return-void
.end method

.method public findItemById(J)Lcom/android/launcher2/HomeItem;
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 4761
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 4762
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4, v0}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v4}, Lcom/android/launcher2/CellLayout;->getChildrenLayout()Lcom/android/launcher2/CellLayoutChildren;

    move-result-object v2

    .line 4763
    .local v2, "page":Lcom/android/launcher2/CellLayoutChildren;
    iget-object v4, v2, Lcom/android/launcher2/CellLayoutChildren;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/BaseItem;

    .line 4764
    .local v3, "pageItem":Lcom/android/launcher2/BaseItem;
    iget-wide v4, v3, Lcom/android/launcher2/BaseItem;->mId:J

    cmp-long v4, v4, p1

    if-nez v4, :cond_0

    .line 4765
    check-cast v3, Lcom/android/launcher2/HomeItem;

    .line 4775
    .end local v2    # "page":Lcom/android/launcher2/CellLayoutChildren;
    .end local v3    # "pageItem":Lcom/android/launcher2/BaseItem;
    :goto_1
    return-object v3

    .line 4761
    .restart local v2    # "page":Lcom/android/launcher2/CellLayoutChildren;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4770
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "page":Lcom/android/launcher2/CellLayoutChildren;
    :cond_2
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v4}, Lcom/android/launcher2/Hotseat;->getLayout()Lcom/android/launcher2/CellLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/launcher2/CellLayout;->getChildrenLayout()Lcom/android/launcher2/CellLayoutChildren;

    move-result-object v4

    iget-object v4, v4, Lcom/android/launcher2/CellLayoutChildren;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/BaseItem;

    .line 4771
    .restart local v3    # "pageItem":Lcom/android/launcher2/BaseItem;
    iget-wide v4, v3, Lcom/android/launcher2/BaseItem;->mId:J

    cmp-long v4, v4, p1

    if-nez v4, :cond_3

    .line 4772
    check-cast v3, Lcom/android/launcher2/HomeItem;

    goto :goto_1

    .line 4775
    .end local v3    # "pageItem":Lcom/android/launcher2/BaseItem;
    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public finishBindingItems()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 3580
    iget-object v6, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    if-nez v6, :cond_1

    .line 3622
    :cond_0
    :goto_0
    return-void

    .line 3584
    :cond_1
    iget-object v6, p0, Lcom/android/launcher2/HomeView;->mSavedState:Lcom/android/launcher2/HomeView$SavedState;

    if-eqz v6, :cond_3

    .line 3585
    iget-object v6, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v6}, Lcom/android/launcher2/Workspace;->hasFocus()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v6}, Lcom/android/launcher2/Workspace;->isShown()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v6}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v6

    if-nez v6, :cond_2

    .line 3586
    iget-object v6, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget-object v7, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v7}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3587
    .local v1, "currentPage":Landroid/view/View;
    if-eqz v1, :cond_2

    .line 3588
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 3591
    .end local v1    # "currentPage":Landroid/view/View;
    :cond_2
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/android/launcher2/HomeView;->mSavedState:Lcom/android/launcher2/HomeView$SavedState;

    .line 3594
    :cond_3
    iput-boolean v8, p0, Lcom/android/launcher2/HomeView;->mWorkspaceLoading:Z

    .line 3598
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    sget-object v6, Lcom/android/launcher2/HomeView;->sPendingAddList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_4

    .line 3599
    sget-object v6, Lcom/android/launcher2/HomeView;->sPendingAddList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/launcher2/HomeView$PendingAddArguments;

    invoke-direct {p0, v6}, Lcom/android/launcher2/HomeView;->completeAdd(Lcom/android/launcher2/HomeView$PendingAddArguments;)Z

    .line 3598
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 3601
    :cond_4
    sget-object v6, Lcom/android/launcher2/HomeView;->sPendingAddList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 3603
    iget-object v6, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v6}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 3604
    iget-object v6, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v6}, Lcom/android/launcher2/QuickViewWorkspace;->invalidate()V

    .line 3606
    :cond_5
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v6

    check-cast v6, Lcom/android/launcher2/Launcher;

    invoke-virtual {v6}, Lcom/android/launcher2/Launcher;->addAnyPendingWidgetsToWorkspace()V

    .line 3609
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "skt_phone20_settings"

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 3610
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    .line 3611
    .local v0, "app":Lcom/android/launcher2/LauncherApplication;
    const-string v6, "com.sec.android.app.launcher.prefs"

    invoke-virtual {v0, v6, v8}, Lcom/android/launcher2/LauncherApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 3612
    .local v5, "prefs":Landroid/content/SharedPreferences;
    const-string v6, "PrefsIsCSCLoad"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 3614
    .local v4, "isCscLoad":Z
    if-eqz v4, :cond_0

    .line 3615
    const-string v6, "ChangeTphoneMode"

    const-string v7, "T phone and isCscLoad = true"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3616
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->ChangeTphoneMode()V

    .line 3617
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 3618
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v6, "PrefsIsCSCLoad"

    invoke-interface {v2, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 3619
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 5050
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    return-object v0
.end method

.method getAllAppsIcon()Landroid/view/View;
    .locals 1

    .prologue
    .line 2975
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAppWidgetHost()Lcom/android/launcher2/LauncherAppWidgetHost;
    .locals 1

    .prologue
    .line 1988
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    return-object v0
.end method

.method getCellLayout(JI)Lcom/android/launcher2/CellLayout;
    .locals 3
    .param p1, "container"    # J
    .param p3, "screen"    # I

    .prologue
    .line 2997
    const-wide/16 v0, -0x65

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 2998
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_0

    .line 2999
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->getLayout()Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 3004
    :goto_0
    return-object v0

    .line 3001
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3004
    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, p3}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    goto :goto_0
.end method

.method public getCreateFolderColor(I)V
    .locals 0
    .param p1, "createFolderColor"    # I

    .prologue
    .line 4668
    iput p1, p0, Lcom/android/launcher2/HomeView;->mCreateFolderColor:I

    .line 4669
    return-void
.end method

.method public getCurrentDragItem()Lcom/android/launcher2/BaseItem;
    .locals 1

    .prologue
    .line 4656
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mCurrentDragItem:Lcom/android/launcher2/BaseItem;

    return-object v0
.end method

.method public getCurrentPage()I
    .locals 1

    .prologue
    .line 4202
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    if-eqz v0, :cond_0

    .line 4203
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v0

    .line 4205
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getCurrentResizeWidgetItem()Lcom/android/launcher2/BaseItem;
    .locals 1

    .prologue
    .line 4648
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mCurrentResizeWidgetItem:Lcom/android/launcher2/BaseItem;

    return-object v0
.end method

.method public getDarkenView()Landroid/view/View;
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mDarkenView:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->mAttached:Z

    if-eqz v0, :cond_0

    .line 243
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f008f

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/HomeView;->mDarkenView:Landroid/view/View;

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mDarkenView:Landroid/view/View;

    return-object v0
.end method

.method public getDeleteDropLayout()Lcom/android/launcher2/QuickViewDragBar;
    .locals 1

    .prologue
    .line 4323
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mDeleteDropLayout:Lcom/android/launcher2/QuickViewDragBar;

    return-object v0
.end method

.method getEditBar()Lcom/android/launcher2/HomeEditBar;
    .locals 1

    .prologue
    .line 2924
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mEditBar:Lcom/android/launcher2/HomeEditBar;

    return-object v0
.end method

.method public getGoogleSearchView()Landroid/view/View;
    .locals 1

    .prologue
    .line 2928
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mGoogleSearchView:Landroid/view/View;

    return-object v0
.end method

.method getHomeAppsBtn()Landroid/view/View;
    .locals 1

    .prologue
    .line 2948
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeAppsBtn:Landroid/view/View;

    return-object v0
.end method

.method public getHomeBottomBar()Landroid/view/View;
    .locals 1

    .prologue
    .line 5408
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    return-object v0
.end method

.method getHomeContainer()Landroid/view/View;
    .locals 1

    .prologue
    .line 2986
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeContainer:Landroid/view/View;

    return-object v0
.end method

.method getHomeDarkenLayer()Landroid/view/View;
    .locals 1

    .prologue
    .line 2983
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeDarkenLayer:Landroid/view/View;

    return-object v0
.end method

.method getHomeEditBtn()Landroid/view/View;
    .locals 1

    .prologue
    .line 2940
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mEditIcon:Landroid/view/View;

    return-object v0
.end method

.method getHomeEditTitleBar()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 2990
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeEditTitleBar:Landroid/view/ViewGroup;

    return-object v0
.end method

.method getHomePhoneBtn()Landroid/view/View;
    .locals 1

    .prologue
    .line 2944
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomePhoneBtn:Landroid/view/View;

    return-object v0
.end method

.method public getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;
    .locals 1

    .prologue
    .line 4327
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeScreenOptionMenu:Lcom/android/launcher2/HomeScreenOptionMenu;

    if-nez v0, :cond_0

    .line 4328
    const v0, 0x7f0f00c9

    invoke-virtual {p0, v0}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/HomeScreenOptionMenu;

    iput-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeScreenOptionMenu:Lcom/android/launcher2/HomeScreenOptionMenu;

    .line 4329
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeScreenOptionMenu:Lcom/android/launcher2/HomeScreenOptionMenu;

    return-object v0
.end method

.method getHotseat()Lcom/android/launcher2/Hotseat;
    .locals 1

    .prologue
    .line 2935
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    return-object v0
.end method

.method public getHotseatItemcount()I
    .locals 1

    .prologue
    .line 4993
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_0

    .line 4994
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    iget-object v0, v0, Lcom/android/launcher2/Hotseat;->mContent:Lcom/android/launcher2/CellLayoutHotseat;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayoutHotseat;->getChildrenLayout()Lcom/android/launcher2/CellLayoutChildren;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayoutChildren;->getChildCount()I

    move-result v0

    .line 4995
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLauncherModel()Lcom/android/launcher2/LauncherModel;
    .locals 1

    .prologue
    .line 4640
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mModel:Lcom/android/launcher2/LauncherModel;

    return-object v0
.end method

.method public getModel()Lcom/android/launcher2/LauncherModel;
    .locals 1

    .prologue
    .line 1992
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mModel:Lcom/android/launcher2/LauncherModel;

    return-object v0
.end method

.method public getNumFestivalPages()I
    .locals 1

    .prologue
    .line 4235
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getFestivalScreenCount()I

    move-result v0

    return v0
.end method

.method public getNumPages()I
    .locals 1

    .prologue
    .line 4225
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getNumSecretPages()I
    .locals 1

    .prologue
    .line 4230
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getSecretScreenCount()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getPage(I)Landroid/view/View;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 118
    invoke-virtual {p0, p1}, Lcom/android/launcher2/HomeView;->getPage(I)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    return-object v0
.end method

.method public getPage(I)Lcom/android/launcher2/CellLayout;
    .locals 1
    .param p1, "pageNum"    # I

    .prologue
    .line 4220
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    return-object v0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 4347
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    if-nez v0, :cond_0

    .line 4348
    const/4 v0, 0x0

    .line 4351
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    goto :goto_0
.end method

.method public getPageHorizontalOffset()I
    .locals 4

    .prologue
    .line 4286
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getWidth()I

    move-result v1

    .line 4287
    .local v1, "w":I
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 4288
    .local v0, "s":I
    sub-int v2, v1, v0

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getPageSpacing()I

    move-result v3

    add-int/2addr v2, v3

    return v2
.end method

.method public getPagesLocationOnScreen([I)V
    .locals 1
    .param p1, "location"    # [I

    .prologue
    .line 4281
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/Workspace;->getLocationOnScreen([I)V

    .line 4282
    return-void
.end method

.method public getPagesTop()I
    .locals 2

    .prologue
    .line 4293
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    return v0
.end method

.method public getPanelBackgroundAlpha()F
    .locals 1

    .prologue
    .line 5278
    iget v0, p0, Lcom/android/launcher2/HomeView;->mPanelBackgroundAlpha:F

    return v0
.end method

.method public getPkgResCache()Lcom/android/launcher2/PkgResCache;
    .locals 1

    .prologue
    .line 4757
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mPkgResCache:Lcom/android/launcher2/PkgResCache;

    return-object v0
.end method

.method getQuickLaunch()Lcom/android/launcher2/QuickLaunch;
    .locals 1

    .prologue
    .line 2952
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    return-object v0
.end method

.method getQuickLaunchCamera()Lcom/android/launcher2/QuickLaunch;
    .locals 1

    .prologue
    .line 2956
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    return-object v0
.end method

.method getQuickViewWorkspace()Lcom/android/launcher2/QuickViewWorkspace;
    .locals 1

    .prologue
    .line 4008
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    return-object v0
.end method

.method getTopBar()Landroid/view/View;
    .locals 1

    .prologue
    .line 2979
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeTopBar:Landroid/view/View;

    return-object v0
.end method

.method getWorkspace()Lcom/android/launcher2/Workspace;
    .locals 1

    .prologue
    .line 3009
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    return-object v0
.end method

.method public getWorkspaceLoading()Z
    .locals 1

    .prologue
    .line 4529
    iget-boolean v0, p0, Lcom/android/launcher2/HomeView;->mWorkspaceLoading:Z

    return v0
.end method

.method public goHomeOptionMenu()V
    .locals 1

    .prologue
    .line 5397
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/HomeView;->showWorkspaceEditmode(Z)V

    .line 5398
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/HomeView;->setVisibilityHomeOptionMenu(I)V

    .line 5399
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/launcher2/HomeView;->setVisibilityHotseat(I)V

    .line 5400
    return-void
.end method

.method public handleResizeWidget()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 5338
    const/4 v1, 0x0

    .line 5339
    .local v1, "isHandled":Z
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v2

    .line 5340
    .local v2, "workspace":Lcom/android/launcher2/Workspace;
    sget-boolean v3, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getState()Lcom/android/launcher2/Workspace$State;

    move-result-object v3

    sget-object v4, Lcom/android/launcher2/Workspace$State;->RESIZE:Lcom/android/launcher2/Workspace$State;

    if-ne v3, v4, :cond_1

    sget v3, Lcom/android/launcher2/guide/GuideFragment;->sHelpHubStepNumber:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    sget-boolean v3, Lcom/android/launcher2/Workspace;->widgetSizeChanged:Z

    if-eqz v3, :cond_1

    .line 5342
    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->exitWidgetResizeMode()V

    .line 5343
    const-string v3, "resize_widgets"

    sget-object v4, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5344
    sput-boolean v5, Lcom/android/launcher2/guide/GuideFragment;->isGuideCompleted:Z

    .line 5345
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f1000c2

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 5346
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 5347
    .local v0, "handler":Landroid/os/Handler;
    new-instance v3, Lcom/android/launcher2/HomeView$27;

    invoke-direct {v3, p0}, Lcom/android/launcher2/HomeView$27;-><init>(Lcom/android/launcher2/HomeView;)V

    const-wide/16 v4, 0x7d0

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 5361
    .end local v0    # "handler":Landroid/os/Handler;
    :cond_0
    const/4 v1, 0x1

    .line 5363
    :cond_1
    return v1
.end method

.method public helpHubSnapToLastPage()V
    .locals 2

    .prologue
    .line 5330
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getNumPages()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    .line 5331
    return-void
.end method

.method public hideHomeBottomBar()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 5420
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 5421
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 5422
    :cond_0
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isTabletLayout()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5423
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 5424
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Hotseat;->setVisibility(I)V

    .line 5425
    :cond_1
    return-void
.end method

.method hideHotseat(Z)V
    .locals 4
    .param p1, "animated"    # Z

    .prologue
    const/4 v1, 0x0

    .line 3075
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_0

    .line 3076
    if-eqz p1, :cond_1

    .line 3077
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xaf

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 3082
    :cond_0
    :goto_0
    return-void

    .line 3079
    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Hotseat;->setAlpha(F)V

    goto :goto_0
.end method

.method public isDeleteWorkScreenPopup()Z
    .locals 1

    .prologue
    .line 4098
    iget-boolean v0, p0, Lcom/android/launcher2/HomeView;->mIsDeletePopup:Z

    return v0
.end method

.method public isFolderPage(I)Z
    .locals 1
    .param p1, "pageNum"    # I

    .prologue
    .line 4210
    const/4 v0, 0x0

    return v0
.end method

.method isHotseatLayout(Landroid/view/View;)Z
    .locals 1
    .param p1, "layout"    # Landroid/view/View;

    .prologue
    .line 2919
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->getLayout()Lcom/android/launcher2/CellLayout;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isIncludeItem(I)Z
    .locals 3
    .param p1, "page"    # I

    .prologue
    const/4 v2, 0x0

    .line 4341
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1, p1}, Lcom/android/launcher2/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/CellLayout;->getChildrenLayout()Lcom/android/launcher2/CellLayoutChildren;

    move-result-object v0

    .line 4342
    .local v0, "layout":Lcom/android/launcher2/CellLayoutChildren;
    if-nez v0, :cond_0

    .line 4343
    :goto_0
    return v2

    :cond_0
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayoutChildren;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 5059
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVisibleHomeOptionMenu()Z
    .locals 2

    .prologue
    .line 5389
    const v1, 0x7f0f00c9

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 5390
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    .line 5391
    const/4 v1, 0x0

    .line 5393
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isWorkspaceLocked()Z
    .locals 1

    .prologue
    .line 2021
    iget-boolean v0, p0, Lcom/android/launcher2/HomeView;->mWorkspaceLoading:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/launcher2/HomeView;->mWaitingForResult:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public movePage(II)V
    .locals 8
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I

    .prologue
    .line 4240
    if-ne p1, p2, :cond_0

    .line 4277
    :goto_0
    return-void

    .line 4242
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {p1, p2, v7}, Lcom/android/launcher2/WorkspacePages;->movePage(IILandroid/content/Context;)Z

    .line 4243
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getHomeScreenIndex()I

    move-result v1

    .line 4245
    .local v1, "homeIndex":I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 4246
    .local v6, "toSave":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/launcher2/HomeItem;>;"
    iget-object v7, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v7}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    .line 4248
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_4

    .line 4249
    if-ne v2, p1, :cond_2

    move v5, p2

    .line 4254
    .local v5, "newi":I
    :goto_2
    iget-object v7, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v7, v2}, Lcom/android/launcher2/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v7}, Lcom/android/launcher2/CellLayout;->removeChildrenLayout()Lcom/android/launcher2/CellLayoutChildren;

    move-result-object v3

    .line 4256
    .local v3, "l":Lcom/android/launcher2/CellLayoutChildren;
    iget-object v7, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v7, v5}, Lcom/android/launcher2/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/CellLayout;

    .line 4257
    .local v4, "layout":Lcom/android/launcher2/CellLayout;
    const/4 v7, 0x0

    invoke-virtual {v4, v3, v7}, Lcom/android/launcher2/CellLayout;->addChildrenLayout(Lcom/android/launcher2/CellLayoutChildren;I)V

    .line 4259
    invoke-virtual {v3, v5, v6}, Lcom/android/launcher2/CellLayoutChildren;->updateChildrenToNewPage(ILjava/util/List;)V

    .line 4261
    if-ltz v1, :cond_1

    if-ne v2, v1, :cond_1

    .line 4265
    invoke-virtual {p0, v5}, Lcom/android/launcher2/HomeView;->setHomeScreenAt(I)V

    .line 4266
    const/4 v1, -0x1

    .line 4248
    .end local v3    # "l":Lcom/android/launcher2/CellLayoutChildren;
    .end local v4    # "layout":Lcom/android/launcher2/CellLayout;
    .end local v5    # "newi":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4250
    :cond_2
    if-le v2, p1, :cond_3

    if-gt v2, p2, :cond_3

    add-int/lit8 v5, v2, -0x1

    .restart local v5    # "newi":I
    goto :goto_2

    .line 4251
    .end local v5    # "newi":I
    :cond_3
    if-ge v2, p1, :cond_1

    if-lt v2, p2, :cond_1

    add-int/lit8 v5, v2, 0x1

    .restart local v5    # "newi":I
    goto :goto_2

    .line 4272
    .end local v5    # "newi":I
    :cond_4
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v0, :cond_5

    .line 4273
    iget-object v7, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v7, v2}, Lcom/android/launcher2/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v7}, Lcom/android/launcher2/CellLayout;->reset()V

    .line 4272
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 4276
    :cond_5
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7, v6}, Lcom/android/launcher2/LauncherModel;->moveItemsInDatabase(Landroid/content/Context;Ljava/util/List;)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 10
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 5000
    iput-boolean v9, p0, Lcom/android/launcher2/HomeView;->mWaitingForResult:Z

    .line 5008
    if-ne p2, v8, :cond_2

    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-wide v4, v4, Lcom/android/launcher2/HomeItem;->container:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    .line 5009
    new-instance v2, Lcom/android/launcher2/HomeView$PendingAddArguments;

    const/4 v4, 0x0

    invoke-direct {v2, v4}, Lcom/android/launcher2/HomeView$PendingAddArguments;-><init>(Lcom/android/launcher2/HomeView$1;)V

    .line 5010
    .local v2, "args":Lcom/android/launcher2/HomeView$PendingAddArguments;
    iput p1, v2, Lcom/android/launcher2/HomeView$PendingAddArguments;->requestCode:I

    .line 5011
    iput-object p3, v2, Lcom/android/launcher2/HomeView$PendingAddArguments;->intent:Landroid/content/Intent;

    .line 5012
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    invoke-static {v2, v4}, Lcom/android/launcher2/HomeView;->copyHomeItemToPendingAdd(Lcom/android/launcher2/HomeView$PendingAddArguments;Lcom/android/launcher2/HomeItem;)V

    .line 5014
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->isWorkspaceLocked()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 5015
    sget-object v4, Lcom/android/launcher2/HomeView;->sPendingAddList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5036
    .end local v2    # "args":Lcom/android/launcher2/HomeView$PendingAddArguments;
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    .line 5037
    .local v0, "app":Lcom/android/launcher2/LauncherApplication;
    const-string v4, "com.sec.android.app.launcher.prefs"

    invoke-virtual {v0, v4, v9}, Lcom/android/launcher2/LauncherApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 5038
    .local v3, "prefs":Landroid/content/SharedPreferences$Editor;
    const-string v4, "tempScreen"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 5039
    const-string v4, "tempCellX"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 5040
    const-string v4, "tempCellY"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 5041
    const-string v4, "tempSpanX"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 5042
    const-string v4, "tempSpanY"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 5043
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 5045
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/Launcher;

    invoke-virtual {v4}, Lcom/android/launcher2/Launcher;->addAnyPendingWidgetsToWorkspace()V

    .line 5046
    return-void

    .line 5017
    .end local v0    # "app":Lcom/android/launcher2/LauncherApplication;
    .end local v3    # "prefs":Landroid/content/SharedPreferences$Editor;
    .restart local v2    # "args":Lcom/android/launcher2/HomeView$PendingAddArguments;
    :cond_1
    invoke-direct {p0, v2}, Lcom/android/launcher2/HomeView;->completeAdd(Lcom/android/launcher2/HomeView$PendingAddArguments;)Z

    goto :goto_0

    .line 5019
    .end local v2    # "args":Lcom/android/launcher2/HomeView$PendingAddArguments;
    :cond_2
    const/16 v4, 0x9

    if-eq p1, v4, :cond_3

    const/4 v4, 0x5

    if-ne p1, v4, :cond_4

    :cond_3
    if-nez p2, :cond_4

    .line 5021
    if-eqz p3, :cond_0

    .line 5023
    const-string v4, "appWidgetId"

    invoke-virtual {p3, v4, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 5024
    .local v1, "appWidgetId":I
    if-eq v1, v8, :cond_0

    .line 5025
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    invoke-virtual {v4, v1}, Lcom/android/launcher2/LauncherAppWidgetHost;->deleteAppWidgetId(I)V

    goto :goto_0

    .line 5028
    .end local v1    # "appWidgetId":I
    :cond_4
    const/16 v4, 0xa

    if-ne p1, v4, :cond_0

    .line 5029
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v4}, Lcom/android/launcher2/QuickViewWorkspace;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 5030
    if-ne p2, v8, :cond_0

    .line 5031
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/android/launcher2/HomeView;->closeQuickViewWorkspace(Z)V

    goto :goto_0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 4137
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 4129
    const/4 v1, 0x0

    sget-object v2, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, v1, v2}, Lcom/android/launcher2/HomeView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 4130
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v0

    .line 4131
    .local v0, "ws":Lcom/android/launcher2/Workspace;
    if-eqz v0, :cond_0

    .line 4132
    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->onFadeEnd()V

    .line 4133
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 4141
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 4125
    const/4 v0, 0x2

    sget-object v1, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/HomeView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 4126
    return-void
.end method

.method public onBackPressed()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 715
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v1

    .line 725
    .local v1, "workspace":Lcom/android/launcher2/Workspace;
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v2}, Lcom/android/launcher2/QuickViewWorkspace;->drawCloseAnimation()Z

    .line 728
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 729
    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v0

    .line 731
    .local v0, "openFolder":Lcom/android/launcher2/Folder;
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->isEditingName()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 732
    invoke-virtual {v0, v3}, Lcom/android/launcher2/Folder;->dismissEditingName(Z)V

    .line 749
    .end local v0    # "openFolder":Lcom/android/launcher2/Folder;
    :cond_0
    :goto_0
    return v3

    .line 734
    .restart local v0    # "openFolder":Lcom/android/launcher2/Folder;
    :cond_1
    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->closeFolder()V

    goto :goto_0

    .line 737
    .end local v0    # "openFolder":Lcom/android/launcher2/Folder;
    :cond_2
    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->exitWidgetResizeMode()V

    .line 740
    sget-boolean v2, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v2, :cond_0

    .line 741
    invoke-static {v4}, Lcom/android/launcher2/Launcher;->setHomeRemoveMode(Z)V

    .line 742
    invoke-static {}, Lcom/android/launcher2/Launcher;->isHomeEditMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 743
    invoke-static {v4}, Lcom/android/launcher2/Launcher;->setHomeEditMode(Z)V

    .line 744
    invoke-virtual {p0, v3}, Lcom/android/launcher2/HomeView;->showWorkspace(Z)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 2518
    sget-boolean v5, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v5, :cond_2

    .line 2519
    instance-of v5, p1, Lcom/android/launcher2/CellLayout;

    if-nez v5, :cond_0

    .line 2520
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    .end local p1    # "v":Landroid/view/View;
    check-cast p1, Landroid/view/View;

    .line 2523
    .restart local p1    # "v":Landroid/view/View;
    :cond_0
    instance-of v5, p1, Lcom/android/launcher2/CellLayout;

    if-eqz v5, :cond_2

    move-object v0, p1

    .line 2525
    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 2526
    .local v0, "cl":Lcom/android/launcher2/CellLayout;
    if-eqz v0, :cond_2

    .line 2528
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getContainer()Lcom/android/launcher2/CellLayoutContainer;

    move-result-object v2

    .line 2529
    .local v2, "parent":Lcom/android/launcher2/CellLayoutContainer;
    instance-of v5, v2, Lcom/android/launcher2/Hotseat;

    if-eqz v5, :cond_2

    .line 2578
    .end local v0    # "cl":Lcom/android/launcher2/CellLayout;
    .end local v2    # "parent":Lcom/android/launcher2/CellLayoutContainer;
    :cond_1
    :goto_0
    return-void

    .line 2539
    :cond_2
    sget-boolean v5, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v5, :cond_3

    .line 2540
    invoke-static {}, Lcom/android/launcher2/Launcher;->isHomeEditMode()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2546
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 2550
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5}, Lcom/android/launcher2/Workspace;->isSwitchingState()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2554
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->cancelClicksOnHome()V

    .line 2555
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    .line 2556
    .local v4, "tag":Ljava/lang/Object;
    instance-of v5, v4, Lcom/android/launcher2/HomeShortcutItem;

    if-eqz v5, :cond_4

    move-object v5, v4

    .line 2564
    check-cast v5, Lcom/android/launcher2/HomeShortcutItem;

    iget-object v1, v5, Lcom/android/launcher2/HomeShortcutItem;->intent:Landroid/content/Intent;

    .line 2566
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_1

    .line 2567
    const/4 v5, 0x2

    new-array v3, v5, [I

    .line 2568
    .local v3, "pos":[I
    invoke-virtual {p1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2569
    new-instance v5, Landroid/graphics/Rect;

    aget v6, v3, v8

    aget v7, v3, v10

    aget v8, v3, v8

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    aget v9, v3, v10

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    .line 2571
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/android/launcher2/Launcher;

    invoke-virtual {v5, p1, v1, v4}, Lcom/android/launcher2/Launcher;->startActivitySafely(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2575
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v3    # "pos":[I
    :cond_4
    sget-boolean v5, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mEditIcon:Landroid/view/View;

    if-ne p1, v5, :cond_1

    .line 2576
    invoke-virtual {p0, p1}, Lcom/android/launcher2/HomeView;->onClickEditButton(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onClickAllAppsButton(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2596
    sget-boolean v2, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v2, :cond_5

    .line 2597
    sget-object v2, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v3, "addapps"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v3, "create_folder"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    sget-boolean v2, Lcom/android/launcher2/HomeView;->mIsAllAppsButtonDisable:Z

    if-eqz v2, :cond_2

    .line 2627
    :cond_1
    :goto_0
    return-void

    .line 2599
    :cond_2
    sget-object v2, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v3, "addapps"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-boolean v2, Lcom/android/launcher2/guide/GuideFragment;->isViewApps:Z

    if-nez v2, :cond_3

    sget-object v2, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v3, "create_folder"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2600
    :cond_3
    sget-object v2, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v3, "addapps"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v3, "create_folder"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2601
    :cond_4
    const/4 v1, 0x0

    .line 2602
    .local v1, "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    new-instance v1, Lcom/android/launcher2/guide/AddAppsGuider;

    .end local v1    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/launcher2/guide/AddAppsGuider;-><init>(Landroid/app/Activity;)V

    .restart local v1    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    move-object v2, v1

    .line 2603
    check-cast v2, Lcom/android/launcher2/guide/AddAppsGuider;

    invoke-virtual {v2}, Lcom/android/launcher2/guide/AddAppsGuider;->isStep1DialogVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2605
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->showAllApps()V

    move-object v2, v1

    .line 2606
    check-cast v2, Lcom/android/launcher2/guide/AddAppsGuider;

    invoke-virtual {v2}, Lcom/android/launcher2/guide/AddAppsGuider;->dismissShowHelpDialog_step1()V

    .line 2607
    check-cast v1, Lcom/android/launcher2/guide/AddAppsGuider;

    .end local v1    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {v1}, Lcom/android/launcher2/guide/AddAppsGuider;->showHelpDialog_step2()V

    goto :goto_0

    .line 2621
    :cond_5
    const/16 v2, 0x80

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 2622
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2623
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.android.launcher.action.USE_EXPAND_MODE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2624
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2625
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->showAllApps()V

    goto :goto_0
.end method

.method public onClickAlwaysButton(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2630
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2631
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 2632
    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2633
    const-string v1, "com.samsung.android.app.always"

    const-string v2, "com.samsung.android.app.always.ui.MainActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2634
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2635
    return-void
.end method

.method public onClickEditButton(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 2639
    sget-boolean v0, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v0, :cond_0

    .line 2640
    invoke-static {v1}, Lcom/android/launcher2/Launcher;->setHomeEditMode(Z)V

    .line 2641
    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->showWorkspaceEditmode(Z)V

    .line 2643
    :cond_0
    return-void
.end method

.method public onClickSearchButton(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 4177
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    .line 4178
    .local v0, "launcher":Lcom/android/launcher2/Launcher;
    sget-boolean v1, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-nez v1, :cond_0

    .line 4179
    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->onSearchRequested()Z

    .line 4180
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 541
    const-string v0, "Launcher.HomeView"

    const-string v1, "onDestroyView"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/Launcher;->removeStateAnimatorProvider(Lcom/android/launcher2/Launcher$StateAnimatorProvider;)V

    .line 552
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 553
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 561
    iput-object v2, p0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    .line 563
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWidgetsToAdvance:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 565
    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/method/TextKeyListener;->release()V

    .line 568
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mModel:Lcom/android/launcher2/LauncherModel;

    if-eqz v0, :cond_0

    .line 569
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mModel:Lcom/android/launcher2/LauncherModel;

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherModel;->unbindAllHomeItems()V

    .line 572
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWidgetObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 573
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHelphubObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 575
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mChangeTphoneModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 577
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 578
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->removeAllViews()V

    .line 579
    iput-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    .line 580
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeDarkenLayer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 584
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mTiltWallpaperScroller:Lcom/sec/dtl/launcher/WallpaperScroller;

    if-eqz v0, :cond_1

    .line 585
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mTiltWallpaperScroller:Lcom/sec/dtl/launcher/WallpaperScroller;

    invoke-virtual {v0}, Lcom/sec/dtl/launcher/WallpaperScroller;->shutdown()V

    .line 587
    :cond_1
    return-void
.end method

.method public onFinishInflate()V
    .locals 4

    .prologue
    .line 310
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 311
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/HomeView;->mInflater:Landroid/view/LayoutInflater;

    .line 313
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/HomeView;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    .line 314
    new-instance v0, Lcom/android/launcher2/LauncherAppWidgetHost;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x400

    invoke-direct {v0, v1, v2}, Lcom/android/launcher2/LauncherAppWidgetHost;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    .line 315
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherAppWidgetHost;->startListening()V

    .line 316
    new-instance v1, Lcom/android/launcher2/AppWidgetBinder;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mAppWidgetHost:Lcom/android/launcher2/LauncherAppWidgetHost;

    invoke-direct {v1, v0, v2, v3}, Lcom/android/launcher2/AppWidgetBinder;-><init>(Lcom/android/launcher2/Launcher;Landroid/appwidget/AppWidgetManager;Lcom/android/launcher2/LauncherAppWidgetHost;)V

    iput-object v1, p0, Lcom/android/launcher2/HomeView;->mAppWidgetBinder:Lcom/android/launcher2/AppWidgetBinder;

    .line 323
    invoke-direct {p0}, Lcom/android/launcher2/HomeView;->setupViews()V

    .line 325
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->updateGlobalSearchIcon()Z

    .line 327
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/Launcher;->addStateAnimatorProvider(Lcom/android/launcher2/Launcher$StateAnimatorProvider;)V

    .line 328
    const-string v0, "Launcher.HomeView"

    const-string v1, "OnFinishInflate() completed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    return-void
.end method

.method public onFolderCreated(Lcom/android/launcher2/BaseItem;Ljava/lang/String;JZZII)V
    .locals 9
    .param p1, "folderChildItem"    # Lcom/android/launcher2/BaseItem;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "destinationContainerId"    # J
    .param p5, "removeItem"    # Z
    .param p6, "findNewPosition"    # Z
    .param p7, "originalContainerScreen"    # I
    .param p8, "originalContainerCell"    # I

    .prologue
    .line 4662
    move-object v2, p1

    check-cast v2, Lcom/android/launcher2/HomeShortcutItem;

    move-object v1, p0

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-virtual/range {v1 .. v7}, Lcom/android/launcher2/HomeView;->completeAddFolderWithShortcutItem(Lcom/android/launcher2/HomeShortcutItem;Ljava/lang/String;JZZ)V

    .line 4664
    return-void
.end method

.method public onHomePressed(Z)Z
    .locals 3
    .param p1, "moveToDefaultScreen"    # Z

    .prologue
    const/4 v0, 0x1

    .line 830
    sget-boolean v1, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v1, :cond_0

    .line 831
    iput-boolean v0, p0, Lcom/android/launcher2/HomeView;->mHomeKeyPress:Z

    .line 832
    const/4 v0, 0x0

    .line 855
    :goto_0
    return v0

    .line 835
    :cond_0
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getMeasuredWidth()I

    move-result v1

    if-nez v1, :cond_1

    .line 837
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    new-instance v2, Lcom/android/launcher2/HomeView$1;

    invoke-direct {v2, p0, p1}, Lcom/android/launcher2/HomeView$1;-><init>(Lcom/android/launcher2/HomeView;Z)V

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Workspace;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    goto :goto_0

    .line 853
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/launcher2/HomeView;->performOnHomePressed(Z)V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v10, 0x1

    .line 5195
    const/4 v6, 0x0

    .line 5202
    .local v6, "result":Z
    sget-boolean v7, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v7, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v7

    if-ne v7, v10, :cond_0

    .line 5204
    invoke-static {}, Lcom/android/launcher2/guide/HelpAnimatedDialog;->setAlpha()V

    .line 5207
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v7

    if-nez v7, :cond_4

    .line 5209
    iget-object v7, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v7}, Lcom/android/launcher2/Workspace;->getState()Lcom/android/launcher2/Workspace$State;

    move-result-object v7

    sget-object v8, Lcom/android/launcher2/Workspace$State;->RESIZE:Lcom/android/launcher2/Workspace$State;

    if-ne v7, v8, :cond_4

    .line 5214
    const/4 v3, 0x1

    .line 5215
    .local v3, "exitResizeMode":Z
    iget-object v7, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v7}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v1

    .line 5216
    .local v1, "count":I
    iget-object v7, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v7}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v2

    .line 5217
    .local v2, "current":I
    const/4 v7, -0x1

    if-le v2, v7, :cond_1

    if-ge v2, v1, :cond_1

    .line 5218
    iget-object v7, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v7, v2}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayoutWithResizableWidgets;

    .line 5220
    .local v0, "cellLayout":Lcom/android/launcher2/CellLayoutWithResizableWidgets;
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayoutWithResizableWidgets;->getResizeFrame()Lcom/android/launcher2/AppWidgetResizeFrame;

    move-result-object v5

    .line 5221
    .local v5, "resizeFrame":Lcom/android/launcher2/AppWidgetResizeFrame;
    if-eqz v5, :cond_1

    .line 5222
    sget-object v7, Lcom/android/launcher2/HomeView;->sTouchPt:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/PointF;->set(FF)V

    .line 5223
    sget-object v7, Lcom/android/launcher2/HomeView;->sTouchPt:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    sget-object v8, Lcom/android/launcher2/HomeView;->sTouchPt:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v7, v8}, Lcom/android/launcher2/AppWidgetResizeFrame;->isPointInFrame(FF)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 5224
    const/4 v3, 0x0

    .line 5228
    .end local v0    # "cellLayout":Lcom/android/launcher2/CellLayoutWithResizableWidgets;
    .end local v5    # "resizeFrame":Lcom/android/launcher2/AppWidgetResizeFrame;
    :cond_1
    sget-boolean v7, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v7, :cond_2

    sget-boolean v7, Lcom/android/launcher2/Workspace;->widgetSizeChanged:Z

    if-nez v7, :cond_2

    .line 5229
    const/4 v3, 0x0

    .line 5231
    :cond_2
    if-eqz v3, :cond_4

    .line 5232
    iget-object v7, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v7}, Lcom/android/launcher2/Workspace;->exitWidgetResizeMode()V

    .line 5233
    sget-boolean v7, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v7, :cond_3

    const-string v7, "resize_widgets"

    sget-object v8, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 5234
    sput-boolean v10, Lcom/android/launcher2/guide/GuideFragment;->isGuideCompleted:Z

    .line 5235
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f1000c2

    invoke-static {v7, v8, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 5236
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    .line 5237
    .local v4, "handler":Landroid/os/Handler;
    new-instance v7, Lcom/android/launcher2/HomeView$26;

    invoke-direct {v7, p0}, Lcom/android/launcher2/HomeView$26;-><init>(Lcom/android/launcher2/HomeView;)V

    const-wide/16 v8, 0x7d0

    invoke-virtual {v4, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 5244
    .end local v4    # "handler":Landroid/os/Handler;
    :cond_3
    const/4 v6, 0x1

    .line 5248
    .end local v1    # "count":I
    .end local v2    # "current":I
    .end local v3    # "exitResizeMode":Z
    :cond_4
    return v6
.end method

.method protected onLayout(ZIIII)V
    .locals 12
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 5114
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 5117
    sget-boolean v7, Lcom/android/launcher2/Launcher;->is_TB:Z

    if-eqz v7, :cond_0

    .line 5118
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/android/launcher2/Utilities;->getDeviceRotation(Landroid/content/Context;)I

    move-result v6

    .line 5119
    .local v6, "rotationAngle":I
    sget v7, Lcom/android/launcher2/HomeView;->sCurrentRotationAngle:I

    if-eq v7, v6, :cond_0

    .line 5120
    sput v6, Lcom/android/launcher2/HomeView;->sCurrentRotationAngle:I

    .line 5121
    invoke-direct {p0, v6}, Lcom/android/launcher2/HomeView;->setupHomeViewAfterRotationForTB(I)V

    .line 5125
    .end local v6    # "rotationAngle":I
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getChildCount()I

    move-result v1

    .line 5126
    .local v1, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_2

    .line 5127
    invoke-virtual {p0, v3}, Lcom/android/launcher2/HomeView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 5128
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 5129
    .local v2, "flp":Landroid/widget/FrameLayout$LayoutParams;
    instance-of v7, v2, Lcom/android/launcher2/HomeView$LayoutParams;

    if-eqz v7, :cond_1

    move-object v4, v2

    .line 5130
    check-cast v4, Lcom/android/launcher2/HomeView$LayoutParams;

    .line 5131
    .local v4, "lp":Lcom/android/launcher2/HomeView$LayoutParams;
    iget-boolean v7, v4, Lcom/android/launcher2/HomeView$LayoutParams;->customPosition:Z

    if-eqz v7, :cond_1

    .line 5132
    iget v7, v4, Lcom/android/launcher2/HomeView$LayoutParams;->x:I

    iget v8, v4, Lcom/android/launcher2/HomeView$LayoutParams;->y:I

    iget v9, v4, Lcom/android/launcher2/HomeView$LayoutParams;->x:I

    iget v10, v4, Lcom/android/launcher2/HomeView$LayoutParams;->width:I

    add-int/2addr v9, v10

    iget v10, v4, Lcom/android/launcher2/HomeView$LayoutParams;->y:I

    iget v11, v4, Lcom/android/launcher2/HomeView$LayoutParams;->height:I

    add-int/2addr v10, v11

    invoke-virtual {v0, v7, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 5126
    .end local v4    # "lp":Lcom/android/launcher2/HomeView$LayoutParams;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 5137
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "flp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_2
    if-eqz p1, :cond_3

    .line 5138
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getRootView()Landroid/view/View;

    move-result-object v5

    .line 5139
    .local v5, "rootView":Landroid/view/View;
    sget-object v7, Lcom/android/launcher2/HomeView;->sPanelDrawer:Lcom/android/launcher2/PanelDrawer;

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v8

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/android/launcher2/PanelDrawer;->setViewport(II)V

    .line 5141
    .end local v5    # "rootView":Landroid/view/View;
    :cond_3
    sget-object v7, Lcom/android/launcher2/HomeView;->sPanelDrawer:Lcom/android/launcher2/PanelDrawer;

    invoke-virtual {v7}, Lcom/android/launcher2/PanelDrawer;->hasLoadedResources()Z

    move-result v7

    if-nez v7, :cond_4

    .line 5142
    sget-object v7, Lcom/android/launcher2/HomeView;->sPanelDrawer:Lcom/android/launcher2/PanelDrawer;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/launcher2/PanelDrawer;->loadResources(Landroid/content/res/Resources;)V

    .line 5143
    :cond_4
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 14
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2705
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v6

    check-cast v6, Lcom/android/launcher2/Launcher;

    .line 2706
    .local v6, "launcher":Lcom/android/launcher2/Launcher;
    iget-boolean v11, v6, Lcom/android/launcher2/Launcher;->mInMenu:Z

    if-eqz v11, :cond_0

    .line 2707
    const/4 v11, 0x0

    .line 2915
    :goto_0
    return v11

    .line 2710
    :cond_0
    sget-boolean v11, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v11, :cond_4

    .line 2711
    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "change_wallpaper"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "add_widgets"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 2713
    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "resize_widgets"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "remove_items"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "move_app"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 2715
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v11

    if-nez v11, :cond_2

    .line 2716
    const/4 v11, 0x0

    goto :goto_0

    .line 2718
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    .line 2719
    .local v10, "tag":Ljava/lang/Object;
    instance-of v11, v10, Lcom/android/launcher2/HomeWidgetItem;

    if-eqz v11, :cond_6

    move-object v11, v10

    .line 2720
    check-cast v11, Lcom/android/launcher2/HomeWidgetItem;

    invoke-virtual {v11}, Lcom/android/launcher2/HomeWidgetItem;->getComponentName()Landroid/content/ComponentName;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "com.google.android.apps.chrome.appwidget.bookmarks.BookmarkThumbnailWidgetProvider"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    check-cast v10, Lcom/android/launcher2/HomeWidgetItem;

    .end local v10    # "tag":Ljava/lang/Object;
    invoke-virtual {v10}, Lcom/android/launcher2/HomeWidgetItem;->getComponentName()Landroid/content/ComponentName;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "com.sec.android.app.sbrowser.BookmarkThumbnailWidgetProvider"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2722
    :cond_3
    const/4 v8, 0x0

    .line 2723
    .local v8, "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    new-instance v8, Lcom/android/launcher2/guide/ResizeWidgetsGuider;

    .end local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-direct {v8, v11}, Lcom/android/launcher2/guide/ResizeWidgetsGuider;-><init>(Landroid/app/Activity;)V

    .line 2724
    .restart local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    check-cast v8, Lcom/android/launcher2/guide/ResizeWidgetsGuider;

    .end local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {v8}, Lcom/android/launcher2/guide/ResizeWidgetsGuider;->dismissHelpDialog()V

    .line 2753
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->isWorkspaceLocked()Z

    move-result v11

    if-eqz v11, :cond_a

    .line 2754
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 2726
    :cond_5
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 2728
    .restart local v10    # "tag":Ljava/lang/Object;
    :cond_6
    instance-of v11, v10, Lcom/android/launcher2/HomeShortcutItem;

    if-eqz v11, :cond_4

    move-object v11, v10

    .line 2729
    check-cast v11, Lcom/android/launcher2/HomeShortcutItem;

    invoke-virtual {v11}, Lcom/android/launcher2/HomeShortcutItem;->getComponentName()Landroid/content/ComponentName;

    move-result-object v11

    if-eqz v11, :cond_4

    move-object v11, v10

    .line 2730
    check-cast v11, Lcom/android/launcher2/HomeShortcutItem;

    invoke-virtual {v11}, Lcom/android/launcher2/HomeShortcutItem;->getComponentName()Landroid/content/ComponentName;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "com.sec.android.app.popupcalculator.Calculator"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_7

    check-cast v10, Lcom/android/launcher2/HomeShortcutItem;

    .end local v10    # "tag":Ljava/lang/Object;
    invoke-virtual {v10}, Lcom/android/launcher2/HomeShortcutItem;->getComponentName()Landroid/content/ComponentName;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "com.samsung.helphub.HelpHubActivity"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_7

    .line 2732
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 2733
    :cond_7
    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "remove_items"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 2734
    const/4 v8, 0x0

    .line 2735
    .restart local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    new-instance v8, Lcom/android/launcher2/guide/RemoveItemsGuider;

    .end local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-direct {v8, v11}, Lcom/android/launcher2/guide/RemoveItemsGuider;-><init>(Landroid/app/Activity;)V

    .restart local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    move-object v11, v8

    .line 2736
    check-cast v11, Lcom/android/launcher2/guide/RemoveItemsGuider;

    invoke-virtual {v11}, Lcom/android/launcher2/guide/RemoveItemsGuider;->dismissHelpDialog()V

    .line 2737
    check-cast v8, Lcom/android/launcher2/guide/RemoveItemsGuider;

    .end local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {v8}, Lcom/android/launcher2/guide/RemoveItemsGuider;->showHelpDialog2()V

    goto :goto_1

    .line 2739
    :cond_8
    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "move_app"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 2740
    const/4 v8, 0x0

    .line 2741
    .restart local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    new-instance v8, Lcom/android/launcher2/guide/MoveAppsGuider;

    .end local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-direct {v8, v11}, Lcom/android/launcher2/guide/MoveAppsGuider;-><init>(Landroid/app/Activity;)V

    .restart local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    move-object v11, v8

    .line 2742
    check-cast v11, Lcom/android/launcher2/guide/MoveAppsGuider;

    invoke-virtual {v11}, Lcom/android/launcher2/guide/MoveAppsGuider;->dismissHelpDialog()V

    .line 2743
    check-cast v8, Lcom/android/launcher2/guide/MoveAppsGuider;

    .end local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {v8}, Lcom/android/launcher2/guide/MoveAppsGuider;->showHelpDialog_step2()V

    goto :goto_1

    .line 2748
    :cond_9
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 2757
    :cond_a
    instance-of v11, p1, Lcom/android/launcher2/CellLayout;

    if-nez v11, :cond_b

    .line 2758
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    invoke-interface {v11}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    .end local p1    # "v":Landroid/view/View;
    check-cast p1, Landroid/view/View;

    .line 2762
    .restart local p1    # "v":Landroid/view/View;
    :cond_b
    sget-boolean v11, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v11, :cond_c

    instance-of v11, p1, Lcom/android/launcher2/CellLayout;

    if-eqz v11, :cond_c

    move-object v2, p1

    .line 2763
    check-cast v2, Lcom/android/launcher2/CellLayout;

    .line 2764
    .local v2, "cl":Lcom/android/launcher2/CellLayout;
    if-eqz v2, :cond_c

    .line 2765
    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getContainer()Lcom/android/launcher2/CellLayoutContainer;

    move-result-object v9

    .line 2766
    .local v9, "parent":Lcom/android/launcher2/CellLayoutContainer;
    instance-of v11, v9, Lcom/android/launcher2/Hotseat;

    if-eqz v11, :cond_c

    invoke-static {}, Lcom/android/launcher2/Launcher;->isHomeEditMode()Z

    move-result v11

    if-nez v11, :cond_c

    .line 2767
    const-string v11, "Launcher.HomeView"

    const-string v12, "Hotseat item long click but we do nothing in normal mode"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2768
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 2774
    .end local v2    # "cl":Lcom/android/launcher2/CellLayout;
    .end local v9    # "parent":Lcom/android/launcher2/CellLayoutContainer;
    :cond_c
    invoke-direct {p0}, Lcom/android/launcher2/HomeView;->resetAddInfo()V

    .line 2775
    const/4 v7, 0x0

    .line 2776
    .local v7, "longClickCellInfo":Lcom/android/launcher2/CellLayout$CellInfo;
    const/4 v4, 0x0

    .line 2777
    .local v4, "itemUnderLongClick":Landroid/view/View;
    instance-of v11, p1, Lcom/android/launcher2/CellLayout;

    if-eqz v11, :cond_e

    move-object v5, p1

    .line 2778
    check-cast v5, Lcom/android/launcher2/CellLayout;

    .line 2779
    .local v5, "l":Lcom/android/launcher2/CellLayout;
    invoke-virtual {v5}, Lcom/android/launcher2/CellLayout;->getTag()Lcom/android/launcher2/CellLayout$CellInfo;

    move-result-object v10

    .local v10, "tag":Lcom/android/launcher2/CellLayout$CellInfo;
    move-object v7, v10

    .line 2781
    check-cast v7, Lcom/android/launcher2/CellLayout$CellInfo;

    .line 2783
    if-nez v7, :cond_d

    .line 2784
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 2786
    :cond_d
    iget-object v4, v7, Lcom/android/launcher2/CellLayout$CellInfo;->cell:Landroid/view/View;

    .line 2788
    invoke-virtual {v5}, Lcom/android/launcher2/CellLayout;->clearTagCellInfo()V

    .line 2795
    .end local v5    # "l":Lcom/android/launcher2/CellLayout;
    .end local v10    # "tag":Lcom/android/launcher2/CellLayout$CellInfo;
    :cond_e
    invoke-virtual {p0, p1}, Lcom/android/launcher2/HomeView;->isHotseatLayout(Landroid/view/View;)Z

    move-result v11

    if-nez v11, :cond_f

    iget-object v11, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v11}, Lcom/android/launcher2/Workspace;->allowLongPress()Z

    move-result v11

    if-eqz v11, :cond_10

    :cond_f
    const/4 v0, 0x1

    .line 2797
    .local v0, "allowLongPress":Z
    :goto_2
    invoke-virtual {p0, p1}, Lcom/android/launcher2/HomeView;->isHotseatLayout(Landroid/view/View;)Z

    move-result v11

    if-eqz v11, :cond_11

    sget-boolean v11, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v11, :cond_11

    .line 2798
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 2795
    .end local v0    # "allowLongPress":Z
    :cond_10
    const/4 v0, 0x0

    goto :goto_2

    .line 2802
    .restart local v0    # "allowLongPress":Z
    :cond_11
    invoke-virtual {p0, p1}, Lcom/android/launcher2/HomeView;->isHotseatLayout(Landroid/view/View;)Z

    move-result v11

    if-eqz v11, :cond_12

    if-nez v4, :cond_12

    .line 2803
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 2806
    :cond_12
    sget-boolean v11, Lcom/android/launcher2/LauncherApplication;->sFestivalPageLauncher:Z

    if-eqz v11, :cond_14

    sget-boolean v11, Lcom/android/launcher2/Launcher;->sIsFestivalModeOn:Z

    if-eqz v11, :cond_14

    invoke-static {}, Lcom/android/launcher2/Launcher;->isHomeEditMode()Z

    move-result v11

    if-nez v11, :cond_14

    .line 2807
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getCurrentPage()I

    move-result v3

    .line 2808
    .local v3, "currentPage":I
    invoke-virtual {p0, v3}, Lcom/android/launcher2/HomeView;->getPage(I)Lcom/android/launcher2/CellLayout;

    move-result-object v1

    .line 2809
    .local v1, "cell":Lcom/android/launcher2/CellLayout;
    invoke-virtual {v1}, Lcom/android/launcher2/CellLayout;->getPageType()I

    move-result v11

    const/4 v12, 0x2

    if-ne v11, v12, :cond_14

    .line 2810
    sget-boolean v11, Lcom/android/launcher2/HomeView;->DEBUGGABLE:Z

    if-eqz v11, :cond_13

    const-string v11, "Launcher.HomeView"

    const-string v12, "-----Skip long click on festival page "

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2811
    :cond_13
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 2815
    .end local v1    # "cell":Lcom/android/launcher2/CellLayout;
    .end local v3    # "currentPage":I
    :cond_14
    if-eqz v0, :cond_19

    .line 2819
    sget-object v11, Lcom/android/launcher2/HomeView;->cpuBooster:Landroid/os/DVFSHelper;

    const-string v12, "Launcher_touch"

    invoke-virtual {v11, v12}, Landroid/os/DVFSHelper;->addExtraOptionsByDefaultPolicy(Ljava/lang/String;)V

    .line 2820
    sget-object v11, Lcom/android/launcher2/HomeView;->cpuBooster:Landroid/os/DVFSHelper;

    const-string v12, "Launcher_touch"

    invoke-virtual {v11, v12}, Landroid/os/DVFSHelper;->acquire(Ljava/lang/String;)V

    .line 2822
    if-nez v4, :cond_1b

    iget-boolean v11, p0, Lcom/android/launcher2/HomeView;->mAfterSavedInstanceState:Z

    if-nez v11, :cond_1b

    .line 2824
    iget-object v11, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-virtual {v11, v12, v13}, Lcom/android/launcher2/Workspace;->performHapticFeedback(II)Z

    .line 2827
    sget-boolean v11, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v11, :cond_15

    invoke-static {}, Lcom/android/launcher2/Launcher;->isHomeEditMode()Z

    move-result v11

    if-nez v11, :cond_18

    .line 2829
    :cond_15
    sget-boolean v11, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v11, :cond_1a

    .line 2830
    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "change_wallpaper"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_16

    .line 2831
    const/4 v8, 0x0

    .line 2832
    .restart local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    new-instance v8, Lcom/android/launcher2/guide/ChangeWallpaperGuider;

    .end local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-direct {v8, v11}, Lcom/android/launcher2/guide/ChangeWallpaperGuider;-><init>(Landroid/app/Activity;)V

    .restart local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    move-object v11, v8

    .line 2833
    check-cast v11, Lcom/android/launcher2/guide/ChangeWallpaperGuider;

    invoke-virtual {v11}, Lcom/android/launcher2/guide/ChangeWallpaperGuider;->dismissHelpDialog()V

    .line 2834
    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-virtual {p0, v11, v12}, Lcom/android/launcher2/HomeView;->openQuickViewWorkspace(Lcom/android/launcher2/HomeView$SavedState;Z)V

    .line 2835
    check-cast v8, Lcom/android/launcher2/guide/ChangeWallpaperGuider;

    .end local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {v8}, Lcom/android/launcher2/guide/ChangeWallpaperGuider;->showHelpDialog_step1()V

    .line 2839
    :cond_16
    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "remove_items"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_17

    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "create_folder"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_17

    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "move_app"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_17

    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "resize_widgets"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_17

    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "add_widgets"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_17

    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "change_wallpaper"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_17

    .line 2845
    sget v11, Lcom/android/launcher2/guide/GuideFragment;->sHelpHubStepNumber:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_17

    .line 2847
    invoke-virtual {v6}, Lcom/android/launcher2/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    invoke-static {v11}, Lcom/android/launcher2/AddToHomescreenDialogFragment;->createAndShow(Landroid/app/FragmentManager;)V

    .line 2848
    const/4 v11, 0x2

    sput v11, Lcom/android/launcher2/guide/GuideFragment;->sHelpHubStepNumber:I

    .line 2852
    :cond_17
    sget-object v11, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v12, "add_widgets"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_18

    .line 2853
    const-string v11, "DaliLogs"

    const-string v12, "addwidgets"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2854
    const/4 v8, 0x0

    .line 2855
    .restart local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    new-instance v8, Lcom/android/launcher2/guide/AddWidgetsGuider;

    .end local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-direct {v8, v11}, Lcom/android/launcher2/guide/AddWidgetsGuider;-><init>(Landroid/app/Activity;)V

    .restart local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    move-object v11, v8

    .line 2856
    check-cast v11, Lcom/android/launcher2/guide/AddWidgetsGuider;

    invoke-virtual {v11}, Lcom/android/launcher2/guide/AddWidgetsGuider;->dismissHelpDialog()V

    .line 2857
    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-virtual {p0, v11, v12}, Lcom/android/launcher2/HomeView;->openQuickViewWorkspace(Lcom/android/launcher2/HomeView$SavedState;Z)V

    .line 2858
    check-cast v8, Lcom/android/launcher2/guide/AddWidgetsGuider;

    .end local v8    # "mGuider":Lcom/android/launcher2/guide/GuiderLifecycleListener;
    invoke-virtual {v8}, Lcom/android/launcher2/guide/AddWidgetsGuider;->showHelpDialog_step1()V

    .line 2870
    :cond_18
    :goto_3
    iget-object v11, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lcom/android/launcher2/Workspace;->requestDisallowInterceptTouchEvent(Z)V

    .line 2913
    :goto_4
    sget-object v11, Lcom/android/launcher2/HomeView;->cpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v11}, Landroid/os/DVFSHelper;->release()V

    .line 2915
    :cond_19
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 2863
    :cond_1a
    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-virtual {p0, v11, v12}, Lcom/android/launcher2/HomeView;->openQuickViewWorkspace(Lcom/android/launcher2/HomeView$SavedState;Z)V

    goto :goto_3

    .line 2877
    :cond_1b
    sget-boolean v11, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v11, :cond_1c

    sget-boolean v11, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-nez v11, :cond_1c

    .line 2878
    invoke-static {}, Lcom/android/launcher2/Launcher;->isHomeEditMode()Z

    move-result v11

    if-nez v11, :cond_1c

    .line 2879
    const/4 v11, 0x1

    invoke-static {v11}, Lcom/android/launcher2/Launcher;->setHomeRemoveMode(Z)V

    .line 2906
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v11

    const v12, 0x7f100092

    const/4 v13, 0x1

    invoke-static {v11, v12, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/Toast;->show()V

    .line 2910
    :cond_1c
    iget-object v11, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v11, v4}, Lcom/android/launcher2/Workspace;->startDrag(Landroid/view/View;)Z

    goto :goto_4
.end method

.method public onPageSwitch(Landroid/view/View;I)V
    .locals 1
    .param p1, "newPage"    # Landroid/view/View;
    .param p2, "newPageIndex"    # I

    .prologue
    .line 4018
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    if-eqz v0, :cond_0

    .line 4029
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->closeFolder()V

    .line 4031
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 591
    const-string v0, "Launcher.HomeView"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mTiltWallpaperScroller:Lcom/sec/dtl/launcher/WallpaperScroller;

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mTiltWallpaperScroller:Lcom/sec/dtl/launcher/WallpaperScroller;

    invoke-virtual {v0}, Lcom/sec/dtl/launcher/WallpaperScroller;->pause()V

    .line 598
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->removeHoverScrollHandler()V

    .line 599
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->closeFolder()V

    .line 600
    sget-object v0, Lcom/android/launcher2/HomeView;->sPanelDrawer:Lcom/android/launcher2/PanelDrawer;

    invoke-virtual {v0}, Lcom/android/launcher2/PanelDrawer;->pause()V

    .line 601
    return-void
.end method

.method public onQuickViewCloseAnimationCompleted()V
    .locals 1

    .prologue
    .line 4303
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/HomeView;->closeQuickViewWorkspace(Z)V

    .line 4304
    return-void
.end method

.method public onQuickViewCloseAnimationStarted()V
    .locals 0

    .prologue
    .line 4308
    invoke-direct {p0}, Lcom/android/launcher2/HomeView;->closeQuickViewWorkspaceStartAnimation()V

    .line 4309
    return-void
.end method

.method public onQuickViewDragEnd()V
    .locals 3

    .prologue
    .line 4318
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    const/4 v1, 0x0

    sget-object v2, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/QuickViewWorkspace;->setLayerType(ILandroid/graphics/Paint;)V

    .line 4319
    return-void
.end method

.method public onQuickViewDragStart()V
    .locals 3

    .prologue
    .line 4313
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    const/4 v1, 0x2

    sget-object v2, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/QuickViewWorkspace;->setLayerType(ILandroid/graphics/Paint;)V

    .line 4314
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 647
    instance-of v1, p1, Lcom/android/launcher2/HomeView$SavedState;

    if-nez v1, :cond_0

    .line 648
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 657
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 653
    check-cast v0, Lcom/android/launcher2/HomeView$SavedState;

    .line 654
    .local v0, "ss":Lcom/android/launcher2/HomeView$SavedState;
    invoke-virtual {v0}, Lcom/android/launcher2/HomeView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 655
    invoke-direct {p0, v0}, Lcom/android/launcher2/HomeView;->restoreState(Lcom/android/launcher2/HomeView$SavedState;)V

    .line 656
    iput-object v0, p0, Lcom/android/launcher2/HomeView;->mSavedState:Lcom/android/launcher2/HomeView$SavedState;

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 486
    const-string v0, "Launcher.HomeView"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    iget-boolean v0, p0, Lcom/android/launcher2/HomeView;->mRestoring:Z

    if-eqz v0, :cond_0

    .line 488
    iput-boolean v3, p0, Lcom/android/launcher2/HomeView;->mWorkspaceLoading:Z

    .line 489
    iput-boolean v2, p0, Lcom/android/launcher2/HomeView;->mRestoring:Z

    .line 492
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v0}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v0

    if-nez v0, :cond_6

    .line 493
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->resumeScreen(I)V

    .line 496
    sget-boolean v0, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v0, :cond_4

    .line 497
    invoke-static {}, Lcom/android/launcher2/Launcher;->isHomeEditMode()Z

    move-result v0

    if-nez v0, :cond_2

    .line 498
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomePhoneBtn:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 499
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomePhoneBtn:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 501
    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeAppsBtn:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 502
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeAppsBtn:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 517
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->updateWallpaperOffsets()V

    .line 519
    iput-boolean v2, p0, Lcom/android/launcher2/HomeView;->mAfterSavedInstanceState:Z

    .line 521
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mTiltWallpaperScroller:Lcom/sec/dtl/launcher/WallpaperScroller;

    if-eqz v0, :cond_3

    .line 522
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mTiltWallpaperScroller:Lcom/sec/dtl/launcher/WallpaperScroller;

    invoke-virtual {v0}, Lcom/sec/dtl/launcher/WallpaperScroller;->resume()V

    .line 524
    :cond_3
    sget-object v0, Lcom/android/launcher2/HomeView;->sPanelDrawer:Lcom/android/launcher2/PanelDrawer;

    invoke-virtual {v0}, Lcom/android/launcher2/PanelDrawer;->resume()V

    .line 525
    return-void

    .line 505
    :cond_4
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomePhoneBtn:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 506
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomePhoneBtn:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 508
    :cond_5
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeAppsBtn:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 509
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeAppsBtn:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 512
    :cond_6
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v0}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 513
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v0}, Lcom/android/launcher2/QuickViewWorkspace;->invalidate()V

    .line 514
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/HomeScreenOptionMenu;->setMakeActive(Z)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 606
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    .line 607
    .local v2, "superState":Landroid/os/Parcelable;
    new-instance v1, Lcom/android/launcher2/HomeView$SavedState;

    invoke-direct {v1, v2}, Lcom/android/launcher2/HomeView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 609
    .local v1, "ss":Lcom/android/launcher2/HomeView$SavedState;
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getComingPage()I

    move-result v3

    iput v3, v1, Lcom/android/launcher2/HomeView$SavedState;->currentScreen:I

    .line 613
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 614
    .local v0, "folderBundle":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lcom/android/launcher2/HomeView;->saveOpenFolderState(Landroid/os/Bundle;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 615
    iput-object v0, v1, Lcom/android/launcher2/HomeView$SavedState;->folderBundle:Landroid/os/Bundle;

    .line 619
    :cond_0
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-wide v6, v3, Lcom/android/launcher2/HomeItem;->container:J

    const-wide/16 v8, -0x1

    cmp-long v3, v6, v8

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v3, v3, Lcom/android/launcher2/HomeItem;->mScreen:I

    const/4 v6, -0x1

    if-le v3, v6, :cond_1

    iget-boolean v3, p0, Lcom/android/launcher2/HomeView;->mWaitingForResult:Z

    if-eqz v3, :cond_1

    .line 621
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-wide v6, v3, Lcom/android/launcher2/HomeItem;->container:J

    iput-wide v6, v1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddContainer:J

    .line 622
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v3, v3, Lcom/android/launcher2/HomeItem;->mScreen:I

    iput v3, v1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddScreen:I

    .line 623
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v3, v3, Lcom/android/launcher2/HomeItem;->cellX:I

    iput v3, v1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddCellX:I

    .line 624
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v3, v3, Lcom/android/launcher2/HomeItem;->cellY:I

    iput v3, v1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddCellY:I

    .line 625
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v3, v3, Lcom/android/launcher2/HomeItem;->spanX:I

    iput v3, v1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddSpanX:I

    .line 626
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget v3, v3, Lcom/android/launcher2/HomeItem;->spanY:I

    iput v3, v1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddSpanY:I

    .line 627
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-boolean v3, v3, Lcom/android/launcher2/HomeItem;->mSecretItem:Z

    if-eqz v3, :cond_4

    move v3, v4

    :goto_0
    iput v3, v1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddSecretItem:I

    .line 628
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-object v3, v3, Lcom/android/launcher2/HomeItem;->dropPos:[I

    if-eqz v3, :cond_1

    .line 629
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-object v3, v3, Lcom/android/launcher2/HomeItem;->dropPos:[I

    aget v3, v3, v5

    iput v3, v1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddDropPosX:I

    .line 630
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iget-object v3, v3, Lcom/android/launcher2/HomeItem;->dropPos:[I

    aget v3, v3, v4

    iput v3, v1, Lcom/android/launcher2/HomeView$SavedState;->pendingAddDropPoxY:I

    .line 634
    :cond_1
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v3}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 635
    iput-boolean v4, v1, Lcom/android/launcher2/HomeView$SavedState;->inQuickView:Z

    .line 636
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v3}, Lcom/android/launcher2/QuickViewWorkspace;->getDeletePageIndex()I

    move-result v3

    iput v3, v1, Lcom/android/launcher2/HomeView$SavedState;->quickViewDeleteIndex:I

    .line 638
    :cond_2
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mHomeDarkenLayer:Landroid/view/View;

    if-eqz v3, :cond_3

    .line 639
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mHomeDarkenLayer:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    iput v3, v1, Lcom/android/launcher2/HomeView$SavedState;->darkenLayerVisibility:I

    .line 641
    :cond_3
    iput-boolean v4, p0, Lcom/android/launcher2/HomeView;->mAfterSavedInstanceState:Z

    .line 642
    return-object v1

    :cond_4
    move v3, v5

    .line 627
    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 466
    const-string v0, "Launcher.HomeView"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->setWallpaperDimension()V

    .line 482
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 529
    const-string v0, "Launcher.HomeView"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->closeFolder()V

    .line 531
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->onStop()V

    .line 532
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getCurrentPage()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->pauseScreen(I)V

    .line 534
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v0}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/HomeScreenOptionMenu;->setMakeActive(Z)V

    .line 537
    :cond_0
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2584
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/HomeView;->showWorkspace(Z)V

    .line 2585
    const/4 v0, 0x0

    return v0
.end method

.method public onVisibilityChanged(Landroid/view/View;I)V
    .locals 3
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    const/4 v2, 0x0

    .line 4830
    if-ne p1, p0, :cond_1

    .line 4831
    if-eqz p2, :cond_2

    const/4 v0, 0x1

    .line 4832
    .local v0, "hidden":Z
    :goto_0
    if-nez v0, :cond_0

    sget-boolean v1, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v1, :cond_0

    .line 4833
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iput-boolean v2, v1, Lcom/android/launcher2/Workspace;->mIsHelpOrientationChanged:Z

    .line 4834
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->getTransitionToAllApps()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 4835
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/Launcher;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Launcher;->setTransitionToAllApps(Z)V

    .line 4838
    .end local v0    # "hidden":Z
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 4831
    goto :goto_0
.end method

.method public onWindowVisibilityChanged(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 1873
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->updateRunning()V

    .line 1874
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mShadow:Lcom/android/launcher2/ShadowBuilder;

    if-nez v0, :cond_0

    .line 1882
    :goto_0
    return-void

    .line 1876
    :cond_0
    if-nez p1, :cond_1

    .line 1877
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mShadow:Lcom/android/launcher2/ShadowBuilder;

    invoke-virtual {v0}, Lcom/android/launcher2/ShadowBuilder;->draw()V

    goto :goto_0

    .line 1879
    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mShadow:Lcom/android/launcher2/ShadowBuilder;

    invoke-virtual {v0}, Lcom/android/launcher2/ShadowBuilder;->clear()V

    goto :goto_0
.end method

.method openQuickViewWorkspace(Lcom/android/launcher2/HomeView$SavedState;Z)V
    .locals 12
    .param p1, "ss"    # Lcom/android/launcher2/HomeView$SavedState;
    .param p2, "drawOpenAnimation"    # Z

    .prologue
    .line 3648
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v8}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 3827
    :cond_0
    :goto_0
    return-void

    .line 3650
    :cond_1
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->getPageIndicatorManager()Lcom/android/launcher2/PageIndicatorManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/launcher2/PageIndicatorManager;->getPageIndicator()Lcom/android/launcher2/PageIndicator;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/android/launcher2/PageIndicator;->setPageIndicatorPanelRightAdjust(I)V

    .line 3652
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHomePhoneBtn:Landroid/view/View;

    if-eqz v8, :cond_2

    .line 3653
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHomePhoneBtn:Landroid/view/View;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 3655
    :cond_2
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHomeAppsBtn:Landroid/view/View;

    if-eqz v8, :cond_3

    .line 3656
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHomeAppsBtn:Landroid/view/View;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 3658
    :cond_3
    sget-boolean v8, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-nez v8, :cond_6

    .line 3659
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/android/launcher2/Launcher;

    .line 3664
    .local v5, "launcher":Lcom/android/launcher2/Launcher;
    invoke-virtual {v5}, Lcom/android/launcher2/Launcher;->isTransitioningToShowAllApps()Z

    move-result v8

    if-nez v8, :cond_0

    invoke-static {}, Lcom/android/launcher2/Launcher;->isPopupMenuShowing()Z

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->isAnimating()Z

    move-result v8

    if-nez v8, :cond_0

    .line 3668
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->isInResizeMode()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 3669
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->exitWidgetResizeMode()V

    .line 3683
    :cond_4
    sget-boolean v8, Lcom/android/launcher2/LauncherApplication;->sFestivalPageLauncher:Z

    if-eqz v8, :cond_5

    .line 3684
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->getPageIndicatorManager()Lcom/android/launcher2/PageIndicatorManager;

    move-result-object v8

    sget-object v9, Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;->NORMAL_PAGE:Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;

    invoke-virtual {v8, v9}, Lcom/android/launcher2/PageIndicatorManager;->setDisplayItem(Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;)V

    .line 3685
    :cond_5
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/android/launcher2/QuickViewWorkspace;->setSecretQuickViewMode(Z)V

    .line 3690
    .end local v5    # "launcher":Lcom/android/launcher2/Launcher;
    :cond_6
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    if-eqz v8, :cond_7

    iget-boolean v8, p0, Lcom/android/launcher2/HomeView;->removeHotseat:Z

    if-eqz v8, :cond_7

    .line 3691
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 3693
    :cond_7
    if-eqz p2, :cond_8

    .line 3694
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v8}, Lcom/android/launcher2/QuickViewWorkspace;->drawOpenAnimation()V

    .line 3697
    :cond_8
    sget-object v8, Lcom/android/launcher2/HomeView;->cpuBooster:Landroid/os/DVFSHelper;

    const-string v9, "Launcher_touch"

    invoke-virtual {v8, v9}, Landroid/os/DVFSHelper;->addExtraOptionsByDefaultPolicy(Ljava/lang/String;)V

    .line 3698
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v8

    check-cast v8, Lcom/android/launcher2/Launcher;

    invoke-virtual {v8}, Lcom/android/launcher2/Launcher;->getAnimationLayer()Lcom/android/launcher2/AnimationLayer;

    move-result-object v4

    .line 3699
    .local v4, "l":Lcom/android/launcher2/AnimationLayer;
    new-instance v6, Lcom/android/launcher2/HomeView$23;

    invoke-direct {v6, p0, v4}, Lcom/android/launcher2/HomeView$23;-><init>(Lcom/android/launcher2/HomeView;Lcom/android/launcher2/AnimationLayer;)V

    .line 3735
    .local v6, "listener":Landroid/animation/AnimatorListenerAdapter;
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-eqz v8, :cond_9

    .line 3736
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    const/4 v9, 0x2

    sget-object v10, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {v8, v9, v10}, Lcom/android/launcher2/Hotseat;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3737
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Lcom/android/launcher2/Hotseat;->setVisibility(I)V

    .line 3738
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v8}, Lcom/android/launcher2/Hotseat;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    const-wide/16 v10, 0x190

    invoke-virtual {v8, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 3745
    :cond_9
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    if-eqz v8, :cond_a

    .line 3746
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    const/4 v9, 0x2

    sget-object v10, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {v8, v9, v10}, Lcom/android/launcher2/QuickLaunch;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3747
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v8}, Lcom/android/launcher2/QuickLaunch;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    const-wide/16 v10, 0x190

    invoke-virtual {v8, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 3749
    :cond_a
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    if-eqz v8, :cond_b

    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v8}, Lcom/android/launcher2/QuickLaunch;->isLandscape()Z

    move-result v8

    if-eqz v8, :cond_b

    .line 3750
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    const/4 v9, 0x2

    sget-object v10, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {v8, v9, v10}, Lcom/android/launcher2/QuickLaunch;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3751
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v8}, Lcom/android/launcher2/QuickLaunch;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    const-wide/16 v10, 0x190

    invoke-virtual {v8, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 3754
    :cond_b
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHomeTopBar:Landroid/view/View;

    if-eqz v8, :cond_c

    .line 3755
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHomeTopBar:Landroid/view/View;

    const/4 v9, 0x2

    sget-object v10, Lcom/android/launcher2/Launcher;->sViewLayerPaint:Landroid/graphics/Paint;

    invoke-virtual {v8, v9, v10}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3756
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHomeTopBar:Landroid/view/View;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 3757
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mHomeTopBar:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    const-wide/16 v10, 0x190

    invoke-virtual {v8, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 3760
    :cond_c
    sget-boolean v8, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-nez v8, :cond_d

    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v8

    if-eqz v8, :cond_d

    .line 3761
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->closeFolder()V

    .line 3766
    :cond_d
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/android/launcher2/Workspace;->setHideItems(Z)V

    .line 3768
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getCurrentPage()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/android/launcher2/Workspace;->pauseScreen(I)V

    .line 3770
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->invalidate()V

    .line 3772
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/16 v9, 0x80

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/android/launcher2/Workspace;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 3774
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v8}, Lcom/android/launcher2/QuickViewWorkspace;->bringToFront()V

    .line 3775
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v8, p0}, Lcom/android/launcher2/QuickViewWorkspace;->initScreen(Lcom/android/launcher2/QuickView$QuickViewInfoProvider;)V

    .line 3776
    if-eqz p1, :cond_e

    .line 3777
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    iget v9, p1, Lcom/android/launcher2/HomeView$SavedState;->quickViewDeleteIndex:I

    invoke-virtual {v8, v9}, Lcom/android/launcher2/QuickViewWorkspace;->setDeletePageIndex(I)V

    .line 3779
    :cond_e
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->closeOptionsMenu()V

    .line 3781
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Lcom/android/launcher2/Workspace;->setAllPagesVisibility(I)V

    .line 3792
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v8

    if-ge v2, v8, :cond_11

    .line 3793
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8, v2}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 3794
    .local v0, "cl":Lcom/android/launcher2/CellLayout;
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getChildrenLayout()Lcom/android/launcher2/CellLayoutChildren;

    move-result-object v1

    .line 3795
    .local v1, "clChildren":Lcom/android/launcher2/CellLayoutChildren;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    invoke-virtual {v1}, Lcom/android/launcher2/CellLayoutChildren;->getChildCount()I

    move-result v8

    if-ge v3, v8, :cond_10

    .line 3796
    invoke-virtual {v1, v3}, Lcom/android/launcher2/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 3797
    .local v7, "v":Landroid/view/View;
    instance-of v8, v7, Lcom/android/launcher2/SurfaceWidgetView;

    if-eqz v8, :cond_f

    move-object v8, v7

    .line 3798
    check-cast v8, Lcom/android/launcher2/SurfaceWidgetView;

    const/4 v9, 0x0

    invoke-virtual {v8, v7, v9}, Lcom/android/launcher2/SurfaceWidgetView;->onVisibilityChanged(Landroid/view/View;I)V

    .line 3799
    check-cast v7, Lcom/android/launcher2/SurfaceWidgetView;

    .end local v7    # "v":Landroid/view/View;
    const v8, 0x3f7d70a4    # 0.99f

    invoke-virtual {v7, v8}, Lcom/android/launcher2/SurfaceWidgetView;->setAlpha(F)V

    .line 3795
    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 3792
    :cond_10
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3804
    .end local v0    # "cl":Lcom/android/launcher2/CellLayout;
    .end local v1    # "clChildren":Lcom/android/launcher2/CellLayoutChildren;
    .end local v3    # "j":I
    :cond_11
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v8}, Lcom/android/launcher2/QuickViewWorkspace;->open()V

    .line 3818
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->getPageIndicatorManager()Lcom/android/launcher2/PageIndicatorManager;

    move-result-object v8

    sget-object v9, Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;->NORMAL_PAGE:Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;

    invoke-virtual {v8, v9}, Lcom/android/launcher2/PageIndicatorManager;->setDisplayItem(Lcom/android/launcher2/PageIndicatorManager$DISPLAY_ITEM;)V

    .line 3819
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/android/launcher2/QuickViewWorkspace;->setSecretQuickViewMode(Z)V

    .line 3822
    sget-boolean v8, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-nez v8, :cond_12

    if-nez p2, :cond_12

    .line 3823
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v8}, Lcom/android/launcher2/QuickViewWorkspace;->drawOpenNonAnimation()V

    .line 3826
    :cond_12
    iget-object v8, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    sget-object v9, Lcom/android/launcher2/PagedView$LayerOptions;->FORCE_NONE:Lcom/android/launcher2/PagedView$LayerOptions;

    invoke-virtual {v8, v9}, Lcom/android/launcher2/Workspace;->updateChildrenLayersEnabled(Lcom/android/launcher2/PagedView$LayerOptions;)V

    goto/16 :goto_0
.end method

.method processItemDropToNewFolder(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 2170
    invoke-direct {p0}, Lcom/android/launcher2/HomeView;->resetAddInfo()V

    .line 2171
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    const-wide/16 v2, -0x64

    iput-wide v2, v1, Lcom/android/launcher2/HomeItem;->container:J

    .line 2172
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CREATE_SHORTCUT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2173
    .local v0, "createItemIntent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2174
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/HomeView;->startActivityForResultSafely(Landroid/content/Intent;I)V

    .line 2175
    return-void
.end method

.method processShortcutFromDrop(Landroid/content/ComponentName;JI[I[IZ)V
    .locals 4
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "container"    # J
    .param p4, "screen"    # I
    .param p5, "cell"    # [I
    .param p6, "loc"    # [I
    .param p7, "isSecretItem"    # Z

    .prologue
    const/4 v3, 0x1

    .line 2152
    invoke-direct {p0}, Lcom/android/launcher2/HomeView;->resetAddInfo()V

    .line 2153
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iput-wide p2, v1, Lcom/android/launcher2/HomeItem;->container:J

    .line 2154
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iput p4, v1, Lcom/android/launcher2/HomeItem;->mScreen:I

    .line 2155
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    iput-object p6, v1, Lcom/android/launcher2/HomeItem;->dropPos:[I

    .line 2159
    if-eqz p5, :cond_0

    .line 2160
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    const/4 v2, 0x0

    aget v2, p5, v2

    iput v2, v1, Lcom/android/launcher2/HomeItem;->cellX:I

    .line 2161
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mPendingAddInfo:Lcom/android/launcher2/HomeItem;

    aget v2, p5, v3

    iput v2, v1, Lcom/android/launcher2/HomeItem;->cellY:I

    .line 2164
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CREATE_SHORTCUT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2165
    .local v0, "createShortcutIntent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2166
    invoke-virtual {p0, v0, v3}, Lcom/android/launcher2/HomeView;->startActivityForResultSafely(Landroid/content/Intent;I)V

    .line 2167
    return-void
.end method

.method public refreshQuickViewWorkspace(Z)V
    .locals 2
    .param p1, "isSecret"    # Z

    .prologue
    .line 3909
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v0}, Lcom/android/launcher2/QuickViewWorkspace;->isOpened()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3919
    :goto_0
    return-void

    .line 3912
    :cond_0
    if-eqz p1, :cond_1

    .line 3913
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/QuickViewWorkspace;->setSecretQuickViewMode(Z)V

    .line 3918
    :goto_1
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/QuickViewWorkspace;->initScreen(Lcom/android/launcher2/QuickView$QuickViewInfoProvider;)V

    goto :goto_0

    .line 3915
    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/QuickViewWorkspace;->setSecretQuickViewMode(Z)V

    goto :goto_1
.end method

.method public removeAppWidget(Lcom/android/launcher2/HomeWidgetItem;)V
    .locals 1
    .param p1, "launcherInfo"    # Lcom/android/launcher2/HomeWidgetItem;

    .prologue
    .line 1971
    iget-object v0, p1, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/HomeView;->removeWidgetToAutoAdvance(Landroid/view/View;)V

    .line 1972
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/android/launcher2/HomeWidgetItem;->hostView:Landroid/appwidget/AppWidgetHostView;

    .line 1973
    return-void
.end method

.method removeFolder(Lcom/android/launcher2/HomeFolderItem;)V
    .locals 4
    .param p1, "folder"    # Lcom/android/launcher2/HomeFolderItem;

    .prologue
    .line 2446
    sget-object v0, Lcom/android/launcher2/HomeView;->sFolders:Ljava/util/HashMap;

    iget-wide v2, p1, Lcom/android/launcher2/HomeFolderItem;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2447
    return-void
.end method

.method removePage()V
    .locals 1

    .prologue
    .line 4078
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/HomeView;->mIsDeletePopup:Z

    .line 4079
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickViewWorkspace:Lcom/android/launcher2/QuickViewWorkspace;

    invoke-virtual {v0}, Lcom/android/launcher2/QuickViewWorkspace;->removeScreen()V

    .line 4080
    return-void
.end method

.method removeWidgetToAutoAdvance(Landroid/view/View;)V
    .locals 1
    .param p1, "hostView"    # Landroid/view/View;

    .prologue
    .line 1964
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWidgetsToAdvance:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1965
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWidgetsToAdvance:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1966
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->updateRunning()V

    .line 1968
    :cond_0
    return-void
.end method

.method public removehelpAppPage()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 4381
    iget-boolean v0, p0, Lcom/android/launcher2/HomeView;->isHelpAppPageDeleted:Z

    if-eqz v0, :cond_0

    .line 4393
    :goto_0
    return-void

    .line 4384
    :cond_0
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->sFestivalPageLauncher:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/android/launcher2/Launcher;->sIsFestivalModeOn:Z

    if-eqz v0, :cond_1

    .line 4385
    iget v0, p0, Lcom/android/launcher2/HomeView;->screenIndexFestivalHelpAppPageAddition:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/HomeView;->deletePage(I)V

    .line 4388
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/HomeView;->isHelpAppPageDeleted:Z

    .line 4389
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/HomeView;->isHelpAppPageAdded:Z

    .line 4390
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    iget v1, p0, Lcom/android/launcher2/HomeView;->screenIndexBeforeHelpAppPageAddition:I

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    .line 4391
    iput v2, p0, Lcom/android/launcher2/HomeView;->screenIndexBeforeHelpAppPageAddition:I

    .line 4392
    iput v2, p0, Lcom/android/launcher2/HomeView;->screenIndexFestivalHelpAppPageAddition:I

    goto :goto_0

    .line 4387
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getNumPages()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/HomeView;->deletePage(I)V

    goto :goto_1
.end method

.method public resetPanelViewport()V
    .locals 3

    .prologue
    .line 3304
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    .line 3305
    sget-object v0, Lcom/android/launcher2/HomeView;->sPanelDrawer:Lcom/android/launcher2/PanelDrawer;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/PanelDrawer;->setViewport(II)V

    .line 3307
    :cond_0
    return-void
.end method

.method public restoreOpenFolder(Landroid/os/Bundle;)Z
    .locals 12
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 3364
    if-eqz p1, :cond_0

    iget-object v9, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    if-nez v9, :cond_1

    .line 3388
    :cond_0
    :goto_0
    return v8

    .line 3367
    :cond_1
    iget-object v9, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getCurrentPage()I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 3368
    .local v7, "v":Landroid/view/View;
    if-eqz v7, :cond_0

    .line 3371
    const-string v9, "launcher.workspace_open_folder_id"

    const-wide/16 v10, -0x1

    invoke-virtual {p1, v9, v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 3372
    .local v4, "openFolderId":J
    iget-object v9, p0, Lcom/android/launcher2/HomeView;->mModel:Lcom/android/launcher2/LauncherModel;

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v10

    sget-object v11, Lcom/android/launcher2/HomeView;->sFolders:Ljava/util/HashMap;

    invoke-virtual {v9, v10, v11, v4, v5}, Lcom/android/launcher2/LauncherModel;->getFolderById(Landroid/content/Context;Ljava/util/HashMap;J)Lcom/android/launcher2/HomeFolderItem;

    move-result-object v2

    .line 3373
    .local v2, "openFolder":Lcom/android/launcher2/HomeFolderItem;
    if-eqz v2, :cond_0

    .line 3376
    iget-object v9, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v9, v2}, Lcom/android/launcher2/Workspace;->restoreOpenFolder(Lcom/android/launcher2/HomeFolderItem;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 3377
    iget-object v9, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v9}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v1

    .line 3378
    .local v1, "folder":Lcom/android/launcher2/Folder;
    if-eqz v1, :cond_0

    .line 3379
    const-string v8, "launcher.workspace_pending_folder_edit_text"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3380
    .local v0, "editText":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 3381
    const-string v8, "launcher.workspace_pending_folder_edit_text_selection_start"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 3382
    .local v6, "selStart":I
    const-string v8, "launcher.workspace_pending_folder_edit_text_selection_end"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 3383
    .local v3, "selEnd":I
    invoke-virtual {v1, v0, v6, v3}, Lcom/android/launcher2/Folder;->restoreText(Ljava/lang/String;II)Z

    .line 3385
    .end local v3    # "selEnd":I
    .end local v6    # "selStart":I
    :cond_2
    const/4 v8, 0x1

    goto :goto_0
.end method

.method public saveCurrentPageOrder()V
    .locals 0

    .prologue
    .line 4298
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->saveScreenInfo()V

    .line 4299
    return-void
.end method

.method public saveFestivalScreenInfo()V
    .locals 3

    .prologue
    .line 4062
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 4070
    :goto_0
    return-void

    .line 4064
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->getFestivalPageManager()Lcom/android/launcher2/FestivalPageManager;

    move-result-object v1

    .line 4065
    .local v1, "fpMgr":Lcom/android/launcher2/FestivalPageManager;
    invoke-virtual {v1}, Lcom/android/launcher2/FestivalPageManager;->getFestivalPageCount()I

    move-result v0

    .line 4069
    .local v0, "festivalChildCount":I
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/android/launcher2/LauncherApplication;->setFestivalScreenCount(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public saveOpenFolderState(Landroid/os/Bundle;)Z
    .locals 6
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 3346
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    if-nez v3, :cond_1

    .line 3360
    :cond_0
    :goto_0
    return v2

    .line 3349
    :cond_1
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v0

    .line 3350
    .local v0, "openFolder":Lcom/android/launcher2/Folder;
    if-eqz v0, :cond_0

    .line 3353
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->isEditingName()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3354
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->getEditTextRegion()Landroid/widget/EditText;

    move-result-object v1

    .line 3355
    .local v1, "text":Landroid/widget/EditText;
    const-string v2, "launcher.workspace_pending_folder_edit_text"

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3356
    const-string v2, "launcher.workspace_pending_folder_edit_text_selection_start"

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3357
    const-string v2, "launcher.workspace_pending_folder_edit_text_selection_end"

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3359
    .end local v1    # "text":Landroid/widget/EditText;
    :cond_2
    const-string v2, "launcher.workspace_open_folder_id"

    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->getInfo()Lcom/android/launcher2/FolderItem;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/launcher2/FolderItem;->getId()J

    move-result-wide v4

    invoke-virtual {p1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 3360
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public saveScreenInfo()V
    .locals 6

    .prologue
    .line 4034
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v4

    if-nez v4, :cond_0

    .line 4048
    :goto_0
    return-void

    .line 4035
    :cond_0
    const/4 v2, 0x0

    .line 4036
    .local v2, "secretChildCount":I
    const/4 v0, 0x0

    .line 4037
    .local v0, "festivalChildCount":I
    sget-boolean v4, Lcom/android/launcher2/Launcher;->sIsSecretModeOn:Z

    if-eqz v4, :cond_1

    .line 4038
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/Launcher;

    invoke-virtual {v4}, Lcom/android/launcher2/Launcher;->getSecretPageManager()Lcom/android/launcher2/SecretPageManager;

    move-result-object v3

    .line 4039
    .local v3, "spMgr":Lcom/android/launcher2/SecretPageManager;
    invoke-virtual {v3}, Lcom/android/launcher2/SecretPageManager;->getSecretPageCount()I

    move-result v2

    .line 4040
    invoke-virtual {v3}, Lcom/android/launcher2/SecretPageManager;->printWorkspaceInfo()V

    .line 4042
    .end local v3    # "spMgr":Lcom/android/launcher2/SecretPageManager;
    :cond_1
    sget-boolean v4, Lcom/android/launcher2/Launcher;->sIsFestivalModeOn:Z

    if-eqz v4, :cond_2

    .line 4043
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/Launcher;

    invoke-virtual {v4}, Lcom/android/launcher2/Launcher;->getFestivalPageManager()Lcom/android/launcher2/FestivalPageManager;

    move-result-object v1

    .line 4044
    .local v1, "fpMgr":Lcom/android/launcher2/FestivalPageManager;
    invoke-virtual {v1}, Lcom/android/launcher2/FestivalPageManager;->getFestivalPageCount()I

    move-result v0

    .line 4045
    invoke-virtual {v1}, Lcom/android/launcher2/FestivalPageManager;->printWorkspaceInfo()V

    .line 4047
    .end local v1    # "fpMgr":Lcom/android/launcher2/FestivalPageManager;
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v5

    sub-int/2addr v5, v2

    sub-int/2addr v5, v0

    invoke-static {v4, v5}, Lcom/android/launcher2/LauncherApplication;->setScreenCount(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public saveSecretScreenInfo()V
    .locals 3

    .prologue
    .line 4051
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 4059
    :goto_0
    return-void

    .line 4053
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->getSecretPageManager()Lcom/android/launcher2/SecretPageManager;

    move-result-object v1

    .line 4054
    .local v1, "spMgr":Lcom/android/launcher2/SecretPageManager;
    invoke-virtual {v1}, Lcom/android/launcher2/SecretPageManager;->getSecretPageCount()I

    move-result v0

    .line 4058
    .local v0, "secretChildCount":I
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/android/launcher2/LauncherApplication;->setSecretScreenCount(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public setCurrentDragItem(Lcom/android/launcher2/BaseItem;)V
    .locals 0
    .param p1, "item"    # Lcom/android/launcher2/BaseItem;

    .prologue
    .line 4652
    iput-object p1, p0, Lcom/android/launcher2/HomeView;->mCurrentDragItem:Lcom/android/launcher2/BaseItem;

    .line 4653
    return-void
.end method

.method public setCurrentResizeWidgetItem(Lcom/android/launcher2/BaseItem;)V
    .locals 0
    .param p1, "item"    # Lcom/android/launcher2/BaseItem;

    .prologue
    .line 4644
    iput-object p1, p0, Lcom/android/launcher2/HomeView;->mCurrentResizeWidgetItem:Lcom/android/launcher2/BaseItem;

    .line 4645
    return-void
.end method

.method public setDarkenViewAlpha(F)V
    .locals 2
    .param p1, "alpha"    # F

    .prologue
    .line 4489
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mDarkenView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 4490
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f008f

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/HomeView;->mDarkenView:Landroid/view/View;

    .line 4492
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mDarkenView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 4493
    return-void
.end method

.method setDestinationNewFolderId(J)V
    .locals 1
    .param p1, "mId"    # J

    .prologue
    .line 1425
    sput-wide p1, Lcom/android/launcher2/HomeView;->mDestinationNewFolderId:J

    .line 1426
    return-void
.end method

.method public setDummyPanelProperties()V
    .locals 13

    .prologue
    const/4 v12, -0x1

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 5282
    iget v7, p0, Lcom/android/launcher2/HomeView;->mPanelBackgroundAlpha:F

    .line 5283
    .local v7, "v":F
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v8

    .line 5285
    .local v8, "w":Lcom/android/launcher2/Workspace;
    if-eqz v8, :cond_1

    .line 5286
    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->getBackgroundDarken()F

    move-result v1

    .line 5288
    .local v1, "d":F
    sget v9, Lcom/android/launcher2/Workspace;->sPanelBaseEditDarkenAmount:F

    sub-float v10, v11, v7

    invoke-static {v9, v1, v10}, Lcom/android/launcher2/PagedView;->mix(FFF)F

    move-result v0

    .line 5289
    .local v0, "baseDarken":F
    invoke-virtual {v8, v0, v12}, Lcom/android/launcher2/Workspace;->setDummyPanelProperties(FI)V

    .line 5291
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v9

    check-cast v9, Lcom/android/launcher2/Launcher;

    invoke-virtual {v9}, Lcom/android/launcher2/Launcher;->getMenuView()Lcom/android/launcher2/MenuView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/launcher2/MenuView;->getMenuAppsGrid()Lcom/android/launcher2/MenuAppsGrid;

    move-result-object v6

    .line 5292
    .local v6, "m":Lcom/android/launcher2/MenuAppsGrid;
    if-eqz v6, :cond_0

    .line 5293
    sub-float v9, v11, v7

    mul-float/2addr v9, v1

    invoke-virtual {v6, v9, v12}, Lcom/android/launcher2/MenuAppsGrid;->setDummyPanelProperties(FI)V

    .line 5307
    .end local v0    # "baseDarken":F
    .end local v1    # "d":F
    .end local v6    # "m":Lcom/android/launcher2/MenuAppsGrid;
    :cond_0
    :goto_0
    return-void

    .line 5299
    :cond_1
    iget-object v11, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    if-nez v11, :cond_2

    move v5, v9

    .line 5300
    .local v5, "isWorkspaceNull":Z
    :goto_1
    iget-object v11, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-nez v11, :cond_3

    move v4, v9

    .line 5301
    .local v4, "isHotseatNull":Z
    :goto_2
    const/4 v2, 0x1

    .line 5302
    .local v2, "isAllAppsIconNull":Z
    iget-object v11, p0, Lcom/android/launcher2/HomeView;->mHomeContainer:Landroid/view/View;

    if-nez v11, :cond_4

    move v3, v9

    .line 5303
    .local v3, "isHomeContainerNull":Z
    :goto_3
    const-string v9, "Launcher.HomeView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "CRR: isWorkspaceNull = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " isHotseatNull = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " isAllAppsIconNull = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " isHomeContainerNull = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5305
    new-instance v9, Ljava/lang/Throwable;

    invoke-direct {v9}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    goto :goto_0

    .end local v2    # "isAllAppsIconNull":Z
    .end local v3    # "isHomeContainerNull":Z
    .end local v4    # "isHotseatNull":Z
    .end local v5    # "isWorkspaceNull":Z
    :cond_2
    move v5, v10

    .line 5299
    goto :goto_1

    .restart local v5    # "isWorkspaceNull":Z
    :cond_3
    move v4, v10

    .line 5300
    goto :goto_2

    .restart local v2    # "isAllAppsIconNull":Z
    .restart local v4    # "isHotseatNull":Z
    :cond_4
    move v3, v10

    .line 5302
    goto :goto_3
.end method

.method public setEditBar(Lcom/android/launcher2/HomeEditBar;)V
    .locals 0
    .param p1, "editBar"    # Lcom/android/launcher2/HomeEditBar;

    .prologue
    .line 2931
    iput-object p1, p0, Lcom/android/launcher2/HomeView;->mEditBar:Lcom/android/launcher2/HomeEditBar;

    .line 2932
    return-void
.end method

.method public setHomeEditItem(Landroid/view/MenuItem;)V
    .locals 0
    .param p1, "homeEditItem"    # Landroid/view/MenuItem;

    .prologue
    .line 4013
    iput-object p1, p0, Lcom/android/launcher2/HomeView;->mHomeEditItem:Landroid/view/MenuItem;

    .line 4014
    return-void
.end method

.method public setHomeScreenAt(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 4074
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/launcher2/LauncherApplication;->setHomeScreenIndex(Landroid/content/Context;I)V

    .line 4075
    return-void
.end method

.method public setHotseat(Lcom/android/launcher2/Hotseat;)V
    .locals 1
    .param p1, "hotSeat"    # Lcom/android/launcher2/Hotseat;

    .prologue
    .line 2970
    iput-object p1, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    .line 2971
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/Hotseat;->setup(Lcom/android/launcher2/HomeView;)V

    .line 2972
    return-void
.end method

.method public setPage(I)V
    .locals 1
    .param p1, "pageNum"    # I

    .prologue
    .line 4215
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    .line 4216
    return-void
.end method

.method public setPanelBackgroundAlpha(F)V
    .locals 0
    .param p1, "v"    # F

    .prologue
    .line 5274
    iput p1, p0, Lcom/android/launcher2/HomeView;->mPanelBackgroundAlpha:F

    .line 5275
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->setDummyPanelProperties()V

    .line 5276
    return-void
.end method

.method public setPanelViewport(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 3310
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    .line 3311
    sget-object v0, Lcom/android/launcher2/HomeView;->sPanelDrawer:Lcom/android/launcher2/PanelDrawer;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/PanelDrawer;->setViewport(II)V

    .line 3313
    :cond_0
    return-void
.end method

.method public setQuickLaunch(Lcom/android/launcher2/QuickLaunch;)V
    .locals 1
    .param p1, "quickLaunch"    # Lcom/android/launcher2/QuickLaunch;

    .prologue
    .line 2960
    iput-object p1, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    .line 2961
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/QuickLaunch;->setup(Lcom/android/launcher2/HomeView;)V

    .line 2962
    return-void
.end method

.method public setQuickLaunchCamera(Lcom/android/launcher2/QuickLaunch;)V
    .locals 1
    .param p1, "quickLaunch"    # Lcom/android/launcher2/QuickLaunch;

    .prologue
    .line 2965
    iput-object p1, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    .line 2966
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/QuickLaunch;->setup(Lcom/android/launcher2/HomeView;)V

    .line 2967
    return-void
.end method

.method public setStateTargetFolder(Lcom/android/launcher2/FolderItem;)V
    .locals 2
    .param p1, "folder"    # Lcom/android/launcher2/FolderItem;

    .prologue
    .line 1508
    invoke-interface {p1}, Lcom/android/launcher2/FolderItem;->getId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/launcher2/HomeView;->mTargetFolderId:J

    .line 1509
    return-void
.end method

.method public setTabletHotseat()V
    .locals 3

    .prologue
    .line 5428
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-eqz v1, :cond_0

    .line 5429
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v1}, Lcom/android/launcher2/Hotseat;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 5430
    .local v0, "lp":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e00f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 5431
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e00f7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 5432
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e00f3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 5433
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e00f5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 5434
    const/16 v1, 0x55

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 5435
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Hotseat;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5437
    .end local v0    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method public setVisibilityHomeOptionMenu(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    .line 5367
    const v1, 0x7f0f00c9

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 5368
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 5369
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 5372
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 5373
    if-nez p1, :cond_2

    .line 5374
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/launcher2/HomeScreenOptionMenu;->setMakeActive(Z)V

    .line 5380
    :cond_1
    :goto_0
    return-void

    .line 5376
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getHomeScreenOptionMenu()Lcom/android/launcher2/HomeScreenOptionMenu;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/launcher2/HomeScreenOptionMenu;->setMakeActive(Z)V

    goto :goto_0
.end method

.method public setVisibilityHotseat(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 5383
    const v1, 0x7f0f007b

    invoke-virtual {p0, v1}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 5384
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 5385
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 5387
    :cond_0
    return-void
.end method

.method public setWorkspaceLoading()V
    .locals 1

    .prologue
    .line 4525
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/HomeView;->mWorkspaceLoading:Z

    .line 4526
    return-void
.end method

.method public setupCameraCut(I)V
    .locals 2
    .param p1, "currentOrientation"    # I

    .prologue
    .line 5310
    const v0, 0x7f0f0086

    invoke-virtual {p0, v0}, Lcom/android/launcher2/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/launcher2/HomeView;->mCameraCut:Landroid/widget/ImageView;

    .line 5311
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 5312
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mCameraCut:Landroid/widget/ImageView;

    const v1, 0x7f020009

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 5316
    :goto_0
    return-void

    .line 5314
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mCameraCut:Landroid/widget/ImageView;

    const v1, 0x7f02000a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method showAllApps()V
    .locals 1

    .prologue
    .line 3085
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->showAllApps()V

    .line 3086
    :cond_0
    return-void
.end method

.method public showCameraCut(ZZ)V
    .locals 3
    .param p1, "isShow"    # Z
    .param p2, "withTransition"    # Z

    .prologue
    .line 5319
    if-eqz p2, :cond_1

    .line 5320
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_0

    const/high16 v1, 0x7f040000

    :goto_0
    invoke-static {v2, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 5322
    .local v0, "animation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mCameraCut:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 5326
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :goto_1
    iget-object v2, p0, Lcom/android/launcher2/HomeView;->mCameraCut:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5327
    return-void

    .line 5320
    :cond_0
    const v1, 0x7f040001

    goto :goto_0

    .line 5324
    :cond_1
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mCameraCut:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    goto :goto_1

    .line 5326
    :cond_2
    const/4 v1, 0x4

    goto :goto_2
.end method

.method public showDeleteWorkScreen()V
    .locals 1

    .prologue
    .line 4089
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4094
    :goto_0
    return-void

    .line 4092
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/HomeView;->mIsDeletePopup:Z

    .line 4093
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/android/launcher2/DeleteWorkscreenDialogFragment;->createAndShow(Landroid/app/FragmentManager;)V

    goto :goto_0
.end method

.method public showHomeBottomBar()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5412
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 5413
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHomeBottomBar:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 5414
    :cond_0
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isTabletLayout()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5415
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 5416
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Hotseat;->setVisibility(I)V

    .line 5417
    :cond_1
    return-void
.end method

.method showHotseat(Z)V
    .locals 4
    .param p1, "animated"    # Z

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 3062
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_0

    .line 3063
    if-eqz p1, :cond_1

    .line 3064
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 3069
    :cond_0
    :goto_0
    return-void

    .line 3066
    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Hotseat;->setAlpha(F)V

    goto :goto_0
.end method

.method showOutOfSpaceMessage()V
    .locals 1

    .prologue
    .line 1976
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/launcher2/HomeView;->showOutOfSpaceMessage(Landroid/content/Context;)V

    .line 1977
    return-void
.end method

.method showWorkspace(Z)V
    .locals 7
    .param p1, "animated"    # Z

    .prologue
    const/4 v6, 0x1

    .line 3022
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 3023
    .local v1, "res":Landroid/content/res/Resources;
    const v3, 0x7f0c0005

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 3025
    .local v2, "stagger":I
    iget-boolean v3, p0, Lcom/android/launcher2/HomeView;->sIsHeadlinesHiddenForEditMode:Z

    if-eqz v3, :cond_0

    .line 3026
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    .line 3027
    .local v0, "launcher":Lcom/android/launcher2/Launcher;
    invoke-virtual {v0, v6}, Lcom/android/launcher2/Launcher;->setEnableHeadlines(Z)V

    .line 3028
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/launcher2/HomeView;->sIsHeadlinesHiddenForEditMode:Z

    .line 3031
    .end local v0    # "launcher":Lcom/android/launcher2/Launcher;
    :cond_0
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    sget-object v4, Lcom/android/launcher2/Workspace$State;->NORMAL:Lcom/android/launcher2/Workspace$State;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, p1, v2, v5}, Lcom/android/launcher2/Workspace;->changeState(Lcom/android/launcher2/Workspace$State;ZILcom/android/launcher2/BaseItem;)V

    .line 3034
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->updateRunning()V

    .line 3037
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 3039
    invoke-virtual {p0, v6}, Lcom/android/launcher2/HomeView;->closeQuickViewWorkspace(Z)V

    .line 3040
    return-void
.end method

.method showWorkspaceEditmode(Z)V
    .locals 6
    .param p1, "animated"    # Z

    .prologue
    const/4 v4, 0x1

    .line 3044
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 3045
    .local v1, "res":Landroid/content/res/Resources;
    const v3, 0x7f0c0005

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 3047
    .local v2, "stagger":I
    sget-boolean v3, Lcom/android/launcher2/Launcher;->sIsHeadlinesAppEnable:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/android/launcher2/HomeView;->sIsHeadlinesHiddenForEditMode:Z

    if-nez v3, :cond_0

    .line 3048
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    .line 3049
    .local v0, "launcher":Lcom/android/launcher2/Launcher;
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->setEnableHeadlines(Z)V

    .line 3050
    iput-boolean v4, p0, Lcom/android/launcher2/HomeView;->sIsHeadlinesHiddenForEditMode:Z

    .line 3052
    .end local v0    # "launcher":Lcom/android/launcher2/Launcher;
    :cond_0
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->closeFolder()V

    .line 3054
    invoke-static {v4}, Lcom/android/launcher2/Launcher;->setHomeEditMode(Z)V

    .line 3055
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    sget-object v4, Lcom/android/launcher2/Workspace$State;->EDIT:Lcom/android/launcher2/Workspace$State;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, p1, v2, v5}, Lcom/android/launcher2/Workspace;->changeState(Lcom/android/launcher2/Workspace$State;ZILcom/android/launcher2/BaseItem;)V

    .line 3056
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    .line 5054
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/HomeView;->mWaitingForResult:Z

    .line 5055
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    invoke-virtual {v0, p1, p2, p0}, Lcom/android/launcher2/Launcher;->startActivityForResult(Landroid/content/Intent;ILcom/android/launcher2/Launcher$ActivityResultCallback;)V

    .line 5056
    return-void
.end method

.method startActivityForResultSafely(Landroid/content/Intent;I)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    const v3, 0x7f100002

    const/4 v2, 0x0

    .line 2692
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/android/launcher2/HomeView;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2701
    :goto_0
    return-void

    .line 2693
    :catch_0
    move-exception v0

    .line 2694
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2695
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :catch_1
    move-exception v0

    .line 2696
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2697
    const-string v1, "Launcher.HomeView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Launcher does not have the permission to launch "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Make sure to create a MAIN intent-filter for the corresponding activity "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "or use the exported attribute for this activity."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method startApplicationDetailsActivity(Landroid/content/ComponentName;)V
    .locals 5
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 2646
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 2647
    .local v1, "packageName":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.settings.APPLICATION_DETAILS_SETTINGS"

    const-string v3, "package"

    const/4 v4, 0x0

    invoke-static {v3, v1, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2649
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10800000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2650
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2651
    return-void
.end method

.method startApplicationUninstallActivity(Lcom/android/launcher2/AppItem;)V
    .locals 6
    .param p1, "app"    # Lcom/android/launcher2/AppItem;

    .prologue
    .line 2654
    iget-object v4, p1, Lcom/android/launcher2/AppItem;->mComponentName:Landroid/content/ComponentName;

    if-eqz v4, :cond_0

    .line 2655
    iget-boolean v4, p1, Lcom/android/launcher2/AppItem;->mSystemApp:Z

    if-eqz v4, :cond_1

    .line 2658
    const v2, 0x7f100068

    .line 2659
    .local v2, "messageId":I
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 2670
    .end local v2    # "messageId":I
    :cond_0
    :goto_0
    return-void

    .line 2661
    :cond_1
    iget-object v4, p1, Lcom/android/launcher2/AppItem;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 2662
    .local v3, "packageName":Ljava/lang/String;
    iget-object v4, p1, Lcom/android/launcher2/AppItem;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 2663
    .local v0, "className":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.DELETE"

    const-string v5, "package"

    invoke-static {v5, v3, v0}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2665
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v4, 0x10800000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2667
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public startBinding()V
    .locals 5

    .prologue
    .line 3206
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    .line 3208
    .local v3, "workspace":Lcom/android/launcher2/Workspace;
    if-nez v3, :cond_1

    .line 3232
    :cond_0
    :goto_0
    return-void

    .line 3213
    :cond_1
    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->abortConfigChanges()V

    .line 3214
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/launcher2/HomeView;->mWorkspaceLoading:Z

    .line 3216
    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    .line 3217
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 3219
    invoke-virtual {v3, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/CellLayout;

    .line 3220
    .local v2, "layoutParent":Lcom/android/launcher2/CellLayout;
    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->removeAllViewsInLayout()V

    .line 3217
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3223
    .end local v2    # "layoutParent":Lcom/android/launcher2/CellLayout;
    :cond_2
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    if-eqz v4, :cond_3

    .line 3224
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mHotseat:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v4}, Lcom/android/launcher2/Hotseat;->resetLayout()V

    .line 3226
    :cond_3
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    if-eqz v4, :cond_4

    .line 3227
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mQuickLaunch:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v4}, Lcom/android/launcher2/QuickLaunch;->resetLayout()V

    .line 3229
    :cond_4
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    if-eqz v4, :cond_0

    .line 3230
    iget-object v4, p0, Lcom/android/launcher2/HomeView;->mQuickLaunchCamera:Lcom/android/launcher2/QuickLaunch;

    invoke-virtual {v4}, Lcom/android/launcher2/QuickLaunch;->resetLayout()V

    goto :goto_0
.end method

.method startWallpaper(I)V
    .locals 8
    .param p1, "mode"    # I

    .prologue
    const v7, 0x7f100002

    const/4 v6, 0x0

    const/16 v5, 0xa

    .line 2450
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->exitWidgetResizeMode()V

    .line 2454
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.wallpapers.WallpaperPickerActivity"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2455
    .local v0, "chooser":Landroid/content/Intent;
    const-string v3, "type"

    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2457
    if-nez p1, :cond_1

    .line 2458
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.SET_WALLPAPER"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2459
    .local v2, "pickWallpaper":Landroid/content/Intent;
    const-string v3, "android.intent.extra.INTENT"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2461
    sget-boolean v3, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v3, :cond_0

    .line 2462
    const-string v3, "mode"

    const-string v4, "Guide_Mode"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2463
    invoke-virtual {p0, v0, v5}, Lcom/android/launcher2/HomeView;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2481
    .end local v2    # "pickWallpaper":Landroid/content/Intent;
    :goto_0
    return-void

    .line 2468
    .restart local v2    # "pickWallpaper":Landroid/content/Intent;
    :cond_0
    :try_start_0
    const-string v3, "mode"

    const-string v4, "null"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2469
    const/16 v3, 0xa

    invoke-virtual {p0, v0, v3}, Lcom/android/launcher2/HomeView;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2470
    :catch_0
    move-exception v1

    .line 2471
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2476
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    .end local v2    # "pickWallpaper":Landroid/content/Intent;
    :cond_1
    const/16 v3, 0xa

    :try_start_1
    invoke-virtual {p0, v0, v3}, Lcom/android/launcher2/HomeView;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 2477
    :catch_1
    move-exception v1

    .line 2478
    .restart local v1    # "e":Landroid/content/ActivityNotFoundException;
    iget-object v3, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public updateGlobalSearchIcon()Z
    .locals 1

    .prologue
    .line 4197
    const/4 v0, 0x0

    return v0
.end method

.method protected updateRunning()V
    .locals 12

    .prologue
    const-wide/16 v2, 0x4e20

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 1892
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWidgetsToAdvance:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    move v0, v1

    .line 1893
    .local v0, "autoAdvanceRunning":Z
    :goto_0
    iget-boolean v5, p0, Lcom/android/launcher2/HomeView;->mAutoAdvanceRunning:Z

    if-eq v0, v5, :cond_0

    .line 1894
    iput-boolean v0, p0, Lcom/android/launcher2/HomeView;->mAutoAdvanceRunning:Z

    .line 1895
    if-eqz v0, :cond_3

    .line 1896
    iget-wide v4, p0, Lcom/android/launcher2/HomeView;->mAutoAdvanceTimeLeft:J

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-nez v1, :cond_2

    .line 1897
    .local v2, "delay":J
    :goto_1
    invoke-direct {p0, v2, v3}, Lcom/android/launcher2/HomeView;->sendAdvanceMessage(J)V

    .line 1907
    .end local v2    # "delay":J
    :cond_0
    :goto_2
    return-void

    .end local v0    # "autoAdvanceRunning":Z
    :cond_1
    move v0, v4

    .line 1892
    goto :goto_0

    .line 1896
    .restart local v0    # "autoAdvanceRunning":Z
    :cond_2
    iget-wide v2, p0, Lcom/android/launcher2/HomeView;->mAutoAdvanceTimeLeft:J

    goto :goto_1

    .line 1899
    :cond_3
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mWidgetsToAdvance:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 1900
    const-wide/16 v6, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/android/launcher2/HomeView;->mAutoAdvanceSentTime:J

    sub-long/2addr v8, v10

    sub-long v8, v2, v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/launcher2/HomeView;->mAutoAdvanceTimeLeft:J

    .line 1903
    :cond_4
    iget-object v5, p0, Lcom/android/launcher2/HomeView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1904
    iget-object v1, p0, Lcom/android/launcher2/HomeView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_2
.end method

.method public updateWallpaperOffsets()V
    .locals 1

    .prologue
    .line 4497
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->updateWallpaperOffsets()V

    .line 4498
    return-void
.end method

.method updateWallpaperVisibility(Z)V
    .locals 4
    .param p1, "visible"    # Z

    .prologue
    const/high16 v2, 0x100000

    .line 3013
    if-eqz p1, :cond_1

    move v1, v2

    .line 3014
    .local v1, "wpflags":I
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int v0, v3, v2

    .line 3016
    .local v0, "curflags":I
    if-eq v1, v0, :cond_0

    .line 3017
    invoke-virtual {p0}, Lcom/android/launcher2/HomeView;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Landroid/view/Window;->setFlags(II)V

    .line 3019
    :cond_0
    return-void

    .line 3013
    .end local v0    # "curflags":I
    .end local v1    # "wpflags":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public wallpaperHack()V
    .locals 2

    .prologue
    .line 4336
    iget-object v0, p0, Lcom/android/launcher2/HomeView;->mWorkspace:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->setVisibility(I)V

    .line 4337
    return-void
.end method
