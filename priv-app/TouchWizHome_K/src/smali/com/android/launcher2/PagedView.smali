.class public abstract Lcom/android/launcher2/PagedView;
.super Landroid/view/ViewGroup;
.source "PagedView.java"

# interfaces
.implements Landroid/hardware/motion/MRListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/launcher2/PagedView$HoverScrollHandler;,
        Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;,
        Lcom/android/launcher2/PagedView$DelaySnapToPage;,
        Lcom/android/launcher2/PagedView$LayerOptions;,
        Lcom/android/launcher2/PagedView$SavedState;,
        Lcom/android/launcher2/PagedView$ScrollInterpolator;,
        Lcom/android/launcher2/PagedView$PageSwitchListener;,
        Lcom/android/launcher2/PagedView$PageInfo;,
        Lcom/android/launcher2/PagedView$TransitionEffect;
    }
.end annotation


# static fields
.field protected static final ALPHA_QUANTIZE_LEVEL:F = 1.0E-4f

.field private static final CAMERA_INTENT:Landroid/content/Intent;

.field private static final DEBUG:Z = false

.field private static final DEBUGGABLE:Z

.field public static DEVICE_NAME:Ljava/lang/String; = null

.field public static final DIR_LEFT:I = 0x2

.field public static final DIR_RIGHT:I = 0x3

.field public static final DIR_SHORTEST:I = 0x1

.field private static final HEADLINES_INTENT:Landroid/content/Intent;

.field private static final HOVERSCROLL_SPEED:J = 0xc8L

.field public static final INVALID_DIR:I = 0x0

.field protected static final INVALID_PAGE:I = -0x80000000

.field protected static final INVALID_POINTER:I = -0x1

.field private static final MINIMUM_SNAP_VELOCITY:I = 0x898

.field private static final MIN_FLING_VELOCITY:I = 0xfa

.field private static final MOTION_ENGINE_LOG:Ljava/lang/String; = "MotionEngine"

.field protected static final NANOTIME_DIV:F = 1.0E9f

.field private static final OVERSCROLL_ACCELERATE_FACTOR:F = 2.0f

.field private static final OVERSCROLL_DAMP_FACTOR:F = 0.14f

.field public static final PAGE_BG_ALPHA:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/android/launcher2/PagedView;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final PAGE_HOVER_SCROLL:I = 0x1

.field public static final PAGE_ZOOM:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/android/launcher2/PagedView;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final PANNING_THRESHOLD_MULTIPLIER:I = 0xf

.field private static final PANNING_THRESHOLD_SHIFTER:I = 0x28

.field protected static final SNAP_STATE_ADJACENT:I = 0x0

.field protected static final SNAP_STATE_FIRST_TO_LAST:I = 0x1

.field protected static final SNAP_STATE_LAST_TO_FIRST:I = 0x2

.field private static final TAG:Ljava/lang/String; = "Launcher.PagedView"

.field protected static final TOUCH_STATE_DRIFTING:I = 0x4

.field protected static final TOUCH_STATE_NEXT_PAGE:I = 0x3

.field protected static final TOUCH_STATE_PREV_PAGE:I = 0x2

.field protected static final TOUCH_STATE_REST:I = 0x0

.field protected static final TOUCH_STATE_SCROLLING:I = 0x1

.field private static final advancedDriftSlop:I

.field private static final advancedTouchSlop:I

.field private static busBooster:Landroid/os/DVFSHelper;

.field private static final chipset:Ljava/lang/String;

.field private static cpuBooster:Landroid/os/DVFSHelper;

.field private static cpuMaxBooster:Landroid/os/DVFSHelper;

.field private static gpuBooster:Landroid/os/DVFSHelper;

.field private static mHoverScrollVerticalPaddingMatchParent:I

.field private static mIsPageFastMoving:Z

.field private static sInclusivePages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sIsMotionEngineEnabled:Z


# instance fields
.field protected final ENABLE_DVFS_ONSCROLL:Z

.field protected final ENABLE_SLOP_JUMP:Z

.field protected final ENABLE_TOUCH_PREDICTION:Z

.field private final MIN_LENGTH_FOR_FLING:I

.field protected final PAGE_SNAP_ANIMATION_DURATION:I

.field private USE_SET_INTEGRATOR_HAPTIC:Z

.field protected isShowCamera:Z

.field private mAccumulatedDelta:D

.field private mAllowLongPress:Z

.field protected mAllowOverScroll:Z

.field protected mBlockHardwareLayers:Z

.field protected mCellCountX:I

.field protected mCellCountY:I

.field protected mCellGapX:I

.field protected mCellGapY:I

.field protected mContentIsRefreshable:Z

.field private mCurrentPage:I

.field protected mCurrentRotation:I

.field protected mCurrentTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

.field protected mDefaultTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

.field private mDeferLoadAssociatedPagesUntilScrollCompletes:Z

.field protected mDeferScrollUpdate:Z

.field private final mDelaySnapToPage:Lcom/android/launcher2/PagedView$DelaySnapToPage;

.field protected mDirtyPageContent:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mDownMotionX:F

.field protected mDriftSlop:I

.field protected mFadeInAdjacentScreens:Z

.field protected mFirstLayout:Z

.field private mHardwarePages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected mHideItems:Z

.field protected mHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

.field private mHoverScrollBottom:I

.field private mHoverScrollDir:I

.field private mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

.field private mHoverScrollRightPadding:I

.field private mHoverScrollTop:I

.field private mHoverScrollVerticalPadding:I

.field private mHoverScrollWidth:I

.field mIgnoreSecretPage:Z

.field private mInGesture:Z

.field protected mIsDataReady:Z

.field protected mIsOverScrolled:Z

.field protected mIsPageMoving:Z

.field private mLaidOutAfterViewTreeDirty:Z

.field private mLastDragLocationX:I

.field private mLastDragLocationY:I

.field private mLastFocusDir:I

.field private mLastHit:I

.field protected mLastMotionX:F

.field protected mLastMotionXRemainder:F

.field protected mLastMotionY:F

.field protected mLastPageNotify:I

.field private mLastScreenCenter:I

.field mLastScrollX:I

.field mLongClickListener:Landroid/view/View$OnLongClickListener;

.field protected mMaxScrollX:I

.field private mMaximumVelocity:I

.field private mMotionListenerRegistered:Z

.field private mMotionSensorManager:Landroid/hardware/motion/MotionRecognitionManager;

.field protected mNextPage:I

.field protected mPageBackgroundAlpha:F

.field private mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

.field protected mPageIndicatorTop:I

.field protected mPagePaddingBottom:I

.field protected mPagePaddingLeft:I

.field protected mPagePaddingRight:I

.field protected mPagePaddingTop:I

.field private final mPagePreviewSnapDelay:Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;

.field protected mPageSpacing:I

.field protected mPageSpacingHint:I

.field private mPageSwitchListener:Lcom/android/launcher2/PagedView$PageSwitchListener;

.field protected mPageTransformsDirty:Z

.field protected mPageZoom:F

.field protected mPagesPool:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/launcher2/PagedView$PageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPagingTouchSlop:I

.field protected mPanningBasis:D

.field protected mPanningStopDelta:D

.field protected mPendingSavedState:Lcom/android/launcher2/PagedView$SavedState;

.field private mScrollByHover:Z

.field protected mScroller:Landroid/widget/Scroller;

.field protected mSmoothingTime:F

.field protected mSnapState:I

.field private mSnapToPageAfterLayout:I

.field private mSnapToPageAfterLayoutDir:I

.field private mSnapToPageAfterLayoutDur:I

.field private mSnapToPageAfterLayoutMustAnim:Z

.field protected mSnapVelocity:I

.field protected mTotalMotionX:F

.field protected mTotalPredictionX:F

.field protected mTouchDownPointX:F

.field protected mTouchSlop:I

.field protected mTouchState:I

.field protected mTouchUpPointX:F

.field protected mTouchX:F

.field protected mUnboundedScrollX:I

.field protected mUsePagingTouchSlop:Z

.field protected mUseSlopJump:Z

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field protected mVisibleAllCounts:I

.field protected mVisiblePages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/launcher2/PagedView$PageInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected motionTrackingIsCurrent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 104
    invoke-static {}, Lcom/android/launcher2/Utilities;->DEBUGGABLE()Z

    move-result v0

    sput-boolean v0, Lcom/android/launcher2/PagedView;->DEBUGGABLE:Z

    .line 105
    sput-object v2, Lcom/android/launcher2/PagedView;->cpuBooster:Landroid/os/DVFSHelper;

    .line 106
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/launcher2/PagedView;->DEVICE_NAME:Ljava/lang/String;

    .line 107
    sput-object v2, Lcom/android/launcher2/PagedView;->busBooster:Landroid/os/DVFSHelper;

    .line 108
    sput-object v2, Lcom/android/launcher2/PagedView;->cpuMaxBooster:Landroid/os/DVFSHelper;

    .line 109
    sput-object v2, Lcom/android/launcher2/PagedView;->gpuBooster:Landroid/os/DVFSHelper;

    .line 110
    sget-object v0, Landroid/os/Build;->BOARD:Ljava/lang/String;

    sput-object v0, Lcom/android/launcher2/PagedView;->chipset:Ljava/lang/String;

    .line 115
    new-instance v0, Lcom/android/launcher2/PagedView$1;

    const-class v1, Ljava/lang/Float;

    const-string v2, "pageZoom"

    invoke-direct {v0, v1, v2}, Lcom/android/launcher2/PagedView$1;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/android/launcher2/PagedView;->PAGE_ZOOM:Landroid/util/Property;

    .line 120
    new-instance v0, Lcom/android/launcher2/PagedView$2;

    const-class v1, Ljava/lang/Float;

    const-string v2, "pageBackgroundAlpha"

    invoke-direct {v0, v1, v2}, Lcom/android/launcher2/PagedView$2;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/android/launcher2/PagedView;->PAGE_BG_ALPHA:Landroid/util/Property;

    .line 140
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sput-object v0, Lcom/android/launcher2/PagedView;->CAMERA_INTENT:Landroid/content/Intent;

    .line 141
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sput-object v0, Lcom/android/launcher2/PagedView;->HEADLINES_INTENT:Landroid/content/Intent;

    .line 310
    const v0, 0x7fffffff

    sput v0, Lcom/android/launcher2/PagedView;->mHoverScrollVerticalPaddingMatchParent:I

    .line 320
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher2/PagedView;->mIsPageFastMoving:Z

    .line 2881
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/launcher2/PagedView;->sInclusivePages:Ljava/util/ArrayList;

    .line 3618
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/launcher2/PagedView;->sIsMotionEngineEnabled:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 331
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 332
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 335
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 336
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 340
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 130
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0c0024

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->PAGE_SNAP_ANIMATION_DURATION:I

    .line 150
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mUseSlopJump:Z

    .line 151
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->ENABLE_TOUCH_PREDICTION:Z

    .line 152
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->ENABLE_SLOP_JUMP:Z

    .line 153
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mTotalPredictionX:F

    .line 155
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mFirstLayout:Z

    .line 156
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->ENABLE_DVFS_ONSCROLL:Z

    .line 159
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mLastPageNotify:I

    .line 160
    const/high16 v8, -0x80000000

    iput v8, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    .line 172
    const/4 v8, -0x1

    iput v8, p0, Lcom/android/launcher2/PagedView;->mLastScreenCenter:I

    .line 184
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mTouchState:I

    .line 188
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mInGesture:Z

    .line 192
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mAllowLongPress:Z

    .line 205
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mCellCountX:I

    .line 206
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mCellCountY:I

    .line 207
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mAllowOverScroll:Z

    .line 209
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mHideItems:Z

    .line 210
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mIsOverScrolled:Z

    .line 227
    sget-object v8, Lcom/android/launcher2/PagedView$TransitionEffect;->NONE:Lcom/android/launcher2/PagedView$TransitionEffect;

    iput-object v8, p0, Lcom/android/launcher2/PagedView;->mDefaultTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

    .line 228
    sget-object v8, Lcom/android/launcher2/PagedView$TransitionEffect;->NONE:Lcom/android/launcher2/PagedView$TransitionEffect;

    iput-object v8, p0, Lcom/android/launcher2/PagedView;->mCurrentTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

    .line 251
    const/high16 v8, -0x80000000

    iput v8, p0, Lcom/android/launcher2/PagedView;->mLastScrollX:I

    .line 252
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/launcher2/PagedView;->mVisiblePages:Ljava/util/ArrayList;

    .line 253
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/launcher2/PagedView;->mPagesPool:Ljava/util/ArrayList;

    .line 263
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mContentIsRefreshable:Z

    .line 266
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mFadeInAdjacentScreens:Z

    .line 270
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mUsePagingTouchSlop:Z

    .line 274
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mDeferScrollUpdate:Z

    .line 276
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mIsPageMoving:Z

    .line 277
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mBlockHardwareLayers:Z

    .line 280
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mIsDataReady:Z

    .line 286
    const/high16 v8, -0x80000000

    iput v8, p0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayout:I

    .line 287
    const/4 v8, 0x1

    iput v8, p0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayoutDir:I

    .line 288
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayoutDur:I

    .line 289
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayoutMustAnim:Z

    .line 297
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mPageTransformsDirty:Z

    .line 303
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mScrollByHover:Z

    .line 304
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollDir:I

    .line 305
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollWidth:I

    .line 306
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollTop:I

    .line 307
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollBottom:I

    .line 308
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollRightPadding:I

    .line 309
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollVerticalPadding:I

    .line 315
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mVisibleAllCounts:I

    .line 322
    new-instance v8, Lcom/android/launcher2/PagedView$DelaySnapToPage;

    const/4 v9, 0x0

    invoke-direct {v8, p0, v9}, Lcom/android/launcher2/PagedView$DelaySnapToPage;-><init>(Lcom/android/launcher2/PagedView;Lcom/android/launcher2/PagedView$1;)V

    iput-object v8, p0, Lcom/android/launcher2/PagedView;->mDelaySnapToPage:Lcom/android/launcher2/PagedView$DelaySnapToPage;

    .line 323
    new-instance v8, Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;

    invoke-direct {v8, p0}, Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;-><init>(Lcom/android/launcher2/PagedView;)V

    iput-object v8, p0, Lcom/android/launcher2/PagedView;->mPagePreviewSnapDelay:Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;

    .line 324
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v8

    const-string v9, "SEC_FLOATING_FEATURE_FRAMEWORK_ENABLE_INTEGRATOR_HAPTIC"

    invoke-virtual {v8, v9}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->USE_SET_INTEGRATOR_HAPTIC:Z

    .line 993
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mIgnoreSecretPage:Z

    .line 2453
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->motionTrackingIsCurrent:Z

    .line 2480
    const/4 v8, 0x1

    iput v8, p0, Lcom/android/launcher2/PagedView;->mLastFocusDir:I

    .line 3352
    const/high16 v8, 0x3f800000    # 1.0f

    iput v8, p0, Lcom/android/launcher2/PagedView;->mPageZoom:F

    .line 3366
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mPageBackgroundAlpha:F

    .line 3385
    const/4 v8, -0x1

    iput v8, p0, Lcom/android/launcher2/PagedView;->mLastHit:I

    .line 3619
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/android/launcher2/PagedView;->mMotionSensorManager:Landroid/hardware/motion/MotionRecognitionManager;

    .line 3620
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mMotionListenerRegistered:Z

    .line 3621
    const-wide/high16 v8, 0x404e000000000000L    # 60.0

    iput-wide v8, p0, Lcom/android/launcher2/PagedView;->mPanningBasis:D

    .line 3622
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    .line 3625
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Lcom/android/launcher2/PagedView;->mPanningStopDelta:D

    .line 3799
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mLaidOutAfterViewTreeDirty:Z

    .line 3809
    new-instance v8, Lcom/android/launcher2/PagedView$5;

    invoke-direct {v8, p0}, Lcom/android/launcher2/PagedView$5;-><init>(Lcom/android/launcher2/PagedView;)V

    iput-object v8, p0, Lcom/android/launcher2/PagedView;->mHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    .line 342
    sget-object v8, Lcom/sec/android/app/launcher/R$styleable;->PagedView:[I

    const/4 v9, 0x0

    invoke-virtual {p1, p2, v8, p3, v9}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 344
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v8, 0x2

    const/4 v9, 0x4

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mCellCountX:I

    .line 345
    const/4 v8, 0x3

    const/4 v9, 0x4

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mCellCountY:I

    .line 346
    const/16 v8, 0x8

    const/4 v9, -0x1

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mPageSpacingHint:I

    .line 347
    const/4 v8, 0x4

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mPagePaddingTop:I

    .line 349
    const/4 v8, 0x5

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mPagePaddingBottom:I

    .line 351
    const/4 v8, 0x6

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mPagePaddingLeft:I

    .line 353
    const/4 v8, 0x7

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mPagePaddingRight:I

    .line 355
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mCellGapX:I

    .line 357
    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mCellGapY:I

    .line 360
    const/16 v8, 0x9

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v6

    .line 361
    .local v6, "pageIndicatorTop":I
    const/16 v8, 0xa

    const/16 v9, 0xa

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 362
    .local v4, "pageIndicatorGap":I
    const/16 v8, 0xb

    const/16 v9, 0xb

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    .line 363
    .local v5, "pageIndicatorMaxVisible":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 365
    iput v6, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorTop:I

    .line 366
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/android/launcher2/PagedView;->setHapticFeedbackEnabled(Z)V

    .line 368
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/launcher2/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    .line 369
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    const/16 v9, 0x20

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 370
    new-instance v8, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v9

    new-instance v10, Lcom/android/launcher2/PagedView$ScrollInterpolator;

    invoke-direct {v10}, Lcom/android/launcher2/PagedView$ScrollInterpolator;-><init>()V

    invoke-direct {v8, v9, v10}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v8, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    .line 371
    const/4 v8, 0x0

    iput v8, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    .line 373
    sget-object v8, Lcom/android/launcher2/PagedView;->CAMERA_INTENT:Landroid/content/Intent;

    new-instance v9, Landroid/content/ComponentName;

    const-string v10, "com.sec.android.app.camera"

    const-string v11, "com.sec.android.app.camera.Camera"

    invoke-direct {v9, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 374
    sget-object v8, Lcom/android/launcher2/PagedView;->CAMERA_INTENT:Landroid/content/Intent;

    const-string v9, "android.intent.action.MAIN"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 375
    sget-object v8, Lcom/android/launcher2/PagedView;->CAMERA_INTENT:Landroid/content/Intent;

    const-string v9, "android.intent.category.LAUNCHER"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 378
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 379
    .local v7, "pm":Landroid/content/pm/PackageManager;
    const/4 v3, 0x0

    .line 381
    .local v3, "newBriefingApp_installed":Z
    :try_start_0
    const-string v8, "flipboard.boxer.app"

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 382
    const/4 v3, 0x1

    .line 386
    :goto_0
    if-eqz v3, :cond_1

    .line 387
    sget-object v8, Lcom/android/launcher2/PagedView;->HEADLINES_INTENT:Landroid/content/Intent;

    new-instance v9, Landroid/content/ComponentName;

    const-string v10, "flipboard.boxer.app"

    const-string v11, "flipboard.boxer.gui.LaunchActivity"

    invoke-direct {v9, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 390
    :goto_1
    sget-object v8, Lcom/android/launcher2/PagedView;->HEADLINES_INTENT:Landroid/content/Intent;

    const-string v9, "android.intent.action.MAIN"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 391
    sget-object v8, Lcom/android/launcher2/PagedView;->HEADLINES_INTENT:Landroid/content/Intent;

    const-string v9, "android.intent.category.LAUNCHER"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 392
    sget-object v8, Lcom/android/launcher2/PagedView;->HEADLINES_INTENT:Landroid/content/Intent;

    const-string v9, "fromHome"

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 394
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/launcher2/PagedView;->mHardwarePages:Ljava/util/ArrayList;

    .line 396
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 397
    .local v1, "configuration":Landroid/view/ViewConfiguration;
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e00e5

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mTouchSlop:I

    .line 398
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e00e6

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mDriftSlop:I

    .line 399
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mPagingTouchSlop:I

    .line 400
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mMaximumVelocity:I

    .line 402
    invoke-virtual {p0, p0, v6, v4, v5}, Lcom/android/launcher2/PagedView;->getPageIndicatorManager(Lcom/android/launcher2/PagedView;III)Lcom/android/launcher2/PageIndicatorManager;

    move-result-object v8

    iput-object v8, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    .line 404
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0197

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollVerticalPadding:I

    .line 405
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0009

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    sput v8, Lcom/android/launcher2/PagedView;->mHoverScrollVerticalPaddingMatchParent:I

    .line 407
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/launcher2/LauncherApplication;->getCpuBoosterObject()Landroid/os/DVFSHelper;

    move-result-object v8

    sput-object v8, Lcom/android/launcher2/PagedView;->cpuBooster:Landroid/os/DVFSHelper;

    .line 408
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/launcher2/LauncherApplication;->getBusBoosterObject()Landroid/os/DVFSHelper;

    move-result-object v8

    sput-object v8, Lcom/android/launcher2/PagedView;->busBooster:Landroid/os/DVFSHelper;

    .line 409
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/launcher2/LauncherApplication;->getCpuMaxBoosterObject()Landroid/os/DVFSHelper;

    move-result-object v8

    sput-object v8, Lcom/android/launcher2/PagedView;->cpuMaxBooster:Landroid/os/DVFSHelper;

    .line 410
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getInst()Lcom/android/launcher2/LauncherApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/launcher2/LauncherApplication;->getGpuBoosterObject()Landroid/os/DVFSHelper;

    move-result-object v8

    sput-object v8, Lcom/android/launcher2/PagedView;->gpuBooster:Landroid/os/DVFSHelper;

    .line 412
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e00e2

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->MIN_LENGTH_FOR_FLING:I

    .line 413
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e00e3

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, p0, Lcom/android/launcher2/PagedView;->mSnapVelocity:I

    .line 414
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/android/launcher2/PagedView;->setSaveEnabled(Z)V

    .line 415
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    invoke-virtual {p0, v8}, Lcom/android/launcher2/PagedView;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 416
    sget-boolean v8, Lcom/android/launcher2/PagedView;->sIsMotionEngineEnabled:Z

    if-eqz v8, :cond_0

    .line 417
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->getMotionSensor()V

    .line 418
    :cond_0
    return-void

    .line 383
    .end local v1    # "configuration":Landroid/view/ViewConfiguration;
    :catch_0
    move-exception v2

    .line 384
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 389
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    sget-object v8, Lcom/android/launcher2/PagedView;->HEADLINES_INTENT:Landroid/content/Intent;

    new-instance v9, Landroid/content/ComponentName;

    const-string v10, "com.samsung.android.app.headlines"

    const-string v11, "com.samsung.android.app.headlines.activity.HeadlinesActivity"

    invoke-direct {v9, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto/16 :goto_1
.end method

.method static synthetic access$200()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/android/launcher2/PagedView;->HEADLINES_INTENT:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/launcher2/PagedView;III)V
    .locals 0
    .param p0, "x0"    # Lcom/android/launcher2/PagedView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 102
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher2/PagedView;->snapToPageInternal(III)V

    return-void
.end method

.method static synthetic access$600(Lcom/android/launcher2/PagedView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/PagedView;

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->mScrollByHover:Z

    return v0
.end method

.method static synthetic access$700(Lcom/android/launcher2/PagedView;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/PagedView;

    .prologue
    .line 102
    iget v0, p0, Lcom/android/launcher2/PagedView;->mHoverScrollDir:I

    return v0
.end method

.method static synthetic access$800(Lcom/android/launcher2/PagedView;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/launcher2/PagedView;

    .prologue
    .line 102
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    return v0
.end method

.method private acquireVelocityTrackerAndAddMovement(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 2436
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 2437
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 2439
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2440
    return-void
.end method

.method private canOverScroll()Z
    .locals 1

    .prologue
    .line 806
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->mAllowOverScroll:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isFastScrolling()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private canOverScrollForHeadlines()Z
    .locals 2

    .prologue
    .line 811
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    .line 812
    .local v0, "launcher":Lcom/android/launcher2/Launcher;
    sget-boolean v1, Lcom/android/launcher2/Launcher;->sIsHeadlinesAppEnable:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getHomeView()Lcom/android/launcher2/HomeView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 813
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v1

    if-nez v1, :cond_0

    .line 814
    const/4 v1, 0x0

    .line 816
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private distanceInfluenceForSnapDuration(F)F
    .locals 4
    .param p1, "f"    # F

    .prologue
    .line 2608
    const/high16 v0, 0x3f000000    # 0.5f

    sub-float/2addr p1, v0

    .line 2609
    float-to-double v0, p1

    const-wide v2, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v0, v2

    double-to-float p1, v0

    .line 2610
    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private drawPage(Landroid/graphics/Canvas;Lcom/android/launcher2/PagedView$PageInfo;J)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "pageInfo"    # Lcom/android/launcher2/PagedView$PageInfo;
    .param p3, "drawingTime"    # J

    .prologue
    .line 1483
    iget v3, p2, Lcom/android/launcher2/PagedView$PageInfo;->mIndex:I

    invoke-virtual {p0, v3}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    .line 1484
    .local v1, "page":Landroid/view/View;
    if-nez v1, :cond_0

    .line 1489
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Draw page is NULL. Report this."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1491
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_2

    instance-of v3, v1, Lcom/android/launcher2/CellLayout;

    if-eqz v3, :cond_2

    move-object v3, v1

    check-cast v3, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v3}, Lcom/android/launcher2/CellLayout;->getChildrenAlpha()F

    move-result v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_2

    .line 1492
    const-string v3, "Launcher.PagedView"

    const-string v4, "Page visibility not in sync with list of visible pages. This MUST be investigated"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1518
    :cond_1
    :goto_0
    return-void

    .line 1495
    :cond_2
    iget-boolean v3, p0, Lcom/android/launcher2/PagedView;->mPageTransformsDirty:Z

    if-eqz v3, :cond_4

    .line 1496
    instance-of v3, v1, Lcom/android/launcher2/CellLayout;

    if-eqz v3, :cond_3

    move-object v3, v1

    .line 1497
    check-cast v3, Lcom/android/launcher2/CellLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/android/launcher2/CellLayout;->setNoNeedToDraw(Z)V

    .line 1499
    :cond_3
    invoke-direct {p0, p2, p3, p4}, Lcom/android/launcher2/PagedView;->transformPage(Lcom/android/launcher2/PagedView$PageInfo;J)V

    .line 1502
    :cond_4
    instance-of v3, v1, Lcom/android/launcher2/CellLayout;

    if-eqz v3, :cond_5

    move-object v3, v1

    check-cast v3, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v3}, Lcom/android/launcher2/CellLayout;->isNoNeedToDraw()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1505
    :cond_5
    invoke-virtual {p0, p2}, Lcom/android/launcher2/PagedView;->getScrollProgress(Lcom/android/launcher2/PagedView$PageInfo;)F

    move-result v2

    .line 1506
    .local v2, "progress":F
    const/4 v0, 0x0

    .line 1507
    .local v0, "mag":Lcom/android/launcher2/MenuAppsGrid;
    instance-of v3, p0, Lcom/android/launcher2/MenuAppsGrid;

    if-eqz v3, :cond_6

    move-object v0, p0

    .line 1508
    check-cast v0, Lcom/android/launcher2/MenuAppsGrid;

    .line 1511
    :cond_6
    instance-of v3, v1, Lcom/android/launcher2/CellLayoutMenu;

    if-eqz v3, :cond_7

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/android/launcher2/MenuAppsGrid;->getState()Lcom/android/launcher2/MenuAppsGrid$State;

    move-result-object v3

    sget-object v4, Lcom/android/launcher2/MenuAppsGrid$State;->NORMAL:Lcom/android/launcher2/MenuAppsGrid$State;

    if-ne v3, v4, :cond_7

    .line 1512
    const v3, 0x3f733333    # 0.95f

    cmpg-float v3, v2, v3

    if-gez v3, :cond_1

    const v3, -0x408ccccd    # -0.95f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_1

    .line 1513
    invoke-virtual {p0, p1, v1, p3, p4}, Lcom/android/launcher2/PagedView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    goto :goto_0

    .line 1516
    :cond_7
    invoke-virtual {p0, p1, v1, p3, p4}, Lcom/android/launcher2/PagedView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    goto :goto_0
.end method

.method private dumpScroller()Ljava/lang/String;
    .locals 2

    .prologue
    .line 495
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "scrollerStartX "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getStartX()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " scrollerCurrX "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " scrollerFinalX "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getFinalX()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " scrollerDur "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getDuration()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " scrollingFinished "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private dumpVisiblePages()V
    .locals 0

    .prologue
    .line 1456
    return-void
.end method

.method private findPageIndicatorHit(Landroid/view/DragEvent;)I
    .locals 10
    .param p1, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 3558
    const/4 v4, -0x1

    .line 3559
    .local v4, "result":I
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {v8}, Lcom/android/launcher2/PageIndicatorManager;->getPreviewRects()[Landroid/graphics/Rect;

    move-result-object v3

    .line 3560
    .local v3, "rects":[Landroid/graphics/Rect;
    if-nez v3, :cond_0

    move v5, v4

    .line 3580
    .end local v4    # "result":I
    .local v5, "result":I
    :goto_0
    return v5

    .line 3564
    .end local v5    # "result":I
    .restart local v4    # "result":I
    :cond_0
    invoke-virtual {p1}, Landroid/view/DragEvent;->getX()F

    move-result v8

    float-to-int v6, v8

    .line 3565
    .local v6, "x":I
    invoke-virtual {p1}, Landroid/view/DragEvent;->getY()F

    move-result v8

    float-to-int v7, v8

    .line 3568
    .local v7, "y":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v8

    array-length v9, v3

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 3569
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 3570
    aget-object v2, v3, v0

    .line 3571
    .local v2, "r":Landroid/graphics/Rect;
    invoke-virtual {v2, v6, v7}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 3572
    move v4, v0

    .line 3577
    .end local v2    # "r":Landroid/graphics/Rect;
    :cond_1
    const/4 v8, -0x1

    if-eq v4, v8, :cond_2

    .line 3578
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->getSecretPageCountForAdjust()I

    move-result v8

    add-int/2addr v4, v8

    :cond_2
    move v5, v4

    .line 3580
    .end local v4    # "result":I
    .restart local v5    # "result":I
    goto :goto_0

    .line 3569
    .end local v5    # "result":I
    .restart local v2    # "r":Landroid/graphics/Rect;
    .restart local v4    # "result":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private getCenterOfViewRelative(Landroid/view/View;)I
    .locals 7
    .param p1, "page"    # Landroid/view/View;

    .prologue
    .line 2518
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 2519
    .local v0, "index":I
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getNonLoopedScrollXForPageIndex(I)I

    move-result v3

    .line 2520
    .local v3, "scrollX":I
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    .line 2521
    .local v4, "width":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 2522
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v4

    .line 2525
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 2527
    .local v1, "left":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isScreenLarge()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v5

    const/16 v6, 0x320

    if-ne v5, v6, :cond_1

    .line 2528
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v5

    div-int/lit8 v6, v4, 0xa

    add-int v1, v5, v6

    .line 2530
    :cond_1
    sub-int v5, v1, v3

    div-int/lit8 v6, v4, 0x2

    add-int v2, v5, v6

    .line 2534
    .local v2, "res":I
    return v2
.end method

.method private getLowerBoundForScrollX(I)I
    .locals 5
    .param p1, "scrollX"    # I

    .prologue
    .line 1011
    const/4 v0, 0x0

    .line 1012
    .local v0, "adjustedScrollX":I
    const/4 v2, 0x0

    .line 1013
    .local v2, "res":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageTotWidth()I

    move-result v1

    .line 1015
    .local v1, "pageTotWidth":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1016
    const/4 v3, 0x0

    iget v4, p0, Lcom/android/launcher2/PagedView;->mMaxScrollX:I

    invoke-static {p1, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1017
    div-int v3, v0, v1

    mul-int v2, v3, v1

    .line 1026
    :goto_0
    return v2

    .line 1019
    :cond_0
    move v0, p1

    .line 1020
    if-gez v0, :cond_1

    .line 1021
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    div-int/2addr v3, v1

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v3, v1

    neg-int v2, v3

    goto :goto_0

    .line 1023
    :cond_1
    div-int v3, v0, v1

    mul-int v2, v3, v1

    goto :goto_0
.end method

.method private getMotionSensor()V
    .locals 2

    .prologue
    .line 3677
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mMotionSensorManager:Landroid/hardware/motion/MotionRecognitionManager;

    if-nez v0, :cond_0

    .line 3678
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "motion_recognition"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/motion/MotionRecognitionManager;

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->mMotionSensorManager:Landroid/hardware/motion/MotionRecognitionManager;

    .line 3681
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mMotionSensorManager:Landroid/hardware/motion/MotionRecognitionManager;

    if-nez v0, :cond_1

    .line 3682
    sget-boolean v0, Lcom/android/launcher2/PagedView;->DEBUGGABLE:Z

    if-eqz v0, :cond_1

    const-string v0, "MotionEngine"

    const-string v1, "Cannot get Motion Sensor Service"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3683
    :cond_1
    return-void
.end method

.method private getNearestScrollXForPage(III)I
    .locals 9
    .param p1, "pageIndex"    # I
    .param p2, "scrollX"    # I
    .param p3, "dir"    # I

    .prologue
    .line 912
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageTotWidth()I

    move-result v4

    .line 914
    .local v4, "pageTotWidth":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v8

    if-nez v8, :cond_1

    .line 915
    mul-int v5, p1, v4

    .line 945
    :cond_0
    :goto_0
    return v5

    .line 917
    :cond_1
    invoke-direct {p0, p2}, Lcom/android/launcher2/PagedView;->getLowerBoundForScrollX(I)I

    move-result v6

    .line 918
    .local v6, "startScrollX":I
    const/4 v1, 0x0

    .line 919
    .local v1, "multiplier":I
    packed-switch p3, :pswitch_data_0

    .line 931
    add-int/2addr v6, v4

    .line 932
    const/4 v1, 0x1

    .line 935
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v3

    .line 936
    .local v3, "pageCount":I
    move v5, p2

    .line 937
    .local v5, "res":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    if-ge v0, v3, :cond_0

    .line 938
    mul-int v8, v0, v4

    mul-int/2addr v8, v1

    add-int v7, v6, v8

    .line 939
    .local v7, "temp":I
    invoke-virtual {p0, v7}, Lcom/android/launcher2/PagedView;->getPageIndexForScrollX(I)I

    move-result v2

    .line 940
    .local v2, "pageAtSlot":I
    if-ne v2, p1, :cond_2

    .line 941
    move v5, v7

    .line 942
    goto :goto_0

    .line 921
    .end local v0    # "i":I
    .end local v2    # "pageAtSlot":I
    .end local v3    # "pageCount":I
    .end local v5    # "res":I
    .end local v7    # "temp":I
    :pswitch_0
    const/4 v1, -0x1

    .line 922
    goto :goto_1

    .line 937
    .restart local v0    # "i":I
    .restart local v2    # "pageAtSlot":I
    .restart local v3    # "pageCount":I
    .restart local v5    # "res":I
    .restart local v7    # "temp":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 919
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private getScrollXForPageIndex(III)I
    .locals 6
    .param p1, "pageIndex"    # I
    .param p2, "scrollX"    # I
    .param p3, "dir"    # I

    .prologue
    .line 953
    const/4 v3, 0x0

    .line 954
    .local v3, "res":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 955
    packed-switch p3, :pswitch_data_0

    .line 987
    :goto_0
    return v3

    .line 958
    :pswitch_0
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher2/PagedView;->getNearestScrollXForPage(III)I

    move-result v3

    .line 963
    goto :goto_0

    .line 966
    :pswitch_1
    const/4 v5, 0x3

    invoke-direct {p0, p1, p2, v5}, Lcom/android/launcher2/PagedView;->getNearestScrollXForPage(III)I

    move-result v4

    .line 967
    .local v4, "rightScrollX":I
    const/4 v5, 0x2

    invoke-direct {p0, p1, p2, v5}, Lcom/android/launcher2/PagedView;->getNearestScrollXForPage(III)I

    move-result v2

    .line 968
    .local v2, "leftScrollX":I
    sub-int v5, p2, v2

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 969
    .local v0, "delta1":I
    sub-int v5, v4, p2

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 970
    .local v1, "delta2":I
    if-ge v0, v1, :cond_0

    .line 971
    move v3, v2

    goto :goto_0

    .line 973
    :cond_0
    move v3, v4

    goto :goto_0

    .line 984
    .end local v0    # "delta1":I
    .end local v1    # "delta2":I
    .end local v2    # "leftScrollX":I
    .end local v4    # "rightScrollX":I
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getNonLoopedScrollXForPageIndex(I)I

    move-result v3

    goto :goto_0

    .line 955
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getSecretPageCountForAdjust()I
    .locals 1

    .prologue
    .line 3594
    const/4 v0, 0x0

    return v0
.end method

.method private getSlotForScrollX(I)I
    .locals 4
    .param p1, "scrollX"    # I

    .prologue
    .line 857
    const/4 v1, 0x0

    .line 858
    .local v1, "res":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageTotWidth()I

    move-result v0

    .line 860
    .local v0, "pageTotWidth":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 861
    if-gez p1, :cond_0

    .line 862
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    div-int v1, v2, v0

    .line 869
    :goto_0
    return v1

    .line 864
    :cond_0
    div-int v1, p1, v0

    goto :goto_0

    .line 867
    :cond_1
    const/4 v2, 0x0

    iget v3, p0, Lcom/android/launcher2/PagedView;->mMaxScrollX:I

    invoke-static {p1, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    div-int v1, v2, v0

    goto :goto_0
.end method

.method private isEasyOneHandTriggerGesture()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2384
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "any_screen_enabled"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-nez v4, :cond_1

    .line 2395
    :cond_0
    :goto_0
    return v3

    .line 2386
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2387
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2388
    .local v2, "width":I
    iget v4, p0, Lcom/android/launcher2/PagedView;->mTouchDownPointX:F

    iget v5, p0, Lcom/android/launcher2/PagedView;->mTouchUpPointX:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 2389
    .local v1, "distance_down_up":F
    div-int/lit8 v4, v2, 0x8

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    iget v4, p0, Lcom/android/launcher2/PagedView;->mTouchDownPointX:F

    div-int/lit8 v5, v2, 0x5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_2

    iget v4, p0, Lcom/android/launcher2/PagedView;->mTouchDownPointX:F

    div-int/lit8 v5, v2, 0x5

    sub-int v5, v2, v5

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    :cond_2
    iget v4, p0, Lcom/android/launcher2/PagedView;->mTouchUpPointX:F

    div-int/lit8 v5, v2, 0x5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_3

    iget v4, p0, Lcom/android/launcher2/PagedView;->mTouchUpPointX:F

    div-int/lit8 v5, v2, 0x5

    sub-int v5, v2, v5

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 2392
    :cond_3
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private isLoopingDisabledInCSC()Z
    .locals 3

    .prologue
    .line 832
    const/4 v0, 0x0

    .line 833
    .local v0, "cscLauncherDisabledLooping":Z
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Launcher_DisablePageRotation"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    .line 834
    return v0
.end method

.method private isNewPageIndex(I)Z
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 3548
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private jumpToPageInternal(I)V
    .locals 2
    .param p1, "currentPage"    # I

    .prologue
    .line 567
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getAdjustedPageIndex(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    .line 571
    iget v1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->getNonLoopedScrollXForPageIndex(I)I

    move-result v0

    .line 572
    .local v0, "newX":I
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1, v0}, Landroid/widget/Scroller;->setFinalX(I)V

    .line 573
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    .line 574
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->abortAnimation()V

    .line 580
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/PagedView;->scrollTo(II)V

    .line 581
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->notifyPageSwitchListener()V

    .line 582
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    .line 583
    return-void
.end method

.method private loadAssociatedPages(IZ)V
    .locals 6
    .param p1, "page"    # I
    .param p2, "immediateAndOnly"    # Z

    .prologue
    const/4 v5, 0x1

    .line 2843
    iget-boolean v3, p0, Lcom/android/launcher2/PagedView;->mContentIsRefreshable:Z

    if-eqz v3, :cond_1

    .line 2844
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    .line 2845
    .local v0, "count":I
    iget-object v3, p0, Lcom/android/launcher2/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 2846
    iget-object v3, p0, Lcom/android/launcher2/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2845
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2854
    :cond_0
    if-ltz p1, :cond_1

    if-ge p1, v0, :cond_1

    .line 2855
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getInclusivePages(I)V

    .line 2857
    invoke-direct {p0, p1, p2}, Lcom/android/launcher2/PagedView;->syncPageItemsInternal(IZ)V

    .line 2860
    if-eqz p2, :cond_2

    .line 2876
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_1
    return-void

    .line 2861
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_1

    .line 2862
    if-ne p1, v1, :cond_3

    .line 2861
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2863
    :cond_3
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/Page;

    .line 2864
    .local v2, "layout":Lcom/android/launcher2/Page;
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->isPageInclusive(I)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2865
    invoke-interface {v2}, Lcom/android/launcher2/Page;->getPageItemCount()I

    move-result v3

    if-lez v3, :cond_4

    .line 2866
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->clearLayout(Lcom/android/launcher2/Page;)V

    .line 2868
    :cond_4
    iget-object v3, p0, Lcom/android/launcher2/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2869
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 2871
    :cond_5
    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lcom/android/launcher2/PagedView;->syncPageItemsInternal(IZ)V

    goto :goto_2
.end method

.method static mix(FFF)F
    .locals 2
    .param p0, "x"    # F
    .param p1, "y"    # F
    .param p2, "mix"    # F

    .prologue
    .line 3146
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    mul-float/2addr v0, p0

    mul-float v1, p1, p2

    add-float/2addr v0, v1

    return v0
.end method

.method private newPageInfo()Lcom/android/launcher2/PagedView$PageInfo;
    .locals 3

    .prologue
    .line 820
    const/4 v0, 0x0

    .line 821
    .local v0, "res":Lcom/android/launcher2/PagedView$PageInfo;
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mPagesPool:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 822
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mPagesPool:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "res":Lcom/android/launcher2/PagedView$PageInfo;
    check-cast v0, Lcom/android/launcher2/PagedView$PageInfo;

    .line 826
    .restart local v0    # "res":Lcom/android/launcher2/PagedView$PageInfo;
    :goto_0
    invoke-virtual {v0}, Lcom/android/launcher2/PagedView$PageInfo;->reset()V

    .line 827
    return-object v0

    .line 824
    :cond_0
    new-instance v0, Lcom/android/launcher2/PagedView$PageInfo;

    .end local v0    # "res":Lcom/android/launcher2/PagedView$PageInfo;
    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/launcher2/PagedView$PageInfo;-><init>(Lcom/android/launcher2/PagedView$1;)V

    .restart local v0    # "res":Lcom/android/launcher2/PagedView$PageInfo;
    goto :goto_0
.end method

.method private notifyPageSwitchListener()V
    .locals 3

    .prologue
    .line 586
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageSwitchListener:Lcom/android/launcher2/PagedView$PageSwitchListener;

    if-eqz v0, :cond_0

    .line 587
    iget v0, p0, Lcom/android/launcher2/PagedView;->mLastPageNotify:I

    iget v1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    if-ne v0, v1, :cond_1

    .line 591
    :cond_0
    :goto_0
    return-void

    .line 588
    :cond_1
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    iput v0, p0, Lcom/android/launcher2/PagedView;->mLastPageNotify:I

    .line 589
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageSwitchListener:Lcom/android/launcher2/PagedView$PageSwitchListener;

    iget v1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-interface {v0, v1, v2}, Lcom/android/launcher2/PagedView$PageSwitchListener;->onPageSwitch(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private overScrollInfluenceCurve(F)F
    .locals 2
    .param p1, "f"    # F

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2080
    sub-float/2addr p1, v1

    .line 2081
    mul-float v0, p1, p1

    mul-float/2addr v0, p1

    add-float/2addr v0, v1

    return v0
.end method

.method private releaseVelocityTracker()V
    .locals 1

    .prologue
    .line 2443
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 2444
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 2445
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 2447
    :cond_0
    return-void
.end method

.method private scrollToNonLooped(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v1, 0x1

    .line 772
    if-gez p1, :cond_1

    .line 773
    const/4 v0, 0x0

    invoke-super {p0, v0, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    .line 774
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->canOverScroll()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->canOverScrollForHeadlines()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 775
    iput-boolean v1, p0, Lcom/android/launcher2/PagedView;->mIsOverScrolled:Z

    .line 776
    int-to-float v0, p1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->overScroll(F)V

    .line 787
    :cond_0
    :goto_0
    return-void

    .line 778
    :cond_1
    iget v0, p0, Lcom/android/launcher2/PagedView;->mMaxScrollX:I

    if-le p1, v0, :cond_2

    .line 779
    iget v0, p0, Lcom/android/launcher2/PagedView;->mMaxScrollX:I

    invoke-super {p0, v0, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    .line 780
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->canOverScroll()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 781
    iput-boolean v1, p0, Lcom/android/launcher2/PagedView;->mIsOverScrolled:Z

    .line 782
    iget v0, p0, Lcom/android/launcher2/PagedView;->mMaxScrollX:I

    sub-int v0, p1, v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->overScroll(F)V

    goto :goto_0

    .line 785
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_0
.end method

.method private snapToPageInternal(III)V
    .locals 9
    .param p1, "whichPage"    # I
    .param p2, "duration"    # I
    .param p3, "dir"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 2681
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getAdjustedPageIndex(I)I

    move-result p1

    .line 2685
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v0

    invoke-direct {p0, p1, v0, p3}, Lcom/android/launcher2/PagedView;->getScrollXForPageIndex(III)I

    move-result v6

    .line 2686
    .local v6, "dstScrollX":I
    iget v0, p0, Lcom/android/launcher2/PagedView;->mUnboundedScrollX:I

    sub-int v3, v6, v0

    .line 2688
    .local v3, "delta":I
    if-nez v3, :cond_0

    .line 2689
    iput p1, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    .line 2742
    :goto_0
    return-void

    .line 2700
    :cond_0
    iput p1, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    .line 2701
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2702
    iget v0, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    if-nez v0, :cond_4

    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_4

    if-lez v3, :cond_4

    .line 2703
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/launcher2/PagedView;->mSnapState:I

    .line 2713
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getFocusedChild()Landroid/view/View;

    move-result-object v7

    .line 2714
    .local v7, "focusedChild":Landroid/view/View;
    if-eqz v7, :cond_1

    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    if-eq p1, v0, :cond_1

    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    if-ne v7, v0, :cond_1

    .line 2716
    invoke-virtual {v7}, Landroid/view/View;->clearFocus()V

    .line 2719
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->pageBeginMoving()V

    .line 2720
    invoke-virtual {p0, p2}, Lcom/android/launcher2/PagedView;->awakenScrollBars(I)Z

    .line 2721
    if-nez p2, :cond_2

    .line 2722
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result p2

    .line 2725
    :cond_2
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2726
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 2731
    :cond_3
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/android/launcher2/PagedView;->mUnboundedScrollX:I

    move v4, v2

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 2735
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->mDeferScrollUpdate:Z

    if-eqz v0, :cond_7

    .line 2736
    iget v0, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->loadAssociatedPages(I)V

    .line 2740
    :goto_2
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->notifyPageSwitchListener()V

    .line 2741
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    goto :goto_0

    .line 2704
    .end local v7    # "focusedChild":Landroid/view/View;
    :cond_4
    iget v0, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_5

    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    if-nez v0, :cond_5

    if-gez v3, :cond_5

    .line 2705
    iput v8, p0, Lcom/android/launcher2/PagedView;->mSnapState:I

    goto :goto_1

    .line 2707
    :cond_5
    iput v2, p0, Lcom/android/launcher2/PagedView;->mSnapState:I

    goto :goto_1

    .line 2710
    :cond_6
    iput v2, p0, Lcom/android/launcher2/PagedView;->mSnapState:I

    goto :goto_1

    .line 2738
    .restart local v7    # "focusedChild":Landroid/view/View;
    :cond_7
    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mDeferLoadAssociatedPagesUntilScrollCompletes:Z

    goto :goto_2
.end method

.method private syncPageItemsInternal(IZ)V
    .locals 6
    .param p1, "page"    # I
    .param p2, "immediateAndOnly"    # Z

    .prologue
    const/4 v4, 0x0

    .line 2945
    iget-object v3, p0, Lcom/android/launcher2/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2946
    invoke-virtual {p0, p1, p2}, Lcom/android/launcher2/PagedView;->syncPageItems(IZ)V

    .line 2947
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 2948
    .local v2, "itemContainer":Landroid/view/ViewGroup;
    if-nez v2, :cond_1

    .line 2949
    const-string v3, "Launcher.PagedView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "syncPageItemsInternal Null Occured Page : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2979
    .end local v2    # "itemContainer":Landroid/view/ViewGroup;
    :cond_0
    :goto_0
    return-void

    .line 2952
    .restart local v2    # "itemContainer":Landroid/view/ViewGroup;
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 2953
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2956
    .local v0, "dragReceiver":Landroid/view/View;
    instance-of v3, v0, Lcom/android/launcher2/DragReceivable;

    if-eqz v3, :cond_2

    .line 2963
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    .line 2964
    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2965
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2952
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2976
    .end local v0    # "dragReceiver":Landroid/view/View;
    :cond_3
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2977
    iget-object v3, p0, Lcom/android/launcher2/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private transformPage(Lcom/android/launcher2/PagedView$PageInfo;J)V
    .locals 6
    .param p1, "pageInfo"    # Lcom/android/launcher2/PagedView$PageInfo;
    .param p2, "drawingTime"    # J

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 1460
    iget-boolean v2, p0, Lcom/android/launcher2/PagedView;->mPageTransformsDirty:Z

    if-nez v2, :cond_1

    .line 1480
    :cond_0
    :goto_0
    return-void

    .line 1461
    :cond_1
    iget v2, p1, Lcom/android/launcher2/PagedView$PageInfo;->mIndex:I

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    .line 1462
    .local v0, "page":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/launcher2/PagedView;->mCurrentTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

    sget-object v3, Lcom/android/launcher2/PagedView$TransitionEffect;->CARD:Lcom/android/launcher2/PagedView$TransitionEffect;

    invoke-virtual {v2, v3}, Lcom/android/launcher2/PagedView$TransitionEffect;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1466
    :cond_2
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getScrollProgress(Lcom/android/launcher2/PagedView$PageInfo;)F

    move-result v1

    .line 1467
    .local v1, "scrollProgress":F
    invoke-virtual {v0, v4}, Landroid/view/View;->setTranslationX(F)V

    .line 1468
    invoke-virtual {v0, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 1469
    invoke-virtual {v0, v5}, Landroid/view/View;->setScaleX(F)V

    .line 1470
    invoke-virtual {v0, v5}, Landroid/view/View;->setScaleY(F)V

    .line 1471
    invoke-virtual {v0, v4}, Landroid/view/View;->setRotationY(F)V

    .line 1472
    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 1474
    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/PagedView;->updatePageTransform(Landroid/view/View;F)V

    .line 1476
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1477
    iget v2, p1, Lcom/android/launcher2/PagedView$PageInfo;->mTransX:I

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v3

    add-float/2addr v2, v3

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 1478
    iget v2, p1, Lcom/android/launcher2/PagedView$PageInfo;->mTransY:I

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v3

    add-float/2addr v2, v3

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0
.end method

.method private updateMotionTracking(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 2466
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 2470
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->mDownMotionX:F

    iput v1, p0, Lcom/android/launcher2/PagedView;->mLastMotionX:F

    .line 2471
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->mLastMotionY:F

    .line 2472
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/launcher2/PagedView;->mLastMotionXRemainder:F

    .line 2473
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_0

    .line 2474
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    .line 2475
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/launcher2/PagedView;->motionTrackingIsCurrent:Z

    .line 2476
    return-void
.end method


# virtual methods
.method public CheckFolderOpen()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3077
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v1}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v0

    .line 3078
    .local v0, "mWorkspace":Lcom/android/launcher2/Workspace;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3080
    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Lcom/android/launcher2/Folder;->close(ZZ)V

    .line 3082
    :cond_0
    return-void
.end method

.method protected abortScroll()V
    .locals 2

    .prologue
    .line 4028
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    .line 4034
    .local v0, "running":Z
    if-nez v0, :cond_0

    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->mIsPageMoving:Z

    if-eqz v1, :cond_1

    .line 4035
    :cond_0
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->abortAnimation()V

    .line 4036
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->computeScrollHelper()Z

    .line 4038
    :cond_1
    return-void
.end method

.method protected acceleratedOverScroll(F)V
    .locals 6
    .param p1, "amount"    # F

    .prologue
    const/4 v5, 0x0

    .line 2085
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v2

    .line 2089
    .local v2, "screenSize":I
    const/high16 v3, 0x40000000    # 2.0f

    int-to-float v4, v2

    div-float v4, p1, v4

    mul-float v0, v3, v4

    .line 2091
    .local v0, "f":F
    cmpl-float v3, v0, v5

    if-nez v3, :cond_0

    .line 2105
    :goto_0
    return-void

    .line 2094
    :cond_0
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    .line 2095
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    div-float/2addr v0, v3

    .line 2098
    :cond_1
    int-to-float v3, v2

    mul-float/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 2099
    .local v1, "overScrollAmount":I
    cmpg-float v3, p1, v5

    if-gez v3, :cond_2

    .line 2100
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v3

    invoke-super {p0, v1, v3}, Landroid/view/ViewGroup;->scrollTo(II)V

    .line 2104
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    goto :goto_0

    .line 2102
    :cond_2
    iget v3, p0, Lcom/android/launcher2/PagedView;->mMaxScrollX:I

    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v4

    invoke-super {p0, v3, v4}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_1
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 2
    .param p2, "direction"    # I
    .param p3, "focusableMode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 1657
    .local p1, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1658
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    .line 1659
    const/16 v0, 0x11

    if-ne p2, v0, :cond_1

    .line 1660
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    if-lez v0, :cond_0

    .line 1661
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    .line 1669
    :cond_0
    :goto_0
    return-void

    .line 1663
    :cond_1
    const/16 v0, 0x42

    if-ne p2, v0, :cond_0

    .line 1664
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 1665
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    goto :goto_0
.end method

.method public allowLongPress()Z
    .locals 1

    .prologue
    .line 2768
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->mAllowLongPress:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isFastScrolling()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected animateClickFeedback(Landroid/view/View;Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "r"    # Ljava/lang/Runnable;
    .param p3, "onFinishAnimationRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 1931
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04000b

    invoke-static {v1, v2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    .line 1933
    .local v0, "anim":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1934
    new-instance v1, Lcom/android/launcher2/PagedView$3;

    invoke-direct {v1, p0, p2, p3}, Lcom/android/launcher2/PagedView$3;-><init>(Lcom/android/launcher2/PagedView;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1945
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1946
    return-void
.end method

.method public cancelClicksOnChildrenForCurrentPage()V
    .locals 6

    .prologue
    .line 3920
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/CellLayout;

    .line 3921
    .local v3, "page":Lcom/android/launcher2/CellLayout;
    if-nez v3, :cond_1

    .line 3922
    const-string v4, "Launcher.PagedView"

    const-string v5, "cancelClicksOnChildrenForCurrentPage, page is null!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3931
    :cond_0
    return-void

    .line 3926
    :cond_1
    invoke-virtual {v3}, Lcom/android/launcher2/CellLayout;->getChildrenLayout()Lcom/android/launcher2/CellLayoutChildren;

    move-result-object v1

    .line 3927
    .local v1, "clChildren":Lcom/android/launcher2/CellLayoutChildren;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Lcom/android/launcher2/CellLayoutChildren;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 3928
    invoke-virtual {v1, v2}, Lcom/android/launcher2/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3929
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    .line 3927
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected cancelCurrentPageLongPress()V
    .locals 2

    .prologue
    .line 2049
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->mAllowLongPress:Z

    if-eqz v1, :cond_0

    .line 2050
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/launcher2/PagedView;->mAllowLongPress:Z

    .line 2054
    iget v1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    .line 2055
    .local v0, "currentPage":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 2056
    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    .line 2059
    .end local v0    # "currentPage":Landroid/view/View;
    :cond_0
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 427
    instance-of v0, p1, Landroid/widget/FrameLayout$LayoutParams;

    return v0
.end method

.method public clampCurrentPage()V
    .locals 3

    .prologue
    .line 3802
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v0

    .line 3803
    .local v0, "currentPage":I
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 3804
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 3805
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->setCurrentPageIfNotSnapping(I)V

    .line 3807
    :cond_0
    return-void
.end method

.method public clearLayout(Lcom/android/launcher2/Page;)V
    .locals 0
    .param p1, "layout"    # Lcom/android/launcher2/Page;

    .prologue
    .line 2982
    invoke-interface {p1}, Lcom/android/launcher2/Page;->removeAllViewsOnPage()V

    .line 2983
    return-void
.end method

.method public computeScroll()V
    .locals 0

    .prologue
    .line 1190
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->computeScrollHelper()Z

    .line 1191
    return-void
.end method

.method protected computeScrollHelper()Z
    .locals 10

    .prologue
    const/high16 v8, -0x80000000

    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 1128
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v3

    .line 1129
    .local v3, "pageCount":I
    if-ge v3, v9, :cond_1

    move v4, v6

    .line 1185
    :cond_0
    :goto_0
    return v4

    .line 1133
    :cond_1
    const/4 v4, 0x0

    .line 1134
    .local v4, "res":Z
    iget-object v7, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1136
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v6

    iget-object v7, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->getCurrX()I

    move-result v7

    if-ne v6, v7, :cond_2

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v6

    iget-object v7, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->getCurrY()I

    move-result v7

    if-eq v6, v7, :cond_3

    .line 1137
    :cond_2
    iget-object v6, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrX()I

    move-result v6

    iget-object v7, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->getCurrY()I

    move-result v7

    invoke-virtual {p0, v6, v7}, Lcom/android/launcher2/PagedView;->scrollTo(II)V

    .line 1139
    :cond_3
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    .line 1140
    const/4 v4, 0x1

    goto :goto_0

    .line 1141
    :cond_4
    iget v7, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    if-eq v7, v8, :cond_0

    .line 1142
    iget v1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    .line 1143
    .local v1, "currentPage":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageNearestToCenterOfScreen()I

    move-result v7

    iput v7, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    .line 1145
    iget v7, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    if-ne v7, v1, :cond_5

    .line 1152
    :cond_5
    iput v8, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    .line 1153
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->notifyPageSwitchListener()V

    .line 1156
    iget-boolean v7, p0, Lcom/android/launcher2/PagedView;->mDeferLoadAssociatedPagesUntilScrollCompletes:Z

    if-eqz v7, :cond_6

    iget v7, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    if-eq v7, v1, :cond_6

    .line 1157
    iget v7, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v7}, Lcom/android/launcher2/PagedView;->loadAssociatedPages(I)V

    .line 1158
    iput-boolean v6, p0, Lcom/android/launcher2/PagedView;->mDeferLoadAssociatedPagesUntilScrollCompletes:Z

    .line 1163
    :cond_6
    iget v7, p0, Lcom/android/launcher2/PagedView;->mTouchState:I

    if-nez v7, :cond_7

    .line 1164
    iget v7, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v8

    invoke-direct {p0, v7, v8, v9}, Lcom/android/launcher2/PagedView;->getScrollXForPageIndex(III)I

    move-result v5

    .line 1165
    .local v5, "temp":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v7

    invoke-virtual {p0, v5, v7}, Lcom/android/launcher2/PagedView;->scrollTo(II)V

    .line 1166
    iput v6, p0, Lcom/android/launcher2/PagedView;->mSnapState:I

    .line 1167
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->pageEndMoving()V

    .line 1168
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    .line 1172
    .end local v5    # "temp":I
    :cond_7
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "accessibility"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 1174
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1175
    const/16 v6, 0x1000

    invoke-static {v6}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v2

    .line 1177
    .local v2, "ev":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v6

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPageDescription()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1178
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1180
    sget-object v6, Lcom/sec/dtl/launcher/Talk;->INSTANCE:Lcom/sec/dtl/launcher/Talk;

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPageDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, p0, v7, v9}, Lcom/sec/dtl/launcher/Talk;->sayByTalkback(Landroid/view/View;Ljava/lang/String;Z)V

    .line 1182
    .end local v2    # "ev":Landroid/view/accessibility/AccessibilityEvent;
    :cond_8
    const/4 v4, 0x1

    goto/16 :goto_0
.end method

.method protected dampedOverScroll(F)V
    .locals 6
    .param p1, "amount"    # F

    .prologue
    const/4 v5, 0x0

    .line 2108
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v2

    .line 2110
    .local v2, "screenSize":I
    int-to-float v3, v2

    div-float v0, p1, v3

    .line 2112
    .local v0, "f":F
    cmpl-float v3, v0, v5

    if-nez v3, :cond_0

    .line 2127
    :goto_0
    return-void

    .line 2113
    :cond_0
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    div-float v3, v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-direct {p0, v4}, Lcom/android/launcher2/PagedView;->overScrollInfluenceCurve(F)F

    move-result v4

    mul-float v0, v3, v4

    .line 2116
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    .line 2117
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    div-float/2addr v0, v3

    .line 2120
    :cond_1
    const v3, 0x3e0f5c29    # 0.14f

    mul-float/2addr v3, v0

    int-to-float v4, v2

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 2121
    .local v1, "overScrollAmount":I
    cmpg-float v3, p1, v5

    if-gez v3, :cond_2

    .line 2122
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v3

    invoke-super {p0, v1, v3}, Landroid/view/ViewGroup;->scrollTo(II)V

    .line 2126
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    goto :goto_0

    .line 2124
    :cond_2
    iget v3, p0, Lcom/android/launcher2/PagedView;->mMaxScrollX:I

    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v4

    invoke-super {p0, v3, v4}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_1
.end method

.method protected determineScrollingStart(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1949
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher2/PagedView;->determineScrollingStart(Landroid/view/MotionEvent;F)V

    .line 1950
    return-void
.end method

.method protected determineScrollingStart(Landroid/view/MotionEvent;F)V
    .locals 24
    .param p1, "ev"    # Landroid/view/MotionEvent;
    .param p2, "touchSlopScale"    # F

    .prologue
    .line 1961
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v13

    .line 1962
    .local v13, "x":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mDownMotionX:F

    move/from16 v18, v0

    sub-float v18, v13, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    move/from16 v0, v18

    float-to-int v14, v0

    .line 1964
    .local v14, "xDiff":I
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher2/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 1965
    .local v9, "velocityTracker":Landroid/view/VelocityTracker;
    const/16 v18, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mMaximumVelocity:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1966
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v18

    move/from16 v0, v18

    float-to-int v12, v0

    .line 1971
    .local v12, "velocityX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchSlop:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v18, v18, p2

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 1972
    .local v7, "touchSlop":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mDriftSlop:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v18, v18, p2

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 1974
    .local v5, "driftSlop":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mPagingTouchSlop:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-le v14, v0, :cond_3

    const/16 v16, 0x1

    .line 1975
    .local v16, "xPaged":Z
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mDriftSlop:I

    move/from16 v18, v0

    if-lez v18, :cond_5

    if-le v14, v5, :cond_4

    const/4 v15, 0x1

    .line 1977
    .local v15, "xMoved":Z
    :goto_1
    if-gtz v5, :cond_8

    .line 1978
    if-nez v15, :cond_0

    if-eqz v16, :cond_2

    .line 1979
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/launcher2/PagedView;->mUsePagingTouchSlop:Z

    move/from16 v18, v0

    if-eqz v18, :cond_7

    if-eqz v16, :cond_1

    .line 1981
    :goto_2
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTouchState:I

    .line 1982
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTotalMotionX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mLastMotionX:F

    move/from16 v19, v0

    sub-float v19, v19, v13

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(F)F

    move-result v19

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTotalMotionX:F

    .line 1983
    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/launcher2/PagedView;->mLastMotionX:F

    .line 1984
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mLastMotionXRemainder:F

    .line 1985
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTouchX:F

    .line 1986
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v18, v0

    const v19, 0x4e6e6b28    # 1.0E9f

    div-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mSmoothingTime:F

    .line 1987
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->pageBeginMoving()V

    .line 1990
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->cancelCurrentPageLongPress()V

    .line 2046
    :cond_2
    :goto_3
    return-void

    .line 1974
    .end local v15    # "xMoved":Z
    .end local v16    # "xPaged":Z
    :cond_3
    const/16 v16, 0x0

    goto :goto_0

    .line 1975
    .restart local v16    # "xPaged":Z
    :cond_4
    const/4 v15, 0x0

    goto :goto_1

    :cond_5
    if-le v14, v7, :cond_6

    const/4 v15, 0x1

    goto :goto_1

    :cond_6
    const/4 v15, 0x0

    goto :goto_1

    .line 1979
    .restart local v15    # "xMoved":Z
    :cond_7
    if-eqz v15, :cond_1

    goto :goto_2

    .line 1993
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/launcher2/PagedView;->mUsePagingTouchSlop:Z

    move/from16 v18, v0

    if-eqz v18, :cond_c

    if-eqz v16, :cond_b

    .line 1994
    :goto_4
    const/16 v18, 0x4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchState:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_9

    .line 1995
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->pageBeginMoving()V

    .line 1998
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTotalMotionX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mLastMotionX:F

    move/from16 v19, v0

    sub-float v19, v19, v13

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(F)F

    move-result v19

    add-float v6, v18, v19

    .line 1999
    .local v6, "motionX":F
    int-to-float v0, v7

    move/from16 v18, v0

    cmpl-float v18, v6, v18

    if-lez v18, :cond_d

    const/16 v17, 0x1

    .line 2000
    .local v17, "xTraveled":Z
    :goto_5
    if-lt v7, v14, :cond_a

    if-eqz v17, :cond_e

    :cond_a
    const/16 v18, 0x1

    :goto_6
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTouchState:I

    .line 2002
    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchState:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_b

    .line 2004
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->cancelCurrentPageLongPress()V

    .line 2007
    .end local v6    # "motionX":F
    .end local v17    # "xTraveled":Z
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchState:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchState:I

    move/from16 v18, v0

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_f

    .line 2008
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v18, v0

    const v19, 0x4e6e6b28    # 1.0E9f

    div-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mSmoothingTime:F

    .line 2009
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTotalMotionX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mLastMotionX:F

    move/from16 v19, v0

    sub-float v19, v19, v13

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(F)F

    move-result v19

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTotalMotionX:F

    .line 2010
    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/launcher2/PagedView;->mLastMotionX:F

    .line 2011
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mLastMotionXRemainder:F

    .line 2012
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTouchX:F

    goto/16 :goto_3

    .line 1993
    :cond_c
    if-eqz v15, :cond_b

    goto/16 :goto_4

    .line 1999
    .restart local v6    # "motionX":F
    :cond_d
    const/16 v17, 0x0

    goto/16 :goto_5

    .line 2000
    .restart local v17    # "xTraveled":Z
    :cond_e
    const/16 v18, 0x4

    goto/16 :goto_6

    .line 2013
    .end local v6    # "motionX":F
    .end local v17    # "xTraveled":Z
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchState:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchState:I

    move/from16 v18, v0

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 2015
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mLastMotionX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mLastMotionXRemainder:F

    move/from16 v19, v0

    add-float v18, v18, v19

    sub-float v4, v18, v13

    .line 2016
    .local v4, "deltaX":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/launcher2/PagedView;->mUseSlopJump:Z

    move/from16 v18, v0

    if-eqz v18, :cond_11

    .line 2017
    const-wide v10, 0x3f50624dd2f1a9fcL    # 0.001

    .line 2018
    .local v10, "velocityRatio":D
    const/16 v8, 0x9c4

    .line 2019
    .local v8, "velocityLimit":I
    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v18

    const/16 v19, 0x9c4

    move/from16 v0, v18

    move/from16 v1, v19

    if-gt v0, v1, :cond_12

    .line 2020
    float-to-double v0, v4

    move-wide/from16 v18, v0

    int-to-float v0, v5

    move/from16 v20, v0

    int-to-float v0, v12

    move/from16 v21, v0

    mul-float v20, v20, v21

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    const-wide v22, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v20, v20, v22

    sub-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v4, v0

    .line 2024
    .end local v8    # "velocityLimit":I
    .end local v10    # "velocityRatio":D
    :cond_11
    :goto_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTotalMotionX:F

    move/from16 v18, v0

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v19

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTotalMotionX:F

    .line 2029
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x3f800000    # 1.0f

    cmpl-float v18, v18, v19

    if-ltz v18, :cond_14

    .line 2030
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchX:F

    move/from16 v18, v0

    add-float v18, v18, v4

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTouchX:F

    .line 2031
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v18, v0

    const v19, 0x4e6e6b28    # 1.0E9f

    div-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mSmoothingTime:F

    .line 2032
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/launcher2/PagedView;->mDeferScrollUpdate:Z

    move/from16 v18, v0

    if-nez v18, :cond_13

    .line 2033
    float-to-int v0, v4

    move/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/PagedView;->scrollBy(II)V

    .line 2034
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/launcher2/PagedView;->mUseSlopJump:Z

    .line 2039
    :goto_8
    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/launcher2/PagedView;->mLastMotionX:F

    .line 2040
    float-to-int v0, v4

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    sub-float v18, v4, v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mLastMotionXRemainder:F

    goto/16 :goto_3

    .line 2022
    .restart local v8    # "velocityLimit":I
    .restart local v10    # "velocityRatio":D
    :cond_12
    float-to-double v0, v4

    move-wide/from16 v18, v0

    mul-int/lit16 v0, v5, 0x9c4

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    int-to-float v0, v12

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->signum(F)F

    move-result v21

    mul-float v20, v20, v21

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    const-wide v22, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double v20, v20, v22

    sub-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v4, v0

    goto/16 :goto_7

    .line 2037
    .end local v8    # "velocityLimit":I
    .end local v10    # "velocityRatio":D
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    goto :goto_8

    .line 2042
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->awakenScrollBars()Z

    goto/16 :goto_3
.end method

.method public disableRollNavigation()V
    .locals 3

    .prologue
    .line 3666
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->mMotionListenerRegistered:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 3667
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mMotionSensorManager:Landroid/hardware/motion/MotionRecognitionManager;

    invoke-virtual {v1, p0}, Landroid/hardware/motion/MotionRecognitionManager;->unregisterListener(Landroid/hardware/motion/MRListener;)V

    .line 3668
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/launcher2/PagedView;->mMotionListenerRegistered:Z

    .line 3670
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    .line 3671
    .local v0, "launcher":Lcom/android/launcher2/Launcher;
    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->unRegisterAirMotionGestureListner()V

    .line 3672
    return-void
.end method

.method public dispatchDragEvent(Landroid/view/DragEvent;)Z
    .locals 24
    .param p1, "ev"    # Landroid/view/DragEvent;

    .prologue
    .line 3390
    invoke-static/range {p1 .. p1}, Lcom/android/launcher2/Launcher;->isInValidDragState(Landroid/view/DragEvent;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v22, 0x0

    .line 3542
    :cond_0
    :goto_0
    return v22

    .line 3392
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 3534
    :cond_2
    :goto_1
    :pswitch_0
    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    move-result v22

    .line 3535
    .local v22, "r":Z
    if-eqz v22, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getAction()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getAction()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    .line 3540
    :cond_3
    invoke-virtual/range {p0 .. p1}, Lcom/android/launcher2/PagedView;->onDragEvent(Landroid/view/DragEvent;)Z

    goto :goto_0

    .line 3394
    .end local v22    # "r":Z
    :pswitch_1
    const/4 v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/launcher2/PagedView;->mLastHit:I

    goto :goto_1

    .line 3397
    :pswitch_2
    sget-boolean v3, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/android/launcher2/Launcher;->isHomeRemoveMode()Z

    move-result v3

    if-nez v3, :cond_2

    .line 3398
    :cond_4
    invoke-direct/range {p0 .. p1}, Lcom/android/launcher2/PagedView;->findPageIndicatorHit(Landroid/view/DragEvent;)I

    move-result v19

    .line 3399
    .local v19, "i":I
    if-lez v19, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->isAnimating()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 3400
    const/16 v19, -0x1

    .line 3401
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/PagedView;->mPagePreviewSnapDelay:Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;

    invoke-virtual {v3}, Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;->cancel()V

    .line 3404
    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/launcher2/PagedView;->mLastHit:I

    move/from16 v0, v19

    if-eq v0, v3, :cond_8

    const/4 v3, -0x1

    move/from16 v0, v19

    if-eq v0, v3, :cond_8

    .line 3405
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/launcher2/PagedView;->mLastDragLocationX:I

    int-to-float v3, v3

    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getX()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/launcher2/PagedView;->mTouchSlop:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_6

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/launcher2/PagedView;->mLastDragLocationY:I

    int-to-float v3, v3

    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/launcher2/PagedView;->mTouchSlop:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_b

    .line 3406
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/PagedView;->mPagePreviewSnapDelay:Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;

    invoke-virtual {v3}, Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;->cancel()V

    .line 3424
    :cond_7
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/launcher2/PagedView;->mLastDragLocationX:I

    .line 3425
    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/launcher2/PagedView;->mLastDragLocationY:I

    .line 3429
    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/launcher2/PagedView;->mLastHit:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_a

    const/4 v3, -0x1

    move/from16 v0, v19

    if-ne v0, v3, :cond_a

    .line 3430
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/PagedView;->mPagePreviewSnapDelay:Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;

    invoke-virtual {v3}, Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;->cancel()V

    .line 3431
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/launcher2/PagedView;->mLastHit:I

    invoke-virtual {v3, v4}, Lcom/android/launcher2/PageIndicatorManager;->onDragExit(I)Z

    move-result v23

    .line 3432
    .local v23, "result":Z
    if-eqz v23, :cond_9

    .line 3433
    new-instance v22, Landroid/graphics/Rect;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Rect;-><init>()V

    .line 3434
    .local v22, "r":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v4

    move-object/from16 v0, v22

    invoke-virtual {v3, v0, v4}, Lcom/android/launcher2/PageIndicatorManager;->getHitRect(Landroid/graphics/Rect;I)V

    .line 3435
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PagedView;->invalidate(Landroid/graphics/Rect;)V

    .line 3437
    .end local v22    # "r":Landroid/graphics/Rect;
    :cond_9
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mLastHit:I

    .line 3440
    .end local v23    # "result":Z
    :cond_a
    const/4 v3, -0x1

    move/from16 v0, v19

    if-eq v0, v3, :cond_2

    .line 3468
    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    .line 3469
    const/16 v22, 0x1

    goto/16 :goto_0

    .line 3408
    :cond_b
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mLastHit:I

    .line 3410
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Lcom/android/launcher2/PageIndicatorManager;->onDragEnter(I)Z

    move-result v23

    .line 3411
    .restart local v23    # "result":Z
    if-eqz v23, :cond_c

    .line 3412
    new-instance v22, Landroid/graphics/Rect;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Rect;-><init>()V

    .line 3413
    .restart local v22    # "r":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v4

    move-object/from16 v0, v22

    invoke-virtual {v3, v0, v4}, Lcom/android/launcher2/PageIndicatorManager;->getHitRect(Landroid/graphics/Rect;I)V

    .line 3414
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PagedView;->invalidate(Landroid/graphics/Rect;)V

    .line 3415
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher2/Launcher;->isAirMoveOninSettings()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 3416
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher2/Launcher;->registerForAirMotionGestureListner()V

    .line 3419
    .end local v22    # "r":Landroid/graphics/Rect;
    :cond_c
    sget-boolean v3, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-nez v3, :cond_7

    .line 3420
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/PagedView;->mPagePreviewSnapDelay:Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;->snapTo(I)V

    goto/16 :goto_2

    .line 3477
    .end local v19    # "i":I
    .end local v23    # "result":Z
    :pswitch_3
    sget-boolean v3, Lcom/android/launcher2/Launcher;->CSCFEATURE_LAUNCHER_HOMESCREENEDITMODE:Z

    if-eqz v3, :cond_d

    invoke-static {}, Lcom/android/launcher2/Launcher;->isHomeRemoveMode()Z

    move-result v3

    if-nez v3, :cond_2

    .line 3478
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/PagedView;->mPagePreviewSnapDelay:Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;

    invoke-virtual {v3}, Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;->cancel()V

    .line 3479
    invoke-direct/range {p0 .. p1}, Lcom/android/launcher2/PagedView;->findPageIndicatorHit(Landroid/view/DragEvent;)I

    move-result v14

    .line 3480
    .local v14, "pageIndicatorHit":I
    const/4 v3, -0x1

    if-eq v14, v3, :cond_2

    .line 3481
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {v3, v14}, Lcom/android/launcher2/PageIndicatorManager;->onDrop(I)Z

    .line 3483
    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_12

    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/DragState;

    move-object v8, v3

    .line 3486
    .local v8, "dragState":Lcom/android/launcher2/DragState;
    :goto_3
    invoke-virtual {v8}, Lcom/android/launcher2/DragState;->getItem()Lcom/android/launcher2/BaseItem;

    move-result-object v17

    .line 3490
    .local v17, "dragItem":Lcom/android/launcher2/BaseItem;
    move-object/from16 v0, v17

    instance-of v3, v0, Lcom/android/launcher2/HomePendingItem;

    if-nez v3, :cond_13

    move-object/from16 v0, v17

    instance-of v3, v0, Lcom/android/launcher2/HomeItem;

    if-eqz v3, :cond_13

    const/16 v21, 0x1

    .line 3491
    .local v21, "itemFromWorkspace":Z
    :goto_4
    const/16 v20, 0x0

    .line 3492
    .local v20, "itemFromHotseat":Z
    if-eqz v21, :cond_e

    move-object/from16 v18, v17

    .line 3493
    check-cast v18, Lcom/android/launcher2/HomeItem;

    .line 3494
    .local v18, "homeItem":Lcom/android/launcher2/HomeItem;
    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/android/launcher2/HomeItem;->container:J

    const-wide/16 v10, -0x65

    cmp-long v3, v4, v10

    if-nez v3, :cond_e

    .line 3495
    const/16 v20, 0x1

    .line 3498
    .end local v18    # "homeItem":Lcom/android/launcher2/HomeItem;
    :cond_e
    if-eqz v21, :cond_f

    move-object/from16 v0, v17

    iget v3, v0, Lcom/android/launcher2/BaseItem;->mScreen:I

    if-ne v14, v3, :cond_f

    if-eqz v20, :cond_10

    :cond_f
    sget-boolean v3, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v3, :cond_14

    :cond_10
    const/16 v16, 0x1

    .line 3499
    .local v16, "abortDrop":Z
    :goto_5
    if-nez v16, :cond_2

    .line 3505
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/CellLayout;

    .line 3506
    .local v2, "cl":Lcom/android/launcher2/CellLayout;
    if-eqz v2, :cond_11

    .line 3507
    const/4 v3, 0x2

    new-array v7, v3, [I

    .line 3508
    .local v7, "res":[I
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v17 .. v17}, Lcom/android/launcher2/BaseItem;->getSpanX()I

    move-result v5

    invoke-virtual/range {v17 .. v17}, Lcom/android/launcher2/BaseItem;->getSpanY()I

    move-result v6

    invoke-virtual/range {v2 .. v7}, Lcom/android/launcher2/CellLayout;->findNearestVacantArea(IIII[I)[I

    .line 3509
    const/4 v3, 0x0

    aget v3, v7, v3

    if-ltz v3, :cond_15

    const/4 v3, 0x1

    aget v3, v7, v3

    if-ltz v3, :cond_15

    .line 3511
    const/4 v3, 0x0

    aget v10, v7, v3

    const/4 v3, 0x1

    aget v11, v7, v3

    invoke-virtual/range {v17 .. v17}, Lcom/android/launcher2/BaseItem;->getSpanX()I

    move-result v12

    invoke-virtual/range {v17 .. v17}, Lcom/android/launcher2/BaseItem;->getSpanY()I

    move-result v13

    const/4 v15, 0x1

    move-object v9, v2

    invoke-virtual/range {v8 .. v15}, Lcom/android/launcher2/DragState;->moveItemTo(Lcom/android/launcher2/DragReceivable;IIIIIZ)Z

    .line 3513
    invoke-virtual {v8}, Lcom/android/launcher2/DragState;->onDrop()V

    .line 3514
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v3

    if-eq v14, v3, :cond_11

    move-object/from16 v3, p0

    .line 3515
    check-cast v3, Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->removeCreatedPageForDrag()V

    .line 3516
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/launcher2/PagedView;->snapToPage(I)V

    .line 3522
    .end local v7    # "res":[I
    :cond_11
    :goto_6
    const/16 v22, 0x1

    goto/16 :goto_0

    .line 3483
    .end local v2    # "cl":Lcom/android/launcher2/CellLayout;
    .end local v8    # "dragState":Lcom/android/launcher2/DragState;
    .end local v16    # "abortDrop":Z
    .end local v17    # "dragItem":Lcom/android/launcher2/BaseItem;
    .end local v20    # "itemFromHotseat":Z
    .end local v21    # "itemFromWorkspace":Z
    :cond_12
    sget-object v8, Lcom/android/launcher2/Launcher;->dragstate:Lcom/android/launcher2/DragState;

    goto/16 :goto_3

    .line 3490
    .restart local v8    # "dragState":Lcom/android/launcher2/DragState;
    .restart local v17    # "dragItem":Lcom/android/launcher2/BaseItem;
    :cond_13
    const/16 v21, 0x0

    goto :goto_4

    .line 3498
    .restart local v20    # "itemFromHotseat":Z
    .restart local v21    # "itemFromWorkspace":Z
    :cond_14
    const/16 v16, 0x0

    goto :goto_5

    .line 3519
    .restart local v2    # "cl":Lcom/android/launcher2/CellLayout;
    .restart local v7    # "res":[I
    .restart local v16    # "abortDrop":Z
    :cond_15
    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v8, v14, v3, v4}, Lcom/android/launcher2/CellLayout;->noRoomForDrop(Lcom/android/launcher2/DragState;III)V

    goto :goto_6

    .line 3528
    .end local v2    # "cl":Lcom/android/launcher2/CellLayout;
    .end local v7    # "res":[I
    .end local v8    # "dragState":Lcom/android/launcher2/DragState;
    .end local v14    # "pageIndicatorHit":I
    .end local v16    # "abortDrop":Z
    .end local v17    # "dragItem":Lcom/android/launcher2/BaseItem;
    .end local v20    # "itemFromHotseat":Z
    .end local v21    # "itemFromWorkspace":Z
    :pswitch_4
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/launcher2/PagedView;->motionTrackingIsCurrent:Z

    .line 3529
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/PagedView;->mPagePreviewSnapDelay:Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;

    invoke-virtual {v3}, Lcom/android/launcher2/PagedView$PagePreviewSnapDelay;->cancel()V

    goto/16 :goto_1

    .line 3392
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v13, 0x1

    .line 1531
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->updateVisiblePages()I

    move-result v7

    .line 1532
    .local v7, "visiblePages":I
    if-ge v7, v13, :cond_0

    .line 1601
    :goto_0
    return-void

    .line 1539
    :cond_0
    iget-boolean v8, p0, Lcom/android/launcher2/PagedView;->mHideItems:Z

    if-nez v8, :cond_c

    .line 1541
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mCurrentTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

    sget-object v9, Lcom/android/launcher2/PagedView$TransitionEffect;->CARD:Lcom/android/launcher2/PagedView$TransitionEffect;

    invoke-virtual {v8, v9}, Lcom/android/launcher2/PagedView$TransitionEffect;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1542
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v1

    .line 1546
    .local v1, "halfScreenSize":I
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v8

    add-int v6, v8, v1

    .line 1547
    .local v6, "screenCenter":I
    iget v8, p0, Lcom/android/launcher2/PagedView;->mLastScreenCenter:I

    if-eq v6, v8, :cond_1

    .line 1548
    invoke-virtual {p0, v6}, Lcom/android/launcher2/PagedView;->screenScrolled(I)V

    .line 1549
    iput v6, p0, Lcom/android/launcher2/PagedView;->mLastScreenCenter:I

    .line 1552
    :cond_1
    const/4 v0, 0x0

    .line 1553
    .local v0, "centerOfScreen":Lcom/android/launcher2/PagedView$PageInfo;
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mVisiblePages:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/launcher2/PagedView$PageInfo;

    .line 1554
    .local v5, "pageInfo":Lcom/android/launcher2/PagedView$PageInfo;
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mCurrentTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

    sget-object v9, Lcom/android/launcher2/PagedView$TransitionEffect;->CARD:Lcom/android/launcher2/PagedView$TransitionEffect;

    invoke-virtual {v8, v9}, Lcom/android/launcher2/PagedView$TransitionEffect;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1555
    if-eqz v0, :cond_3

    invoke-virtual {p0, v5}, Lcom/android/launcher2/PagedView;->getScrollProgress(Lcom/android/launcher2/PagedView$PageInfo;)F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-nez v8, :cond_2

    .line 1557
    :cond_3
    move-object v0, v5

    goto :goto_2

    .line 1544
    .end local v0    # "centerOfScreen":Lcom/android/launcher2/PagedView$PageInfo;
    .end local v1    # "halfScreenSize":I
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "pageInfo":Lcom/android/launcher2/PagedView$PageInfo;
    .end local v6    # "screenCenter":I
    :cond_4
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v8

    div-int/lit8 v1, v8, 0x2

    .restart local v1    # "halfScreenSize":I
    goto :goto_1

    .line 1560
    .restart local v0    # "centerOfScreen":Lcom/android/launcher2/PagedView$PageInfo;
    .restart local v4    # "i$":Ljava/util/Iterator;
    .restart local v5    # "pageInfo":Lcom/android/launcher2/PagedView$PageInfo;
    .restart local v6    # "screenCenter":I
    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {p0, v5}, Lcom/android/launcher2/PagedView;->getScrollProgress(Lcom/android/launcher2/PagedView$PageInfo;)F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getScrollProgress(Lcom/android/launcher2/PagedView$PageInfo;)F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    cmpg-float v8, v8, v9

    if-gez v8, :cond_2

    .line 1563
    :cond_6
    move-object v0, v5

    goto :goto_2

    .line 1568
    .end local v5    # "pageInfo":Lcom/android/launcher2/PagedView$PageInfo;
    :cond_7
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1569
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v8

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v9

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v10

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getWidth()I

    move-result v11

    add-int/2addr v10, v11

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v11

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getHeight()I

    move-result v12

    add-int/2addr v11, v12

    invoke-virtual {p1, v8, v9, v10, v11}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 1571
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_8

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isFastScrolling()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1575
    iput-boolean v13, p0, Lcom/android/launcher2/PagedView;->mPageTransformsDirty:Z

    .line 1578
    :cond_8
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getDrawingTime()J

    move-result-wide v2

    .line 1579
    .local v2, "drawingTime":J
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mVisiblePages:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_9
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/launcher2/PagedView$PageInfo;

    .line 1580
    .restart local v5    # "pageInfo":Lcom/android/launcher2/PagedView$PageInfo;
    if-eq v5, v0, :cond_9

    .line 1581
    invoke-direct {p0, p1, v5, v2, v3}, Lcom/android/launcher2/PagedView;->drawPage(Landroid/graphics/Canvas;Lcom/android/launcher2/PagedView$PageInfo;J)V

    goto :goto_3

    .line 1584
    .end local v5    # "pageInfo":Lcom/android/launcher2/PagedView$PageInfo;
    :cond_a
    if-eqz v0, :cond_b

    .line 1585
    invoke-direct {p0, p1, v0, v2, v3}, Lcom/android/launcher2/PagedView;->drawPage(Landroid/graphics/Canvas;Lcom/android/launcher2/PagedView$PageInfo;J)V

    .line 1587
    :cond_b
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1588
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/launcher2/PagedView;->mPageTransformsDirty:Z

    .line 1592
    .end local v0    # "centerOfScreen":Lcom/android/launcher2/PagedView$PageInfo;
    .end local v1    # "halfScreenSize":I
    .end local v2    # "drawingTime":J
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "screenCenter":I
    :cond_c
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    invoke-virtual {v8, p1, v10, v11}, Lcom/android/launcher2/PageIndicatorManager;->drawPageIndicator(Landroid/graphics/Canvas;J)V

    goto/16 :goto_0
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 3
    .param p1, "focused"    # Landroid/view/View;
    .param p2, "direction"    # I

    .prologue
    const/4 v0, 0x1

    .line 1641
    const/16 v1, 0x11

    if-ne p2, v1, :cond_0

    .line 1642
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v1

    if-lez v1, :cond_1

    .line 1643
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->snapToPage(I)V

    .line 1652
    :goto_0
    return v0

    .line 1646
    :cond_0
    const/16 v1, 0x42

    if-ne p2, v1, :cond_1

    .line 1647
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    .line 1648
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->snapToPage(I)V

    goto :goto_0

    .line 1652
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    goto :goto_0
.end method

.method public enableRollNavigation()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 3633
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/Launcher;

    .line 3634
    .local v1, "launcher":Lcom/android/launcher2/Launcher;
    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->isAirMoveOninSettings()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3635
    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->registerForAirMotionGestureListner()V

    .line 3639
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "motion_engine"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v5, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "motion_panning"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v5, :cond_3

    .line 3643
    :cond_1
    sget-boolean v2, Lcom/android/launcher2/PagedView;->DEBUGGABLE:Z

    if-eqz v2, :cond_2

    const-string v2, "MotionEngine"

    const-string v3, "motion setting is turned off"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3661
    :cond_2
    :goto_0
    return-void

    .line 3646
    :cond_3
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 3647
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    iput v2, p0, Lcom/android/launcher2/PagedView;->mCurrentRotation:I

    .line 3648
    sget-boolean v2, Lcom/android/launcher2/PagedView;->sIsMotionEngineEnabled:Z

    if-ne v2, v5, :cond_2

    iget-object v2, p0, Lcom/android/launcher2/PagedView;->mMotionSensorManager:Landroid/hardware/motion/MotionRecognitionManager;

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/android/launcher2/PagedView;->mMotionListenerRegistered:Z

    if-nez v2, :cond_2

    .line 3650
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    .line 3653
    iget-object v3, p0, Lcom/android/launcher2/PagedView;->mMotionSensorManager:Landroid/hardware/motion/MotionRecognitionManager;

    move-object v2, p0

    check-cast v2, Lcom/samsung/android/motion/MRListener;

    const/4 v4, 0x2

    invoke-virtual {v3, v2, v4}, Landroid/hardware/motion/MotionRecognitionManager;->registerListener(Lcom/samsung/android/motion/MRListener;I)V

    .line 3654
    iput-boolean v5, p0, Lcom/android/launcher2/PagedView;->mMotionListenerRegistered:Z

    .line 3655
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "motion_panning_sensitivity"

    const/4 v4, 0x5

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    rsub-int/lit8 v2, v2, 0xa

    mul-int/lit8 v2, v2, 0xf

    add-int/lit8 v2, v2, 0x28

    int-to-double v2, v2

    iput-wide v2, p0, Lcom/android/launcher2/PagedView;->mPanningBasis:D

    .line 3659
    sget-boolean v2, Lcom/android/launcher2/PagedView;->DEBUGGABLE:Z

    if-eqz v2, :cond_2

    const-string v2, "MotionEngine"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableRollNavigation : mPanningBasis "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/launcher2/PagedView;->mPanningBasis:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public focusableViewAvailable(Landroid/view/View;)V
    .locals 4
    .param p1, "focused"    # Landroid/view/View;

    .prologue
    .line 1680
    iget v3, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v3}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    .line 1681
    .local v0, "current":Landroid/view/View;
    move-object v2, p1

    .line 1683
    .local v2, "v":Landroid/view/View;
    :goto_0
    if-ne v2, v0, :cond_1

    .line 1684
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->focusableViewAvailable(Landroid/view/View;)V

    .line 1694
    :cond_0
    return-void

    .line 1687
    :cond_1
    if-eq v2, p0, :cond_0

    .line 1690
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 1691
    .local v1, "parent":Landroid/view/ViewParent;
    instance-of v3, v1, Landroid/view/View;

    if-eqz v3, :cond_0

    .line 1692
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .end local v2    # "v":Landroid/view/View;
    check-cast v2, Landroid/view/View;

    .restart local v2    # "v":Landroid/view/View;
    goto :goto_0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 437
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 422
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 432
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected getAdjustedPageIndex(I)I
    .locals 3
    .param p1, "pageIndex"    # I

    .prologue
    const/4 v1, 0x0

    .line 995
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 996
    .local v0, "lastPageIndex":I
    iput-boolean v1, p0, Lcom/android/launcher2/PagedView;->mIgnoreSecretPage:Z

    .line 997
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 998
    if-gez p1, :cond_0

    .line 1005
    .end local v0    # "lastPageIndex":I
    :goto_0
    return v0

    .line 1000
    .restart local v0    # "lastPageIndex":I
    :cond_0
    if-le p1, v0, :cond_1

    move v0, v1

    .line 1001
    goto :goto_0

    :cond_1
    move v0, p1

    .line 1003
    goto :goto_0

    .line 1005
    :cond_2
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method protected getAssociatedLowerPageBound(I)I
    .locals 2
    .param p1, "page"    # I

    .prologue
    .line 2992
    const/4 v0, 0x0

    add-int/lit8 v1, p1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected getAssociatedUpperPageBound(I)I
    .locals 3
    .param p1, "page"    # I

    .prologue
    .line 2995
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    .line 2996
    .local v0, "count":I
    add-int/lit8 v1, p1, 0x1

    add-int/lit8 v2, v0, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    return v1
.end method

.method protected getChildOffset(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 2509
    const/4 v1, 0x0

    .line 2510
    .local v1, "offset":I
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2511
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 2512
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 2514
    :cond_0
    return v1
.end method

.method public getClosestPageForScrollX(I)I
    .locals 4
    .param p1, "scrollX"    # I

    .prologue
    .line 873
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v0

    .line 876
    .local v0, "pageCount":I
    int-to-float v2, p1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageTotWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 877
    .local v1, "pageIndex":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 878
    rem-int/2addr v1, v0

    .line 880
    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->getAdjustedPageIndex(I)I

    move-result v1

    .line 882
    return v1
.end method

.method getComingPage()I
    .locals 2

    .prologue
    .line 479
    iget v0, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    goto :goto_0
.end method

.method getCurrentPage()I
    .locals 1

    .prologue
    .line 468
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    return v0
.end method

.method protected getCurrentPageDescription()Ljava/lang/String;
    .locals 4

    .prologue
    .line 3169
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f10006e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getComingPage()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFastScrollFactor()Lcom/android/launcher2/ScalarAnimator;
    .locals 1

    .prologue
    .line 3065
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {v0}, Lcom/android/launcher2/PageIndicatorManager;->getFastScrollFactor()Lcom/android/launcher2/ScalarAnimator;

    move-result-object v0

    return-object v0
.end method

.method public getFestivalPageCnt()I
    .locals 2

    .prologue
    .line 4108
    const/4 v0, 0x0

    .line 4109
    .local v0, "ret":I
    sget-boolean v1, Lcom/android/launcher2/LauncherApplication;->sFestivalPageLauncher:Z

    if-eqz v1, :cond_0

    .line 4110
    instance-of v1, p0, Lcom/android/launcher2/Workspace;

    if-eqz v1, :cond_0

    .line 4112
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->getFestivalScreenCount()I

    move-result v0

    .line 4115
    :cond_0
    return v0
.end method

.method protected getHorizontalVisibilityExtension()I
    .locals 1

    .prologue
    .line 1054
    const/4 v0, 0x0

    return v0
.end method

.method protected getInclusivePages(I)V
    .locals 11
    .param p1, "currentPageIndex"    # I

    .prologue
    .line 2891
    sget-object v9, Lcom/android/launcher2/PagedView;->sInclusivePages:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 2892
    sget-object v2, Lcom/android/launcher2/PagedView;->sInclusivePages:Ljava/util/ArrayList;

    .line 2894
    .local v2, "inclusivePages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2896
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCacheSize()I

    move-result v0

    .line 2897
    .local v0, "cacheSize":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v4

    .line 2900
    .local v4, "pageCount":I
    if-lt v0, v4, :cond_0

    .line 2901
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_8

    .line 2902
    sget-object v9, Lcom/android/launcher2/PagedView;->sInclusivePages:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2901
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2907
    .end local v1    # "i":I
    :cond_0
    add-int/lit8 v8, v0, -0x1

    .line 2908
    .local v8, "size":I
    int-to-float v9, v8

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 2909
    .local v5, "pagesToLeft":I
    div-int/lit8 v6, v8, 0x2

    .line 2910
    .local v6, "pagesToRight":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2911
    const/4 v1, 0x1

    .restart local v1    # "i":I
    :goto_1
    if-gt v1, v5, :cond_2

    .line 2912
    sub-int v3, p1, v1

    .line 2913
    .local v3, "leftIndex":I
    if-gez v3, :cond_1

    .line 2914
    add-int/2addr v3, v4

    .line 2917
    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2911
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2920
    .end local v3    # "leftIndex":I
    :cond_2
    const/4 v1, 0x1

    :goto_2
    if-gt v1, v6, :cond_8

    .line 2921
    add-int v7, p1, v1

    .line 2922
    .local v7, "rightIndex":I
    if-lt v7, v4, :cond_3

    .line 2923
    sub-int/2addr v7, v4

    .line 2925
    :cond_3
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2920
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2928
    .end local v1    # "i":I
    .end local v7    # "rightIndex":I
    :cond_4
    const/4 v1, 0x1

    .restart local v1    # "i":I
    :goto_3
    if-gt v1, v5, :cond_6

    .line 2929
    sub-int v3, p1, v1

    .line 2930
    .restart local v3    # "leftIndex":I
    if-ltz v3, :cond_5

    .line 2931
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2928
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2935
    .end local v3    # "leftIndex":I
    :cond_6
    const/4 v1, 0x1

    :goto_4
    if-gt v1, v6, :cond_8

    .line 2936
    add-int v7, p1, v1

    .line 2937
    .restart local v7    # "rightIndex":I
    if-ge v7, v4, :cond_7

    .line 2938
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2935
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2942
    .end local v5    # "pagesToLeft":I
    .end local v6    # "pagesToRight":I
    .end local v7    # "rightIndex":I
    .end local v8    # "size":I
    :cond_8
    return-void
.end method

.method public getLeftPage()Landroid/view/View;
    .locals 1

    .prologue
    .line 3784
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    if-nez v0, :cond_0

    .line 3785
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3787
    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected getNonLoopedMappedScrollX(I)I
    .locals 6
    .param p1, "scrollX"    # I

    .prologue
    .line 1036
    move v0, p1

    .line 1037
    .local v0, "adjustedScrollX":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1038
    invoke-direct {p0, p1}, Lcom/android/launcher2/PagedView;->getLowerBoundForScrollX(I)I

    move-result v2

    .line 1039
    .local v2, "lb":I
    sub-int v1, p1, v2

    .line 1040
    .local v1, "deltaScrollX":I
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getPageIndexForScrollX(I)I

    move-result v4

    .line 1041
    .local v4, "pg":I
    invoke-virtual {p0, v4}, Lcom/android/launcher2/PagedView;->getNonLoopedScrollXForPageIndex(I)I

    move-result v3

    .line 1043
    .local v3, "nonLoopedScrollXForPg":I
    add-int v0, v3, v1

    .line 1050
    .end local v1    # "deltaScrollX":I
    .end local v2    # "lb":I
    .end local v3    # "nonLoopedScrollXForPg":I
    .end local v4    # "pg":I
    :cond_0
    return v0
.end method

.method public getNonLoopedScrollXForPageIndex(I)I
    .locals 1
    .param p1, "pageIndex"    # I

    .prologue
    .line 949
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageTotWidth()I

    move-result v0

    mul-int/2addr v0, p1

    return v0
.end method

.method getPageAt(I)Landroid/view/View;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 487
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getPageBackgroundAlpha()F
    .locals 1

    .prologue
    .line 3374
    iget v0, p0, Lcom/android/launcher2/PagedView;->mPageBackgroundAlpha:F

    return v0
.end method

.method public getPageCacheSize()I
    .locals 1

    .prologue
    .line 3893
    const/4 v0, 0x3

    return v0
.end method

.method getPageCount()I
    .locals 1

    .prologue
    .line 483
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getPageIndexForItemId(J)I
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 3903
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 3904
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v4}, Lcom/android/launcher2/CellLayout;->getChildrenLayout()Lcom/android/launcher2/CellLayoutChildren;

    move-result-object v3

    .line 3905
    .local v3, "page":Lcom/android/launcher2/CellLayoutChildren;
    iget-object v4, v3, Lcom/android/launcher2/CellLayoutChildren;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/BaseItem;

    .line 3906
    .local v2, "item":Lcom/android/launcher2/BaseItem;
    iget-wide v4, v2, Lcom/android/launcher2/BaseItem;->mId:J

    cmp-long v4, v4, p1

    if-nez v4, :cond_0

    .line 3912
    .end local v0    # "i":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "item":Lcom/android/launcher2/BaseItem;
    .end local v3    # "page":Lcom/android/launcher2/CellLayoutChildren;
    :goto_1
    return v0

    .line 3903
    .restart local v0    # "i":I
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v3    # "page":Lcom/android/launcher2/CellLayoutChildren;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3912
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "page":Lcom/android/launcher2/CellLayoutChildren;
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected getPageIndexForScrollX(I)I
    .locals 5
    .param p1, "scrollX"    # I

    .prologue
    .line 886
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v0

    .line 887
    .local v0, "pageCount":I
    const/high16 v1, -0x80000000

    .line 889
    .local v1, "res":I
    invoke-direct {p0, p1}, Lcom/android/launcher2/PagedView;->getSlotForScrollX(I)I

    move-result v2

    .line 890
    .local v2, "slot":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 891
    if-gez p1, :cond_0

    .line 892
    add-int/lit8 v3, v0, -0x1

    rem-int v4, v2, v0

    sub-int v1, v3, v4

    .line 899
    :goto_0
    return v1

    .line 894
    :cond_0
    rem-int v1, v2, v0

    goto :goto_0

    .line 897
    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public getPageIndicatorManager()Lcom/android/launcher2/PageIndicatorManager;
    .locals 1

    .prologue
    .line 3182
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    return-object v0
.end method

.method protected getPageIndicatorManager(Lcom/android/launcher2/PagedView;III)Lcom/android/launcher2/PageIndicatorManager;
    .locals 1
    .param p1, "pagedView"    # Lcom/android/launcher2/PagedView;
    .param p2, "pageIndicatorTop"    # I
    .param p3, "pageIndicatorGap"    # I
    .param p4, "pageIndicatorMaxVisible"    # I

    .prologue
    .line 4025
    new-instance v0, Lcom/android/launcher2/PageIndicatorManager;

    invoke-direct {v0, p0, p2, p3, p4}, Lcom/android/launcher2/PageIndicatorManager;-><init>(Lcom/android/launcher2/PagedView;III)V

    return-object v0
.end method

.method public getPageNearestToCenterOfScreen()I
    .locals 15

    .prologue
    .line 2543
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v8

    .line 2544
    .local v8, "pageWidth":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v2

    .line 2545
    .local v2, "leftEdge":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v13

    add-int v10, v13, v8

    .line 2546
    .local v10, "rightEdge":I
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->getPageIndexForScrollX(I)I

    move-result v5

    .line 2547
    .local v5, "pageLeftIndex":I
    invoke-virtual {p0, v10}, Lcom/android/launcher2/PagedView;->getPageIndexForScrollX(I)I

    move-result v7

    .line 2548
    .local v7, "pageRightIndex":I
    const/high16 v0, -0x80000000

    .line 2550
    .local v0, "indexOfPageOnScreen":I
    if-ne v5, v7, :cond_1

    .line 2551
    move v0, v5

    .line 2585
    :cond_0
    :goto_0
    return v0

    .line 2553
    :cond_1
    invoke-virtual {p0, v5}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v3

    .line 2554
    .local v3, "leftView":Landroid/view/View;
    invoke-virtual {p0, v7}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v11

    .line 2555
    .local v11, "rightView":Landroid/view/View;
    if-eqz v3, :cond_0

    if-eqz v11, :cond_0

    .line 2556
    invoke-direct {p0, v2}, Lcom/android/launcher2/PagedView;->getLowerBoundForScrollX(I)I

    move-result v13

    invoke-direct {p0, v3}, Lcom/android/launcher2/PagedView;->getCenterOfViewRelative(Landroid/view/View;)I

    move-result v14

    add-int v4, v13, v14

    .line 2557
    .local v4, "pageLeftCenter":I
    invoke-direct {p0, v10}, Lcom/android/launcher2/PagedView;->getLowerBoundForScrollX(I)I

    move-result v13

    invoke-direct {p0, v11}, Lcom/android/launcher2/PagedView;->getCenterOfViewRelative(Landroid/view/View;)I

    move-result v14

    add-int v6, v13, v14

    .line 2558
    .local v6, "pageRightCenter":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v13

    div-int/lit8 v14, v8, 0x2

    add-int v12, v13, v14

    .line 2559
    .local v12, "screenCenter":I
    sub-int v13, v12, v4

    invoke-static {v13}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 2560
    .local v1, "leftDelta":I
    sub-int v13, v6, v12

    invoke-static {v13}, Ljava/lang/Math;->abs(I)I

    move-result v9

    .line 2563
    .local v9, "rightDelta":I
    iget v13, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    if-ne v13, v5, :cond_7

    .line 2564
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v13

    iget v13, v13, Landroid/content/res/Configuration;->orientation:I

    const/4 v14, 0x2

    if-ne v13, v14, :cond_3

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isScreenLarge()Z

    move-result v13

    if-nez v13, :cond_3

    .line 2565
    add-int v13, v1, v9

    div-int/lit8 v13, v13, 0xa

    if-lt v1, v13, :cond_2

    move v0, v7

    :goto_1
    goto :goto_0

    :cond_2
    move v0, v5

    goto :goto_1

    .line 2566
    :cond_3
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isScreenLarge()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 2567
    add-int v13, v1, v9

    div-int/lit8 v13, v13, 0x2

    if-lt v1, v13, :cond_4

    move v0, v7

    :goto_2
    goto :goto_0

    :cond_4
    move v0, v5

    goto :goto_2

    .line 2569
    :cond_5
    add-int v13, v1, v9

    div-int/lit8 v13, v13, 0x3

    if-lt v1, v13, :cond_6

    move v0, v7

    :goto_3
    goto :goto_0

    :cond_6
    move v0, v5

    goto :goto_3

    .line 2573
    :cond_7
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isScreenLarge()Z

    move-result v13

    if-eqz v13, :cond_9

    .line 2574
    add-int v13, v1, v9

    div-int/lit8 v13, v13, 0x2

    if-lt v9, v13, :cond_8

    move v0, v5

    :goto_4
    goto :goto_0

    :cond_8
    move v0, v7

    goto :goto_4

    .line 2576
    :cond_9
    add-int v13, v1, v9

    div-int/lit8 v13, v13, 0x3

    if-lt v9, v13, :cond_a

    move v0, v5

    :goto_5
    goto/16 :goto_0

    :cond_a
    move v0, v7

    goto :goto_5
.end method

.method public getPageSpacing()I
    .locals 1

    .prologue
    .line 3085
    iget v0, p0, Lcom/android/launcher2/PagedView;->mPageSpacing:I

    return v0
.end method

.method public getPageSwitchListener()Lcom/android/launcher2/PagedView$PageSwitchListener;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageSwitchListener:Lcom/android/launcher2/PagedView$PageSwitchListener;

    return-object v0
.end method

.method protected getPageTotWidth()I
    .locals 4

    .prologue
    .line 842
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v2

    iget v3, p0, Lcom/android/launcher2/PagedView;->mPageSpacing:I

    add-int v1, v2, v3

    .line 848
    .local v1, "result":I
    if-gtz v1, :cond_0

    .line 849
    const/16 v0, 0x13

    .line 850
    .local v0, "RANDOM_NUMBER_WITH_NO_MEANING":I
    const/16 v1, 0x13

    .line 853
    .end local v0    # "RANDOM_NUMBER_WITH_NO_MEANING":I
    :cond_0
    return v1
.end method

.method public getPageZoom()F
    .locals 1

    .prologue
    .line 3360
    iget v0, p0, Lcom/android/launcher2/PagedView;->mPageZoom:F

    return v0
.end method

.method protected getRelativeChildOffset(I)I
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 2497
    const/4 v1, 0x0

    .line 2498
    .local v1, "offset":I
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2499
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 2500
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getNonLoopedScrollXForPageIndex(I)I

    move-result v3

    sub-int v1, v2, v3

    .line 2502
    :cond_0
    return v1
.end method

.method public getRightPage()Landroid/view/View;
    .locals 2

    .prologue
    .line 3792
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 3793
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3795
    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected getScrollProgress(Lcom/android/launcher2/PagedView$PageInfo;)F
    .locals 8
    .param p1, "pageInfo"    # Lcom/android/launcher2/PagedView$PageInfo;

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 2062
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v3

    .line 2063
    .local v3, "totalDistance":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v1

    .line 2064
    .local v1, "screenLeft":I
    iget v4, p1, Lcom/android/launcher2/PagedView$PageInfo;->mLowerBound:I

    .line 2065
    .local v4, "viewLeft":I
    sub-int v0, v1, v4

    .line 2066
    .local v0, "delta":I
    int-to-float v5, v0

    int-to-float v6, v3

    mul-float/2addr v6, v7

    div-float v2, v5, v6

    .line 2067
    .local v2, "scrollProgress":F
    invoke-static {v2, v7}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 2068
    const/high16 v5, -0x40800000    # -1.0f

    invoke-static {v2, v5}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 2070
    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2071
    const/4 v2, 0x0

    .line 2073
    :cond_0
    return v2
.end method

.method public getScroller()Landroid/widget/Scroller;
    .locals 1

    .prologue
    .line 3934
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    return-object v0
.end method

.method public getSecretPageCnt()I
    .locals 1

    .prologue
    .line 4084
    const/4 v0, 0x0

    .line 4104
    .local v0, "ret":I
    return v0
.end method

.method public handleScrollOnOrientationChange()V
    .locals 1

    .prologue
    .line 3883
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3884
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getComingPage()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    .line 3888
    :cond_0
    :goto_0
    return-void

    .line 3885
    :cond_1
    iget v0, p0, Lcom/android/launcher2/PagedView;->mTouchState:I

    if-eqz v0, :cond_0

    .line 3886
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getClosestPageForScrollX(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    goto :goto_0
.end method

.method public hidePageIndicator(Z)V
    .locals 1
    .param p1, "animated"    # Z

    .prologue
    .line 3178
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/PageIndicatorManager;->hidePageIndicator(Z)V

    .line 3179
    return-void
.end method

.method protected hitsNextPage(FF)Z
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 1733
    iget v2, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1734
    .local v0, "child":Landroid/view/View;
    const/4 v1, 0x0

    .line 1735
    .local v1, "res":Z
    if-eqz v0, :cond_0

    .line 1736
    iget v2, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->getRelativeChildOffset(I)I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v2, p1, v2

    if-lez v2, :cond_1

    const/4 v1, 0x1

    .line 1738
    :cond_0
    :goto_0
    return v1

    .line 1736
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected hitsPreviousPage(FF)Z
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 1721
    iget v2, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1722
    .local v0, "child":Landroid/view/View;
    const/4 v1, 0x0

    .line 1723
    .local v1, "res":Z
    if-eqz v0, :cond_0

    .line 1724
    iget v2, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->getRelativeChildOffset(I)I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, p1, v2

    if-gez v2, :cond_1

    const/4 v1, 0x1

    .line 1726
    :cond_0
    :goto_0
    return v1

    .line 1724
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected indexToPage(I)I
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 491
    return p1
.end method

.method protected initCurrentPage(I)V
    .locals 0
    .param p1, "currentPage"    # I

    .prologue
    .line 511
    iput p1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    .line 512
    return-void
.end method

.method protected invalidatePageData()V
    .locals 2

    .prologue
    .line 3014
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/PagedView;->invalidatePageData(IZ)V

    .line 3015
    return-void
.end method

.method protected invalidatePageData(I)V
    .locals 1
    .param p1, "currentPage"    # I

    .prologue
    .line 3017
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher2/PagedView;->invalidatePageData(IZ)V

    .line 3018
    return-void
.end method

.method protected invalidatePageData(IZ)V
    .locals 6
    .param p1, "currentPage"    # I
    .param p2, "immediateAndOnly"    # Z

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x1

    .line 3020
    iget-boolean v2, p0, Lcom/android/launcher2/PagedView;->mIsDataReady:Z

    if-nez v2, :cond_1

    .line 3061
    :cond_0
    :goto_0
    return-void

    .line 3024
    :cond_1
    iget-boolean v2, p0, Lcom/android/launcher2/PagedView;->mContentIsRefreshable:Z

    if-eqz v2, :cond_0

    .line 3025
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 3034
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->syncPages()V

    .line 3037
    iput-boolean v4, p0, Lcom/android/launcher2/PagedView;->mPageTransformsDirty:Z

    .line 3041
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v2

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v3

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/android/launcher2/PagedView;->measure(II)V

    .line 3044
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->mPendingSavedState:Lcom/android/launcher2/PagedView$SavedState;

    if-eqz v2, :cond_3

    .line 3045
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->mPendingSavedState:Lcom/android/launcher2/PagedView$SavedState;

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->restoreCurrentPageFromState(Lcom/android/launcher2/PagedView$SavedState;)V

    .line 3046
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/launcher2/PagedView;->mPendingSavedState:Lcom/android/launcher2/PagedView$SavedState;

    .line 3052
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    .line 3053
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v0, :cond_4

    .line 3054
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3053
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3047
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_3
    const/4 v2, -0x1

    if-le p1, v2, :cond_2

    .line 3048
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->setCurrentPage(I)V

    goto :goto_1

    .line 3058
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_4
    iget v2, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-direct {p0, v2, p2}, Lcom/android/launcher2/PagedView;->loadAssociatedPages(IZ)V

    .line 3059
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->requestLayout()V

    goto :goto_0
.end method

.method public isAnimating()Z
    .locals 1

    .prologue
    .line 3382
    const/4 v0, 0x0

    return v0
.end method

.method protected isDataReady()Z
    .locals 1

    .prologue
    .line 459
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->mIsDataReady:Z

    return v0
.end method

.method public isEnabledFingerHovering()Z
    .locals 1

    .prologue
    .line 3309
    const/4 v0, 0x0

    return v0
.end method

.method isFastScrolling()Z
    .locals 1

    .prologue
    .line 2142
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {v0}, Lcom/android/launcher2/PageIndicatorManager;->isFastScrolling()Z

    move-result v0

    return v0
.end method

.method public isHoveringAreaX(F)I
    .locals 3
    .param p1, "x"    # F

    .prologue
    .line 3313
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/android/launcher2/PagedView;->mHoverScrollRightPadding:I

    sub-int v0, v1, v2

    .line 3315
    .local v0, "width":I
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/android/launcher2/PagedView;->mHoverScrollWidth:I

    int-to-float v1, v1

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_0

    .line 3317
    const/4 v1, 0x2

    .line 3323
    :goto_0
    return v1

    .line 3318
    :cond_0
    iget v1, p0, Lcom/android/launcher2/PagedView;->mHoverScrollWidth:I

    sub-int v1, v0, v1

    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_1

    int-to-float v1, v0

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_1

    .line 3320
    const/4 v1, 0x3

    goto :goto_0

    .line 3323
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isHoveringAreaY(F)Z
    .locals 1
    .param p1, "y"    # F

    .prologue
    .line 3327
    iget v0, p0, Lcom/android/launcher2/PagedView;->mHoverScrollTop:I

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/launcher2/PagedView;->mHoverScrollBottom:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    .line 3328
    const/4 v0, 0x1

    .line 3331
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isLoopingEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 838
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->isLoopingDisabledInCSC()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v1

    if-le v1, v0, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isFastScrolling()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isPageAddedForDrag(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 4081
    const/4 v0, 0x0

    return v0
.end method

.method public isPageConstructed(I)Z
    .locals 3
    .param p1, "i"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2986
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->mContentIsRefreshable:Z

    if-nez v0, :cond_0

    .line 2988
    :goto_0
    return v1

    .line 2987
    :cond_0
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    move v1, v2

    goto :goto_0

    .line 2988
    :cond_1
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public isPageFastMoving()Z
    .locals 1

    .prologue
    .line 725
    sget-boolean v0, Lcom/android/launcher2/PagedView;->mIsPageFastMoving:Z

    return v0
.end method

.method protected isPageInclusive(I)Z
    .locals 2
    .param p1, "page"    # I

    .prologue
    .line 2879
    sget-object v0, Lcom/android/launcher2/PagedView;->sInclusivePages:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected isPageMoving()Z
    .locals 1

    .prologue
    .line 710
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->mIsPageMoving:Z

    return v0
.end method

.method protected loadAssociatedPages(I)V
    .locals 1
    .param p1, "page"    # I

    .prologue
    .line 2840
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/PagedView;->loadAssociatedPages(IZ)V

    .line 2841
    return-void
.end method

.method protected maxOverScroll()F
    .locals 3

    .prologue
    .line 2136
    const/high16 v0, 0x3f800000    # 1.0f

    .line 2137
    .local v0, "f":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float v1, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/launcher2/PagedView;->overScrollInfluenceCurve(F)F

    move-result v2

    mul-float v0, v1, v2

    .line 2138
    const v1, 0x3e0f5c29    # 0.14f

    mul-float/2addr v1, v0

    return v1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 3872
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 3873
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->mLaidOutAfterViewTreeDirty:Z

    .line 3874
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->mFirstLayout:Z

    .line 3875
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {v0}, Lcom/android/launcher2/PageIndicatorManager;->handleOrientationChange()V

    .line 3876
    return-void
.end method

.method protected onDrawComplete(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1525
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v4, 0x9

    const/4 v3, 0x0

    .line 2408
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_0

    .line 2409
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 2432
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    :goto_0
    return v2

    .line 2414
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    .line 2415
    const/4 v1, 0x0

    .line 2416
    .local v1, "vscroll":F
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    .line 2421
    .local v0, "hscroll":F
    :goto_1
    cmpl-float v2, v0, v3

    if-nez v2, :cond_1

    cmpl-float v2, v1, v3

    if-eqz v2, :cond_0

    .line 2422
    :cond_1
    cmpl-float v2, v0, v3

    if-gtz v2, :cond_2

    cmpl-float v2, v1, v3

    if-lez v2, :cond_4

    .line 2423
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->scrollRight()V

    .line 2427
    :goto_2
    const/4 v2, 0x1

    goto :goto_0

    .line 2418
    .end local v0    # "hscroll":F
    .end local v1    # "vscroll":F
    :cond_3
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v2

    neg-float v1, v2

    .line 2419
    .restart local v1    # "vscroll":F
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    .restart local v0    # "hscroll":F
    goto :goto_1

    .line 2425
    :cond_4
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->scrollLeft()V

    goto :goto_2

    .line 2409
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3209
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/launcher2/PagedView;->isHoveringAreaX(F)I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/launcher2/PagedView;->isHoveringAreaY(F)Z

    move-result v4

    if-nez v4, :cond_3

    .line 3210
    :cond_0
    iget-boolean v4, p0, Lcom/android/launcher2/PagedView;->mScrollByHover:Z

    if-eqz v4, :cond_1

    .line 3211
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->setHoveringSpenIcon(I)V

    .line 3212
    iput-boolean v2, p0, Lcom/android/launcher2/PagedView;->mScrollByHover:Z

    :cond_1
    move v2, v3

    .line 3235
    :cond_2
    :goto_0
    return v2

    .line 3217
    :cond_3
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    .line 3220
    .local v1, "toolType":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v4

    if-gtz v4, :cond_4

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_0

    .line 3222
    :cond_4
    if-ne v1, v3, :cond_6

    .line 3224
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isEnabledFingerHovering()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3226
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "finger_air_view"

    invoke-static {v4, v5, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 3227
    .local v0, "isFingerHoveringOn":I
    if-eqz v0, :cond_2

    .end local v0    # "isFingerHoveringOn":I
    :cond_5
    move v2, v3

    .line 3235
    goto :goto_0

    .line 3228
    :cond_6
    const/4 v4, 0x3

    if-eq v1, v4, :cond_2

    .line 3231
    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 3232
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->scrollByHover(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 3159
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 3160
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    .line 3161
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    .line 3162
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 3163
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    .line 3164
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    .line 3166
    :cond_0
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 3153
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 3154
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    .line 3155
    return-void
.end method

.method public onInterceptHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3188
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "com.sec.feature.hovering_ui"

    invoke-static {v4, v5}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 3204
    :cond_0
    :goto_0
    return v2

    .line 3191
    :cond_1
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 3192
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "pen_hovering"

    invoke-static {v4, v5, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 3193
    .local v1, "isHoveringOn":I
    if-eqz v1, :cond_0

    .line 3195
    .end local v1    # "isHoveringOn":I
    :cond_2
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v4

    if-ne v4, v3, :cond_3

    .line 3197
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "finger_air_view"

    invoke-static {v4, v5, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 3198
    .local v0, "isFingerHoveringOn":I
    if-eqz v0, :cond_0

    .line 3201
    .end local v0    # "isFingerHoveringOn":I
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/launcher2/PagedView;->isHoveringAreaX(F)I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/launcher2/PagedView;->isHoveringAreaY(F)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    .line 3204
    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1744
    const/4 v3, 0x0

    .line 1745
    .local v3, "touchOnPageIndicator":Z
    iget-object v7, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/android/launcher2/PageIndicatorManager;->isInPageIndicator(FF)Z

    move-result v3

    .line 1748
    sget-boolean v7, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v7, :cond_5

    .line 1749
    if-eqz v3, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    if-nez v7, :cond_1

    .line 1750
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Lcom/android/launcher2/Launcher;

    iget-object v7, v7, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v7}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1752
    const/4 v7, 0x0

    .line 1926
    :goto_0
    return v7

    .line 1755
    :cond_0
    const/4 v7, 0x0

    goto :goto_0

    .line 1759
    :cond_1
    sget-boolean v7, Lcom/android/launcher2/guide/GuideFragment;->isGuideCompleted:Z

    if-eqz v7, :cond_2

    .line 1760
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Landroid/app/Activity;

    invoke-static {v7}, Lcom/android/launcher2/guide/GuideFragment;->showCompleteDialog(Landroid/app/Activity;)V

    .line 1761
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v7

    sput v7, Lcom/android/launcher2/guide/GuideFragment;->currentPageToStopNavigation:I

    .line 1762
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    const/4 v8, 0x2

    if-eq v7, v8, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_4

    .line 1764
    :cond_3
    sget-object v7, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v8, "navigation"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 1765
    const/4 v7, 0x0

    goto :goto_0

    .line 1769
    :cond_4
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isPageMoving()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1770
    const/4 v7, 0x0

    goto :goto_0

    .line 1774
    :cond_5
    iget-boolean v7, p0, Lcom/android/launcher2/PagedView;->motionTrackingIsCurrent:Z

    if-nez v7, :cond_6

    .line 1775
    invoke-direct {p0, p1}, Lcom/android/launcher2/PagedView;->updateMotionTracking(Landroid/view/MotionEvent;)V

    .line 1781
    :cond_6
    invoke-direct {p0, p1}, Lcom/android/launcher2/PagedView;->acquireVelocityTrackerAndAddMovement(Landroid/view/MotionEvent;)V

    .line 1784
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v7

    if-gtz v7, :cond_7

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    goto :goto_0

    .line 1791
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1792
    .local v0, "action":I
    const/4 v7, 0x2

    if-ne v0, v7, :cond_8

    iget v7, p0, Lcom/android/launcher2/PagedView;->mTouchState:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_8

    .line 1794
    const/4 v7, 0x1

    goto :goto_0

    .line 1797
    :cond_8
    const/4 v2, 0x0

    .line 1798
    .local v2, "shouldInterceptForFastScroll":Z
    sget-object v7, Lcom/android/launcher2/PagedView;->cpuBooster:Landroid/os/DVFSHelper;

    const-string v8, "Launcher_touch"

    invoke-virtual {v7, v8}, Landroid/os/DVFSHelper;->addExtraOptionsByDefaultPolicy(Ljava/lang/String;)V

    .line 1800
    sget-object v7, Lcom/android/launcher2/PagedView;->busBooster:Landroid/os/DVFSHelper;

    const-string v8, "Launcher_touch"

    invoke-virtual {v7, v8}, Landroid/os/DVFSHelper;->addExtraOptionsByDefaultPolicy(Ljava/lang/String;)V

    .line 1802
    sget-object v7, Lcom/android/launcher2/PagedView;->cpuMaxBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v7}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v1

    .line 1803
    .local v1, "coreTable":[I
    if-eqz v1, :cond_9

    .line 1804
    sget-object v7, Lcom/android/launcher2/PagedView;->cpuMaxBooster:Landroid/os/DVFSHelper;

    const-string v8, "CORE_NUM"

    const/4 v9, 0x0

    aget v9, v1, v9

    int-to-long v10, v9

    invoke-virtual {v7, v8, v10, v11}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 1806
    :cond_9
    and-int/lit16 v7, v0, 0xff

    packed-switch v7, :pswitch_data_0

    .line 1926
    :cond_a
    :goto_1
    :pswitch_0
    iget v7, p0, Lcom/android/launcher2/PagedView;->mTouchState:I

    if-eqz v7, :cond_b

    iget v7, p0, Lcom/android/launcher2/PagedView;->mTouchState:I

    const/4 v8, 0x4

    if-ne v7, v8, :cond_c

    :cond_b
    if-eqz v2, :cond_19

    :cond_c
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 1811
    :pswitch_1
    iget-boolean v7, p0, Lcom/android/launcher2/PagedView;->mInGesture:Z

    if-eqz v7, :cond_d

    .line 1812
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->determineScrollingStart(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 1824
    :cond_d
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 1825
    .local v4, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    .line 1826
    .local v6, "y":F
    iput v4, p0, Lcom/android/launcher2/PagedView;->mTouchDownPointX:F

    .line 1827
    iget-object v7, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {v7, v4, v6}, Lcom/android/launcher2/PageIndicatorManager;->isInPageIndicator(FF)Z

    move-result v2

    .line 1828
    const/4 v7, 0x0

    iput v7, p0, Lcom/android/launcher2/PagedView;->mSnapState:I

    .line 1831
    sget-object v7, Lcom/android/launcher2/PagedView;->chipset:Ljava/lang/String;

    const-string v8, "8930"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_e

    sget-object v7, Lcom/android/launcher2/PagedView;->chipset:Ljava/lang/String;

    const-string v8, "piranha"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 1832
    :cond_e
    sget-object v7, Lcom/android/launcher2/PagedView;->cpuMaxBooster:Landroid/os/DVFSHelper;

    const-string v8, "Launcher_touch"

    invoke-virtual {v7, v8}, Landroid/os/DVFSHelper;->acquire(Ljava/lang/String;)V

    .line 1833
    sget-object v7, Lcom/android/launcher2/PagedView;->gpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v7}, Landroid/os/DVFSHelper;->getSupportedGPUFrequency()[I

    move-result-object v7

    if-eqz v7, :cond_f

    .line 1834
    sget-object v7, Lcom/android/launcher2/PagedView;->gpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v7}, Landroid/os/DVFSHelper;->acquire()V

    .line 1849
    :cond_f
    :goto_2
    const/4 v7, 0x0

    iput v7, p0, Lcom/android/launcher2/PagedView;->mTotalMotionX:F

    .line 1850
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/android/launcher2/PagedView;->isShowCamera:Z

    .line 1851
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/android/launcher2/PagedView;->mAllowLongPress:Z

    .line 1853
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/android/launcher2/PagedView;->mUseSlopJump:Z

    .line 1854
    const/4 v7, 0x0

    iput v7, p0, Lcom/android/launcher2/PagedView;->mTotalPredictionX:F

    .line 1860
    iget-object v7, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->getFinalX()I

    move-result v7

    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v8}, Landroid/widget/Scroller;->getCurrX()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 1861
    .local v5, "xDist":I
    iget-object v7, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->isFinished()Z

    move-result v7

    if-nez v7, :cond_10

    iget v7, p0, Lcom/android/launcher2/PagedView;->mTouchSlop:I

    if-ge v5, v7, :cond_13

    .line 1862
    :cond_10
    const/4 v7, 0x0

    iput v7, p0, Lcom/android/launcher2/PagedView;->mTouchState:I

    .line 1863
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->abortScroll()V

    .line 1869
    :goto_3
    sget-boolean v7, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-nez v7, :cond_a

    .line 1874
    invoke-virtual {p0, v4, v6}, Lcom/android/launcher2/PagedView;->hitsPreviousPage(FF)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 1875
    const/4 v7, 0x2

    iput v7, p0, Lcom/android/launcher2/PagedView;->mTouchState:I

    .line 1880
    :cond_11
    :goto_4
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/android/launcher2/PagedView;->mInGesture:Z

    goto/16 :goto_1

    .line 1838
    .end local v5    # "xDist":I
    :cond_12
    sget-object v7, Lcom/android/launcher2/PagedView;->cpuBooster:Landroid/os/DVFSHelper;

    const-string v8, "Launcher_touch"

    invoke-virtual {v7, v8}, Landroid/os/DVFSHelper;->acquire(Ljava/lang/String;)V

    .line 1839
    sget-object v7, Lcom/android/launcher2/PagedView;->busBooster:Landroid/os/DVFSHelper;

    const-string v8, "Launcher_touch"

    invoke-virtual {v7, v8}, Landroid/os/DVFSHelper;->acquire(Ljava/lang/String;)V

    .line 1841
    instance-of v7, p0, Lcom/android/launcher2/Workspace;

    if-eqz v7, :cond_f

    iget-object v7, p0, Lcom/android/launcher2/PagedView;->mCurrentTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

    sget-object v8, Lcom/android/launcher2/PagedView$TransitionEffect;->SPIRAL:Lcom/android/launcher2/PagedView$TransitionEffect;

    invoke-virtual {v7, v8}, Lcom/android/launcher2/PagedView$TransitionEffect;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    sget-object v7, Lcom/android/launcher2/PagedView;->gpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v7}, Landroid/os/DVFSHelper;->getSupportedGPUFrequency()[I

    move-result-object v7

    if-eqz v7, :cond_f

    .line 1844
    sget-object v7, Lcom/android/launcher2/PagedView;->gpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v7}, Landroid/os/DVFSHelper;->acquire()V

    goto :goto_2

    .line 1865
    .restart local v5    # "xDist":I
    :cond_13
    const/4 v7, 0x1

    iput v7, p0, Lcom/android/launcher2/PagedView;->mTouchState:I

    goto :goto_3

    .line 1876
    :cond_14
    invoke-virtual {p0, v4, v6}, Lcom/android/launcher2/PagedView;->hitsNextPage(FF)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 1877
    const/4 v7, 0x3

    iput v7, p0, Lcom/android/launcher2/PagedView;->mTouchState:I

    goto :goto_4

    .line 1885
    .end local v4    # "x":F
    .end local v5    # "xDist":I
    .end local v6    # "y":F
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iput v7, p0, Lcom/android/launcher2/PagedView;->mTouchUpPointX:F

    .line 1886
    sget-object v7, Lcom/android/launcher2/PagedView;->chipset:Ljava/lang/String;

    const-string v8, "8930"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_15

    sget-object v7, Lcom/android/launcher2/PagedView;->chipset:Ljava/lang/String;

    const-string v8, "piranha"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_18

    .line 1887
    :cond_15
    sget-object v7, Lcom/android/launcher2/PagedView;->cpuMaxBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v7}, Landroid/os/DVFSHelper;->release()V

    .line 1888
    sget-object v7, Lcom/android/launcher2/PagedView;->gpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v7}, Landroid/os/DVFSHelper;->getSupportedGPUFrequency()[I

    move-result-object v7

    if-eqz v7, :cond_16

    .line 1889
    sget-object v7, Lcom/android/launcher2/PagedView;->gpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v7}, Landroid/os/DVFSHelper;->release()V

    .line 1902
    :cond_16
    :goto_5
    :pswitch_4
    const/4 v7, 0x4

    iget v8, p0, Lcom/android/launcher2/PagedView;->mTouchState:I

    if-ne v7, v8, :cond_17

    .line 1903
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->snapToDestination()V

    .line 1905
    :cond_17
    const/4 v7, 0x0

    iput v7, p0, Lcom/android/launcher2/PagedView;->mTouchState:I

    .line 1906
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/android/launcher2/PagedView;->mAllowLongPress:Z

    .line 1907
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->releaseVelocityTracker()V

    .line 1908
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/android/launcher2/PagedView;->mInGesture:Z

    .line 1909
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/android/launcher2/PagedView;->motionTrackingIsCurrent:Z

    goto/16 :goto_1

    .line 1892
    :cond_18
    sget-object v7, Lcom/android/launcher2/PagedView;->cpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v7}, Landroid/os/DVFSHelper;->release()V

    .line 1893
    sget-object v7, Lcom/android/launcher2/PagedView;->busBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v7}, Landroid/os/DVFSHelper;->release()V

    .line 1895
    instance-of v7, p0, Lcom/android/launcher2/Workspace;

    if-eqz v7, :cond_16

    iget-object v7, p0, Lcom/android/launcher2/PagedView;->mCurrentTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

    sget-object v8, Lcom/android/launcher2/PagedView$TransitionEffect;->SPIRAL:Lcom/android/launcher2/PagedView$TransitionEffect;

    invoke-virtual {v7, v8}, Lcom/android/launcher2/PagedView$TransitionEffect;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_16

    sget-object v7, Lcom/android/launcher2/PagedView;->gpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v7}, Landroid/os/DVFSHelper;->getSupportedGPUFrequency()[I

    move-result-object v7

    if-eqz v7, :cond_16

    .line 1898
    sget-object v7, Lcom/android/launcher2/PagedView;->gpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v7}, Landroid/os/DVFSHelper;->release()V

    goto :goto_5

    .line 1913
    :pswitch_5
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/android/launcher2/PagedView;->motionTrackingIsCurrent:Z

    goto/16 :goto_1

    .line 1917
    :pswitch_6
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/android/launcher2/PagedView;->motionTrackingIsCurrent:Z

    .line 1918
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->releaseVelocityTracker()V

    goto/16 :goto_1

    .line 1926
    :cond_19
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 1806
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 19
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 1326
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/launcher2/PagedView;->mIsDataReady:Z

    if-nez v15, :cond_0

    .line 1416
    :goto_0
    return-void

    .line 1334
    :cond_0
    sub-int v14, p4, p2

    .line 1335
    .local v14, "pageWidth":I
    sub-int v12, p5, p3

    .line 1336
    .local v12, "pageHeight":I
    const/4 v13, 0x0

    .line 1337
    .local v13, "pageLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v3

    .line 1338
    .local v3, "childCount":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    if-ge v10, v3, :cond_3

    .line 1339
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1340
    .local v2, "child":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v15

    const/16 v16, 0x8

    move/from16 v0, v16

    if-eq v15, v0, :cond_2

    .line 1341
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/FrameLayout$LayoutParams;

    .line 1342
    .local v11, "lp":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 1343
    .local v7, "childWidth":I
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 1345
    .local v4, "childHeight":I
    iget v9, v11, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1346
    .local v9, "gravity":I
    const/4 v15, -0x1

    if-ne v9, v15, :cond_1

    .line 1347
    const/16 v9, 0x31

    .line 1350
    :cond_1
    and-int/lit8 v15, v9, 0x7

    packed-switch v15, :pswitch_data_0

    .line 1361
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPaddingLeft()I

    move-result v15

    sub-int v15, v14, v15

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPaddingRight()I

    move-result v16

    sub-int v15, v15, v16

    iget v0, v11, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    iget v0, v11, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    move/from16 v16, v0

    sub-int v1, v15, v16

    .line 1362
    .local v1, "availWidth":I
    sub-int v8, v1, v7

    .line 1363
    .local v8, "extraWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPaddingLeft()I

    move-result v15

    add-int/2addr v15, v13

    iget v0, v11, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    div-int/lit8 v16, v8, 0x2

    add-int v5, v15, v16

    .line 1366
    .end local v1    # "availWidth":I
    .end local v8    # "extraWidth":I
    .local v5, "childLeft":I
    :goto_2
    and-int/lit8 v15, v9, 0x70

    sparse-switch v15, :sswitch_data_0

    .line 1375
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPaddingTop()I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPaddingTop()I

    move-result v16

    sub-int v16, v12, v16

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPaddingBottom()I

    move-result v17

    sub-int v16, v16, v17

    sub-int v16, v16, v4

    div-int/lit8 v16, v16, 0x2

    add-int v15, v15, v16

    iget v0, v11, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    iget v0, v11, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    move/from16 v16, v0

    sub-int v6, v15, v16

    .line 1380
    .local v6, "childTop":I
    :goto_3
    add-int v15, v5, v7

    add-int v16, v6, v4

    move/from16 v0, v16

    invoke-virtual {v2, v5, v6, v15, v0}, Landroid/view/View;->layout(IIII)V

    .line 1387
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/launcher2/PagedView;->mPageSpacing:I

    add-int/2addr v15, v14

    add-int/2addr v13, v15

    .line 1338
    .end local v4    # "childHeight":I
    .end local v5    # "childLeft":I
    .end local v6    # "childTop":I
    .end local v7    # "childWidth":I
    .end local v9    # "gravity":I
    .end local v11    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 1352
    .restart local v4    # "childHeight":I
    .restart local v7    # "childWidth":I
    .restart local v9    # "gravity":I
    .restart local v11    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPaddingLeft()I

    move-result v15

    add-int/2addr v15, v13

    iget v0, v11, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    move/from16 v16, v0

    add-int v5, v15, v16

    .line 1353
    .restart local v5    # "childLeft":I
    goto :goto_2

    .line 1355
    .end local v5    # "childLeft":I
    :pswitch_2
    add-int v15, v13, v14

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPaddingRight()I

    move-result v16

    sub-int v15, v15, v16

    sub-int/2addr v15, v7

    iget v0, v11, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    move/from16 v16, v0

    sub-int v5, v15, v16

    .line 1357
    .restart local v5    # "childLeft":I
    goto :goto_2

    .line 1369
    :sswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPaddingTop()I

    move-result v15

    iget v0, v11, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v16

    sub-int v6, v15, v16

    .line 1370
    .restart local v6    # "childTop":I
    goto :goto_3

    .line 1372
    .end local v6    # "childTop":I
    :sswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPaddingBottom()I

    move-result v15

    sub-int v15, v12, v15

    sub-int/2addr v15, v4

    iget v0, v11, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    move/from16 v16, v0

    sub-int v6, v15, v16

    .line 1373
    .restart local v6    # "childTop":I
    goto :goto_3

    .line 1391
    .end local v2    # "child":Landroid/view/View;
    .end local v4    # "childHeight":I
    .end local v5    # "childLeft":I
    .end local v6    # "childTop":I
    .end local v7    # "childWidth":I
    .end local v9    # "gravity":I
    .end local v11    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/launcher2/PagedView;->mLaidOutAfterViewTreeDirty:Z

    if-nez v15, :cond_4

    .line 1392
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->setHoverScrollBoundary()V

    .line 1395
    :cond_4
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/launcher2/PagedView;->mLaidOutAfterViewTreeDirty:Z

    .line 1396
    if-nez p1, :cond_5

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/launcher2/PagedView;->mFirstLayout:Z

    if-eqz v15, :cond_7

    :cond_5
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    if-ltz v15, :cond_7

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v16

    move/from16 v0, v16

    if-ge v15, v0, :cond_7

    .line 1397
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/launcher2/PagedView;->jumpToPageInternal(I)V

    .line 1398
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->repositionOpenFolder()V

    .line 1399
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/launcher2/PagedView;->mFirstLayout:Z

    .line 1400
    sget-boolean v15, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v15, :cond_6

    const-string v15, "change_wallpaper"

    sget-object v16, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_7

    .line 1402
    :cond_6
    const/high16 v15, -0x80000000

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayout:I

    .line 1404
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {v15}, Lcom/android/launcher2/PageIndicatorManager;->layoutPageIndicator()V

    .line 1406
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayout:I

    const/high16 v16, -0x80000000

    move/from16 v0, v16

    if-eq v15, v0, :cond_8

    .line 1407
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayoutMustAnim:Z

    if-eqz v15, :cond_9

    .line 1408
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher2/PagedView;->mDelaySnapToPage:Lcom/android/launcher2/PagedView$DelaySnapToPage;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayout:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayoutDur:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayoutDir:I

    move/from16 v18, v0

    invoke-virtual/range {v15 .. v18}, Lcom/android/launcher2/PagedView$DelaySnapToPage;->setup(III)V

    .line 1412
    :goto_4
    const/high16 v15, -0x80000000

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayout:I

    .line 1414
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->setDirtyFlags()V

    goto/16 :goto_0

    .line 1410
    :cond_9
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayout:I

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/launcher2/PagedView;->jumpToPageInternal(I)V

    goto :goto_4

    .line 1350
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 1366
    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x50 -> :sswitch_1
    .end sparse-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 3071
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    iget v1, p0, Lcom/android/launcher2/PagedView;->mLastMotionX:F

    iget v2, p0, Lcom/android/launcher2/PagedView;->mLastMotionY:F

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/PageIndicatorManager;->checkPageIndicatorLongPress(FF)Z

    move-result v0

    return v0
.end method

.method protected onMeasure(II)V
    .locals 25
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 1204
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/launcher2/PagedView;->mIsDataReady:Z

    move/from16 v21, v0

    if-nez v21, :cond_0

    .line 1205
    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 1305
    :goto_0
    return-void

    .line 1209
    :cond_0
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v21

    const/high16 v22, 0x40000000    # 2.0f

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_7

    const/4 v4, 0x1

    .line 1210
    .local v4, "checkMatchParentWidth":Z
    :goto_1
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v21

    const/high16 v22, 0x40000000    # 2.0f

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_8

    const/4 v3, 0x1

    .line 1212
    .local v3, "checkMatchParentHeight":Z
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPaddingTop()I

    move-result v21

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPaddingBottom()I

    move-result v22

    add-int v20, v21, v22

    .line 1213
    .local v20, "verticalPadding":I
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPaddingLeft()I

    move-result v21

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPaddingRight()I

    move-result v22

    add-int v12, v21, v22

    .line 1214
    .local v12, "horizontalPadding":I
    const/16 v16, 0x0

    .line 1215
    .local v16, "maxWidth":I
    const/4 v15, 0x0

    .line 1216
    .local v15, "maxHeight":I
    const/16 v17, 0x0

    .line 1218
    .local v17, "mustMake2ndPass":Z
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v5

    .line 1219
    .local v5, "childCount":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_3
    if-ge v13, v5, :cond_b

    .line 1220
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v18

    .line 1221
    .local v18, "v":Landroid/view/View;
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_6

    .line 1222
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/widget/FrameLayout$LayoutParams;

    .line 1223
    .local v14, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_1

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    move/from16 v21, v0

    and-int/lit8 v21, v21, 0x70

    const/16 v22, 0x30

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_9

    :cond_1
    const/16 v19, 0x1

    .line 1225
    .local v19, "verticalGravityIsTop":Z
    :goto_4
    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    move/from16 v21, v0

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    move/from16 v22, v0

    add-int v8, v21, v22

    .line 1230
    .local v8, "childHorizontalMargins":I
    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    move/from16 v22, v0

    const/16 v23, 0x0

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    move/from16 v24, v0

    if-eqz v19, :cond_a

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getPaddingTop()I

    move-result v21

    :goto_5
    sub-int v21, v24, v21

    move/from16 v0, v23

    move/from16 v1, v21

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v21

    add-int v9, v22, v21

    .line 1233
    .local v9, "childVerticalMargins":I
    add-int v21, v12, v8

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->width:I

    move/from16 v22, v0

    move/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcom/android/launcher2/PagedView;->getChildMeasureSpec(III)I

    move-result v11

    .line 1236
    .local v11, "childWidthMeasureSpec":I
    add-int v21, v20, v9

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->height:I

    move/from16 v22, v0

    move/from16 v0, p2

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcom/android/launcher2/PagedView;->getChildMeasureSpec(III)I

    move-result v7

    .line 1239
    .local v7, "childHeightMeasureSpec":I
    move-object/from16 v0, v18

    invoke-virtual {v0, v11, v7}, Landroid/view/View;->measure(II)V

    .line 1244
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getMeasuredWidth()I

    move-result v21

    add-int v10, v21, v8

    .line 1245
    .local v10, "childWidth":I
    move/from16 v0, v16

    if-le v10, v0, :cond_2

    .line 1246
    move/from16 v16, v10

    .line 1248
    :cond_2
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    add-int v6, v21, v9

    .line 1249
    .local v6, "childHeight":I
    if-le v6, v15, :cond_3

    .line 1250
    move v15, v6

    .line 1252
    :cond_3
    if-eqz v4, :cond_4

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->width:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_5

    :cond_4
    if-eqz v3, :cond_6

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->height:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_6

    .line 1254
    :cond_5
    const/16 v17, 0x1

    .line 1219
    .end local v6    # "childHeight":I
    .end local v7    # "childHeightMeasureSpec":I
    .end local v8    # "childHorizontalMargins":I
    .end local v9    # "childVerticalMargins":I
    .end local v10    # "childWidth":I
    .end local v11    # "childWidthMeasureSpec":I
    .end local v14    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    .end local v19    # "verticalGravityIsTop":Z
    :cond_6
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_3

    .line 1209
    .end local v3    # "checkMatchParentHeight":Z
    .end local v4    # "checkMatchParentWidth":Z
    .end local v5    # "childCount":I
    .end local v12    # "horizontalPadding":I
    .end local v13    # "i":I
    .end local v15    # "maxHeight":I
    .end local v16    # "maxWidth":I
    .end local v17    # "mustMake2ndPass":Z
    .end local v18    # "v":Landroid/view/View;
    .end local v20    # "verticalPadding":I
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 1210
    .restart local v4    # "checkMatchParentWidth":Z
    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 1223
    .restart local v3    # "checkMatchParentHeight":Z
    .restart local v5    # "childCount":I
    .restart local v12    # "horizontalPadding":I
    .restart local v13    # "i":I
    .restart local v14    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v15    # "maxHeight":I
    .restart local v16    # "maxWidth":I
    .restart local v17    # "mustMake2ndPass":Z
    .restart local v18    # "v":Landroid/view/View;
    .restart local v20    # "verticalPadding":I
    :cond_9
    const/16 v19, 0x0

    goto/16 :goto_4

    .line 1230
    .restart local v8    # "childHorizontalMargins":I
    .restart local v19    # "verticalGravityIsTop":Z
    :cond_a
    const/16 v21, 0x0

    goto :goto_5

    .line 1258
    .end local v8    # "childHorizontalMargins":I
    .end local v14    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    .end local v18    # "v":Landroid/view/View;
    .end local v19    # "verticalGravityIsTop":Z
    :cond_b
    add-int v16, v16, v12

    .line 1259
    add-int v15, v15, v20

    .line 1262
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getSuggestedMinimumHeight()I

    move-result v21

    move/from16 v0, v21

    invoke-static {v15, v0}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 1263
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getSuggestedMinimumWidth()I

    move-result v21

    move/from16 v0, v16

    move/from16 v1, v21

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 1265
    move/from16 v0, v16

    move/from16 v1, p1

    invoke-static {v0, v1}, Lcom/android/launcher2/PagedView;->resolveSize(II)I

    move-result v21

    move/from16 v0, p2

    invoke-static {v15, v0}, Lcom/android/launcher2/PagedView;->resolveSize(II)I

    move-result v22

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/PagedView;->setMeasuredDimension(II)V

    .line 1270
    if-eqz v17, :cond_11

    .line 1271
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v21

    sub-int v16, v21, v12

    .line 1272
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v21

    sub-int v15, v21, v20

    .line 1273
    const/4 v13, 0x0

    :goto_6
    if-ge v13, v5, :cond_11

    .line 1274
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v18

    .line 1275
    .restart local v18    # "v":Landroid/view/View;
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_e

    .line 1276
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/widget/FrameLayout$LayoutParams;

    .line 1277
    .restart local v14    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    if-eqz v4, :cond_c

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->width:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_d

    :cond_c
    if-eqz v3, :cond_e

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->height:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_e

    .line 1283
    :cond_d
    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->width:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_f

    .line 1284
    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    move/from16 v21, v0

    sub-int v21, v16, v21

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    move/from16 v22, v0

    sub-int v21, v21, v22

    const/high16 v22, 0x40000000    # 2.0f

    invoke-static/range {v21 .. v22}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    .line 1291
    .restart local v11    # "childWidthMeasureSpec":I
    :goto_7
    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->height:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_10

    .line 1292
    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    move/from16 v21, v0

    sub-int v21, v15, v21

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    move/from16 v22, v0

    sub-int v21, v21, v22

    const/high16 v22, 0x40000000    # 2.0f

    invoke-static/range {v21 .. v22}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 1299
    .restart local v7    # "childHeightMeasureSpec":I
    :goto_8
    move-object/from16 v0, v18

    invoke-virtual {v0, v11, v7}, Landroid/view/View;->measure(II)V

    .line 1273
    .end local v7    # "childHeightMeasureSpec":I
    .end local v11    # "childWidthMeasureSpec":I
    .end local v14    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_e
    add-int/lit8 v13, v13, 0x1

    goto :goto_6

    .line 1287
    .restart local v14    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_f
    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    move/from16 v21, v0

    add-int v21, v21, v12

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    move/from16 v22, v0

    add-int v21, v21, v22

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->width:I

    move/from16 v22, v0

    move/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcom/android/launcher2/PagedView;->getChildMeasureSpec(III)I

    move-result v11

    .restart local v11    # "childWidthMeasureSpec":I
    goto :goto_7

    .line 1295
    :cond_10
    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    move/from16 v21, v0

    add-int v21, v21, v20

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    move/from16 v22, v0

    add-int v21, v21, v22

    iget v0, v14, Landroid/widget/FrameLayout$LayoutParams;->height:I

    move/from16 v22, v0

    move/from16 v0, p2

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcom/android/launcher2/PagedView;->getChildMeasureSpec(III)I

    move-result v7

    .restart local v7    # "childHeightMeasureSpec":I
    goto :goto_8

    .line 1304
    .end local v7    # "childHeightMeasureSpec":I
    .end local v11    # "childWidthMeasureSpec":I
    .end local v14    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    .end local v18    # "v":Landroid/view/View;
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->setMaxScrollX()V

    goto/16 :goto_0
.end method

.method public onMotionListener(Landroid/hardware/motion/MREvent;)V
    .locals 12
    .param p1, "motionEvent"    # Landroid/hardware/motion/MREvent;

    .prologue
    const-wide/16 v10, 0x0

    .line 3688
    iget-boolean v6, p0, Lcom/android/launcher2/PagedView;->mMotionListenerRegistered:Z

    if-nez v6, :cond_1

    .line 3779
    :cond_0
    :goto_0
    return-void

    .line 3690
    :cond_1
    sget-boolean v6, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-nez v6, :cond_0

    .line 3692
    invoke-virtual {p1}, Landroid/hardware/motion/MREvent;->getMotion()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 3694
    :pswitch_0
    invoke-virtual {p1}, Landroid/hardware/motion/MREvent;->getPanningDx()I

    move-result v1

    .line 3695
    .local v1, "dx":I
    invoke-virtual {p1}, Landroid/hardware/motion/MREvent;->getPanningDy()I

    move-result v4

    .line 3702
    .local v4, "dy":I
    iget v6, p0, Lcom/android/launcher2/PagedView;->mCurrentRotation:I

    packed-switch v6, :pswitch_data_1

    .line 3713
    int-to-double v2, v1

    .line 3726
    .local v2, "delta":D
    :goto_1
    sget-boolean v6, Lcom/android/launcher2/PagedView;->DEBUGGABLE:Z

    if-eqz v6, :cond_2

    const-string v6, "MotionEngine"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onMotionListener : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " delta = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " acc = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3729
    :cond_2
    iget-wide v6, p0, Lcom/android/launcher2/PagedView;->mPanningStopDelta:D

    cmpl-double v6, v6, v10

    if-lez v6, :cond_4

    .line 3730
    iget-wide v8, p0, Lcom/android/launcher2/PagedView;->mPanningStopDelta:D

    cmpl-double v6, v2, v10

    if-lez v6, :cond_3

    const/4 v6, -0x1

    :goto_2
    int-to-double v6, v6

    mul-double/2addr v6, v2

    add-double/2addr v6, v8

    iput-wide v6, p0, Lcom/android/launcher2/PagedView;->mPanningStopDelta:D

    goto :goto_0

    .line 3704
    .end local v2    # "delta":D
    :pswitch_1
    neg-int v6, v4

    int-to-double v2, v6

    .line 3705
    .restart local v2    # "delta":D
    goto :goto_1

    .line 3707
    .end local v2    # "delta":D
    :pswitch_2
    neg-int v6, v1

    int-to-double v2, v6

    .line 3708
    .restart local v2    # "delta":D
    goto :goto_1

    .line 3710
    .end local v2    # "delta":D
    :pswitch_3
    int-to-double v2, v4

    .line 3711
    .restart local v2    # "delta":D
    goto :goto_1

    .line 3730
    :cond_3
    const/4 v6, 0x1

    goto :goto_2

    .line 3734
    :cond_4
    iget-wide v6, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    add-double/2addr v6, v2

    iput-wide v6, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    .line 3735
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v0

    .line 3737
    .local v0, "currentPage":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v6

    check-cast v6, Lcom/android/launcher2/Launcher;

    iget-object v6, v6, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v6}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v5

    .line 3738
    .local v5, "workspace":Lcom/android/launcher2/Workspace;
    iget-wide v6, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    iget-wide v8, p0, Lcom/android/launcher2/PagedView;->mPanningBasis:D

    neg-double v8, v8

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_9

    .line 3739
    if-ltz v0, :cond_7

    .line 3741
    iget-object v6, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->isFinished()Z

    move-result v6

    if-nez v6, :cond_5

    .line 3742
    iget-object v6, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->abortAnimation()V

    .line 3743
    iget v0, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    .line 3745
    :cond_5
    iget-wide v6, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    iget-wide v8, p0, Lcom/android/launcher2/PagedView;->mPanningBasis:D

    add-double/2addr v6, v8

    iput-wide v6, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    .line 3747
    if-eqz v5, :cond_6

    invoke-virtual {v5}, Lcom/android/launcher2/Workspace;->getState()Lcom/android/launcher2/Workspace$State;

    move-result-object v6

    sget-object v7, Lcom/android/launcher2/Workspace$State;->EDIT:Lcom/android/launcher2/Workspace$State;

    if-ne v6, v7, :cond_6

    .line 3748
    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p0, v6}, Lcom/android/launcher2/PagedView;->getAdjustedPageIndex(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v6}, Lcom/android/launcher2/CellLayout;->onDragEnter()V

    .line 3750
    :cond_6
    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p0, v6}, Lcom/android/launcher2/PagedView;->snapToPage(I)V

    .line 3752
    sget-boolean v6, Lcom/android/launcher2/PagedView;->DEBUGGABLE:Z

    if-eqz v6, :cond_0

    const-string v6, "MotionEngine"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onMotionListener : panning left from page"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 3754
    :cond_7
    sget-boolean v6, Lcom/android/launcher2/PagedView;->DEBUGGABLE:Z

    if-eqz v6, :cond_8

    const-string v6, "MotionEngine"

    const-string v7, "onMotionListener : accumulated delta is not considered, skip adding"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3755
    :cond_8
    iget-wide v6, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    sub-double/2addr v6, v2

    iput-wide v6, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    goto/16 :goto_0

    .line 3757
    :cond_9
    iget-wide v6, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    iget-wide v8, p0, Lcom/android/launcher2/PagedView;->mPanningBasis:D

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_0

    .line 3758
    iget v6, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-gt v6, v7, :cond_c

    .line 3760
    iget-object v6, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->isFinished()Z

    move-result v6

    if-nez v6, :cond_a

    .line 3761
    iget-object v6, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->abortAnimation()V

    .line 3762
    iget v0, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    .line 3764
    :cond_a
    iget-wide v6, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    iget-wide v8, p0, Lcom/android/launcher2/PagedView;->mPanningBasis:D

    sub-double/2addr v6, v8

    iput-wide v6, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    .line 3766
    if-eqz v5, :cond_b

    invoke-virtual {v5}, Lcom/android/launcher2/Workspace;->getState()Lcom/android/launcher2/Workspace$State;

    move-result-object v6

    sget-object v7, Lcom/android/launcher2/Workspace$State;->EDIT:Lcom/android/launcher2/Workspace$State;

    if-ne v6, v7, :cond_b

    .line 3767
    add-int/lit8 v6, v0, 0x1

    invoke-virtual {p0, v6}, Lcom/android/launcher2/PagedView;->getAdjustedPageIndex(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v6}, Lcom/android/launcher2/CellLayout;->onDragEnter()V

    .line 3769
    :cond_b
    add-int/lit8 v6, v0, 0x1

    invoke-virtual {p0, v6}, Lcom/android/launcher2/PagedView;->snapToPage(I)V

    .line 3771
    sget-boolean v6, Lcom/android/launcher2/PagedView;->DEBUGGABLE:Z

    if-eqz v6, :cond_0

    const-string v6, "MotionEngine"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onMotionListener : panning right from page"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 3773
    :cond_c
    sget-boolean v6, Lcom/android/launcher2/PagedView;->DEBUGGABLE:Z

    if-eqz v6, :cond_d

    const-string v6, "MotionEngine"

    const-string v7, "onMotionListener : accumulated delta is not considered, skip adding"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3774
    :cond_d
    iget-wide v6, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    sub-double/2addr v6, v2

    iput-wide v6, p0, Lcom/android/launcher2/PagedView;->mAccumulatedDelta:D

    goto/16 :goto_0

    .line 3692
    :pswitch_data_0
    .packed-switch 0x3d
        :pswitch_0
    .end packed-switch

    .line 3702
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onPageBeginMoving()V
    .locals 2

    .prologue
    .line 717
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isFastScrolling()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 718
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/launcher2/PagedView;->mIsPageFastMoving:Z

    .line 719
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->updateVisiblePages()I

    .line 720
    const/16 v0, 0x80

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/PagedView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 721
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->setAccessibilityFocusChange(Z)V

    .line 722
    return-void
.end method

.method protected onPageEndMoving()V
    .locals 2

    .prologue
    .line 730
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->setAccessibilityFocusChange(Z)V

    .line 731
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher2/PagedView;->mIsPageFastMoving:Z

    .line 732
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->updateVisiblePages()I

    .line 734
    sget-boolean v0, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v0, :cond_1

    .line 735
    sget-object v0, Lcom/android/launcher2/guide/GuideFragment;->GMode:Ljava/lang/String;

    const-string v1, "navigation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 736
    sget v0, Lcom/android/launcher2/guide/GuideFragment;->currentPageToStopNavigation:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_0

    sget v0, Lcom/android/launcher2/guide/GuideFragment;->currentPageToStopNavigation:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_1

    .line 737
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/launcher2/guide/GuideFragment;->showCompleteDialog(Landroid/app/Activity;)V

    .line 742
    :cond_1
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 3610
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 2
    .param p1, "direction"    # I
    .param p2, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 1632
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getComingPage()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    .line 1633
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1634
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v1

    .line 1636
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 2822
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->mIsDataReady:Z

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/android/launcher2/PagedView$SavedState;

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 2823
    check-cast v1, Lcom/android/launcher2/PagedView$SavedState;

    invoke-virtual {v1}, Lcom/android/launcher2/PagedView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 2824
    check-cast p1, Lcom/android/launcher2/PagedView$SavedState;

    .end local p1    # "state":Landroid/os/Parcelable;
    iput-object p1, p0, Lcom/android/launcher2/PagedView;->mPendingSavedState:Lcom/android/launcher2/PagedView$SavedState;

    .line 2832
    :goto_0
    return-void

    .line 2825
    .restart local p1    # "state":Landroid/os/Parcelable;
    :cond_0
    instance-of v1, p1, Lcom/android/launcher2/PagedView$SavedState;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 2826
    check-cast v0, Lcom/android/launcher2/PagedView$SavedState;

    .line 2827
    .local v0, "ss":Lcom/android/launcher2/PagedView$SavedState;
    invoke-virtual {v0}, Lcom/android/launcher2/PagedView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 2828
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->restoreCurrentPageFromState(Lcom/android/launcher2/PagedView$SavedState;)V

    goto :goto_0

    .line 2830
    .end local v0    # "ss":Lcom/android/launcher2/PagedView$SavedState;
    :cond_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 3606
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 2814
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 2815
    .local v1, "superState":Landroid/os/Parcelable;
    new-instance v0, Lcom/android/launcher2/PagedView$SavedState;

    invoke-direct {v0, v1}, Lcom/android/launcher2/PagedView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 2816
    .local v0, "ss":Lcom/android/launcher2/PagedView$SavedState;
    iget v2, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    iput v2, v0, Lcom/android/launcher2/PagedView$SavedState;->currentPage:I

    .line 2817
    return-object v0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 3613
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {v0}, Lcom/android/launcher2/PageIndicatorManager;->cancelFastScroll()V

    .line 3614
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 36
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 2148
    sget-boolean v31, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v31, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v31

    const/16 v32, 0x2

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_0

    .line 2149
    const/16 v31, 0x0

    .line 2379
    :goto_0
    return v31

    .line 2150
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/launcher2/PagedView;->motionTrackingIsCurrent:Z

    move/from16 v31, v0

    if-nez v31, :cond_1

    .line 2151
    invoke-direct/range {p0 .. p1}, Lcom/android/launcher2/PagedView;->updateMotionTracking(Landroid/view/MotionEvent;)V

    .line 2153
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v31

    if-gtz v31, :cond_2

    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v31

    goto :goto_0

    .line 2155
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PageIndicatorManager;->handleEvent(Landroid/view/MotionEvent;)Z

    .line 2157
    invoke-direct/range {p0 .. p1}, Lcom/android/launcher2/PagedView;->acquireVelocityTrackerAndAddMovement(Landroid/view/MotionEvent;)V

    .line 2159
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    .line 2161
    .local v7, "action":I
    and-int/lit16 v0, v7, 0xff

    move/from16 v31, v0

    packed-switch v31, :pswitch_data_0

    .line 2379
    :cond_3
    :goto_1
    :pswitch_0
    const/16 v31, 0x1

    goto :goto_0

    .line 2167
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/Scroller;->isFinished()Z

    move-result v31

    if-nez v31, :cond_4

    .line 2168
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->abortScroll()V

    .line 2170
    :cond_4
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTotalMotionX:F

    .line 2171
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTotalPredictionX:F

    .line 2172
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/launcher2/PagedView;->isShowCamera:Z

    .line 2174
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchState:I

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_3

    .line 2175
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->pageBeginMoving()V

    goto :goto_1

    .line 2180
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/android/launcher2/PageIndicatorManager;->isFastScrolling()Z

    move-result v31

    if-nez v31, :cond_3

    .line 2181
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v31

    check-cast v31, Lcom/android/launcher2/Launcher;

    invoke-virtual/range {v31 .. v31}, Lcom/android/launcher2/Launcher;->isAddToScreenDialogShowing()Z

    move-result v31

    if-nez v31, :cond_3

    .line 2182
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchState:I

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_a

    .line 2184
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v30

    .line 2187
    .local v30, "x":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v31

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v31

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_6

    .line 2188
    const/16 v20, 0x0

    .local v20, "lastXVelocity":F
    const/16 v24, 0x0

    .line 2189
    .local v24, "predictionDelta":F
    const-wide/16 v18, 0x0

    .line 2192
    .local v18, "lastTime":J
    const v5, 0xf4240

    .line 2193
    .local v5, "NANOS_TO_MS":I
    const/16 v4, 0xc8

    .line 2195
    .local v4, "MAX_PREDICTION_X":I
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v31

    move-object/from16 v0, v31

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    move/from16 v31, v0

    const/16 v32, 0x258

    move/from16 v0, v31

    move/from16 v1, v32

    if-ge v0, v1, :cond_7

    .line 2196
    const/high16 v25, 0x41400000    # 12.0f

    .line 2199
    .local v25, "predictionTime":F
    :goto_2
    const/high16 v6, 0x3f800000    # 1.0f

    .line 2200
    .local v6, "accelerationRatio":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v31, v0

    const/16 v32, 0x1

    invoke-virtual/range {v31 .. v32}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 2201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v12

    .line 2202
    .local v12, "curXVelocity":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v13

    .line 2203
    .local v13, "curYVelocity":F
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v32

    const-wide/32 v34, 0xf4240

    div-long v10, v32, v34

    .line 2204
    .local v10, "curTime":J
    sub-long v14, v10, v18

    .line 2205
    .local v14, "deltaTime":J
    sub-float v31, v12, v20

    long-to-float v0, v14

    move/from16 v32, v0

    div-float v9, v31, v32

    .line 2208
    .local v9, "curXAcceleration":F
    move/from16 v20, v12

    .line 2209
    move/from16 v21, v13

    .line 2210
    .local v21, "lastYVelocity":F
    move-wide/from16 v18, v10

    .line 2211
    const/high16 v31, 0x3f800000    # 1.0f

    mul-float v31, v31, v9

    add-float v31, v31, v12

    mul-float v24, v31, v25

    .line 2215
    add-float v30, v30, v24

    .line 2216
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTotalPredictionX:F

    move/from16 v31, v0

    add-float v31, v31, v24

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTotalPredictionX:F

    .line 2219
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v31

    move-object/from16 v0, v31

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    cmpl-float v31, v30, v31

    if-lez v31, :cond_5

    .line 2220
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v31

    move-object/from16 v0, v31

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v30, v0

    .line 2221
    :cond_5
    const/16 v31, 0x0

    cmpg-float v31, v30, v31

    if-gez v31, :cond_6

    .line 2222
    const/16 v30, 0x0

    .line 2226
    .end local v4    # "MAX_PREDICTION_X":I
    .end local v5    # "NANOS_TO_MS":I
    .end local v6    # "accelerationRatio":F
    .end local v9    # "curXAcceleration":F
    .end local v10    # "curTime":J
    .end local v12    # "curXVelocity":F
    .end local v13    # "curYVelocity":F
    .end local v14    # "deltaTime":J
    .end local v18    # "lastTime":J
    .end local v20    # "lastXVelocity":F
    .end local v21    # "lastYVelocity":F
    .end local v24    # "predictionDelta":F
    .end local v25    # "predictionTime":F
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mLastMotionX:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mLastMotionXRemainder:F

    move/from16 v32, v0

    add-float v31, v31, v32

    sub-float v16, v31, v30

    .line 2227
    .local v16, "deltaX":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTotalMotionX:F

    move/from16 v31, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v32

    add-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTotalMotionX:F

    .line 2232
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v31

    const/high16 v32, 0x3f800000    # 1.0f

    cmpl-float v31, v31, v32

    if-ltz v31, :cond_9

    .line 2233
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchX:F

    move/from16 v31, v0

    add-float v31, v31, v16

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTouchX:F

    .line 2234
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v32

    move-wide/from16 v0, v32

    long-to-float v0, v0

    move/from16 v31, v0

    const v32, 0x4e6e6b28    # 1.0E9f

    div-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mSmoothingTime:F

    .line 2235
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/launcher2/PagedView;->mDeferScrollUpdate:Z

    move/from16 v31, v0

    if-nez v31, :cond_8

    .line 2236
    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v31, v0

    const/16 v32, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/PagedView;->scrollBy(II)V

    .line 2241
    :goto_3
    move/from16 v0, v30

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mLastMotionX:F

    .line 2242
    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    sub-float v31, v16, v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mLastMotionXRemainder:F

    goto/16 :goto_1

    .line 2198
    .end local v16    # "deltaX":F
    .restart local v4    # "MAX_PREDICTION_X":I
    .restart local v5    # "NANOS_TO_MS":I
    .restart local v18    # "lastTime":J
    .restart local v20    # "lastXVelocity":F
    .restart local v24    # "predictionDelta":F
    :cond_7
    const/high16 v25, 0x41500000    # 13.0f

    .restart local v25    # "predictionTime":F
    goto/16 :goto_2

    .line 2239
    .end local v4    # "MAX_PREDICTION_X":I
    .end local v5    # "NANOS_TO_MS":I
    .end local v18    # "lastTime":J
    .end local v20    # "lastXVelocity":F
    .end local v24    # "predictionDelta":F
    .end local v25    # "predictionTime":F
    .restart local v16    # "deltaX":F
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    goto :goto_3

    .line 2244
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->awakenScrollBars()Z

    goto/16 :goto_1

    .line 2247
    .end local v16    # "deltaX":F
    .end local v30    # "x":F
    :cond_a
    invoke-virtual/range {p0 .. p1}, Lcom/android/launcher2/PagedView;->determineScrollingStart(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 2252
    :pswitch_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTouchUpPointX:F

    .line 2253
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchState:I

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchState:I

    move/from16 v31, v0

    const/16 v32, 0x4

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_16

    .line 2254
    :cond_b
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v30

    .line 2255
    .restart local v30    # "x":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v28, v0

    .line 2256
    .local v28, "velocityTracker":Landroid/view/VelocityTracker;
    const/16 v31, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mMaximumVelocity:I

    move/from16 v32, v0

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v32, v0

    move-object/from16 v0, v28

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 2257
    invoke-virtual/range {v28 .. v28}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v31

    move/from16 v0, v31

    float-to-int v0, v0

    move/from16 v27, v0

    .line 2258
    .local v27, "tempVelocityX":I
    if-nez v27, :cond_c

    sget-object v31, Lcom/sec/dtl/launcher/Talk;->INSTANCE:Lcom/sec/dtl/launcher/Talk;

    invoke-virtual/range {v31 .. v31}, Lcom/sec/dtl/launcher/Talk;->isTalkbackEnabled()Z

    move-result v31

    if-eqz v31, :cond_c

    .line 2259
    const/16 v31, 0x1

    move-object/from16 v0, v28

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v31

    move/from16 v0, v31

    float-to-int v0, v0

    move/from16 v27, v0

    .line 2260
    :cond_c
    move/from16 v29, v27

    .line 2262
    .local v29, "velocityX":I
    const/16 v26, 0x0

    .line 2263
    .local v26, "runEasyOneHand":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTotalMotionX:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mLastMotionX:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mLastMotionXRemainder:F

    move/from16 v33, v0

    add-float v32, v32, v33

    sub-float v32, v32, v30

    invoke-static/range {v32 .. v32}, Ljava/lang/Math;->abs(F)F

    move-result v32

    add-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTotalMotionX:F

    .line 2264
    const/16 v17, 0x1

    .line 2265
    .local v17, "dir":I
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->isEasyOneHandTriggerGesture()Z

    move-result v31

    if-eqz v31, :cond_d

    .line 2266
    const/16 v26, 0x1

    .line 2269
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTotalMotionX:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->MIN_LENGTH_FOR_FLING:I

    move/from16 v32, v0

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v32, v0

    cmpl-float v31, v31, v32

    if-lez v31, :cond_14

    invoke-static/range {v29 .. v29}, Ljava/lang/Math;->abs(I)I

    move-result v31

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mSnapVelocity:I

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    if-le v0, v1, :cond_14

    if-nez v26, :cond_14

    .line 2272
    if-lez v29, :cond_12

    .line 2273
    const/16 v17, 0x2

    .line 2274
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mDownMotionX:F

    move/from16 v31, v0

    cmpg-float v31, v30, v31

    if-gez v31, :cond_10

    .line 2276
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    move/from16 v23, v0

    .line 2309
    .local v23, "nextPage":I
    :goto_4
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PagedView;->getAdjustedPageIndex(I)I

    move-result v8

    .line 2310
    .local v8, "adjustedPage":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    move/from16 v31, v0

    move/from16 v0, v31

    if-eq v8, v0, :cond_15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mNextPage:I

    move/from16 v31, v0

    move/from16 v0, v31

    if-eq v8, v0, :cond_15

    .line 2311
    sget-boolean v31, Lcom/android/launcher2/Launcher;->is_TB:Z

    if-eqz v31, :cond_e

    const/16 v31, 0x2

    move/from16 v0, v17

    move/from16 v1, v31

    if-ne v0, v1, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchUpPointX:F

    move/from16 v31, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPageTotWidth()I

    move-result v32

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v32, v0

    cmpl-float v31, v31, v32

    if-lez v31, :cond_e

    .line 2312
    const/16 v17, 0x1

    .line 2314
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->PAGE_SNAP_ANIMATION_DURATION:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v31

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/launcher2/PagedView;->snapToPage(III)V

    .line 2341
    .end local v8    # "adjustedPage":I
    .end local v17    # "dir":I
    .end local v23    # "nextPage":I
    .end local v26    # "runEasyOneHand":Z
    .end local v27    # "tempVelocityX":I
    .end local v28    # "velocityTracker":Landroid/view/VelocityTracker;
    .end local v29    # "velocityX":I
    .end local v30    # "x":F
    :goto_5
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTouchState:I

    .line 2342
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->releaseVelocityTracker()V

    .line 2343
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/launcher2/PagedView;->motionTrackingIsCurrent:Z

    .line 2344
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/launcher2/PagedView;->isShowCamera:Z

    .line 2345
    sget-object v31, Lcom/android/launcher2/PagedView;->chipset:Ljava/lang/String;

    const-string v32, "8930"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v31

    if-nez v31, :cond_f

    sget-object v31, Lcom/android/launcher2/PagedView;->chipset:Ljava/lang/String;

    const-string v32, "piranha"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v31

    if-eqz v31, :cond_1b

    .line 2346
    :cond_f
    sget-object v31, Lcom/android/launcher2/PagedView;->cpuMaxBooster:Landroid/os/DVFSHelper;

    invoke-virtual/range {v31 .. v31}, Landroid/os/DVFSHelper;->release()V

    .line 2347
    sget-object v31, Lcom/android/launcher2/PagedView;->gpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual/range {v31 .. v31}, Landroid/os/DVFSHelper;->getSupportedGPUFrequency()[I

    move-result-object v31

    if-eqz v31, :cond_3

    .line 2348
    sget-object v31, Lcom/android/launcher2/PagedView;->gpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual/range {v31 .. v31}, Landroid/os/DVFSHelper;->release()V

    goto/16 :goto_1

    .line 2280
    .restart local v17    # "dir":I
    .restart local v26    # "runEasyOneHand":Z
    .restart local v27    # "tempVelocityX":I
    .restart local v28    # "velocityTracker":Landroid/view/VelocityTracker;
    .restart local v29    # "velocityX":I
    .restart local v30    # "x":F
    :cond_10
    sget-boolean v31, Lcom/android/launcher2/Launcher;->sIsHeadlinesAppEnable:Z

    if-eqz v31, :cond_11

    .line 2281
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    move/from16 v31, v0

    if-nez v31, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v31

    check-cast v31, Lcom/android/launcher2/Launcher;

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/android/launcher2/HomeView;->isVisible()Z

    move-result v31

    if-eqz v31, :cond_11

    .line 2282
    new-instance v22, Landroid/os/Handler;

    invoke-direct/range {v22 .. v22}, Landroid/os/Handler;-><init>()V

    .line 2283
    .local v22, "mHandler":Landroid/os/Handler;
    new-instance v31, Lcom/android/launcher2/PagedView$4;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/launcher2/PagedView$4;-><init>(Lcom/android/launcher2/PagedView;)V

    const-wide/16 v32, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v31

    move-wide/from16 v2, v32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2289
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    move/from16 v31, v0

    .line 2292
    .end local v22    # "mHandler":Landroid/os/Handler;
    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    move/from16 v31, v0

    add-int/lit8 v23, v31, -0x1

    .restart local v23    # "nextPage":I
    goto/16 :goto_4

    .line 2295
    .end local v23    # "nextPage":I
    :cond_12
    const/16 v17, 0x3

    .line 2296
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mDownMotionX:F

    move/from16 v31, v0

    cmpl-float v31, v30, v31

    if-lez v31, :cond_13

    .line 2298
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    move/from16 v23, v0

    .restart local v23    # "nextPage":I
    goto/16 :goto_4

    .line 2302
    .end local v23    # "nextPage":I
    :cond_13
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    move/from16 v31, v0

    add-int/lit8 v23, v31, 0x1

    .restart local v23    # "nextPage":I
    goto/16 :goto_4

    .line 2306
    .end local v23    # "nextPage":I
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->getPageNearestToCenterOfScreen()I

    move-result v23

    .restart local v23    # "nextPage":I
    goto/16 :goto_4

    .line 2316
    .restart local v8    # "adjustedPage":I
    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->snapToDestination()V

    goto/16 :goto_5

    .line 2318
    .end local v8    # "adjustedPage":I
    .end local v17    # "dir":I
    .end local v23    # "nextPage":I
    .end local v26    # "runEasyOneHand":Z
    .end local v27    # "tempVelocityX":I
    .end local v28    # "velocityTracker":Landroid/view/VelocityTracker;
    .end local v29    # "velocityX":I
    .end local v30    # "x":F
    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchState:I

    move/from16 v31, v0

    const/16 v32, 0x2

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_18

    .line 2322
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    move/from16 v31, v0

    add-int/lit8 v31, v31, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PagedView;->getAdjustedPageIndex(I)I

    move-result v23

    .line 2323
    .restart local v23    # "nextPage":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    move/from16 v31, v0

    move/from16 v0, v23

    move/from16 v1, v31

    if-eq v0, v1, :cond_17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mNextPage:I

    move/from16 v31, v0

    move/from16 v0, v23

    move/from16 v1, v31

    if-eq v0, v1, :cond_17

    .line 2324
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->PAGE_SNAP_ANIMATION_DURATION:I

    move/from16 v31, v0

    const/16 v32, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v31

    move/from16 v3, v32

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/launcher2/PagedView;->snapToPage(III)V

    goto/16 :goto_5

    .line 2326
    :cond_17
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->snapToDestination()V

    goto/16 :goto_5

    .line 2328
    .end local v23    # "nextPage":I
    :cond_18
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchState:I

    move/from16 v31, v0

    const/16 v32, 0x3

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_1a

    .line 2332
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    move/from16 v31, v0

    add-int/lit8 v31, v31, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PagedView;->getAdjustedPageIndex(I)I

    move-result v23

    .line 2333
    .restart local v23    # "nextPage":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    move/from16 v31, v0

    move/from16 v0, v23

    move/from16 v1, v31

    if-eq v0, v1, :cond_19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mNextPage:I

    move/from16 v31, v0

    move/from16 v0, v23

    move/from16 v1, v31

    if-eq v0, v1, :cond_19

    .line 2334
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->PAGE_SNAP_ANIMATION_DURATION:I

    move/from16 v31, v0

    const/16 v32, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v31

    move/from16 v3, v32

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/launcher2/PagedView;->snapToPage(III)V

    goto/16 :goto_5

    .line 2336
    :cond_19
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->snapToDestination()V

    goto/16 :goto_5

    .line 2339
    .end local v23    # "nextPage":I
    :cond_1a
    invoke-virtual/range {p0 .. p1}, Lcom/android/launcher2/PagedView;->onUnhandledTap(Landroid/view/MotionEvent;)V

    goto/16 :goto_5

    .line 2350
    :cond_1b
    sget-object v31, Lcom/android/launcher2/PagedView;->cpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual/range {v31 .. v31}, Landroid/os/DVFSHelper;->release()V

    .line 2351
    sget-object v31, Lcom/android/launcher2/PagedView;->busBooster:Landroid/os/DVFSHelper;

    invoke-virtual/range {v31 .. v31}, Landroid/os/DVFSHelper;->release()V

    .line 2353
    move-object/from16 v0, p0

    instance-of v0, v0, Lcom/android/launcher2/Workspace;

    move/from16 v31, v0

    if-eqz v31, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher2/PagedView;->mCurrentTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

    move-object/from16 v31, v0

    sget-object v32, Lcom/android/launcher2/PagedView$TransitionEffect;->SPIRAL:Lcom/android/launcher2/PagedView$TransitionEffect;

    invoke-virtual/range {v31 .. v32}, Lcom/android/launcher2/PagedView$TransitionEffect;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_3

    sget-object v31, Lcom/android/launcher2/PagedView;->gpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual/range {v31 .. v31}, Landroid/os/DVFSHelper;->getSupportedGPUFrequency()[I

    move-result-object v31

    if-eqz v31, :cond_3

    .line 2356
    sget-object v31, Lcom/android/launcher2/PagedView;->gpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual/range {v31 .. v31}, Landroid/os/DVFSHelper;->release()V

    goto/16 :goto_1

    .line 2362
    :pswitch_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchState:I

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_1c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/launcher2/PagedView;->mTouchState:I

    move/from16 v31, v0

    const/16 v32, 0x4

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_1d

    .line 2363
    :cond_1c
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->snapToDestination()V

    .line 2365
    :cond_1d
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/PagedView;->mTouchState:I

    .line 2366
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/PagedView;->releaseVelocityTracker()V

    .line 2367
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/launcher2/PagedView;->motionTrackingIsCurrent:Z

    goto/16 :goto_1

    .line 2371
    :pswitch_5
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/launcher2/PagedView;->motionTrackingIsCurrent:Z

    goto/16 :goto_1

    .line 2375
    :pswitch_6
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/launcher2/PagedView;->motionTrackingIsCurrent:Z

    goto/16 :goto_1

    .line 2161
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected onUnhandledTap(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 2478
    return-void
.end method

.method protected onViewAddedInPagedView(Landroid/view/View;)V
    .locals 4
    .param p1, "child"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 3825
    const/high16 v1, -0x80000000

    iput v1, p0, Lcom/android/launcher2/PagedView;->mLastScrollX:I

    .line 3826
    iput-boolean v3, p0, Lcom/android/launcher2/PagedView;->mLaidOutAfterViewTreeDirty:Z

    .line 3827
    instance-of v1, p1, Lcom/android/launcher2/Page;

    if-eqz v1, :cond_1

    .line 3828
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 3829
    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v1

    iget-object v2, p0, Lcom/android/launcher2/PagedView;->mHardwarePages:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 3830
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mHardwarePages:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 3832
    :cond_0
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mHardwarePages:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3833
    check-cast p1, Lcom/android/launcher2/Page;

    .end local p1    # "child":Landroid/view/View;
    invoke-interface {p1}, Lcom/android/launcher2/Page;->enableHardwareLayers()V

    .line 3836
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method public onViewRemovedInPagedView(Landroid/view/View;)V
    .locals 1
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 3840
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/android/launcher2/PagedView;->mLastScrollX:I

    .line 3841
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->mLaidOutAfterViewTreeDirty:Z

    .line 3842
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->updateVisiblePages()I

    .line 3843
    return-void
.end method

.method public openCamera()V
    .locals 2

    .prologue
    .line 2399
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    sget-object v1, Lcom/android/launcher2/PagedView;->CAMERA_INTENT:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->startActivityWithTransition(Landroid/content/Intent;)V

    .line 2400
    return-void
.end method

.method public openHeadlines(Z)V
    .locals 2
    .param p1, "moveFromFirstPage"    # Z

    .prologue
    .line 2403
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    sget-object v1, Lcom/android/launcher2/PagedView;->HEADLINES_INTENT:Landroid/content/Intent;

    invoke-virtual {v0, v1, p1}, Lcom/android/launcher2/Launcher;->startActivityWithTransitionForHeadlines(Landroid/content/Intent;Z)V

    .line 2404
    return-void
.end method

.method protected overScroll(F)V
    .locals 0
    .param p1, "amount"    # F

    .prologue
    .line 2130
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->dampedOverScroll(F)V

    .line 2131
    return-void
.end method

.method protected pageBeginMoving()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 598
    iget-boolean v3, p0, Lcom/android/launcher2/PagedView;->mIsPageMoving:Z

    if-nez v3, :cond_3

    .line 599
    iput-boolean v4, p0, Lcom/android/launcher2/PagedView;->mIsPageMoving:Z

    .line 600
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->onPageBeginMoving()V

    .line 601
    sget-object v3, Lcom/android/launcher2/PagedView$LayerOptions;->DEFAULT:Lcom/android/launcher2/PagedView$LayerOptions;

    invoke-virtual {p0, v3}, Lcom/android/launcher2/PagedView;->updateChildrenLayersEnabled(Lcom/android/launcher2/PagedView$LayerOptions;)V

    .line 602
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher2/Launcher;->getAnimationLayer()Lcom/android/launcher2/AnimationLayer;

    move-result-object v1

    .line 603
    .local v1, "l":Lcom/android/launcher2/AnimationLayer;
    invoke-virtual {v1, v5}, Lcom/android/launcher2/AnimationLayer;->cancelAnimationsByGroup(I)V

    .line 604
    iget-boolean v3, p0, Lcom/android/launcher2/PagedView;->USE_SET_INTEGRATOR_HAPTIC:Z

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher2/Launcher;->isHapticFeedbackExtraOn()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 605
    const/16 v3, 0x558c

    invoke-virtual {p0, v3, v4}, Lcom/android/launcher2/PagedView;->performHapticFeedback(II)Z

    .line 612
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 628
    iget-object v3, p0, Lcom/android/launcher2/PagedView;->mDefaultTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

    sget-object v4, Lcom/android/launcher2/PagedView$TransitionEffect;->CARD:Lcom/android/launcher2/PagedView$TransitionEffect;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/PagedView$TransitionEffect;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 629
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->isPageInclusive(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 630
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    .line 631
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 612
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 636
    :cond_2
    iget-object v3, p0, Lcom/android/launcher2/PagedView;->mDefaultTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

    sget-object v4, Lcom/android/launcher2/PagedView$TransitionEffect;->CARD:Lcom/android/launcher2/PagedView$TransitionEffect;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/PagedView$TransitionEffect;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 637
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->syncViewVisibility()V

    .line 640
    .end local v0    # "i":I
    .end local v1    # "l":Lcom/android/launcher2/AnimationLayer;
    :cond_3
    return-void
.end method

.method protected pageEndMoving()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 643
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->mIsPageMoving:Z

    if-eqz v0, :cond_2

    .line 644
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->mIsPageMoving:Z

    .line 645
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->onPageEndMoving()V

    .line 646
    sget-object v0, Lcom/android/launcher2/PagedView$LayerOptions;->DEFAULT:Lcom/android/launcher2/PagedView$LayerOptions;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->updateChildrenLayersEnabled(Lcom/android/launcher2/PagedView$LayerOptions;)V

    .line 648
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->USE_SET_INTEGRATOR_HAPTIC:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->isHapticFeedbackExtraOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 649
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 651
    :cond_0
    const/16 v0, 0x55a0

    invoke-virtual {p0, v0, v4}, Lcom/android/launcher2/PagedView;->performHapticFeedback(II)Z

    .line 658
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->mScrollByHover:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

    if-eqz v0, :cond_2

    .line 659
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v4, v2, v3}, Lcom/android/launcher2/PagedView$HoverScrollHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 662
    :cond_2
    return-void

    .line 655
    :cond_3
    const/16 v0, 0x5596

    invoke-virtual {p0, v0, v4}, Lcom/android/launcher2/PagedView;->performHapticFeedback(II)Z

    goto :goto_0
.end method

.method public removeHoverScrollHandler()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 4065
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->mScrollByHover:Z

    .line 4066
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

    if-eqz v0, :cond_0

    .line 4068
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PagedView$HoverScrollHandler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4069
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PagedView$HoverScrollHandler;->removeMessages(I)V

    .line 4072
    :cond_0
    return-void
.end method

.method protected repositionOpenFolder()V
    .locals 0

    .prologue
    .line 3864
    return-void
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "focused"    # Landroid/view/View;

    .prologue
    .line 2484
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 2485
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->indexToPage(I)I

    move-result v0

    .line 2486
    .local v0, "page":I
    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2487
    iget v1, p0, Lcom/android/launcher2/PagedView;->mLastFocusDir:I

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/PagedView;->snapToPageWithDir(II)V

    .line 2489
    :cond_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/android/launcher2/PagedView;->mLastFocusDir:I

    .line 2490
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "rectangle"    # Landroid/graphics/Rect;
    .param p3, "immediate"    # Z

    .prologue
    .line 1622
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->indexToPage(I)I

    move-result v0

    .line 1623
    .local v0, "page":I
    iget v1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1624
    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->snapToPage(I)V

    .line 1625
    const/4 v1, 0x1

    .line 1627
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 2
    .param p1, "disallowIntercept"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1704
    if-eqz p1, :cond_1

    .line 1707
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    .line 1708
    iget v0, p0, Lcom/android/launcher2/PagedView;->mTouchState:I

    if-eqz v0, :cond_0

    .line 1709
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->snapToDestination()V

    .line 1710
    iput v1, p0, Lcom/android/launcher2/PagedView;->mTouchState:I

    .line 1712
    :cond_0
    iput-boolean v1, p0, Lcom/android/launcher2/PagedView;->motionTrackingIsCurrent:Z

    .line 1714
    :cond_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 1715
    return-void
.end method

.method protected restoreCurrentPageFromState(Lcom/android/launcher2/PagedView$SavedState;)V
    .locals 1
    .param p1, "state"    # Lcom/android/launcher2/PagedView$SavedState;

    .prologue
    .line 2835
    iget v0, p1, Lcom/android/launcher2/PagedView$SavedState;->currentPage:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->setCurrentPage(I)V

    .line 2836
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->loadAssociatedPages(I)V

    .line 2837
    return-void
.end method

.method final screenScrolled(I)V
    .locals 1
    .param p1, "screenCenter"    # I

    .prologue
    .line 1429
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->mPageTransformsDirty:Z

    .line 1430
    return-void
.end method

.method public scrollBy(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 768
    iget v0, p0, Lcom/android/launcher2/PagedView;->mUnboundedScrollX:I

    add-int/2addr v0, p1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/PagedView;->scrollTo(II)V

    .line 769
    return-void
.end method

.method public scrollByHover(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v12, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 3239
    sget-boolean v8, Lcom/android/launcher2/Launcher;->isHelpAppRunning:Z

    if-eqz v8, :cond_0

    move v8, v9

    .line 3305
    :goto_0
    return v8

    .line 3241
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 3242
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    .line 3243
    .local v6, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    .line 3244
    .local v7, "y":F
    invoke-virtual {p0, v6}, Lcom/android/launcher2/PagedView;->isHoveringAreaX(F)I

    move-result v1

    .line 3246
    .local v1, "dir":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v8

    const-string v11, "com.sec.feature.hovering_ui"

    invoke-static {v8, v11}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    move v8, v9

    .line 3247
    goto :goto_0

    .line 3249
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v11, "pen_hovering"

    invoke-static {v8, v11, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 3250
    .local v2, "isHoveringOn":I
    if-nez v2, :cond_2

    move v8, v9

    goto :goto_0

    .line 3252
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v11, "pen_hovering_list_scroll"

    invoke-static {v8, v11, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 3253
    .local v3, "isHoveringScrollOn":I
    if-nez v3, :cond_3

    move v8, v9

    goto :goto_0

    .line 3255
    :cond_3
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v8

    check-cast v8, Lcom/android/launcher2/Launcher;

    iget-object v8, v8, Lcom/android/launcher2/Launcher;->mHomeView:Lcom/android/launcher2/HomeView;

    invoke-virtual {v8}, Lcom/android/launcher2/HomeView;->getWorkspace()Lcom/android/launcher2/Workspace;

    move-result-object v5

    .line 3256
    .local v5, "workspace":Lcom/android/launcher2/Workspace;
    if-nez v5, :cond_4

    move v8, v9

    .line 3257
    goto :goto_0

    .line 3259
    :cond_4
    invoke-virtual {v5}, Lcom/android/launcher2/Workspace;->getState()Lcom/android/launcher2/Workspace$State;

    move-result-object v8

    sget-object v11, Lcom/android/launcher2/Workspace$State;->RESIZE:Lcom/android/launcher2/Workspace$State;

    if-eq v8, v11, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v8

    if-ne v8, v12, :cond_8

    .line 3260
    :cond_5
    iget-boolean v8, p0, Lcom/android/launcher2/PagedView;->mScrollByHover:Z

    if-eqz v8, :cond_6

    .line 3261
    const-string v8, "Launcher.PagedView"

    const-string v11, "scrollByHover Workspace.State.RESIZE"

    invoke-static {v8, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3262
    invoke-virtual {p0, v9}, Lcom/android/launcher2/PagedView;->setHoveringSpenIcon(I)V

    .line 3263
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

    invoke-virtual {v8, v10}, Lcom/android/launcher2/PagedView$HoverScrollHandler;->hasMessages(I)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 3264
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

    invoke-virtual {v8, v10}, Lcom/android/launcher2/PagedView$HoverScrollHandler;->removeMessages(I)V

    .line 3268
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v8

    if-ne v8, v12, :cond_7

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isPageMoving()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->snapToDestination()V

    :cond_7
    move v8, v9

    .line 3269
    goto/16 :goto_0

    .line 3272
    :cond_8
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

    if-nez v8, :cond_9

    .line 3273
    new-instance v8, Lcom/android/launcher2/PagedView$HoverScrollHandler;

    const/4 v11, 0x0

    invoke-direct {v8, p0, v11}, Lcom/android/launcher2/PagedView$HoverScrollHandler;-><init>(Lcom/android/launcher2/PagedView;Lcom/android/launcher2/PagedView$1;)V

    iput-object v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

    .line 3275
    :cond_9
    and-int/lit16 v8, v0, 0xff

    packed-switch v8, :pswitch_data_0

    .line 3303
    :cond_a
    :goto_1
    :pswitch_0
    iget-boolean v8, p0, Lcom/android/launcher2/PagedView;->mScrollByHover:Z

    if-nez v8, :cond_b

    invoke-virtual {p0, v9}, Lcom/android/launcher2/PagedView;->setHoveringSpenIcon(I)V

    :cond_b
    move v8, v10

    .line 3305
    goto/16 :goto_0

    .line 3279
    :pswitch_1
    if-eqz v1, :cond_c

    invoke-virtual {p0, v7}, Lcom/android/launcher2/PagedView;->isHoveringAreaY(F)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 3280
    iget-boolean v8, p0, Lcom/android/launcher2/PagedView;->mScrollByHover:Z

    if-nez v8, :cond_a

    .line 3281
    iput-boolean v10, p0, Lcom/android/launcher2/PagedView;->mScrollByHover:Z

    .line 3282
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->setHoveringSpenIcon(I)V

    .line 3284
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v4

    .line 3285
    .local v4, "msg":Landroid/os/Message;
    iput v10, v4, Landroid/os/Message;->what:I

    .line 3286
    iput v1, p0, Lcom/android/launcher2/PagedView;->mHoverScrollDir:I

    .line 3287
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

    const-wide/16 v12, 0xc8

    invoke-virtual {v8, v4, v12, v13}, Lcom/android/launcher2/PagedView$HoverScrollHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    .line 3290
    .end local v4    # "msg":Landroid/os/Message;
    :cond_c
    iput-boolean v9, p0, Lcom/android/launcher2/PagedView;->mScrollByHover:Z

    .line 3291
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

    invoke-virtual {v8, v10}, Lcom/android/launcher2/PagedView$HoverScrollHandler;->hasMessages(I)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 3292
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

    invoke-virtual {v8, v10}, Lcom/android/launcher2/PagedView$HoverScrollHandler;->removeMessages(I)V

    goto :goto_1

    .line 3297
    :pswitch_2
    iput-boolean v9, p0, Lcom/android/launcher2/PagedView;->mScrollByHover:Z

    .line 3298
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

    invoke-virtual {v8, v10}, Lcom/android/launcher2/PagedView$HoverScrollHandler;->hasMessages(I)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 3299
    iget-object v8, p0, Lcom/android/launcher2/PagedView;->mHoverScrollHandler:Lcom/android/launcher2/PagedView$HoverScrollHandler;

    invoke-virtual {v8, v10}, Lcom/android/launcher2/PagedView$HoverScrollHandler;->removeMessages(I)V

    goto :goto_1

    .line 3275
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public scrollLeft()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 2745
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2746
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/android/launcher2/PagedView;->PAGE_SNAP_ANIMATION_DURATION:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher2/PagedView;->snapToPage(III)V

    .line 2750
    :goto_0
    return-void

    .line 2748
    :cond_0
    iget v0, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/android/launcher2/PagedView;->PAGE_SNAP_ANIMATION_DURATION:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher2/PagedView;->snapToPage(III)V

    goto :goto_0
.end method

.method public scrollRight()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 2753
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2754
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/android/launcher2/PagedView;->PAGE_SNAP_ANIMATION_DURATION:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher2/PagedView;->snapToPage(III)V

    .line 2758
    :goto_0
    return-void

    .line 2756
    :cond_0
    iget v0, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/android/launcher2/PagedView;->PAGE_SNAP_ANIMATION_DURATION:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher2/PagedView;->snapToPage(III)V

    goto :goto_0
.end method

.method public scrollTo(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 794
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->mIsOverScrolled:Z

    .line 795
    iput p1, p0, Lcom/android/launcher2/PagedView;->mUnboundedScrollX:I

    .line 796
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 797
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    .line 801
    :goto_0
    int-to-float v0, p1

    iput v0, p0, Lcom/android/launcher2/PagedView;->mTouchX:F

    .line 802
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    long-to-float v0, v0

    const v1, 0x4e6e6b28    # 1.0E9f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/PagedView;->mSmoothingTime:F

    .line 803
    return-void

    .line 799
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/android/launcher2/PagedView;->scrollToNonLooped(II)V

    goto :goto_0
.end method

.method protected setAccessibilityFocusChange(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 713
    return-void
.end method

.method protected setAllowLongPress(Z)V
    .locals 0
    .param p1, "allowLongPress"    # Z

    .prologue
    .line 2776
    iput-boolean p1, p0, Lcom/android/launcher2/PagedView;->mAllowLongPress:Z

    .line 2777
    return-void
.end method

.method public setCurrentPage(I)V
    .locals 3
    .param p1, "currentPage"    # I

    .prologue
    .line 533
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->abortScroll()V

    .line 537
    iget v0, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    .line 541
    .local v0, "oldCurrentPage":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 561
    :cond_0
    :goto_0
    return-void

    .line 545
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getAdjustedPageIndex(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    .line 546
    iget v1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lcom/android/launcher2/CellLayout;

    if-eqz v1, :cond_2

    .line 547
    iget v1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/CellLayout;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/android/launcher2/CellLayout;->setAlpha(F)V

    .line 548
    :cond_2
    const/high16 v1, -0x80000000

    iput v1, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    .line 549
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->mLaidOutAfterViewTreeDirty:Z

    if-nez v1, :cond_3

    .line 550
    iget v1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    iput v1, p0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayout:I

    .line 551
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayoutMustAnim:Z

    goto :goto_0

    .line 554
    :cond_3
    invoke-direct {p0, p1}, Lcom/android/launcher2/PagedView;->jumpToPageInternal(I)V

    .line 558
    iget v1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    if-eq v0, v1, :cond_0

    .line 559
    iget v1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->loadAssociatedPages(I)V

    goto :goto_0
.end method

.method protected setCurrentPageIfNotSnapping(I)V
    .locals 1
    .param p1, "currentPage"    # I

    .prologue
    .line 515
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher2/PagedView;->setCurrentPageIfNotSnapping(IZ)V

    .line 516
    return-void
.end method

.method protected setCurrentPageIfNotSnapping(IZ)V
    .locals 2
    .param p1, "currentPage"    # I
    .param p2, "bPageIndicatorAnimation"    # Z

    .prologue
    .line 522
    iget v0, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    if-ne p1, v0, :cond_1

    .line 530
    :cond_0
    :goto_0
    return-void

    .line 525
    :cond_1
    iget v0, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    if-ne p1, v0, :cond_2

    iget v0, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    iget v1, p0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayout:I

    if-eq v0, v1, :cond_0

    .line 528
    :cond_2
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    if-nez p2, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/android/launcher2/PageIndicatorManager;->setAnimationPrevented(Z)V

    .line 529
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->setCurrentPage(I)V

    goto :goto_0

    .line 528
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected setDataIsReady()V
    .locals 1

    .prologue
    .line 456
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->mIsDataReady:Z

    .line 457
    return-void
.end method

.method public setDefaultTransitionEffect(Lcom/android/launcher2/PagedView$TransitionEffect;)V
    .locals 0
    .param p1, "transitionEffect"    # Lcom/android/launcher2/PagedView$TransitionEffect;

    .prologue
    .line 3378
    iput-object p1, p0, Lcom/android/launcher2/PagedView;->mDefaultTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

    iput-object p1, p0, Lcom/android/launcher2/PagedView;->mCurrentTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

    .line 3379
    return-void
.end method

.method protected setDirtyFlags()V
    .locals 1

    .prologue
    .line 1423
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/android/launcher2/PagedView;->mLastScrollX:I

    .line 1424
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->mPageTransformsDirty:Z

    .line 1426
    return-void
.end method

.method public setHideItems(Z)V
    .locals 0
    .param p1, "v"    # Z

    .prologue
    .line 3598
    iput-boolean p1, p0, Lcom/android/launcher2/PagedView;->mHideItems:Z

    .line 3599
    return-void
.end method

.method public setHoverScrollBoundary()V
    .locals 5

    .prologue
    const v4, 0x7f0e00f1

    const/4 v3, 0x0

    .line 4000
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 4002
    .local v0, "config":Landroid/content/res/Configuration;
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0198

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->mHoverScrollWidth:I

    .line 4003
    iput v3, p0, Lcom/android/launcher2/PagedView;->mHoverScrollRightPadding:I

    .line 4005
    iget v1, p0, Lcom/android/launcher2/PagedView;->mHoverScrollVerticalPadding:I

    sget v2, Lcom/android/launcher2/PagedView;->mHoverScrollVerticalPaddingMatchParent:I

    if-ne v1, v2, :cond_2

    .line 4006
    iput v3, p0, Lcom/android/launcher2/PagedView;->mHoverScrollTop:I

    .line 4007
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->mHoverScrollBottom:I

    .line 4009
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->isTabletLayout()Z

    move-result v1

    if-nez v1, :cond_1

    instance-of v1, p0, Lcom/android/launcher2/Workspace;

    if-eqz v1, :cond_1

    .line 4010
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 4011
    iget v1, p0, Lcom/android/launcher2/PagedView;->mHoverScrollBottom:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/launcher2/PagedView;->mHoverScrollBottom:I

    .line 4013
    :cond_0
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 4014
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->mHoverScrollRightPadding:I

    .line 4021
    :cond_1
    :goto_0
    return-void

    .line 4018
    :cond_2
    iget v1, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorTop:I

    iget v2, p0, Lcom/android/launcher2/PagedView;->mHoverScrollVerticalPadding:I

    sub-int/2addr v1, v2

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->mHoverScrollTop:I

    .line 4019
    iget v1, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorTop:I

    invoke-static {}, Lcom/android/launcher2/PageIndicatorManager;->getPageIndicatorHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/android/launcher2/PagedView;->mHoverScrollVerticalPadding:I

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->mHoverScrollBottom:I

    goto :goto_0
.end method

.method public setHoveringSpenIcon(I)V
    .locals 2
    .param p1, "dir"    # I

    .prologue
    .line 3336
    packed-switch p1, :pswitch_data_0

    .line 3344
    const/4 v0, 0x1

    const/4 v1, -0x1

    :try_start_0
    invoke-static {v0, v1}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V

    .line 3345
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->mScrollByHover:Z

    .line 3350
    :goto_0
    return-void

    .line 3338
    :pswitch_0
    const/16 v0, 0x11

    const/4 v1, -0x1

    invoke-static {v0, v1}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V

    goto :goto_0

    .line 3348
    :catch_0
    move-exception v0

    goto :goto_0

    .line 3341
    :pswitch_1
    const/16 v0, 0xd

    const/4 v1, -0x1

    invoke-static {v0, v1}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3336
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected setLastPageNotify(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 594
    iput p1, p0, Lcom/android/launcher2/PagedView;->mLastPageNotify:I

    .line 595
    return-void
.end method

.method public setMaxScrollX()V
    .locals 4

    .prologue
    .line 1308
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    .line 1309
    .local v0, "childCount":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v1

    .line 1311
    .local v1, "width":I
    iget v2, p0, Lcom/android/launcher2/PagedView;->mPageSpacingHint:I

    if-gez v2, :cond_0

    .line 1312
    div-int/lit8 v2, v1, 0x10

    iput v2, p0, Lcom/android/launcher2/PagedView;->mPageSpacing:I

    .line 1317
    :goto_0
    const/4 v2, 0x1

    if-gt v0, v2, :cond_1

    .line 1318
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/launcher2/PagedView;->mMaxScrollX:I

    .line 1322
    :goto_1
    return-void

    .line 1314
    :cond_0
    iget v2, p0, Lcom/android/launcher2/PagedView;->mPageSpacingHint:I

    iput v2, p0, Lcom/android/launcher2/PagedView;->mPageSpacing:I

    goto :goto_0

    .line 1320
    :cond_1
    iget v2, p0, Lcom/android/launcher2/PagedView;->mPageSpacing:I

    add-int/2addr v2, v1

    add-int/lit8 v3, v0, -0x1

    mul-int/2addr v2, v3

    iput v2, p0, Lcom/android/launcher2/PagedView;->mMaxScrollX:I

    goto :goto_1
.end method

.method public setOnDragListener(Landroid/view/View$OnDragListener;)V
    .locals 3
    .param p1, "l"    # Landroid/view/View$OnDragListener;

    .prologue
    .line 760
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v0

    .line 761
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 762
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 761
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 764
    :cond_0
    return-void
.end method

.method public setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .locals 3
    .param p1, "l"    # Landroid/view/View$OnLongClickListener;

    .prologue
    .line 751
    iput-object p1, p0, Lcom/android/launcher2/PagedView;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 752
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v0

    .line 753
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 754
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 753
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 756
    :cond_0
    return-void
.end method

.method public setPageBackgroundAlpha(F)V
    .locals 1
    .param p1, "alpha"    # F

    .prologue
    .line 3368
    iput p1, p0, Lcom/android/launcher2/PagedView;->mPageBackgroundAlpha:F

    .line 3369
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->mPageTransformsDirty:Z

    .line 3371
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    .line 3372
    return-void
.end method

.method protected setPageIndicatorTop(I)V
    .locals 1
    .param p1, "top"    # I

    .prologue
    .line 3846
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/PageIndicatorManager;->setTop(I)V

    .line 3847
    return-void
.end method

.method public setPageSwitchListener(Lcom/android/launcher2/PagedView$PageSwitchListener;)V
    .locals 3
    .param p1, "pageSwitchListener"    # Lcom/android/launcher2/PagedView$PageSwitchListener;

    .prologue
    .line 441
    iput-object p1, p0, Lcom/android/launcher2/PagedView;->mPageSwitchListener:Lcom/android/launcher2/PagedView$PageSwitchListener;

    .line 442
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageSwitchListener:Lcom/android/launcher2/PagedView$PageSwitchListener;

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageSwitchListener:Lcom/android/launcher2/PagedView$PageSwitchListener;

    iget v1, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/android/launcher2/PagedView;->mCurrentPage:I

    invoke-interface {v0, v1, v2}, Lcom/android/launcher2/PagedView$PageSwitchListener;->onPageSwitch(Landroid/view/View;I)V

    .line 445
    :cond_0
    return-void
.end method

.method public setPageZoom(F)V
    .locals 1
    .param p1, "zoom"    # F

    .prologue
    .line 3354
    iput p1, p0, Lcom/android/launcher2/PagedView;->mPageZoom:F

    .line 3355
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->mPageTransformsDirty:Z

    .line 3357
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    .line 3358
    return-void
.end method

.method public showPageIndicator(Z)V
    .locals 1
    .param p1, "animated"    # Z

    .prologue
    .line 3174
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/PageIndicatorManager;->showPageIndicator(Z)V

    .line 3175
    return-void
.end method

.method protected snapToDestination()V
    .locals 2

    .prologue
    .line 2589
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageNearestToCenterOfScreen()I

    move-result v0

    iget v1, p0, Lcom/android/launcher2/PagedView;->PAGE_SNAP_ANIMATION_DURATION:I

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/PagedView;->snapToPageWithDur(II)V

    .line 2590
    return-void
.end method

.method protected snapToPage(I)V
    .locals 1
    .param p1, "whichPage"    # I

    .prologue
    .line 2651
    iget v0, p0, Lcom/android/launcher2/PagedView;->PAGE_SNAP_ANIMATION_DURATION:I

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher2/PagedView;->snapToPageWithDur(II)V

    .line 2652
    return-void
.end method

.method protected snapToPage(III)V
    .locals 1
    .param p1, "whichPage"    # I
    .param p2, "duration"    # I
    .param p3, "dir"    # I

    .prologue
    .line 2669
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->mLaidOutAfterViewTreeDirty:Z

    if-nez v0, :cond_0

    .line 2670
    iput p3, p0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayoutDir:I

    .line 2671
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayoutMustAnim:Z

    .line 2672
    iput p1, p0, Lcom/android/launcher2/PagedView;->mNextPage:I

    .line 2673
    iput p1, p0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayout:I

    .line 2674
    iput p2, p0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayoutDur:I

    .line 2678
    :goto_0
    return-void

    .line 2677
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher2/PagedView;->snapToPageInternal(III)V

    goto :goto_0
.end method

.method protected snapToPageFromFocusHelper(II)V
    .locals 0
    .param p1, "whichPage"    # I
    .param p2, "dir"    # I

    .prologue
    .line 2659
    iput p2, p0, Lcom/android/launcher2/PagedView;->mLastFocusDir:I

    .line 2660
    invoke-virtual {p0, p1, p2}, Lcom/android/launcher2/PagedView;->snapToPageWithDir(II)V

    .line 2661
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->loadAssociatedPages(I)V

    .line 2662
    return-void
.end method

.method protected snapToPageWithDir(II)V
    .locals 1
    .param p1, "whichPage"    # I
    .param p2, "dir"    # I

    .prologue
    .line 2665
    iget v0, p0, Lcom/android/launcher2/PagedView;->PAGE_SNAP_ANIMATION_DURATION:I

    invoke-virtual {p0, p1, v0, p2}, Lcom/android/launcher2/PagedView;->snapToPage(III)V

    .line 2666
    return-void
.end method

.method protected snapToPageWithDur(II)V
    .locals 1
    .param p1, "whichPage"    # I
    .param p2, "duration"    # I

    .prologue
    .line 2655
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/launcher2/PagedView;->snapToPage(III)V

    .line 2656
    return-void
.end method

.method protected snapToPageWithVelocity(II)V
    .locals 9
    .param p1, "whichPage"    # I
    .param p2, "velocity"    # I

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    .line 2614
    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {p1, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 2615
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v4, v6, 0x2

    .line 2617
    .local v4, "halfScreenSize":I
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getNonLoopedScrollXForPageIndex(I)I

    move-result v5

    .line 2621
    .local v5, "newX":I
    iget v6, p0, Lcom/android/launcher2/PagedView;->mUnboundedScrollX:I

    sub-int v0, v5, v6

    .line 2622
    .local v0, "delta":I
    const/4 v3, 0x0

    .line 2624
    .local v3, "duration":I
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v6

    const/16 v7, 0xfa

    if-ge v6, v7, :cond_0

    .line 2627
    iget v6, p0, Lcom/android/launcher2/PagedView;->PAGE_SNAP_ANIMATION_DURATION:I

    invoke-virtual {p0, p1, v6}, Lcom/android/launcher2/PagedView;->snapToPageWithDur(II)V

    .line 2648
    :goto_0
    return-void

    .line 2635
    :cond_0
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v8

    mul-int/lit8 v7, v4, 0x2

    int-to-float v7, v7

    div-float/2addr v6, v7

    invoke-static {v8, v6}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 2636
    .local v2, "distanceRatio":F
    int-to-float v6, v4

    int-to-float v7, v4

    invoke-direct {p0, v2}, Lcom/android/launcher2/PagedView;->distanceInfluenceForSnapDuration(F)F

    move-result v8

    mul-float/2addr v7, v8

    add-float v1, v6, v7

    .line 2639
    .local v1, "distance":F
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p2

    .line 2640
    const/16 v6, 0x898

    invoke-static {v6, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 2645
    const/high16 v6, 0x447a0000    # 1000.0f

    int-to-float v7, p2

    div-float v7, v1, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    mul-int/lit8 v3, v6, 0x4

    .line 2647
    invoke-virtual {p0, p1, v0, v3}, Lcom/android/launcher2/PagedView;->snapToPage(III)V

    goto :goto_0
.end method

.method public abstract syncPageItems(IZ)V
.end method

.method protected syncPageWithQuickView(I)Z
    .locals 1
    .param p1, "whichPage"    # I

    .prologue
    .line 2761
    const/4 v0, 0x1

    return v0
.end method

.method public abstract syncPages()V
.end method

.method protected syncViewVisibility()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x0

    .line 676
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_7

    .line 677
    const/4 v0, 0x0

    .line 678
    .local v0, "found":Z
    iget-object v5, p0, Lcom/android/launcher2/PagedView;->mVisiblePages:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/PagedView$PageInfo;

    .line 679
    .local v3, "info":Lcom/android/launcher2/PagedView$PageInfo;
    iget v5, v3, Lcom/android/launcher2/PagedView$PageInfo;->mIndex:I

    if-ne v5, v1, :cond_0

    .line 680
    const/4 v0, 0x1

    .line 685
    .end local v3    # "info":Lcom/android/launcher2/PagedView$PageInfo;
    :cond_1
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    .line 686
    .local v4, "v":Landroid/view/View;
    if-nez v4, :cond_2

    .line 676
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 689
    :cond_2
    if-eqz v0, :cond_6

    .line 690
    iget-object v5, p0, Lcom/android/launcher2/PagedView;->mCurrentTransitionEffect:Lcom/android/launcher2/PagedView$TransitionEffect;

    sget-object v6, Lcom/android/launcher2/PagedView$TransitionEffect;->CARD:Lcom/android/launcher2/PagedView$TransitionEffect;

    invoke-virtual {v5, v6}, Lcom/android/launcher2/PagedView$TransitionEffect;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 694
    instance-of v5, v4, Lcom/android/launcher2/CellLayout;

    if-eqz v5, :cond_4

    move-object v5, v4

    check-cast v5, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v5}, Lcom/android/launcher2/CellLayout;->getChildrenAlpha()F

    move-result v5

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-nez v5, :cond_3

    sget-boolean v5, Lcom/android/launcher2/PagedView;->mIsPageFastMoving:Z

    if-eqz v5, :cond_4

    .line 695
    :cond_3
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 697
    :cond_4
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 701
    :cond_5
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 704
    :cond_6
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 707
    .end local v0    # "found":Z
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "v":Landroid/view/View;
    :cond_7
    return-void
.end method

.method protected updateChildrenLayersEnabled(Lcom/android/launcher2/PagedView$LayerOptions;)V
    .locals 8
    .param p1, "option"    # Lcom/android/launcher2/PagedView$LayerOptions;

    .prologue
    const/4 v7, 0x0

    .line 3102
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isPageMoving()Z

    move-result v0

    .line 3103
    .local v0, "enableChildrenLayers":Z
    iget-boolean v4, p0, Lcom/android/launcher2/PagedView;->mBlockHardwareLayers:Z

    if-eqz v4, :cond_0

    .line 3104
    sget-object p1, Lcom/android/launcher2/PagedView$LayerOptions;->FORCE_NONE:Lcom/android/launcher2/PagedView$LayerOptions;

    .line 3106
    :cond_0
    sget-object v4, Lcom/android/launcher2/PagedView$LayerOptions;->FORCE_NONE:Lcom/android/launcher2/PagedView$LayerOptions;

    if-ne p1, v4, :cond_3

    .line 3107
    const/4 v0, 0x0

    .line 3112
    :cond_1
    :goto_0
    if-eqz v0, :cond_4

    .line 3113
    iget-object v4, p0, Lcom/android/launcher2/PagedView;->mVisiblePages:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/PagedView$PageInfo;

    .line 3114
    .local v3, "pageInfo":Lcom/android/launcher2/PagedView$PageInfo;
    iget v4, v3, Lcom/android/launcher2/PagedView$PageInfo;->mIndex:I

    invoke-virtual {p0, v4}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 3115
    iget v4, v3, Lcom/android/launcher2/PagedView$PageInfo;->mIndex:I

    invoke-virtual {p0, v4}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/Page;

    invoke-interface {v4}, Lcom/android/launcher2/Page;->enableHardwareLayers()V

    .line 3116
    iget-object v4, p0, Lcom/android/launcher2/PagedView;->mHardwarePages:Ljava/util/ArrayList;

    iget v5, v3, Lcom/android/launcher2/PagedView$PageInfo;->mIndex:I

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 3108
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "pageInfo":Lcom/android/launcher2/PagedView$PageInfo;
    :cond_3
    sget-object v4, Lcom/android/launcher2/PagedView$LayerOptions;->FORCE_HARDWARE:Lcom/android/launcher2/PagedView$LayerOptions;

    if-ne p1, v4, :cond_1

    .line 3109
    const/4 v0, 0x1

    goto :goto_0

    .line 3119
    :cond_4
    sget-object v4, Lcom/android/launcher2/PagedView$LayerOptions;->FORCE_NONE:Lcom/android/launcher2/PagedView$LayerOptions;

    if-ne p1, v4, :cond_5

    .line 3120
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v4

    if-ge v1, v4, :cond_7

    .line 3121
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/Page;

    invoke-interface {v4}, Lcom/android/launcher2/Page;->disableHardwareLayers()V

    .line 3122
    iget-object v4, p0, Lcom/android/launcher2/PagedView;->mHardwarePages:Ljava/util/ArrayList;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3120
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3126
    .end local v1    # "i":I
    :cond_5
    iget-object v4, p0, Lcom/android/launcher2/PagedView;->mVisiblePages:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_6
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/PagedView$PageInfo;

    .line 3127
    .restart local v3    # "pageInfo":Lcom/android/launcher2/PagedView$PageInfo;
    if-eqz v3, :cond_6

    iget v4, v3, Lcom/android/launcher2/PagedView$PageInfo;->mIndex:I

    invoke-virtual {p0, v4}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/Page;

    if-eqz v4, :cond_6

    .line 3128
    iget v4, v3, Lcom/android/launcher2/PagedView$PageInfo;->mIndex:I

    invoke-virtual {p0, v4}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/Page;

    invoke-interface {v4}, Lcom/android/launcher2/Page;->disableHardwareLayers()V

    .line 3129
    iget-object v4, p0, Lcom/android/launcher2/PagedView;->mHardwarePages:Ljava/util/ArrayList;

    iget v5, v3, Lcom/android/launcher2/PagedView$PageInfo;->mIndex:I

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 3134
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "pageInfo":Lcom/android/launcher2/PagedView$PageInfo;
    :cond_7
    return-void
.end method

.method updateIndicator(II)V
    .locals 2
    .param p1, "top"    # I
    .param p2, "gap"    # I

    .prologue
    .line 3850
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {v0}, Lcom/android/launcher2/PageIndicatorManager;->cancelEnterFastScroll()V

    .line 3851
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/PageIndicatorManager;->setTop(I)V

    .line 3852
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorManager:Lcom/android/launcher2/PageIndicatorManager;

    invoke-virtual {v0, p2}, Lcom/android/launcher2/PageIndicatorManager;->setGap(I)V

    .line 3853
    iput p1, p0, Lcom/android/launcher2/PagedView;->mPageIndicatorTop:I

    .line 3854
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0197

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/PagedView;->mHoverScrollVerticalPadding:I

    .line 3855
    return-void
.end method

.method updateIndicatorWidth(Z)V
    .locals 0
    .param p1, "hasHotseatRight"    # Z

    .prologue
    .line 3862
    return-void
.end method

.method protected updatePageTransform(Landroid/view/View;F)V
    .locals 0
    .param p1, "page"    # Landroid/view/View;
    .param p2, "scrollProgress"    # F

    .prologue
    .line 1438
    return-void
.end method

.method public updatePageTransforms()V
    .locals 7

    .prologue
    .line 1604
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v2, v6, 0x2

    .line 1605
    .local v2, "halfScreenSize":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v6

    add-int v5, v6, v2

    .line 1606
    .local v5, "screenCenter":I
    iget v6, p0, Lcom/android/launcher2/PagedView;->mLastScreenCenter:I

    if-eq v5, v6, :cond_0

    .line 1607
    invoke-virtual {p0, v5}, Lcom/android/launcher2/PagedView;->screenScrolled(I)V

    .line 1608
    iput v5, p0, Lcom/android/launcher2/PagedView;->mLastScreenCenter:I

    .line 1610
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getDrawingTime()J

    move-result-wide v0

    .line 1611
    .local v0, "drawingTime":J
    iget-boolean v6, p0, Lcom/android/launcher2/PagedView;->mPageTransformsDirty:Z

    if-eqz v6, :cond_1

    .line 1612
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->updateVisiblePages()I

    .line 1614
    :cond_1
    iget-object v6, p0, Lcom/android/launcher2/PagedView;->mVisiblePages:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/PagedView$PageInfo;

    .line 1615
    .local v4, "pageInfo":Lcom/android/launcher2/PagedView$PageInfo;
    invoke-direct {p0, v4, v0, v1}, Lcom/android/launcher2/PagedView;->transformPage(Lcom/android/launcher2/PagedView$PageInfo;J)V

    goto :goto_0

    .line 1617
    .end local v4    # "pageInfo":Lcom/android/launcher2/PagedView$PageInfo;
    :cond_2
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/launcher2/PagedView;->mPageTransformsDirty:Z

    .line 1618
    return-void
.end method

.method protected updateVisiblePages()I
    .locals 15

    .prologue
    .line 1058
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v12

    .line 1059
    .local v12, "scrollX":I
    iget v13, p0, Lcom/android/launcher2/PagedView;->mLastScrollX:I

    if-ne v12, v13, :cond_0

    iget v13, p0, Lcom/android/launcher2/PagedView;->mSnapToPageAfterLayout:I

    const/high16 v14, -0x80000000

    if-ne v13, v14, :cond_0

    .line 1063
    iget-object v13, p0, Lcom/android/launcher2/PagedView;->mVisiblePages:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 1123
    :goto_0
    return v13

    .line 1069
    :cond_0
    iget-object v13, p0, Lcom/android/launcher2/PagedView;->mPagesPool:Ljava/util/ArrayList;

    iget-object v14, p0, Lcom/android/launcher2/PagedView;->mVisiblePages:Ljava/util/ArrayList;

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1070
    iget-object v13, p0, Lcom/android/launcher2/PagedView;->mVisiblePages:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    .line 1071
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v6

    .line 1072
    .local v6, "pageCount":I
    if-lez v6, :cond_6

    .line 1074
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getHorizontalVisibilityExtension()I

    move-result v1

    .line 1077
    .local v1, "horizontalVisibilityExtension":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v11

    .line 1078
    .local v11, "pageWidth":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageTotWidth()I

    move-result v10

    .line 1079
    .local v10, "pageTotWidth":I
    div-int/lit8 v13, v11, 0x2

    add-int v5, v12, v13

    .line 1083
    .local v5, "pageCenter":I
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPaddingLeft()I

    move-result v13

    add-int/2addr v13, v12

    sub-int v8, v13, v1

    .line 1084
    .local v8, "pageLeftEdge":I
    add-int v13, v12, v10

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPaddingRight()I

    move-result v14

    sub-int/2addr v13, v14

    mul-int/lit8 v14, v1, 0x2

    add-int v9, v13, v14

    .line 1091
    .local v9, "pageRightEdge":I
    const/high16 v3, -0x80000000

    .line 1093
    .local v3, "lastIndex":I
    move v0, v8

    .local v0, "currEdge":I
    :goto_1
    if-ge v0, v9, :cond_4

    .line 1094
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getPageIndexForScrollX(I)I

    move-result v2

    .line 1095
    .local v2, "index":I
    const/high16 v13, -0x80000000

    if-eq v3, v13, :cond_2

    if-ne v3, v2, :cond_2

    .line 1093
    :cond_1
    :goto_2
    add-int/lit8 v13, v10, -0x1

    add-int/2addr v0, v13

    goto :goto_1

    .line 1098
    :cond_2
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    .line 1099
    .local v4, "page":Landroid/view/View;
    if-eqz v4, :cond_1

    .line 1102
    move v3, v2

    .line 1103
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->newPageInfo()Lcom/android/launcher2/PagedView$PageInfo;

    move-result-object v7

    .line 1104
    .local v7, "pageInfo":Lcom/android/launcher2/PagedView$PageInfo;
    iput v2, v7, Lcom/android/launcher2/PagedView$PageInfo;->mIndex:I

    .line 1105
    invoke-direct {p0, v0}, Lcom/android/launcher2/PagedView;->getLowerBoundForScrollX(I)I

    move-result v13

    iput v13, v7, Lcom/android/launcher2/PagedView$PageInfo;->mLowerBound:I

    .line 1106
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isLoopingEnabled()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 1107
    iget v13, v7, Lcom/android/launcher2/PagedView$PageInfo;->mIndex:I

    mul-int/2addr v13, v10

    neg-int v13, v13

    iget v14, v7, Lcom/android/launcher2/PagedView$PageInfo;->mLowerBound:I

    add-int/2addr v13, v14

    iput v13, v7, Lcom/android/launcher2/PagedView$PageInfo;->mTransX:I

    .line 1111
    :goto_3
    invoke-direct {p0, v4}, Lcom/android/launcher2/PagedView;->getCenterOfViewRelative(Landroid/view/View;)I

    move-result v13

    iput v13, v7, Lcom/android/launcher2/PagedView$PageInfo;->mViewCntrRel:I

    .line 1112
    iget v13, v7, Lcom/android/launcher2/PagedView$PageInfo;->mViewCntrRel:I

    iget v14, v7, Lcom/android/launcher2/PagedView$PageInfo;->mLowerBound:I

    add-int/2addr v13, v14

    iput v13, v7, Lcom/android/launcher2/PagedView$PageInfo;->mViewCntr:I

    .line 1113
    iget v13, v7, Lcom/android/launcher2/PagedView$PageInfo;->mViewCntr:I

    sub-int v13, v5, v13

    iput v13, v7, Lcom/android/launcher2/PagedView$PageInfo;->mDistFromCntr:I

    .line 1114
    iget-object v13, p0, Lcom/android/launcher2/PagedView;->mVisiblePages:Ljava/util/ArrayList;

    invoke-virtual {v13, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1109
    :cond_3
    const/4 v13, 0x0

    iput v13, v7, Lcom/android/launcher2/PagedView$PageInfo;->mTransX:I

    goto :goto_3

    .line 1116
    .end local v2    # "index":I
    .end local v4    # "page":Landroid/view/View;
    .end local v7    # "pageInfo":Lcom/android/launcher2/PagedView$PageInfo;
    :cond_4
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->syncViewVisibility()V

    .line 1118
    iget-boolean v13, p0, Lcom/android/launcher2/PagedView;->mIsPageMoving:Z

    if-eqz v13, :cond_5

    .line 1119
    sget-object v13, Lcom/android/launcher2/PagedView$LayerOptions;->DEFAULT:Lcom/android/launcher2/PagedView$LayerOptions;

    invoke-virtual {p0, v13}, Lcom/android/launcher2/PagedView;->updateChildrenLayersEnabled(Lcom/android/launcher2/PagedView$LayerOptions;)V

    .line 1121
    :cond_5
    iput v12, p0, Lcom/android/launcher2/PagedView;->mLastScrollX:I

    .line 1123
    .end local v0    # "currEdge":I
    .end local v1    # "horizontalVisibilityExtension":I
    .end local v3    # "lastIndex":I
    .end local v5    # "pageCenter":I
    .end local v8    # "pageLeftEdge":I
    .end local v9    # "pageRightEdge":I
    .end local v10    # "pageTotWidth":I
    .end local v11    # "pageWidth":I
    :cond_6
    iget-object v13, p0, Lcom/android/launcher2/PagedView;->mVisiblePages:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    goto/16 :goto_0
.end method
