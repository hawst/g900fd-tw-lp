.class Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;
.super Landroid/os/Handler;
.source "MoviePlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v8, 0xc

    const/4 v5, 0x0

    const-wide/16 v6, 0x14

    const/4 v4, 0x4

    const/high16 v3, 0x3f800000    # 1.0f

    .line 264
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 328
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 266
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCurrentVolume:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCurrentVolume:F

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 271
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget v1, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCurrentVolume:F

    const v2, 0x3c23d70a    # 0.01f

    add-float/2addr v1, v2

    iput v1, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCurrentVolume:F

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCurrentVolume:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_2

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 275
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iput v3, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCurrentVolume:F

    goto :goto_0

    .line 282
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v1, 0x65

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)V

    goto :goto_0

    .line 286
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setWakeMode(Z)V

    goto :goto_0

    .line 290
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPlayCheck()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$400(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V

    goto :goto_0

    .line 294
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPauseCheck()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V

    goto :goto_0

    .line 298
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isStopCheck()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$600(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V

    goto :goto_0

    .line 302
    :pswitch_7
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RESUME_PLAYBACK_BY_CALLSTATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I
    invoke-static {v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$700(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 304
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$700(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_3

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v8, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # operator++ for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$708(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I

    goto/16 :goto_0

    .line 309
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I
    invoke-static {v0, v5}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$702(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->play()V

    goto/16 :goto_0

    .line 316
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoWidth:I
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I

    move-result v0

    if-gtz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v1, 0x8d

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)V

    goto/16 :goto_0

    .line 321
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v0, :cond_0

    .line 322
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    goto/16 :goto_0

    .line 264
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
