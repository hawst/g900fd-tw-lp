.class public Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;
.super Landroid/app/Activity;
.source "HelpPlayerActivity.java"


# static fields
.field private static final CURRENT_STEP:Ljava/lang/String; = "current_step"

.field public static final END_FROM_VOLUME_CONTOL:I = 0x0

.field private static final FINISH_ACTIVITY:I = 0x0

.field private static final FINISH_ACTIVITY_DELAY:I = 0xbb8

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public mBg:Landroid/graphics/Bitmap;

.field private mFinishHandler:Landroid/os/Handler;

.field private mHelpDialogAdjustVolume:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

.field private mHelpDialogFragmentVolume:Ljava/lang/Object;

.field private mHelpDialogHelpEnd:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

.field private mHelpDialogSwitchScreen:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

.field private mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mBg:Landroid/graphics/Bitmap;

    .line 174
    new-instance v0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$1;-><init>(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mFinishHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)Lcom/sec/android/app/mv/player/help/HelpStep;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->setStepNextOfSwitchScreen()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->setStepNextFocusOnVolumeBtn()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogAdjustVolume:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogFragmentVolume:Ljava/lang/Object;

    return-object v0
.end method

.method private initializeActionBar()V
    .locals 3

    .prologue
    .line 228
    sget-object v1, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->TAG:Ljava/lang/String;

    const-string v2, "initializeActionBar()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    const v1, 0x7f0d001d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 231
    .local v0, "mSwitchMV":Landroid/widget/ImageView;
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_DUAL_LCD:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_FOLDER_TYPE:Z

    if-eqz v1, :cond_0

    .line 232
    if-eqz v0, :cond_0

    .line 233
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusableInTouchMode(Z)V

    .line 234
    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    .line 237
    :cond_0
    new-instance v1, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$2;-><init>(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    return-void
.end method

.method private initializeView()V
    .locals 21

    .prologue
    .line 253
    sget-object v19, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->TAG:Ljava/lang/String;

    const-string v20, "initializeView()"

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    const v19, 0x7f0d00af

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 256
    .local v11, "mTitleLayout":Landroid/view/View;
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/view/View;->setVisibility(I)V

    .line 258
    const v19, 0x7f0d00b1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    .line 259
    .local v10, "mRotateBtn":Landroid/widget/ImageButton;
    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 261
    const v19, 0x7f0d006e

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 262
    .local v7, "duration":Landroid/widget/TextView;
    const-string v19, "06:00"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    const v19, 0x7f0d006d

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 265
    .local v6, "currentPosition":Landroid/widget/TextView;
    const-string v19, "03:00"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    const v19, 0x7f0d00b4

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 268
    .local v18, "title":Landroid/widget/TextView;
    const-string v19, "Samsung"

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    const v19, 0x7f0d0066

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/SeekBar;

    .line 271
    .local v16, "seekBar":Landroid/widget/SeekBar;
    const/16 v19, 0x1f4

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 272
    sget-boolean v19, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_DUAL_LCD:Z

    if-eqz v19, :cond_0

    sget-boolean v19, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_FOLDER_TYPE:Z

    if-eqz v19, :cond_0

    .line 273
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setClickable(Z)V

    .line 274
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 275
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setFocusableInTouchMode(Z)V

    .line 278
    :cond_0
    const v19, 0x7f0d00b2

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageButton;

    .line 279
    .local v12, "mVolumeBtn":Landroid/widget/ImageButton;
    sget-boolean v19, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_DUAL_LCD:Z

    if-eqz v19, :cond_1

    sget-boolean v19, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_FOLDER_TYPE:Z

    if-eqz v19, :cond_1

    .line 280
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 281
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 282
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setFocusableInTouchMode(Z)V

    .line 285
    :cond_1
    new-instance v19, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$3;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$3;-><init>(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)V

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 304
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v19

    const v20, 0x7f0d0043

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/app/mv/player/common/VUtils;->changeActionLayoutForUpgradeModels(Landroid/view/View;)V

    .line 305
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v11}, Lcom/sec/android/app/mv/player/common/VUtils;->changeTitleLayoutForUpgradeModels(Landroid/content/Context;Landroid/view/View;)V

    .line 310
    const v19, 0x7f0d0047

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v19

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    .line 313
    sget-boolean v19, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_DUAL_LCD:Z

    if-eqz v19, :cond_9

    sget-boolean v19, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_FOLDER_TYPE:Z

    if-eqz v19, :cond_9

    .line 314
    const v19, 0x7f0d0045

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 315
    .local v4, "actionBack":Landroid/widget/RelativeLayout;
    if-eqz v4, :cond_2

    .line 316
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 317
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 318
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->setFocusableInTouchMode(Z)V

    .line 321
    :cond_2
    const v19, 0x7f0d0051

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    .line 322
    .local v5, "actionGroup":Landroid/widget/RelativeLayout;
    if-eqz v5, :cond_3

    .line 323
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 324
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 325
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setFocusableInTouchMode(Z)V

    .line 328
    :cond_3
    const v19, 0x7f0d001b

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageView;

    .line 329
    .local v17, "svSwitchButton":Landroid/widget/ImageView;
    if-eqz v17, :cond_4

    .line 330
    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 331
    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 332
    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusableInTouchMode(Z)V

    .line 335
    :cond_4
    const v19, 0x7f0d0070

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageButton;

    .line 336
    .local v9, "fitToSrcBtn":Landroid/widget/ImageButton;
    if-eqz v9, :cond_5

    .line 337
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 338
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 339
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/widget/ImageButton;->setFocusableInTouchMode(Z)V

    .line 342
    :cond_5
    const v19, 0x7f0d0072

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageButton;

    .line 343
    .local v14, "playPauseButton":Landroid/widget/ImageButton;
    if-eqz v14, :cond_6

    .line 344
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 345
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 346
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/widget/ImageButton;->setFocusableInTouchMode(Z)V

    .line 349
    :cond_6
    const v19, 0x7f0d0073

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageButton;

    .line 350
    .local v15, "rewButton":Landroid/widget/ImageButton;
    if-eqz v15, :cond_7

    .line 351
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 352
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 353
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/widget/ImageButton;->setFocusableInTouchMode(Z)V

    .line 356
    :cond_7
    const v19, 0x7f0d0074

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    .line 357
    .local v8, "ffButton":Landroid/widget/ImageButton;
    if-eqz v8, :cond_8

    .line 358
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 359
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 360
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/widget/ImageButton;->setFocusableInTouchMode(Z)V

    .line 363
    :cond_8
    const v19, 0x7f0d0075

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageButton;

    .line 364
    .local v13, "mvPlaylistButton":Landroid/widget/ImageButton;
    if-eqz v13, :cond_9

    .line 365
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 366
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 367
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/widget/ImageButton;->setFocusableInTouchMode(Z)V

    .line 371
    .end local v4    # "actionBack":Landroid/widget/RelativeLayout;
    .end local v5    # "actionGroup":Landroid/widget/RelativeLayout;
    .end local v8    # "ffButton":Landroid/widget/ImageButton;
    .end local v9    # "fitToSrcBtn":Landroid/widget/ImageButton;
    .end local v13    # "mvPlaylistButton":Landroid/widget/ImageButton;
    .end local v14    # "playPauseButton":Landroid/widget/ImageButton;
    .end local v15    # "rewButton":Landroid/widget/ImageButton;
    .end local v17    # "svSwitchButton":Landroid/widget/ImageView;
    :cond_9
    new-instance v19, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    const/16 v20, 0x3

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, p0

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;Landroid/app/Activity;I)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogSwitchScreen:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    .line 372
    new-instance v19, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    const/16 v20, 0x4

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, p0

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;Landroid/app/Activity;I)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogAdjustVolume:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    .line 373
    invoke-static {}, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->getInstance()Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogFragmentVolume:Ljava/lang/Object;

    .line 375
    sget-boolean v19, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v19, :cond_a

    .line 376
    const v19, 0x7f0d0071

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v19

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    .line 380
    :goto_0
    return-void

    .line 378
    :cond_a
    const v19, 0x7f0d0075

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v19

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setStepNextFocusOnVolumeBtn()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 210
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_DUAL_LCD:Z

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_FOLDER_TYPE:Z

    if-eqz v2, :cond_1

    .line 211
    const v2, 0x7f0d00b2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 212
    .local v1, "volumeBtn":Landroid/widget/ImageButton;
    if-eqz v1, :cond_0

    .line 213
    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 214
    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 215
    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setFocusableInTouchMode(Z)V

    .line 216
    invoke-virtual {v1}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 218
    :cond_0
    const v2, 0x7f0d001d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 219
    .local v0, "switchMV":Landroid/widget/ImageView;
    if-eqz v0, :cond_1

    .line 220
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 221
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 222
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setFocusableInTouchMode(Z)V

    .line 225
    .end local v0    # "switchMV":Landroid/widget/ImageView;
    .end local v1    # "volumeBtn":Landroid/widget/ImageButton;
    :cond_1
    return-void
.end method

.method private setStepNextOfSwitchScreen()V
    .locals 2

    .prologue
    .line 200
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->TAG:Ljava/lang/String;

    const-string v1, "setStepNextOfSwitchScreen()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpStep;->setCurrentStep(I)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogSwitchScreen:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogSwitchScreen:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogAdjustVolume:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->show()V

    .line 207
    return-void
.end method

.method private setStepSwitchScreen()V
    .locals 2

    .prologue
    .line 190
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->TAG:Ljava/lang/String;

    const-string v1, "setStepSwitchScreen()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpStep;->setCurrentStep(I)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogSwitchScreen:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->show()V

    .line 193
    return-void
.end method

.method private updateMargins()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 413
    const v8, 0x7f0d0066

    invoke-virtual {p0, v8}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/SeekBar;

    .line 414
    .local v7, "seekBar":Landroid/widget/SeekBar;
    invoke-virtual {v7}, Landroid/widget/SeekBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 415
    .local v5, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080022

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 416
    .local v4, "margin":I
    iput v4, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 417
    iput v4, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 418
    invoke-virtual {v7, v10, v10, v10, v10}, Landroid/widget/SeekBar;->setPadding(IIII)V

    .line 419
    invoke-virtual {v7, v5}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 421
    const v8, 0x7f0d006e

    invoke-virtual {p0, v8}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 422
    .local v1, "duration":Landroid/widget/TextView;
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .end local v5    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 423
    .restart local v5    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    iput v4, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 424
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 426
    const v8, 0x7f0d006d

    invoke-virtual {p0, v8}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 427
    .local v0, "currentPosition":Landroid/widget/TextView;
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .end local v5    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 428
    .restart local v5    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    iput v4, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 429
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 431
    const v8, 0x7f0d0073

    invoke-virtual {p0, v8}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    .line 432
    .local v6, "rewButton":Landroid/widget/ImageButton;
    invoke-virtual {v6}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .end local v5    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 433
    .restart local v5    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080127

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 434
    iput v4, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 435
    invoke-virtual {v6, v5}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 437
    const v8, 0x7f0d0074

    invoke-virtual {p0, v8}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 438
    .local v2, "ffButton":Landroid/widget/ImageButton;
    invoke-virtual {v2}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .end local v5    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 439
    .restart local v5    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080120

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 440
    iput v4, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 441
    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 443
    const v8, 0x7f0d0070

    invoke-virtual {p0, v8}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 444
    .local v3, "fitToSrcBtn":Landroid/widget/ImageButton;
    invoke-virtual {v3}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .end local v5    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 445
    .restart local v5    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080128

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 446
    iput v4, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 447
    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 448
    return-void
.end method


# virtual methods
.method public exitApp(I)V
    .locals 4
    .param p1, "requestCode"    # I

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x1

    .line 152
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->TAG:Ljava/lang/String;

    const-string v1, "exitApp()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    packed-switch p1, :pswitch_data_0

    .line 172
    :goto_0
    return-void

    .line 156
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/mv/player/help/HelpStep;->setCurrentStep(I)V

    .line 158
    new-instance v0, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-direct {v0, p0, p0, v3}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogHelpEnd:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogHelpEnd:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    const v1, 0x7f030005

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->setContentView(I)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogHelpEnd:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    sget-object v1, Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->setTouchTransparencyMode(Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogHelpEnd:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->setShowWrongInputToast(Z)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogHelpEnd:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->setIgnoreHoverEvt(Z)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogHelpEnd:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogHelpEnd:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->show()V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mFinishHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 154
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x4

    const/4 v1, 0x3

    .line 383
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 385
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 386
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->updateMargins()V

    .line 388
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ROTATE_SECOND_SCREEN:Z

    if-eqz v0, :cond_3

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogSwitchScreen:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogSwitchScreen:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 391
    iput-object v3, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogSwitchScreen:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    .line 394
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogAdjustVolume:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogAdjustVolume:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 396
    iput-object v3, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogAdjustVolume:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    .line 399
    :cond_2
    new-instance v0, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-direct {v0, p0, p0, v1}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogSwitchScreen:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    .line 400
    new-instance v0, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-direct {v0, p0, p0, v2}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogAdjustVolume:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 403
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->setStepSwitchScreen()V

    .line 410
    :cond_3
    :goto_0
    return-void

    .line 404
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v0

    if-ne v0, v2, :cond_5

    .line 405
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->setStepNextOfSwitchScreen()V

    goto :goto_0

    .line 407
    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->exitApp(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    sget-object v2, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->TAG:Ljava/lang/String;

    const-string v3, "onCreate()"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const v2, 0x7f030006

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->setContentView(I)V

    .line 61
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    const v2, 0x7f0d004c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 63
    .local v0, "sl":Landroid/widget/RelativeLayout;
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 69
    .end local v0    # "sl":Landroid/widget/RelativeLayout;
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->initializeView()V

    .line 70
    invoke-static {}, Lcom/sec/android/app/mv/player/help/HelpStep;->getInstance()Lcom/sec/android/app/mv/player/help/HelpStep;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    .line 72
    const/4 v1, 0x5

    .line 74
    .local v1, "status":I
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 75
    const/4 v1, 0x5

    .line 84
    :goto_1
    sget-object v2, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate(). status = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    iget-object v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/mv/player/help/HelpStep;->setCurrentStep(I)V

    .line 87
    return-void

    .line 65
    .end local v1    # "status":I
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->initializeActionBar()V

    .line 66
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 77
    .restart local v1    # "status":I
    :cond_1
    if-nez p1, :cond_2

    .line 78
    const/4 v1, 0x3

    goto :goto_1

    .line 80
    :cond_2
    const-string v2, "current_step"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 139
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogHelpEnd:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogHelpEnd:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogHelpEnd:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 145
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 127
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->TAG:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 130
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause. CurrentStep : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogSwitchScreen:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogSwitchScreen:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 135
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 110
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume(). currentStep : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->setStepSwitchScreen()V

    .line 120
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 116
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->setStepNextOfSwitchScreen()V

    goto :goto_0

    .line 118
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->exitApp(I)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->TAG:Ljava/lang/String;

    const-string v1, "onSaveInstanceState()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    if-eqz p1, :cond_1

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    if-eqz v0, :cond_0

    .line 95
    const-string v0, "current_step"

    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 97
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 99
    :cond_1
    return-void
.end method
