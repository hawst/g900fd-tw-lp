.class Lcom/sec/android/app/mv/player/view/VideoFlickView$1;
.super Landroid/os/Handler;
.source "VideoFlickView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoFlickView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoFlickView;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v6, 0xfa

    const v4, 0x7f0200a2

    const v3, 0x7f0200a0

    const/4 v2, 0x2

    .line 92
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 94
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftArrayListImageView:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$100(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$000(Lcom/sec/android/app/mv/player/view/VideoFlickView;)I

    move-result v1

    rem-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f02009f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftArrayListImageView:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$100(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$000(Lcom/sec/android/app/mv/player/view/VideoFlickView;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    rem-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftArrayListImageView:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$100(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$000(Lcom/sec/android/app/mv/player/view/VideoFlickView;)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    rem-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # operator++ for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$008(Lcom/sec/android/app/mv/player/view/VideoFlickView;)I

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$000(Lcom/sec/android/app/mv/player/view/VideoFlickView;)I

    move-result v0

    if-gt v0, v2, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$200(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$200(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 105
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightArrayListImageView:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$300(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$000(Lcom/sec/android/app/mv/player/view/VideoFlickView;)I

    move-result v1

    rem-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0200a1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightArrayListImageView:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$300(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$000(Lcom/sec/android/app/mv/player/view/VideoFlickView;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    rem-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightArrayListImageView:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$300(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$000(Lcom/sec/android/app/mv/player/view/VideoFlickView;)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    rem-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # operator++ for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$008(Lcom/sec/android/app/mv/player/view/VideoFlickView;)I

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$000(Lcom/sec/android/app/mv/player/view/VideoFlickView;)I

    move-result v0

    if-gt v0, v2, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$200(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$200(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
