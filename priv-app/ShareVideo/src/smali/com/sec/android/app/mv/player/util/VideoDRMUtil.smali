.class public Lcom/sec/android/app/mv/player/util/VideoDRMUtil;
.super Ljava/lang/Object;
.source "VideoDRMUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;
    }
.end annotation


# static fields
.field private static final DRM_DIVX_MODE_ACTIVATE_RENTAL:I = 0x4

.field private static final DRM_DIVX_MODE_RENTAL:I = 0x2

.field public static final RIGHTS_EXPIRED:I = 0x2

.field public static final RIGHTS_INVALID:I = 0x1

.field public static final RIGHTS_NOT_ACQUIRED:I = 0x3

.field public static final RIGHTS_VALID:I = 0x0

.field public static final VIDEO_DRM_CONSTRAINTS_ACCUMULATED:I = 0xf

.field public static final VIDEO_DRM_CONSTRAINTS_COUNT:I = 0xb

.field public static final VIDEO_DRM_CONSTRAINTS_DATETIME:I = 0xd

.field public static final VIDEO_DRM_CONSTRAINTS_INDIVIDUAL:I = 0x10

.field public static final VIDEO_DRM_CONSTRAINTS_INTERVAL:I = 0xc

.field public static final VIDEO_DRM_CONSTRAINTS_NONE:I = 0xa

.field public static final VIDEO_DRM_CONSTRAINTS_TIMED_COUNT:I = 0xe

.field public static final VIDEO_DRM_CONSTRAINTS_UNLIMITED:I = 0x11

.field public static final VIDEO_DRM_DIVX:I = 0x4

.field public static final VIDEO_DRM_NEXTI_PV_PLAYREADY:I = 0x6

.field public static final VIDEO_DRM_OMADRM:I = 0x1

.field public static final VIDEO_DRM_PRDRM:I = 0x2

.field public static final VIDEO_DRM_SDRM:I = 0x5

.field public static final VIDEO_DRM_STR_DIVX_NOT_AUTHORIZED:I = 0x24

.field public static final VIDEO_DRM_STR_DIVX_NOT_REGISTERED:I = 0x25

.field public static final VIDEO_DRM_STR_DIVX_RENTAL_BACK_KEY:I = 0x28

.field public static final VIDEO_DRM_STR_DIVX_RENTAL_EXPIRED:I = 0x26

.field public static final VIDEO_DRM_STR_DIVX_RENTAL_INFO:I = 0x27

.field public static final VIDEO_DRM_STR_FIRST_INTERVAL_RENDER:I = 0x22

.field public static final VIDEO_DRM_STR_INVALID_CD:I = 0x20

.field public static final VIDEO_DRM_STR_INVALID_SD:I = 0x21

.field public static final VIDEO_DRM_STR_INVALID_WMDRM:I = 0x23

.field public static final VIDEO_DRM_STR_NULL:I = 0x1e

.field public static final VIDEO_DRM_STR_VALID_COUNT:I = 0x1f

.field public static final VIDEO_DRM_TYPE_CD:I = 0x1

.field public static final VIDEO_DRM_TYPE_FL:I = 0x0

.field public static final VIDEO_DRM_TYPE_SD:I = 0x2

.field public static final VIDEO_DRM_TYPE_SSD:I = 0x3

.field public static final VIDEO_DRM_WMDRM:I = 0x3

.field public static final VIDEO_NON_DRM:I = -0x1

.field private static mInstance:Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

.field public static mPath:Ljava/lang/String;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDrmClient:Landroid/drm/DrmManagerClient;

.field private mDrmType:I

.field private mOMADrmConstraint:[Ljava/lang/String;

.field private mOMADrmConstraintType:I

.field private mOMADrmDelivery:[Ljava/lang/String;

.field private mOMADrmDeliveryType:I

.field private mRightStatus:I

.field private mRightStatusString:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "VideoDRMUtil"

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 78
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraintType:I

    .line 79
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmType:I

    .line 80
    iput v4, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mRightStatus:I

    .line 81
    iput v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    .line 84
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v3

    const-string v1, "Count"

    aput-object v1, v0, v4

    const-string v1, "Interval"

    aput-object v1, v0, v5

    const-string v1, "DateTime"

    aput-object v1, v0, v6

    const-string v1, "Timed Count"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Accumulated"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Individual"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Unlimited"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraint:[Ljava/lang/String;

    .line 85
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "VIDEO_DRM_TYPE_FL"

    aput-object v1, v0, v3

    const-string v1, "VIDEO_DRM_TYPE_CD"

    aput-object v1, v0, v4

    const-string v1, "VIDEO_DRM_TYPE_SD"

    aput-object v1, v0, v5

    const-string v1, "VIDEO_DRM_TYPE_SSD"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmDelivery:[Ljava/lang/String;

    .line 86
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "RIGHTS_VALID"

    aput-object v1, v0, v3

    const-string v1, "RIGHTS_INVALID"

    aput-object v1, v0, v4

    const-string v1, "RIGHTS_EXPIRED"

    aput-object v1, v0, v5

    const-string v1, "RIGHTS_NOT_ACQUIRED"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mRightStatusString:[Ljava/lang/String;

    .line 100
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    .line 101
    new-instance v0, Landroid/drm/DrmManagerClient;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 102
    return-void
.end method

.method public static declared-synchronized createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/util/VideoDRMUtil;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 105
    const-class v1, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mInstance:Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    if-nez v0, :cond_0

    .line 106
    new-instance v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mInstance:Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    .line 108
    :cond_0
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mInstance:Lcom/sec/android/app/mv/player/util/VideoDRMUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getOMARemainedCnt(Ljava/lang/String;)I
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 449
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 451
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v5, p1, v6}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v3

    .line 452
    .local v3, "rightDetails":Landroid/content/ContentValues;
    const-string v5, "license_category"

    invoke-virtual {v3, v5}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 453
    .local v1, "licenseCategory":Ljava/lang/String;
    const-string v4, ""

    .line 454
    .local v4, "validityRemainingCount":Ljava/lang/String;
    const/4 v2, 0x0

    .line 456
    .local v2, "remainCnt":I
    if-eqz v1, :cond_0

    .line 457
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 459
    .local v0, "license":I
    if-ne v0, v6, :cond_0

    .line 460
    const-string v5, "remaining_repeat_count"

    invoke-virtual {v3, v5}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 462
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getOMARemainedCnt. validityRemainingCount = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    if-eqz v4, :cond_0

    .line 465
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 469
    .end local v0    # "license":I
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getOMARemainedCnt. remainCnt = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    return v2
.end method

.method private initDrmMgrClient()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-nez v0, :cond_0

    .line 130
    new-instance v0, Landroid/drm/DrmManagerClient;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 131
    :cond_0
    return-void
.end method


# virtual methods
.method public checkIsNetworkConnected()Z
    .locals 3

    .prologue
    .line 1012
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1013
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDataState()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->checkIsWifiEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1014
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v2, "checkIsNetworkConnected() NO Connected"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    const/4 v1, 0x0

    .line 1018
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public checkIsShare(Landroid/net/Uri;)Z
    .locals 13
    .param p1, "videoUri"    # Landroid/net/Uri;

    .prologue
    const/4 v9, 0x1

    .line 342
    iget-object v10, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/sec/android/app/mv/player/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 343
    .local v5, "filePath":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v11, "checkIsShare()"

    invoke-static {v10, v11}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    if-nez v5, :cond_0

    .line 346
    iget-object v10, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v11, "checkIsShare() - file path is null."

    invoke-static {v10, v11}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 394
    :goto_0
    return v0

    .line 350
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 352
    invoke-virtual {p0, v5}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 353
    .local v7, "mimeType":Ljava/lang/String;
    const/4 v4, -0x1

    .line 354
    .local v4, "drmType":I
    const/4 v0, 0x1

    .line 356
    .local v0, "bResult":Z
    if-eqz v7, :cond_1

    .line 357
    iget-object v10, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v10, v5, v7}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 359
    .local v6, "isDrmSupported":Z
    if-eqz v6, :cond_3

    .line 360
    invoke-virtual {p0, v5}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getFileType(Ljava/lang/String;)I

    move-result v4

    .line 361
    iget-object v10, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "checkIsShare. drmFileType = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    .end local v6    # "isDrmSupported":Z
    :cond_1
    :goto_1
    if-ne v4, v9, :cond_5

    .line 369
    new-instance v3, Landroid/drm/DrmInfoRequest;

    const/16 v9, 0x10

    const-string v10, "application/vnd.oma.drm.content"

    invoke-direct {v3, v9, v10}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 371
    .local v3, "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    const-string v9, "drm_path"

    invoke-virtual {v3, v9, v5}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 372
    iget-object v9, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v9, v3}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v2

    .line 373
    .local v2, "drmInfo":Landroid/drm/DrmInfo;
    const-string v9, "bSendAs"

    invoke-virtual {v2, v9}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    .line 375
    .local v8, "shareObj":Ljava/lang/Object;
    if-eqz v8, :cond_2

    .line 376
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 378
    .local v1, "bSendAs":Ljava/lang/String;
    const-string v9, "1"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 379
    const/4 v0, 0x1

    .line 384
    :goto_2
    iget-object v9, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkIsShare. bSendAs = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    .end local v1    # "bSendAs":Ljava/lang/String;
    .end local v2    # "drmInfo":Landroid/drm/DrmInfo;
    .end local v3    # "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    .end local v8    # "shareObj":Ljava/lang/Object;
    :cond_2
    :goto_3
    iget-object v9, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkIsShare. bResult = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 363
    .restart local v6    # "isDrmSupported":Z
    :cond_3
    iget-object v10, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v11, "checkIsShare. canHandle returned false. Not a drm file by extension"

    invoke-static {v10, v11}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 381
    .end local v6    # "isDrmSupported":Z
    .restart local v1    # "bSendAs":Ljava/lang/String;
    .restart local v2    # "drmInfo":Landroid/drm/DrmInfo;
    .restart local v3    # "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    .restart local v8    # "shareObj":Ljava/lang/Object;
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 386
    .end local v1    # "bSendAs":Ljava/lang/String;
    .end local v2    # "drmInfo":Landroid/drm/DrmInfo;
    .end local v3    # "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    .end local v8    # "shareObj":Ljava/lang/Object;
    :cond_5
    const/4 v9, 0x2

    if-eq v4, v9, :cond_6

    const/4 v9, 0x5

    if-ne v4, v9, :cond_7

    .line 387
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 388
    :cond_7
    const/4 v9, 0x4

    if-ne v4, v9, :cond_2

    .line 389
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public checkIsShare(Ljava/lang/String;)Z
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 291
    iget-object v8, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v9, "checkIsShare()"

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 295
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 296
    .local v6, "mimeType":Ljava/lang/String;
    const/4 v4, -0x1

    .line 297
    .local v4, "drmType":I
    const/4 v0, 0x1

    .line 299
    .local v0, "bResult":Z
    if-eqz v6, :cond_0

    .line 300
    iget-object v8, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v8, p1, v6}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    .line 302
    .local v5, "isDrmSupported":Z
    if-eqz v5, :cond_2

    .line 303
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getFileType(Ljava/lang/String;)I

    move-result v4

    .line 304
    iget-object v8, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkIsShare. drmFileType = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    .end local v5    # "isDrmSupported":Z
    :cond_0
    :goto_0
    const/4 v8, 0x1

    if-ne v4, v8, :cond_4

    .line 312
    new-instance v3, Landroid/drm/DrmInfoRequest;

    const/16 v8, 0x10

    const-string v9, "application/vnd.oma.drm.content"

    invoke-direct {v3, v8, v9}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 314
    .local v3, "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    const-string v8, "drm_path"

    invoke-virtual {v3, v8, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 315
    iget-object v8, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v8, v3}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v2

    .line 316
    .local v2, "drmInfo":Landroid/drm/DrmInfo;
    const-string v8, "bSendAs"

    invoke-virtual {v2, v8}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 318
    .local v7, "shareObj":Ljava/lang/Object;
    if-eqz v7, :cond_1

    .line 319
    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 321
    .local v1, "bSendAs":Ljava/lang/String;
    const-string v8, "1"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 322
    const/4 v0, 0x1

    .line 327
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkIsShare. bSendAs = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    .end local v1    # "bSendAs":Ljava/lang/String;
    .end local v2    # "drmInfo":Landroid/drm/DrmInfo;
    .end local v3    # "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    .end local v7    # "shareObj":Ljava/lang/Object;
    :cond_1
    :goto_2
    iget-object v8, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkIsShare. bResult = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    return v0

    .line 306
    .restart local v5    # "isDrmSupported":Z
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v9, "checkIsShare. canHandle returned false. Not a drm file by extension"

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 324
    .end local v5    # "isDrmSupported":Z
    .restart local v1    # "bSendAs":Ljava/lang/String;
    .restart local v2    # "drmInfo":Landroid/drm/DrmInfo;
    .restart local v3    # "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    .restart local v7    # "shareObj":Ljava/lang/Object;
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 329
    .end local v1    # "bSendAs":Ljava/lang/String;
    .end local v2    # "drmInfo":Landroid/drm/DrmInfo;
    .end local v3    # "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    .end local v7    # "shareObj":Ljava/lang/Object;
    :cond_4
    const/4 v8, 0x2

    if-eq v4, v8, :cond_5

    const/4 v8, 0x5

    if-ne v4, v8, :cond_6

    .line 330
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 331
    :cond_6
    const/4 v8, 0x4

    if-ne v4, v8, :cond_7

    .line 332
    const/4 v0, 0x0

    goto :goto_2

    .line 333
    :cond_7
    const/4 v8, 0x6

    if-ne v4, v8, :cond_1

    .line 334
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public checkIsWMDRM(Ljava/lang/String;)Z
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 475
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 477
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 478
    .local v3, "mimeType":Ljava/lang/String;
    const/4 v0, -0x1

    .line 479
    .local v0, "fileType":I
    const/4 v1, 0x0

    .line 480
    .local v1, "isDrmSupported":Z
    const/4 v2, 0x0

    .line 482
    .local v2, "isPlayready":Z
    if-eqz v3, :cond_1

    .line 483
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v4, p1, v3}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 484
    if-eqz v1, :cond_1

    .line 485
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getFileType(Ljava/lang/String;)I

    move-result v0

    .line 486
    const/4 v4, 0x2

    if-eq v0, v4, :cond_0

    const/4 v4, 0x5

    if-eq v0, v4, :cond_0

    sget-boolean v4, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_WMDRM_NOT_SUPPORT:Z

    if-nez v4, :cond_1

    const/4 v4, 0x3

    if-ne v0, v4, :cond_1

    .line 487
    :cond_0
    const/4 v2, 0x1

    .line 492
    :cond_1
    return v2
.end method

.method public checkIsWifiEnabled()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1024
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const-string v4, "wifi"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 1025
    .local v0, "mWifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1026
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v4, "checkIsWifiEnabled, Wifi is Not Enabled"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1035
    :goto_0
    return v2

    .line 1029
    :cond_0
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    .line 1030
    .local v1, "wifiInfo":Landroid/net/wifi/WifiInfo;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v3

    if-eqz v3, :cond_1

    .line 1031
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v3, "checkIsWifiEnabled Wifi is Connected"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1032
    const/4 v2, 0x1

    goto :goto_0

    .line 1034
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v4, "checkIsWifiEnabled Wifi is Enabled, but not connected"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public checkRightsStatus(Ljava/lang/String;)I
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 176
    if-nez p1, :cond_0

    .line 177
    const/4 v1, 0x1

    .line 206
    :goto_0
    return v1

    .line 179
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 181
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v2, p1}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v0

    .line 182
    .local v0, "LicenseStatus":I
    const/4 v1, 0x1

    .line 183
    .local v1, "rightStatus":I
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkRightsStatus. LicenseStatus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    packed-switch v0, :pswitch_data_0

    .line 200
    :pswitch_0
    const/4 v1, 0x1

    .line 204
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkRightsStatus. rightStatus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mRightStatusString:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    iput v1, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mRightStatus:I

    goto :goto_0

    .line 187
    :pswitch_1
    const/4 v1, 0x2

    .line 188
    goto :goto_1

    .line 191
    :pswitch_2
    const/4 v1, 0x3

    .line 192
    goto :goto_1

    .line 195
    :pswitch_3
    const/4 v1, 0x0

    .line 196
    goto :goto_1

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public divxPopupType(ILjava/lang/String;)I
    .locals 8
    .param p1, "rightStatus"    # I
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 598
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "divxPopupType() rightStatus: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    const/16 v2, 0x1e

    .line 601
    .local v2, "result":I
    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    move v3, v2

    .line 643
    .end local v2    # "result":I
    .local v3, "result":I
    :goto_1
    return v3

    .line 603
    .end local v3    # "result":I
    .restart local v2    # "result":I
    :pswitch_1
    const/16 v2, 0x24

    .line 604
    goto :goto_0

    .line 609
    :pswitch_2
    const/16 v2, 0x26

    .line 611
    goto :goto_0

    .line 613
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 614
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v6, 0x1

    invoke-virtual {v5, p2, v6}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v4

    .line 615
    .local v4, "rightDetails":Landroid/content/ContentValues;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "divxPopupType. rightDetails = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    if-eqz v4, :cond_1

    const-string v5, "license_category"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_2

    .line 617
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v6, "divxPopupType. null check return"

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 618
    .end local v2    # "result":I
    .restart local v3    # "result":I
    goto :goto_1

    .line 620
    .end local v3    # "result":I
    .restart local v2    # "result":I
    :cond_2
    const-string v5, "license_category"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 622
    .local v1, "licenseCategory":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "divxPopupType. licenseCategory = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    if-eqz v1, :cond_0

    .line 625
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 626
    .local v0, "license":I
    const/4 v5, 0x2

    if-eq v0, v5, :cond_3

    const/4 v5, 0x4

    if-ne v0, v5, :cond_5

    .line 627
    :cond_3
    invoke-virtual {p0, p2}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getDivxtotalCnt(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_4

    .line 628
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v6, "divxPopupType. licenseCategory VIDEO_DRM_STR_NULL "

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    const/16 v2, 0x1e

    goto :goto_0

    .line 631
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v6, "divxPopupType. licenseCategory VIDEO_DRM_STR_DIVX_RENTAL_INFO "

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    const/16 v2, 0x27

    goto/16 :goto_0

    .line 635
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v6, "divxPopupType. licenseCategory VIDEO_DRM_STR_NULL "

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    const/16 v2, 0x1e

    goto/16 :goto_0

    .line 601
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public getDetailInfo(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 19
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 794
    if-nez p1, :cond_1

    .line 795
    const/4 v3, 0x0

    .line 1000
    :cond_0
    :goto_0
    return-object v3

    .line 797
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 799
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 800
    .local v3, "detailInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;>;"
    const/4 v6, 0x0

    .line 802
    .local v6, "info":Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v14, v0, v15}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v11

    .line 804
    .local v11, "rightDetails":Landroid/content/ContentValues;
    if-nez v11, :cond_2

    .line 805
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v15, "getDetailInfo. rightDetails is null"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    const/4 v3, 0x0

    goto :goto_0

    .line 809
    :cond_2
    const-string v14, "license_category"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    .line 811
    .local v8, "object":Ljava/lang/Object;
    if-nez v8, :cond_3

    .line 812
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v15, "getDetailInfo() rightDetails is null"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    const/4 v3, 0x0

    goto :goto_0

    .line 816
    :cond_3
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 817
    .local v7, "licenseCategory":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getDetailInfo. licenseCategory = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    if-eqz v7, :cond_4

    const-string v14, "NOT_FOUND_FOR_ACTION_SPECIFIED"

    invoke-virtual {v7, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 820
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v15, "getDetailInfo. licenseCategory is null"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 821
    const/4 v3, 0x0

    goto :goto_0

    .line 824
    :cond_5
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmType:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_6

    .line 825
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 826
    .local v1, "categoryType":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getDetailInfo. categoryType : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    new-instance v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;

    .end local v6    # "info":Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;
    invoke-direct {v6}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;-><init>()V

    .line 830
    .restart local v6    # "info":Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;
    const/4 v14, 0x1

    iput v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionType:I

    .line 831
    iput v1, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->constraintType:I

    .line 833
    iget v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionType:I

    sparse-switch v14, :sswitch_data_0

    .line 844
    const v14, 0x7f0a0093

    iput v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    .line 848
    :goto_1
    sparse-switch v1, :sswitch_data_1

    .line 892
    :goto_2
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getDetailInfo. info.typeStr[0] = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aget v16, v16, v17

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getDetailInfo. info.validityStr = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 835
    :sswitch_1
    const v14, 0x7f0a00b6

    iput v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    goto :goto_1

    .line 838
    :sswitch_2
    const v14, 0x7f0a00b4

    iput v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    goto :goto_1

    .line 841
    :sswitch_3
    const v14, 0x7f0a00b5

    iput v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    goto :goto_1

    .line 850
    :sswitch_4
    iget-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a00b3

    aput v16, v14, v15

    goto :goto_2

    .line 854
    :sswitch_5
    const-string v14, "remaining_repeat_count"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 855
    .local v2, "curCount":Ljava/lang/String;
    const-string v14, "max_repeat_count"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    .line 857
    .local v9, "orgCount":Ljava/lang/String;
    iget-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a00ae

    aput v16, v14, v15

    .line 858
    const-string v14, "%d/%d"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    goto/16 :goto_2

    .line 862
    .end local v2    # "curCount":Ljava/lang/String;
    .end local v9    # "orgCount":Ljava/lang/String;
    :sswitch_6
    const-string v14, "license_start_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    .line 863
    .local v13, "startTime":Ljava/lang/String;
    const-string v14, "license_expiry_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 865
    .local v5, "endTime":Ljava/lang/String;
    iget-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a00b1

    aput v16, v14, v15

    .line 866
    const-string v14, "%s - %s"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v13, v15, v16

    const/16 v16, 0x1

    aput-object v5, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    goto/16 :goto_2

    .line 870
    .end local v5    # "endTime":Ljava/lang/String;
    .end local v13    # "startTime":Ljava/lang/String;
    :sswitch_7
    const-string v14, "license_available_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 872
    .local v10, "remainInterval":Ljava/lang/String;
    iget-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a00b0

    aput v16, v14, v15

    .line 873
    const-string v14, "%s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v10, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    goto/16 :goto_2

    .line 877
    .end local v10    # "remainInterval":Ljava/lang/String;
    :sswitch_8
    iget-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a00b1

    aput v16, v14, v15

    .line 878
    iget-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x1

    const v16, 0x7f0a00ae

    aput v16, v14, v15

    goto/16 :goto_2

    .line 896
    .end local v1    # "categoryType":I
    :cond_6
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmType:I

    const/4 v15, 0x2

    if-eq v14, v15, :cond_7

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmType:I

    const/4 v15, 0x5

    if-ne v14, v15, :cond_0

    .line 897
    :cond_7
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 899
    .restart local v1    # "categoryType":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getDetailInfo. categoryType : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    new-instance v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;

    .end local v6    # "info":Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;
    invoke-direct {v6}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;-><init>()V

    .line 903
    .restart local v6    # "info":Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;
    const/4 v14, 0x1

    iput v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionType:I

    .line 904
    iput v1, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->constraintType:I

    .line 906
    iget v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionType:I

    sparse-switch v14, :sswitch_data_2

    .line 917
    const v14, 0x7f0a0093

    iput v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    .line 921
    :goto_3
    const/4 v12, 0x0

    .line 922
    .local v12, "startDate":Ljava/lang/String;
    const/4 v4, 0x0

    .line 923
    .local v4, "endDate":Ljava/lang/String;
    const/4 v2, 0x0

    .line 925
    .restart local v2    # "curCount":Ljava/lang/String;
    packed-switch v1, :pswitch_data_0

    .line 997
    :goto_4
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 908
    .end local v2    # "curCount":Ljava/lang/String;
    .end local v4    # "endDate":Ljava/lang/String;
    .end local v12    # "startDate":Ljava/lang/String;
    :sswitch_9
    const v14, 0x7f0a00b6

    iput v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    goto :goto_3

    .line 911
    :sswitch_a
    const v14, 0x7f0a00b4

    iput v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    goto :goto_3

    .line 914
    :sswitch_b
    const v14, 0x7f0a00b5

    iput v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    goto :goto_3

    .line 927
    .restart local v2    # "curCount":Ljava/lang/String;
    .restart local v4    # "endDate":Ljava/lang/String;
    .restart local v12    # "startDate":Ljava/lang/String;
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v15, 0x7f0a00b3

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    goto :goto_4

    .line 931
    :pswitch_1
    const-string v14, "license_start_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 933
    const-string v14, "%s %s"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0064

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v12, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 934
    iget-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a00af

    aput v16, v14, v15

    goto :goto_4

    .line 938
    :pswitch_2
    const-string v14, "remaining_repeat_count"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 940
    const-string v14, "%s %d %s"

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0063

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0092

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 942
    iget-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a00ae

    aput v16, v14, v15

    goto/16 :goto_4

    .line 946
    :pswitch_3
    const-string v14, "license_expiry_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 948
    const-string v14, "%s %s"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0065

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v4, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 949
    iget-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a00af

    aput v16, v14, v15

    goto/16 :goto_4

    .line 953
    :pswitch_4
    const-string v14, "license_start_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 954
    const-string v14, "license_expiry_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 956
    const-string v14, "%s %s-%s "

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0064

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v12, v15, v16

    const/16 v16, 0x2

    aput-object v4, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 957
    iget-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a00af

    aput v16, v14, v15

    goto/16 :goto_4

    .line 961
    :pswitch_5
    const-string v14, "license_start_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 962
    const-string v14, "remaining_repeat_count"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 964
    const-string v14, "%s %s %d %s"

    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0064

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v12, v15, v16

    const/16 v16, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0092

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 966
    iget-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a00ae

    aput v16, v14, v15

    goto/16 :goto_4

    .line 970
    :pswitch_6
    const-string v14, "license_expiry_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 971
    const-string v14, "remaining_repeat_count"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 973
    const-string v14, "%s %s %d %s"

    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0065

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v4, v15, v16

    const/16 v16, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0092

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 975
    iget-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a00ae

    aput v16, v14, v15

    goto/16 :goto_4

    .line 979
    :pswitch_7
    const-string v14, "license_start_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 980
    const-string v14, "license_expiry_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 981
    const-string v14, "remaining_repeat_count"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 983
    const-string v14, "%s %s-%s %d %s"

    const/4 v15, 0x5

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0064

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v12, v15, v16

    const/16 v16, 0x2

    aput-object v4, v15, v16

    const/16 v16, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0092

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 985
    iget-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a00ae

    aput v16, v14, v15

    goto/16 :goto_4

    .line 989
    :pswitch_8
    iget-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a0002

    aput v16, v14, v15

    .line 990
    const/4 v14, 0x0

    iput-object v14, v6, Lcom/sec/android/app/mv/player/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    goto/16 :goto_4

    .line 833
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x6 -> :sswitch_3
        0x7 -> :sswitch_2
    .end sparse-switch

    .line 848
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_4
        0x1 -> :sswitch_5
        0x2 -> :sswitch_6
        0x4 -> :sswitch_7
        0x8 -> :sswitch_8
        0x10 -> :sswitch_0
        0x20 -> :sswitch_0
    .end sparse-switch

    .line 906
    :sswitch_data_2
    .sparse-switch
        0x1 -> :sswitch_9
        0x6 -> :sswitch_b
        0x7 -> :sswitch_a
    .end sparse-switch

    .line 925
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public getDivxcurCnt(Ljava/lang/String;)I
    .locals 9
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 647
    const/4 v3, -0x1

    .line 648
    .local v3, "remainedCnt":I
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 649
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v7, 0x1

    invoke-virtual {v6, p1, v7}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v5

    .line 650
    .local v5, "rightDetails":Landroid/content/ContentValues;
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDivxcurCnt. rightDetails = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    if-eqz v5, :cond_0

    const-string v6, "license_category"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_1

    .line 652
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v7, "getDivxcurCnt. return;"

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v3

    .line 667
    .end local v3    # "remainedCnt":I
    .local v4, "remainedCnt":I
    :goto_0
    return v4

    .line 655
    .end local v4    # "remainedCnt":I
    .restart local v3    # "remainedCnt":I
    :cond_1
    const-string v6, "license_category"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 656
    .local v2, "licenseCategory":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDivxcurCnt. licenseCategory = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    if-eqz v2, :cond_3

    .line 659
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 660
    .local v1, "license":I
    const/4 v6, 0x2

    if-eq v1, v6, :cond_2

    const/4 v6, 0x4

    if-ne v1, v6, :cond_3

    .line 661
    :cond_2
    const-string v6, "remaining_repeat_count"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 662
    .local v0, "divxRemainingCount":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 663
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDivxcurCnt() - remained count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .end local v0    # "divxRemainingCount":Ljava/lang/String;
    .end local v1    # "license":I
    :cond_3
    move v4, v3

    .line 667
    .end local v3    # "remainedCnt":I
    .restart local v4    # "remainedCnt":I
    goto :goto_0
.end method

.method public getDivxtotalCnt(Ljava/lang/String;)I
    .locals 9
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 671
    const/4 v3, -0x1

    .line 672
    .local v3, "orgCount":I
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 673
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v7, 0x1

    invoke-virtual {v6, p1, v7}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v5

    .line 674
    .local v5, "rightDetails":Landroid/content/ContentValues;
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDivxtotalCnt. rightDetails: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    if-eqz v5, :cond_0

    const-string v6, "license_category"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_1

    .line 676
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v7, "getDivxtotalCnt. return;"

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v3

    .line 692
    .end local v3    # "orgCount":I
    .local v4, "orgCount":I
    :goto_0
    return v4

    .line 679
    .end local v4    # "orgCount":I
    .restart local v3    # "orgCount":I
    :cond_1
    const-string v6, "license_category"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 680
    .local v2, "licenseCategory":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDivxtotalCnt. licenseCategory = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    if-eqz v2, :cond_3

    .line 683
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v7, "getDivxtotalCnt() licenseCategory is not null"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 685
    .local v1, "license":I
    const/4 v6, 0x2

    if-eq v1, v6, :cond_2

    const/4 v6, 0x4

    if-ne v1, v6, :cond_3

    .line 686
    :cond_2
    const-string v6, "max_repeat_count"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 687
    .local v0, "divxOriginalCount":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 688
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDivxtotalCnt() - original count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .end local v0    # "divxOriginalCount":Ljava/lang/String;
    .end local v1    # "license":I
    :cond_3
    move v4, v3

    .line 692
    .end local v3    # "orgCount":I
    .restart local v4    # "orgCount":I
    goto :goto_0
.end method

.method public getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "drmFilename"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 696
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDrmMimeType : drmFilename => "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    if-eqz p1, :cond_0

    .line 698
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".dcf"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 699
    const-string v0, "application/vnd.oma.drm.content"

    .line 723
    :cond_0
    :goto_0
    return-object v0

    .line 700
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".avi"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 701
    const-string v0, "video/mux/AVI"

    goto :goto_0

    .line 702
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".mkv"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 703
    const-string v0, "video/mux/MKV"

    goto :goto_0

    .line 704
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".divx"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 705
    const-string v0, "video/mux/DivX"

    goto :goto_0

    .line 706
    :cond_4
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".pyv"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 707
    const-string v0, "video/vnd.ms-playready.media.pyv"

    goto :goto_0

    .line 708
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".pya"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 709
    const-string v0, "audio/vnd.ms-playready.media.pya"

    goto :goto_0

    .line 710
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".wmv"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 711
    const-string v0, "video/x-ms-wmv"

    goto :goto_0

    .line 712
    :cond_7
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".wma"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 713
    const-string v0, "audio/x-ms-wma"

    goto :goto_0

    .line 714
    :cond_8
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".mp4"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 715
    const-string v0, "video/mp4"

    goto :goto_0

    .line 716
    :cond_9
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".sm4"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 717
    const-string v0, "video/vnd.sdrm-media.sm4"

    goto/16 :goto_0

    .line 718
    :cond_a
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".ismv"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 719
    const-string v0, "video/ismv"

    goto/16 :goto_0
.end method

.method public getDrmType()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmType:I

    return v0
.end method

.method public getFileType(Ljava/lang/String;)I
    .locals 4
    .param p1, "drmFilename"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x2

    const/4 v1, -0x1

    .line 730
    if-eqz p1, :cond_8

    .line 731
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".dcf"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 732
    const/4 v0, 0x1

    .line 748
    :cond_0
    :goto_0
    return v0

    .line 733
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".avi"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".mkv"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".divx"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 734
    :cond_2
    const/4 v0, 0x4

    goto :goto_0

    .line 735
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".pyv"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".pya"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 737
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".wmv"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".wma"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 738
    :cond_4
    const/4 v0, 0x3

    goto :goto_0

    .line 739
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".mp4"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 741
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, ".sm4"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 742
    const/4 v0, 0x5

    goto :goto_0

    .line 743
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, ".ismv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 744
    const/4 v0, 0x6

    goto :goto_0

    :cond_7
    move v0, v1

    .line 746
    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 748
    goto/16 :goto_0
.end method

.method public getPopupString(IZ)Ljava/lang/String;
    .locals 20
    .param p1, "strType"    # I
    .param p2, "fromAIA"    # Z

    .prologue
    .line 496
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v12, "getPopupString - start"

    invoke-static {v9, v12}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    const/4 v8, 0x0

    .line 498
    .local v8, "str":Ljava/lang/String;
    const/4 v4, 0x0

    .line 499
    .local v4, "fileName":Ljava/lang/String;
    const-wide/16 v6, 0x0

    .line 501
    .local v6, "remainedCnt":J
    const-wide/16 v10, -0x1

    .line 502
    .local v10, "totalCnt":J
    const-wide/16 v2, -0x1

    .line 504
    .local v2, "curCnt":J
    sget-object v9, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    const/16 v12, 0x2f

    invoke-virtual {v9, v12}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    .line 506
    .local v5, "lastIndex":I
    sget-object v9, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ge v5, v9, :cond_1

    .line 507
    sget-object v9, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    add-int/lit8 v12, v5, 0x1

    invoke-virtual {v9, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 511
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 594
    :cond_0
    :goto_1
    return-object v8

    .line 509
    :cond_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0093

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 513
    :pswitch_0
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmType:I

    const/4 v12, 0x1

    if-ne v9, v12, :cond_2

    .line 514
    sget-object v9, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getOMARemainedCnt(Ljava/lang/String;)I

    move-result v9

    int-to-long v6, v9

    .line 517
    :cond_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getPopupString() - remainedCnt = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    const-wide/16 v12, 0x1

    cmp-long v9, v6, v12

    if-nez v9, :cond_3

    .line 520
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00c8

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v4, v13, v14

    const/4 v14, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v9, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 521
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0024

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 522
    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 523
    if-nez p2, :cond_0

    .line 524
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00c5

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    .line 525
    :cond_3
    const-wide/16 v12, 0x2

    cmp-long v9, v6, v12

    if-nez v9, :cond_0

    .line 526
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00c9

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v4, v13, v14

    const/4 v14, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v9, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 527
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0024

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 528
    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 529
    if-nez p2, :cond_0

    .line 530
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00c5

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    .line 536
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00c6

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 537
    goto/16 :goto_1

    .line 540
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00c3

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 541
    goto/16 :goto_1

    .line 544
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00c4

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v4, v13, v14

    invoke-virtual {v9, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 545
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0024

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 546
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00c7

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 548
    goto/16 :goto_1

    .line 552
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00c2

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 553
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0024

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 554
    if-nez p2, :cond_0

    .line 555
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00c5

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    .line 560
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00bd

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 561
    goto/16 :goto_1

    .line 564
    :pswitch_6
    sget-object v9, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getDivxcurCnt(Ljava/lang/String;)I

    move-result v9

    int-to-long v2, v9

    .line 565
    sget-object v9, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getDivxtotalCnt(Ljava/lang/String;)I

    move-result v9

    int-to-long v10, v9

    .line 566
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getPopupString() - totalCnt : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", curCnt :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00c0

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v9, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 569
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0024

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 570
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00c1

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 571
    goto/16 :goto_1

    .line 574
    :pswitch_7
    sget-object v9, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getDivxcurCnt(Ljava/lang/String;)I

    move-result v9

    int-to-long v2, v9

    .line 575
    sget-object v9, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getDivxtotalCnt(Ljava/lang/String;)I

    move-result v9

    int-to-long v10, v9

    .line 576
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getPopupString() - totalCnt : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", curCnt :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00c0

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v9, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 578
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0024

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 579
    if-nez p2, :cond_0

    .line 580
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00bc

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    .line 584
    :pswitch_8
    sget-object v9, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getDivxcurCnt(Ljava/lang/String;)I

    move-result v9

    int-to-long v2, v9

    .line 585
    sget-object v9, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getDivxtotalCnt(Ljava/lang/String;)I

    move-result v9

    int-to-long v10, v9

    .line 586
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getPopupString() - totalCnt : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", curCnt :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a00bb

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    sub-long v16, v10, v2

    const-wide/16 v18, 0x1

    add-long v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v9, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 588
    goto/16 :goto_1

    .line 511
    :pswitch_data_0
    .packed-switch 0x1f
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public getRightStatus()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mRightStatus:I

    return v0
.end method

.method public getURLInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 753
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 755
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 757
    .local v3, "mimeType":Ljava/lang/String;
    new-instance v0, Landroid/drm/DrmInfoRequest;

    const/4 v4, 0x3

    invoke-direct {v0, v4, v3}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 758
    .local v0, "mDrmInfoRequest_ILA":Landroid/drm/DrmInfoRequest;
    const-string v4, "drm_path"

    invoke-virtual {v0, v4, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 759
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v4, v0}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v1

    .line 760
    .local v1, "mDrmInfo_ILA":Landroid/drm/DrmInfo;
    const-string v4, "URL"

    invoke-virtual {v1, v4}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 761
    .local v2, "mLicense_url":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getURLInfo. mLicense_url : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    return-object v2
.end method

.method public hasDrmConstrains(Ljava/lang/String;)Z
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 1041
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v2, p1, v1}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v0

    .line 1042
    .local v0, "values":Landroid/content/ContentValues;
    if-eqz v0, :cond_0

    .line 1045
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public initOMADrmConstraintsInfo(Ljava/lang/String;)Z
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x10

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 248
    if-nez p1, :cond_1

    .line 249
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v5, "initOMADrmConstraintsInfo() - file path is null."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    :cond_0
    :goto_0
    return v3

    .line 253
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 255
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v5, p1, v4}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v2

    .line 256
    .local v2, "rightDetails":Landroid/content/ContentValues;
    const-string v5, "license_category"

    invoke-virtual {v2, v5}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 257
    .local v1, "licenseCategory":Ljava/lang/String;
    const/4 v0, 0x0

    .line 259
    .local v0, "license":I
    if-eqz v1, :cond_0

    .line 261
    const-string v3, "NOT_FOUND_FOR_ACTION_SPECIFIED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 262
    const/4 v0, -0x1

    .line 266
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initOMADrmConstraintsInfo. licenseCategory = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    if-nez v0, :cond_3

    .line 269
    const/16 v3, 0x11

    iput v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraintType:I

    .line 286
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initOMADrmConstraintsInfo. mOMADrmConstraintType = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraint:[Ljava/lang/String;

    iget v7, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraintType:I

    add-int/lit8 v7, v7, -0xa

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 287
    goto :goto_0

    .line 264
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 270
    :cond_3
    if-ne v0, v4, :cond_4

    .line 271
    const/16 v3, 0xb

    iput v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraintType:I

    goto :goto_2

    .line 272
    :cond_4
    const/4 v3, 0x2

    if-ne v0, v3, :cond_5

    .line 273
    const/16 v3, 0xd

    iput v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraintType:I

    goto :goto_2

    .line 274
    :cond_5
    const/4 v3, 0x4

    if-ne v0, v3, :cond_6

    .line 275
    const/16 v3, 0xc

    iput v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraintType:I

    goto :goto_2

    .line 276
    :cond_6
    if-ne v0, v7, :cond_7

    .line 277
    const/16 v3, 0xf

    iput v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraintType:I

    goto :goto_2

    .line 278
    :cond_7
    const/16 v3, 0x20

    if-ne v0, v3, :cond_8

    .line 279
    iput v7, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraintType:I

    goto :goto_2

    .line 280
    :cond_8
    const/16 v3, 0x8

    if-ne v0, v3, :cond_9

    .line 281
    const/16 v3, 0xe

    iput v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraintType:I

    goto :goto_2

    .line 283
    :cond_9
    const/16 v3, 0xa

    iput v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraintType:I

    goto :goto_2
.end method

.method public initOMADrmDeliveryType(Ljava/lang/String;)V
    .locals 12
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 211
    if-nez p1, :cond_1

    .line 212
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v6, "initOMADrmDeliveryType() - file path is null."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 218
    new-instance v2, Landroid/drm/DrmInfoRequest;

    const/16 v5, 0xe

    const-string v6, "application/vnd.oma.drm.content"

    invoke-direct {v2, v5, v6}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 220
    .local v2, "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    const-string v5, "drm_path"

    invoke-virtual {v2, v5, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 221
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v5, v2}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v1

    .line 223
    .local v1, "drmInfo":Landroid/drm/DrmInfo;
    const-string v5, "version"

    invoke-virtual {v1, v5}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 224
    .local v4, "drmVersion":Ljava/lang/Object;
    const-string v5, "type"

    invoke-virtual {v1, v5}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 226
    .local v3, "drmType":Ljava/lang/Object;
    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    .line 229
    new-array v0, v10, [I

    .line 230
    .local v0, "DrmInfo":[I
    const-string v5, "version"

    invoke-virtual {v1, v5}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v0, v9

    .line 231
    const-string v5, "type"

    invoke-virtual {v1, v5}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v0, v8

    .line 232
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "initOMADrmDeliveryType. version = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v0, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", type = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v0, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    aget v5, v0, v8

    if-ne v5, v8, :cond_3

    .line 235
    iput v8, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    .line 243
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "initOMADrmDeliveryType. mOMADrmDeliveryType = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmDelivery:[Ljava/lang/String;

    iget v8, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 236
    :cond_3
    aget v5, v0, v8

    if-ne v5, v11, :cond_4

    .line 237
    iput v10, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    goto :goto_1

    .line 238
    :cond_4
    aget v5, v0, v8

    if-nez v5, :cond_5

    .line 239
    iput v9, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    goto :goto_1

    .line 240
    :cond_5
    aget v5, v0, v8

    if-ne v5, v10, :cond_2

    .line 241
    iput v11, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    goto :goto_1
.end method

.method public isDRM()Z
    .locals 2

    .prologue
    .line 164
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDrmFile(Ljava/lang/String;)I
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 134
    if-nez p1, :cond_0

    .line 135
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v4, "isDrmFile. filePath is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    const/4 v0, -0x1

    .line 160
    :goto_0
    return v0

    .line 139
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 141
    sput-object p1, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    .line 142
    const/4 v0, -0x1

    .line 144
    .local v0, "drmType":I
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 146
    .local v2, "mimeType":Ljava/lang/String;
    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-eqz v3, :cond_1

    .line 147
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v3, p1, v2}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 149
    .local v1, "isDrmSupported":Z
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isDrmFile. isDrmSupported = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    if-eqz v1, :cond_2

    .line 151
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->getFileType(Ljava/lang/String;)I

    move-result v0

    .line 152
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isDrmFile. drmFileType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    .end local v1    # "isDrmSupported":Z
    :cond_1
    :goto_1
    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmType:I

    .line 159
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isDrmFile. drmType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 154
    .restart local v1    # "isDrmSupported":Z
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v4, "isDrmFile. canHandle returned false. Not a drm file by extension"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public launchBrowser(Ljava/lang/String;)Z
    .locals 6
    .param p1, "mLicense_url"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 767
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v5, "launchBrowser() start "

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    if-nez p1, :cond_0

    .line 770
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v5, "launchBrowser. url is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    :goto_0
    return v3

    .line 774
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 775
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 776
    .local v0, "browserIntent":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 777
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 778
    .local v2, "mUri":Landroid/net/Uri;
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 780
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/high16 v5, 0x10000

    invoke-virtual {v4, v0, v5}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 783
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 790
    :cond_1
    const/4 v3, 0x1

    goto :goto_0

    .line 784
    :catch_0
    move-exception v1

    .line 785
    .local v1, "ex":Landroid/content/ActivityNotFoundException;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v5, "could not find a suitable activity for launching license url"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeDrmMgrClient()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "removeDrmClient"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0}, Landroid/drm/DrmManagerClient;->release()V

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 126
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 112
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    .line 114
    iput v1, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmType:I

    .line 115
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mRightStatus:I

    .line 116
    iput v1, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    .line 117
    iput v1, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraintType:I

    .line 118
    return-void
.end method

.method public setInvalidOMADrmMsg()I
    .locals 3

    .prologue
    .line 432
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInvalidOMADrmMsg. mOMADrmDeliveryType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    packed-switch v0, :pswitch_data_0

    .line 444
    const/16 v0, 0x1e

    :goto_0
    return v0

    .line 437
    :pswitch_0
    const/16 v0, 0x20

    goto :goto_0

    .line 441
    :pswitch_1
    const/16 v0, 0x21

    goto :goto_0

    .line 434
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public setValidOMADrmMsg(Ljava/lang/String;)I
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x1e

    .line 399
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 401
    iget-object v7, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v8, 0x1

    invoke-virtual {v7, p1, v8}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v2

    .line 402
    .local v2, "rightDetails":Landroid/content/ContentValues;
    const-string v7, "license_category"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 403
    .local v0, "licenseCategory":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setValidOMADrmMsg. licenseCategory = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    iget v7, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraintType:I

    const/16 v8, 0xb

    if-ne v7, v8, :cond_1

    .line 406
    const-string v7, "remaining_repeat_count"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 407
    .local v4, "validityRemainingCount":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 409
    .local v1, "remainedCnt":I
    iget-object v7, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setValidOMADrmMsg() - remained count : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const/4 v7, 0x2

    if-gt v1, v7, :cond_0

    .line 412
    const/16 v6, 0x1f

    .line 428
    .end local v1    # "remainedCnt":I
    .end local v4    # "validityRemainingCount":Ljava/lang/String;
    :cond_0
    :goto_0
    return v6

    .line 415
    :cond_1
    iget v7, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->mOMADrmConstraintType:I

    const/16 v8, 0xc

    if-ne v7, v8, :cond_0

    .line 416
    const-string v7, "license_original_interval"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 417
    .local v3, "validityOriginalInterval":Ljava/lang/String;
    const-string v7, "license_available_time"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 419
    .local v5, "validityRemainingInterval":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setValidOMADrmMsg() - validityOriginalInterval : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    iget-object v7, p0, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setValidOMADrmMsg() - validityRemainingInterval : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 423
    const/16 v6, 0x22

    goto :goto_0
.end method
