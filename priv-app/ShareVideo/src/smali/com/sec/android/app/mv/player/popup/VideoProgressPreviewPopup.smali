.class public Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
.super Ljava/lang/Object;
.source "VideoProgressPreviewPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$SeekHandler;,
        Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$HoverPopupHandler;,
        Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$HoverMsg;
    }
.end annotation


# static fields
.field private static final MSG_SEEK:I = 0x1

.field private static final TAG:Ljava/lang/String; = "VideoProgressPreviewPopup"

.field private static mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;


# instance fields
.field private FrameImage:Landroid/widget/ImageView;

.field private FrameImageArrow:Landroid/widget/ImageView;

.field private mArrowWidth:I

.field private mContext:Landroid/content/Context;

.field private mCurrentPosition:I

.field private mCurrentTime:Landroid/widget/TextView;

.field private mFormatBuilder:Ljava/lang/StringBuilder;

.field private mFormatter:Ljava/util/Formatter;

.field private mHandler:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$HoverPopupHandler;

.field private mIsInitialized:Z

.field private mMeasuredVideoHeight:I

.field private mMeasuredVideoWidth:I

.field private mPopupHeight:I

.field private mPopupWidth:I

.field private mProgressX:I

.field private mSHCallback:Landroid/view/SurfaceHolder$Callback;

.field private mSeekBarPadding:I

.field private mSeekHandler:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$SeekHandler;

.field private mSeekbarRect:Landroid/graphics/Rect;

.field private mStartPosition:I

.field private mSurfaceView:Landroid/view/SurfaceView;

.field private mUri:Landroid/net/Uri;

.field private mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

.field private mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

.field private mediaPlayer:Landroid/media/MediaPlayer;

.field private relative_layout:Landroid/widget/RelativeLayout;

.field private surfaceHolder:Landroid/view/SurfaceHolder;

.field private videoHeight:I

.field private videoPreviewDialog:Landroid/app/Dialog;

.field private videoWidth:I


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    .line 51
    iput v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoHeight:I

    .line 53
    iput v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mMeasuredVideoHeight:I

    .line 55
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 79
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    .line 81
    iput v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mProgressX:I

    .line 83
    iput v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    .line 85
    iput v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mPopupHeight:I

    .line 87
    iput v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mArrowWidth:I

    .line 89
    iput v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    .line 91
    iput v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentPosition:I

    .line 93
    iput v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mStartPosition:I

    .line 103
    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mIsInitialized:Z

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    .line 697
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$7;-><init>(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    .line 119
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)Lcom/sec/android/app/mv/player/db/VideoDB;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mStartPosition:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->reSetSurfaceViewImage()V

    return-void
.end method

.method static synthetic access$1302(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    .param p1, "x1"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->playVideo()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setAIAContext(Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->hideBoarders()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->initViews()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->show()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->dismiss()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    .param p1, "x1"    # Landroid/media/MediaPlayer;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentPosition:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mIsInitialized:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mIsInitialized:Z

    return p1
.end method

.method private currentTime(I)Ljava/lang/String;
    .locals 9
    .param p1, "timeMs"    # I

    .prologue
    const/4 v8, 0x0

    .line 822
    div-int/lit16 v3, p1, 0x3e8

    .line 823
    .local v3, "totalSeconds":I
    rem-int/lit8 v2, v3, 0x3c

    .line 824
    .local v2, "seconds":I
    div-int/lit8 v4, v3, 0x3c

    rem-int/lit8 v1, v4, 0x3c

    .line 825
    .local v1, "minutes":I
    div-int/lit16 v0, v3, 0xe10

    .line 827
    .local v0, "hours":I
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 828
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mFormatter:Ljava/util/Formatter;

    const-string v5, "%02d:%02d:%02d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private dismiss()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 554
    const-string v1, "VideoProgressPreviewPopup"

    const-string v2, "dismiss() "

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->isShow()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 557
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 558
    invoke-direct {p0, v4}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setAIAContext(Z)V

    .line 559
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 562
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 566
    :goto_0
    iput-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    .line 569
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_2

    .line 571
    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setAIAContext(Z)V

    .line 572
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 573
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 579
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v1, :cond_3

    .line 580
    iput-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 582
    :cond_3
    return-void

    .line 563
    :catch_0
    move-exception v0

    .line 564
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 574
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 575
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private extractData(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "dataType"    # I

    .prologue
    .line 585
    const-string v3, "VideoProgressPreviewPopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "extractData() type:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    if-nez p1, :cond_0

    .line 588
    const/4 v1, 0x0

    .line 607
    :goto_0
    return-object v1

    .line 592
    :cond_0
    const/4 v1, 0x0

    .line 595
    .local v1, "extracted":Ljava/lang/String;
    :try_start_0
    new-instance v2, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 596
    .local v2, "retriever":Landroid/media/MediaMetadataRetriever;
    invoke-virtual {v2, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 597
    invoke-virtual {v2, p2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    .line 598
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 599
    .end local v2    # "retriever":Landroid/media/MediaMetadataRetriever;
    :catch_0
    move-exception v0

    .line 600
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 601
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 602
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0

    .line 603
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_2
    move-exception v0

    .line 604
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getCheckRoation()I
    .locals 4

    .prologue
    .line 318
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x18

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->extractData(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 320
    .local v1, "rotation":Ljava/lang/String;
    if-eqz v1, :cond_0

    .end local v1    # "rotation":Ljava/lang/String;
    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 321
    .local v0, "m_VideoRotation":I
    sparse-switch v0, :sswitch_data_0

    .line 323
    const/4 v0, 0x0

    .line 325
    :goto_1
    return v0

    .line 320
    .end local v0    # "m_VideoRotation":I
    .restart local v1    # "rotation":Ljava/lang/String;
    :cond_0
    const-string v1, "0"

    goto :goto_0

    .line 322
    .end local v1    # "rotation":Ljava/lang/String;
    .restart local v0    # "m_VideoRotation":I
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_1

    .line 321
    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0x10e -> :sswitch_0
    .end sparse-switch
.end method

.method private getHandler()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 230
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "getHandler()"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mHandler:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$HoverPopupHandler;

    if-nez v0, :cond_0

    .line 233
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$HoverPopupHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$HoverPopupHandler;-><init>(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$1;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mHandler:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$HoverPopupHandler;

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mHandler:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$HoverPopupHandler;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    .locals 1

    .prologue
    .line 611
    sget-object v0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    if-nez v0, :cond_0

    .line 612
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .line 614
    :cond_0
    sget-object v0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    return-object v0
.end method

.method private getRawY(I)I
    .locals 3
    .param p1, "mY"    # I

    .prologue
    .line 340
    iget v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mPopupHeight:I

    sub-int v0, p1, v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private getSeekHandler()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 240
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "getSeekHandler()"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekHandler:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$SeekHandler;

    if-nez v0, :cond_0

    .line 243
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$SeekHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$SeekHandler;-><init>(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$1;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekHandler:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$SeekHandler;

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekHandler:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$SeekHandler;

    return-object v0
.end method

.method private getThumbnailBitmap(Landroid/net/Uri;J)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "durationTime"    # J

    .prologue
    .line 892
    const-string v3, "VideoProgressPreviewPopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getThumbnail - uri : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", durationTime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    const/4 v2, 0x0

    .line 894
    .local v2, "thumb":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 897
    .local v1, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 898
    iget v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mMeasuredVideoWidth:I

    iget v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mMeasuredVideoHeight:I

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/media/MediaMetadataRetriever;->setVideoSize(IIZZ)V

    .line 899
    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p2

    invoke-virtual {v1, v4, v5}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 900
    const-string v3, "VideoProgressPreviewPopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getThumbnail() tumb :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 905
    :try_start_1
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 910
    :goto_0
    return-object v2

    .line 906
    :catch_0
    move-exception v0

    .line 907
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v3, "VideoProgressPreviewPopup"

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 901
    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v0

    .line 902
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v3, "VideoProgressPreviewPopup"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 905
    :try_start_3
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 906
    :catch_2
    move-exception v0

    .line 907
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v3, "VideoProgressPreviewPopup"

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 904
    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    .line 905
    :try_start_4
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_3

    .line 908
    :goto_1
    throw v3

    .line 906
    :catch_3
    move-exception v0

    .line 907
    .restart local v0    # "ex":Ljava/lang/RuntimeException;
    const-string v4, "VideoProgressPreviewPopup"

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private hideBoarders()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 808
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 809
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 812
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 813
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 816
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 817
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 819
    :cond_2
    return-void
.end method

.method private initViews()V
    .locals 11

    .prologue
    const v10, 0x7f0801ba

    .line 261
    const-string v7, "VideoProgressPreviewPopup"

    const-string v8, "initView"

    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    if-eqz v7, :cond_0

    .line 263
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v7}, Landroid/app/Dialog;->dismiss()V

    .line 264
    :cond_0
    new-instance v7, Landroid/app/Dialog;

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    .line 266
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 267
    .local v0, "ImageViewParams":Landroid/widget/FrameLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mMeasuredVideoWidth:I

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0801b3

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0801b5

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 270
    iget v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mMeasuredVideoHeight:I

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0801b7

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 275
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0801aa

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 276
    .local v1, "addtional_margin":I
    iget v7, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    mul-int/lit8 v8, v1, 0x2

    add-int/2addr v7, v8

    iput v7, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 277
    iget v7, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    mul-int/lit8 v8, v1, 0x4

    add-int/2addr v7, v8

    iput v7, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 280
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 283
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    const v8, 0x7f0d00aa

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 284
    .local v4, "surface_timetext_layout":Landroid/widget/RelativeLayout;
    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 285
    .local v5, "surface_timetext_params":Landroid/widget/FrameLayout$LayoutParams;
    iget v7, v5, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v7, v1

    iput v7, v5, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 286
    iget v7, v5, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    add-int/2addr v7, v1

    iput v7, v5, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 287
    iget v7, v5, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v7, v1

    iput v7, v5, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 288
    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 291
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    const v8, 0x7f0d00ab

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/SurfaceView;

    iput-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSurfaceView:Landroid/view/SurfaceView;

    .line 292
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v7}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 293
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v7, v8}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 294
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v7}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 295
    .local v3, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mMeasuredVideoWidth:I

    iput v7, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 296
    iget v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mMeasuredVideoHeight:I

    iput v7, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 297
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v7, v3}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 299
    iget v7, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    .line 301
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setDialogProperties()V

    .line 302
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setDialogPosition()V

    .line 303
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v8}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 304
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v7}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 305
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v7, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v7, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 306
    iget v7, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    iget v8, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0801bd

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 310
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v7}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    .line 311
    .local v6, "window":Landroid/view/Window;
    invoke-virtual {v6, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 313
    iget v7, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    iput v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mPopupHeight:I

    .line 314
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0801ae

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mArrowWidth:I

    .line 315
    return-void
.end method

.method private isShow()Z
    .locals 1

    .prologue
    .line 546
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    .line 549
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private playVideo()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 425
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v1, :cond_1

    .line 426
    :cond_0
    const-string v1, "VideoProgressPreviewPopup"

    const-string v2, "openVideo() uri null or surfaceholder null"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    :goto_0
    return-void

    .line 430
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_2

    .line 431
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 432
    iput-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 435
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    if-nez v1, :cond_3

    .line 436
    new-instance v1, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    .line 439
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    if-nez v1, :cond_4

    .line 440
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/mv/player/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 445
    :cond_4
    :try_start_0
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 447
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$1;-><init>(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 455
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$2;-><init>(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 459
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$3;-><init>(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 464
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$4;-><init>(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 473
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$5;-><init>(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 483
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$6;-><init>(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 497
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnTimedTextListener(Landroid/media/MediaPlayer$OnTimedTextListener;)V

    .line 499
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    .line 500
    const-string v1, "VideoProgressPreviewPopup"

    const-string v2, "playVideo() :: mVideoDB.getFilePath(mUri) is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_0

    .line 520
    :catch_0
    move-exception v0

    .line 521
    .local v0, "ex":Ljava/io/IOException;
    const-string v1, "VideoProgressPreviewPopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IOException - Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 504
    .end local v0    # "ex":Ljava/io/IOException;
    :cond_5
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mIsInitialized:Z

    .line 505
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "content://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 506
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 512
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/16 v2, 0x79e

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setParameter(II)Z

    .line 513
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 514
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 515
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 516
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setAIAContext(Z)V

    .line 517
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 518
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->showBoarders()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_0

    .line 522
    :catch_1
    move-exception v0

    .line 523
    .local v0, "ex":Ljava/lang/IllegalStateException;
    const-string v1, "VideoProgressPreviewPopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IllegalStateException - Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 508
    .end local v0    # "ex":Ljava/lang/IllegalStateException;
    :cond_6
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 524
    :catch_2
    move-exception v0

    .line 525
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v1, "VideoProgressPreviewPopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IllegalArgumentException - Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method private reSetSurfaceViewImage()V
    .locals 2

    .prologue
    .line 925
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSurfaceView:Landroid/view/SurfaceView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 926
    return-void
.end method

.method private setAIAContext(Z)V
    .locals 3
    .param p1, "set"    # Z

    .prologue
    .line 883
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->setAIAContext(Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 889
    :goto_0
    return-void

    .line 884
    :catch_0
    move-exception v0

    .line 885
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "VideoProgressPreviewPopup"

    const-string v2, "setAIAContext - NullPointerException "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 886
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 887
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "VideoProgressPreviewPopup"

    const-string v2, "setAIAContext - IllegalStateException "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setDialogProperties()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 391
    return-void
.end method

.method private setSurfaceViewImage()V
    .locals 6

    .prologue
    .line 914
    const-string v2, "VideoProgressPreviewPopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSurfaceViewImage mCurrentPosition: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentPosition:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 915
    iget v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentPosition:I

    iput v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mStartPosition:I

    .line 917
    const/4 v1, 0x0

    .line 918
    .local v1, "imageDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v0, 0x0

    .line 919
    .local v0, "imageBitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    iget v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentPosition:I

    int-to-long v4, v3

    invoke-direct {p0, v2, v4, v5}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getThumbnailBitmap(Landroid/net/Uri;J)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 920
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "imageDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 921
    .restart local v1    # "imageDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v2, v1}, Landroid/view/SurfaceView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 922
    return-void
.end method

.method private show()V
    .locals 4

    .prologue
    .line 530
    iget v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoHeight:I

    if-nez v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoWidth:I

    if-eqz v1, :cond_1

    .line 533
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const-string v3, "FLAG_PRIVATE_NO_ANIMATION_WHEN_RESIZE_INCLUDE_CHILD"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/mv/player/common/VUtils;->setWindowFlag(Landroid/view/Window;Ljava/lang/String;)V

    .line 535
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/4 v2, 0x0

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 536
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_1

    .line 543
    :cond_1
    :goto_0
    return-void

    .line 537
    :catch_0
    move-exception v0

    .line 538
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 539
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 540
    .local v0, "e":Landroid/view/WindowManager$BadTokenException;
    invoke-virtual {v0}, Landroid/view/WindowManager$BadTokenException;->printStackTrace()V

    goto :goto_0
.end method

.method private showBoarders()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 790
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 794
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 795
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 798
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 799
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 802
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSurfaceView:Landroid/view/SurfaceView;

    if-eqz v0, :cond_3

    .line 803
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setSurfaceViewImage()V

    .line 805
    :cond_3
    return-void
.end method


# virtual methods
.method public getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "drmFilename"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 856
    if-eqz p1, :cond_0

    .line 857
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".dcf"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 858
    const-string v0, "application/vnd.oma.drm.content"

    .line 877
    :cond_0
    :goto_0
    return-object v0

    .line 859
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".avi"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 860
    const-string v0, "video/mux/AVI"

    goto :goto_0

    .line 861
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".mkv"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 862
    const-string v0, "video/mux/MKV"

    goto :goto_0

    .line 863
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".divx"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 864
    const-string v0, "video/mux/DivX"

    goto :goto_0

    .line 865
    :cond_4
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".pyv"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 866
    const-string v0, "video/vnd.ms-playready.media.pyv"

    goto :goto_0

    .line 867
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".pya"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 868
    const-string v0, "audio/vnd.ms-playready.media.pya"

    goto :goto_0

    .line 869
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".wmv"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 870
    const-string v0, "video/x-ms-wmv"

    goto :goto_0

    .line 871
    :cond_7
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".wma"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 872
    const-string v0, "audio/x-ms-wma"

    goto :goto_0
.end method

.method public getVideoViewSize()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 344
    const/4 v0, 0x0

    .line 345
    .local v0, "dimensionsString":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    if-nez v7, :cond_0

    .line 346
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/mv/player/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 349
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    if-eqz v7, :cond_1

    .line 350
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/mv/player/db/VideoDB;->getResolution(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 353
    :cond_1
    if-eqz v0, :cond_2

    .line 354
    const-string v7, "x"

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 355
    .local v1, "index":I
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    .line 357
    .local v4, "totalLenght":I
    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoWidth:I

    .line 358
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v0, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoHeight:I

    .line 361
    .end local v1    # "index":I
    .end local v4    # "totalLenght":I
    :cond_2
    iget v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoWidth:I

    if-eqz v7, :cond_5

    iget v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoHeight:I

    if-eqz v7, :cond_5

    .line 362
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getCheckRoation()I

    move-result v2

    .line 363
    .local v2, "m_VideoRotation":I
    if-ne v2, v5, :cond_3

    .line 364
    iget v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoWidth:I

    .line 365
    .local v3, "temp":I
    iget v6, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoHeight:I

    iput v6, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoWidth:I

    .line 366
    iput v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoHeight:I

    .line 368
    .end local v3    # "temp":I
    :cond_3
    const-string v6, "VideoProgressPreviewPopup"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getVideoViewSize() Width "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Height "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    iget v6, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoWidth:I

    iget v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoHeight:I

    if-lt v6, v7, :cond_4

    .line 371
    iget-object v6, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0801b2

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mMeasuredVideoHeight:I

    .line 372
    iget v6, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mMeasuredVideoHeight:I

    iget v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoWidth:I

    mul-int/2addr v6, v7

    iget v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoHeight:I

    div-int/2addr v6, v7

    iput v6, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mMeasuredVideoWidth:I

    .line 378
    :goto_0
    const-string v6, "VideoProgressPreviewPopup"

    const-string v7, "getvideoViewSize return true"

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    .end local v2    # "m_VideoRotation":I
    :goto_1
    return v5

    .line 374
    .restart local v2    # "m_VideoRotation":I
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0801b9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mMeasuredVideoWidth:I

    .line 375
    iget v6, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mMeasuredVideoWidth:I

    iget v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoHeight:I

    mul-int/2addr v6, v7

    iget v7, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoWidth:I

    div-int/2addr v6, v7

    iput v6, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mMeasuredVideoHeight:I

    goto :goto_0

    .line 381
    .end local v2    # "m_VideoRotation":I
    :cond_5
    const-string v5, "VideoProgressPreviewPopup"

    const-string v7, "Video width or height zero"

    invoke-static {v5, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v6

    .line 382
    goto :goto_1
.end method

.method public init()V
    .locals 3

    .prologue
    .line 250
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "init"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mFormatBuilder:Ljava/lang/StringBuilder;

    .line 253
    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mFormatter:Ljava/util/Formatter;

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    const v1, 0x7f030024

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d00a9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d00ad

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d00ac

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    .line 258
    return-void
.end method

.method public isDrmFile(Ljava/lang/String;)Z
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 832
    if-nez p1, :cond_1

    .line 851
    :cond_0
    :goto_0
    return v3

    .line 836
    :cond_1
    const/4 v1, 0x0

    .line 837
    .local v1, "mDrmClient":Landroid/drm/DrmManagerClient;
    new-instance v1, Landroid/drm/DrmManagerClient;

    .end local v1    # "mDrmClient":Landroid/drm/DrmManagerClient;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    .line 839
    .restart local v1    # "mDrmClient":Landroid/drm/DrmManagerClient;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 840
    .local v2, "mimeType":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    if-eqz v1, :cond_0

    .line 841
    invoke-virtual {v1, p1, v2}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 843
    .local v0, "isDrmSupported":Z
    if-eqz v0, :cond_2

    .line 844
    const-string v3, "VideoProgressPreviewPopup"

    const-string v4, "isDrmFile. canHandle returned true. it\'s drm file"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    const/4 v3, 0x1

    goto :goto_0

    .line 847
    :cond_2
    const-string v4, "VideoProgressPreviewPopup"

    const-string v5, "isDrmFile. canHandle returned false. Not a drm file by extension"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 750
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 752
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mIsInitialized:Z

    if-eqz v1, :cond_0

    .line 753
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 754
    const-string v1, "VideoProgressPreviewPopup"

    const-string v2, "video progress popup pause() called"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 762
    :cond_0
    :goto_0
    return-void

    .line 758
    :catch_0
    move-exception v0

    .line 759
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public progressSeekto()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 216
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getSeekHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 217
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getSeekHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "progressSeekto() :: remove SEEK msg"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getSeekHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 222
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getSeekHandler()Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 227
    :cond_1
    :goto_0
    return-void

    .line 225
    :cond_2
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "progressSeekto mediaplayer is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public removeDelayedMessage()V
    .locals 7

    .prologue
    const/4 v6, 0x7

    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 660
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "removeDelayedMessage"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 663
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_SHOW_POPUP)"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 667
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 668
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_DISMISS_POPUP)"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 672
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 673
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_SHOW_SEEK)"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 677
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 678
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_SHOW_PLAY)"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 682
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 683
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_SHOW_PLAY)"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 687
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 688
    return-void
.end method

.method public seek()V
    .locals 5

    .prologue
    .line 765
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_1

    .line 766
    iget v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentPosition:I

    .line 767
    .local v1, "position":I
    if-gtz v1, :cond_0

    .line 768
    const/4 v1, 0x0

    .line 770
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mIsInitialized:Z

    if-eqz v2, :cond_1

    .line 772
    :try_start_0
    const-string v2, "VideoProgressPreviewPopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "seek() position: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->seekTo(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 779
    .end local v1    # "position":I
    :cond_1
    :goto_0
    return-void

    .line 774
    .restart local v1    # "position":I
    :catch_0
    move-exception v0

    .line 775
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public sendDelayedMoveMessage()V
    .locals 4

    .prologue
    const/4 v3, 0x7

    const/4 v2, 0x5

    .line 630
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 631
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "send move msg remove MSG_SHOW_PLAY"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 634
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 635
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "send move msg remove MSG_SHOW_SEEK"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 639
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 640
    return-void
.end method

.method public sendDelayedPauseMessage()V
    .locals 2

    .prologue
    .line 647
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 648
    return-void
.end method

.method public sendDelayedSeekMessage(I)V
    .locals 5
    .param p1, "ms"    # I

    .prologue
    const/4 v4, 0x7

    .line 651
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 652
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "send move msg remove MSG_SHOW_SEEK"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 656
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 657
    return-void
.end method

.method public sendDelayedShowMessage()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 618
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "sendDelayedMessage"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "sendDelayedMessage: removeMessages(HoverMsg.MSG_SHOW_POPUP)"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 625
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 626
    return-void
.end method

.method public sendDelayedStartMessage(I)V
    .locals 4
    .param p1, "ms"    # I

    .prologue
    .line 643
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x5

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 644
    return-void
.end method

.method public setCurrentPosition(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 782
    iput p1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentPosition:I

    .line 784
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 785
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->currentTime(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 787
    :cond_0
    return-void
.end method

.method public setDialogPosition()V
    .locals 6

    .prologue
    .line 395
    const-string v3, "VideoProgressPreviewPopup"

    const-string v4, "setPosition"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    if-eqz v3, :cond_1

    .line 399
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 400
    .local v2, "params":Landroid/view/WindowManager$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 401
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x33

    invoke-virtual {v3, v4}, Landroid/view/Window;->setGravity(I)V

    .line 402
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mProgressX:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 403
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getRawY(I)I

    move-result v3

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 404
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-ge v3, v4, :cond_2

    .line 405
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0801ac

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 410
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    .line 411
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 412
    .local v0, "ArrowParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mProgressX:I

    add-int/2addr v3, v4

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mArrowWidth:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 413
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 422
    .end local v0    # "ArrowParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v2    # "params":Landroid/view/WindowManager$LayoutParams;
    :cond_1
    :goto_1
    return-void

    .line 406
    .restart local v2    # "params":Landroid/view/WindowManager$LayoutParams;
    :cond_2
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    if-le v3, v4, :cond_0

    .line 407
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0801ac

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 416
    .end local v2    # "params":Landroid/view/WindowManager$LayoutParams;
    :catch_0
    move-exception v1

    .line 417
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 418
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v1

    .line 419
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public setHolder()V
    .locals 3

    .prologue
    .line 724
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 726
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 733
    :cond_0
    :goto_0
    return-void

    .line 727
    :catch_0
    move-exception v0

    .line 728
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 729
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 730
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public setParam(Landroid/content/Context;ILandroid/graphics/Rect;Landroid/net/Uri;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listtype"    # I
    .param p3, "mRect"    # Landroid/graphics/Rect;
    .param p4, "uri"    # Landroid/net/Uri;

    .prologue
    .line 691
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    .line 692
    iput-object p4, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    .line 693
    iput-object p3, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    .line 694
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 695
    return-void
.end method

.method public setRawX(IIII)V
    .locals 1
    .param p1, "mX"    # I
    .param p2, "seekBarLeftPadding"    # I
    .param p3, "mPositionStatus"    # I
    .param p4, "progress"    # I

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 336
    :goto_0
    return-void

    .line 333
    :cond_0
    iput p1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mProgressX:I

    .line 334
    iput p2, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    .line 335
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setDialogPosition()V

    goto :goto_0
.end method

.method public start()V
    .locals 3

    .prologue
    .line 736
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 738
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mIsInitialized:Z

    if-eqz v1, :cond_0

    .line 739
    const-string v1, "VideoProgressPreviewPopup"

    const-string v2, "video progress popup start() called"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 741
    const/16 v1, 0xbb8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->sendDelayedSeekMessage(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 747
    :cond_0
    :goto_0
    return-void

    .line 743
    :catch_0
    move-exception v0

    .line 744
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
