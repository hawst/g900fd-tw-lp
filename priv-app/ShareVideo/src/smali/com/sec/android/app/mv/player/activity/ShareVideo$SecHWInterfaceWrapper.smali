.class public Lcom/sec/android/app/mv/player/activity/ShareVideo$SecHWInterfaceWrapper;
.super Ljava/lang/Object;
.source "ShareVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SecHWInterfaceWrapper"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SecHWInterfaceWrapper"

.field private static mDNIeUIMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3029
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$SecHWInterfaceWrapper;->mDNIeUIMode:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static resetMode()V
    .locals 1

    .prologue
    .line 3032
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$SecHWInterfaceWrapper;->mDNIeUIMode:I

    .line 3033
    return-void
.end method

.method public static setBatteryADC(Ljava/lang/String;Z)V
    .locals 3
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "set"    # Z

    .prologue
    .line 3049
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/android/hardware/SecHardwareInterface;->setBatteryADC(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 3053
    :goto_0
    return-void

    .line 3050
    :catch_0
    move-exception v0

    .line 3051
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v1, "SecHWInterfaceWrapper"

    const-string v2, "NoSuchMethod - SecHardwareInterface.setBatteryADC()"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setmDNIeUIMode(I)V
    .locals 4
    .param p0, "set"    # I

    .prologue
    .line 3037
    :try_start_0
    sget v1, Lcom/sec/android/app/mv/player/activity/ShareVideo$SecHWInterfaceWrapper;->mDNIeUIMode:I

    if-eq v1, p0, :cond_0

    .line 3038
    invoke-static {p0}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeUIMode(I)Z

    .line 3039
    sput p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$SecHWInterfaceWrapper;->mDNIeUIMode:I

    .line 3040
    const-string v1, "SecHWInterfaceWrapper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setmDNIeUIMode : set as "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 3045
    :cond_0
    :goto_0
    return-void

    .line 3042
    :catch_0
    move-exception v0

    .line 3043
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v1, "SecHWInterfaceWrapper"

    const-string v2, "NoSuchMethod - SecHardwareInterface.setmDNIeUIMode()"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
