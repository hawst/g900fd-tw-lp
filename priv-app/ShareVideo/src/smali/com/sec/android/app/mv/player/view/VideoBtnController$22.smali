.class Lcom/sec/android/app/mv/player/view/VideoBtnController$22;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V
    .locals 0

    .prologue
    .line 1827
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$22;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 12
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v11, 0x16

    const/16 v10, 0x15

    .line 1829
    const/4 v4, 0x0

    .line 1830
    .local v4, "seekKey":I
    const/4 v1, 0x0

    .line 1831
    .local v1, "retVal":Z
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$22;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1832
    const/4 v5, 0x0

    .line 1886
    :goto_0
    return v5

    .line 1834
    :cond_0
    if-ne p2, v10, :cond_3

    .line 1835
    const/4 v4, 0x7

    .line 1840
    :cond_1
    :goto_1
    packed-switch p2, :pswitch_data_0

    .line 1868
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$22;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # invokes: Lcom/sec/android/app/mv/player/view/VideoBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v5, p2, p3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$600(Lcom/sec/android/app/mv/player/view/VideoBtnController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1869
    .local v0, "isReturn":Z
    if-eqz v0, :cond_6

    .end local v0    # "isReturn":Z
    :cond_2
    :goto_2
    move v5, v1

    .line 1886
    goto :goto_0

    .line 1836
    :cond_3
    if-ne p2, v11, :cond_1

    .line 1837
    const/4 v4, 0x6

    goto :goto_1

    .line 1843
    :pswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_1

    goto :goto_2

    .line 1845
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$22;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$22;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getDuration()I

    move-result v6

    int-to-long v6, v6

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mDuration:J
    invoke-static {v5, v6, v7}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2902(Lcom/sec/android/app/mv/player/view/VideoBtnController;J)J

    .line 1846
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$22;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_2

    .line 1850
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$22;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v5

    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 1851
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v6

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v8

    sub-long v2, v6, v8

    .line 1852
    .local v2, "pressTime":J
    const-wide/16 v6, 0x1f4

    cmp-long v5, v2, v6

    if-gez v5, :cond_2

    .line 1853
    if-ne p2, v10, :cond_5

    .line 1854
    const/16 v4, 0xd

    .line 1858
    :cond_4
    :goto_3
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$22;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_2

    .line 1855
    :cond_5
    if-ne p2, v11, :cond_4

    .line 1856
    const/16 v4, 0xc

    goto :goto_3

    .line 1872
    .end local v2    # "pressTime":J
    .restart local v0    # "isReturn":Z
    :cond_6
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_2

    .line 1874
    :pswitch_3
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$22;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 1875
    goto :goto_2

    .line 1878
    :pswitch_4
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$22;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 1879
    goto :goto_2

    .line 1840
    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1843
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1872
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
