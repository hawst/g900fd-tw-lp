.class Lcom/sec/android/app/mv/player/activity/PlaylistActivity$1;
.super Ljava/lang/Object;
.source "PlaylistActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/PlaylistActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)V
    .locals 0

    .prologue
    .line 456
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 460
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 461
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 476
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$300(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "---- default"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    :cond_0
    :goto_0
    return-void

    .line 464
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$100(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_1

    .line 465
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->changeAllState(Z)V

    .line 470
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$200(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/view/ActionMode;

    move-result-object v1

    if-nez v1, :cond_0

    .line 471
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$100(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->toggle()V

    .line 472
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->invalidateOptionsMenu()V

    goto :goto_0

    .line 467
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->changeAllState(Z)V

    goto :goto_1

    .line 461
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d0030
        :pswitch_0
    .end packed-switch
.end method
