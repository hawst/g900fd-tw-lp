.class public Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;
.super Ljava/lang/Object;
.source "VideoSubtitleSyncPopup.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# static fields
.field private static final LONGMINUSMSG:I = 0x2

.field private static final LONGPLUSMSG:I = 0x1

.field private static final SYNC_RANGE_VALUE:I = 0x96

.field private static final SYNC_TIME_VALUE:I = 0xc8

.field private static final TAG:Ljava/lang/String; = "VideoSubtitleSyncPopup"


# instance fields
.field private mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

.field private mContext:Landroid/content/Context;

.field public mCurPosition:I

.field private mHandler:Landroid/os/Handler;

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mMinusButton:Landroid/widget/ImageButton;

.field private mPlusButton:Landroid/widget/ImageButton;

.field private mPrevSyncTime:I

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mSubtitleSyncPopup:Landroid/app/AlertDialog;

.field private mSubtitleSyncText:Landroid/widget/TextView;

.field private mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mContext:Landroid/content/Context;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    .line 31
    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleSyncText:Landroid/widget/TextView;

    .line 34
    iput v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mCurPosition:I

    .line 35
    iput v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mPrevSyncTime:I

    .line 294
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$9;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mHandler:Landroid/os/Handler;

    .line 316
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$10;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 44
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mContext:Landroid/content/Context;

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->createPopup()V

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->callSubtitleMenu()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;IJ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;
    .param p1, "x1"    # I
    .param p2, "x2"    # J

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->sendMessage(IJ)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;
    .param p1, "x1"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->removeMessage(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->callSubtitleDismiss()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    return-object v0
.end method

.method private callSubtitleDismiss()V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getSettingsSubPopup()Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getSettingsSubPopup()Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/popup/IVideoPopup;->getID()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getSettingsSubPopup()Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->dismiss()V

    .line 60
    :cond_0
    return-void
.end method

.method private callSubtitleMenu()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getSettingsSubPopup()Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getSettingsSubPopup()Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/popup/IVideoPopup;->getID()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getSettingsSubPopup()Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleMenu()V

    .line 54
    :cond_0
    return-void
.end method

.method private createPopup()V
    .locals 9

    .prologue
    const v8, 0x7f0a0094

    const/4 v7, 0x1

    .line 63
    const-string v3, "VideoSubtitleSyncPopup"

    const-string v4, "createPopup E"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mPrevSyncTime:I

    .line 66
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mContext:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 67
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030023

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 69
    .local v2, "view":Landroid/view/View;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 71
    .local v1, "popup":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0a0091

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 72
    const v3, 0x7f0a0060

    new-instance v4, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$1;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 79
    const v3, 0x7f0a0016

    new-instance v4, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$2;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 86
    const v3, 0x7f0d00a5

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    iput-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    .line 87
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 88
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    const/16 v4, 0x12c

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setMax(I)V

    .line 89
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 91
    const v3, 0x7f0d00a3

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleSyncText:Landroid/widget/TextView;

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->setSyncProgress()V

    .line 94
    const v3, 0x7f0d00a6

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    .line 95
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_0

    .line 96
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00a0

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 98
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$3;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_0

    .line 105
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$4;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 130
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$5;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$5;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 153
    :cond_0
    const v3, 0x7f0d00a4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    .line 154
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_1

    .line 155
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a009a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 157
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$6;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$6;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_1

    .line 164
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$7;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$7;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 189
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$8;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$8;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 212
    :cond_1
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 213
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    .line 214
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 215
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 216
    return-void
.end method

.method private removeMessage(I)V
    .locals 1
    .param p1, "what"    # I

    .prologue
    .line 313
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 314
    return-void
.end method

.method private revertSubtitleSyncTime()V
    .locals 3

    .prologue
    .line 359
    const-string v0, "VideoSubtitleSyncPopup"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "revertSubtitleSyncTime() : mPrevSyncTime = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mPrevSyncTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mPrevSyncTime:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSyncTime(I)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleSyncTime()V

    .line 362
    return-void
.end method

.method private sendMessage(IJ)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "delay"    # J

    .prologue
    .line 307
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 308
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 309
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 310
    return-void
.end method


# virtual methods
.method public clickMINUSBtn()V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v0

    const/16 v1, -0x7530

    if-le v0, v1, :cond_0

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v1

    add-int/lit16 v1, v1, -0xc8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSyncTime(I)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mCurPosition:I

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleSyncTime()V

    .line 292
    :cond_0
    return-void
.end method

.method public clickPLUSBtn()V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v0

    const/16 v1, 0x7530

    if-ge v0, v1, :cond_0

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v1

    add-int/lit16 v1, v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSyncTime(I)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mCurPosition:I

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleSyncTime()V

    .line 283
    :cond_0
    return-void
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 227
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->revertSubtitleSyncTime()V

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    .line 231
    :cond_0
    return-void
.end method

.method public isShowSyncPopup()Z
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 222
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 9
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const v8, 0x7f0a007c

    .line 253
    if-eqz p3, :cond_1

    .line 254
    iget v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mCurPosition:I

    if-ge v1, p2, :cond_2

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mCurPosition:I

    sub-int v3, p2, v3

    mul-int/lit16 v3, v3, 0xc8

    add-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSyncTime(I)V

    .line 256
    iput p2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mCurPosition:I

    .line 261
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleSyncTime()V

    .line 264
    :cond_1
    const-string v1, "VideoSubtitleSyncPopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onProgressChanged - position : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mSyncTime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string v1, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v4

    int-to-double v4, v4

    const-wide v6, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 266
    .local v0, "time":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleSyncText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleSyncText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 268
    return-void

    .line 257
    .end local v0    # "time":Ljava/lang/String;
    :cond_2
    iget v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mCurPosition:I

    if-le v1, p2, :cond_0

    .line 258
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mCurPosition:I

    sub-int/2addr v3, p2

    mul-int/lit16 v3, v3, 0xc8

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSyncTime(I)V

    .line 259
    iput p2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mCurPosition:I

    goto/16 :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 271
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 274
    return-void
.end method

.method public setSyncProgress()V
    .locals 9

    .prologue
    const v8, 0x7f0a007c

    .line 238
    const-string v1, "VideoSubtitleSyncPopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSyncProgress. current sync time : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v1

    if-nez v1, :cond_0

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    const/16 v2, 0x96

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 246
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mCurPosition:I

    .line 247
    const-string v1, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v4

    int-to-double v4, v4

    const-wide v6, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 248
    .local v0, "time":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 249
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleSyncText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    return-void

    .line 243
    .end local v0    # "time":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v2

    div-int/lit16 v2, v2, 0xc8

    add-int/lit16 v2, v2, 0x96

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method public show()V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->createPopup()V

    .line 235
    return-void
.end method
