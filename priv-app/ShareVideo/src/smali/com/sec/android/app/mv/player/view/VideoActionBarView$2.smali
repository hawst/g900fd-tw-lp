.class Lcom/sec/android/app/mv/player/view/VideoActionBarView$2;
.super Ljava/lang/Object;
.source "VideoActionBarView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/view/VideoActionBarView;->initCounterView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V
    .locals 0

    .prologue
    .line 335
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 338
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-nez p3, :cond_1

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$100(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListView()Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->toggleSelectAll()V

    .line 343
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$100(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListView()Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->getSelectedItemSize()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setCounterSpinnerText(I)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$100(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListView()Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->updateActionBarBtn()V

    .line 345
    return-void

    .line 340
    :cond_1
    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$100(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListView()Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->changeAllState(Z)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 348
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
