.class Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$1;
.super Ljava/lang/Object;
.source "VideoProgressPreviewPopup.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->playVideo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V
    .locals 0

    .prologue
    .line 447
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$1;->this$0:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "player"    # Landroid/media/MediaPlayer;

    .prologue
    .line 449
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "onPrepared"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$1;->this$0:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mStartPosition:I
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->access$1100(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 451
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$1;->this$0:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mIsInitialized:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->access$802(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;Z)Z

    .line 453
    return-void
.end method
