.class public Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;
.super Ljava/lang/Object;
.source "PlaylistSelectedItem.java"


# instance fields
.field private mItem:Lcom/sec/android/app/mv/player/type/PlaylistItem;

.field private mPos:I


# direct methods
.method public constructor <init>(ILcom/sec/android/app/mv/player/type/PlaylistItem;)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "item"    # Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput p1, p0, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->mPos:I

    .line 9
    iput-object p2, p0, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->mItem:Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .line 10
    return-void
.end method


# virtual methods
.method public getItem()Lcom/sec/android/app/mv/player/type/PlaylistItem;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->mItem:Lcom/sec/android/app/mv/player/type/PlaylistItem;

    return-object v0
.end method

.method public getPos()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->mPos:I

    return v0
.end method
