.class public Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;
.super Landroid/app/DialogFragment;
.source "VideoTitleController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeviceVolumeDialogFragment"
.end annotation


# static fields
.field private static frag:Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;


# instance fields
.field private listView:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 931
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;)Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;

    .prologue
    .line 931
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->listView:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;
    .locals 2

    .prologue
    .line 937
    const-class v1, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->frag:Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;

    if-nez v0, :cond_0

    .line 938
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->frag:Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;

    .line 940
    :cond_0
    sget-object v0, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->frag:Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 937
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    .line 951
    const/4 v4, 0x0

    .line 953
    .local v4, "mode":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 955
    .local v1, "context":Landroid/content/Context;
    new-instance v6, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    invoke-direct {v6, v1, v4}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;-><init>(Landroid/content/Context;I)V

    iput-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->listView:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    .line 957
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030002

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 958
    .local v5, "view":Landroid/view/View;
    const v6, 0x7f0d0009

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 959
    .local v3, "linearLayout":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->listView:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 961
    new-instance v2, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v2, v6}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 962
    .local v2, "dialog":Landroid/app/Dialog;
    invoke-virtual {v2, v9}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 963
    invoke-virtual {v2, v9}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 964
    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 965
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f02005a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 966
    const/4 v6, 0x0

    const v7, 0x1030129

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->setStyle(II)V

    .line 968
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 969
    .local v0, "WMLP":Landroid/view/WindowManager$LayoutParams;
    const/16 v6, 0x11

    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 970
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->listView:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08003d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 971
    const/4 v6, -0x2

    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 972
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 974
    return-object v2
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 945
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 946
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 947
    :cond_0
    return-void
.end method
