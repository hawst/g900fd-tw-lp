.class Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$7;
.super Ljava/lang/Object;
.source "VideoProgressPreviewPopup.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V
    .locals 0

    .prologue
    .line 697
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$7;->this$0:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 705
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, ">>>>>>>>surfaceChanged<<<<<<<<<<<<"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 699
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, ">>>>>>>>surfaceCreated<<<<<<<<<<<<"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$7;->this$0:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, p1}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->access$1302(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 701
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$7;->this$0:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->playVideo()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->access$1400(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V

    .line 702
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    const/4 v2, 0x0

    .line 709
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, ">>>>>>>>surfaceDestroyed<<<<<<<<<<<<"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$7;->this$0:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->access$1302(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 712
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$7;->this$0:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->access$600(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 713
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "mediaPlayer release() start"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$7;->this$0:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setAIAContext(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->access$1500(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;Z)V

    .line 715
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$7;->this$0:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->access$600(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 716
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$7;->this$0:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->access$602(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;

    .line 719
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup$7;->this$0:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->hideBoarders()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->access$1600(Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)V

    .line 720
    return-void
.end method
