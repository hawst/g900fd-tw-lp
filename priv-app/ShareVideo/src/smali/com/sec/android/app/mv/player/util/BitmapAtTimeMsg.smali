.class public Lcom/sec/android/app/mv/player/util/BitmapAtTimeMsg;
.super Ljava/lang/Object;
.source "BitmapAtTimeMsg.java"


# instance fields
.field private mMeasured:Landroid/graphics/Rect;

.field private mPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/BitmapAtTimeMsg;->mPath:Ljava/lang/String;

    .line 11
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, p2, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/BitmapAtTimeMsg;->mMeasured:Landroid/graphics/Rect;

    .line 12
    return-void
.end method


# virtual methods
.method public getHeight()I
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/BitmapAtTimeMsg;->mMeasured:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/BitmapAtTimeMsg;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/BitmapAtTimeMsg;->mMeasured:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method
