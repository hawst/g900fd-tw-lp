.class public Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PlaylistArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/mv/player/type/PlaylistItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final PROGRESS_RESOLUTION:J = 0x3e8L


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mPlaylistAttr:I

.field private mPlaylistType:I

.field mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

.field private mSelectedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;",
            ">;"
        }
    .end annotation
.end field

.field mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;ILcom/sec/android/app/mv/player/playlist/PlaylistView;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p4, "attr"    # I
    .param p5, "v"    # Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistItem;",
            ">;I",
            "Lcom/sec/android/app/mv/player/playlist/PlaylistView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    .local p3, "pl":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistItem;>;"
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/android/app/mv/player/playlist/PlaylistView;)V

    .line 60
    iput p4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistAttr:I

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/android/app/mv/player/playlist/PlaylistView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p4, "v"    # Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistItem;",
            ">;",
            "Lcom/sec/android/app/mv/player/playlist/PlaylistView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 49
    .local p3, "pl":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistItem;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 31
    const-class v0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->TAG:Ljava/lang/String;

    .line 50
    iput-object p1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mContext:Landroid/content/Context;

    .line 51
    invoke-virtual {p4}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getPlaylistType()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    .line 52
    invoke-virtual {p4}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mSelectedList:Ljava/util/ArrayList;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/SharedPreference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 56
    return-void
.end method

.method private isSelected(I)Z
    .locals 3
    .param p1, "pos"    # I

    .prologue
    const/4 v2, 0x0

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mSelectedList:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 154
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->getPos()I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 156
    const/4 v1, 0x1

    .line 161
    .end local v0    # "i":I
    :goto_1
    return v1

    .line 154
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v1, v2

    .line 159
    goto :goto_1

    .end local v0    # "i":I
    :cond_2
    move v1, v2

    .line 161
    goto :goto_1
.end method

.method private showCheckbox(Landroid/view/View;I)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "pos"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 166
    const v1, 0x7f0d002b

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 168
    .local v0, "cb":Landroid/widget/CheckBox;
    if-nez v0, :cond_1

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    iget v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    if-ne v1, v4, :cond_2

    .line 173
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    .line 175
    :cond_2
    iget v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    iget v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 178
    :cond_3
    invoke-direct {p0, p2}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->isSelected(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 179
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 183
    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    .line 181
    :cond_4
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1
.end method

.method private showDuration(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "item"    # Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .prologue
    const/4 v9, 0x0

    .line 208
    const v6, 0x7f0d002a

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 209
    .local v4, "tv":Landroid/widget/TextView;
    const v6, 0x7f0d002b

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 210
    .local v0, "cb":Landroid/widget/CheckBox;
    if-eqz v4, :cond_0

    if-nez v0, :cond_1

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    sget-boolean v6, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v6, :cond_3

    iget v6, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistAttr:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    .line 215
    iget v6, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_2

    iget v6, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_4

    .line 217
    :cond_2
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 218
    .local v2, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v6, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v7, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f080168

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    mul-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 219
    .local v5, "width":I
    invoke-virtual {v4, v9, v9, v5, v9}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 225
    .end local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .end local v5    # "width":I
    :cond_3
    :goto_1
    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getDuration()J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->stringForTime(J)Ljava/lang/String;

    move-result-object v1

    .line 226
    .local v1, "durString":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getResumePos()I

    move-result v6

    int-to-long v6, v6

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->stringForTime(J)Ljava/lang/String;

    move-result-object v3

    .line 227
    .local v3, "resumePosString":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 221
    .end local v1    # "durString":Ljava/lang/String;
    .end local v3    # "resumePosString":Ljava/lang/String;
    :cond_4
    invoke-virtual {v4, v9, v9, v9, v9}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1
.end method

.method private showFooter(Landroid/view/View;I)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;
    .param p2, "pos"    # I

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 268
    const v4, 0x7f0d0019

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 269
    .local v2, "layoutCount":Landroid/widget/RelativeLayout;
    const v4, 0x7f0d002c

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 271
    .local v0, "bottomLine":Landroid/widget/LinearLayout;
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v1

    .line 273
    .local v1, "count":I
    add-int/lit8 v4, v1, -0x1

    if-ne p2, v4, :cond_2

    .line 274
    const v4, 0x7f0d001a

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 275
    .local v3, "textCount":Landroid/widget/TextView;
    if-ne v1, v9, :cond_1

    .line 276
    iget-object v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mContext:Landroid/content/Context;

    const v5, 0x7f0a001d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    :goto_0
    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 281
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 287
    .end local v3    # "textCount":Landroid/widget/TextView;
    :goto_1
    sget-boolean v4, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistAttr:I

    if-ne v4, v9, :cond_0

    .line 288
    invoke-virtual {v2, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 289
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 291
    :cond_0
    return-void

    .line 278
    .restart local v3    # "textCount":Landroid/widget/TextView;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mContext:Landroid/content/Context;

    const v5, 0x7f0a001e

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 283
    .end local v3    # "textCount":Landroid/widget/TextView;
    :cond_2
    invoke-virtual {v2, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 284
    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method private showProgress(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "item"    # Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .prologue
    .line 189
    const v4, 0x7f0d0026

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 190
    .local v1, "pb":Landroid/widget/ProgressBar;
    if-nez v1, :cond_0

    .line 205
    :goto_0
    return-void

    .line 194
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getResumePos()I

    move-result v3

    .line 195
    .local v3, "resumePos":I
    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getDuration()J

    move-result-wide v4

    long-to-int v0, v4

    .line 196
    .local v0, "duration":I
    const/4 v2, 0x0

    .line 198
    .local v2, "progress":I
    if-eqz v0, :cond_1

    .line 199
    int-to-long v4, v3

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    int-to-long v6, v0

    div-long/2addr v4, v6

    long-to-int v2, v4

    .line 202
    :cond_1
    const/16 v4, 0x3e8

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 203
    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 204
    if-nez v2, :cond_2

    const/16 v4, 0x8

    :goto_1
    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private showRecentlyPlayMark(Landroid/view/View;I)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "pos"    # I

    .prologue
    .line 254
    const v1, 0x7f0d0027

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 256
    .local v0, "iv":Landroid/widget/ImageView;
    if-nez v0, :cond_0

    .line 265
    :goto_0
    return-void

    .line 260
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getCurPlayingIndex()I

    move-result v1

    if-ne p2, v1, :cond_1

    iget v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 261
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 263
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private showThumbnail(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "item"    # Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .prologue
    const/4 v4, 0x0

    .line 231
    const v2, 0x7f0d0025

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 233
    .local v1, "videoThumb":Landroid/widget/ImageView;
    if-nez v1, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->TAG:Ljava/lang/String;

    const-string v3, "showThumbnail() E"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 242
    .local v0, "thumbnail":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 243
    if-eqz v1, :cond_0

    .line 244
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 245
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 248
    :cond_2
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 249
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private showTitle(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "item"    # Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 120
    const v4, 0x7f0d0029

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 121
    .local v2, "tv":Landroid/widget/TextView;
    const v4, 0x7f0d002b

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 123
    .local v0, "cb":Landroid/widget/CheckBox;
    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    sget-boolean v4, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-eqz v4, :cond_5

    .line 126
    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    if-ne v4, v8, :cond_3

    .line 127
    invoke-virtual {v2, v7, v7, v7, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 148
    :cond_2
    :goto_1
    const/4 v4, 0x0

    invoke-virtual {v2, v8, v4}, Landroid/widget/TextView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 149
    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 129
    :cond_3
    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    if-eqz v4, :cond_4

    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    if-eq v4, v5, :cond_4

    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    if-ne v4, v6, :cond_2

    .line 132
    :cond_4
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 133
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v2, v7, v7, v4, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1

    .line 137
    .end local v1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_5
    sget-boolean v4, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistAttr:I

    if-ne v4, v8, :cond_2

    .line 138
    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    if-eq v4, v5, :cond_6

    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    if-ne v4, v6, :cond_7

    .line 140
    :cond_6
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 141
    .restart local v1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v5, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080168

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    mul-int/lit8 v5, v5, 0x2

    add-int v3, v4, v5

    .line 142
    .local v3, "width":I
    invoke-virtual {v2, v7, v7, v3, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1

    .line 144
    .end local v1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .end local v3    # "width":I
    :cond_7
    invoke-virtual {v2, v7, v7, v7, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1
.end method

.method private stringForTime(J)Ljava/lang/String;
    .locals 13
    .param p1, "timeMs"    # J

    .prologue
    const-wide/16 v10, 0x3c

    .line 112
    const-wide/16 v8, 0x3e8

    div-long v6, p1, v8

    .line 113
    .local v6, "totalSeconds":J
    rem-long v4, v6, v10

    .line 114
    .local v4, "seconds":J
    div-long v8, v6, v10

    rem-long v2, v8, v10

    .line 115
    .local v2, "minutes":J
    const-wide/16 v8, 0xe10

    div-long v0, v6, v8

    .line 116
    .local v0, "hours":J
    const-string v8, "%02d:%02d:%02d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 65
    if-nez p2, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 68
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistAttr:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 69
    const v3, 0x7f03000d

    invoke-virtual {v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 74
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->getCount()I

    move-result v3

    if-lt p1, v3, :cond_2

    move-object p2, v2

    .line 89
    .end local p2    # "view":Landroid/view/View;
    :goto_1
    return-object p2

    .line 71
    .restart local v0    # "inflater":Landroid/view/LayoutInflater;
    .restart local p2    # "view":Landroid/view/View;
    :cond_1
    const v3, 0x7f030014

    invoke-virtual {v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 77
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .line 79
    .local v1, "mItem":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->showTitle(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V

    .line 80
    invoke-direct {p0, p2, p1}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->showCheckbox(Landroid/view/View;I)V

    .line 81
    invoke-direct {p0, p2, p1}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->showRecentlyPlayMark(Landroid/view/View;I)V

    .line 82
    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->showProgress(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V

    .line 83
    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->showDuration(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V

    .line 84
    invoke-direct {p0, p2, p1}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->showFooter(Landroid/view/View;I)V

    .line 85
    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->showThumbnail(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V

    .line 87
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 94
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 96
    return-void
.end method

.method public setListType(II)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "attr"    # I

    .prologue
    .line 107
    iput p1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistType:I

    .line 108
    iput p2, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->mPlaylistAttr:I

    .line 109
    return-void
.end method
