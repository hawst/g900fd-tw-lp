.class Lcom/sec/android/app/mv/player/view/VideoBtnController$7;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V
    .locals 0

    .prologue
    .line 679
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$7;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v4, 0xbb8

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 681
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$7;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 682
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 714
    :cond_0
    :goto_0
    return v3

    .line 684
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$7;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    const v1, 0x36ee80

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 688
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$7;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 692
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    cmpg-float v0, v2, v0

    if-gtz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    cmpg-float v0, v2, v0

    if-gtz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 694
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$7;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->changeScreen()V

    .line 695
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$7;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->refreshFitToScrBtn()V

    .line 697
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$7;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 701
    :pswitch_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 703
    :cond_2
    invoke-virtual {p1, v3}, Landroid/view/View;->setPressed(Z)V

    goto/16 :goto_0

    .line 705
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    goto/16 :goto_0

    .line 682
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method
