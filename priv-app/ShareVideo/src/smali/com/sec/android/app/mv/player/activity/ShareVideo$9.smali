.class Lcom/sec/android/app/mv/player/activity/ShareVideo$9;
.super Landroid/os/Handler;
.source "ShareVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 1671
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 26
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1673
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v21, v0

    sparse-switch v21, :sswitch_data_0

    .line 1933
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 1675
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    const-wide/16 v24, 0x1f4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move-wide/from16 v2, v24

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideo;->queueNextRefresh(IJ)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1000(Lcom/sec/android/app/mv/player/activity/ShareVideo;IJ)V

    goto :goto_0

    .line 1679
    :sswitch_2
    const-string v21, "ShareVideo"

    const-string v22, "mHandler. case QUIT E"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1680
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->finish()V

    goto :goto_0

    .line 1684
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v21

    if-eqz v21, :cond_2

    .line 1685
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1100(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/db/SharedPreference;

    move-result-object v21

    if-eqz v21, :cond_1

    .line 1686
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1100(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/db/SharedPreference;

    move-result-object v22

    const-string v23, "subtitle_synctime"

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSyncTime(I)V

    .line 1688
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleActivated()Z

    move-result v21

    if-eqz v21, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v21

    if-eqz v21, :cond_0

    .line 1689
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleSyncTime()V

    goto/16 :goto_0

    .line 1691
    :cond_2
    const/16 v21, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->obtainMessage(I)Landroid/os/Message;

    move-result-object v21

    const-wide/16 v22, 0x1f4

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-wide/from16 v2, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 1696
    :sswitch_4
    const-string v21, "ShareVideo"

    const-string v22, "mHandler. case EXITMOVIEPLAYER E"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1697
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mStopped:Z
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 1698
    const-string v21, "ShareVideo"

    const-string v22, "mHandler. case EXITMOVIEPLAYER E return case "

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1703
    :cond_3
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->pause()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1708
    :goto_1
    new-instance v4, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/content/Context;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v4, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1709
    .local v4, "builder":Landroid/app/AlertDialog$Builder;
    const v21, 0x7f0a0009

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1710
    const v21, 0x7f0a0072

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1712
    new-instance v21, Lcom/sec/android/app/mv/player/activity/ShareVideo$9$1;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo$9$1;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo$9;)V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 1719
    const v21, 0x7f0a0060

    new-instance v22, Lcom/sec/android/app/mv/player/activity/ShareVideo$9$2;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo$9$2;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo$9;)V

    move/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v4, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1726
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v22

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;
    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1502(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 1727
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1500(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/app/AlertDialog;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1728
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1500(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/app/AlertDialog;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 1704
    .end local v4    # "builder":Landroid/app/AlertDialog$Builder;
    :catch_0
    move-exception v6

    .line 1705
    .local v6, "e":Ljava/lang/Exception;
    const-string v21, "ShareVideo"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "EXIT_MOVIE_PLAYER mService.pause RemoteException occured :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1732
    .end local v6    # "e":Ljava/lang/Exception;
    :sswitch_5
    const-string v21, "ShareVideo"

    const-string v22, "mHandler. case SET_CONTROLLER_UPDATE E"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1733
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v21

    const/16 v22, 0x7

    invoke-virtual/range {v21 .. v22}, Landroid/os/Handler;->removeMessages(I)V

    .line 1735
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    .line 1736
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setControllerUpdate()V

    goto/16 :goto_0

    .line 1746
    :sswitch_6
    const-string v21, "ShareVideo"

    const-string v22, "mHandler. case VOLUME_UPDATE E"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1748
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v21

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/os/Handler;->removeMessages(I)V

    .line 1750
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    .line 1751
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->updateVolume()V

    goto/16 :goto_0

    .line 1755
    :sswitch_7
    const-string v21, "ShareVideo"

    const-string v22, "mHandler. case MOVIEPLAYER_EXIT_ON_ERRORPOPUP E"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1757
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/app/AlertDialog;

    move-result-object v21

    if-eqz v21, :cond_4

    .line 1758
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/app/AlertDialog;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/app/AlertDialog;->dismiss()V

    .line 1761
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    goto/16 :goto_0

    .line 1765
    :sswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v21

    const/16 v22, 0xa

    invoke-virtual/range {v21 .. v22}, Landroid/os/Handler;->removeMessages(I)V

    .line 1767
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1700(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    move-result-object v21

    if-nez v21, :cond_5

    .line 1768
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    new-instance v22, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/content/Context;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;
    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1702(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;)Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    .line 1769
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1700(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    move-result-object v21

    new-instance v22, Lcom/sec/android/app/mv/player/activity/ShareVideo$9$3;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo$9$3;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo$9;)V

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1776
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShowShareVideoGuidePopup:Z
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1800(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Z

    move-result v21

    if-eqz v21, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 1777
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1700(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    move-result-object v21

    if-eqz v21, :cond_0

    .line 1778
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1700(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->show()V

    goto/16 :goto_0

    .line 1783
    :sswitch_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v21

    const/16 v22, 0x4

    invoke-virtual/range {v21 .. v22}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 1784
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v21

    const/16 v22, 0x4

    invoke-virtual/range {v21 .. v22}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 1789
    :sswitch_a
    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    .line 1792
    .local v8, "path":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v21

    if-eqz v21, :cond_6

    .line 1793
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->startChordService()V

    .line 1794
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->bindChordService()V

    .line 1798
    :cond_6
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v0, v8}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->openPath(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 1799
    :catch_1
    move-exception v6

    .line 1800
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v21, "ShareVideo"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "mHandler - SHARE_VIDEO_OPEN_PATH RemoteException occured :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1806
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v8    # "path":Ljava/lang/String;
    :sswitch_b
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->stop()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 1807
    :catch_2
    move-exception v6

    .line 1808
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v21, "ShareVideo"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "stop() - SHARE_VIDEO_CURRENT_CONTENT_STOP RemoteException occured :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1813
    .end local v6    # "e":Ljava/lang/Exception;
    :sswitch_c
    move-object/from16 v0, p1

    iget v9, v0, Landroid/os/Message;->arg1:I

    .line 1814
    .local v9, "position":I
    const-string v21, "ShareVideo"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "SHARE_VIDEO_SHOW_NUMBER : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideo;->showDeviceNumber(I)V
    invoke-static {v0, v9}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1900(Lcom/sec/android/app/mv/player/activity/ShareVideo;I)V

    .line 1817
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShowInfo:Z
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2000(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Z

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 1818
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/content/Context;

    move-result-object v21

    const v22, 0x7f0a006e

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x5

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    const/16 v25, 0x3

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-virtual/range {v21 .. v23}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 1819
    .local v12, "str":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v0, v12, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v15

    .line 1820
    .local v15, "toast":Landroid/widget/Toast;
    invoke-virtual {v15}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/LinearLayout;

    .line 1821
    .local v16, "toastLayout":Landroid/widget/LinearLayout;
    const/16 v21, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 1822
    .local v19, "tv":Landroid/widget/TextView;
    const/16 v21, 0x11

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 1823
    invoke-virtual {v15}, Landroid/widget/Toast;->show()V

    .line 1824
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShowInfo:Z
    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2002(Lcom/sec/android/app/mv/player/activity/ShareVideo;Z)Z

    goto/16 :goto_0

    .line 1829
    .end local v9    # "position":I
    .end local v12    # "str":Ljava/lang/String;
    .end local v15    # "toast":Landroid/widget/Toast;
    .end local v16    # "toastLayout":Landroid/widget/LinearLayout;
    .end local v19    # "tv":Landroid/widget/TextView;
    :sswitch_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideo;->dismissDeviceNumber()V
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2100(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    goto/16 :goto_0

    .line 1833
    :sswitch_e
    const-string v21, "ShareVideo"

    const-string v22, "------ SHARE_VIDEO_SET_VOLUME_MUTE"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1834
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    .line 1835
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->muteDevice()V

    goto/16 :goto_0

    .line 1840
    :sswitch_f
    const-string v21, "ShareVideo"

    const-string v22, "------ SHARE_VIDEO_SET_VOLUME_ON"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1841
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    .line 1842
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->unMuteDevice()V

    goto/16 :goto_0

    .line 1847
    :sswitch_10
    move-object/from16 v0, p1

    iget v10, v0, Landroid/os/Message;->arg1:I

    .line 1848
    .local v10, "screenMode":I
    const-string v21, "ShareVideo"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Set Screen Rotaion Mode = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1849
    const/16 v21, 0x12f

    move/from16 v0, v21

    if-ne v10, v0, :cond_7

    .line 1850
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->forceRotateScreen(I)V

    goto/16 :goto_0

    .line 1851
    :cond_7
    const/16 v21, 0x130

    move/from16 v0, v21

    if-ne v10, v0, :cond_8

    .line 1852
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->forceRotateScreen(I)V

    goto/16 :goto_0

    .line 1854
    :cond_8
    const-string v21, "ShareVideo"

    const-string v22, "screenMode is wrong"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1859
    .end local v10    # "screenMode":I
    :sswitch_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleActivated()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 1860
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v21

    if-eqz v21, :cond_a

    .line 1861
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mClientSubtitlePath:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_9

    .line 1862
    const-string v21, "ShareVideo"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "selectedFile file path : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mClientSubtitlePath:Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1864
    new-instance v11, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mClientSubtitlePath:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1866
    .local v11, "selectedFile":Ljava/io/File;
    if-eqz v11, :cond_9

    .line 1867
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v22

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleFile:Ljava/lang/String;
    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2302(Lcom/sec/android/app/mv/player/activity/ShareVideo;Ljava/lang/String;)Ljava/lang/String;

    .line 1868
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleFile:Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    .line 1869
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->selectOutbandSubtitle()Z

    .line 1870
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->restartSubtitle()V

    .line 1872
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-object/from16 v21, v0

    if-eqz v21, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControlsShowing()Z

    move-result v21

    if-eqz v21, :cond_9

    .line 1873
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->moveSubtitleViewUp()V

    .line 1885
    .end local v11    # "selectedFile":Ljava/io/File;
    :cond_9
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setMultivisionSubtitleShowingDevice(Z)V

    goto/16 :goto_0

    .line 1878
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->restartSubtitle()V

    .line 1880
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-object/from16 v21, v0

    if-eqz v21, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControlsShowing()Z

    move-result v21

    if-eqz v21, :cond_9

    .line 1881
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->moveSubtitleViewUp()V

    goto :goto_2

    .line 1890
    :sswitch_12
    move-object/from16 v0, p1

    iget v7, v0, Landroid/os/Message;->arg1:I

    .line 1891
    .local v7, "myPosition":I
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/mv/player/common/SUtils;->getMVMembersNumber()I

    move-result v21

    move/from16 v0, v21

    int-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    div-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v14, v0

    .line 1893
    .local v14, "subtitleShowingPosition":I
    const-string v21, "ShareVideo"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "SHARE_VIDEO_SHOW_SUBTITLE_MULTI_VISION / my position: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " subtitle showing position : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " MVmembers : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/mv/player/common/SUtils;->getMVMembersNumber()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1895
    if-ne v7, v14, :cond_b

    .line 1896
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setMultivisionSubtitleShowingDevice(Z)V

    goto/16 :goto_0

    .line 1898
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setMultivisionSubtitleShowingDevice(Z)V

    goto/16 :goto_0

    .line 1903
    .end local v7    # "myPosition":I
    .end local v14    # "subtitleShowingPosition":I
    :sswitch_13
    const-string v21, "ShareVideo"

    const-string v22, "SHARE_VIDEO_DETERMINE_FIRST_MASTER"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1904
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendMessageDetermineMasterNode()V
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    goto/16 :goto_0

    .line 1908
    :sswitch_14
    const-string v21, "ShareVideo"

    const-string v22, "SHARE_VIDEO_MULTIVISION_DEVICE_DISCONNECTED"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1909
    move-object/from16 v0, p1

    iget v5, v0, Landroid/os/Message;->arg2:I

    .line 1911
    .local v5, "disconnected_position":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    const v22, 0x7f0a0019

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-virtual/range {v21 .. v23}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 1912
    .restart local v12    # "str":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v0, v12, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v15

    .line 1913
    .restart local v15    # "toast":Landroid/widget/Toast;
    invoke-virtual {v15}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/LinearLayout;

    .line 1914
    .restart local v16    # "toastLayout":Landroid/widget/LinearLayout;
    const/16 v21, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 1915
    .restart local v19    # "tv":Landroid/widget/TextView;
    const/16 v21, 0x11

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 1916
    invoke-virtual {v15}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1920
    .end local v5    # "disconnected_position":I
    .end local v12    # "str":Ljava/lang/String;
    .end local v15    # "toast":Landroid/widget/Toast;
    .end local v16    # "toastLayout":Landroid/widget/LinearLayout;
    .end local v19    # "tv":Landroid/widget/TextView;
    :sswitch_15
    const-string v21, "ShareVideo"

    const-string v22, "SHARE_VIDEO_MSG_MAXIMUM_DEVICE_CONNECTED"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1921
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    const v22, 0x7f0a006c

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x5

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-virtual/range {v21 .. v23}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 1922
    .local v13, "strMaxDevice":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v0, v13, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v18

    .line 1923
    .local v18, "toastMaxDevice":Landroid/widget/Toast;
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    .line 1924
    .local v17, "toastLayout1":Landroid/widget/LinearLayout;
    const/16 v21, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 1925
    .local v20, "tvMaxDevice":Landroid/widget/TextView;
    const/16 v21, 0x11

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setGravity(I)V

    .line 1926
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1673
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_4
        0x4 -> :sswitch_9
        0x5 -> :sswitch_3
        0x7 -> :sswitch_5
        0x8 -> :sswitch_6
        0x9 -> :sswitch_7
        0xa -> :sswitch_8
        0x10 -> :sswitch_a
        0x11 -> :sswitch_b
        0x12 -> :sswitch_c
        0x13 -> :sswitch_d
        0x14 -> :sswitch_e
        0x15 -> :sswitch_f
        0x17 -> :sswitch_10
        0x18 -> :sswitch_11
        0x19 -> :sswitch_12
        0x1a -> :sswitch_13
        0x1b -> :sswitch_14
        0x1c -> :sswitch_15
        0xc8 -> :sswitch_0
    .end sparse-switch
.end method
