.class Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;
.super Ljava/lang/Object;
.source "DeviceVolumeListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VolumeObject"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;
    .param p2, "x1"    # Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;-><init>(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)V

    return-void
.end method


# virtual methods
.method getCount()I
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mDeviceList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$400(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method getVolume(I)I
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 152
    const/4 v0, 0x0

    .line 154
    .local v0, "volume":I
    packed-switch p1, :pswitch_data_0

    .line 166
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->getMasterVolumeValue()I

    move-result v0

    .line 167
    sget-object v1, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " ----------- VolumeObject SPK_MODE_DEFAULT getVolume. volume: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :goto_0
    return v0

    .line 156
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/common/SUtils;->getClientVolumeValue(I)I

    move-result v0

    .line 157
    sget-object v1, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " ----------- VolumeObject SPK_MODE_LEFT getVolume. volume : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 161
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/common/SUtils;->getClientVolumeValue(I)I

    move-result v0

    .line 162
    sget-object v1, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " ----------- VolumeObject SPK_MODE_RIGHT getVolume. volume :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 154
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method setVolumeProgress(IIZ)V
    .locals 6
    .param p1, "index"    # I
    .param p2, "volume"    # I
    .param p3, "needChange"    # Z

    .prologue
    .line 175
    packed-switch p1, :pswitch_data_0

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 177
    :pswitch_0
    if-eqz p3, :cond_1

    .line 178
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    const/16 v4, 0x12c

    invoke-virtual {v3, v4, p2}, Lcom/sec/android/app/mv/player/common/SUtils;->setVolumeValue(II)V

    .line 181
    :cond_1
    sget-object v3, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ----------- VolumeObject setVolumeProgress. volume:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;->getCount()I

    move-result v0

    .line 185
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_0

    .line 186
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    .line 187
    .local v2, "seekBar":Landroid/widget/SeekBar;
    if-eqz v2, :cond_2

    .line 188
    invoke-virtual {v2, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 185
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 194
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "seekBar":Landroid/widget/SeekBar;
    :pswitch_1
    if-eqz p3, :cond_0

    .line 195
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    const/16 v4, 0x12d

    invoke-virtual {v3, v4, p2}, Lcom/sec/android/app/mv/player/common/SUtils;->setVolumeValue(II)V

    goto :goto_0

    .line 200
    :pswitch_2
    if-eqz p3, :cond_0

    .line 201
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    const/16 v4, 0x12e

    invoke-virtual {v3, v4, p2}, Lcom/sec/android/app/mv/player/common/SUtils;->setVolumeValue(II)V

    goto :goto_0

    .line 175
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
