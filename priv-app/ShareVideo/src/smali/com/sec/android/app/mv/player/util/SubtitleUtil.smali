.class public Lcom/sec/android/app/mv/player/util/SubtitleUtil;
.super Ljava/lang/Object;
.source "SubtitleUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;
    }
.end annotation


# static fields
.field private static final CLEAR_SUBTITLE:I = 0x1

.field public static final FALSE:I = 0x0

.field public static final SUBTITLE_COLOR_BLACK:I = -0x1000000

.field public static final SUBTITLE_COLOR_BLUE:I = -0xffff01

.field public static final SUBTITLE_COLOR_GREEN:I = -0xff8000

.field public static final SUBTITLE_COLOR_NONE:I = 0x0

.field public static final SUBTITLE_COLOR_WHITE:I = -0x1

.field public static SUBTITLE_SIZE_LARGE:I = 0x0

.field public static SUBTITLE_SIZE_MEDIUM:I = 0x0

.field public static SUBTITLE_SIZE_SMALL:I = 0x0

.field public static final SUBTITLE_TYPE_INBAND:I = 0x0

.field public static final SUBTITLE_TYPE_OUTBAND:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SubtitleUtil"

.field public static final TRUE:I = 0x1

.field public static final UNDEFINED:I = -0x1

.field private static final baseSubtitleDir:Ljava/lang/String; = "/storage/sdcard0/.SubTitle/"

.field private static final baseSubtitleFile:Ljava/lang/String; = "SubTitleFile"

.field private static final clientSubtitleDir:Ljava/lang/String;

.field private static mSubtitleLanguageIndex:I


# instance fields
.field private mBackgroundColor:I

.field private mContext:Landroid/content/Context;

.field private mCountSubtitleOn:I

.field private mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

.field private mFontName:Ljava/lang/String;

.field private mFontPackageName:Ljava/lang/String;

.field private mFontStringName:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mHasSubtitleFile:Z

.field private mIsMultiSubtitle:Z

.field private mMultivisionSubtitleDevice:Z

.field private mPrevSubtitleViewVisibility:I

.field private mRemoteSubTitledownloadThread:Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;

.field private mRemoteSubtitleFileExist:Z

.field private mSelectedSubtitleIndex:[I

.field private mServerSubtitleActivatedState:Z

.field private mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

.field private mSubtitle:Ljava/lang/String;

.field private mSubtitleActivation:I

.field private mSubtitleExt:[Ljava/lang/String;

.field private mSubtitleFilePath:Ljava/lang/String;

.field private mSubtitlePath:Ljava/lang/String;

.field private mSubtitleType:I

.field private mSubtitleUrl:Ljava/lang/String;

.field private mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

.field private mSyncTime:I

.field private mTextColor:I

.field private mTextSize:I

.field private mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

.field private mWatchNowSubtitleExist:Z

.field private mWatchNowSubtitleFilePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    const/16 v0, 0x1c

    sput v0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_LARGE:I

    .line 54
    const/16 v0, 0x16

    sput v0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_MEDIUM:I

    .line 56
    const/16 v0, 0x12

    sput v0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_SMALL:I

    .line 100
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleLanguageIndex:I

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Chord_ShareVideo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->clientSubtitleDir:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)V
    .locals 5
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;
    .param p2, "service"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitle:Ljava/lang/String;

    .line 44
    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    .line 46
    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    .line 48
    iput v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mPrevSubtitleViewVisibility:I

    .line 50
    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 58
    sget v0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_MEDIUM:I

    iput v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTextSize:I

    .line 74
    iput v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTextColor:I

    .line 76
    iput v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mBackgroundColor:I

    .line 78
    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontName:Ljava/lang/String;

    .line 80
    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontPackageName:Ljava/lang/String;

    .line 82
    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontStringName:Ljava/lang/String;

    .line 84
    iput v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSyncTime:I

    .line 86
    iput v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleActivation:I

    .line 88
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mMultivisionSubtitleDevice:Z

    .line 90
    iput-boolean v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mServerSubtitleActivatedState:Z

    .line 92
    iput-boolean v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    .line 98
    iput v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleType:I

    .line 102
    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    .line 107
    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 109
    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    .line 111
    iput-boolean v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mRemoteSubtitleFileExist:Z

    .line 117
    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;

    .line 119
    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 121
    iput-boolean v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mIsMultiSubtitle:Z

    .line 123
    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSelectedSubtitleIndex:[I

    .line 125
    iput v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mCountSubtitleOn:I

    .line 127
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ".smi"

    aput-object v1, v0, v4

    const-string v1, ".srt"

    aput-object v1, v0, v2

    const/4 v1, 0x2

    const-string v2, ".sub"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleExt:[Ljava/lang/String;

    .line 130
    iput-boolean v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mWatchNowSubtitleExist:Z

    .line 132
    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mWatchNowSubtitleFilePath:Ljava/lang/String;

    .line 656
    new-instance v0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil$1;-><init>(Lcom/sec/android/app/mv/player/util/SubtitleUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHandler:Landroid/os/Handler;

    .line 135
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    .line 136
    iput-object p2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_LARGE:I

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_MEDIUM:I

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_SMALL:I

    .line 143
    new-instance v0, Lcom/sec/android/app/mv/player/flipfont/FontList;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/flipfont/FontList;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    .line 147
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->C_USA:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->C_CANADA:Z

    if-eqz v0, :cond_1

    .line 148
    :cond_0
    invoke-virtual {p0, v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleActivation(Z)V

    .line 150
    :cond_1
    return-void
.end method

.method private static existFile(Ljava/lang/String;)Z
    .locals 2
    .param p0, "subTitleFileName"    # Ljava/lang/String;

    .prologue
    .line 596
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 597
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method private getHTMLString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 601
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 602
    :cond_0
    const-string v3, ""

    .line 621
    :goto_0
    return-object v3

    .line 604
    :cond_1
    const-string v3, ""

    .line 605
    .local v3, "subString":Ljava/lang/String;
    const-string v5, "\n"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 606
    const-string v5, "\n"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 607
    .local v4, "text":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 608
    .local v2, "strBuilder":Ljava/lang/StringBuilder;
    array-length v1, v4

    .line 610
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 611
    aget-object v5, v4, v0

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 612
    add-int/lit8 v5, v1, -0x1

    if-ge v0, v5, :cond_2

    .line 613
    const-string v5, "\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 610
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 616
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 617
    goto :goto_0

    .line 618
    .end local v0    # "i":I
    .end local v1    # "length":I
    .end local v2    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v4    # "text":[Ljava/lang/String;
    :cond_4
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private isMultiVisionAndSubtitleEnabled()Z
    .locals 1

    .prologue
    .line 643
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isMultiVisionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isMultivisionSubtitleShowingDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mServerSubtitleActivatedState:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSingleVisionAndActivated()Z
    .locals 1

    .prologue
    .line 647
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isSingleVisionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleActivated()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeHandlerMsg(I)V
    .locals 1
    .param p1, "msg"    # I

    .prologue
    .line 668
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 670
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 672
    :cond_0
    return-void
.end method


# virtual methods
.method public DeleteClientSubTitleFile()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 780
    new-instance v3, Ljava/io/File;

    sget-object v6, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->clientSubtitleDir:Ljava/lang/String;

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 781
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 782
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 783
    .local v2, "childFileList":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 784
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v1, v0, v4

    .line 785
    .local v1, "childFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 784
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 788
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "childFile":Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 791
    .end local v2    # "childFileList":[Ljava/io/File;
    :cond_1
    iput-object v7, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    .line 792
    iput-object v7, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 793
    return-void
.end method

.method public DeleteRemoteSubTitleFile()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 763
    new-instance v3, Ljava/io/File;

    const-string v6, "/storage/sdcard0/.SubTitle/"

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 764
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 765
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 766
    .local v2, "childFileList":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 767
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v1, v0, v4

    .line 768
    .local v1, "childFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 767
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 771
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "childFile":Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 774
    .end local v2    # "childFileList":[Ljava/io/File;
    :cond_1
    iput-object v7, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    .line 775
    iput-object v7, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 776
    return-void
.end method

.method public DownloadRemoteSubTitleFile(Ljava/lang/String;)V
    .locals 6
    .param p1, "subtitleUrl"    # Ljava/lang/String;

    .prologue
    .line 710
    const-string v3, "SubtitleUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DownloadRemoteSubTitleFile subtitleUrl: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 714
    new-instance v0, Ljava/io/File;

    const-string v3, "/storage/sdcard0/.SubTitle/"

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 715
    .local v0, "folder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 716
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 719
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleExt:[Ljava/lang/String;

    array-length v2, v3

    .line 721
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 722
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleExt:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 723
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/storage/sdcard0/.SubTitle/SubTitleFile"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleExt:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    .line 721
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 727
    :cond_2
    new-instance v3, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;

    .line 728
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->start()V

    .line 730
    return-void
.end method

.method public DownloadRemoteSubTitleFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "subtitleUrl"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 733
    const-string v1, "SubtitleUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DownloadRemoteSubTitleFile subtitleUrl: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 737
    new-instance v0, Ljava/io/File;

    const-string v1, "/storage/sdcard0/.SubTitle/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 738
    .local v0, "folder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 739
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 742
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/storage/sdcard0/.SubTitle/SubTitleFile"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    .line 744
    new-instance v1, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;

    .line 745
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->start()V

    .line 747
    return-void
.end method

.method public GetRemoteSubtitlePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 686
    const-string v0, "SubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GetRemoteSubtitlePath: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    return-object v0
.end method

.method public GetRemoteSubtitleUrl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 681
    const-string v0, "SubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GetRemoteSubtitleUrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    return-object v0
.end method

.method public RemoteSubTitledownloadThreadCheck()Z
    .locals 4

    .prologue
    .line 751
    :try_start_0
    const-string v1, "SubtitleUtil"

    const-string v2, "RemoteSubTitledownloadThreadCheck"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->join(J)V

    .line 753
    const-string v1, "SubtitleUtil"

    const-string v2, "RemoteSubTitledownloadThread join"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 758
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 754
    :catch_0
    move-exception v0

    .line 755
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 756
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public SetRemoteSubtitleUrl(Ljava/lang/String;)V
    .locals 3
    .param p1, "SubtitleUrl"    # Ljava/lang/String;

    .prologue
    .line 676
    const-string v0, "SubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SetRemoteSubtitleUrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 678
    return-void
.end method

.method public changeSubtitleFile(Ljava/lang/String;)Z
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 517
    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    .line 518
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->stopSubtitle()V

    .line 519
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    .line 524
    if-eqz p1, :cond_0

    const-string v2, "file://"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 525
    const/4 v2, 0x7

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 528
    :cond_0
    const-string v2, "SubtitleUtil"

    const-string v3, "changeSubtitleFile"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    if-eqz p1, :cond_1

    const-string v2, "smi"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "srt"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "sub"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 532
    const-string v1, "SubtitleUtil"

    const-string v2, "changeSubtitleFile. this file is not subtitle file extension"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    .line 534
    const-string v1, ""

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    .line 553
    :goto_0
    return v0

    .line 539
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->existFile(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 540
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    .line 546
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    move v0, v1

    .line 553
    goto :goto_0

    .line 550
    :cond_2
    const-string v1, "SubtitleUtil"

    const-string v2, "changeSubtitleFile. subtitle file not exist"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public checkExistOutbandSubtitle(Ljava/lang/String;)Z
    .locals 9
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 379
    if-nez p1, :cond_0

    .line 380
    const-string v7, "SubtitleUtil"

    const-string v8, "checkExistOutbandSubtitle() - path is null"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    :goto_0
    return v6

    .line 383
    :cond_0
    const-string v7, "SubtitleUtil"

    const-string v8, "checkExistOutbandSubtitle()"

    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    const/4 v0, 0x0

    .line 387
    .local v0, "bIsOutBandSubtitle":Z
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5}, Ljava/lang/String;-><init>()V

    .line 388
    .local v5, "subtitleFileName":Ljava/lang/String;
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 389
    .local v2, "smiFileName":Ljava/lang/String;
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    .line 390
    .local v3, "srtFileName":Ljava/lang/String;
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4}, Ljava/lang/String;-><init>()V

    .line 392
    .local v4, "subFileName":Ljava/lang/String;
    const/16 v7, 0x2e

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 393
    .local v1, "pos":I
    if-lez v1, :cond_1

    .line 394
    invoke-virtual {p1, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 397
    :cond_1
    const-string v7, ".smi"

    invoke-virtual {v5, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 398
    const-string v7, ".srt"

    invoke-virtual {v5, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 399
    const-string v7, ".sub"

    invoke-virtual {v5, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 401
    invoke-static {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->existFile(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 402
    const/4 v0, 0x1

    .line 403
    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    .line 415
    :goto_1
    if-eqz v0, :cond_5

    .line 416
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    .line 421
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    .line 422
    iget-boolean v6, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    goto :goto_0

    .line 404
    :cond_2
    invoke-static {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->existFile(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 405
    const/4 v0, 0x1

    .line 406
    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    goto :goto_1

    .line 407
    :cond_3
    invoke-static {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->existFile(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 408
    const/4 v0, 0x1

    .line 409
    invoke-virtual {p0, v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    goto :goto_1

    .line 411
    :cond_4
    const/4 v0, 0x0

    .line 412
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    goto :goto_1

    .line 418
    :cond_5
    iput-boolean v6, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    goto :goto_2
.end method

.method public checkExistWatchNowSubtitle()Z
    .locals 1

    .prologue
    .line 1082
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mWatchNowSubtitleExist:Z

    return v0
.end method

.method public checkRemoteSubtitleFile(Z)V
    .locals 0
    .param p1, "check"    # Z

    .prologue
    .line 691
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mRemoteSubtitleFileExist:Z

    .line 692
    return-void
.end method

.method public checkSubtitleValidty(Ljava/lang/String;)Z
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 699
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleExt:[Ljava/lang/String;

    array-length v1, v2

    .line 701
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 702
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleExt:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 703
    const/4 v2, 0x1

    .line 706
    :goto_1
    return v2

    .line 701
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 706
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public clearSubtitle()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 651
    const-string v0, "SubtitleUtil"

    const-string v1, "clearSubtitle E : "

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->removeHandlerMsg(I)V

    .line 653
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 654
    return-void
.end method

.method public clearTracks()V
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->clear()V

    .line 236
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 237
    return-void
.end method

.method public createSelectedSubtitleIndexArray(I)V
    .locals 3
    .param p1, "arraySize"    # I

    .prologue
    .line 1049
    const-string v0, "SubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createSelectedSubtitleIndexArray() :: arraySize = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSelectedSubtitleIndex:[I

    .line 1051
    return-void
.end method

.method public deleteSubtitleFile()V
    .locals 2

    .prologue
    .line 561
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    if-eqz v1, :cond_1

    .line 562
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleFilePathExist()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 563
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 564
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 565
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 568
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    .line 569
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    .line 571
    :cond_1
    return-void
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mBackgroundColor:I

    return v0
.end method

.method public getColorValue(I)Ljava/lang/String;
    .locals 3
    .param p1, "menu"    # I

    .prologue
    .line 163
    const/4 v0, 0x0

    .line 164
    .local v0, "curColor":I
    const/4 v1, 0x5

    if-ne p1, v1, :cond_0

    .line 165
    iget v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTextColor:I

    .line 168
    :goto_0
    sparse-switch v0, :sswitch_data_0

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a005e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    return-object v1

    .line 167
    :cond_0
    iget v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mBackgroundColor:I

    goto :goto_0

    .line 170
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 172
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 174
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 176
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 168
    :sswitch_data_0
    .sparse-switch
        -0x1000000 -> :sswitch_0
        -0xffff01 -> :sswitch_1
        -0xff8000 -> :sswitch_2
        -0x1 -> :sswitch_3
    .end sparse-switch
.end method

.method public getFontList()[Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 904
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    if-eqz v3, :cond_0

    .line 905
    const-string v3, "SubtitleUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getFontList() :: mFontList.getCount() = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getCount()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_1

    .line 917
    :cond_0
    return-object v2

    .line 909
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getCount()I

    move-result v3

    new-array v2, v3, [Ljava/lang/String;

    .line 910
    .local v2, "retString":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getCount()I

    move-result v0

    .line 911
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 912
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getFontName(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 911
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getFontName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1015
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontName:Ljava/lang/String;

    return-object v0
.end method

.method public getFontPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1023
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getFontStringName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontStringName:Ljava/lang/String;

    return-object v0
.end method

.method public getHasSubtitleFile()Z
    .locals 1

    .prologue
    .line 1011
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    return v0
.end method

.method public getIsMultiSubtitle()Z
    .locals 1

    .prologue
    .line 855
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mIsMultiSubtitle:Z

    return v0
.end method

.method public getMultiSelectSubtitle()I
    .locals 1

    .prologue
    .line 871
    iget v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mCountSubtitleOn:I

    return v0
.end method

.method public getSelectedFont()Ljava/lang/String;
    .locals 4

    .prologue
    .line 941
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0093

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 942
    .local v0, "retStr":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    if-eqz v2, :cond_0

    .line 943
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontStringName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 944
    .local v1, "tempString":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 945
    move-object v0, v1

    .line 948
    .end local v1    # "tempString":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getSelectedFontIndex()I
    .locals 7

    .prologue
    .line 921
    const/4 v2, 0x0

    .line 922
    .local v2, "retValue":I
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    if-eqz v4, :cond_2

    .line 923
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontName:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontName:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v3, v2

    .line 937
    .end local v2    # "retValue":I
    .local v3, "retValue":I
    :goto_0
    return v3

    .line 927
    .end local v3    # "retValue":I
    .restart local v2    # "retValue":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getCount()I

    move-result v0

    .line 928
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 929
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontName:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v5, v1}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getTypefaceName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 930
    move v2, v1

    .line 931
    const-string v4, "SubtitleUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getSelectedFont() :: return = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_2
    move v3, v2

    .line 937
    .end local v2    # "retValue":I
    .restart local v3    # "retValue":I
    goto :goto_0

    .line 928
    .end local v3    # "retValue":I
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    .restart local v2    # "retValue":I
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getSelectedSubtitleIndex()[I
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSelectedSubtitleIndex:[I

    return-object v0
.end method

.method public getSubTitleExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 495
    if-eqz p1, :cond_0

    .line 496
    const-string v1, "smi"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 497
    const-string v0, ".smi"

    .line 507
    :cond_0
    :goto_0
    return-object v0

    .line 498
    :cond_1
    const-string v1, "srt"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 499
    const-string v0, ".srt"

    goto :goto_0

    .line 500
    :cond_2
    const-string v1, "sub"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 501
    const-string v0, ".sub"

    goto :goto_0

    .line 503
    :cond_3
    const-string v1, "SubtitleUtil"

    const-string v2, "This file extension is not supported"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getSubtitleCount()I
    .locals 2

    .prologue
    .line 225
    const-string v0, "SubtitleUtil"

    const-string v1, "getSubtitleCount()"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-nez v0, :cond_0

    .line 227
    const/4 v0, 0x0

    .line 229
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getSubtitleFilePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 486
    const-string v0, "SubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSubtitleFile : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getSubtitleLanguage(I)Ljava/lang/String;
    .locals 7
    .param p1, "index"    # I

    .prologue
    const v6, 0x7f0a0093

    .line 257
    const-string v1, ""

    .line 258
    .local v1, "retValue":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-nez v4, :cond_0

    move-object v4, v1

    .line 300
    :goto_0
    return-object v4

    .line 260
    :cond_0
    const/4 v4, -0x1

    if-ge v4, p1, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    iget-object v4, v4, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, p1, :cond_1

    .line 261
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    iget-object v4, v4, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "retValue":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 263
    .restart local v1    # "retValue":Ljava/lang/String;
    :cond_1
    const-string v4, "eng"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 264
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0a00cd

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 265
    :cond_2
    const-string v4, "kor"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 266
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0a00d2

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 267
    :cond_3
    const-string v4, "chi"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 268
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0a00cb

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 269
    :cond_4
    const-string v4, "jpn"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 270
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0a00d1

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 271
    :cond_5
    const-string v4, "fre"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 272
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0a00cf

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 273
    :cond_6
    const-string v4, "ger"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 274
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0a00cc

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 275
    :cond_7
    const-string v4, "spa"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 276
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0a00ce

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 277
    :cond_8
    const-string v4, "ita"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 278
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    const v5, 0x7f0a00d0

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 280
    :cond_9
    invoke-static {}, Ljava/util/Locale;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v2

    .line 282
    .local v2, "temp":[Ljava/util/Locale;
    if-eqz v2, :cond_b

    array-length v4, v2

    if-lez v4, :cond_b

    .line 283
    array-length v3, v2

    .line 285
    .local v3, "temp_size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v3, :cond_b

    .line 286
    aget-object v4, v2, v0

    invoke-virtual {v4}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 287
    const-string v4, "SubtitleUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getISO3Language() "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v2, v0

    invoke-virtual {v6}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " retValue : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    aget-object v4, v2, v0

    invoke-virtual {v4}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 291
    :cond_a
    const-string v4, "SubtitleUtil"

    const-string v5, "Not Founud!!!"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 295
    .end local v0    # "i":I
    .end local v3    # "temp_size":I
    :cond_b
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_c

    .line 296
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    move-object v4, v1

    .line 300
    goto/16 :goto_0

    .line 298
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public getSubtitleLanguageIndex()I
    .locals 1

    .prologue
    .line 240
    sget v0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleLanguageIndex:I

    return v0
.end method

.method public getSubtitleType()I
    .locals 3

    .prologue
    .line 511
    const-string v0, "SubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSubtitleType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    iget v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleType:I

    return v0
.end method

.method public getSyncTime()I
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSyncTime:I

    return v0
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTextColor:I

    return v0
.end method

.method public getTextSize()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTextSize:I

    return v0
.end method

.method public getTextSizeValue()Ljava/lang/String;
    .locals 3

    .prologue
    const v2, 0x7f0a0044

    .line 316
    iget v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTextSize:I

    sget v1, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_LARGE:I

    if-ne v0, v1, :cond_0

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0041

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 323
    :goto_0
    return-object v0

    .line 318
    :cond_0
    iget v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTextSize:I

    sget v1, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_MEDIUM:I

    if-ne v0, v1, :cond_1

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 320
    :cond_1
    iget v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTextSize:I

    sget v1, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_SMALL:I

    if-ne v0, v1, :cond_2

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0087

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 323
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getWatchNowSubtitleFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mWatchNowSubtitleFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public increaseSelectSubtitle()V
    .locals 1

    .prologue
    .line 867
    iget v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mCountSubtitleOn:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mCountSubtitleOn:I

    .line 868
    return-void
.end method

.method public initFontList()V
    .locals 1

    .prologue
    .line 1043
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/flipfont/FontList;->fontListInit()V

    .line 1044
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/flipfont/FontList;->loadTypefaces()V

    .line 1046
    return-void
.end method

.method public initInbandSubtitle()V
    .locals 2

    .prologue
    .line 426
    const-string v0, "SubtitleUtil"

    const-string v1, "initInbandSubtitle"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    .line 428
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setInbandSubtitleType()V

    .line 429
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    .line 430
    return-void
.end method

.method public initSubtitle()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 449
    const-string v1, "SubtitleUtil"

    const-string v2, "initSubtitle"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleFilePathExist()Z

    move-result v1

    if-nez v1, :cond_0

    .line 451
    const-string v0, "SubtitleUtil"

    const-string v1, "initSubtitle() - mSubttitleFile NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    const/4 v0, 0x0

    .line 455
    :goto_0
    return v0

    .line 454
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->initSubtitle(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public isMultivisionSubtitleShowingDevice()Z
    .locals 1

    .prologue
    .line 363
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mMultivisionSubtitleDevice:Z

    return v0
.end method

.method public isRemoteSubtitleFile()Z
    .locals 1

    .prologue
    .line 695
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mRemoteSubtitleFileExist:Z

    return v0
.end method

.method public isSelectedSubtitleLanguage(Ljava/lang/String;)Z
    .locals 4
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 304
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-nez v2, :cond_0

    move v2, v3

    .line 312
    :goto_0
    return v2

    .line 305
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 307
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 308
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 309
    const/4 v2, 0x1

    goto :goto_0

    .line 307
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v2, v3

    .line 312
    goto :goto_0
.end method

.method public isSubtitleActivated()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 328
    iget v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleActivation:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSubtitleActivationUndefined()Z
    .locals 2

    .prologue
    .line 332
    iget v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleActivation:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSubtitleFilePathExist()Z
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSubtitleViewVisible()Z
    .locals 1

    .prologue
    .line 1086
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveSubtitleViewUp()V
    .locals 1

    .prologue
    .line 1107
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-eqz v0, :cond_0

    .line 1108
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->moveUp()V

    .line 1109
    :cond_0
    return-void
.end method

.method public resetSelectSubtitle()V
    .locals 1

    .prologue
    .line 863
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mCountSubtitleOn:I

    .line 864
    return-void
.end method

.method public resetSubtitleLang()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 892
    const-string v0, "SubtitleUtil"

    const-string v1, "resetSubtitleLang()"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    iput v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mCountSubtitleOn:I

    .line 894
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mIsMultiSubtitle:Z

    .line 895
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSelectedSubtitleIndex:[I

    .line 896
    sput v2, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleLanguageIndex:I

    .line 897
    return-void
.end method

.method public resetSubtitleSettings()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 459
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    .line 460
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    .line 461
    iput v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSyncTime:I

    .line 462
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitle:Ljava/lang/String;

    .line 463
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->clearTracks()V

    .line 464
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->resetSubtitleLang()V

    .line 465
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleType:I

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetSubtitle()V

    .line 467
    return-void
.end method

.method public selectOutbandSubtitle()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 437
    const-string v1, "SubtitleUtil"

    const-string v2, "selectOutbandSubtitle"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleFilePathExist()Z

    move-result v1

    if-nez v1, :cond_0

    .line 439
    const-string v0, "SubtitleUtil"

    const-string v1, "selectOutbandSubtitle() - mSubttitleFile NULL"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const/4 v0, 0x0

    .line 444
    :goto_0
    return v0

    .line 443
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->addOutbandSubTitle(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public setBackgroundColor(I)V
    .locals 2
    .param p1, "setColor"    # I

    .prologue
    .line 195
    iput p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mBackgroundColor:I

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    iget v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mBackgroundColor:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/SubtitleView;->updateSubTitleBackgroundColor(I)V

    .line 198
    :cond_0
    return-void
.end method

.method public setFont(Ljava/lang/String;)V
    .locals 6
    .param p1, "fontName"    # Ljava/lang/String;

    .prologue
    .line 952
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    if-eqz v3, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 953
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getCount()I

    move-result v0

    .line 954
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 955
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getFontName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 956
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getTypefaceName(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontName:Ljava/lang/String;

    .line 957
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getFontPackageName(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontPackageName:Ljava/lang/String;

    .line 958
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getFontStringName(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontStringName:Ljava/lang/String;

    .line 959
    const-string v3, "SubtitleUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setFont(1) :: mFontName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mFontPackageName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontPackageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mFontStringName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontStringName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getFont(I)Landroid/graphics/Typeface;

    move-result-object v2

    .line 962
    .local v2, "tf":Landroid/graphics/Typeface;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 963
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/mv/player/view/SubtitleView;->updateSubTitleFont(Landroid/graphics/Typeface;)V

    .line 969
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "tf":Landroid/graphics/Typeface;
    :cond_0
    return-void

    .line 954
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setFont(Ljava/lang/String;Landroid/widget/TextView;)V
    .locals 6
    .param p1, "fontName"    # Ljava/lang/String;
    .param p2, "tv"    # Landroid/widget/TextView;

    .prologue
    .line 994
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    if-eqz v3, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz p2, :cond_0

    .line 995
    const-string v3, "SubtitleUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setFont(4) :: fontName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getCount()I

    move-result v0

    .line 998
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 999
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getFontName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1000
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getFont(I)Landroid/graphics/Typeface;

    move-result-object v2

    .line 1001
    .local v2, "tf":Landroid/graphics/Typeface;
    if-eqz v2, :cond_0

    .line 1002
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1008
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "tf":Landroid/graphics/Typeface;
    :cond_0
    return-void

    .line 998
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setFont(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "fontName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 972
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    if-eqz v1, :cond_0

    .line 973
    const-string v1, "SubtitleUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setFont(2) :: fontName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", packageName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getFont(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 976
    .local v0, "tf":Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 977
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->updateSubTitleFont(Landroid/graphics/Typeface;)V

    .line 980
    .end local v0    # "tf":Landroid/graphics/Typeface;
    :cond_0
    return-void
.end method

.method public setFont(Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;)V
    .locals 4
    .param p1, "fontName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "tv"    # Landroid/widget/TextView;

    .prologue
    .line 983
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    if-eqz v1, :cond_0

    if-eqz p3, :cond_0

    .line 984
    const-string v1, "SubtitleUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setFont(3) :: fontName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", packageName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 986
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontList:Lcom/sec/android/app/mv/player/flipfont/FontList;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getFont(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 987
    .local v0, "tf":Landroid/graphics/Typeface;
    if-eqz p3, :cond_0

    if-eqz v0, :cond_0

    .line 988
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 991
    .end local v0    # "tf":Landroid/graphics/Typeface;
    :cond_0
    return-void
.end method

.method public setFontNameForPreference(Ljava/lang/String;)V
    .locals 3
    .param p1, "fontName"    # Ljava/lang/String;

    .prologue
    .line 1027
    const-string v0, "SubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFontNameForPreference() :: fontName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontName:Ljava/lang/String;

    .line 1029
    return-void
.end method

.method public setFontPackageNameForPreference(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1037
    const-string v0, "SubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFontPackageNameForPreference() :: packageName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontPackageName:Ljava/lang/String;

    .line 1039
    return-void
.end method

.method public setFontStringNameForPreference(Ljava/lang/String;)V
    .locals 3
    .param p1, "fontStringName"    # Ljava/lang/String;

    .prologue
    .line 1032
    const-string v0, "SubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFontStringNameForPreference() :: fontStringName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1033
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mFontStringName:Ljava/lang/String;

    .line 1034
    return-void
.end method

.method public setInbandSubtitleType()V
    .locals 1

    .prologue
    .line 433
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleType:I

    .line 434
    return-void
.end method

.method public setIsMultiSubtitle(Z)V
    .locals 0
    .param p1, "on"    # Z

    .prologue
    .line 859
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mIsMultiSubtitle:Z

    .line 860
    return-void
.end method

.method public setMultiSelectSubtitle(I)V
    .locals 0
    .param p1, "CountSubtitleOn"    # I

    .prologue
    .line 900
    iput p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mCountSubtitleOn:I

    .line 901
    return-void
.end method

.method public setMultiSelectedSubtitleIndex([I)V
    .locals 5
    .param p1, "multiSubtitleIndex"    # [I

    .prologue
    .line 879
    if-nez p1, :cond_1

    .line 880
    const-string v2, "SubtitleUtil"

    const-string v3, "setMultiSelectSubtitle multiSubtitleIndex is null return"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    :cond_0
    return-void

    .line 883
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSelectedSubtitleIndex:[I

    .line 885
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSelectedSubtitleIndex:[I

    array-length v1, v2

    .line 886
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 887
    const-string v2, "SubtitleUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setMultiSelectSubtitle mSelectedSubtitleIndex["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSelectedSubtitleIndex:[I

    aget v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 886
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setMultivisionSubtitleShowingDevice(Z)V
    .locals 0
    .param p1, "active"    # Z

    .prologue
    .line 355
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mMultivisionSubtitleDevice:Z

    .line 356
    return-void
.end method

.method public setSelectedSubtitleIndex(II)V
    .locals 1
    .param p1, "pos"    # I
    .param p2, "value"    # I

    .prologue
    .line 1054
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSelectedSubtitleIndex:[I

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSelectedSubtitleIndex:[I

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 1055
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSelectedSubtitleIndex:[I

    aput p2, v0, p1

    .line 1057
    :cond_0
    return-void
.end method

.method public setServerSubtitleActivationState(Z)V
    .locals 0
    .param p1, "serverActivated"    # Z

    .prologue
    .line 359
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mServerSubtitleActivatedState:Z

    .line 360
    return-void
.end method

.method public setSubtitleActivation(Z)V
    .locals 3
    .param p1, "active"    # Z

    .prologue
    .line 367
    const-string v0, "SubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSubtitleActiveForPreference() :: active = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleActivation:I

    .line 370
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setServerSubtitleActivationState(Z)V

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendMessageServerSubtitleActivatedState()V

    .line 376
    :cond_0
    return-void

    .line 368
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSubtitleActive(Z)V
    .locals 4
    .param p1, "active"    # Z

    .prologue
    .line 337
    const-string v1, "SubtitleUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSubtitleActive = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", oldActive = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleActivation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    iget v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleActivation:I

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-ne v2, v1, :cond_1

    .line 352
    :goto_1
    return-void

    .line 339
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 342
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleActivation(Z)V

    .line 344
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleActivated()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 345
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->startSubtitle()V

    .line 346
    const/4 v0, 0x0

    .line 351
    .local v0, "visibility":I
    :goto_2
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleViewVisibility(I)V

    goto :goto_1

    .line 348
    .end local v0    # "visibility":I
    :cond_2
    const/4 v0, 0x4

    .restart local v0    # "visibility":I
    goto :goto_2
.end method

.method public setSubtitleFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 470
    const-string v0, "SubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSubtitleFile : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    .line 473
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    .line 478
    :goto_0
    return-void

    .line 475
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    .line 476
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setSubtitleFilePath(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 481
    const-string v0, "SubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSubtitleFilePath : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleFilePath:Ljava/lang/String;

    .line 483
    return-void
.end method

.method public setSubtitleLanguageIndex(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 244
    sput p1, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleLanguageIndex:I

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setSubtitleLanguageIndex(I)V

    .line 246
    return-void
.end method

.method public setSubtitleSyncTime()V
    .locals 2

    .prologue
    .line 557
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSyncTime:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setSubtitleSyncTime(I)V

    .line 558
    return-void
.end method

.method public setSubtitleView(Lcom/sec/android/app/mv/player/view/SubtitleView;)V
    .locals 0
    .param p1, "subtitleView"    # Lcom/sec/android/app/mv/player/view/SubtitleView;

    .prologue
    .line 153
    if-eqz p1, :cond_0

    .line 154
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    .line 156
    :cond_0
    return-void
.end method

.method public setSubtitleViewVisibility(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 1090
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-eqz v0, :cond_2

    .line 1091
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/view/SubtitleView;->setVisibility(I)V

    .line 1093
    iget v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mPrevSubtitleViewVisibility:I

    if-eq v0, p1, :cond_0

    .line 1094
    if-nez p1, :cond_1

    .line 1095
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/mv/player/common/VUtils$Color;->BLACK:Lcom/sec/android/app/mv/player/common/VUtils$Color;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/VUtils;->changeWindowBackgroundColor(Lcom/sec/android/app/mv/player/common/VUtils$Color;)V

    .line 1100
    :cond_0
    :goto_0
    iput p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mPrevSubtitleViewVisibility:I

    .line 1104
    :goto_1
    return-void

    .line 1097
    :cond_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/mv/player/common/VUtils$myView;->SUBTITLE_VIEW:Lcom/sec/android/app/mv/player/common/VUtils$myView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/VUtils;->setNullBackgroundColor(Lcom/sec/android/app/mv/player/common/VUtils$myView;)V

    goto :goto_0

    .line 1102
    :cond_2
    const-string v0, "SubtitleUtil"

    const-string v1, "setSubtitleViewVisibility : mSubtitleView is NULL!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setSyncTime(I)V
    .locals 0
    .param p1, "time"    # I

    .prologue
    .line 217
    iput p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSyncTime:I

    .line 218
    return-void
.end method

.method public setTextColor(I)V
    .locals 2
    .param p1, "setColor"    # I

    .prologue
    .line 184
    iput p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTextColor:I

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    iget v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTextColor:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/SubtitleView;->updateSubTitleColor(I)V

    .line 188
    :cond_0
    return-void
.end method

.method public setTextSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 205
    iput p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTextSize:I

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    iget v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTextSize:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/SubtitleView;->updateSubTitleSize(I)V

    .line 208
    :cond_0
    return-void
.end method

.method public setTextSizeForPreference(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 211
    iput p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTextSize:I

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    iget v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTextSize:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/SubtitleView;->updateSubTitleSize(I)V

    .line 214
    :cond_0
    return-void
.end method

.method public setTimedTextTrack(Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;)V
    .locals 2
    .param p1, "track"    # Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .prologue
    .line 249
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    sget v0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleLanguageIndex:I

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 251
    :cond_0
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleLanguageIndex:I

    .line 252
    const-string v0, "SubtitleUtil"

    const-string v1, "setTimedTextTrack mSubtitleLanguageIndex is out of index reset mSubtitleLanguageIndex value!!"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :cond_1
    return-void
.end method

.method public setWatchNowSubtitleFilePath(Ljava/lang/String;)V
    .locals 4
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 1061
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mWatchNowSubtitleExist:Z

    .line 1062
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mWatchNowSubtitleFilePath:Ljava/lang/String;

    .line 1064
    if-eqz p1, :cond_0

    .line 1065
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1066
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1067
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mWatchNowSubtitleFilePath:Ljava/lang/String;

    .line 1068
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mWatchNowSubtitleExist:Z

    .line 1069
    const-string v1, "SubtitleUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setWatchNowSubtitleFilePath() : mWatchNowSubtitleFilePath = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mWatchNowSubtitleFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1074
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    const-string v1, "SubtitleUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setWatchNowSubtitleFilePath() : mWatchNowSubtitleExist = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mWatchNowSubtitleExist:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1075
    return-void

    .line 1071
    .restart local v0    # "file":Ljava/io/File;
    :cond_1
    const-string v1, "SubtitleUtil"

    const-string v2, "setWatchNowSubtitleFilePath() : subtitle does not exist"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startSubtitle()V
    .locals 3

    .prologue
    .line 574
    const-string v0, "SubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startSubtitle E mHasSubtitleFile = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mPrevSubtitleViewVisibility:I

    .line 577
    return-void
.end method

.method public stopSubtitle()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 580
    const-string v0, "SubtitleUtil"

    const-string v1, "stopSubtitle E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mHasSubtitleFile:Z

    .line 582
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    .line 583
    iput v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSyncTime:I

    .line 584
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->resetSubtitleLang()V

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-eqz v0, :cond_0

    .line 587
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleViewVisibility(I)V

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/SubtitleView;->updateSubTitle(Ljava/lang/String;)V

    .line 591
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->clearTracks()V

    .line 592
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mPrevSubtitleViewVisibility:I

    .line 593
    return-void
.end method

.method public updateSubtitle(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 625
    const-string v0, "SubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateSubtitle E text="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->removeHandlerMsg(I)V

    .line 627
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-nez v0, :cond_0

    .line 640
    :goto_0
    return-void

    .line 631
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSingleVisionAndActivated()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isMultiVisionAndSubtitleEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 632
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleViewVisibility(I)V

    .line 637
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getHTMLString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitle:Ljava/lang/String;

    .line 639
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->mSubtitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/SubtitleView;->updateSubTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 634
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleViewVisibility(I)V

    goto :goto_0
.end method
