.class public Lcom/sec/android/app/mv/player/common/PlaylistUtils;
.super Ljava/lang/Object;
.source "PlaylistUtils.java"


# static fields
.field private static mUniqueInstance:Lcom/sec/android/app/mv/player/common/PlaylistUtils;


# instance fields
.field private CurPlayingIndex:I

.field private final TAG:Ljava/lang/String;

.field private mPlaylistList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mUniqueInstance:Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-string v0, "Playlist"

    iput-object v0, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->TAG:Ljava/lang/String;

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->CurPlayingIndex:I

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    .line 34
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mUniqueInstance:Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mUniqueInstance:Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    .line 39
    :cond_0
    sget-object v0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mUniqueInstance:Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    return-object v0
.end method

.method private setResumePosition(Lcom/sec/android/app/mv/player/type/PlaylistItem;)V
    .locals 8
    .param p1, "item"    # Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .prologue
    .line 213
    const/4 v3, 0x0

    .line 214
    .local v3, "uri":Landroid/net/Uri;
    const-wide/16 v0, -0x1

    .line 215
    .local v0, "_id":J
    const/4 v2, -0x1

    .line 216
    .local v2, "resumePos":I
    invoke-static {}, Lcom/sec/android/app/mv/player/db/VideoDB;->getInstance()Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v4

    .line 218
    .local v4, "videoDB":Lcom/sec/android/app/mv/player/db/VideoDB;
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J

    move-result-wide v0

    .line 219
    sget-object v5, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 220
    invoke-virtual {v4, v3}, Lcom/sec/android/app/mv/player/db/VideoDB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v6

    long-to-int v2, v6

    .line 222
    invoke-virtual {p1, v2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->setResumePos(I)V

    .line 223
    return-void
.end method


# virtual methods
.method public add(JLjava/lang/String;JLjava/lang/String;II)V
    .locals 10
    .param p1, "videoId"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "duration"    # J
    .param p6, "path"    # Ljava/lang/String;
    .param p7, "isPlayed"    # I
    .param p8, "resumePos"    # I

    .prologue
    .line 98
    iget-object v9, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    move-wide v1, p1

    move-object v3, p3

    move-wide v4, p4

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/mv/player/type/PlaylistItem;-><init>(JLjava/lang/String;JLjava/lang/String;II)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    return-void
.end method

.method public add(Ljava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p1, "playlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistItem;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .line 103
    .local v9, "i":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->contains(J)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 104
    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J

    move-result-wide v1

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getDuration()J

    move-result-wide v4

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getIsPlayed()I

    move-result v7

    const/4 v8, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->add(JLjava/lang/String;JLjava/lang/String;II)V

    goto :goto_0

    .line 106
    :cond_0
    invoke-direct {p0, v9}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->setResumePosition(Lcom/sec/android/app/mv/player/type/PlaylistItem;)V

    .line 107
    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J

    move-result-wide v1

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getDuration()J

    move-result-wide v4

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getIsPlayed()I

    move-result v7

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getResumePos()I

    move-result v8

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->add(JLjava/lang/String;JLjava/lang/String;II)V

    goto :goto_0

    .line 110
    .end local v9    # "i":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    :cond_1
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 196
    return-void
.end method

.method public contains(J)Z
    .locals 7
    .param p1, "videoId"    # J

    .prologue
    .line 125
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getList()Ljava/util/ArrayList;

    move-result-object v2

    .line 127
    .local v2, "originalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistItem;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .line 128
    .local v0, "i":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J

    move-result-wide v4

    cmp-long v3, p1, v4

    if-nez v3, :cond_0

    .line 129
    const/4 v3, 0x1

    .line 132
    .end local v0    # "i":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getCurPlayingIndex()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->CurPlayingIndex:I

    return v0
.end method

.method public getIndexOf(J)I
    .locals 5
    .param p1, "videoId"    # J

    .prologue
    .line 143
    iget-object v2, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .line 144
    .local v0, "i":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 145
    iget-object v2, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 148
    .end local v0    # "i":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    :goto_0
    return v2

    :cond_1
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getResumePos(I)I
    .locals 3
    .param p1, "i"    # I

    .prologue
    .line 173
    const/4 v1, 0x0

    .line 175
    .local v1, "pos":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getResumePos()I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 179
    :goto_0
    return v1

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVideoID(I)J
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 158
    const-wide/16 v2, -0x1

    .line 160
    .local v2, "id":J
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 164
    :goto_0
    return-wide v2

    .line 161
    :catch_0
    move-exception v0

    .line 162
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public print()V
    .locals 6

    .prologue
    .line 199
    const-string v2, "Playlist"

    const-string v3, "---------"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    iget-object v2, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .line 201
    .local v0, "i":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    const-string v2, "Playlist"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "--------- index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getResumePos()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 204
    .end local v0    # "i":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    :cond_0
    const-string v2, "Playlist"

    const-string v3, "---------"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    return-void
.end method

.method public remove(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->print()V

    .line 115
    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object v2

    invoke-static {p1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 117
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 118
    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 121
    .end local v0    # "i":I
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->print()V

    .line 122
    return-void
.end method

.method public removeDeletedIdsFromPlayList(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 63
    const/4 v1, 0x0

    .line 65
    .local v1, "uri":Landroid/net/Uri;
    const/4 v11, 0x0

    .line 66
    .local v11, "plType":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    const/4 v8, 0x0

    .line 68
    .local v8, "cur":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 69
    .local v9, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/mv/player/type/PlaylistItem;>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 71
    .local v10, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistItem;>;"
    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 72
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "plType":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    check-cast v11, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .line 73
    .restart local v11    # "plType":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    invoke-virtual {v11}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J

    move-result-wide v6

    .line 74
    .local v6, "_id":J
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 75
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 77
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 78
    :cond_1
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    :cond_2
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 83
    const/4 v8, 0x0

    goto :goto_0

    .line 87
    .end local v6    # "_id":J
    :cond_3
    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 90
    :cond_4
    return-void
.end method

.method public setCurPlayingIndex(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->CurPlayingIndex:I

    .line 44
    return-void
.end method

.method public setResumePos(J)V
    .locals 3
    .param p1, "resumePos"    # J

    .prologue
    .line 184
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->CurPlayingIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    long-to-int v2, p1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->setResumePos(I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :goto_0
    return-void

    .line 185
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->mPlaylistList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
