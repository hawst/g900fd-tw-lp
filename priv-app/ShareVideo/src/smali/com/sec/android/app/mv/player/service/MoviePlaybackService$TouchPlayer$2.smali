.class Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)V
    .locals 0

    .prologue
    .line 1674
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 10
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1676
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer :: OnPreparedListener()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1678
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isStateViewVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1680
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideStateView()V

    .line 1683
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v2

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoWidth:I
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$802(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 1684
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v2

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoHeight:I
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$2102(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 1686
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TouchPlayer :: OnPreparedListener(). mVideoWidth : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v3, v3, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoWidth:I
    invoke-static {v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ,mVideoHeight : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v3, v3, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoHeight:I
    invoke-static {v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$2100(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ,mPosition : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getResumePosition()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ,isPausedByUser : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPausedByUser()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1689
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    if-eqz v1, :cond_1

    .line 1690
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 1692
    .local v0, "sh":Landroid/view/SurfaceHolder;
    if-eqz v0, :cond_5

    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getSurfaceExists()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1693
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->requestLayout()V

    .line 1700
    .end local v0    # "sh":Landroid/view/SurfaceHolder;
    :cond_1
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    .line 1701
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z
    invoke-static {v1, v6}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2202(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;Z)Z

    .line 1703
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    if-eqz v1, :cond_2

    .line 1704
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer :: OnPreparedListener(): mTrackInfoUtil is not null"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1705
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->resetTrackInfo()V

    .line 1708
    :cond_2
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1709
    const-string v1, "MoviePlaybackService"

    const-string v2, "mApp.mSubtitleUtil.getIsMultiSubtitle()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1710
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Landroid/media/MediaPlayer;

    move-result-object v1

    const/16 v2, 0x5de

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setParameter(II)Z

    .line 1713
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z
    invoke-static {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2400(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1714
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TouchPlayer :: OnPreparedListener() : mIsOutbandSubtitle: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z
    invoke-static {v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2400(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1715
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2500(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->addOutbandSubTitle(Ljava/lang/String;)V

    .line 1725
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setSoundAliveMode()V

    .line 1727
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getResumePosition()J

    move-result-wide v2

    cmp-long v1, v2, v8

    if-ltz v1, :cond_9

    .line 1728
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getResumePosition()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->seek(J)J

    .line 1730
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPausedByUser()Z

    move-result v1

    if-nez v1, :cond_8

    .line 1731
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1732
    const-string v1, "MoviePlaybackService"

    const-string v2, "MultiVision_prepareListener_pause()_1"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1733
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->pause()V

    .line 1734
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    .line 1756
    :goto_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1757
    const-string v1, "MoviePlaybackService"

    const-string v2, "PreparedListener state, send to READY status to multivision server"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1758
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/common/SUtils;->sendPlayerState(I)V

    .line 1760
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v2, 0x85

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)V

    .line 1761
    :goto_2
    return-void

    .line 1695
    .restart local v0    # "sh":Landroid/view/SurfaceHolder;
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->resetPlayer()V

    goto :goto_2

    .line 1717
    .end local v0    # "sh":Landroid/view/SurfaceHolder;
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->initTrackInfoUtil()V

    goto :goto_0

    .line 1736
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->play()V

    .line 1738
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    goto :goto_1

    .line 1741
    :cond_8
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    .line 1743
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setWakeMode(Z)V

    goto :goto_1

    .line 1746
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v1, v8, v9}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->seek(J)J

    .line 1747
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1748
    const-string v1, "MoviePlaybackService"

    const-string v2, "MultiVision_prepareListener_pause()_2"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1749
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->pause()V

    .line 1750
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    goto :goto_1

    .line 1752
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->play()V

    .line 1753
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    goto :goto_1
.end method
