.class public interface abstract Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;
.super Ljava/lang/Object;
.source "MVPlayerServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnMessageResponseListener"
.end annotation


# virtual methods
.method public abstract onChangeToMultiVision(I)V
.end method

.method public abstract onChangeToSingleVision(I)V
.end method

.method public abstract onClientPause(I)V
.end method

.method public abstract onClientPlay(I)V
.end method

.method public abstract onExitMVGroup(I)V
.end method

.method public abstract onGetPlayPosition(I)V
.end method

.method public abstract onGetPlayerState(I)V
.end method

.method public abstract onHide(I)V
.end method

.method public abstract onJoinMVGroup(I)V
.end method

.method public abstract onPause(I)V
.end method

.method public abstract onPlay(I)V
.end method

.method public abstract onResume(I)V
.end method

.method public abstract onSeek(I)V
.end method

.method public abstract onSendClientDisconnect(I)V
.end method

.method public abstract onSendClientInitialInfo(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V
.end method

.method public abstract onSendClientMVRequest(IIJ)V
.end method

.method public abstract onSendContentURL(I)V
.end method

.method public abstract onSendMasterClock(I)V
.end method

.method public abstract onSendPlayerState(I)V
.end method

.method public abstract onSetDataSource(I)V
.end method

.method public abstract onSetPlayPosition(I)V
.end method

.method public abstract onShow(I)V
.end method

.method public abstract onStop(I)V
.end method

.method public abstract sendVolumeValueUpdateById(II)V
.end method
