.class Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$4;
.super Ljava/lang/Object;
.source "VideoSubtitlePopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleMenu()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$4;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x0

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$4;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleView:Landroid/view/View;
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$1402(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Landroid/view/View;)Landroid/view/View;

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$4;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mFontStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$1500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/view/VideoStateView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$4;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mFontStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$1500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/view/VideoStateView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoStateView;->setVisibility(I)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$4;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mFontStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$1502(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Lcom/sec/android/app/mv/player/view/VideoStateView;)Lcom/sec/android/app/mv/player/view/VideoStateView;

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$4;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$1302(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 310
    return-void
.end method
