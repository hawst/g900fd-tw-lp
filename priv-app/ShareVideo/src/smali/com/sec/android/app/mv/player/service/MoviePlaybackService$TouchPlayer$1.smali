.class Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$1;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)V
    .locals 0

    .prologue
    .line 1649
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$1;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 1651
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: onCompletionListener()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1658
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1659
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setProgressMax()V

    .line 1662
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCompletedListenrCall:Z

    .line 1663
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$1;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2000(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1665
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1666
    const-string v0, "MoviePlaybackService"

    const-string v1, "MultiVision_onCompletionListener send MVPlayerClient status to server"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1667
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/SUtils;->sendPlayerState(I)V

    .line 1671
    :goto_0
    return-void

    .line 1669
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$1;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v1, 0x65

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)V

    goto :goto_0
.end method
