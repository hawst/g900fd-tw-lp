.class public Lcom/sec/android/app/mv/player/db/SharedPreference;
.super Ljava/lang/Object;
.source "SharedPreference.java"


# static fields
.field private static final APPPREFS:Ljava/lang/String; = "P1SharedPreferences"

.field public static final AUTO_PLAY:Ljava/lang/String; = "autoPlay"

.field public static final BRIGHTNESS_LEVEL:Ljava/lang/String; = "Brightness_Level"

.field public static final CHECK_STATUS:Ljava/lang/String; = "showscontextualcheckstatus"

.field public static final COLOUR_TONE:Ljava/lang/String; = "colortone"

.field public static final CURRENT_CURSOR:Ljava/lang/String; = "currentlist"

.field public static final CURRENT_TAB:Ljava/lang/String; = "current_tab"

.field public static final FIRST_VISIBLE_ITEM:Ljava/lang/String; = "firstVisibleItem"

.field public static final FOLDER_INDEX:Ljava/lang/String; = "folder_index"

.field public static final IS_AUTO_BRIGHTNESS:Ljava/lang/String; = "Is_Auto_Brightness"

.field public static final IS_FIRST_TIME_DROPBOX:Ljava/lang/String; = "is_first_time_without_dropbox_account"

.field public static final LAST_PLAYED_ITEM:Ljava/lang/String; = "lastPlayedItem"

.field public static final LAST_PLAYED_ITEM_OF_AIA:Ljava/lang/String; = "lastPlayedItemOfAIA"

.field public static final OUTDOOR_VISIBILITY:Ljava/lang/String; = "outdoorvisibility"

.field public static final SCREEN_MODE:Ljava/lang/String; = "screen_mode"

.field public static final SHOW_ALLSHARECAST_POPUP:Ljava/lang/String; = "showallsharecastpopup"

.field public static final SHOW_SHAREVIDEO_POPUP:Ljava/lang/String; = "showsharevideopopup"

.field public static final SHOW_SWITCH_STATE:Ljava/lang/String; = "showsswitchstate_contextual"

.field public static final SORT_ORDER:Ljava/lang/String; = "sortorder"

.field public static final SOUND_EFFECT:Ljava/lang/String; = "sound_effect"

.field public static final SPLIT_WIDTH:Ljava/lang/String; = "split_width"

.field public static final SUBTITLE_ACTIVATION:Ljava/lang/String; = "subtitle_activation"

.field public static final SUBTITLE_FONT:Ljava/lang/String; = "subtitle_font"

.field public static final SUBTITLE_FONTBGCOLOR:Ljava/lang/String; = "subtitle_font_backgroundcolor"

.field public static final SUBTITLE_FONTCOLOR:Ljava/lang/String; = "subtitle_font_color"

.field public static final SUBTITLE_FONTPACKAGE:Ljava/lang/String; = "subtitle_font_pakcagename"

.field public static final SUBTITLE_FONTSTRING:Ljava/lang/String; = "subtitle_font_string"

.field public static final SUBTITLE_PATH:Ljava/lang/String; = "subtitle_path"

.field public static final SUBTITLE_SIZE:Ljava/lang/String; = "subtitle_size"

.field public static final SUBTITLE_SYNC_TIME:Ljava/lang/String; = "subtitle_synctime"

.field private static final TAG:Ljava/lang/String; = "SharedPreferenceManager"

.field public static final TEXT_DATE_STATUS:Ljava/lang/String; = "showscontextualdatestatus"

.field public static final TEXT_LOCATION_STATUS:Ljava/lang/String; = "showscontextuallocationstatus"

.field public static final TEXT_WEATHER_STATUS:Ljava/lang/String; = "showscontextualweatherstatus"

.field public static final VIDEO_CAPTURE:Ljava/lang/String; = "videoCapture"

.field public static final VIEW_BY_OPTIONS_IN_CLOUD:Ljava/lang/String; = "list_view_by_cloud"

.field private static mContext:Landroid/content/Context;

.field private static mUniqueInstance:Lcom/sec/android/app/mv/player/db/SharedPreference;


# instance fields
.field private mode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/mv/player/db/SharedPreference;->mUniqueInstance:Lcom/sec/android/app/mv/player/db/SharedPreference;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/db/SharedPreference;->mode:I

    .line 54
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/SharedPreference;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/app/mv/player/db/SharedPreference;->mUniqueInstance:Lcom/sec/android/app/mv/player/db/SharedPreference;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lcom/sec/android/app/mv/player/db/SharedPreference;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/db/SharedPreference;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/db/SharedPreference;->mUniqueInstance:Lcom/sec/android/app/mv/player/db/SharedPreference;

    .line 60
    :cond_0
    sput-object p0, Lcom/sec/android/app/mv/player/db/SharedPreference;->mContext:Landroid/content/Context;

    .line 61
    sget-object v0, Lcom/sec/android/app/mv/player/db/SharedPreference;->mUniqueInstance:Lcom/sec/android/app/mv/player/db/SharedPreference;

    return-object v0
.end method


# virtual methods
.method public loadBooleanKey(Ljava/lang/String;Z)Z
    .locals 5
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "returnValue"    # Z

    .prologue
    .line 157
    :try_start_0
    sget-object v2, Lcom/sec/android/app/mv/player/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v3, "P1SharedPreferences"

    iget v4, p0, Lcom/sec/android/app/mv/player/db/SharedPreference;->mode:I

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 158
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 162
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return p2

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "SharedPreferenceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public loadIntKey(Ljava/lang/String;I)I
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "returnValue"    # I

    .prologue
    .line 134
    const/4 v1, 0x0

    .line 136
    .local v1, "key":I
    :try_start_0
    sget-object v3, Lcom/sec/android/app/mv/player/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v4, "P1SharedPreferences"

    iget v5, p0, Lcom/sec/android/app/mv/player/db/SharedPreference;->mode:I

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 137
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2, p1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 142
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return v1

    .line 138
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Ljava/lang/Exception;
    move v1, p2

    .line 140
    const-string v3, "SharedPreferenceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public loadLongKey(Ljava/lang/String;J)J
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "returnValue"    # J

    .prologue
    .line 147
    :try_start_0
    sget-object v2, Lcom/sec/android/app/mv/player/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v3, "P1SharedPreferences"

    iget v4, p0, Lcom/sec/android/app/mv/player/db/SharedPreference;->mode:I

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 148
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1, p1, p2, p3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    .line 152
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-wide p2

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "SharedPreferenceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public loadStringKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;

    .prologue
    .line 123
    const-string v1, ""

    .line 125
    .local v1, "key":Ljava/lang/String;
    :try_start_0
    sget-object v3, Lcom/sec/android/app/mv/player/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v4, "P1SharedPreferences"

    iget v5, p0, Lcom/sec/android/app/mv/player/db/SharedPreference;->mode:I

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 126
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string v3, ""

    invoke-interface {v2, p1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 130
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-object v1

    .line 127
    :catch_0
    move-exception v0

    .line 128
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "SharedPreferenceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public saveState(Ljava/lang/String;I)V
    .locals 7
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # I

    .prologue
    .line 72
    :try_start_0
    sget-object v4, Lcom/sec/android/app/mv/player/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v5, "P1SharedPreferences"

    iget v6, p0, Lcom/sec/android/app/mv/player/db/SharedPreference;->mode:I

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 73
    .local v3, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 74
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 75
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 76
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 82
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "SharedPreferenceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 79
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 80
    .local v2, "oome":Ljava/lang/OutOfMemoryError;
    const-string v4, "SharedPreferenceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "saveState"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public saveState(Ljava/lang/String;J)V
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # J

    .prologue
    .line 102
    sget-object v2, Lcom/sec/android/app/mv/player/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v3, "P1SharedPreferences"

    iget v4, p0, Lcom/sec/android/app/mv/player/db/SharedPreference;->mode:I

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 103
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 104
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 105
    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 106
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 107
    return-void
.end method

.method public saveState(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 91
    :try_start_0
    sget-object v3, Lcom/sec/android/app/mv/player/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v4, "P1SharedPreferences"

    iget v5, p0, Lcom/sec/android/app/mv/player/db/SharedPreference;->mode:I

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 92
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 93
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 94
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 95
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 96
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "SharedPreferenceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public saveState(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # Z

    .prologue
    .line 110
    sget-object v2, Lcom/sec/android/app/mv/player/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v3, "P1SharedPreferences"

    iget v4, p0, Lcom/sec/android/app/mv/player/db/SharedPreference;->mode:I

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 111
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 112
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 113
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 114
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 115
    return-void
.end method
