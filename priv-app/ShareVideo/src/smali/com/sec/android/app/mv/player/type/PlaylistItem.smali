.class public Lcom/sec/android/app/mv/player/type/PlaylistItem;
.super Ljava/lang/Object;
.source "PlaylistItem.java"


# instance fields
.field private mDuration:J

.field private mId:J

.field private mIsPlayed:I

.field private mPath:Ljava/lang/String;

.field private mResumePos:I

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;JLjava/lang/String;II)V
    .locals 0
    .param p1, "vId"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "duration"    # J
    .param p6, "path"    # Ljava/lang/String;
    .param p7, "isPlayed"    # I
    .param p8, "rPos"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-wide p1, p0, Lcom/sec/android/app/mv/player/type/PlaylistItem;->mId:J

    .line 26
    iput-object p3, p0, Lcom/sec/android/app/mv/player/type/PlaylistItem;->mTitle:Ljava/lang/String;

    .line 27
    iput-wide p4, p0, Lcom/sec/android/app/mv/player/type/PlaylistItem;->mDuration:J

    .line 28
    iput-object p6, p0, Lcom/sec/android/app/mv/player/type/PlaylistItem;->mPath:Ljava/lang/String;

    .line 29
    iput p7, p0, Lcom/sec/android/app/mv/player/type/PlaylistItem;->mIsPlayed:I

    .line 30
    iput p8, p0, Lcom/sec/android/app/mv/player/type/PlaylistItem;->mResumePos:I

    .line 31
    return-void
.end method


# virtual methods
.method public getDuration()J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/type/PlaylistItem;->mDuration:J

    return-wide v0
.end method

.method public getIsPlayed()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/sec/android/app/mv/player/type/PlaylistItem;->mIsPlayed:I

    return v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/mv/player/type/PlaylistItem;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getResumePos()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/mv/player/type/PlaylistItem;->mResumePos:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/mv/player/type/PlaylistItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoID()J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/type/PlaylistItem;->mId:J

    return-wide v0
.end method

.method public setResumePos(I)V
    .locals 0
    .param p1, "resumePos"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/sec/android/app/mv/player/type/PlaylistItem;->mResumePos:I

    .line 59
    return-void
.end method
