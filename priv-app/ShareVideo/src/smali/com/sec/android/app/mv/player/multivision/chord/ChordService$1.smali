.class Lcom/sec/android/app/mv/player/multivision/chord/ChordService$1;
.super Ljava/lang/Object;
.source "ChordService.java"

# interfaces
.implements Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->initialize(Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$1;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(I)V
    .locals 2
    .param p1, "interfaceType"    # I

    .prologue
    .line 219
    const-string v0, "MVChordService"

    const-string v1, "onConnected E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$1;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$1;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onConnectivityChanged()V

    .line 224
    :cond_0
    return-void
.end method

.method public onDisconnected(I)V
    .locals 2
    .param p1, "interfaceType"    # I

    .prologue
    .line 228
    const-string v0, "MVChordService"

    const-string v1, "onDisconnected E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$1;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$1;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onConnectivityChanged()V

    .line 233
    :cond_0
    return-void
.end method
